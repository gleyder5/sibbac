sibbac20
    .controller(
                'CuentaCompensacionController',
                [
                 '$scope',
                 '$rootScope',
                 'CuentaCompensacionService',
                 'CuentaLiquidacionService',
                 'EntidadRegistroService',
                 'growl',
                 '$compile',
                 'ngDialog',
                 '$document',
                 '$location',
                 'SecurityService',
                 function ($scope, $rootScope, CuentaCompensacionService, CuentaLiquidacionService,
                           EntidadRegistroService, growl, $compile, ngDialog, $document, $location, SecurityService) {
                   //
                   var oTable = undefined;
                   $scope.cuentasList = [];
                   $scope.tiposER = [];
                   $scope.tiposCuenta = [];
                   $scope.tiposCuentaCompensacion = [];
                   $scope.tiposNeteoTitulos = [];
                   $scope.tiposNeteoEfectivo = [];
                   $scope.tiposFichero = [];
                   $scope.sistemasLiquidacion = {};
                   $scope.cuentasMercados = [];
                   $scope.cuentasLiquidacion = {};
                   $scope.entidadesDeRegistro = {};
                   // modelo datos para la cuenta seleccionada
                   $scope.cuentaCopy = {};
                   $scope.cuenta = new CuentaDeCompensacion();
                   // end
                   // accion si va a modificar o crear
                   $scope.action = "";
                   $scope.actionTitle = "";
                   $scope.needRefresh = false;
                   $scope.dialogSavingAction = false;
                   // valor seleccionado del autocomplete Entidad Registro
                   $scope.idER;
                   function showErrorMessage (msg) {
                     $.unblockUI();
                     growl.addErrorMessage(msg);
                   }
                   function onLoadConsultarHandler (data) {
                     if (data.error !== null && data.error !== undefined && data.error !== "") {
                       $.unblockUI();
                       showErrorMessage(data.error);
                     } else {
                       var datos = {};
                       if (data !== undefined && data.resultados !== undefined && data.resultados !== null
                           && data.resultados.result_cuentas_compensacion !== undefined
                           && data.resultados.result_cuentas_compensacion !== null) {
                         datos = data.resultados.result_cuentas_compensacion;
                         $scope.cuentasList = datos;
                       }
                       populateDataTable(datos);
                       $.unblockUI();
                     }
                   }
                   function consultar (params) {
                     inicializarLoading();
                     CuentaCompensacionService.getCuentasDeCompensacion(onLoadConsultarHandler, showErrorMessage,
                                                                        params);
                   }
                   function populateDataTable (datos) {
                     var difClass = 'centrado';
                     oTable.clear();
                     var frecuencia = 0;
                     var cuentaLiquidacion = "";
                     var entidadRegistro = "";
                     var cl = null;
                     var codigoS3 = "";
                     var esClearing = "";
                     var idCompensador = "";
                     var idTipoCuenta = "";
                     var subcustodio = "";
                     var numCtaSubcustodio = "";
                     var modCta = "";
                     var sistemaLiquidacion = {};
                     angular.forEach(datos, function (val, key) {
                       cl = val.cuentaLiquidacion;
                       entidadRegistro = '<span data="'
                                         + ((val.entidadRegistro !== null) ? val.entidadRegistro.id : "") + '">'
                                         + ((val.entidadRegistro !== null) ? val.entidadRegistro.nombre : "")
                                         + '</span>';
                       frecuencia = val.frecuenciaEnvioExtracto;
                       frecuencia = $.number(frecuencia, 0, ',', '.');
                       cuentaLiquidacion = "<ul class=\"inner-table-ul\">";
                       cuentaLiquidacion += '<li id="' + val.cuentaLiquidacionId + '">' + val.cuentaLiquidacionCodigo
                                            + '</li>';
                       cuentaLiquidacion += "</ul>";
                       codigoS3 = val.codigoS3;
                       esClearing = val.esClearing;
                       idCompensador = val.idCompensador;
                       idTipoCuenta = val.idTipoCuenta;
                       subcustodio = (val.subcustodio === null) ? "" : val.subcustodio;
                       numCtaSubcustodio = (val.numCtaSubcustodio === null) ? "" : val.numCtaSubcustodio;

                       // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                       var modCta = "";
                       if (SecurityService.inicializated) {
                         if (SecurityService.isPermisoAccion($rootScope.SecurityActions.MODIFICAR, $location.path())) {
                           modCta = '<a class="btn" data="' + val.idCuentaCompensacion + '" ng-click="modificarCuenta('
                                    + val.idCuentaCompensacion + ');"><img src="img/editp.png"  title="editar" /></a>';
                         }
                       }

                       sistemaLiquidacion = val.sistemaLiquidacion === null ? {
                         id : '',
                         codigo : ''
                       } : val.sistemaLiquidacion;
                       oTable.row.add([
                                       modCta,
                                       val.idCuentaCompensacion,
                                       entidadRegistro,
                                       (val.entidadRegistro !== null) ? val.entidadRegistro.tipoer.nombre : "",
                                       addMercados(val.relacionCuentaMercadoDataList, "mercados_" + key),
                                       val.numCuentaER,
                                       (val.tipoCuentaConciliacion !== null) ? val.tipoCuentaConciliacion.name : '',
                                       (val.tipoNeteoTitulo !== null) ? val.tipoNeteoTitulo.nombre : '',
                                       (val.tipoNeteoEfectivo !== null) ? val.tipoNeteoEfectivo.nombre : '',
                                       val.cdCodigo,
                                       cuentaLiquidacion,
                                       frecuencia,
                                       val.tipoFicheroConciliacion,
                                       codigoS3,
                                       esClearing,
                                       idCompensador,
                                       idTipoCuenta,
                                       subcustodio,
                                       numCtaSubcustodio,
                                       '<span id="' + sistemaLiquidacion.id + '">' + sistemaLiquidacion.codigo
                                           + '</span>', val.cdCuentaCompensacion ], false);
                     });
                     oTable.draw();
                   }
                   function addMercados (datos, id) {
                     // var list = "<select class=\"inner-select\" id=\""+id+"\"
                     // multiple=\"multiple\" >";
                     var list = '<ul class="inner-table-ul" id="' + id + '" >';
                     var valor;
                     var texto;
                     $(datos).each(function (key, val) {
                       valor = val.id;
                       texto = val.codigo;
                       // list +='<option value="'+valor+'">'+texto+
                       // '</option>';
                       list += '<li value="' + valor + '" >' + texto + '</li>';
                     });
                     list += "</ul>";
                     // list +="</select>";
                     return list;
                   }
                   // obtiene el id de entidad de registro seleccionada
                   function getIdEntidadRegistro () {
                     var er = $("#ER").val();
                     if (er === undefined) {
                       return "";
                     }
                     er = er.toLowerCase();
                     if (er.trim().length > 0 && er.indexOf("tipo") !== -1) {
                       er = er.substring(0, er.indexOf("tipo") - 1);
                     }
                     return er;
                   }
                   function obtenerIdsSeleccionados (lista) {
                     var selected = "";

                     $(lista + ' option').each(function (key, val) {
                       if (key > 0) {
                         selected += ", ";
                       }
                       selected += val.value.trim();
                     });

                     return selected;
                   }
                   // ///////////////////////////////////////////////
                   // ////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
                   function initialize () {

                     cargaCombos();
                     // autocomplete multiple
                     CuentaCompensacionService.getCuentasMercado(cargarCuentasMercados, showErrorMessage, {});
                     CuentaCompensacionService.getCuentaLiquidacion(cargarCuentasLiquidacion, showErrorMessage, {});
                     CuentaCompensacionService.getListaCompensadores(cargarCompensadores, showErrorMessage, {});
                     CuentaCompensacionService.getTiposCuentaCompensacion(cargarTiposCuentaCompensacion,
                                                                          showErrorMessage, {});
                     obtenerDatos();
                     consultar({});
                   }
                   function cargarCuentasMercados (json) {
                     var availableTags = [];
                     var datos = undefined;
                     if (json.error === undefined || json.error === null || json.error === "") {
                       if (json === undefined || json.resultados === undefined
                           || json.resultados.result_cuentas_mercados === undefined) {
                         datos = {};
                       } else {
                         datos = json.resultados.result_cuentas_mercados;
                         $scope.cuentasMercados = datos;
                       }
                     } else {
                       $.unblockUI();
                       growl.addErrorMessage("Error: " + json.error);
                     }
                     availableTags = cargarElementos(datos, "id", "codigo", "descripcion");
                     populateMCAComponents("#MCA", "#selectedMCA", "#btnDelMCA", availableTags);
                     populateMCAComponents("#alta_MCA", "#alta_selectedMCA", "#alta_btnDelMCA", availableTags);
                   }

                   /**
                     * Para cargar el autocomplete de los mercados disponibles
                     */
                   function populateMCAComponents (input, lista, boton, availableTags) {
                     $(input).autocomplete({
                       source : availableTags,
                       minLengh : 0,
                       maxLength : 10,
                       focus : function (event, ui) {
                         // $("#MCA" ).val(ui.item.label);
                         return false;
                       },
                       select : function (event, ui) {
                         if (!valueExistInList(lista, ui.item.id)) {
                           $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                           $(input).val("");
                           $(lista + "> option").attr('selected', 'selected');
                         }
                         return false;
                       }
                     });

                     // preparar boton para eliminar filas de mercados selecccionados
                     $(boton).on("click", function (event) {
                       event.preventDefault();
                       $(lista).find('option:selected').remove().end();
                       $(lista + "> option").attr('selected', 'selected');
                     });
                   }
                   /** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
                   function valueExistInList (lista, valor) {
                     var inList = false;

                     $(lista + ' option').each(function (index) {

                       if (this.value == valor) {
                         inList = true;
                         return inList;
                       }
                     });

                     return inList;
                   }
                   /**
                     * Carga autocomplete de tiops de cuenta compensacion
                     */
                   function cargarTiposCuentaCompensacion (json, headers, status, config) {
                     var tipos = [];
                     var datos = undefined;
                     //
                     if (json !== null && json !== undefined) {
                       if (json.error !== null && json.error !== "") {
                         $.unblockUI();
                         growl.addErrorMessage("Error: " + json.error);
                         return false;
                       }
                       if (json.resultados === null || json.resultados.result_tipos_cuenta_compensacion === null) {
                         datos = {};
                       } else {
                         datos = json.resultados.result_tipos_cuenta_compensacion;
                         $scope.tiposCuentaCompensacion = datos;
                       }
                       tipos = cargarElementos(datos, "id", "codigo", "descripcion");
                       populateAutocomplete("#tipoCuenta", "#idTipoCuenta", tipos);
                       populateAutocomplete("#alta_tipoCuenta", "#alta_idTipoCuenta", tipos);
                     }
                   }
                   /**
                     * Carga autocomplete de compensadores
                     */
                   function cargarCompensadores (json) {
                     var compensadores = [];
                     if (json !== null && json !== undefined) {
                       if (json.error !== null && json.error !== "") {
                         $.unblockUI();
                         growl.addErrorMessage("Error: " + json.error);
                         return false;
                       }
                       var datos = undefined;
                       if (json.resultados === null || json.resultados.result_lista_compensadores === null) {
                         datos = {};
                       } else {
                         datos = json.resultados.result_lista_compensadores;
                         $scope.compensadoresList = datos;
                       }
                       compensadores = cargarElementos(datos, "idCompensador", "nombre", "descripcion");
                       populateAutocomplete("#compensador", "#idCompensador", compensadores);
                       populateAutocomplete("#alta_compensador", "#alta_idCompensador", compensadores);
                     }
                   }

                   function populateAutocomplete (autoInput, idInput, datos) {
                     $(autoInput).autocomplete({
                       source : datos,
                       focus : function (event, ui) {
                         $(autoInput).val(ui.item.label);
                         $(idInput).val(ui.item.value);
                         return false;
                       },
                       select : function (event, ui) {
                         $(autoInput).val(ui.item.label);
                         $(idInput).val(ui.item.value);
                         return false;
                       }
                     });
                   }
                   /**
                     * Carga el autocomplete para las cuentas de compensacion
                     *
                     */
                   function cargarCuentasLiquidacion (json) {

                     var datos = undefined;
                     if (json === undefined || json.resultados === undefined || json.resultados === null
                         || json.resultados.result_cuentas_liquidacion === null
                         || json.resultados.result_cuentas_liquidacion === undefined) {
                       datos = {};
                     } else {
                       datos = json.resultados.result_cuentas_liquidacion;
                       $scope.cuentasLiquidacion = datos;
                     }
                     $scope.populateCuentasLiquidacionAutocomplete();
                     // DE MOMENTO NO SE VAN A MODIFICAR LAS CUENTAS DE
                     // COMPENSACION DESDE EL ALTA
                     // populateCuentaRelacionadaAutocomplete("#alta_cuentasrelacionadas","#alta_idCtaComp",
                     // availableTags, "#alta_selectedCC");
                     // fin
                   }
                   $scope.populateCuentasLiquidacionAutocomplete = function () {
                     var obj;
                     var datos = $scope.cuentasLiquidacion;
                     var availableTags = [];
                     $(datos).each(function (key, val) {
                       obj = {
                         label : val.cdCodigo,
                         value : val.id
                       };
                       availableTags.push(obj);
                     });
                     $("#btnDelCL").click(function (event) {
                       event.preventDefault();
                       $("#selectedCL").find("option:selected").remove().end();
                     });
                     populateCuentaRelacionadaAutocomplete("#cuentasrelacionadas", "#idCtaLiq", availableTags,
                                                           "#selectedCL");
                     populateCuentaRelacionadaAutocomplete("#alta_ctaLiq", "#alta_idCtaLiq", availableTags, '');
                   };
                   function populateCuentaRelacionadaAutocomplete (input, idContainer, availableTags, lista) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         if (lista === '') {
                           $(this).val(ui.item.label);
                           $(idContainer).val(ui.item.value);
                         }
                         return false;
                       },
                       select : function (event, ui) {
                         // $(idContainer).val(ui.item.value);
                         if (lista !== '' && !valueExistInList(lista, ui.item.value)) {
                           $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                           $(lista + "> option").attr('selected', 'selected');
                           $(input).val('');

                         }
                         return false;
                       }
                     });

                   }
                   function loadDataTable () {
                     oTable = angular.element("#tblCuentasCompensacion").DataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                       },
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "scrollX" : true,
                       "aoColumns" : [ {
                         "sClass" : "centrado",
                         "bSortable" : "false"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "align-right",
                         "sType" : "numeric"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "align-left"
                       }, {
                         "sClass" : "align-left"
                       }, {
                         "sClass" : "align-center"
                       }, {
                         "sClass" : "align-left"
                       } ],
                       "order" : [ 1 ],
                       "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                         $compile(nRow)($scope);
                       }
                     });
                   }
                   $document.ready(function () {
                     if (oTable === undefined) {
                       loadDataTable();
                     }
                     initialize();
                     prepareCollapsion();
                     angular.element('#cargarTabla').submit(function (event) {
                       event.preventDefault();
                       obtenerDatos();
                       collapseSearchForm();
                       seguimientoBusqueda();
                     });

                     $('#limpiar').click(function (event) {
                       event.preventDefault();
                       $('input[type=text]').val('');
                       $('select').val('0');
                     });

                   });
                   function validarFormulario () {
                     if ($scope.cuenta.cuentaLiquidacionId === '') {
                       alert("Por favor introduzca el Código de la Cuenta de Liquidación.");
                       angular.element("#alta_ctaLiq_value").addClass('autocomplete-required');
                       return false;
                     } else if ($scope.currentTipoCuentaComp.id === '') {
                       alert("Por favor seleccione un Tipo de Cuenta.");
                       angular.element("#alta_ctaLiq_value").addClass('autocomplete-required');
                       return false;
                     } else if ($scope.currentCompensador.idCompensador === '') {
                       alert("Por favor introduzca el id del compensador asociado a la cuenta.");
                       angular.element("#alta_ctaLiq_value").addClass('autocomplete-required');
                       return false;
                     }
                     return true;
                   }
                   $scope.modificarDatosCuenta = function (event) {
                     // event.preventDefault();
                     var isValid = validarFormulario();
                     if (!isValid) {
                       return false;
                     }
                     angular.element('#infoMsgBox').show();
                     var ER = $scope.cuenta.entidadRegistro.id;
                     var MCA = obtenerIdsSeleccionados("#alta_selectedMCA");
                     var numero_cuenta = $scope.cuenta.numCuentaER;
                     var tipo_cuenta = $scope.cuenta.tipoCuentaConciliacion.id;
                     var tipo_neteo_titulos = $scope.cuenta.tipoNeteoTitulo.id;
                     var tipo_neteo_efectivo = $scope.cuenta.tipoNeteoEfectivo.id;
                     var cuantaIberclear = $scope.cuenta.cdCuentaCompensacion;
                     var cdCodigo = $scope.cuenta.cdCodigo;
                     var FEE = $scope.cuenta.frecuenciaEnvioExtracto;
                     var tipo_fichero = $scope.cuenta.tipoFicheroConciliacion;

                     // do validar
                     var filtro = {
                       "entidadRegistro" : ER,
                       "idMercado" : MCA,
                       "numCuentaER" : numero_cuenta,
                       "cdCuentaCompensacion" : cuantaIberclear,
                       "esCuentaPropia" : tipo_cuenta,
                       "tipoNeteoTitulo" : tipo_neteo_titulos,
                       "tipoNeteoEfectivo" : tipo_neteo_efectivo,
                       "cdCodigo" : cdCodigo,
                       "frecuenciaEnvioExtracto" : FEE,
                       "idCtaCompensacion" : $scope.cuenta.idCuentaCompensacion,
                       "idCuentaLiquidacion" : $scope.cuenta.cuentaLiquidacionId,
                       "tipoFicheroConciliacion" : tipo_fichero,
                       "idCompensador" : $scope.cuenta.idCompensador,
                       "idTipoCuentaComp" : $scope.currentTipoCuentaComp.id,
                       "idTipoCuentaConciliacion" : "",
                       "esClearing" : $scope.cuenta.esClearing,
                       "numCtaSubcustodio" : $scope.cuenta.numCtaSubcustodio,
                       "subcustodio" : $scope.cuenta.subcustodio,
                       "idSistemaLiquidacion" : $scope.cuenta.sistemaLiquidacion.id,
                       "codigoS3" : $scope.codigoS3
                     };
                     var data = new DataBean();
                     data.setService('SIBBACServiceCuentasDeCompensacion');
                     data.setAction('addCuentaCompensacion');

                     data.setFilters(JSON.stringify(filtro));
                     var request = requestSIBBAC(data);
                     request.success(function (json) {
                       // alert(json.request.filters.diaMes);
                       $scope.dialogSavingAction = false;
                       angular.element('#infoMsgBox').hide();
                       if (json !== null && json.error !== null && json.error !== "" && json.error !== undefined) {
                         angular.element('#errorMsgText').text(
                                                               "Ocurrió un error al intentar guardar la cuenta: "
                                                                   + json.error);
                         angular.element('#errorMsgBox').show();
                       } else {
                         angular.element('#successMsgText').text("Datos guardados correctamente.");
                         angular.element('#successMsgBox').show();
                         //
                         $scope.needRefresh = true;
                       }
                     });
                   }

                   function seguimientoBusqueda () {
                     $('.mensajeBusqueda').empty();
                     var cadenaFiltros = "";
                     if ($('#ER').val() !== "" && $('#ER').val() !== undefined) {
                       cadenaFiltros += " Entidad de registro: " + $('#ER').val();
                     }
                     if ($('#TER').val() !== "" && $('#TER').val() !== undefined && $('#TER').val() !== "0") {
                       cadenaFiltros += " Tipo de entidad de registro: " + $('#TER').val();
                     }
                     if ($('#MCA').val() !== "" && $('#MCA').val() !== undefined) {
                       cadenaFiltros += " Mercados asociados: " + $('#MCA').val();
                     }
                     if ($('#numero_cuenta').val() !== "" && $('#numero_cuenta').val() !== undefined) {
                       cadenaFiltros += "N&uacute;mero Cuenta: " + $('#numero_cuenta').val();
                     }
                     if ($('#tipo_cuenta').val() !== "" && $('#tipo_cuenta').val() !== undefined
                         && $('#tipo_cuenta').val() !== "0") {
                       cadenaFiltros += " Tipo Cuenta: " + $('#tipo_cuenta').val();
                     }
                     if ($('#tipo_neteo').val() !== "" && $('#tipo_neteo').val() !== undefined
                         && $('#tipo_neteo').val() !== "0") {
                       cadenaFiltros += " Tipo Neteo: " + $('#tipo_neteo').val();
                     }
                     if ($('#FEE').val() !== "" && $('#FEE').val() !== undefined) {
                       cadenaFiltros += " Frecuencia Env&iacute;o Extracto: " + $('#FEE').val();
                     }
                     if ($('#tipo_fichero').val() !== "" && $('#tipo_fichero').val() !== undefined
                         && $('#tipo_fichero').val() !== "0") {
                       cadenaFiltros += " Tipo Fichero: " + $('#tipo_fichero').val();
                     }
                     if ($('#cuentas_relacionadas').val() !== "" && $('#cuentas_relacionadas').val() !== undefined) {
                       cadenaFiltros += " Tipo Fichero: " + $('#cuentas_relacionadas').val();
                     }
                     if ($('#selectedMCA').find('option:selected').length > 0) {
                       cadenaFiltros += " Mercados Contratación: ";
                       $('#selectedMCA').find('option:selected').each(function (key, val) {
                         cadenaFiltros += $(val).text() + " ";
                       });
                     }
                     if ($("#selectedCL").find("option:selected").length > 0) {
                       cadenaFiltros += "Cuentas de Liquidaci&oacute;n: ";
                       $("#selectedCL").find("option:selected").each(function (key, val) {
                         cadenaFiltros += $(val).text() + " ";
                       });
                     }
                     $('.mensajeBusqueda').append(cadenaFiltros);

                   }

                   function obtenerDatos () {
                     var params = {
                       "entidadRegistro.id" : ($("#idER").val() !== undefined && $("#idER").val() !== "") ? $("#idER")
                           .val() : "",
                       /* "tipoer.id" : $("#TER").val(), */
                       "tipoFicheroConciliacion" : $("#tipo_fichero").val(),
                       "idMercado" : obtenerIdsSeleccionados("#selectedMCA"),
                       "numCuentaER" : $("#numero_cuenta").val(),
                       "tipoCuentaConciliacion.id" : $("#tipo_cuenta").val(),
                       "tipoNeteoTitulo.id" : $("#tipo_neteo_titulos").val(),
                       "tipoNeteoEfectivo.id" : $("#tipo_neteo_efectivo").val(),
                       "tmct0CuentaLiquidacion.id" : obtenerIdsSeleccionados("#selectedCL"),
                       "cdCodigo" : $("#cdCodigo").val(),
                       "frecuenciaEnvioExtracto" : $("#FEE").val()
                     };
                     consultar(params);

                   }
                   // mete en tiposNeteoTitulos todos los tipos excepto el nulo
                   function prepararTiposNeteoTitulos (datos) {
                     $scope.tiposNeteoTitulos = [];
                     angular.forEach(datos, function (val, key) {
                       if (val.name.toLowerCase() != "nula") {
                         $scope.tiposNeteoTitulos.push(val)
                       }
                     });

                   }
                   function onRequestDatosComboSuccess (json, id) {
                     var datos = undefined;
                     if (json !== undefined && json.resultados !== undefined) {
                       if (json.resultados.result_tipo_cuenta !== undefined) {
                         $scope.tiposCuenta = json.resultados.result_tipo_cuenta;
                         return false;
                       }
                       if (json.resultados.result_tipo_Neteo !== undefined) {
                         datos = json.resultados.result_tipo_Neteo;

                         prepararTiposNeteoTitulos(datos);
                         $scope.tiposNeteoEfectivo = datos;
                         return false;
                       }
                       if (json.resultados.result_tipo_fichero !== undefined) {
                         datos = json.resultados.result_tipo_fichero;
                         $scope.tiposFichero = datos;
                       }
                       if (json.resultados.result_tipos_entidad_registro !== undefined) {
                         datos = json.resultados.result_tipos_entidad_registro;
                         $scope.tiposER = datos;
                       }
                       if (json.resultados.result_sistemas_liquidacion !== undefined) {
                         datos = json.resultados.result_sistemas_liquidacion;
                         $scope.sistemasLiquidacion = datos;
                       }
                       if (datos === undefined) {
                         return;
                       } else {
                         // $.each(datos, function (key, val) {
                         var idData;
                         var nombre;

                         for (var i = 0; i < datos.length; i++) {
                           idData = (datos[i].id !== undefined) ? datos[i].id : datos[i];
                           if (id === 'sistemas_liq') {
                             nombre = (datos[i].codigo !== undefined) ? datos[i].codigo
                                                                     : ((datos[i].codigo !== undefined) ? datos[i].codigo
                                                                                                       : datos[i]);
                           } else {
                             nombre = (datos[i].nombre !== undefined) ? datos[i].nombre
                                                                     : ((datos[i].name !== undefined) ? datos[i].name
                                                                                                     : datos[i]);
                           }

                         }
                         // });
                       }
                     }
                   }
                   // llamada para cargar datos combos
                   function cargaCombos () {
                     CuentaLiquidacionService.getTipoCuenta(onRequestDatosComboSuccess, showErrorMessage, {},
                                                            'tipo_cuenta');
                     CuentaLiquidacionService.getTipoNeteo(onRequestDatosComboSuccess, showErrorMessage, {},
                                                           'tipo_neteo_titulos');
                     CuentaLiquidacionService.getTipoNeteo(onRequestDatosComboSuccess, showErrorMessage, {},
                                                           'tipo_neteo_efectivo');
                     CuentaLiquidacionService.getTipoFichero(onRequestDatosComboSuccess, showErrorMessage, {},
                                                             'tipo_fichero');
                     EntidadRegistroService
                         .getAllTiposEntidadRegistro(onRequestDatosComboSuccess, showErrorMessage, {});
                     EntidadRegistroService.getAllEntidadRegistro(cargarEntidadesRegistro, showErrorMessage, {});
                     CuentaLiquidacionService.getSistemasLiquidacion(cargarSistemasLiquidacion, showErrorMessage, {},
                                                                     "");
                   }
                   function cargarSistemasLiquidacion (json, idComponent) {
                     onRequestDatosComboSuccess(json);
                   }
                   function cargarEntidadesRegistro (json) {
                     var datos = undefined;
                     if (json !== undefined && json.resultados !== null && json.resultados !== undefined) {
                       if (json.resultados.result_entidades_de_registro !== null
                           && json.resultados.result_entidades_de_registro !== undefined) {
                         datos = json.resultados.result_entidades_de_registro;
                         $scope.entidadesDeRegistro = datos;
                         var tags = new Array(datos.length);
                         var tag;
                         $(datos).each(function (key, val) {
                           if (val.tipoer !== null) {
                             tag = {
                               label : val.nombre + " - " + val.tipoer.nombre,
                               value : val.id,
                               tipoId : val.tipoer.id,
                               tipoName : val.tipoer.nombre
                             };
                             tags[key] = tag;
                           }

                         });

                         loadAutocompleteER("#ER", "#TER", "#idER", tags);
                       } else {
                         return false;
                       }
                     }
                   }
                   /**
                     * Cuando se efectua un cambio en la entidad registro, hay que seleccionar el tipo de entidad en el
                     * combo, si la entidad registro esta vacia, se vacia lo que haya en la lista del tipo de entidad de
                     * registro.
                     *
                     * @returns {undefined}
                     */
                   $scope.onEntidadRegistroChange = function () {
                     if ($scope.idER === "") {
                       angular.element("#idER").val("");
                       angular.element("#TER").val("");
                     }
                   };
                   function loadAutocompleteER (idER, idTER, idHidden, tags) {
                     $(idER).autocomplete({
                       source : tags,
                       focus : function (event, ui) {
                         $(this).val(ui.item.label);
                         return false;
                       },
                       select : function (event, ui) {
                         $(idHidden).val(ui.item.value);
                         $(idTER).val(ui.item.tipoId);
                         return false;
                       }
                     });
                   }
                   function getSelectedCuenta (id) {
                     // var cuenta = new CuentaDeCompensacion();
                     $.each($scope.cuentasList, function (key, val) {
                       if (id == val.idCuentaCompensacion) {
                         cuenta = val;
                         return false;
                       }
                     });
                     return cuenta;
                   }
                   function limpiarMercadosCuenta () {
                     $("#alta_selectedMCA").find("option").remove().end();
                   }
                   function selectOption (queLista, texto) {
                     $(queLista + " option:selected").attr('selected', 'false');
                     var external = "";
                     var inList = "";
                     $(queLista).find("option").each(function (key, val) {
                       inList = $(val).text().trim().toLowerCase();
                       external = texto.trim().toLowerCase();

                       if (inList === external) {
                         $(val).attr("selected", "selected");
                       }
                     });
                   }
                   function extraerDatosCtaLiquidacion (celda) {
                     var idCta = $(celda).find("ul > li").attr("id");
                     var codCta = $(celda).find("ul > li").text();
                     //
                     $("#alta_ctaLiq").val(codCta);
                     $("#alta_idCtaLiq").val(idCta);
                   }

                   $scope.isTipoERSelected = function (id) {
                     var res = ($scope.cuenta.tipoER !== null && id === $scope.cuenta.tipoER.id);
                     // console.log("id: "+id + " tipoER.id: "+$scope.cuenta.tipoER.id + " res: "+res);
                     return res;
                   };
                   $scope.isSameIdTipoCuenta = function (id) {
                     if ($scope.cuenta.tipoCuentaConciliacion === null) {
                       $scope.cuenta.tipoCuentaConciliacion = {};
                       $scope.cuenta.tipoCuentaConciliacion.id = "";
                     }
                     var res = id === $scope.cuenta.tipoCuentaConciliacion.id;
                     // console.log("id: "+id + " cuenta.tipoCuentaConciliacion.id:
                     // "+$scope.cuenta.tipoCuentaConciliacion.id + " res: "+res);
                     return res;
                   };
                   function initializeMessagesBox () {
                     $('#successMsgBox').hide();
                     $('#errorMsgBox').hide();
                     $('#successMsgText').text("");
                     $('#errorMsgText').text("");
                   }
                   $scope.resetForm = function (event) {
                     event.preventDefault();
                     $scope.cuenta = $scope.cuentaCopy;
                   }
                   $scope.modificarCuenta = function (id) {
                     $scope.cuenta = new CuentaDeCompensacion();
                     if (id > 0) {
                       $scope.actionTitle = "Modificar Cuenta De Compensación";
                       $scope.action = "modificar";
                       $scope.cuenta = getSelectedCuenta(id);

                       // por si se quiere resetear
                       $scope.cuentaCopy = angular.copy($scope.content);
                     } else {
                       // alta
                       $scope.actionTitle = "Crear Cuenta De Compensación";
                       $scope.action = "alta";
                     }
                     $scope.currentCompensador = {
                       idCompensador : $scope.cuenta.idCompensador,
                       nombre : "",
                       descripcion : ""
                     };
                     $scope.currentTipoCuentaComp = {
                       id : $scope.cuenta.idTipoCuenta,
                       codigo : "",
                       descripcion : ""
                     };

                     initializeMessagesBox();
                     ngDialog.open({
                       template : 'template/configuracion/cuenta-de-compensacion-template.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-480',
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           obtenerDatos();
                           $scope.needRefresh = false;
                         }
                       }
                     });
                   };

                   $scope.selectedCuentaLiquidacion = function (selected) {
                     if (selected == undefined || selected.originalObject === undefined) {
                       return false;
                     }
                     $scope.cuenta.cuentaLiquidacionId = selected.originalObject.id;
                     $scope.cuenta.cuentaLiquidacionCodigo = selected.originalObject.cdCodigo;

                   };
                   $scope.selectedCuentaMercado = function (selected) {
                     if (selected === undefined || selected.originalObject === undefined) {
                       return false;
                     }
                     var inArray = false;
                     var toSearch = selected.originalObject.codigo;

                     angular.forEach($scope.cuenta.relacionCuentaMercadoDataList, function (mkt, key) {
                       if (mkt.codigo === toSearch) {
                         inArray = true;
                         return false;
                       }
                     });
                     if (!inArray) {
                       $scope.cuenta.relacionCuentaMercadoDataList.push(selected.originalObject);
                     }
                   };
                   $scope.selectedMCA = {
                     id : ""
                   };
                   $scope.removeSelectedMCA = function (event) {
                     event.preventDefault();
                     if ($scope.selectedMCA.id === "" || $scope.selectedMCA.id === undefined) {
                       return false;
                     }
                     var aux = [];
                     angular.forEach($scope.cuenta.relacionCuentaMercadoDataList, function (mkt, key) {
                       if (mkt.id != $scope.selectedMCA.id) {
                         aux.push(mkt);
                       }
                     });
                     $scope.cuenta.relacionCuentaMercadoDataList = [];
                     $scope.cuenta.relacionCuentaMercadoDataList = aux;
                     return false;
                   };
                   $scope.selectedCompensador = function (selected) {
                     if (selected !== undefined && selected.originalObject !== undefined) {
                       $scope.cuenta.idCompensador = selected.originalObject.idCompensador;
                       $scope.currentCompensador = selected.originalObject;
                     }
                   };
                   $scope.selectedTipoCuentaComp = function (selected) {
                     if (selected !== undefined && selected.originalObject !== undefined) {
                       $scope.cuenta.idTipoCuenta = selected.originalObject.id;
                       $scope.currentTipoCuentaComp = selected.originalObject;
                     }
                   };
                   $scope.selectedER = function (selected) {
                     if (selected !== undefined && selected.originalObject !== undefined) {
                       $scope.cuenta.entidadRegistro = selected.originalObject;
                     }
                   }

                 } ]);
