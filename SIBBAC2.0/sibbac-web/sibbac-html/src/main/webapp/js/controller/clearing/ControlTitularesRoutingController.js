'use strict';

sibbac20
	.controller('ControlTitularesRoutingController', ['$scope',	'$compile',	'growl', 'JobsService', '$document',
			'TitularesRoutingService', 	'IsinService', 'AliasService', '$rootScope', '$location', 'SecurityService',
			'$httpParamSerializer',function ($scope, $compile, growl, JobsService, $document, TitularesRoutingService, IsinService,
					AliasService, $rootScope, $location, SecurityService, $httpParamSerializer) {
				var self = this;
				var oTable = undefined, lMarcar = true, hoy, listaModi;
				var openError = [], openPrecio = [];
				var alPaginas = [], pagina = 0, paginasTotales = 0, ultimaPagina = 0;
				var inicializaCombos = function() {
					TitularesRoutingService.loadCombo("getErrorsList", "errorList", function(datos){
						$scope.errorList = datos;
					}, $scope.failCombo);
					$scope.tiposConsulta = [
						{value: 0, label: "TODOS LOS TITULARES"},
						{value: 1, label: "TITULARES SIN ERROR"},
						{value: 2, label: "TITULARES CON ERROR"},
						{value: 3, label: "TITULARES ROUTING SIN ORDEN SV"},
						{value: 4, label: "DESGLOSES SIN TITULAR"}
					];
					$scope.mercados = [
						{value: "N", label: "NACIONAL"},
						{value: "E", label: "INTERNACIONAL"}
					];
					$scope.listaChk = [];
				};
				self.listAfiIds = function() {
					var list = [], data = oTable.fnGetData(), l = data.length, c, r;
					for(var i = 0; i < l; i++) {
						if($scope.listaChk[i] && !data[i][1]) {
							r = data[i];
							list.push({
								nuorden: r[c = 27],
								nbooking: r[++c],
								nucnfclt: r[++c],
								nucnfliq: r[++c],
								nusecuen: r[++c]});
						}
					}
					return list;
				};
				self.listAfiErrorIds = function() {
					var list = [], data = oTable.fnGetData(), l = data.length, c, r;
					for(var i = 0; i < l; i++) {
						if($scope.listaChk[i] && data[i][1]) {
							list.push(data[i][1]);
						}
					}
					return list;
				};
				var recuperaDatosComunes = function() {
					var data, l, w, comunes = [], primera = true;
					listaModi = [];
					data = oTable.fnGetData();
					l = data.length;
					for (var i = 0; i < l; i++) 
						if($scope.listaChk[i]) {
							listaModi.push(traduceIdDesglose(data[i]));
							if(primera) {
								w = data[i].length;
								for(var j = 0; j < w; j++) 
									comunes[j] = data[i][j];
								primera = false;
							}
							else {
								for(var j = 0; j < w; j++)
									if(comunes[j] && comunes[j] !== data[i][j])
										comunes[j] = undefined;
							}
						}
					if(primera) {
						fErrorTxt("Debe seleccionar al menos un registro");
						return null;
					}
					return comunes;
				};
				var tryTrim = function(value) {
					if(angular.isString(value))
						return value.trim();
					return value;
				};
				var traduceTitular = function(comunes) {
					var i;
					return {
							ccv: tryTrim(comunes[4]),
							nbcliente: tryTrim(comunes[i = 10]),
							nbcliente1: tryTrim(comunes[++i]),
							nbcliente2: tryTrim(comunes[++i]),
							tpidenti: tryTrim(comunes[++i]),
							nudnicif: tryTrim(comunes[++i]),
							tpsocied: tryTrim(comunes[++i]),
							cddepais: tryTrim(comunes[++i]),
							tpnactit: tryTrim(comunes[++i]),
							nbdomici: tryTrim(comunes[++i]),
							nbciudad: tryTrim(comunes[++i]),
							nbprovin: tryTrim(comunes[++i]),
							cdpostal: tryTrim(comunes[++i]),
							tptiprep: tryTrim(comunes[++i]),
							particip: comunes[++i],
							cdnactit: tryTrim(comunes[i = 32]),
							tpdomici: tryTrim(comunes[++i]),
							nudomici: tryTrim(comunes[++i])
					};
				};
				var traduceIdDesglose = function(comunes) {
					var i, l;
					return {
						iderror: comunes[1],
						nuorden: comunes[i = 27],
						nbooking: comunes[++i],
						nucnfclt: comunes[++i],
						nucnfliq: comunes[++i],
						nusecuen: comunes[++i]
					};
				};
				var inicializaComponestsJQ = function () {
					$("#ui-datepicker-div").remove();
					$('input#fcontratacionDe').datepicker({
						onClose : function (selectedDate) {
							$("#fcontratacionA").datepicker("option", "minDate", selectedDate);
						} // onClose
					}); // $('input#fcontratacionDe').datepicker
					$('input#fcontratacionA').datepicker({
						onClose : function (selectedDate) {
							$("#fcontratacionDe").datepicker("option", "maxDate", selectedDate);
						} // onClose
					}); // $('input#fcontratacionA').datepicker
					$('#selecFichero').change(function (event) {
						event.preventDefault();
						cargarFichero(event.target.files);
					}); // $('#selecFichero').change
					$('#btnSelecFichero').click(function (event) {
						event.preventDefault();
						$('#cargarFichero').css('display', 'none');
						enviarFichero();
					}); // $('#btnSelecFichero').click
                    // Botón X que cierra formulario de importación de ficheros.
                    $('#cerrarimportacion').click(function (event) {
                      event.preventDefault();
                      $('#cargarFichero').css('display', 'none');
                    }); // $('#cerrarimportacion').click
                    // Botón X que cierra formulario de importación de ficheros.
                    $('#cerrarimportacion').click(function (event) {
                      event.preventDefault();
                      $('#cargarFichero').css('display', 'none');
                    }); // $('#cerrarimportacion').click
                    self.importaFinalHoldersDialog = angular.element("#importaFinalHoldersDialog");
                    self.importaFinalHoldersDialog.dialog({
                    	width: 600,
                    	autoOpen: false, title: "Importa Final Holders", modal: true,
                    	buttons: {
                    		"Agrega" : function() { self.ejecutaImportacion("agregaImportacion");},
                    		"Sustituye" : function() { self.ejecutaImportacion("sustituyeImportacion");},
                    		"Cierra" : function() { self.importaFinalHoldersDialog.dialog("close"); }
                    	}
                    });
				};
				self.ejecutaImportacion = function(evento) {
					$scope.$broadcast(evento, self.listAfiIds(), self.listAfiErrorIds());
				};
				var inicializaEntorno = function() {
					var vhoy = new Date();
					hoy = vhoy.getFullYear() + "_" + vhoy.getMonth() + 1 + "_" + vhoy.getDate();
					$scope.listaChk = [];
					$scope.botones = { visibles: false, importarVisible: true};
					inicializarLoading();
					inicializaCombos();
					inicializaComponestsJQ();
					AliasService.getAlias().success(onSuccessRequestAlias).error(onErrorRequest);
					IsinService.getIsines(onSuccessRequestIsines, onErrorRequest);
					loadDataTableClientSidePagination([]);
					prepareCollapsion();
					$scope.limpiaFiltros();
					$.unblockUI();
				};
				$scope.failCombo = function(mensaje, combo) {
					fErrorTxt("Error al cargar combo " + combo + ": " + mensaje, 2);
				};
				// Botón que limpia las condiciones del filtro
				$scope.limpiaFiltros = function () {
					lMarcar = true;
					$scope.filtro = {
							mercadoNacInt: $scope.mercados[0].value,
							tipoConsulta: $scope.tiposConsulta[0].value,
							nbclientEq: true,
							nbclient1Eq: true,
							nbclient2Eq: true,
							aliasEq: true,
							isinEq: true,
							refClienteEq: true,
							nbookingEq: true,
							errorCodeEq: true,
							nudnicifEq: true
					};
					$scope.btnMarcarTodos = "Marcar Todos";
					$("#fcontratacionA").datepicker("option", "minDate", "");
					$("#fcontratacionDe").datepicker("option", "maxDate", "");
					$('input[type=text]').val('');
					$("#selectedNombreTitular").find("option").remove().end();
					$("#selectedApellido1").find("option").remove().end();
					$("#selectedApellido2").find("option").remove().end();
					$("#selectedNif").find("option").remove().end();
					$("#selectedAlias").find("option").remove().end();
					$("#selectedIsin").find("option").remove().end();
				}; // $scope.LimpiarFiltros
				/** Botón de ejecutar la consulta. */
				$scope.Consultar = function () {
					var errorTxt = validadTitulares();
					if (errorTxt !== null) {
						fErrorTxt(errorTxt, 2);
						return false;
					}
					$scope.botones.visibles = false;
					$scope.registrosTotales = 0;
					cargarDatosConsultaTitulares();
					collapseSearchForm();
					$("#BtRefrescar").css('display', 'block');
					return false;
				}; // $scope.Consultar
				$scope.ImportarFichero = function () {
					$('#cargarFichero').css('display', 'block');
				}; // $scope.ImportarFichero
				// Boton de marcar todos
				$scope.selececionarCheck = function () {
					if (lMarcar) {
						lMarcar = false;
						document.getElementById("MarcarTod").value = "Desmarcar Todos";
						toggleCheckBoxes(true);
					} else {
						lMarcar = true;
						document.getElementById("MarcarTod").value = "Marcar Todos";
						toggleCheckBoxes(false);
					}
				}; // $scope.selececionarCheck
				// Boton de marcar página
				$scope.selececionarCheckPag = function () {
					var valor = alPaginas[pagina];
					toggleCheckBoxesPag(valor);
					if (valor) {
						document.getElementById("MarcarPag").value = "Desmarcar Página";
						alPaginas[pagina] = false;
					} else {
						document.getElementById("MarcarPag").value = "Marcar Página";
						alPaginas[pagina] = true;
					}
				}; // scope.selececionarCheckPag
				$scope.editaTitulares = function (accion) {
					var comunes, titular, desgloseId; 
					comunes = recuperaDatosComunes();
					if(comunes === null)
						return;
					if(accion === "modificar") {
						titular = traduceTitular(comunes);
					}
					else {
						titular = {};
					}
					desgloseId = traduceIdDesglose(comunes);
					$scope.$broadcast("abreEdicion", titular, desgloseId, listaModi, accion);
				}; // $scope.ModificarTitulares
				// Activa la sublinea de los errores
				$scope.muestraErrores = function (fila) {
					console.log("$scope.muestraErrores fila: " + fila);
					var elementoprop = angular.element('img[id=imgError_' + fila + ']');
					console.log("$scope.muestraErrores elementoprop: " + elementoprop);
					var elemento = angular.element('img[id=Precio_' + fila + ']');
					console.log("$scope.muestraErrores elemento: " + elemento);
					if (oTable.fnIsOpen(fila)) {
						console.log("$scope.muestraErrores: cerrando");
						if (openError[fila]) {
							$(elementoprop[0]).attr('src', '/sibbac20/images/add_verde.png');
							$(elemento[0]).attr('src', '/sibbac20/images/add_verde.png');
							$(this).removeClass('open err');
							oTable.fnClose(fila);
							openError[fila] = false;
						}
					} else {
						console.log("$scope.muestraErrores: abriendo");
						var aData = oTable.fnGetData(fila);
						// console.log("aData: ",aData);
						if (aData[0] !== "SIN ERROR") {
							$(elementoprop[0]).attr('src', '/sibbac20/images/supr_verde.png');
							$(elemento[0]).attr('src', '/sibbac20/images/forbidden.png');
							openError[fila] = true;
							$(this).addClass('open err');
							oTable.fnOpen(fila, TablaErrores(oTable, fila), 'details-err');
							// ajustarAltoTabla();
							document.getElementById('div_err_' + fila).scrollIntoView();
						}
					}
				}; // $scope.muestraErrores
				// Activa la sublinea de los Precios.
				$scope.muestraPrecio = function (fila) {
					var elementoprop = angular.element('img[id=Precio_' + fila + ']');
					var elemento = angular.element('img[id=imgError_' + fila + ']');
					if (oTable.fnIsOpen(fila)) {
						if (openPrecio[fila]) {
							$(elementoprop[0]).attr('src', '/sibbac20/images/add_verde.png');
							$(elemento[0]).attr('src', '/sibbac20/images/add_verde.png');
							$(this).removeClass('open pre');
							oTable.fnClose(fila);
							openPrecio[fila] = false;
						}
					} else {
						$(elementoprop[0]).attr('src', '/sibbac20/images/supr_verde.png');
						$(elemento[0]).attr('src', '/sibbac20/images/forbidden.png');
						openPrecio[fila] = true;
						$(this).addClass('open pre');
						oTable.fnOpen(fila, TablaPrecio(fila), 'details-pre');
						document.getElementById('div_pre_' + fila).scrollIntoView();
					}
				}; // $scope.muestraPrecio
				$scope.Refrescar = cargarDatosConsultaTitulares;

					$scope.btnAddNif = function () {
						var valor = $('#nif').val();
						$('#selectedNif').append("<option value=\"" + valor + "\" >" + valor + "</option>");
						$('#nif').val("");
					}; // $scope.btnAddNif

					$scope.btnDelNif = function () {
						$("#selectedNif").find("option:selected").remove().end();
					}; // $scope.btnDelNif

					$scope.btnAddNombreTitular = function () {
						var valor = $('#nombreTitular').val();
						$('#selectedNombreTitular').append("<option value=\"" + valor + "\" >" + valor + "</option>");
						$('#nombreTitular').val("");
					}; // $scope.btnAddNombreTitular

					$scope.btnDelNombreTitular = function () {
						$("#selectedNombreTitular").find("option:selected").remove().end();
					}; // $scope.btnDelNombreTitular

					$scope.btnAddApellido1 = function () {
						var valor = $('#apellido1').val();
						$('#selectedApellido1').append("<option value=\"" + valor + "\" >" + valor + "</option>");
						$('#apellido1').val("");
					}; // $scope.btnAddApellido1

					$scope.btnDelApellido1 = function () {
						$("#selectedApellido1").find("option:selected").remove().end();
					}; // $scope.btnDelApellido1

					$scope.btnAddApellido2 = function () {
						var valor = $('#apellido2').val();
						$('#selectedApellido2').append("<option value=\"" + valor + "\" >" + valor + "</option>");
						$('#apellido2').val("");
					}; // $scope.btnAddApellido2

					$scope.btnDelApellido2 = function () {
						$("#selectedApellido2").find("option:selected").remove().end();
					}; // $scope.btnDelApellido2

					$("#mensajeBorrado").dialog({
						autoOpen : false,
						modal : false,
						width : 500,
						buttons : [ {
							text : " Continuar ",
							icons : {
								primary : "ui-icon-trash",
							},
							class : "ui-button",
							click : function () {
								$(this).dialog("close");
								$scope.borrarSi = true;
								borrarTitulares(oTable, true);
							}
						}, {
							text : " Cancelar ",
							icons : {
								primary : "ui-icon-closethick",
							},
							class : "ui-button",
							click : function () {
								$(this).dialog("close");
								$scope.borrarSi = false;
							}
						} ],

						show : {
							effect : "blind",
							duration : 1000
						},
						hide : {
							effect : "explode",
							duration : 1000
						}
					}); // $("#mensajeBorrado").dialog

					$("#mensajeModificado").dialog({
						autoOpen : false,
						modal : false,
						width : 500,
						buttons : [ {
							text : " Continuar ",
							icons : {
								primary : "ui-icon-trash",
							},
							class : "ui-button",
							click : function () {
								$(this).dialog("close");
								$scope.modificarSi = true;
								// modificarTitulares(oTable, true);
								mandarTitularesModificados(true);
							}
						}, {
							text : " Cancelar ",
							icons : {
								primary : "ui-icon-closethick",
							},
							class : "ui-button",
							click : function () {
								$(this).dialog("close");
								$scope.modificarSi = false;
							}
						} ],

						show : {
							effect : "blind",
							duration : 1000
						},
						hide : {
							effect : "explode",
							duration : 1000
						}
					}); // $("#mensajeModificado").dialog

				/** Procesa un error durante la petición de datos al servidor. */
				function onErrorRequest (data) {
					$.unblockUI();
					fErrorTxt("Ocurrió un error durante la petición de datos.", 1);
				} // onErrorRequest

				/** Carga el autocompletable de alias. */
				function onSuccessRequestAlias (datosAlias) {
					var availableTagsAlias = [];
					var obj;
					angular.forEach(datosAlias.resultados.result_alias, function (val, key) {
						obj = {
								label : val.alias + " - " + val.descripcion,
								value : val.alias
						};
						availableTagsAlias.push(obj);
					});

					$("#btnDelAlias").click(function (event) {
						event.preventDefault();
						$("#selectedAlias").find("option:selected").remove().end();
					});

					angular.element("#textAlias")
					.autocomplete(
							{
								minLength : 0,
								source : availableTagsAlias,
								focus : function (event, ui) {
									return false;
								},
								select : function (event, ui) {
									if (!valueExistInList("#selectedAlias", ui.item.value)) {
										$("#selectedAlias").append(
												"<option value=\"" + ui.item.value + "\" >"
												+ ui.item.label + "</option>");
										$("#textAlias").val("");
										$("#selectedAlias" + "> option").attr('selected', 'selected');
									}
									return false;
								} // select
							}); // angular.element("#textAlias").autocomplete

				} // onSuccessRequestAlias

				/** Carga el autocompletable de isines. */
				function onSuccessRequestIsines (datosIsin) {
					var availableTagsIsin = [];
					var obj;
					angular.forEach(datosIsin, function (val, key) {
						// var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
						obj = {
								label : datosIsin[key].codigo + " - " + datosIsin[key].descripcion,
								value : datosIsin[key].codigo
						};
						availableTagsIsin.push(obj);
					});

					$("#btnDelIsin").click(function (event) {
						event.preventDefault();
						$("#selectedIsin").find("option:selected").remove().end();
					});

					angular.element("#textIsin")
					.autocomplete(
							{
								minLength : 0,
								source : availableTagsIsin,
								focus : function (event, ui) {
									return false;
								},
								select : function (event, ui) {
									if (!valueExistInList("#selectedIsin", ui.item.value)) {
										$("#selectedIsin").append(
												"<option value=\"" + ui.item.value + "\" >"
												+ ui.item.label + "</option>");
										$("#textIsin").val("");
										$("#selectedIsin" + "> option").attr('selected', 'selected');
									}
									return false;
								} // select
							}); // angular.element("#textIsin").autocomplete

					// growl.addInfoMessage("Isines cargados.");
				} // onSuccessRequestIsines

				var excelButtonAction = function(e, dt, node, config) {
          var errorTxt = validadTitulares();
          if (errorTxt !== null) {
            fErrorTxt(errorTxt, 2);
            return;
          }
          complementaFiltro();
          var url = "/sibbac20back/rest/service/SIBBACServiceTitulares/excel?" + $httpParamSerializer($scope.filtro);
          window.open(url);
				};
				/** Crea la tabla de forma normal. */
				function loadDataTableClientSidePagination (requestData) {
					if (oTable !== undefined && oTable !== null) {
						oTable.fnDestroy();
					}
					oTable = $("#tablaConsultaTitulares").dataTable({
						"data" : requestData,
						"dom" : 'B<"clear">ltip',
                        "buttons" : [ {
                            text : 'Excel',
                            action : excelButtonAction }],
						"language" : {
							"url" : "i18n/Spanish.json"
						},
						"columns" : [
							//0
							{ //Editar
								className : 'checkbox',
								targets : 0,
								sClass : "centrar",
								type : "checkbox",
								bSortable : false,
								name : "active",
								width : 50,
								render : function (data, type, row, meta) {
									return '<input type="checkbox" ng-model="listaChk[' + (meta.row)
									+ ']" id="check_' + (meta.row) + '" name="check_'
									+ meta.row + '" class="' + meta.row
									+ '" class="editor-active" bvalue="N" />';
								}
							}, 
							{ //Error
								sClass : "centrar details-err",
								targets : 1,
								bSortable : false,
								width : 50,
								render : function (data, type, row, meta) {
									if (row[1]) {
										return "<img src='/sibbac20/images/add_verde.png' ng-click='muestraErrores("
	                                        + meta.row
	                                        + ")' id='imgError_"
	                                        + meta.row
	                                        + "' style='cursor: pointer; color: #ff0000;' />";
									} else {
										return "SIN ERROR";
	                               }
	                             }
							},
							{ //Booking
								sClass : "centrar",
								width : 300,
								bSortable : false
							},
							{ //Ref. cliente
								//data : 'nureford',
								sClass : "centrar",
								width : 100,
								bSortable : false
							},
							{ //CCV
								sClass : "centrar",
								width : 100,
								bSortable : false
							},
							{ //Sentido (cdtpoper?)
								sClass : "centrar",
								width : 50,
								bSortable : false
							},
							{ //Isin
								sClass : "centrar",
								width : 100,
								bSortable : false
							},
							{ //Titulos ('nutiteje')
								sClass : "centrar",
								width : 100,
								bSortable : false
							},
							{ //P. Ejecucion
								sClass : "centrar nodetails-pre",
								//targets : 8,
								bSortable : false,
								width : 50,
								render : function (data, type, row, meta) {
									if (row[2] !== "") {
										return "<img src='/sibbac20/images/add_verde.png' ng-click='muestraPrecio("
										+ meta.row
										+ ")' id='Precio_"
										+ meta.row
										+ "' style='cursor: pointer; color: #ff0000;' />";
									} else {
										return "SIN REFERENCIA";
									}
								}
							},
							{ //Mercado ('mercadonacint')
								sClass : "centrar",
								width : 50,
								bSortable : false
							},
							{ //Nombre ('nbclient')
								sClass : "centrar",
								width : 250,
								bSortable : false
							}, 
							{ //1erApellido ('nbclient1')
								sClass : "centrar",
								width : 250,
								bSortable : false
							},
							{ //2oApellido ('nbclient2')
								sClass : "centrar",
								width : 250,
								bSortable : false
							},
							{ //T.Documento
								sClass : "centrar",
								width : 75,
								bSortable : false
							},
							{ //Documento ('nudnicif')
								sClass : "centrar",
								width : 100,
								bSortable : false
							},
							{ //T.Persona ('tipopers')
								sClass : "centrar",
								width : 75,
								bSortable : false
							},
							{ //P.Residencia ('paisres')
								sClass : "centrar",
								width : 75,
								bSortable : false
							},
							{ //T.Nacionalidad ('tipnactit'),
								sClass : "centrar",
								width : 75,
								bSortable : false
							},
							{ //Domicilio ('tpdomici'?)
								sClass : "centrar",
								width : 300,
								bSortable : false /*,
                             render : function (data, type, row, meta) {
                               return ((row.tpdomici !== null) ? row.tpdomici : "")
                                      + ((row.nbdomici !== null) ? row.nbdomici : "")
                                      + (row.nudomici !== null) ? row.nudomici : "";
                             }*/
							},
							{ //Poblacion
								sClass : "centrar",
								width : 200,
								bSortable : false
							}, 
							{ //Provincia
								sClass : "centrar",
								width : 200,
								bSortable : false
							}, 
							{ //'codpostal',
								sClass : "centrar",
								width : 75,
								bSortable : false
							}, 
							{ //TipoTitular ('tiptitu')
								sClass : "centrar",
								width : 75,
								bSortable : false
							}, 
							{ //%Participacion ('particip')
								sClass : "align-right",
								"sType" : "numeric",
								width : 75,
								bSortable : false
							},
							{ //Fecha Contratacion ('fechacontratacion')
								sClass : "centrar",
								width : 75,
								bSortable : false
							},
							{ //Id Alias ('alias'?),
								sClass : "centrar",
								width : 75,
								bSortable : false
							}, 
							{ //Nombre Alias ('nombrealias')
								sClass : "centrar",
								width : 200,
								bSortable : false
							} ],
							"fnCreatedRow" : function (nRow, aData, iDataIndex) {
								$compile(nRow)($scope);
							},
							"order" : [ [ 25, "asc" ], [ 1, "asc" ], [ 3, "asc" ], [ 2, "asc" ] ],
							"scrollY" : "480px",
							"scrollX" : "100%",
							"scrollCollapse" : true,
							drawCallback : function () {
								// ajustarAltoTabla();
								processInfo(this.api().page.info());
   	                            if (alPaginas.length !== paginasTotales) {
		                             alPaginas = new Array(paginasTotales);
		                             for (var i = 0; i < alPaginas.length; i++) {
		                               alPaginas[i] = true;
		                             }
		                        } // if
	                            if (ultimaPagina !== pagina) {
	                                 var valor = alPaginas[pagina];
		                             if (valor) {
		                               document.getElementById("MarcarPag").value = "Marcar Página";
		                             } else {
		                               document.getElementById("MarcarPag").value = "Desmarcar Página";
		                             }
	                           } // if
	                           ultimaPagina = pagina;
							} // drawCallback
					}); // $("#tablaConsultaTitulares").dataTable
					$scope.botones.visibles = requestData.length > 0;
				} // loadDataTableClientSidePagination

				/** Limpia las opciones del select especificado. */
				function cleanOptions (selectbox) {
					var i;
					for (i = selectbox.options.length - 1; i >= 0; i--) {
						selectbox.selectedIndex = 0;
					}
				} // cleanOptions

				/** Marca/desmarca todos los registros. */
				function toggleCheckBoxes (b) {
					var l = oTable.fnGetData().length;
					for (var i = 0; i < l; i++) {
						$scope.listaChk[i] = b;
					}
				} // toggleCheckBoxes

				/** Marca/desmarca los registros de la pagina. */
				function toggleCheckBoxesPag (b) {
					var inputs = angular.element('input[type=checkbox]');
					var cb = null;
					for (var i = 0; i < inputs.length; i++) {
						cb = inputs[i];
						var clase = $(cb).attr('class');
						var n = clase.indexOf(" ");
						var fila = clase.substr(0, n);
						$scope.listaChk[fila] = b;
					}
				} // toggleCheckBoxesPag

				/** Funcion en la que captura el filtro y muestra todos titulares que cumplan dicha condición. */
				function cargarDatosConsultaTitulares () {
					inicializarLoading();
					console.log("cargarDatosConsultaTitulares");
					document.getElementById("MarcarTod").value = "Marcar Todos";
					complementaFiltro();
					$scope.listaChk = [];
					loadDataTableFromServer();
				} // cargarDatosConsultaTitulares
				
				function complementaFiltro() {
          var filtro = $scope.filtro;
          filtro.fechaDesde = transformaFechaInv($("#fcontratacionDe").val());
          filtro.fechaHasta = transformaFechaInv($("#fcontratacionA").val());
          filtro.nbclient = obtenerIdsSeleccionados('#selectedNombreTitular');
          filtro.nbclient1 = obtenerIdsSeleccionados('#selectedApellido1');
          filtro.nbclient2 = obtenerIdsSeleccionados('#selectedApellido2');
          filtro.nudnicif = obtenerIdsSeleccionados('#selectedNif');
          filtro.alias = obtenerIdsSeleccionados('#selectedAlias');
          filtro.isin = obtenerIdsSeleccionados('#selectedIsin');
				}

				function onErrorCountRequest (data) {
					growl
					.addErrorMessage("Ocurrió un error de comunicación con el servidor, por favor inténtelo más tarde.");
					$.unblockUI();
				} // onErrorCountRequest

				/** carga la tablita de los errores de un registro. */
				function TablaErrores (Table, nTr) {
					var aData = Table.fnGetData(nTr);
					var resultado = "<div border-width:0 !important;background-color:#dfdfdf; style='float:left;;margin-left: 80px;' id='div_err_"
						+ nTr
						+ "'>"
						+ "<table id='tablaERR"
						+ nTr
						+ "' style=margin-top:5px;'>"
						+ "<tr>"
						+ "<th class='taleft' style='background-color: #000 !important;'>C&oacute;digo</th>"
						+ "<th class='taleft' style='background-color: #000 !important;'>Descripci&oacute;n</th>"
						+ "</tr>";
					// var aObjError = aData[19];
					desplegarErrores(aData, nTr);
					return resultado;
				} // TablaErrores

				function desplegarErrores (aData, nTr) {
					var filtro = {id: aData[1]};
					var tablaErr = "#tablaERR" + nTr;
					var data = new DataBean();
					data.setService('SIBBACServiceTitulares');
					data.setAction('getListErroresTitular');
					data.setFilters(angular.toJson(filtro));
					var request = requestSIBBAC(data);
					request.success(function (json) {
						var rowError;
						console.log(json);
						if (json.resultados != null && json.resultados !== undefined) {
							var aObjError = json.resultados.listaErrores;
							for ( var i in aObjError) {
								var aError = aObjError[i];
								rowError = "<tr><td  width=75>" + 
									aError['codigo'] + "</td>" +
									"<td  width=500>" +
									aError['descripcion'] + "</td></tr>";
								$(tablaErr).append(rowError);
							}
						}
					});
					$(tablaErr).append("</table><div>");
				} // desplegarPrecios

				/** carga la tablita de los precios de un registro (cabecera). */
				function TablaPrecio (nTr) {
					var aData = oTable.fnGetData(nTr);
					var resultado = "<div border-width:0 !important;background-color:#dfdfdf; style='float:left;margin-left: 1136px;' id='div_pre_"
						+ nTr
						+ "'>"
						+ "<table id='tablaPRE"
						+ nTr
						+ "' style=margin-top:5px;'>"
						+ "<tr>"
						+ "<th class='taleft' style='background-color: #000 !important;'>T&iacute;tulos</th>"
						+ "<th class='taleft' style='background-color: #000 !important;'>Precio</th>"
						+ "</tr>";
					desplegarPrecios(aData, nTr);
					return resultado;
				} // TablaPrecio

				function desplegarPrecios (aData, nTr) {
					var filtro = traduceIdDesglose(aData);
					var data = new DataBean();
					var rowPrecios = ""
					var tablaPre = "#tablaPRE" + nTr;
					data.setService('SIBBACServiceTitulares');
					data.setAction('getEjecucionesFromBooking');
					data.setFilters(angular.toJson(filtro));
					var request = requestSIBBAC(data);
					request.success(function (json) {
						console.log(json);
						if (json.resultados != null && json.resultados !== undefined) {
							var item = null;
							var datos = json.resultados.resultados_ejecuciones;
							var arPrecios = [];
							for ( var k in datos) {
								var registro = datos[k];
								var titulos;
								var precio;
								for ( var j in registro) {
									item = registro[j];
									if (j == "nutiteje") {
										titulos = item;
									} else if (j == "imcbmerc") {
										precio = item;
									}
								}
								arPrecios.push({
									titulo : titulos,
									precio : precio
								})
							} // for

							arPrecios.sort(function (a, b) {
								if (a.precio > b.precio) {
									return 1;
								}
								if (a.precio < b.precio) {
									return -1;
								}
								// a must be equal to b
								return 0;
							});
							for (var i = 0; i < arPrecios.length; i++) {
								var rwPrecio = arPrecios[i];
								rowPrecios += "<tr><td>" + rwPrecio.titulo + "</td><td>" + rwPrecio.precio + "</td></tr>";
							}
							$(tablaPre).append(rowPrecios);
						}
					});
					$(tablaPre).append("</table><div>");
				} // desplegarPrecios

				/** Valida las condiciones del filtro. */
				function validadTitulares () {
					var error = null, filtro = $scope.filtro;
					var fContratacionDe = $("#fcontratacionDe").val();
					var fContratacionA = $("#fcontratacionA").val();
					if ((filtro.nbooking && filtro.nbooking.length > 0 && filtro.nbookingEq) || 
							(filtro.refCliente && filtro.refCliente.length > 0 && filtro.refClienteEq)) {
						return null;
					}
					if (filtro.tipoConsulta !== 3 && fContratacionDe.length == 0) {
						error = "Las fecha de contratación desde no puede ir vacia.";
						return error;
					}

					if (!(fContratacionDe.length == 0 || fContratacionA.length == 0)) {
						if (!(validDifFechas(fContratacionDe, fContratacionA))) {
							error = "La diferencia entre la fechas de inicio y fin no puede ser superior a un año."
								return error;
						}
					}

					if (!(fContratacionDe.length == 0) && fContratacionA.length == 0) {
						var fechaDeHoy = new Date();
						var fechaHoyTxt = fechaDeHoy.getDate() + "/" + (fechaDeHoy.getMonth() + 1) + "/"
						+ fechaDeHoy.getFullYear()
						if (!(validDifFechas(fContratacionDe, fechaHoyTxt))) {
							error = "Si no se introduce \"fecha de contración hasta\" la \"fecha de contratación desde\" no puede ser de hace mas de un año."
								+ " Si desea esta \"fecha de contratación desde\" esta obligado a introducir una \"fecha de contratación hasta\" inferior a un año de diferencia.";
							return error;
						}
					}
					return error;
				} // validadTitulares

				// Valida la diferencia de fechas.
				function validDifFechas (fecha1, fecha2) {
					var date1 = fecha1.split('/');
					var date2 = fecha2.split('/')
					var dateAnterior = [];
					dateAnterior[0] = date1[1];
					dateAnterior[1] = date1[0];
					dateAnterior[2] = date1[2];
					var datePosterior = [];
					datePosterior[0] = date2[1];
					datePosterior[1] = date2[0];
					datePosterior[2] = date2[2];
					var date11 = new Date(dateAnterior);
					var date22 = new Date(datePosterior);
					var timeDiff = Math.abs(date22.getTime() - date11.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
					if (diffDays > 366) {
						return false;
					}
					return true;
				}

				// Chequea los errores.
				$scope.CheckErrors = function() {
					var data = new DataBean();
					data.setService('SIBBACServiceTitulares');
					data.setAction('checkAfiErrors');
					var request = requestSIBBAC(data);
					request.success(function (json) {

						console.log(json);
						console.log(json.resultados);
						if (json.resultados != null && json.resultados !== undefined) {
							if(json.resultados.mensaje != null && json.resultados.mensaje !== undefined){
								fErrorTxt(json.resultados.mensaje + "\n", 3);
							}else{
								var fixed = json.resultados.fixedAfis;
								fErrorTxt("Se han movido " + fixed + " registros a la tabla de sin errores \n", 3);
								if(fixed > 0) {
									$scope.Consultar();
								}
							}
						} else {
							fErrorTxt(json.error, 2);
						}

					});
				}

				function cargarFichero (files) {
					console.log('Entra en enviarFichero....');
					var file = files[0];
					console.log('file ..: ' + file)

					if (files && file) {
						var reader = new FileReader();
						var btoaTxt;
						var nombre = file.name;
						reader.onload = function (readerEvt) {
							var binaryString = readerEvt.target.result;
							btoaTxt = btoa(binaryString);
							enviarFichero(btoaTxt, nombre);
						};
						reader.readAsBinaryString(file);
					}
					$('#cargarFichero').css('display', 'none');
					if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
						fErrorTxt('The File APIs are not fully supported in this browser.', 1);
					}

				}

				function enviarFichero (Text, nombre) {
					var filtro = "{\"filename\" : \"" + nombre + "\",";
					filtro += "\"file\" : \"" + Text + "\"";
					filtro += "}";
					var data = new DataBean();
					data.setService('SIBBACServiceTitulares');
					data.setAction('importTitularesExcel');
					data.setFilters(filtro);
					var request = requestSIBBAC(data);
					request.success(function (json) {
						console.log(json);
						console.log(json.resultados);
						if (json.resultados != null && json.resultados !== undefined) {
							var resultado = json.resultados.wellProcessed;
							if (resultado) {
								fErrorTxt("La importación se ha realizado correctamente", 3);
							} else {
								fErrorTxt("La importación se ha realizado NO correctamente", 1);
							}
						} else {
							fErrorTxt(json.error, 2);
						}
					});
				}

				function obtenerIdsSeleccionados (lista) {
					var selected = []
					$(lista + ' option').each(function () {
						selected.push($(this).val());
					});
					return selected;
				}

				/** Procesa un error durante la petición de dartos al servidor. */
				function onErrorRequest (data) {
					$.unblockUI();
					fErrorTxt("Ocurrió un error durante la petición de datos.", 1);
				} // onErrorRequest

				// Botón de eliminar titulares.
				$scope.eliminaTitulares = function () {
					$scope.afiPro = 0;
					$scope.afiErrorPro = 0;
					borrarTitulares(oTable, false);
				};

				// Declara los datos de modificaciones
				function borrarTitulares (oTable, saltaFiltro) {
					var data, l, params;
					console.log("Eliminación de titulares routing");
					listaModi = [];
					data = oTable.fnGetData();
					l = data.length;
					for (var i = 0; i < l; i++) 
						if($scope.listaChk[i]) 
							listaModi.push(traduceIdDesglose(data[i]));
					if (listaModi.length < 1) {
						fErrorTxt("Debe seleccionar al menos un titular.", 2);
						return;
					}
					params =  [{referencias : listaModi.length, titulares: 0}].concat(listaModi);
					TitularesRoutingService.deleteTitular(onSuccessDeleteTitulares, onErrorRequest, 
							params); //si falla en este punto, prueba con angular.toJson(params)
				}

				function onSuccessDeleteTitulares (json) {
					if (json !== null && json !== undefined) {

						if (json.error !== null && json.error === "") {
							fErrorTxt(json.error, 1);
						} else if (json.resultados !== null && json.resultados !== undefined) {
							// agregado por alex 28dic para testear
							if (json.resultados.mensaje !== null && json.resultados.mensaje !== undefined && json.resultados.mensaje != "") {
								console.log("--> Mensaje: ", json.resultados.mensaje);
								fErrorTxt(json.resultados.mensaje, 3);
							}
							// declaramos las variables
							var datosAfiNoProcesados = json.resultados.nAfiNotProcessed;
							var datosAfiErrorNoProcesados = json.resultados.nAfiErrorNotProcessed;
							var datosAfiProcesados = json.resultados.nAfiProcessed;
							var datosAfiErrorProcesados = json.resultados.nAfiErrorProcessed;
							// Agregador por alex el 28dic para poner los datos en el mensaje final al cliente
							$scope.afiPro += datosAfiProcesados;
							$scope.afiErrorPro += datosAfiErrorProcesados;
							// hasta aqui
							if (datosAfiNoProcesados === 0 && datosAfiErrorNoProcesados === 0) {
								console.log("mensaje: se han procesado " + $scope.afiPro + " de AFI y " + $scope.afiErrorPro
										+ " de AFI ERROR, Mensaje: ", json.resultados.mensaje);
								var texto = "Se han borrado " + $scope.afiPro + " titulares sin error y " + $scope.afiErrorPro
								+ " titular con error/sin orden";
								fErrorTxt(texto, 3);
								document.onclick = function (event) {
									// procedemos a recargar la pagina una vez que se haya efectuado el borrado
									var targetElement = event.target;
									if (targetElement.className == "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary") {

										$scope.Consultar(); // en caso de que queremos que se vuelva a cargar el filtro
									}
								};
							} else {
								$scope.afiPro = datosAfiProcesados;
								$scope.afiErrorPro = datosAfiErrorProcesados;
								if (json.resultados.esBloqueante != null && json.resultados.esBloqueante !== undefined) {
									if (json.resultados.esBloqueante) {
										fErrorTxt(json.resultados.mensaje, 2);
										$scope.Consultar();
									} else {
										var error = fErrorTxtRespuesta(json.resultados.mensaje);
									}
								} else {
									var error = fErrorTxtRespuesta(json.resultados.mensaje);
								}

							}
						} else {
							console.log("error: ", json.error, " - resultado: ", json.resultado);
						}
					}
				}

				// Carga el mensaje adecuando en el dialog
				function fErrorTxtRespuesta (msgError) {

					$('#mensajeBorrado').empty();
					$('.ui-dialog-titlebar-close').remove();

					var option = $(".mensajeBorrado").dialog("option");

					$("#mensajeBorrado").dialog(option, "title", "Advertencia");
					$('#mensajeBorrado')
					.append("<div class='dialogMensajeIncon'> <img src='/sibbac20/images/warning.png'/></div>");

					var btnAceptar = $('.ui-dialog-buttonpane').find('button:contains("Aceptar")');

					btnAceptar.width(100);
					btnAceptar.height(25);
					$('#mensajeBorrado').append("<p class='dialogMensajeText'ALIGN=center>" + msgError + "</p>");
					$('#mensajeBorrado').dialog("open");

				}

				function fErrorTxtRespuestaModificar (msgError) {

					$('#mensajeModificado').empty();
					$('.ui-dialog-titlebar-close').remove();

					var option = $(".mensajeCreado").dialog("option");

					$("#mensajeModificado").dialog(option, "title", "Advertencia");
					$('#mensajeModificado')
					.append("<div class='dialogMensajeIncon'> <img src='/sibbac20/images/warning.png'/></div>");

					var btnAceptar = $('.ui-dialog-buttonpane').find('button:contains("Aceptar")');

					btnAceptar.width(100);
					btnAceptar.height(25);
					$('#mensajeModificado').append("<p class='dialogMensajeText'ALIGN=center>" + msgError + "</p>");
					$('#mensajeModificado').dialog("open");

				}

				function fErrorTxtRespuestaCrear (msgError) {

					$('#mensajeCreado').empty();
					$('.ui-dialog-titlebar-close').remove();

					var option = $(".mensajeCreado").dialog("option");
					$("#mensajeCreado").dialog(option, "title", "Advertencia");
					$('#mensajeCreado')
					.append("<div class='dialogMensajeIncon'> <img src='/sibbac20/images/warning.png'/></div>");
					var btnAceptar = $('.ui-dialog-buttonpane').find('button:contains("Aceptar")');

					btnAceptar.width(100);
					btnAceptar.height(25);
					$('#mensajeCreado').append("<p class='dialogMensajeText'ALIGN=center>" + msgError + "</p>");
					$('#mensajeCreado').dialog("open");
				}

				$("#mensajeCreado").dialog({
					autoOpen : false,
					modal : false,
					width : 500,
					buttons : [ {
						text : " Continuar ",
						icons : {
							primary : "ui-icon-trash",
						},
						class : "ui-button",
						click : function () {
							$(this).dialog("close");
							$scope.crearSi = true;
							mandarTitularesCreados(true);
						}
					}, {
						text : " Cancelar ",
						icons : {
							primary : "ui-icon-closethick",
						},
						class : "ui-button",
						click : function () {
							$(this).dialog("close");
							$scope.crearSi = false;
						}
					} ],

					show : {
						effect : "blind",
						duration : 1000
					},
					hide : {
						effect : "explode",
						duration : 1000
					}
				});


				function loadDataTableFromServer () {
					TitularesRoutingService.getTitularesDataTable(function(response){
						$.unblockUI();
						if(response.error) {
							fErrorTxt(response.error, 2);
							return;
						}
						loadDataTableClientSidePagination(response);
					}, function(failResponse){
						$.unblockUI();
						fErrorTxt("Error en la invocacion desde servidor",2);
						$scope.botones.visibles = false;
					}, $scope.filtro);
				} // loadDataTableFromServer


				/** Devuelve información de la tabla. */
				function processInfo (info) {
                    pagina = info.page;
                    paginasTotales = info.pages;
                    return info.start - info.end;
				} // processInfo
				
                /** Comprueba que el valor especificado esté dentro de la lista. */
                function valueExistInList (lista, valor) {
                  var inList = false;
                  $(lista + ' option').each(function (index) {
                    if (this.value == valor) {
                      inList = true;
                      return inList;
                    }
                  });
                  return inList;
                } // valueExistInList				

				/**
				 * Ajusta el alto de una tabla.
				 */
				function ajustarAltoTabla () {
					console.log("ajustando inicio");
					var oSettings = oTable.fnSettings();
					console.log("ajustando 1");
					var scrill = calcDataTableHeight(oTable.fnSettings().oScroll.sY);
					console.log("cuanto: " + scrill);
					oSettings.oScroll.sY = scrill;
					console.log("tamanio antes: " + $('.dataTables_scrollBody').height());
					$('.dataTables_scrollBody').height(scrill);
					console.log("tamanio despues: " + $('.dataTables_scrollBody').height());
				} // ajustarAltoTabla

				var calcDataTableHeight = function (sY) {
					if ($('#tablaEntregaRecepcion').height() > 500) {
						return 480;
					} else {
						return $('#tablaEntregaRecepcion').height() + 20;
					}
				}; // calcDataTableHeight
				$scope.refrescaConsulta = cargarDatosConsultaTitulares;
				$scope.openImportaDialog = function() {
					var l = $scope.listaChk.length, i;
					for(i = 0; i < l; i++) {
						if($scope.listaChk[i]) {
							self.importaFinalHoldersDialog.dialog("open");
							return;
						}
					}
					fErrorTxt("Debe seleccionar al menos un titular");
				}
				if (SecurityService.inicializated) {
					if (!SecurityService.isPermisoAccion($rootScope.SecurityActions.EXPORTAR_EXCEL, $location.path())) {
						$(".DTTT_button.DTTT_button_xls, .DTTT_button.DTTT_button_csv").hide();
					}
				} else {
					$scope.$watch(security.event.initialized, function () {

						if (!SecurityService
								.isPermisoAccion($rootScope.SecurityActions.EXPORTAR_EXCEL, $location.path())) {
							$(".DTTT_button.DTTT_button_xls, .DTTT_button.DTTT_button_csv").hide();
						}

					});
				}
		    	$scope.titularesModificadosSuccess = function(json) {
					console.log(json.resultados);
					if (json.resultados != null && json.resultados !== undefined) {
						var modiConError = json.resultados.nAfiErrorProcessed;
						var modiSinError = json.resultados.nAfiProcessed;
						var dejarErrores = json.resultados.nAfiErrorFixed;
						var noPasarErrores = json.resultados.nAfiNotProcessed;
						if (json.resultados.mensaje != null && json.resultados.mensaje !== undefined
								&& json.resultados.mensaje != "") {
							fErrorTxt(json.resultados.mensaje, 2);
							console.log("esBloqueante: ", json.resultados.esBloqueante);
						} 
						else {
							fErrorTxt("Modificados " + modiConError + " titulares con errores. </br> Modificados "
									+ modiSinError + " titulares sin errores. </br>" + dejarErrores
									+ " titulares dejaron de tener errores.</br>" + noPasarErrores
									+ " titulares no se modificaron porque pasarían a un estado con error", 3);
							if(modiConError > 0 || modiSinError > 0 || dejarErrores > 0) {
								$scope.refrescaConsulta();
							}
						} // else
					} 
					else {
						fErrorTxt(json.error, 2);
					} // else
		    		
		    	};
		    	$scope.titularesCreadosSuccess = function(json) {
					console.log(json.resultados);
					if (json.resultados != null && json.resultados !== undefined) {
						var creadosSinError = json.resultados.afiErrorFixed;
						var creadosConError = json.resultados.afiErrorCreated;
						console.log("mensaje: ", json.resultados.mensaje);
						if (json.resultados.mensaje != null && json.resultados.mensaje !== undefined
								&& json.resultados.mensaje != "") {
							fErrorTxt(json.resultados.mensaje, 2);
						} 
						else {
							fErrorTxt("Creados " + creadosConError + " titulares pdte. revisar. </br> De todos los titulares creados, "
									+ creadosSinError + " titulares sin errores.", 3);
							$scope.refrescaConsulta();
						} // else
					} else {
						fErrorTxt(json.error, 2);
					} // else
		    		
		    	};
				$scope.$on("closeImportacion", function(evento, json) {
					$.unblockUI();
					if(json.accion === "agregacion") {
						$scope.titularesCreadosSuccess(json);
					}
					else {
						$scope.titularesModificadosSuccess(json);
					}
					if(!json.resultados.esBloqueante) {
						self.importaFinalHoldersDialog.dialog("close");
						$scope.Refrescar();
					}
				});
				inicializaEntorno();
				
	} ])
    .controller('ControlTitularesDialogController', ["$scope", "TitularesRoutingService", function($scope, 
    		TitularesRoutingService) {
    	var self = this;
    	var campos = [ "ccv", "nbcliente", "nbcliente1", "nbcliente2", "tpidenti", "nudnicif", "tpsocied",
    		"tpnactit", "cddepais", "cdnactit", "nbdomici", "nbciudad", "nbprovin", "cdpostal", "tptiprep", "particip",
    		"tpdomici", "nudomici"];
    	var muestraFallo = function(error) {
    		fErrorTxt(error, 2);
    	};
		var mandarTitularesModificados = function() {
			var envio = {}, form, data, request, params;
			console.log("Actualización datos titulares routing."); 
			form = $scope.formTitulares;
			campos.forEach(function(campo){
				if(form[campo].$dirty)
					envio[campo] = $scope.titular[campo];
			});
			envio.refCliente = $scope.titular.refCliente;
			params =  [{referencias : self.listaModi.length, titulares: 1}].concat(self.listaModi, [envio]);
			data = new DataBean();
			data.setService('SIBBACServiceTitulares');
			data.setAction('updateTitularesList');
			data.setParams(angular.toJson(params));
			request = requestSIBBAC(data);
			request.success($scope.titularesModificadosSuccess).fail(muestraFallo); // request.success
			$scope.cerrar();
		}; // mandarTitularesModificados

		/**
		 * Almacena los datos de alta.
		 */
		var mandarTitularesCreados =  function(sustituir) {
			var data, request, params, filter;
			console.log("Actualización datos de creacion de titulares routing.");
			params = [{referencias : self.listaModi.length, titulares : self.listaTitulos.length}]
				.concat(self.listaModi, self.listaTitulos);
			filter = { accion : sustituir };
			data = new DataBean();
			data.setService('SIBBACServiceTitulares');
			data.setAction('createTitulares');
			data.setParams(angular.toJson(params));
			data.setFilters(angular.toJson(filter));
			request = requestSIBBAC(data);
			request.success($scope.titularesCreadosSuccess).fail(muestraFallo);
			$scope.cerrar();
		}; // mandarTitularesCreados
		/**
		 * Establece el valor de los campos de dirección en función del pais de residencia seleccionado.
		 */
		var paisResidenciaChange = function () {
			var titular = $scope.titular;
			if(!titular || !titular.cdpost) 
				return;
			titular.cdpost == "011" ? undefined :  '99' + titular.paires;
			titular.tpdomi = undefined;
			titular.nbdomi = undefined; 
			titular.nudomi = undefined;
			titular.ciudad = undefined;
			titular.provin = undefined;
		}; // paisResidenciaChange
		var falloenCargaCombo = function(fallo) {
			console.log("Fallo al cargar un combo: " + fallo);
		};
		var inicializaEntorno = function() {
			var lc = TitularesRoutingService.loadCombo;
			lc("getTpidentiList", "resultados_tpidenti", 
					function(data) { $scope.tiposIdentificacion = data; }, falloenCargaCombo);
			lc("getTpnactitList", "resultados_tpnactit",
					function(data) { $scope.tiposNacionalidad = data; }, falloenCargaCombo);
			lc("getTpsociedList", "resultados_tpsocied",
					function(data) { $scope.tiposSociedad = data; }, falloenCargaCombo);
			lc("getTptiprepList", "resultados_tptiprep",
					function(data) { $scope.tiposPrep = data; }, falloenCargaCombo);
			lc("getTpDomiciList", "resultados_tpdomici",
					function(data) { $scope.tiposVia = data;}, falloenCargaCombo,
					function(item) {
						item.descripcion =  item.nbviapub + " - " + item.cdviapub;
					});
			lc("getNacionalidadList", "resultados_nacionalidad",
					function(data) { $scope.nacionalidades = data;}, falloenCargaCombo, function(item){
						item.descripcion =  item.cdiso2po + " - " + item.nbispais + "(" + item.cdispais + ")"; 
					});
			lc("getPaisResList", "resultados_paisres",
					function(data) { $scope.paisesResidencia = data;}, falloenCargaCombo, function(item) {
						var id = item.id;
						item.descripcion = item.cdiso2po + " - " + item.nbhppais + "(" + id['cdhppais'] + ")";});
		}; 
		var iniciaEdicionMultiple = function() {
			self.listaTitulos = [$scope.titular];
			self.editando = 0;
			$scope.siguienteVisible = true;
			actualizaTitulo();
		};
		var actualizaTitulo = function() {
			$scope.titleTitu = self.tituloAccion +  
				(self.listaTitulos.length == 1 ? " 1 titular" : " "
					+ self.listaTitulos.length + " titulares");
		};
		var abreEdicion = function(event, titular, desgloseId, listaModi, accion) {
			self.listaModi = listaModi;
			self.accion = accion;
			self.titularOrigen = titular;
			$scope.formTitulares.$setPristine();
			$scope.titular = angular.copy(titular);
			$scope.desgloseId = desgloseId;
			$scope.anteriorVisible = false;
			switch(accion) {
			case "modificar":
				self.listaTitulos = undefined;
				$scope.titleTitu = "Modificando titular";
				$scope.siguienteVisible = false;
				$scope.anteriorVisible = false;
				break;
			case "anyadir":
				self.tituloAccion = "Añadiendo"; 
				iniciaEdicionMultiple();
				break;
			default:
				self.tituloAccion = "Sustituyendo";
				iniciaEdicionMultiple();
			}
			$('#formulariotitulares').css('display', 'block');
		};
		$scope.Guardar = function () {
			switch(self.accion) {
				case 'modificar':
					mandarTitularesModificados();
					break;
				case "anyadir":
					mandarTitularesCreados(false);
					break;
				default:
					mandarTitularesCreados(true);
			}
		}; // $scope.Guardar
		$scope.anterior = function() {
			if(self.editando == 0) 
				return;
			self.editando--;
			$scope.titular = self.listaTitulos[self.editando];
			$scope.anteriorVisible = self.editando > 0;
			actualizaTitulo();
		};
		$scope.siguiente = function() {
			if(self.editando == self.listaTitulos.length - 1) {
				$scope.titular = angular.copy(self.titularOrigen);
				self.editando++;
				self.listaTitulos[self.editando] = $scope.titular;
			}
			else {
				self.editando++;
				$scope.titular = self.listaTitulos[self.editando];
			}
			$scope.anteriorVisible = true;
			actualizaTitulo();
		};
		/** Cierra el div de edición de los datos de los titulares y muestra los botones de acciones. */
		$scope.cerrar = function() {
			$('#formulariotitulares').css('display', 'none');
		}; // cerrar
		$scope.$watch("titular.cddepais", paisResidenciaChange);
		$scope.$on("abreEdicion", abreEdicion);
		inicializaEntorno();
    }])
	.directive('sbMuestraBoolColor', [function(){
		return {
			template: "<span ng-show='value' style='color: green;'>{{ positivo }}</span>" +
			"<span ng-show='!value' style='color: red;'>{{ negativo }}</span>",
			scope: { value: "=", positivo: "@", negativo: "@"}
		}
	}])
	.directive('sbMuestraIgual', [function(){
		return {
			template: "<span sb-muestra-bool-color value='value' positivo='IGUAL' negativo='DISTINTO'></span>",
			scope: { value: "="}
		}
	}])
	.directive('sbMuestraIncluidos', [function() {
		return {
			template: "<span sb-muestra-bool-color value='value' positivo='INCLUIDOS' negativo='NO INCLUIDOS'></span>",
			scope: { value: "="}
		}
	}])
	.directive('sbMuestraBoolSigno', [function(){
		return {
			template: "<span ng-show='value'>=</span>" +
			"<span ng-show='!value'>&lt;&gt;</span>",
			scope: { value: "="}
		}
	}]);
