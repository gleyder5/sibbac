'use strict';
sibbac20
    .controller(
                'CrearFacturaController',
                [
                 '$scope',
                 '$compile',
                 'growl',
                 'FacturaSugeridaService',
                 'AliasService',
                 '$document',
                 function ($scope, $compile, growl, FacturaSugeridaService, AliasService, $document) {
                   var idOcultos = [];
                   var availableTags = [];
                   var oTable = undefined;
                   var posicionAlias = -1;
                   $scope.filtro = {
                     fcontratacionA : ""
                   }

                   function showError (data, status, headers, config) {
                     $.unblockUI();
                     growl.addErrorMessage("Error: " + data);
                   }
                   function loadAlias () {
                     AliasService.getAliasFactura(cargarAlias, showError);
                   }

                   function loadDataTable () {
                     oTable = $("#tblCrearFactura").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                       },
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "aoColumns" : [ {
                         "width" : "9%",
                         sClass : "centrar"
                       }, {
                         "width" : "35%"
                       }, {
                         "width" : "15%",
                         type : "date-eu"
                       }, {
                         "width" : "15%",
                         sClass : "monedaR"
                       }, {
                         "width" : "13%",
                         type : "date-eu"
                       }, {
                         "width" : "13%",
                         type : "date-eu"
                       } ],
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                         $compile(nRow)($scope);
                       }
                     });
                   }
                   $document.ready(function () {
                     loadDataTable();
                     loadAlias();
                     $("#ui-datepicker-div").remove();
                     angular.element("#fcontratacionA").datepicker({
                       changeMonth : true,// this option for allowing user to select month
                       changeYear : true
                     // this option for allowing user to select from year range
                     });
                     prepareCollapsion();
                     /* BUSCADOR */
                     ajustarAnchoDivTable();

                   });

                   $scope.limpiarFiltro = function () {
                     angular.element('input[type=text]').val('');
                     angular.element('select').val('');
                     angular.element("#fcontratacionA").val('');
                     $scope.filtro = {
                       fcontratacionA : ""
                     }
                   };

                   $scope.crearFactura = function (event, idBoton) {
                     if (event !== null) {
                       event.preventDefault();
                     }
                     // Crear una factura
                     // Miramos si el alias existe
                     var listaAlias = angular.element("#textAlias").val();
                     posicionAlias = availableTags.indexOf(listaAlias);

                     var fechaHasta = $scope.filtro.fcontratacionA;
// if (fechaHasta === null || fechaHasta === undefined || fechaHasta === "") {
// alert("Debe introducir una fecha hasta la que generar/buscar facturas");
// return false;
// }

                     if ((posicionAlias === "-1") && (listaAlias !== "")) {
                       alert("El alias introducido no existe");
                       return false;
                     } else {
                       collapseSearchForm();
                       var valorSeleccionadoAlias = "";

                       if (listaAlias === null || listaAlias === undefined || listaAlias === "") {
                         valorSeleccionadoAlias = "";
                       } else {
                         posicionAlias = availableTags.indexOf(listaAlias);
                         valorSeleccionadoAlias = idOcultos[posicionAlias];
                       }
                       // console.log( "[valorSeleccionadoAlias=="+valorSeleccionadoAlias+"]" );

                       var filtro = {};
                       if (valorSeleccionadoAlias !== "") {
                         filtro.idAlias = valorSeleccionadoAlias;
                         filtro.fechaHasta = fechaHasta;
                       }

                       if (idBoton === 'buscar') {
                         // El buscar de siempre:
                         // Aquí asignamos el estado

                         inicializarLoading();
                         FacturaSugeridaService.getListForCreate(cargarTabla, showError, filtro);
                       } else {
                         // Inicialmente, "sugerir".
                         if (valorSeleccionadoAlias === "") {
                           alert("Debe introducir un alias");
                         } else {
                           inicializarLoading();
                           FacturaSugeridaService.create(onCreateFactura, showError, filtro);
                         }
                       }
                       return false;
                     }

                   };
                   function onCreateFactura (data, status, headers, config) {
                     $.unblockUI();
                     growl.addSuccessMessage("Factura creada con respuesta: " + data.error);
                     $scope.crearFactura(null, 'buscar');
                   }
                   function cargarAlias (datos) {
                     var item = null;
                     for ( var k in datos) {
                       item = datos[k];
                       idOcultos.push(item.id);
                       availableTags.push(item.nombre.trim());
                     }
                     // código de autocompletar

                     angular.element("#textAlias").autocomplete({
                       source : availableTags
                     });
                     growl.addSuccessMessage("Alias cargados correctamente.");

                   }

                   // funcion para cargar la tabla
                   function cargarTabla (datos) {
                     var cont, eCont, dir, eDir, cif, eCif, chkD;
                     var contador = 0;
                     oTable.fnClearTable();
                     var item = null;
                     var activeC = 0;
                     for ( var k in datos) {
                       item = datos[k];
                       // miro los valores de info alias, y si están a 1 los pinto verdes, a 0 rojos
                       cont = item.nContacto === 1 ? "s" : "n";
                       eCont = item.nContacto === 1 ? "" : "no ";
                       dir = item.nDireccion === 1 ? "s" : "n";
                       eDir = item.nDireccion === 1 ? "" : "no ";
                       cif = item.nCIF === 1 ? "s" : "n";
                       eCif = item.nCIF === 1 ? "" : "no ";
                       if (item.nContacto === 1) {
                         activeC += 1;
                       }
                       if (item.nDireccion === 1) {
                         activeC += 1;
                       }
                       if (item.nCIF === 1) {
                         activeC += 1;
                       }
                       chkD = (activeC === 3) ? "" : "disabled";

                       oTable.fnAddData([
                                         "<input type='checkbox' class='checkTabla' id='checkTabla' name='checkTabla' data='idmarcar" + contador
                                             + "' " + chkD + "/><input type='hidden' name='idmarcar" + contador + "' id='idmarcar" + contador
                                             + "' value='" + item.idFacturaSugerida + "' />&nbsp;<img src='img/" + cont
                                             + "cont.gif' title='Contacto " + eCont + "asignado'></a>&nbsp;<img src='img/" + dir
                                             + "direc.gif' title='Direcci&oacute;n " + eDir + "asignada'></a>&nbsp;<img src='img/" + cif
                                             + "cif.gif' title='CIF " + eCif + "asignado'></a>",
                                         "<span >" + item.cdAlias.trim() + " - " + item.nombreAlias.trim() + "</span>",
                                         "<span>" + transformaFecha(item.fechaCreacion) + "</span>",
                                         "<span>" + $.number(item.importe, 2, ',', '.') + "</span>",
                                         "<span>" + transformaFecha(item.fechaInicio) + "</span>",
                                         "<span>" + transformaFecha(item.fechaFin) + "</span>" ]);
                       activeC = 0;
                       contador++;
                     }
                     // poner botones
                     angular.element('.botonera').empty();
                     angular
                         .element('.botonera')
                         .append(
                                 "<input type='button' class='mybutton' id='seleccionarTodos' value='Seleccionar Todas' ng-click='clickSeleccionarTodos()' />");
                     angular
                         .element('.botonera')
                         .append(
                                 "<input type='button' class='mybutton' id='desmarcarTodos' value='Desmarcar Todas' ng-click='clickDesmarcarTodos()' />");
                     angular.element('.piePagina').empty();
                     angular.element('.piePagina')
                         .append('<input type="button" class="mybutton" id="btnCrearFactura" value="Crear" ng-click="crear()"/>');
                     $compile(angular.element('.botonera'))($scope);
                     $compile(angular.element('.piePagina'))($scope);

                     $.unblockUI();
                   }

                   // Evento para marcar facturas
                   $scope.crear = function () {

                     var suma = 0;
                     var valores = new Array();
                     var los_cboxes = document.getElementsByName('checkTabla');
                     var valor = "";
                     for (var i = 0, j = los_cboxes.length; i < j; i++) {
                       if (los_cboxes[i].checked === true) {
                         var idCampo = angular.element(los_cboxes[i]).attr('data');
                         valor = angular.element('#' + idCampo).val();
                         valores.push(valor);
                         suma++;
                       }
                     }

                     if (suma === 0) {
                       alert('Marque al menos una línea');
                       return false;
                     } else {
                       marcarFacturas(valores);
                     }
                   };

// marcar y desmarcar
                   $scope.clickSeleccionarTodos = function () {
                     var fila = angular.element('.tablaPrueba').find('td input.checkTabla:enabled');
                     fila.prop('checked', true);
                   };

                   $scope.clickDesmarcarTodos = function () {
                     var fila = angular.element('.tablaPrueba').find('td input.checkTabla');
                     fila.prop('checked', false);
                   };
                   function onCheckFacturarHandler (data, status, headers, config) {
                     $.unblockUI();
                     growl.addSuccessMessage("Facturas marcadas correctamente.");
                     $scope.crearFactura(null, '');
                   }
                   // Funcion para marcar facturas
                   function marcarFacturas (valores) {
                     var bucleVal = valores.length;
                     // console.log("entro en el metodo");
                     var params = [];
                     for (var v = 0; v < bucleVal; v++) {
                       params.push({
                         idFacturaSugerida : valores[v]
                       });
                     }
                     inicializarLoading();
                     FacturaSugeridaService.marcarParaFacturar(onCheckFacturarHandler, showError, params);
                   }

                 } ]);
