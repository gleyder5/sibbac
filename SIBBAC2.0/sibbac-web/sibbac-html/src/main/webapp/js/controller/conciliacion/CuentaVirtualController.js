sibbac20.controller('CuentaVirtualController', ['$scope', '$compile', 'MovimientoVirtualService', 'IsinService', 'CuentaLiquidacionService', 'AliasService', '$document', '$compile','ngDialog',
    function ($scope, $compile, MovimientoVirtualService, IsinService, CuentaLiquidacionService, AliasService, $document, $compile, ngDialog) {
        $scope.cuentas = [];
        $scope.filtro = {
                    "isin": "",
                    "cdaliass": "",
                    "cdcodigocuentaliq": "",
                    "fcontratacionDe": "",
                    "fcontratacionA": ""
		}

        var cuentasVirtuales = [];
        $scope.fila = {
        		auditUser : "",
        		auditDate : "",
        		titulos   : "",
        		efectivo  : "",
        		isin      : "",
        		fcontratacion : "",
        		fliquidacion  : ""
        };
        $scope.dialogTitle = "";

        /** ISINES **/
        function loadIsines() {
            IsinService.getIsines(cargarIsines, showError);
        }
        /** CUENTAS **/
        function loadCuentas() {
            CuentaLiquidacionService.getCuentasLiquidacion().success(cargarCuentas).error(showError);
        }
        /** END **/
        /** ALIAS **/
        function loadAlias() {
            AliasService.getAlias(cargarAlias, showError);
        }
        /** END ALIAS **/
        /**
         *
         * @param {type} data
         * @param {type} status
         * @param {type} headers
         * @param {type} config
         * @returns {undefined}
         */
        function showError(data, status, headers, config) {
            $.unblockUI();
            console.log("Error al inicializar datos: " + status);
        }
        function cargarCuentas(data, status, header, config) {
            if (data !== null && data.resultados !== null && data.resultados.cuentasLiquidacion !== null) {
                $scope.cuentas = data.resultados.cuentasLiquidacion;
            }
            $.unblockUI();

        }
        function cargarIsines(datosIsin) {
            var availableTags = [];
            var ponerTag = "";
            $.each(datosIsin, function (key, val) {
                ponerTag = val.codigo + " - " + val.descripcion;
                availableTags.push(ponerTag);
            });
            $("#cdisin").autocomplete({
                source: availableTags
            });
        }

        var oTable = undefined;
        function round(value, decimals) {
            return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
        }
        function consultar(params) {
            inicializarLoading();
            MovimientoVirtualService.getMovimientosEspejo(params, populateDataTable, showError);
        }

        function openPopup(row) {

        	$scope.dialogTitle = "Mostrar Información";
            ngDialog.open({
                template: 'template/conciliacion/cuentasEspejo/mostrarInformacion.html',
                className: 'ngdialog-theme-plain custom-width-480 custom-height-240',
                scope: $scope,
                preCloseCallback: function () {
                	$(row).css("background-color", "");
                	$scope.fila = {
                    		auditUser : "",
                    		auditDate : "",
                    		titulos   : "",
                    		efectivo  : "",
                    		isin      : "",
                    		fcontratacion : "",
                    		fliquidacion  : ""
                    };
                	$scope.dialogTitle = "";
                }
            });
        }

        function populateDataTable(datos) {
            oTable.fnClearTable();

            //
            var SALDO_INICIAL = "Sald.Inicial";
            var SALDO_FINAL = "Sald.Final";
            var descali = "";
            var cdcodigocuentaliq = "";
            var cdalias = "";
            var isin = "";
            var descrIsin = "";
            var sistemaLiq = "";
            var descOperacion = "";
            var cdoperacion = "";
            var signoAnotacion = "";
            var fcontratacion = "";
            var fliquidacion = "";
            var titulos = "";
            var efectivo = "";
            var strTitulos = "";
            var strEfectivo = "";
            var floatTitulos = 0;
            var floatEfectivo = 0;
            var corretaje = "";
            var acumTitulos = 0;
            var acumEfectivo = 0;
            var strAcumTitulos = "";
            var strAcumEfectivo = "";
            var cont = 0;
            var auditUser = "";
            var auditDate = "";
			var esSaldoInicial = false;
			var esSaldoFinal = false;
			var contador = 0;
            cuentasVirtuales = [];
            angular.forEach(datos, function (val, key) {
                descali = (val.descali === null) ? "" : val.descali;
                cdcodigocuentaliq = (val.cdcodigocuentaliq !== null) ? val.cdcodigocuentaliq : "";
                cdalias = (val.cdalias !== null) ? val.cdalias : "";
                isin = (val.isin !== null) ? val.isin : "";
                descrIsin = (val.descrIsin !== null) ? val.descrIsin : "";
                sistemaLiq = (val.sistemaLiq !== null) ? val.sistemaLiq : "";
                cdoperacion = (val.cdoperacion !== null) ? val.cdoperacion.trim() : "";
                descOperacion = (val.descripcionOperacion !== null) ? val.descripcionOperacion : "";
                signoAnotacion = (val.signoAnotacion !== null) ? val.signoAnotacion : "";
                fcontratacion = (val.fcontratacion !== null) ? transformaFecha(val.fcontratacion) : "";
                fliquidacion = (val.fliquidacion !== null) ? transformaFecha(val.fliquidacion) : "";
				esSaldoInicial = (cdoperacion === SALDO_INICIAL);
				esSaldoFinal = (cdoperacion === SALDO_FINAL);


				// Cogemos los titulos
				titulos = (val.titulos !== null) ? val.titulos : "0";

				// Formateamos los titulos (devuelve: [xxx.xxx.]xxx,xxxxx)
                strTitulos = $.number(titulos, 0, ',', '.');

				// Acumulados
				floatTitulos = parseFloat(acumTitulos) + parseFloat(titulos);
				floatTitulos = floatTitulos.toFixed(0);
				//A petición del responsable de BackOffice se elimina esta condicion: 2017-10-23
				/*if(fliquidacion === ""){
                                    acumTitulos = (esSaldoInicial) ? (titulos) : acumTitulos;
                                }else{*/
                                    acumTitulos = (esSaldoInicial) ? (titulos) : floatTitulos;
                //                }
                strAcumTitulos = $.number(acumTitulos, 0, ',', '.');


				// Cogemos el efectivo (devuelve: [xxx.xxx.]xxx,xxxxx)
				efectivo = (val.efectivo !== null) ? val.efectivo : 0;

				strEfectivo = $.number( efectivo, 2, ',', '.');

				// Acumulados
				floatEfectivo = parseFloat(acumEfectivo) + parseFloat(efectivo);
				floatEfectivo = floatEfectivo.toFixed(2);
				//A petición del responsable de BackOffice se elimina esta condicion: 2017-10-23
                                /*if(fliquidacion === ""){
                                    acumEfectivo = (esSaldoInicial) ? (efectivo) : acumEfectivo;
                                }else{*/
                                    acumEfectivo = (esSaldoInicial) ? (efectivo) : floatEfectivo;
                                //}
                strAcumEfectivo = $.number(round(acumEfectivo, 2), 2, ',', '.');
                fliquidacion = (fliquidacion === "" && (!esSaldoInicial && !esSaldoFinal)) ? "Pendiente" : fliquidacion;

		corretaje = (val.corretaje !== null) ? a2digitos(val.corretaje) : "0,00";

		auditUser = (val.auditUser !== null) ? val.auditUser : "";
                auditDate = (val.auditDate !== null) ? transformaFecha(val.auditDate) : "";
                var obj = oTable.fnAddData([
                    cdcodigocuentaliq,
                    cdalias,
                    descali,
                    isin,
                    descrIsin,
                    sistemaLiq,
                    cdoperacion,
                    descOperacion,
                    signoAnotacion,
                    fcontratacion,
                    fliquidacion,
                    strTitulos,
                    strEfectivo,
                    /*corretaje,*/
                    ((!esSaldoInicial && !esSaldoFinal) ? strAcumTitulos : ''),
                    ((!esSaldoInicial && !esSaldoFinal) ? strAcumEfectivo : '')], false);
                var row = oTable.fnSettings().aoData[ obj[0] ].nTr;
                var aData = $("td", row);

                if ($(aData[6]).text().trim() === SALDO_INICIAL) {
                    //Cambia el color del fondo a nivel de fila
                    $(row).css("background-color", "#E5F5F9!important");
                    $(row).css("font-weight", "bold");
                    //cambia el color del fondo a nivel de columna
                    //$('td', row).css('background-color', '#ddeeee');
                } else if ($(aData[6]).text().trim() === "Sald.Final") {
                    $(row).css("background-color", "#D8EFF7!important");
                    $(row).css("border-bottom", "1px solid #C00");
                    $(row).css("font-weight", "bold");
                    //$('td', row).css('background-color', '#ccccee');
                } else if ($(aData[6]).text().trim() === "MAN") {
                	$(row).attr("id",contador);

                	cuentasVirtuales[contador] = {
                    		auditUser : auditUser,
                    		auditDate : auditDate,
                    		titulos   : titulos,
                    		efectivo  : efectivo,
                    		isin      : isin,
                    		fcontratacion : fcontratacion,
                    		fliquidacion  : fliquidacion
                    };
                	contador++;
                    $(row).click(function () {
                    	$(row).css("background-color", "#FE2E2E!important");

                    	var id = $(row).attr("id");
                    	var cuentaVirtual = cuentasVirtuales[id];

                    	$scope.fila = cuentaVirtual;
                    	openPopup(row);
                    });
                }
            });
            oTable.fnDraw();
            $.unblockUI();


        }
// ///////////////////////////////////////////////
// ////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
        function initialize() {

            //verificarFechas([['fcontratacionD', 'fcontratacionA']]);
            cargarFechaActualizacionMovimientoAlias();
            loadIsines();
            loadCuentas();
            loadAlias();

            getFechaPrevia();
            //var fechas = ['fcontratacionD', 'fcontratacionA'];
            //loadpicker(fechas);
            // obtenerDatos();
            $("#ui-datepicker-div").remove();
			$('input#fcontratacionD').datepicker({
			    onClose: function( selectedDate ) {
			      $( "#fcontratacionA" ).datepicker( "option", "minDate", selectedDate );
			    } // onClose
			  });

			  $('input#fcontratacionA').datepicker({
			    onClose: function( selectedDate ) {
			      $( "#fcontratacionD" ).datepicker( "option", "maxDate", selectedDate );
			    } // onClose
			  });
			  /*
			  console.log("tenemos: ",document.getElementById("fcontratacionD").value);
              //ALEX 5 feb, de esta manera logramos cargar los limites de fechas
              $( "#fcontratacionA" ).datepicker( "option", "minDate", document.getElementById("fcontratacionD").value);
              */
        }
        $document.ready(function () {
            initialize();
            prepareCollapsion();
            loadDataTable();
            $('#limpiar').click(function (event) {
            	//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
                $( "#fcontratacionA" ).datepicker( "option", "minDate", "");
                $( "#fcontratacionD" ).datepicker( "option", "maxDate", "");

                //
                event.preventDefault();
                $('input[type=text]').val('');
                $('select').val('0');
            });

        });
        function loadDataTable() {
            oTable = $("#tblCuentaVirtual").dataTable({
                "dom": 'T<"clear">lfrtip',
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "scrollX": "auto",
                "pageLength": 20,
                "lengthMenu": [20, 25, 50, 75, 100],
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "order": [],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    }],
                "aoColumns": [
                    {sClass: "centrar"},
                    {sClass: "monedaR", type: "formatted-num"},
                    {sClass: "aleft"},
                    {sClass: "aleft"},
                    {sClass: "aleft"},
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "align-left"},
                    {sClass: "centrar"},
                    {sClass: "centrar", type: 'date-eu'},
                    {sClass: "centrar", type: "date-eu"},
                    {sClass: "monedaR", type: "formatted-num"},
                    {sClass: "monedaR", type: "formatted-num"},
                    {sClass: "monedaR", type: "formatted-num"},
                    {sClass: "monedaR", type: "formatted-num"}
                ],
                "fnCreatedRow" : function(row, data, index){
                    $compile(row)($scope);
                }
            });
        }
        $scope.submitForm = function (event) {
            obtenerDatos();
            collapseSearchForm();
            seguimientoBusqueda();
        }

        function obtenerDatos() {
        	$scope.filtro.fcontratacionD = $('#fcontratacionD').val();
          	$scope.filtro.fcontratacionA = $('#fcontratacionA').val();
			var params = {
                "service": "SIBBACServiceMovimientoVirtual",
                "action": "getMovimientosVirtuales",
                "filters": {
                    "isin": getIsin(),
                    "cdaliass": getAliasId(),
                    "cdcodigocuentaliq": ($("#selectCuenta option:selected").val() !== "") ?
                            $("#selectCuenta option:selected").text().trim() : "",
                    "fcontratacionDe": transformaFechaInv($scope.filtro.fcontratacionD),
                    "fcontratacionA": transformaFechaInv($scope.filtro.fcontratacionA)
                }
            };
            consultar(params);
        }


        function cargarAlias(datos) {
            var availableTagsAlias = [];
            var item = null;

            for (var k in datos) {
                item = datos[k];
                ponerTag = item.alias + " - " + item.descripcion;
                availableTagsAlias.push(ponerTag);

            }
            // código de autocompletar

            $("#alias").autocomplete({
                source: availableTagsAlias
            });
            // fin de código de autocompletar
            //console.log("tenemos: ",document.getElementById("fcontratacionD").value);
            $( "#fcontratacionA" ).datepicker( "option", "minDate", document.getElementById("fcontratacionD").value);
        }

        function getFechaPrevia() {
            var params = new DataBean();
            params.setService('SIBBACServiceMovimientoVirtual');
            params.setAction('getFechaPreviaLiquidacion');
            var request = requestSIBBAC(params);
            request.success(function (json) {
                var datos = undefined;

                if (json === undefined || json.resultados === undefined
                        || json.resultados.result_fecha_previa_laboral === undefined) {
                    datos = {};
                } else {
                    datos = json.resultados.result_fecha_previa_laboral;
                    $scope.filtro.fcontratacionD = (transformaFecha(datos[0]));

                }

            });

        }
        function getIsin() {
            var isinCompleto = $('#cdisin').val();
            var guion = isinCompleto.indexOf("-");

            return (guion > 0) ? isinCompleto.substring(0, guion) : isinCompleto;
        }

        function getAliasId() {
            var alias = $('#alias').val();
            var guion = alias.indexOf("-");
            return (guion > 0) ? alias.substring(0, guion) : alias;
        }

        function getAliasDes() {
            var aliasd = $('#alias').val();
            var guion = aliasd.indexOf("-");
            return (guion > 0) ? aliasd.substring(guion + 1) : aliasd;
        }
        function seguimientoBusqueda() {
            $('.mensajeBusqueda').empty();
            var cadenaFiltros = "";

            if ($scope.filtro.fcontratacionD !== "") {
                cadenaFiltros += " fecha Desde: " + $scope.filtro.fcontratacionD;
            }
            if ($scope.filtro.fcontratacionA !== "") {
                cadenaFiltros += " fecha Hasta: " + $scope.filtro.fcontratacionA;
            }
            if ($('#cdisin').val() !== "" && $('#cdisin').val() !== undefined) {
                cadenaFiltros += " isin: " + $('#cdisin').val();
            }
            if ($('#alias').val() !== "" && $('#alias').val() !== undefined) {
                cadenaFiltros += " alias: " + $('#alias').val();
            }
            if ($('#selectCuenta').val() !== "" && $("#selectCuenta option:selected").val() !== undefined) {
                if ($("#selectCuenta option:selected").val() === "0") {
                    cadenaFiltros += " Cuenta: todas ";
                }
                else {
                    cadenaFiltros += " Cuenta: " + $("#selectCuenta option:selected").text();
                }
            }
            $('.mensajeBusqueda').append(cadenaFiltros);
        }

    }]);

