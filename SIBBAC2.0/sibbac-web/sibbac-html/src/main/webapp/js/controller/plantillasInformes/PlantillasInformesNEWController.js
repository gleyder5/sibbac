'use strict';
sibbac20
    .controller(
                'PlantillasInformesNEWController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'PlantillasInformesNEWService',
                 '$compile',
                 'SecurityService',
                 '$location',
                 'Logger',
                 'commonHelper',
                 'DirectivasService',
                 'CONSTANTES',
                 'camposDinamicosHelper',
                 '$rootScope',
                 '$filter',
                 function ($scope, $document, growl, PlantillasInformesNEWService, $compile, SecurityService, $location,
                           Logger, commonHelper, DirectivasService, CONSTANTES, camposDinamicosHelper, $rootScope, $filter) {

                	 SecurityService.redirectToLoginIfNotLoggedIn();

                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   /***************************************************************************************************
                     * ** INICIALIZACION DE DATOS ***
                     **************************************************************************************************/
                   $scope.showingFilter = true;
                   
                   var labelFiltro = {
                       clase : "Clase",
                       plantilla : "Plantilla",
                       idioma : "Idioma",
                       mercado : "Mercado",
                       camposDetalle : "Campos Detalle",
                   };

                   // Variables y listas general
                   $scope.checked = true;
                   $scope.filtro = {};
                   $scope.listClases = [];
                   $scope.listClasesFiltro = [];
                   $scope.listPlantillas = [];
                   $scope.listIdiomas = [];
                   $scope.listMercados = [];
                   $scope.listCamposDetalle = [];
                   $scope.listCamposDetalleFiltro = [];
                   $scope.listTotalizar = [];
                   $scope.listRelleno = [];
                   $scope.listTipoRelleno = [];
                   $scope.activeAlert = false;
                   $scope.mensajeAlert = "";
                   $scope.borrarCampoAgrup = false;

                   $scope.listPlantillasInformes = [];
                   $scope.plantillasInformesSeleccionadas = [];
                   $scope.plantillasInformesSeleccionadasBorrar = [];

                   $scope.modal = {
                     titulo : "",
                     showCrear : false,
                     showModificar : false,
                     showFilters : false,
                     showGuardar : false
                   };

                   $scope.modalEditar = {
                     titulo : ""
                   };

                   $scope.ocultarAlert = function () {
                     if (!$scope.borrarCampoAgrup) {
                       $scope.mensajeAlert = "";
                     } else {
                       $scope.borrarCampoAgrup = false;
                     }
                   };

                   // Reset objeto plantilla
                   $scope.resetPlantilla = function () {
                     $scope.plantilla = {
                       idInforme : "",
                       clase: "",
                       nbdescription : "",
                       nbtxtlibre : "",
                       nbidioma : $scope.listIdiomas? $scope.listIdiomas[0]:{key: "", value: ""},
                       totalizar : $scope.listTotalizar? $scope.listTotalizar[1]:{key: "", value: ""},
                       relleno : $scope.listRelleno? $scope.listRelleno[1]:{key: "", value: ""},
                       mercado : {key: "", value: ""},
                       camposDetalle : "",
                       listaCamposDetalle : [],
                       listaCamposAgrupacion : [],
                       listaCamposOrdenacion : [],
                       listaCamposCabecera : [],
                       listaCamposSeparacion : [],
                       listaCamposAgrupacionExcel : [],
                       listaFiltrosDinamicos : [],
                       selected : false,
                       auditUser : $rootScope.userName
                     };
                     $scope.listCamposDetalle = [];
                     $scope.listMercadosInformes = [{key: "TODOS", value: "TODOS"}];
                     $scope.activeAlert = false;
                     $scope.mensajeAlert = "";
                     $scope.checked = true;
                     $scope.form.idClase = "";
                   };

                   $scope.form = {};
                   $scope.resetForms = function () {
                     $scope.form = {
                       campoDetalleFilter : "",
                       camposDetalleFilter : "",
                       campoDetallePlantilla : "",
                       camposDetallePlantilla : "",
                       campoAgrupacionPlantilla : "",
                       camposAgrupacionPlantilla : "",
                       campoOrdenacionPlantilla : "",
                       camposOrdenacionPlantilla : "",
                       campoSeparacionPlantilla : "",
                       camposSeparacionPlantilla : "",
                       campoAgrupacionExcel : "",
                       camposAgrupacionExcel : "",
                       campoCabeceraPlantilla : "",
                       camposCabeceraPlantilla : "",
                     }
                   };

                   $scope.resetForms();
                   $scope.resetPlantilla();

                   $scope.resetButtonFilters = function () {
                     $scope.btnsFilter = {
                    	textClase : "IGUAL",
                        btnClase : "=",
                        colorClase : {
                          'color' : 'green'
                        },
                       textPlantilla : "IGUAL",
                       btnPlantilla : "=",
                       colorPlantilla : {
                         'color' : 'green'
                       },
                       textIdioma : "IGUAL",
                       btnIdioma : "=",
                       colorIdioma : {
                         'color' : 'green'
                       },
                       textMercado : "IGUAL",
                       btnMercado : "=",
                       colorMercado : {
                         'color' : 'green'
                       },
                       textCamposDetalle : "INCLUIDOS",
                       btnCamposDetalle : "=",
                       colorCamposDetalle : {
                         'color' : 'green'
                       },
                       textTotalizar : "IGUAL",
                       btnTotalizar : "=",
                       colorTotalizar : {
                         'color' : 'green'
                       }
                     };
                   };

                   $scope.resetButtonFilters();
                   
                   $scope.capitalizeFirstLetter = function (string) {
                	    return string.charAt(0).toUpperCase() + string.slice(1);
                	};
                   
                   $scope.getValorDescriptivo = function (filtro, valor) {
                	   if(filtro == "camposDetalle"){
                		   var valCamposDetalle = "";
                		   for(var i=0;i<valor.length;i++){
                			   if(i != 0){
                				   valCamposDetalle +=  " | " + valor[i].value;
                			   }else{
                				   valCamposDetalle +=  valor[i].value;
                			   }
                		   }
                		   return valCamposDetalle;
                	   }else{
                		   return valor;
                	   }
                	   
                   };
                   
                   $scope.getFollowSearch = function () {
   	             	var texto = "";

   	             	for ( var key in $scope.filtro) {
   	             		if ($scope.filtro[key] != null && $scope.filtro[key] != "") {

   	             			var operador = "";
   	             			var keyOperator = "not" + $scope.capitalizeFirstLetter(key);
   	             			if ($scope.filtro[keyOperator] == false) {
   	             				operador = "=";
   	             			} else {
   	             				operador = "<>";
   	             			}

   	             			var valorDescriptivo = $scope.getValorDescriptivo(key, $scope.filtro[key]);

   	             			if (texto == "") {
   	             				if(null!=valorDescriptivo.value) {
   	             					texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo.value;
   	             				} else {
   	             					if(null!=valorDescriptivo.nombre){
   	             						texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo.nombre;
   	             					}else{
   	             						texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo;
   	             					}
   	             				}
   	             			} else {
   	             				if(null!=valorDescriptivo.value) {
   	             					texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo.value;
   	             				} else {
   	             					if(null!=valorDescriptivo.nombre){
   	             						texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo.nombre;
   	             					}else{
   	             						texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo;
   	             					}
   	             				}
   	             			}

   	             		}
   	             	}
   	             	$scope.followSearch = texto;
   	             	return texto;
   	             }
                   
                   $scope.limpiarFiltrosDinamicos = function(){
                	    for(var i = 0; i<$scope.plantilla.listaFiltrosDinamicos.length; i++){
                	    	for(var j = 0; j<$scope.plantilla.listaFiltrosDinamicos[i].length; j++){
                	    		$scope.plantilla.listaFiltrosDinamicos[i][j].listaSeleccSelectDTO = [];
                	    		$scope.plantilla.listaFiltrosDinamicos[i][j].valorCampo = "";
                	    		$scope.plantilla.listaFiltrosDinamicos[i][j].valorCampoHasta = "";
                	    	}
                	    }
                   };
                   

                   /***************************************************************************************************
                     * ** DEFINICION TABLA ***
                     **************************************************************************************************/

                   $scope.oTable = $("#datosPlantillasInformes").dataTable({
                	   "dom" : 'T<"clear">lfrtip',
                	   "tableTools" : {
                		   "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                		   "aButtons" : [ "copy", {
                			   "sExtends" : "csv",
                			   "sFileName" : "Listado_Plantillas_Informes_" + hoy + ".csv",
                			   "mColumns" : [ 1, 2, 3, 4, 5, 6 ]
                		   }, {
                			   "sExtends" : "xls",
                			   "sFileName" : "Listado_Plantillas_Informes_" + hoy + ".xls",
                			   "mColumns" : [ 1, 2, 3, 4, 5, 6 ]
                		   }, {
                			   "sExtends" : "pdf",
                			   "sPdfOrientation" : "landscape",
                			   "sTitle" : " ",
                			   "sPdfSize" : "A3",
                			   "sPdfMessage" : "Listado Final Holders",
                			   "sFileName" : "Listado_Plantillas_Informes_" + hoy + ".pdf",
                			   "mColumns" : [ 1, 2, 3, 4, 5, 6 ]
                		   }, "print" ]
                	   },
                	   "aoColumns" : [ {
                		   sClass : "centrar",
                		   bSortable : false,
                		   width : "7%"
                	   }, {
                		   sClass : "centrar",
                		   width : "14%"
                	   }, {
                		   sClass : "centrar",
                		   width : "14%"
                	   }, {
                		   sClass : "centrar",
                		   width : "14%"
                	   },{
                		   sClass : "centrar",
                		   width : "9%"
                	   }, {
                		   sClass : "centrar",
                		   width : "9%"
                	   }, {
                		   sClass : "centrar",
                		   width : "19%"
                	   }, {
                		   sClass : "centrar",
                		   width : "14%",
                		   sType : "date-eu"
                	   } ],
                	   "fnCreatedRow" : function (nRow, aData, iDataIndex) {

                		   $compile(nRow)($scope);
                	   },

                	   "scrollY" : "480px",
                	   "scrollX" : "100%",
                	   "scrollCollapse" : true,
                	   "language" : {
                		   "url" : "i18n/Spanish.json"
                	   }
                   });

                   $scope.seleccionarElemento = function (row) {
                     if ($scope.listPlantillasInformes[row].selected) {
                       $scope.listPlantillasInformes[row].selected = false;
                       for (var i = 0; i < $scope.plantillasInformesSeleccionadas.length; i++) {
                         if ($scope.plantillasInformesSeleccionadas[i].idInforme === $scope.listPlantillasInformes[row].idInforme) {
                           $scope.plantillasInformesSeleccionadas.splice(i, 1);
                           $scope.plantillasInformesSeleccionadasBorrar.splice(i, 1);
                         }
                       }
                     } else {
                       $scope.listPlantillasInformes[row].selected = true;
                       $scope.plantillasInformesSeleccionadas.push($scope.listPlantillasInformes[row]);
                       $scope.plantillasInformesSeleccionadasBorrar.push($scope.listPlantillasInformes[row].idInforme);

                     }
                   };

                   /***************************************************************************************************
                     * ** FUNCIONES GENERALES ***
                     **************************************************************************************************/

                   var pupulateAutocomplete = function (input, availableTags) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {

                         var option = {
                           key : ui.item.key,
                           value : ui.item.value
                         };

                         switch (input) {
                           case "#filtro_campoDetalle":
                             var existe = false;
                             angular.forEach($scope.filtro.camposDetalle, function (campo) {
                               if (campo.value === option.value) {
                                 existe = true;
                               }
                             });
                             $scope.form.campoDetalleFilter = "";
                             if (!existe) {
                               $scope.filtro.camposDetalle.push(option);
                             }
                             break;
                           default:
                             break;
                         }

                         $scope.safeApply();

                         return false;
                       }
                     });
                   };
                   
                   $scope.pupulateAutocompleteDinamic = function (input, availableTags, index, index2) {
                	   $(input).autocomplete({
                           minLength : 0,
                           source : availableTags,
                           focus : function (event, ui) {
                             return false;
                           },
                           select : function (event, ui) {

                             var option = {
                               key : ui.item.key,
                               value : ui.item.value
                             };
                             
                             switch (input) {
	                             case '#' + $scope.plantilla.listaFiltrosDinamicos[index][index2].identificador:
	                            	 	var existe = false;
		                             	angular.forEach($scope.plantilla.listaFiltrosDinamicos[index][index2].listaSeleccSelectDTO, function (campo) {
		                             		if (campo.value === option.value.trim()) {
		                             			existe = true;
		                             		}
		                             	});
		                             	$scope.plantilla.listaFiltrosDinamicos[index][index2].valor = "";
		                             	if (!existe) {
		                             		$scope.plantilla.listaFiltrosDinamicos[index][index2].listaSeleccSelectDTO.push(option);
		                             	}
	                             default: break;
                             }
                             
                             $scope.safeApply();

                             return false;
                           }
                       });
                    };
                    
                    $scope.operarLista = function(valor, elemento) {
         			   switch (valor) {
	         			   case 'vaciar':
	         				   elemento.listaSeleccSelectDTO = [];
	         				   break;
	         			   case 'subir':
	         				   var listLength = elemento.listaSeleccSelectDTO.length;
	         				   for (var i = 0; i < listLength; i++) {
	                               if (elemento.listaSeleccSelectDTO[i].value === elemento.valorCampo.value && i > 0) {
	                            	   var elementList = elemento.listaSeleccSelectDTO[i];
		                               elemento.listaSeleccSelectDTO[i] = elemento.listaSeleccSelectDTO[i - 1];
		                               elemento.listaSeleccSelectDTO[i - 1] = elementList;
	                               }
	                           }
	         				   break;
	         			   case 'bajar':
	                           var listLength = elemento.listaSeleccSelectDTO.length;
	                           for (var i = listLength - 1; i >= 0; i--) {
	                             if (elemento.listaSeleccSelectDTO[i].value === elemento.valorCampo.value && i < (listLength - 1)) {
	                               var elementList = elemento.listaSeleccSelectDTO[i];
	                               elemento.listaSeleccSelectDTO[i] = elemento.listaSeleccSelectDTO[i + 1];
	                               elemento.listaSeleccSelectDTO[i + 1] = elementList;
	                             }
	                           }
	         				   break;
	         			   case 'eliminar':
	                           for (var i = 0; i < elemento.listaSeleccSelectDTO.length; i++) {
	                        	   if (elemento.listaSeleccSelectDTO[i].value === elemento.valorCampo.value) {
	                        		   elemento.listaSeleccSelectDTO.splice(i, 1);
	                        	   }
	                           }
	         				   break;
	         			   case 'agregar':
	         				   var option = {key : elemento.valor, value :elemento.valor};
	                           var existe = false;
	                           angular.forEach(elemento.listaSeleccSelectDTO, function (campo) {
	                             if (campo.value === elemento.valor) {
	                            	 existe = true;
	                             }
	                           });
	                           if (!existe) {
	                        	   elemento.listaSeleccSelectDTO.push(option);
	                           }
	                           elemento.valor = "";
	         				   break;
     			   			default:
     			   			   break;
     			   		}
     		   		}   
                   

                   $scope.invertirValores = function (option) {

                     switch (option) {

                     case "clase":
                         if (!$scope.filtro.notClase) {
                           $scope.filtro.notClase = true;
                           $scope.btnsFilter.textClase = "DISTINTO";
                           $scope.btnsFilter.btnClase = "<>";
                           $scope.btnsFilter.colorClase = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notClase = false;
                           $scope.btnsFilter.textClase = "IGUAL";
                           $scope.btnsFilter.btnClase = "=";
                           $scope.btnsFilter.colorClase = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "plantilla":
                         if (!$scope.filtro.notPlantilla) {
                           $scope.filtro.notPlantilla = true;
                           $scope.btnsFilter.textPlantilla = "DISTINTO";
                           $scope.btnsFilter.btnPlantilla = "<>";
                           $scope.btnsFilter.colorPlantilla = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notPlantilla = false;
                           $scope.btnsFilter.textPlantilla = "IGUAL";
                           $scope.btnsFilter.btnPlantilla = "=";
                           $scope.btnsFilter.colorPlantilla = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "idioma":
                         if (!$scope.filtro.notIdioma) {
                           $scope.filtro.notIdioma = true;
                           $scope.btnsFilter.textIdioma = "DISTINTO";
                           $scope.btnsFilter.btnIdioma = "<>";
                           $scope.btnsFilter.colorIdioma = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notIdioma = false;
                           $scope.btnsFilter.textIdioma = "IGUAL";
                           $scope.btnsFilter.btnIdioma = "=";
                           $scope.btnsFilter.colorIdioma = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "mercado":
                           if (!$scope.filtro.notMercado) {
                             $scope.filtro.notMercado = true;
                             $scope.btnsFilter.textMercado = "DISTINTO";
                             $scope.btnsFilter.btnMercado = "<>";
                             $scope.btnsFilter.colorMercado = {
                               'color' : 'red'
                             };
                           } else {
                             $scope.filtro.notMercado = false;
                             $scope.btnsFilter.textMercado = "IGUAL";
                             $scope.btnsFilter.btnMercado = "=";
                             $scope.btnsFilter.colorMercado = {
                               'color' : 'green'
                             };
                           }
                           break;
                       case "camposDetalle":
                         if (!$scope.filtro.notCamposDetalle) {
                           $scope.filtro.notCamposDetalle = true;
                           $scope.btnsFilter.textCamposDetalle = "NO INCLUIDOS";
                           $scope.btnsFilter.btnCamposDetalle = "<>";
                           $scope.btnsFilter.colorCamposDetalle = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notCamposDetalle = false;
                           $scope.btnsFilter.textCamposDetalle = "INCLUIDOS";
                           $scope.btnsFilter.btnCamposDetalle = "=";
                           $scope.btnsFilter.colorCamposDetalle = {
                             'color' : 'green'
                           };
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   /***************************************************************************************************
                     * ** CARGA DE DATOS INICIAL ***
                     **************************************************************************************************/

                   // Carga de los combos Totalizar e Idiomas
                   $scope.cargarEstaticos = function () {
                	   if($rootScope.isAuthenticated()){
	                     PlantillasInformesNEWService.cargaSelectsEstaticos(function (data) {
	                       $scope.listTotalizar = data.resultados["listaTotalizar"];
	                       $scope.listIdiomas = data.resultados["listaIdiomas"];
	                       //$scope.listMercados = data.resultados["listaMercados"];
	                       $scope.listRelleno = data.resultados["listaRelleno"];
	                       $scope.listTipoRelleno = data.resultados["listaTipoRelleno"];
	                     }, function (error) {
	                       fErrorTxt("Se produjo un error en la carga de combos estaticos.", 1);
	                     });
                	   }
                   }

                   // Carga del combo Clases
                   $scope.cargaClases = function () {
                	   if($rootScope.isAuthenticated()){
	                     PlantillasInformesNEWService.cargarClases(function (data) {
	                       $scope.listClasesFiltro = data.resultados["listClases"];
	                       $scope.listClases = data.resultados["listClases"];
	                     }, function (error) {
	                       fErrorTxt("Se produjo un error en la carga del combo de clases.", 1);
	                     });
                	   }
                   }

                   $scope.cargaPlantillas = function () {
                	   if($rootScope.isAuthenticated()){
	                     PlantillasInformesNEWService.cargaPlantillas(function (data) {
	                       $scope.listPlantillas = data.resultados["listaPlantillas"];
	                     }, function (error) {
	                       fErrorTxt("Se produjo un error en la carga del combo de plantillas.", 1);
	                     });
                	   }
                   }

                   // Carga del combo campo detalle
                   $scope.cargarCamposDetalle = function () {
                   	 
                   	 $scope.listCamposDetalle = [];
                     PlantillasInformesNEWService.cargaCamposDetalle(function (data) {
                       $scope.listCamposDetalle = data.resultados["listInformesCampos"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Campos Detalle.", 1);
                     }, {
              		   "clase" : $scope.plantilla.clase,
              		   "mercado" : $scope.plantilla.mercado
              	     });
                   }
                   
                   // Reset del filtro
                   $scope.resetFiltro = function () {
                  	 /** Inicio - Componente filtros dinamicos. */
                  	 camposDinamicosHelper.vaciarFiltrosComp($scope);  
                  	 /** Fin - Componente filtros dinamicos. */
                     $scope.filtro = {
                       clase : "",
                       notClase : false,
                       plantilla : "",
                       notPlantilla : false,
                       idioma : {key: "", value: ""},
                       notIdioma : false,
                       mercado : {key: "", value: ""},
                       notMercado : false,
                       camposDetalle : [],
                       notCamposDetalle : false
                     };
                     
                     $scope.listMercados = [];
                     $scope.listCamposDetalleFiltro = [];
                     $scope.cargaPlantillas();
                     $scope.resetButtonFilters();
                   };
                   
                   $scope.resetFiltro();
                   
                   // Carga del combo campo detalle cuando abrimos la modificacion
                   $scope.cargarCamposDetalleModificar = function () {
                   	 
//                   	 $scope.listMercadosInformes = [{key: "TODOS", value: "TODOS"}, {key: $scope.plantilla.clase.mercado, value: $scope.plantilla.clase.mercado}];
//                   	 $scope.plantilla.mercado = $scope.listMercadosInformes[1];
                   	 
                   	 $scope.listCamposDetalle = [];
                     PlantillasInformesNEWService.cargaCamposDetalle(function (data) {
                       $scope.listCamposDetalle = data.resultados["listInformesCampos"];
                       for (var i=0;i<$scope.listCamposDetalle.length;i++) {
                    	   for (var j=0;j<$scope.plantilla.listaCamposDetalle.length;j++) {
                    		   if ($scope.listCamposDetalle[i].value ===  $scope.plantilla.listaCamposDetalle[j].nombreOriginal && $scope.plantilla.listaCamposDetalle[j].renombre != "") {
                    			   $scope.listCamposDetalle[i].value = $scope.plantilla.listaCamposDetalle[j].renombre;
                    		   }
                    	   }
                       }
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Campos Detalle.", 1);
                     }, {
              		   "clase" : $scope.plantilla.clase,
              		   "mercado" : $scope.plantilla.mercado
              	     });
                   }
                   
                   // Carga del combo campo detalle del filtro
                   $scope.cargarCamposDetalleFiltro = function () {
                   	 $scope.filtro.camposDetalle = [];
                     PlantillasInformesNEWService.cargaCamposDetalleFiltro(function (data) {
                    	 $scope.listCamposDetalleFiltro = data.resultados["listInformesCampos"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Campos Detalle.", 1);
                     }, {
              		   "clase" : $scope.filtro.clase,
              		   "mercado" : $scope.filtro.mercado
              	     });
                   }

                   $scope.consultar = function() {
                	   $scope.showingFilter = false;
                	   $scope.getPlantillasInformes();
                   }

                   $scope.getPlantillasInformes = function () {
                	   
                     inicializarLoading();
                     $scope.plantillasInformesSeleccionadasBorrar = [];
                     $scope.plantillasInformesSeleccionadas = [];
                     
                     PlantillasInformesNEWService.cargaPlantillasInformes(
                          function (data) {

                            $scope.listPlantillasInformes = data.resultados["listaPlantillasInformes"];

                            // borra el contenido del body de la tabla
                            var tbl = $("#datosPlantillasInformes > tbody");
                            $(tbl).html("");
                            $scope.oTable.fnClearTable();

                            for (var i = 0; i < $scope.listPlantillasInformes.length; i++) {

                              var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listPlantillasInformes['
                                          + i
                                          + '].selected" ng-click="seleccionarElemento('
                                          + i + ');"/>';
                              var totalizarTexto = "Si";
                              if ($scope.listPlantillasInformes[i].totalizar === "N") {
                                totalizarTexto = "No";
                              }

                              var camposDetalle = $scope.listPlantillasInformes[i].camposDetalle;
                              if ($scope.listPlantillasInformes[i].camposDetalle.length > 30) {
                                camposDetalle = camposDetalle.substring(0, 30);
                                camposDetalle += "...";
                              }
                              
                              var plantillaList = [
                                                   check,
                                                   $scope.listPlantillasInformes[i].nbdescription,
                                                   $scope.listPlantillasInformes[i].nbClase,
                                                   $scope.listPlantillasInformes[i].mercado,
                                                   $scope.listPlantillasInformes[i].filtrosAdicionales,
                                                   $scope.listPlantillasInformes[i].planificada,
                                                   $scope.listPlantillasInformes[i].modUsu,
                                                   $filter('date')($scope.listPlantillasInformes[i].modFechaHora, "dd/MM/yyyy")];

                              $scope.oTable.fnAddData(plantillaList, false);

                            }
                            $scope.oTable.fnDraw();
                            $.unblockUI();
                            if ($scope.listIdiomas.length == 0) {
                              $scope.cargarEstaticos();
                              $scope.cargaPlantillas();
                              $scope.cargaClases();
                            }
                          },
                          function (error) {
                            $.unblockUI();
                            fErrorTxt(
                                      "Se produjo un error en la carga del listado de plantillas informes.",
                                      1);
                          }, $scope.filtro);
                   };

//                   $scope.getPlantillasInformes();
                   $scope.cargarEstaticos();
                   $scope.cargaPlantillas();
                   $scope.cargaClases();
                   
                   /***************************************************************************************************
                     * ** ACCIONES MODALES ***
                     **************************************************************************************************/

                   // Abrir modal creacion informes
                   $scope.abrirCrearPlantillaInforme = function () {
                     $scope.modal.showCrear = true;
                     $scope.modal.showModificar = false;
                     $scope.modal.showFilters = false;
                     $scope.modal.titulo = "Crear Plantilla Informe";
                     $scope.resetPlantilla();
                     angular.element('#formularios').modal({
                       backdrop : 'static'
                     });
                     $(".modal-backdrop").remove();
                     $(".mensajeBusquedaPlantillaInforme").html("");
                   };
                   

                   // Abrir modal modificar informes
                   $scope.abrirModificarPlantillaInforme = function () {
                	 
                     $scope.modal.showFilters = false;
                     $scope.modal.showCrear = false;
                     $scope.modal.showModificar = true;
                     
                     $scope.modal.titulo = "Modificar Plantilla Informe";
                     if ($scope.plantillasInformesSeleccionadas.length > 1
                         || $scope.plantillasInformesSeleccionadas.length == 0) {
                       if ($scope.plantillasInformesSeleccionadas.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de plantillas de informe", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de plantillas de informe.", 2)
                       }
                     } else {
                       $scope.resetPlantilla();
                       inicializarLoading();
                       PlantillasInformesNEWService.cargaPlantillaInforme(function (data) {
                        	$scope.plantilla = data.resultados.plantillaInforme;
                        	$scope.cargarCamposDetalleModificar();
                        	 

                            if ($scope.plantilla.listaCamposAgrupacion.length > 0) {
                          	   $scope.checked = true;
                            } else {
                            	$scope.checked = false;
                            }
                       
                       		if ($scope.plantilla.rellenoCampos=='S') {
                    	   		$scope.plantilla.relleno = 'SI';
                       		} else {
                    	   		$scope.plantilla.relleno = 'NO';
                       		}
                       		
                     	   if (null != $scope.plantilla.listaCamposAgrupacion) {
                    		   for (var i = 0; i < $scope.plantilla.listaCamposAgrupacion.length; i++) {
                    			   for (var j = 0; j < $scope.plantilla.listaCamposDetalle.length; j++) {
                        			   if ($scope.plantilla.listaCamposAgrupacion[i].value === $scope.plantilla.listaCamposDetalle[j].nombreOriginal && $scope.plantilla.listaCamposDetalle[j].renombre != "") {
                        					   $scope.plantilla.listaCamposAgrupacion[i].value = $scope.plantilla.listaCamposDetalle[j].renombre;
                        			   } 
                    			   }
                    		   }
                    	   }
                    	   if (null != $scope.plantilla.listaCamposOrdenacion) {
                    		   for (var i = 0; i < $scope.plantilla.listaCamposOrdenacion.length; i++) {
                    			   for (var j = 0; j < $scope.plantilla.listaCamposDetalle.length; j++) {
                        			   if ($scope.plantilla.listaCamposOrdenacion[i].value === $scope.plantilla.listaCamposDetalle[j].nombreOriginal && $scope.plantilla.listaCamposDetalle[j].renombre != "") {
                        				   $scope.plantilla.listaCamposOrdenacion[i].value = $scope.plantilla.listaCamposDetalle[j].renombre;
                        			   } 
                    			   }
                    		   }
                    	   }
                    	   if (null != $scope.plantilla.listaCamposSeparacion) {
                    		   for (var i = 0; i < $scope.plantilla.listaCamposSeparacion.length; i++) {
                    			   for (var j = 0; j < $scope.plantilla.listaCamposDetalle.length; j++) {
                        			   if ($scope.plantilla.listaCamposSeparacion[i].value === $scope.plantilla.listaCamposDetalle[j].nombreOriginal && $scope.plantilla.listaCamposDetalle[j].renombre != "") {
                        				   $scope.plantilla.listaCamposSeparacion[i].value = $scope.plantilla.listaCamposDetalle[j].renombre;
                        			   } 
                    			   }
                    		   }
                    	   }
                    	   if (null != $scope.plantilla.listaCamposAgrupacionExcel) {
                    		   for (var i = 0; i < $scope.plantilla.listaCamposAgrupacionExcel.length; i++) {
                    			   for (var j = 0; j < $scope.plantilla.listaCamposDetalle.length; j++) {
                        			   if ($scope.plantilla.listaCamposAgrupacionExcel[i].value === $scope.plantilla.listaCamposDetalle[j].nombreOriginal && $scope.plantilla.listaCamposDetalle[j].renombre != "") {
                        				   $scope.plantilla.listaCamposAgrupacionExcel[i].value = $scope.plantilla.listaCamposDetalle[j].renombre;
                        			   } 
                    			   }
                    		   }
                    	   }
                    	   if (null != $scope.plantilla.listaCamposCabecera) {
                    		   for (var i = 0; i < $scope.plantilla.listaCamposCabecera.length; i++) {
                    			   for (var j = 0; j < $scope.plantilla.listaCamposDetalle.length; j++) {
                        			   if ($scope.plantilla.listaCamposCabecera[i].value === $scope.plantilla.listaCamposDetalle[j].nombreOriginal && $scope.plantilla.listaCamposDetalle[j].renombre != "") {
                        				   $scope.plantilla.listaCamposCabecera[i].value = $scope.plantilla.listaCamposDetalle[j].renombre;
                        			   } 
                    			   }
                    		   }
                    	   }
                       		
							$scope.plantilla.listaFiltrosDinamicos = angular.copy($scope.plantilla.listComponenteDirectivaDTO);
							$scope.listMercadosInformes = [{key: "TODOS", value: "TODOS"}, {key: $scope.plantilla.clase.mercado, value: $scope.plantilla.clase.mercado}];
							
							//Seleccionamos la CLASE que viene de la clase seleccionada
							for (var i = 0; i < $scope.listClases.length; i++) {
								if ($scope.listClases[i].nombre===$scope.plantilla.clase.nombre) {
									$scope.plantilla.clase=$scope.listClases[i];
								}
							}
							
							//Seleccionamos el TOTALIZAR que viene de la clase seleccionada
							for (var i = 0; i < $scope.listTotalizar.length; i++) {
								if ($scope.listTotalizar[i].key===$scope.plantilla.totalizar) {
									$scope.plantilla.totalizar=$scope.listTotalizar[i];
								}
							}
							
							//Seleccionamos el IDIOMA que viene de la clase seleccionada
							for (var i = 0; i < $scope.listIdiomas.length; i++) {
								if ($scope.listIdiomas[i].value===$scope.plantilla.nbidioma) {
									$scope.plantilla.nbidioma=$scope.listIdiomas[i];
								}
							}
							
							//Seleccionamos el RELLENO DE CAMPOS que viene de la clase seleccionada
							for (var i = 0; i < $scope.listRelleno.length; i++) {
								if ($scope.listRelleno[i].value===$scope.plantilla.relleno) {
									$scope.plantilla.relleno=$scope.listRelleno[i];
								}
							}
							
							//Seleccionamos el MERCADO que viene de la clase seleccionada
							for (var i = 0; i < $scope.listMercadosInformes.length; i++) {
								if ($scope.listMercadosInformes[i].value===$scope.plantilla.mercado) {
									$scope.plantilla.mercado=$scope.listMercadosInformes[i];
								}
							}
							
                             angular.element('#formularios').modal({backdrop : 'static'});
                             $(".modal-backdrop").remove();
                             $scope.mostrarFiltrosDinaCargados();
                             
                      	   $.unblockUI();
                       }, function (e) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al recuperar los datos de la plantilla de informe.", 1);
                       }, {idInforme : $scope.plantillasInformesSeleccionadas[0].idInforme});
                     }
                   };

                   $scope.procesarCampoDetalle = function () {
                	   var existe = false;
                	   angular.forEach($scope.listCamposDetalle, function (campo) {
                		   if (campo.value === $scope.form.campoDetallePlantilla) {
                			   $scope.form.campoDetallePlantilla = campo;
                			   angular.forEach($scope.plantilla.listaCamposDetalle, function (campo) {
                				   if (campo.value === $scope.form.campoDetallePlantilla.value) {
                					   existe = true;
                				   }
                			   });
                			   if (!existe) {
                				   $scope.plantilla.listaCamposDetalle.push($scope.form.campoDetallePlantilla);
                				   if ($scope.form.campoDetallePlantilla.description === "N" && $scope.checked) {
                					   $scope.plantilla.listaCamposAgrupacion.push($scope.form.campoDetallePlantilla);
                					   $scope.plantilla.listaCamposOrdenacion.push($scope.form.campoDetallePlantilla);
                				   }
                			   }
                			   $scope.form.campoDetallePlantilla = "";
                		   }
                	   });
                   };
                   
                   $scope.procesarCampoDetalleFiltro = function () {
                	   var existe = false;
                	   angular.forEach($scope.listCamposDetalleFiltro, function (campo) {
                		   if (campo.value === $scope.form.campoDetalleFilter) {
                			   $scope.form.campoDetalleFilter = campo;
                			   angular.forEach($scope.filtro.camposDetalle, function (campo) {
                				   if (campo.value === $scope.form.campoDetalleFilter.value) {
                					   existe = true;
                				   }
                			   });
                			   if (!existe) {
                				   $scope.filtro.camposDetalle.push($scope.form.campoDetalleFilter);
                			   }
                			   $scope.form.campoDetalleFilter = "";
                		   }
                	   });
                   };

                   $scope.procesarCampoAgrup = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposAgrupacion, function (campo) {
                       if (campo.value === $scope.form.campoAgrupacionPlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.plantilla.listaCamposAgrupacion.push($scope.form.campoAgrupacionPlantilla);
                       $scope.plantilla.listaCamposOrdenacion.push($scope.form.campoAgrupacionPlantilla);
                     }
                     $scope.form.campoAgrupacionPlantilla = "";
                   };

                   $scope.procesarCampoOrden = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposOrdenacion, function (campo) {
                       if (campo.value === $scope.form.campoOrdenacionPlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.plantilla.listaCamposOrdenacion.push($scope.form.campoOrdenacionPlantilla);
                     }
                     $scope.form.campoOrdenacionPlantilla = "";
                   };

                   $scope.procesarCampoCabecera = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposCabecera, function (campo) {
                       if (campo.value === $scope.form.campoCabeceraPlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.plantilla.listaCamposCabecera.push($scope.form.campoCabeceraPlantilla);
                     }
                     $scope.form.campoCabeceraPlantilla = "";
                   };

                   $scope.procesarCampoSep = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposSeparacion, function (campo) {
                       if (campo.value === $scope.form.campoSeparacionPlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.plantilla.listaCamposSeparacion.push($scope.form.campoSeparacionPlantilla);
                     }
                     $scope.form.campoSeparacionPlantilla = "";
                   };
                   
                   $scope.procesarCampoAgrupacionExcel = function () {
                       var existe = false;
                       angular.forEach($scope.plantilla.listaCamposAgrupacionExcel, function (campo) {
                         if (campo.value === $scope.form.campoAgrupacionExcel.value) {
                           existe = true;
                         }
                       });
                       if (!existe) {
                         $scope.plantilla.listaCamposAgrupacionExcel.push($scope.form.campoAgrupacionExcel);
                       }
                       $scope.form.campoAgrupacionExcel = "";
                     };
                   
                   $scope.subirElementoTabla = function (tabla) {
                     var elemento = {};
                     switch (tabla) {
                       case "detalle":
                         for (var i = 0; i < $scope.plantilla.listaCamposDetalle.length; i++) {
                           if ($scope.plantilla.listaCamposDetalle[i].value === $scope.form.camposDetallePlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposDetalle[i];
                             $scope.plantilla.listaCamposDetalle[i] = $scope.plantilla.listaCamposDetalle[i - 1];
                             $scope.plantilla.listaCamposDetalle[i - 1] = elemento;
                           }
                         }
                         break;
                       case "agrupacion":
                         for (var i = 0; i < $scope.plantilla.listaCamposAgrupacion.length; i++) {
                           if ($scope.plantilla.listaCamposAgrupacion[i].value === $scope.form.camposAgrupacionPlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposAgrupacion[i];
                             $scope.plantilla.listaCamposAgrupacion[i] = $scope.plantilla.listaCamposAgrupacion[i - 1];
                             $scope.plantilla.listaCamposAgrupacion[i - 1] = elemento;
                           }
                         }
                         break;
                       case "ordenacion":
                         for (var i = 0; i < $scope.plantilla.listaCamposOrdenacion.length; i++) {
                           if ($scope.plantilla.listaCamposOrdenacion[i].value === $scope.form.camposOrdenacionPlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposOrdenacion[i];
                             $scope.plantilla.listaCamposOrdenacion[i] = $scope.plantilla.listaCamposOrdenacion[i - 1];
                             $scope.plantilla.listaCamposOrdenacion[i - 1] = elemento;
                           }
                         }
                         break;
                       case "separacion":
                         for (var i = 0; i < $scope.plantilla.listaCamposSeparacion.length; i++) {
                           if ($scope.plantilla.listaCamposSeparacion[i].value === $scope.form.camposSeparacionPlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposSeparacion[i];
                             $scope.plantilla.listaCamposSeparacion[i] = $scope.plantilla.listaCamposSeparacion[i - 1];
                             $scope.plantilla.listaCamposSeparacion[i - 1] = elemento;
                           }
                         }
                         break;
                       case "agrupacionExcel":
                           for (var i = 0; i < $scope.plantilla.listaCamposAgrupacionExcel.length; i++) {
                             if ($scope.plantilla.listaCamposAgrupacionExcel[i].value === $scope.form.camposAgrupacionExcel.value
                                 && i > 0) {
                               elemento = $scope.plantilla.listaCamposAgrupacionExcel[i];
                               $scope.plantilla.listaCamposAgrupacionExcel[i] = $scope.plantilla.listaCamposAgrupacionExcel[i - 1];
                               $scope.plantilla.listaCamposAgrupacionExcel[i - 1] = elemento;
                             }
                           }
                           break;
                       case "cabecera":
                         for (var i = 0; i < $scope.plantilla.listaCamposCabecera.length; i++) {
                           if ($scope.plantilla.listaCamposCabecera[i].value === $scope.form.camposCabeceraPlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposCabecera[i];
                             $scope.plantilla.listaCamposCabecera[i] = $scope.plantilla.listaCamposCabecera[i - 1];
                             $scope.plantilla.listaCamposCabecera[i - 1] = elemento;
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.bajarElementoTabla = function (tabla) {
                     var elemento = {};
                     switch (tabla) {
                       case "detalle":
                         var listLength = $scope.plantilla.listaCamposDetalle.length;
                         for (var i = $scope.plantilla.listaCamposDetalle.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposDetalle[i].value === $scope.form.camposDetallePlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposDetalle[i];
                             $scope.plantilla.listaCamposDetalle[i] = $scope.plantilla.listaCamposDetalle[i + 1];
                             $scope.plantilla.listaCamposDetalle[i + 1] = elemento;
                           }
                         }

                         break;
                       case "agrupacion":
                         var listLength = $scope.plantilla.listaCamposAgrupacion.length;
                         for (var i = $scope.plantilla.listaCamposAgrupacion.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposAgrupacion[i].value === $scope.form.camposAgrupacionPlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposAgrupacion[i];
                             $scope.plantilla.listaCamposAgrupacion[i] = $scope.plantilla.listaCamposAgrupacion[i + 1];
                             $scope.plantilla.listaCamposAgrupacion[i + 1] = elemento;
                           }
                         }
                         break;
                       case "ordenacion":
                         var listLength = $scope.plantilla.listaCamposOrdenacion.length;
                         for (var i = $scope.plantilla.listaCamposOrdenacion.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposOrdenacion[i].value === $scope.form.camposOrdenacionPlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposOrdenacion[i];
                             $scope.plantilla.listaCamposOrdenacion[i] = $scope.plantilla.listaCamposOrdenacion[i + 1];
                             $scope.plantilla.listaCamposOrdenacion[i + 1] = elemento;
                           }
                         }
                         break;
                       case "separacion":
                         var listLength = $scope.plantilla.listaCamposSeparacion.length;
                         for (var i = $scope.plantilla.listaCamposSeparacion.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposSeparacion[i].value === $scope.form.camposSeparacionPlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposSeparacion[i];
                             $scope.plantilla.listaCamposSeparacion[i] = $scope.plantilla.listaCamposSeparacion[i + 1];
                             $scope.plantilla.listaCamposSeparacion[i + 1] = elemento;
                           }
                         }
                         break;
                       case "agrupacionExcel":
                           var listLength = $scope.plantilla.listaCamposAgrupacionExcel.length;
                           for (var i = $scope.plantilla.listaCamposAgrupacionExcel.length - 1; i >= 0; i--) {
                             if ($scope.plantilla.listaCamposAgrupacionExcel[i].value === $scope.form.camposAgrupacionExcel.value
                                 && i < (listLength - 1)) {
                               elemento = $scope.plantilla.listaCamposAgrupacionExcel[i];
                               $scope.plantilla.listaCamposAgrupacionExcel[i] = $scope.plantilla.listaCamposAgrupacionExcel[i + 1];
                               $scope.plantilla.listaCamposAgrupacionExcel[i + 1] = elemento;
                             }
                           }
                           break;
                       case "cabecera":
                         var listLength = $scope.plantilla.listaCamposCabecera.length;
                         for (var i = $scope.plantilla.listaCamposCabecera.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposCabecera[i].value === $scope.form.camposCabeceraPlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposCabecera[i];
                             $scope.plantilla.listaCamposCabecera[i] = $scope.plantilla.listaCamposCabecera[i + 1];
                             $scope.plantilla.listaCamposCabecera[i + 1] = elemento;
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   // Abrir modal edicion campo detalle
                   $scope.abrirEditarDetalle = function () {
                	   var seleccion = $scope.form.camposDetallePlantilla.value;
                	   if (seleccion != null && seleccion != "") {
                		   $scope.form.camposDetallePlantilla.renombre = seleccion;
                		   $scope.modalEditar.titulo = "Editar Campo detalle";
                		   angular.element('#editar').modal({
                			   backdrop : 'static'
                		   });
                		   $(".modal-backdrop").remove();
                	   }
                   };
                   
                   $scope.guardarEdicionCampoDetalle = function () {
                	   if (null != $scope.plantilla.listaCamposAgrupacion) {
                		   for (var i = 0; i < $scope.plantilla.listaCamposAgrupacion.length; i++) {
                			   if ($scope.plantilla.listaCamposAgrupacion[i].value === $scope.form.camposDetallePlantilla.nombreOriginal) {
                				   $scope.plantilla.listaCamposAgrupacion[i].value = $scope.form.camposDetallePlantilla.renombre;
                			   }
                		   }
                	   }
                	   if (null != $scope.plantilla.listaCamposOrdenacion) {
                		   for (var i = 0; i < $scope.plantilla.listaCamposOrdenacion.length; i++) {
                			   if ($scope.plantilla.listaCamposOrdenacion[i].value === $scope.form.camposDetallePlantilla.nombreOriginal) {
                				   $scope.plantilla.listaCamposOrdenacion[i].value = $scope.form.camposDetallePlantilla.renombre;
                			   }
                		   }
                	   }
                	   if (null != $scope.plantilla.listaCamposSeparacion) {
                		   for (var i = 0; i < $scope.plantilla.listaCamposSeparacion.length; i++) {
                			   if ($scope.plantilla.listaCamposSeparacion[i].value === $scope.form.camposDetallePlantilla.nombreOriginal) {
                				   $scope.plantilla.listaCamposSeparacion[i].value = $scope.form.camposDetallePlantilla.renombre;
                			   }
                		   }
                	   }
                	   if (null != $scope.plantilla.listaCamposAgrupacionExcel) {
                		   for (var i = 0; i < $scope.plantilla.listaCamposAgrupacionExcel.length; i++) {
                			   if ($scope.plantilla.listaCamposAgrupacionExcel[i].value === $scope.form.camposDetallePlantilla.nombreOriginal) {
                				   $scope.plantilla.listaCamposAgrupacionExcel[i].value = $scope.form.camposDetallePlantilla.renombre;
                			   }
                		   }
                	   }
                	   if (null != $scope.plantilla.listaCamposCabecera) {
                		   for (var i = 0; i < $scope.plantilla.listaCamposCabecera.length; i++) {
                			   if ($scope.plantilla.listaCamposCabecera[i].value === $scope.form.camposDetallePlantilla.nombreOriginal) {
                				   $scope.plantilla.listaCamposCabecera[i].value = $scope.form.camposDetallePlantilla.renombre;
                			   }
                		   }
                	   }
                       for (var i=0;i<$scope.listCamposDetalle.length;i++) {
                    	   for (var j=0;j<$scope.plantilla.listaCamposDetalle.length;j++) {
                    		   if ($scope.listCamposDetalle[i].value ===  $scope.plantilla.listaCamposDetalle[j].nombreOriginal && $scope.plantilla.listaCamposDetalle[j].renombre != "") {
                    			   $scope.listCamposDetalle[i].value = $scope.plantilla.listaCamposDetalle[j].renombre;
                    		   }
                    	   }
                       }
                	   $scope.form.camposDetallePlantilla.value = $scope.form.camposDetallePlantilla.renombre;
                	   angular.element('#editar').modal("hide");
                   };

                   $scope.cancelarEdicionCampoDetalle = function () {
                	   angular.element('#editar').modal("hide");
                   };
                   
                   $scope.eliminarElementoTabla = function (tabla) {
                     switch (tabla) {
                       case "detalle":

                         $scope.form.camposAgrupacionPlantilla = {
                           value : $scope.form.camposDetallePlantilla.value
                         };
                         $scope.eliminarElementoTabla("agrupacion");
                         $scope.form.camposOrdenacionPlantilla = {
                           value : $scope.form.camposDetallePlantilla.value
                         };
                         $scope.eliminarElementoTabla("ordenacion");
                         
                         $scope.form.camposAgrupacionExcel = {
                                 value : $scope.form.camposDetallePlantilla.value
                               };
                         $scope.eliminarElementoTabla("agrupacionExcel");

                         for (var i = 0; i < $scope.plantilla.listaCamposDetalle.length; i++) {
                           if ($scope.plantilla.listaCamposDetalle[i].value === $scope.form.camposDetallePlantilla.value) {
                             $scope.plantilla.listaCamposDetalle.splice(i, 1);
                           }
                         }
                         break;
                       case "agrupacion":
                         if ($scope.form.camposAgrupacionPlantilla.description !== "N") {
                           for (var i = 0; i < $scope.plantilla.listaCamposAgrupacion.length; i++) {
                             if ($scope.plantilla.listaCamposAgrupacion[i].value === $scope.form.camposAgrupacionPlantilla.value) {
                               $scope.plantilla.listaCamposAgrupacion.splice(i, 1);
                             }
                           }
                         } else {
                           $scope.mensajeAlert = "El campo que desea eliminar, no totaliza. No se puede eliminar de los campos de agrupacion";
                           $scope.borrarCampoAgrup = true;
                         }
                         break;
                       case "ordenacion":
                         $scope.form.camposSeparacionPlantilla = {
                           value : $scope.form.camposOrdenacionPlantilla.value
                         };
                         $scope.eliminarElementoTabla("separacion");
                         for (var i = 0; i < $scope.plantilla.listaCamposOrdenacion.length; i++) {
                           if ($scope.plantilla.listaCamposOrdenacion[i].value === $scope.form.camposOrdenacionPlantilla.value) {
                             $scope.plantilla.listaCamposOrdenacion.splice(i, 1);
                           }
                         }
                         break;
                       case "separacion":
                         $scope.form.camposCabeceraPlantilla = {
                           value : $scope.form.camposSeparacionPlantilla.value
                         };
                         $scope.eliminarElementoTabla("cabecera");
                         for (var i = 0; i < $scope.plantilla.listaCamposSeparacion.length; i++) {
                           if ($scope.plantilla.listaCamposSeparacion[i].value === $scope.form.camposSeparacionPlantilla.value) {
                             $scope.plantilla.listaCamposSeparacion.splice(i, 1);
                           }
                         }
                         break;
                       case "agrupacionExcel":
                           $scope.form.camposAgrupacionExcel = {
                               value : $scope.form.camposAgrupacionExcel.value
                             };
                           for (var i = 0; i < $scope.plantilla.listaCamposAgrupacionExcel.length; i++) {
                             if ($scope.plantilla.listaCamposAgrupacionExcel[i].value === $scope.form.camposAgrupacionExcel.value) {
                               $scope.plantilla.listaCamposAgrupacionExcel.splice(i, 1);
                             }
                           }
                           break;
                       case "cabecera":
                         for (var i = 0; i < $scope.plantilla.listaCamposCabecera.length; i++) {
                           if ($scope.plantilla.listaCamposCabecera[i].value === $scope.form.camposCabeceraPlantilla.value) {
                             $scope.plantilla.listaCamposCabecera.splice(i, 1);
                           }
                         }
                         break;
                       case "detalleFiltro":
                         for (var i = 0; i < $scope.filtro.camposDetalle.length; i++) {
                           if ($scope.filtro.camposDetalle[i].value === $scope.form.camposDetalleFilter.value) {
                             $scope.filtro.camposDetalle.splice(i, 1);
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.vaciarTabla = function (tabla) {
                     switch (tabla) {
                       case "detalle":
                         $scope.plantilla.listaCamposDetalle = [];
                         $scope.plantilla.listaCamposAgrupacion = [];
                         $scope.plantilla.listaCamposOrdenacion = [];
                         $scope.plantilla.listaCamposSeparacion = [];
                         $scope.plantilla.listaCamposAgrupacionExcel = [];
                         $scope.plantilla.listaCamposCabecera = [];
                         break;
                       case "agrupacion":
                         if (!$scope.checked) {
                           $scope.plantilla.listaCamposAgrupacion = [];
                           $scope.plantilla.listaCamposOrdenacion = [];
                         } else {
                        	$scope.plantilla.listaCamposOrdenacion = [];
                           for (var i = 0; i < $scope.plantilla.listaCamposDetalle.length; i++) {
                             if ($scope.plantilla.listaCamposDetalle[i].description === "N") {
                               $scope.plantilla.listaCamposAgrupacion.push($scope.plantilla.listaCamposDetalle[i]);
                               $scope.plantilla.listaCamposOrdenacion.push($scope.plantilla.listaCamposDetalle[i]);
                             }
                           }
                         }
                         break;
                       case "ordenacion":
                         $scope.plantilla.listaCamposOrdenacion = [];
                         $scope.plantilla.listaCamposSeparacion = [];
                         $scope.plantilla.listaCamposAgrupacionExcel = [];
                         break;
                       case "separacion":
                         $scope.plantilla.listaCamposSeparacion = [];
                         $scope.plantilla.listaCamposCabecera = [];
                         break;
                       case "agrupacionExcel":
                         $scope.plantilla.listaCamposAgrupacionExcel = [];
                         break;
                       case "cabecera":
                         $scope.plantilla.listaCamposCabecera = [];
                         break;
                       case "detalleFiltro":
                         $scope.filtro.camposDetalle = [];
                         break;
                       default:
                         break;
                     }
                   };
                   
                   $scope.mostrarFiltros = function(){
                	   if($scope.modal.showFilters){
                		   $scope.modal.showFilters = false;
                	   }else{
                		   $scope.modal.showFilters = true;
                	   }
                   }

                   /***************************************************************************************************
                     * ** ACCIONES CRUD / MODALES ***
                     **************************************************************************************************/

                   $scope.srcImage = "images/warning.png";
                   
                   $scope.crearPlantillaInforme = function (salir) {
                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                    
                       PlantillasInformesNEWService.crearPlantilla(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           if (salir) {
                             $scope.getPlantillasInformes();
                             angular.element('#formularios').modal("hide");
                             $scope.activeAlert = false;
                             fErrorTxt('La plantilla de informe ' + $scope.plantilla.nbdescription
                                       + ' creada correctamente', 3);
                           } else {
                             $scope.plantilla.nbdescription = "";
                             $scope.srcImage = "images/accept.png";
                             $scope.mensajeAlert = "Plantilla de informe  " + $scope.plantilla.nbdescription
                                                   + " creada correctamente";
                           }
                         }
                       }, function (e) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al crear plantilla informe.", 1);
                       }, $scope.plantilla);
                     }
                   };

                   $scope.modificarPlantillaInforme = function (modificar) {
//                	   if ($scope.plantilla.fechaBase!="") {
//                		   var campo = "";
////                		   var alias = "";
//                		   var campoFecha = "";
////                		   var aliasFecha = "";
//                		   $scope.errorValidarFechaBase = false;
//                		   $scope.errorValidarFechaBaseNoEncontrada = false;
//                		   if (null!=$scope.plantilla.listaFiltrosDinamicos && $scope.plantilla.listaFiltrosDinamicos.length>0) {
//                			   for (var i=0; i<$scope.plantilla.listaFiltrosDinamicos.length; i++) {
//                				   for (var j=0; j<$scope.plantilla.listaFiltrosDinamicos[i].length; j++) {
//                					   if ($scope.plantilla.listaFiltrosDinamicos[i][j].tipo == CONSTANTES.TIPO_FECHA) {
//                						// Si solo tiene un elemento sin delimitador '/' se toma el 1er elemento.
//                						   if ($scope.plantilla.listaFiltrosDinamicos[i][j].campo.indexOf('/') != -1) {
//                							   campo = $scope.plantilla.listaFiltrosDinamicos[i][j].campo.split('/', -1);
//                							   campoFecha = campo[$scope.plantilla.posicion];
//                						   } else {
//                							   campo = $scope.plantilla.listaFiltrosDinamicos[i][j].campo;
//                							   campoFecha = campo;
//                						   }
//                						   // Si solo tiene un elemento sin delimitador '/' se toma el 1er elemento.
////                						   if ($scope.plantilla.listaFiltrosDinamicos[i][j].tabla.indexOf('/') != -1) {
////                							   alias = $scope.plantilla.listaFiltrosDinamicos[i][j].tabla.split('/', -1);
////                							   aliasFecha = alias[$scope.plantilla.posicion];
////                						   } else {
////                							   alias = $scope.plantilla.listaFiltrosDinamicos[i][j].tabla;
////                							   aliasFecha = alias;
////                						   }
////                						   var fechaBaseCampo = aliasFecha + '.' + campoFecha;
////                						   var alias = $scope.plantilla.listaFiltrosDinamicos[i][j].tabla.split('/',-1);
////                						   var campo = $scope.plantilla.listaFiltrosDinamicos[i][j].campo.split('/',-1);
////                						   var fechaBaseCampo = alias[$scope.plantilla.posicion]+'.'+campo[$scope.plantilla.posicion];
//                						   if (campoFecha === $scope.plantilla.fechaBase) {
//                							   $scope.labelFechaObligatoria = $scope.plantilla.listaFiltrosDinamicos[i][j].nombre;
//                							   $scope.errorValidarFechaBase = true;
//                							   if ((null!=$scope.plantilla.listaFiltrosDinamicos[i][j].valorCampo && $scope.plantilla.listaFiltrosDinamicos[i][j].valorCampo !="") || 
//                									   (null!=$scope.plantilla.listaFiltrosDinamicos[i][j].valorNumeroFechaDesde && $scope.plantilla.listaFiltrosDinamicos[i][j].valorNumeroFechaDesde !="") 
//                									   && $scope.errorValidarFechaBase) {
//                								   $scope.errorValidarFechaBase = false;
//                								   $scope.errorValidarFechaBaseNoEncontrada = false;
//                								   i = $scope.plantilla.listaFiltrosDinamicos.length;
//                								   break;
//                							   }
//                						   } else {
//                							   $scope.errorValidarFechaBaseNoEncontrada = true;
//                						   }
//                					   }
//                				   }
//                			   }
//                			   if ($scope.errorValidarFechaBase) {
//                				   $scope.errorRangoFechas = true;
//                				   $scope.errorValidarFechaBaseNoEncontrada = false;
//                			   } else if ($scope.errorValidarFechaBaseNoEncontrada) {
//                				   $scope.errorRangoFechas = true;
//                			   }
//                		   }
//                	   }
                	 inicializarLoading();  
                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       if (modificar) {
                    	 angular.element('#formularios').modal("hide");
                         PlantillasInformesNEWService.modificarPlantilla(
                                                 function (data) {
                                                   if (data.resultados.status === 'KO') {
                                                	 angular.element('#formularios').modal({backdrop : 'static'});
                                                     $(".modal-backdrop").remove();
                                                     $.unblockUI();
                                                     $scope.srcImage = "images/warning.png";
                                                     $scope.mensajeAlert = data.error;
                                                   } else {
                                                	 $.unblockUI();
                                                     $scope.getPlantillasInformes();
                                                     fErrorTxt('Plantilla de informe ' + $scope.plantilla.nbdescription + ' modificada correctamente', 3);
                                                   }
                                                 },
                                                 function (error) {
                                                   $.unblockUI();
                                                   angular.element('#formularios').modal("hide");
                                                   fErrorTxt("Ocurrió un error durante la petición de datos al modificar la plantilla de informe.",1);
                                                 }, $scope.plantilla);
                       } else {
                    	 angular.element('#formularios').modal("hide");
                         PlantillasInformesNEWService.crearPlantilla(function (data) {
                           if (data.resultados.status === 'KO') {
                        	 angular.element('#formularios').modal({backdrop : 'static'});
                             $(".modal-backdrop").remove();
                             $.unblockUI();
                             $scope.srcImage = "images/warning.png";
                             $scope.mensajeAlert = data.error;
                           } else {
                        	 $.unblockUI();
                             $scope.plantilla.idInforme = data.resultados.idInforme;
                             $scope.srcImage = "images/accept.png";
                             $scope.mensajeAlert = "Plantilla de informe  " + $scope.plantilla.nbdescription
                                                   + " creada correctamente";

                           }
                         }, function (e) {
                           $.unblockUI();
                           angular.element('#formularios').modal("hide");
                           fErrorTxt("Ocurrió un error durante la petición de datos al crear plantilla informe.", 1);
                         }, $scope.plantilla);
                       }
                     }
                   };

                   $scope.validacionFormulario = function () {
                     if (!$scope.plantilla.nbdescription || $scope.plantilla.listaCamposDetalle.length == 0
                    		 /*|| $scope.plantilla.listaCamposOrdenacion.length == 0 || $scope.errorValidarFechaBase===true || $scope.errorValidarFechaBaseNoEncontrada===true*/) {
                       return true;
                     } else {
                       return false;
                     }
                   }

                   $scope.borrarPlantillaInforme = function () {
                     if ($scope.plantillasInformesSeleccionadasBorrar.length > 0) {
                       if ($scope.plantillasInformesSeleccionadasBorrar.length == 1) {
                         angular.element("#dialog-confirm")
                             .html("¿Desea eliminar la plantilla de informe seleccionada?");
                       } else {
                         angular.element("#dialog-confirm")
                             .html(
                                   "¿Desea eliminar las " + $scope.plantillasInformesSeleccionadasBorrar.length
                                       + " plantillas de informe seleccionadas?");
                       }

                       // Define the Dialog and its properties.
                       angular
                           .element("#dialog-confirm")
                           .dialog(
                                   {
                                     resizable : false,
                                     modal : true,
                                     title : "Mensaje de Confirmación",
                                     height : 150,
                                     width : 360,
                                     buttons : {
                                       " Sí " : function () {
                                         PlantillasInformesNEWService
                                             .eliminarPlantillas(
                                                                 function (data) {
                                                                	 
                                                                	 if(data.resultados.tmct0Informes != null) {
                                                                		angular.element("#dialog-confirm").html("Las plantillas: " + data.resultados.tmct0Informes + "tienen planificaciones asociadas ¿Desea eliminarlas igualmente?");
                                                                		angular.element("#dialog-confirm").dialog(
                                                                		{
                                                                			resizable : false,
                                                                			modal : true,
                                                                			title : "Mensaje de Confirmación",
                                                                			height : 200,
                                                                			width : 360,
                                                                			buttons : {
                                                                					 " Sí " : function () {
                                                                					   PlantillasInformesNEWService
                                                                						   .eliminarPlantillas(
                                                                											   function (data2) {
                                                                												   $scope.getPlantillasInformes();
                                                                											   },
                                                                											   function (error) {
                                                                												 $.unblockUI();
                                                                												 fErrorTxt(
                                                                														   "Ocurrió un error durante la petición de datos al eliminar plantillas de informes.",
                                                                														   1);
                                                                											   },
                                                                											   {
                                                                												 listaPlantillasBorrar : $scope.plantillasInformesSeleccionadasBorrar,
                                                                												 eliminarPlanificacion : true
                                                                											   });

                                                                					   $(this).dialog('close');
                                                                					 },
                                                                					 " No " : function () {
                                                                					   $(this).dialog('close');
                                                                					 }
                                                                				   }
                                                                		});
                                                                	} else {
                                                                		$scope.getPlantillasInformes();
                                                                	}
                                                                 },
                                                                 function (error) {
                                                                   $.unblockUI();
                                                                   fErrorTxt(
                                                                             "Ocurrió un error durante la petición de datos al eliminar plantillas de informes.",
                                                                             1);
                                                                 },
                                                                 {
                                                                   listaPlantillasBorrar : $scope.plantillasInformesSeleccionadasBorrar,
                                                                   eliminarPlanificacion : false
                                                                 });
                                         $scope.deseleccionarTodos();
                                         $(this).dialog('close');
                                       },
                                       " No " : function () {
                                         $(this).dialog('close');
                                       }
                                     }
                                   });
                       $('.ui-dialog-titlebar-close').remove();
                     } else {
                       fErrorTxt("Debe seleccionar al menos un elemento de la tabla de plantillas de informe.", 2)
                     }
                   };

                   /***************************************************************************************************
                     * ** ACCIONES TABLA ***
                     **************************************************************************************************/
                   $scope.seleccionarTodos = function () {
                     // Se inicializa los elementos que ya tuviera la lista de elementos a borrar.
                     $scope.plantillasInformesSeleccionadasBorrar = [];
                     for (var i = 0; i < $scope.listPlantillasInformes.length; i++) {
                       $scope.listPlantillasInformes[i].selected = true;
                       $scope.plantillasInformesSeleccionadasBorrar.push($scope.listPlantillasInformes[i].idInforme);
                     }
                     $scope.plantillasInformesSeleccionadas = angular.copy($scope.listPlantillasInformes);
                   };

                   $scope.deseleccionarTodos = function () {
                     for (var i = 0; i < $scope.listPlantillasInformes.length; i++) {
                       $scope.listPlantillasInformes[i].selected = false;
                     }
                     $scope.plantillasInformesSeleccionadas = [];
                     $scope.plantillasInformesSeleccionadasBorrar = [];
                   };
                  
                   $scope.mostrarFiltrosDinaCargados = function () {
                	   angular.element('.mensajeBusquedaPlantillaInforme').empty();
                	   angular.element('.mensajeBusquedaPlantillaInforme').show();
                	   var cadenaFiltros = "";
                	   for (var x = 0; x < $scope.plantilla.listaFiltrosDinamicos.length; x++) {
                		   var filtros = $scope.plantilla.listaFiltrosDinamicos[x];
                		   
                		   for (var y = 0; y < filtros.length; y++) {
                			   var filtro = filtros[y];
                			   
                			   if(cadenaFiltros != "") {
                				   if("" != filtro.valorCampo && null != filtro.valorCampo) {
                					   cadenaFiltros = cadenaFiltros + " | ";
                				   }else if(!filtro.tipoLista && !filtro.tipoAutorrellenable && (filtro.valorCampo != null && filtro.valorCampo != "")) {
                					   cadenaFiltros = cadenaFiltros + " | ";
                				   }else if(filtro.tipoLista || filtro.tipoAutorrellenable || filtro.tipoTexto) {
                					   if(filtro.listaSeleccSelectDTO.length > 0) {
                						   cadenaFiltros = cadenaFiltros + " | ";
                					   }
                					   
                				   }
                			   }
                			   
                			   if(filtro.subtipoDesdeHasta) {
                				   if ("" != filtro.valorCampo && null != filtro.valorCampo) {
                					   if(filtro.valorCampoHasta == null) {
                						   cadenaFiltros = cadenaFiltros + filtro.nombre + ": " + filtro.valorCampo + " - " + "";
                					   }else {
                						   cadenaFiltros = cadenaFiltros + filtro.nombre + ": " + filtro.valorCampo + "-" + filtro.valorCampoHasta;
                					   }
                				   }else if ("" != filtro.valorNumeroFechaDesde && null != filtro.valorNumeroFechaHasta) {
                					   if(filtro.valorNumeroFechaHasta == null) {
                						   cadenaFiltros = cadenaFiltros + filtro.nombre + ": hoy " + filtro.lblBtnSentidoFiltro + " " + filtro.valorNumeroFechaDesde + " - " + "";
                					   }else {
                						   cadenaFiltros = cadenaFiltros + filtro.nombre + ": hoy " + filtro.lblBtnSentidoFiltro + " " + filtro.valorNumeroFechaDesde + " días a hoy " + filtro.lblBtnSentidoFiltroHasta + filtro.valorNumeroFechaHasta + " días.";
                					   }
                				   }
                			   } else {
                				   if ("" != filtro.valorCampo && null != filtro.valorCampo) {
                					   cadenaFiltros = cadenaFiltros + filtro.nombre + ": " + filtro.valorCampo;
                				   }else if("" != filtro.valorNumeroFechaDesde && null != filtro.valorNumeroFechaDesde){
                					   cadenaFiltros = cadenaFiltros + filtro.nombre + ": " + filtro.valorNumeroFechaDesde;
                				   }
                			   }
                			   
                			   if(filtro.tipoLista || filtro.tipoAutorrellenable || filtro.tipoTexto) {
                				   var cadenaLista = "";
                				   if(filtro.listaSeleccSelectDTO.length > 0) {
                    				   for(var i = 0; i < filtro.listaSeleccSelectDTO.length; i++) {
                    					   if(cadenaLista != "") {
                    						  cadenaLista = cadenaLista + ", " + filtro.listaSeleccSelectDTO[i].value;  
                    					   }else {
                    						  cadenaLista = filtro.listaSeleccSelectDTO[i].value;
                    					   }
                    				   }
                    				   
                    				   cadenaFiltros = cadenaFiltros + filtro.nombre + ": " + cadenaLista;
                				   }
                			   }
                			   
                		   }
                	   }
                	   angular.element('.mensajeBusquedaPlantillaInforme').append(cadenaFiltros);
                   };
                   
                   /** Inicio - Componente filtros dinamicos. */
                   /**
                    *	Precarga de datos del servicio. 
                    */
                   
                   
                   $scope.cargarSelectMercado = function(){
                	   $scope.listMercadosInformes = [{key: "TODOS", value: "TODOS"}, {key: $scope.plantilla.clase.mercado, value: $scope.plantilla.clase.mercado}];
                  	   $scope.plantilla.mercado = $scope.listMercadosInformes[1];
                   };
                    	   
                   $scope.limpiarFiltrosDinaCargados = function () {
                	   angular.element('.mensajeBusquedaPlantillaInforme').empty();
                   }
                   
                   
                   $scope.precargarFiltroDinamico =  function () {
                	   
                	   inicializarLoading();
                	   
                	   DirectivasService.devolverElementosByParams(function (data) {
                		   if (data.resultados.status === 'KO') {
                			   $.unblockUI();
                			   $scope.srcImage = "images/warning.png";
                			   $scope.mensajeAlert = data.error;
                		   } else {
                			   $scope.plantilla.listaFiltrosDinamicos = data.resultados["datosFiltroInformeByIdInforme"];
                			   $.unblockUI();
                		   }
                	   }, function (error) {
                		   $.unblockUI();
                		   fErrorTxt("Se produjo un error al cargar ids de elementos.", 1);
                	   }, {
                		   "clase" : $scope.plantilla.clase,
                		   "mercado" : $scope.plantilla.mercado
                	   });
                	   
                	  
                	   $scope.cargarCamposDetalle();
                	   $scope.limpiarFiltrosDinaCargados();
                	   $scope.vaciarTabla('detalleFiltro');
                	   
                   }
                   
                   $scope.operarFecha = function(valor, elemento) {
            		   camposDinamicosHelper.operarFecha($scope, valor, elemento);
            	   };
            	   
                   $scope.operarListaTexto = function(valor, elemento) {
            		   camposDinamicosHelper.operarListaTexto($scope, valor, elemento);
            	   };
            	   
                   $scope.cargarListasAutocompletables = function(){
                	   $scope.modal.showGuardar = true;
            		   camposDinamicosHelper.inicializarScope($scope);
            		   $(".mensajeBusquedaPlantillaInforme").hide();
            		   for (var x = 0; x < $scope.plantilla.listaFiltrosDinamicos.length; x++) {
            			   for (var v = 0; v < $scope.plantilla.listaFiltrosDinamicos[x].length; v++) {
            				   if($scope.plantilla.listaFiltrosDinamicos[x][v].tipo == CONSTANTES.TIPO_AUTORRELLENABLE){
            					   $scope.pupulateAutocompleteDinamic('#' + $scope.plantilla.listaFiltrosDinamicos[x][v].identificador, $scope.plantilla.listaFiltrosDinamicos[x][v].listaSelectDTO, x, v);
	            			   }
            				   if($scope.plantilla.listaFiltrosDinamicos[x][v].subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA){
            					   $scope.plantilla.listaFiltrosDinamicos[x][v].mostrarFechaDesde = true;
            				   }
            			   }
            		   }
                   };
                   
                   /** Fin - Componente filtros dinamicos. */

                   $scope.cargarPlantillasMercadosFiltro = function () {
                       PlantillasInformesNEWService.cargaPlantillasFiltro(function (data) {
                         $scope.listPlantillas = data.resultados["listaPlantillasFiltro"];
                         $scope.listMercados = data.resultados["listaMercados"];
                       }, function (error) {
                         fErrorTxt("Se produjo un error en la carga del combo de plantillas.", 1);
                       }, $scope.filtro.clase);
                     };
                   
                 } ]);
