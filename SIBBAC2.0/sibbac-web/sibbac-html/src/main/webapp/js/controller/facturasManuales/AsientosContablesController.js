'use strict';
sibbac20
	.controller(
        'AsientosContablesController',
        [
         '$scope',
         '$document',
         'growl',
         'AsientosContablesService',
         'CombosFacturasManualesService',
         'CatalogoService',
         '$compile',
         'SecurityService',
         '$rootScope',
         'moment',
         
         function ($scope, $document, growl, AsientosContablesService, CombosFacturasManualesService,
        		 CatalogoService, $compile, SecurityService, $rootScope, moment) {

        	 console.log(' +++++ AsientosContablesController +++++ ');

             /***************************************************************************************************
              * ** INICIALIZACION DE DATOS ***
              **************************************************************************************************/
        	 var hoy = new Date();
             var dd = hoy.getDate();
             var mm = hoy.getMonth() + 1;
             var yyyy = hoy.getFullYear();
             hoy = yyyy + "_" + mm + "_" + dd;

        	 $scope.catalogos = [];
        	 var oTableAsientosContables = null;
        	 $scope.filtro = {};
        	 $scope.followSearch = "";
             $scope.showingFilters = true;

        	 /***************************************************************************************************
              * ** CARGA DE DATOS INICIAL ***
              **************************************************************************************************/
        	// Carga del combo Estados Fichero.
            $scope.cargarCatalogos = function () {
            	CombosFacturasManualesService.consultarAlias(function (data) {
            		$scope.catalogos.listaAlias = data.resultados["listaAlias"];
            		populateAutocomplete("#aliasFiltro", $scope.catalogos.listaAlias, function (option) {
                        $scope.filtro.compania = option.key;
                        $scope.aliasFiltro = $.trim(option.value);
                        console.log(' +++++ $scope.filtro.compania +++++ ' + $scope.filtro.compania);
                    });
            	}, function (error) {
            		fErrorTxt("Se produjo un error en la carga del combo de asientos contables.", 1);
            	});

            	AsientosContablesService.cargaCatalogos(function (data) {
            		$scope.catalogos = data.resultados["catalogos"];
            	}, function (error) {
            		fErrorTxt("Se produjo un error en la carga del combo de asientos contables.", 1);
            	});
            }

            // Reset del filtro
            $scope.resetFiltro = function () {
            	/** TODO - Resetear parametros */
            	$scope.filtro = {
            		compania : "",
            		fecha: "",
            		tipoFactura: ""
            	};
            	$scope.aliasFiltro = "";
            	$scope.sentidoFiltro = [];
            	$scope.asientosContablesSeleccionados = [];
            	$scope.modalAsientosContables = {};
            	$scope.asientosContablesSeleccionadosBorrar = [];
            	for ( var key in $scope.filtro) {
            		$scope.sentidoFiltro[key] = "IGUAL";
            	}
            };

            // Setea el filtro que se pasará al back
            $scope.getFiltro = function () {
            	var filtro = {};
            	for (var key in $scope.filtro) {
            		if ($scope.filtro[key] != null && $scope.filtro[key] != "") {
	        			if ($scope.sentidoFiltro[key] == "IGUAL") {
	        				filtro[key] = $scope.filtro[key];
	        			} else {
	        				filtro[key + "Not"] = $scope.filtro[key];
	                        filtro[key] = null;
	                    }
            		}
            	}
            	return filtro;
            };

            var labelFiltro = {
        		compania : "Compañía",
        		fecha : "Fecha",
        		tipoFactura : "Tipo"
            };


            $scope.getValorDescriptivo = function (filtro, valor) {
            	return valor;
            }

        	/** Follow search.  */
            $scope.getFollowSearch = function () {
            	var texto = "";

            	for ( var key in $scope.filtro) {
            		console.log("key: " + key);
            		if ($scope.filtro[key] != null && $scope.filtro[key] != "") {

            			var operador = "";
            			if ($scope.sentidoFiltro[key] == "IGUAL") {
            				operador = "=";
            			} else {
            				operador = "<>";
            			}

            			var valorDescriptivo = $scope.getValorDescriptivo(key, $scope.filtro[key]);
            			if (valorDescriptivo == "") {
            				valorDescriptivo = $scope.filtro[key];
            			}

            			if (texto == "") {
            				texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo;
            			} else {
            				texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo;
            			}

            		}
            	}
            	$scope.followSearch = texto;
            	return texto;
            }

            // Carga del listado principal
            $scope.busqueda = function () {
            	// Se oculta el panel de búsqueda y se muestra el detalle
                $scope.showingFilter = false;
                // Se muestra el loader.
                inicializarLoading();
                AsientosContablesService.buscarAsientosContables(function (data) {
                	// Se oculta el loader.
                	$.unblockUI();
                	$scope.pintarTabla(data);
                }, function (error) {
                	// Se oculta el loader.
                	$.unblockUI();
                	fErrorTxt("Se produjo un error en la carga del listado de asientos contables.", 1);
                }, $scope.getFiltro());
            }

            // Borrado de tabla
            $scope.borrarTabla = function () {
            	var tbl = $("#datosAsientosContables > tbody");
                $(tbl).html("");
                oTableAsientosContables.fnClearTable();
            };

            // Seleccion de un elemento de la tabla
            $scope.seleccionarElemento = function (row) {
            	if ($scope.listaRespAsientosContables[row] != null && $scope.listaRespAsientosContables[row].selected) {
            		$scope.listaRespAsientosContables[row].selected = false;
            		for (var i = 0; i < $scope.asientosContablesSeleccionados.length; i++) {
            			if ($scope.asientosContablesSeleccionados[i].prefijoId === $scope.listaRespAsientosContables[row].prefijoId) {
            				$scope.asientosContablesSeleccionados.splice(i, 1);
            				$scope.asientosContablesSeleccionadosBorrar.splice(i, 1);
            			}
            		}
            	} else {
            		$scope.listaRespAsientosContables[row].selected = true;
            		$scope.asientosContablesSeleccionados.push($scope.listaRespAsientosContables[row]);
            		$scope.asientosContablesSeleccionadosBorrar.push($scope.listaRespAsientosContables[row].prefijoId);
            	}
            };


            // Se pinta el datatable resultado de la busqueda.
            $scope.pintarTabla = function (data) {
            	/** TODO - Pintado de la tabla de resultados. */

            	// borra el contenido del body de la tabla
            	if (oTableAsientosContables != null) {
                    $scope.borrarTabla();
            	}

            	var list = [];

            	// Se pasa resultado del servicio al scope
            	$scope.listaRespAsientosContables = data.resultados["listaBusquedaAsientosContablesDTO"];

            	// Se inicializa tabla
            	inicializarTabla();

            	// Se popula tabla
            	if ($scope.listaRespAsientosContables.length > 0) {
	            	for (var i = 0; i < $scope.listaRespAsientosContables.length; i++) {

	            		var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listaRespAsientosContables['
	                        + i + '].selected" ng-click="seleccionarElemento(' + i + ');"/>';
	            		list[i] = [check,
	            		           $scope.listaRespAsientosContables[i].estado,
	            		           $.number($scope.listaRespAsientosContables[i].totalDebeHaber, 2, ',', '.'),
	            		           $scope.listaRespAsientosContables[i].tipoApunte,
	            		           $scope.listaRespAsientosContables[i].nroFactura,
	            		           $scope.listaRespAsientosContables[i].descripcionFactura,
	            		           $scope.listaRespAsientosContables[i].empresaContraparte,
	            		           $scope.listaRespAsientosContables[i].importeImpuestos,
	            		           $scope.listaRespAsientosContables[i].fechaFactura != undefined && $scope.listaRespAsientosContables[i].fechaFactura != null ?
	            		        		   moment($scope.listaRespAsientosContables[i].fechaFactura).format('DD/MM/YYYY') : ''
	            		];

	            	}
	            	// Se pintan datos de la tabla
	            	oTableAsientosContables.fnAddData(list, false);
	            	oTableAsientosContables.fnDraw();
            	}
            }

            /***************************************************************************************************
             * ** DEFINICION TABLA **
             **************************************************************************************************/
            var inicializarTabla = function () {
            	/** TODO - Inicializacion de la tabla de resultados. */
            	if (oTableAsientosContables == null) {
            		oTableAsientosContables = $("#datosAsientosContables").dataTable({
	                    "dom" : 'T<"clear">lfrtip',
	                    "tableTools" : {
	                    	"sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
	                        "aButtons" : [ "copy", {
	                          "sExtends" : "csv",
	                          "sFileName" : "Listado_Asientos_Contables_" + hoy + ".csv",
	                          "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8 ]
	                        }, {
	                          "sExtends" : "xls",
	                          "sFileName" : "Listado_Asientos_Contables_" + hoy + ".xls",
	                          "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8 ]
	                        }, {
	                          "sExtends" : "pdf",
	                          "sPdfOrientation" : "landscape",
	                          "sTitle" : " ",
	                          "sPdfSize" : "A3",
	                          "sPdfMessage" : "Resultados Asientos Contables",
	                          "sFileName" : "Listado_Asientos_Contables_" + hoy + ".pdf",
	                          "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8 ]
	                        }, "print" ]
	                    },
	                    "aoColumns" : [ {
	                      sClass : "centrar",
	                      bSortable : false,
	                      width : "50px"
	                    }, {
	                      width : "200px"
	                    }, {
	                      width : "200px"
	                    }, {
	                      width : "200px"
	                    }, {
	                      width : "200px"
	                    }, {
	                      width : "200px"
	                    }, {
		                  width : "200px"
	                    }, {
			              width : "200px"
	                    }, {
			              width : "200px"
	                    }],
	                    "fnCreatedRow" : function (nRow, aData, iDataIndex) {
	                      $compile(nRow)($scope);
	                    },
	                    "scrollY" : "480px",
	                    "scrollX" : "100%",
	                    "scrollCollapse" : true,
	                    "language" : {
	                      "url" : "i18n/Spanish.json"
	                    }
	                  });
	            	  console.log(' +++++ oTableAsientosContables : ' + oTableAsientosContables + ' +++++ ');
            	}
            }

            $scope.resetFiltro();
            $scope.cargarCatalogos();
            /** TODO - Asignar filtros de combos a la key del scope. */

            $scope.safeApply = function (fn) {
              var phase = this.$root.$$phase;
              if (phase === '$apply' || phase === '$digest') {
                if (fn && (typeof (fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };


            var populateAutocomplete = function (input, availableTags, callback) {

              $(input).autocomplete({
                minLength : 0,
                source : availableTags,
                focus : function (event, ui) {
                  return false;
                },
                select : function (event, ui) {
                  var option = {
                    key : ui.item.key,
                    value : ui.item.value
                  };
                  callback(option);
                  $scope.safeApply();
                  return false;
                }
              });

            };


            /***************************************************************************************************
             * ** CARGA DE DATOS INICIALES
             **************************************************************************************************/

            $document.ready(function () {
            	inicializarTabla();
            });

	         /***************************************************************************************************
	          * ** ACCIONES CRUD / MODALES
	          **************************************************************************************************/

	         // Abrir modal Ver Asiento Contable
	         $scope.abrirModalAsientoContable = function () {
	        	 $scope.errores = [];
	        	 // Solo se permite abrir modal para respuestas con error
	        	 if ($scope.asientosContablesSeleccionados.length > 1 || $scope.asientosContablesSeleccionados.length == 0) {
	        		 if ($scope.asientosContablesSeleccionados.length == 0) {
	                     fErrorTxt("Debe seleccionar al menos un elemento de la tabla.", 2)
	                 } else {
	                     fErrorTxt("Debe seleccionar solo un elemento de la tabla.", 2)
	                 }
	             } else {
	            	 inicializarLoading();
	            	 AsientosContablesService.verAsientoContable(
            			 function (data) {
            				 if (data.resultados == null) {
                        		 $.unblockUI();
                                 fErrorTxt(
                                		 "Ocurrió un error durante la petición de datos al consultar el asiento contable.",
                                         1);
                                 return;
                             }
                             $(".modal-backdrop").remove();

                             // Se oculta el loader.
                         	 $.unblockUI();
                         	 $scope.pintarModalVerAsiento(data);

                             $scope.modalAsientosContables.titulo = "Detalle Asiento Contable";
                             angular.element('#formularioDetalleAsiento').modal({
                            	 backdrop : 'static'
                             });
            			 },
                         function (error) {
            				 $.unblockUI();
                             angular.element('#formularioDetalleAsiento').modal("hide");
                             fErrorTxt(
                                       "Ocurrió un error durante la petición de datos al consultar el asiento contable.",
                                       1);
                         }, {
                             "id" : $scope.asientosContablesSeleccionados[0].prefijoId
                         }
                	 )
	             }
	         }

	      // Se pinta el datatable resultado de la busqueda.
	      $scope.pintarModalVerAsiento = function (data) {
	    	  	// Se pasa resultado del servicio al scope
          		$scope.modalAsientosContables.detalles = data.resultados["detalleAsientoContableDTO"];
          		$scope.modalAsientosContables.detalles.cuentaMayor = $scope.modalAsientosContables.detalles.cuentaMayor + " - " + $scope.modalAsientosContables.detalles.cuentaMayorName;
          		$scope.modalAsientosContables.detalles.tipoMovimiento = $scope.modalAsientosContables.detalles.tipoMovimiento == 'D'?"DEBE":"HABER";
	      }

         } ]);
