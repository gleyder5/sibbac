'use strict';
sibbac20.controller('DesglosesMQControler', ['$scope', '$compile', 'growl', 'IsinService', '$document', 'DesgloseMQService', 'ngDialog', 'TitularesRoutingService', function ($scope, $compile, growl, IsinService, $document, DesgloseMQService, ngDialog, TitularesRoutingService) {
	var SHOW_LOG = true;
	var availableTags = [];
	var oTableOrdenes = undefined;
	var oTableDesgloses = undefined;
	$scope.listaChkOrdenes = [];
    $scope.listaChkOrdenesValidado = [];
    $scope.listaChkOrdenesNumOperacion = [];
    $scope.listaChkDesgloses           = [];
    $scope.listaChkDesglosesValidado   = [];
    $scope.listaChkDesglosesNumOperacion = [];
    $scope.filtroConsulta                = {};
    $scope.filtroValidar                 = {};
    $scope.dialogTitle                   = "";
    $scope.needRefresh                   = false;
    $scope.altaDesglose                  = {};
    $scope.altaTitular                   = {};
    $scope.editarDesglose 				 = {};
    $scope.editarTitular 				 = {};
    $scope.listaSentido 				 = [];
    $scope.listaTipoSaldo 				 = [];
    $scope.listaTipoCambio 				 = [];
    $scope.sentido 						 = {};
    $scope.tipoSaldo 					 = {};
    $scope.tipoCambio 					 = {};
    $scope.listaBolsaMercado 			 = [];
    $scope.listaNacionalidad 			 = [];
    $scope.listaTipoNacionalidad		 = [];
    $scope.listaTipoDocumento			 = [];
    $scope.listaTipoSociedad			 = [];
    $scope.numOrden			 			 = "";
    var oDesgloses 						 = [];
    var oTitulares 						 = [];
    var lMarcarOrdenes					 = true;
    var lMarcarDesgloses				 = true;
    var hoy 							 = new Date();
    var dd 								 = hoy.getDate();
    var mm 								 = hoy.getMonth() + 1;
    var yyyy 							 = hoy.getFullYear();
    var openDesgloses;
    var openTitulares;
    var paramsCrear 					 = [];
    var ejecutadaCon 					 = false;
    var alPaginasOrdenes				 = [];
    var paginaOrdenes					 = 0;
    var paginasTotalesOrdenes			 = 0;
    var ultimaPaginaOrdenes				 = 0;
    var alPaginasDesgloses				 = [];
    var paginaDesgloses					 = 0;
    var paginasTotalesDesgloses			 = 0;
    var ultimaPaginaDesgloses			 = 0;
    var desgloses 						 = [];
    var fsService 						 = server.service.conciliacion.desgloses;
    hoy                                  = yyyy + "_" + mm + "_" + dd;
    var IMG_SIZE = "14px";

    function  processInfoOrdenes(info) {

    	paginaOrdenes = info.page;
    	paginasTotalesOrdenes = info.pages;
        var regInicio  = info.start;
        var regFin     = info.end;
        return regFin - regInicio;
    }

    function  processInfoDesgloses(info) {

    	paginaDesgloses = info.page;
    	paginasTotalesDesgloses = info.pages;
        var regInicio  = info.start;
        var regFin     = info.end;
        return regFin - regInicio;
    }

    var calcDataTableOrdenesHeight = function (sY) {
        console.log("calcDataTableHeight: " + $('#tblOrdenesMQ').height());
        console.log("sY: " + sY);
        var numeroRegistros = processInfoOrdenes(oTableOrdenes.api().page.info());
        var tamanno         = 50 * numeroRegistros;
        if ($('#tblOrdenesMQ').height() > 510) {
            return 480;
        } else {
            if ($('#tblOrdenesMQ').height() > tamanno)
            {
                return $('#tblOrdenesMQ').height() + 30;
            } else
            {
                if (tamanno > 510)
                {
                    return 480;
                } else
                {
                    return tamanno;
                }
            }
        }
    };

    var calcDataTableDesglosesHeight = function (sY) {
        console.log("calcDataTableHeight: " + $('#tblDesglosesMQ').height());
        console.log("sY: " + sY);
        var numeroRegistros = processInfoDesgloses(oTableDesgloses.api().page.info());
        var tamanno         = 50 * numeroRegistros;
        if ($('#tblDesglosesMQ').height() > 510) {
            return 480;
        } else {
            if ($('#tblDesglosesMQ').height() > tamanno)
            {
                return $('#tblDesglosesMQ').height() + 30;
            } else
            {
                if (tamanno > 510)
                {
                    return 480;
                } else
                {
                    return tamanno;
                }
            }
        }
    };

    function cargarIsines(datosIsin) {
        availableTags = [];
        angular.forEach(datosIsin, function (val, key) {
            var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
            availableTags.push(ponerTag);
        });
        angular.element("#cdisin").autocomplete({
            source: availableTags
        });
        $scope.isines = datosIsin;
    };//function cargarIsines(datosIsin) {

    function showError(data, status, headers, config) {
        $.unblockUI();
        console.log("Error al inicializar datos: " + status);
    }

    function loadIsines() {
        IsinService.getIsines(cargarIsines, showError);
    }

 // Inicializa la página. Carga los combos y autocompletar del filtro.
    function initialize() {

        //var fechas = ["fechaDe", "fechaA"];
        //verificarFechas([["fechaDe", "fechaA"]]);
        $("#ui-datepicker-div").remove();
        $('input#fechaDe').datepicker({
            onClose: function( selectedDate ) {
              $( "#fechaA" ).datepicker( "option", "minDate", selectedDate );
            } // onClose
          });

          $('input#fechaA').datepicker({
            onClose: function( selectedDate ) {
              $( "#fechaDe" ).datepicker( "option", "maxDate", selectedDate );
            } // onClose
          });
        loadIsines();
        prepareCollapsion();
        var sentido = {id: 1, texto: 'Compra'};
        $scope.listaSentido.push(sentido);
        sentido = {id: 2, texto: 'Venta'};
        $scope.listaSentido.push(sentido);
        var tipoSaldo = {id: 'P', texto: 'Propio'};
        $scope.listaTipoSaldo.push(tipoSaldo);
        tipoSaldo = {id: 'T', texto: 'Terceros'};
        $scope.listaTipoSaldo.push(tipoSaldo);
        var tipoCambio = {id: 1, texto: 'Porcentaje'};
        $scope.listaTipoCambio.push(tipoCambio);
        tipoCambio = {id: 2, texto: 'Efectivo'};
        $scope.listaTipoCambio.push(tipoCambio);
    }

    /**
     * Se crea la tabla de forma normal
     * @returns {undefined}
     */
    function loadDataTableOrdenes() {
        oTableOrdenes = $("#tblOrdenesMQ").dataTable({
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": ["/sibbac20/js/swf/copy_csv_xls_pdf.swf"],
                "aButtons": ["copy",
                    {"sExtends": "csv",
                        "sFileName": "Listado_de_ordenes_" + hoy + ".csv",
                        "mColumns": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "Listado_de_ordenes_" + hoy + ".csv",
//                  	 "sCharSet" : "utf8",
                        "mColumns": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                    },
                    {
                        "sExtends": "pdf",
                        "sPdfOrientation": "landscape",
                        "sTitle": " ",
                        "sPdfSize": "A4",
                        "sPdfMessage": "Listado de Listado_de_ordenes",
                        "sFileName": "Listado_de_ordenes_" + hoy + ".pdf",
                        "mColumns": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                    },
                    "print"
                ]
            },
            "language": {
                "url": "i18n/Spanish.json"
            },
            "aoColumns": [
                {className: 'checkbox', targets: 0, sClass: "centrar", type: "checkbox", bSortable: false, name: "active", width: 50}, //0 Validar
                {sClass: "centrar", bSortable: false, width: 125},        //1 Acciones
                {sClass: "centrar", width: 100}, // 2 N. Orden.
                {sClass: "centrar", width: 100}, // 3 Cod. SV
                {sClass: "centrar", width: 50}, // 4 F. ejecución
                {sClass: "centrar", width: 50}, // 5 Tipo operación
                {sClass: "align-right", "sType": "numeric", width: 75}, // 6 Titulos
                {sClass: "align-right", "sType": "numeric", width: 75}, // 7 Titulos
                {sClass: "align-right", "sType": "numeric", width: 105}, // 8 Nominal
                {sClass: "centrar", width: 100}, // 9 Isin
                {sClass: "centrar", width: 250}, //10 descripcion
                {sClass: "centrar", width: 75}, //11 Bolsa de negociación
                {sClass: "centrar", width: 75}, //12 Tipo saldo
                {sClass: "centrar", width: 75}, //13 Entidad liquidadora
                {sClass: "centrar", width: 100}, //14 Tipo cambio
                {sClass: "centrar", width: 75}, //15 Validado
                {sClass: "centrar", width: 75} //16 Procesado

            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $compile(nRow)($scope);
            },
            "order": [],
            "scrollY": "480px",
            "scrollX": "100%",
            "scrollCollapse": true,
//"rowHeight" : "44px",

            drawCallback: function () {
                processInfoOrdenes(this.api().page.info());
                if (alPaginasOrdenes.length !== paginasTotalesOrdenes) {
                	alPaginasOrdenes = new Array(paginasTotalesOrdenes);
                    for (var i = 0; i < alPaginasOrdenes.length; i++) {
                    	alPaginasOrdenes[i] = true;
                    }
                }

                if (ultimaPaginaOrdenes !== paginaOrdenes) {

                    var valor = alPaginasOrdenes[paginaOrdenes];

                    if (valor) {
                        angular.element("#MarcarPagOrdenes").val("Marcar Página");
                    } else {
                        angular.element("#MarcarPagOrdenes").val("Desmarcar Página");
                    }

                }
                ultimaPaginaOrdenes = paginaOrdenes;
            }
        });

        angular.element("#fechaDe, #fechaA").datepicker({
            dateFormat: 'dd/mm/yy',
            defaultDate: new Date()
        }); // datepick

    }//function loadDataTableOrdenes() {

    function loadDataTableDesgloses() {



    	oTableDesgloses = $("#tblDesglosesMQ").dataTable({
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": ["/sibbac20/js/swf/copy_csv_xls_pdf.swf"],
                "aButtons": ["copy",
                    {"sExtends": "csv",
                        "sFileName": "Listado_de_desgloses_" + hoy + ".csv",
                        "mColumns": [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "Listado_de_desgloses_" + hoy + ".csv",
//                  	 "sCharSet" : "utf8",
                        "mColumns":  [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                    {
                        "sExtends": "pdf",
                        "sPdfOrientation": "landscape",
                        "sTitle": " ",
                        "sPdfSize": "A4",
                        "sPdfMessage": "Listado de Listado_de_desgloses",
                        "sFileName": "Listado_de_desgloses_" + hoy + ".pdf",
                        "mColumns": [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                    "print"
                ]
            },
            "language": {
                "url": "i18n/Spanish.json"
            },
            "aoColumns": [
               // {className: 'checkbox', targets: 0, sClass: "centrar", type: "checkbox", bSortable: false, name: "active", width: 50}, //0 Validar
                {sClass: "centrar", bSortable: false, width: 125},        //1 Acciones<th>Acciones</th>
                {sClass: "centrar", width: 50}, // 2 Fecha.<th>Fecha</th>
                {sClass: "centrar", width: 100}, // 3 N. referencia<th>N. referencia</th>
                {sClass: "centrar", width: 50}, // 4 Sentido<th>Sentido</th>
                {sClass: "centrar", width: 100}, // 5 Bolsa negociaci&oacute;n<th>Bolsa negociaci&oacute;n</th>
                {sClass: "centrar", width: 100}, // 6 Isin<th>Isin</th>
                {sClass: "align-right", "sType": "numeric", width: 75}, // 7 Titulos   <th>Titulos</th>
                {sClass: "align-right", "sType": "numeric", width: 75}, // 8 Precio    <th>Precio</th>
                {sClass: "centrar", width: 100}, // 9 Tipo Saldo <th>Tipo Saldo</th>
                {sClass: "centrar", width: 100}, //10 CCV<th>CCV</th>
                {sClass: "centrar", width: 75}, //11 Titular<th>Titular</th>
                {sClass: "centrar", width: 75}, //12 Entidad liquidadora <th>Entidad liquidadora</th>
                {sClass: "centrar", width: 75} //13 Tipo cambio <th>Tipo cambio</th>
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $compile(nRow)($scope);
            },
            "order": [],
            "scrollY": "480px",
            "scrollX": "100%",
            "scrollCollapse": true,
//"rowHeight" : "44px",

            drawCallback: function () {
                processInfoDesgloses(this.api().page.info());
                if (alPaginasDesgloses.length !== paginasTotalesDesgloses) {
                    alPaginasDesgloses = new Array(paginasTotalesDesgloses);
                    for (var i = 0; i < alPaginasDesgloses.length; i++) {
                        alPaginasDesgloses[i] = true;
                    }
                }

                if (ultimaPaginaDesgloses !== paginaDesgloses) {

                    var valor = alPaginasDesgloses[paginaDesgloses];

                    if (valor) {
                        angular.element("#MarcarPagDesgloses").val("Marcar Página");
                    } else {
                        angular.element("#MarcarPagDesgloses").val("Desmarcar Página");
                    }

                }
                ultimaPaginaDesgloses = paginaDesgloses;
            }
        });

    }//function loadDataTableDesgloses() {

    function cargarBolsaMercado(json) {
    	$scope.listaBolsaMercado = [];
        if (json.resultados != null && json.resultados !== undefined && json.resultados.bolsaMercadoList !== undefined) {
            var datos = json.resultados.bolsaMercadoList;

            var contador = 0;
            for (var k in datos) {
                var item = datos[k];
                var bolsaMercado = {
                		codigo: item.codigo,
                		descripcion : item.descripcion
                };
                $scope.listaBolsaMercado.push(bolsaMercado);
            }//for (var k in datos) {
        }//for (var k in datos) {
    };//cargarBolsaMercado

    function onSuccessBolsaMercado(data, status, header, config) {
        console.log("onSuccessBolsaMercado: " + data);
        cargarBolsaMercado(data);
    };

    function loadBolsaMercado (){
	   	 console.log("listar bolsa Mercado: ");
	   	 var filtroBolsaMercado = {};
	     DesgloseMQService.getListaBolsaMercado(onSuccessBolsaMercado, onErrorRequest, filtroBolsaMercado);
   };

   function cargarNacionalidad(json) {
   	   $scope.listaNacionalidad = [];
	   if (json.resultados == null) {
	        fErrorTxt(json.error, 1);
	   }
       if (json.resultados != null && json.resultados !== undefined && json.resultados.resultados_nacionalidad !== undefined) {
           var datos = json.resultados.resultados_nacionalidad;
           $scope.listaNacionalidad = [];
           var contador = 0;
           for (var k in datos) {
               var item = datos[k];
               var textoOpcion = item.cdiso2po + " - " + item.nbispais + "(" + item.cdispais + ")";
               var nacionalidad = {
            		cdispais: item.cdispais,
            		cdiso2po : item.cdiso2po,
            		nbispais : item.nbispais,
            		textoOpcion : textoOpcion
               };
               $scope.listaNacionalidad.push(nacionalidad);
           }//for (var k in datos) {
       }//for (var k in datos) {
   };//function cargarNacionalidad(json) {

   function onSuccessComboNacionalidad(data, status, header, config) {
       console.log("onSuccessComboNacionalidad: " + data);
       cargarNacionalidad(data);
   };

   function cargarComboNacionalidad() {
	   var filtroNacionalidad = {};
	   TitularesRoutingService.getNacionalidadList(onSuccessComboNacionalidad, onErrorRequest, filtroNacionalidad);
       //  $('#cdnaci_TI').append("<option value='" + item.cdispais + "'>" + item.cdiso2po + " - " + item.nbispais + "(" + item.cdispais + ") </option>"
   }

   function cargarTipoNacionalidad(json) {
   	   $scope.listaTipoNacionalidad = [];
	   if (json.resultados == null) {
	        fErrorTxt(json.error, 1);
	   }
       if (json.resultados != null && json.resultados !== undefined && json.resultados.resultados_tpnactit !== undefined) {
           var datos = json.resultados.resultados_tpnactit;
           $scope.listaTipoNacionalidad = [];
           var contador = 0;
           for (var k in datos) {
               var item = datos[k];
               var textoOpcion = "(" + item.codigo + ") " + item.descripcion;
               var nacionalidad = {
            		   codigo: item.codigo,
            		   descripcion : item.descripcion,
            		   textoOpcion : textoOpcion
               };
               $scope.listaTipoNacionalidad.push(nacionalidad);
           }//for (var k in datos) {
       }//for (var k in datos) {
   };//function cargarTipoNacionalidad(json) {

   function onSuccessTipoNacionalidad(data, status, header, config) {
       console.log("onSuccessTipoNacionalidad: " + data);
       cargarTipoNacionalidad(data);
   };

   function cargarComboTipoNacionalidad() {

	   var filtroNacionalidad = {};
	   TitularesRoutingService.getTpnactitList(onSuccessTipoNacionalidad, onErrorRequest, filtroNacionalidad);
       //$('#tpnaci_TI').append("<option value='" + item.codigo + "'>(" + item.codigo + ") " + item.descripcion + "</option>"
   };

   function cargarPaisRes(json) {
   	   $scope.listaPaisRes = [];
	   if (json.resultados == null) {
	        fErrorTxt(json.error, 1);
	   }
       if (json.resultados != null && json.resultados !== undefined && json.resultados.resultados_paisres !== undefined) {
           var datos = json.resultados.resultados_paisres;
           $scope.listaPaisRes = [];
           var contador = 0;
           for (var k in datos) {
               var item = datos[k];
               var id = item.id;
               var idPais = id['cdhppais'];
               var textoOpcion = item.cdiso2po + " - " + item.nbhppais + "(" + id['cdhppais'] + ")";
               var pais = {
            		   idPais : idPais,
            		   id: item.id,
            		   cdiso2po : item.cdiso2po,
            		   nbhppais : item.nbhppais,
            		   textoOpcion : textoOpcion
               };
               $scope.listaPaisRes.push(pais);
           }//for (var k in datos) {
       }//for (var k in datos) {
   };//function cargarPaisRes(json) {

   function onSuccessPaisRes(data, status, header, config) {
       console.log("onSuccessPaisRes: " + data);
       cargarPaisRes(data);
   };

   function cargarComboPaisResidencia() {

	   var filtroPais = {};
	   TitularesRoutingService.getPaisResList(onSuccessPaisRes, onErrorRequest, filtroPais);
       //$('#paires_TI').append("<option value='" + id['cdhppais'] + "'>" + item.cdiso2po + " - " + item.nbhppais + "(" + id['cdhppais'] + ") </option>"
   }

   function cargarTipoDocumento(json) {
   	   $scope.listaTipoDocumento = [];
	   if (json.resultados == null) {
	        fErrorTxt(json.error, 1);
	   }
       if (json.resultados != null && json.resultados !== undefined && json.resultados.resultados_tpidenti !== undefined) {
           var datos = json.resultados.resultados_tpidenti;

           var contador = 0;
           for (var k in datos) {
               var item = datos[k];
               var textoOpcion = "(" + item.codigo + ") " + item.descripcion;
               var tipoDocumento = {
            		   codigo: item.codigo,
            		   descripcion : item.descripcion,
            		   textoOpcion : textoOpcion
               };
               $scope.listaTipoDocumento.push(tipoDocumento);
           }//for (var k in datos) {
       }//for (var k in datos) {
   };//function cargarTipoDocumento(json) {

   function onSuccessTipoDocumento(data, status, header, config) {
       console.log("onSuccessTipoDocumento: " + data);
       cargarTipoDocumento(data);
   };

   function cargarComboTipoDocumento() {

	   var filtroTipoDocumento = {};
	   TitularesRoutingService.getTpidentiList(onSuccessTipoDocumento, onErrorRequest, filtroTipoDocumento);
       //$('#tpdocu_TI').append("<option value='" + item.codigo + "'>(" + item.codigo + ") " + item.descripcion + "</option>"
   }

   function cargarTipoSociedad(json) {
   	   $scope.listaTipoSociedad = [];
	   if (json.resultados == null) {
	        fErrorTxt(json.error, 1);
	   }
       if (json.resultados != null && json.resultados !== undefined && json.resultados.resultados_tpsocied !== undefined) {
           var datos = json.resultados.resultados_tpsocied;
           $scope.listaTipoSociedad = [];
           var contador = 0;
           for (var k in datos) {
               var item = datos[k];
               var textoOpcion = "(" + item.codigo + ") " + item.descripcion;
               var tipoSociedad = {
            		   codigo: item.codigo,
            		   descripcion : item.descripcion,
            		   textoOpcion : textoOpcion
               };
               $scope.listaTipoSociedad.push(tipoSociedad);
           }//for (var k in datos) {
       }//for (var k in datos) {
   };//function cargarTipoSociedad(json) {

   function onSuccessTipoSociedad(data, status, header, config) {
       console.log("onSuccessTipoSociedad: " + data);
       cargarTipoSociedad(data);
   };

   function cargarComboTipoSociedad() {

	   var filtroTipoSociedad = {};
	   TitularesRoutingService.getTpsociedList(onSuccessTipoSociedad, onErrorRequest, filtroTipoSociedad);
   }

   function cargarTipoTitular(json) {
   	   $scope.listaTipoTitular = [];
	   if (json.resultados == null) {
	        fErrorTxt(json.error, 1);
	   }
       if (json.resultados != null && json.resultados !== undefined && json.resultados.resultados_tptiprep !== undefined) {
           var datos = json.resultados.resultados_tptiprep;

           var contador = 0;
           for (var k in datos) {
               var item = datos[k];
               var textoOpcion = "(" + item.codigo + ") " + item.descripcion;
               var tipoTitular = {
            		   codigo: item.codigo,
            		   descripcion : item.descripcion,
            		   textoOpcion : textoOpcion
               };
               $scope.listaTipoTitular.push(tipoTitular);
           }//for (var k in datos) {
       }//for (var k in datos) {
   };//function cargarTipoTitular(json) {

   function onSuccessTipoTitular(data, status, header, config) {
       console.log("onSuccessTipoTitular: " + data);
       cargarTipoTitular(data);
   };

   function cargarComboTipoTitular() {

	   var filtroTipoTitular = {};
	   TitularesRoutingService.getTptiprepList(onSuccessTipoTitular, onErrorRequest, filtroTipoTitular);
       //$('#tptipr_TI').append("<option value='" + item.codigo + "'>(" + item.codigo + ") " + item.descripcion + "</option>"
   }

   $document.ready(function () {
       initialize();
       loadDataTableOrdenes();
       loadDataTableDesgloses();
       loadBolsaMercado();
       cargarComboNacionalidad();
       cargarComboTipoNacionalidad();
       cargarComboPaisResidencia();
       cargarComboTipoDocumento();
       cargarComboTipoSociedad();
       cargarComboTipoTitular();
   });

   $scope.LimpiarFiltros = function () {
  	 //ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
      $( "#fechaA" ).datepicker( "option", "minDate", "");
      $( "#fechaDe" ).datepicker( "option", "maxDate", "");

      //
      angular.element('input[type=text]').val('');
      angular.element('#fechaDe').val('');
      angular.element('#fechaA').val('');
      angular.element('#cdisin').val('');
      lMarcarOrdenes = true;
      lMarcarDesgloses = true;
      angular.element('#MarcarTodOrdenes').val('Marcar Todos');
      angular.element('#MarcarTodDesgloses').val('Marcar Todos');
  };

  function lookBotonesOrdenes(tipo) {
      angular.element('#MarcarTodOrdenes').css('display', 'none');
      angular.element('#MarcarPagOrdenes').css('display', 'none');
      angular.element('#ValidarDesgloses').css('display', 'none');
      if (tipo) {
          if ($scope.listaChkOrdenes.length > 0) {
              angular.element('#MarcarTodOrdenes').css('display', 'inline');
              angular.element('#MarcarPagOrdenes').css('display', 'inline');
              angular.element('#ValidarDesgloses').css('display', 'inline');
          }//if($scope.listaChk.length>0){
      }//if(tipo)
  }//function lookBotones(tipo){

  function lookBotonesDesgloses(tipo) {
      angular.element('#MarcarTodDesgloses').css('display', 'none');
      angular.element('#MarcarPagDesgloses').css('display', 'none');
      angular.element('#guardarTitulares').css('display', 'none');
      if (tipo) {
          if ($scope.listaChkOrdenes.length > 0) {
              angular.element('#MarcarTodDesgloses').css('display', 'inline');
              angular.element('#MarcarPagDesgloses').css('display', 'inline');
              angular.element('#guardarTitulares').css('display', 'inline');
          }//if($scope.listaChk.length>0){
      }//if(tipo)
  }//function lookBotones(tipo){

//Botón de ejecutar la consulta.
  $scope.cargarTabla = function () {
      ejecutadaCon = true;
      $scope.numOrden = "";
      cargarDatosConsulta();
      return false;
  };

  function getIsin(isinCompleto) {

      var guion = isinCompleto.indexOf("-");
      if (guion < 0) {
          return isinCompleto;
      } else {
          return isinCompleto.substring(0, guion);
      }
  }

  function onErrorRequest(data) {
      growl.addErrorMessage("Ocurrió un error de comunicación con el servidor, por favor inténtelo más tarde.");
      $.unblockUI();
  }

  function collapseSearchForm(){
      $('.collapser_search').parents('.title_section').next().slideToggle();
      $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
      $('.collapser_search').toggleClass('active');
      if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
          $('.collapser_search').text("Mostrar opciones de búsqueda");
      } else {
          $('.collapser_search').text("Ocultar opciones de búsqueda");
      }
  }

  function seguimientoBusqueda() {
      $scope.menBusDesgloseMQ = "";
      if ($scope.filtroConsulta.fechaDesde !== "") {
          $scope.menBusDesgloseMQ += " fecha desde: " + $scope.filtroConsulta.fechaDesde;
      }
      if ($scope.filtroConsulta.fechaHasta !== "") {
          $scope.menBusDesgloseMQ += " fecha hasta: " + $scope.filtroConsulta.fechaHasta;
      }
      if ($scope.filtroConsulta.isin !== "") {
          $scope.menBusDesgloseMQ += " isin: " + $scope.filtroConsulta.isin;
      }
  }

  // Activa la sublinea.//
  $scope.desplegarTitulares = function (fila, validado, numReferencia) {
      if (oTableDesgloses.fnIsOpen(fila)) {
          if (openTitulares[fila]) {
        	  oTableDesgloses.fnClose(fila);
              openTitulares[fila] = false;
          }
      } else {
          var aData = oTableDesgloses.fnGetData(fila);
          openDesgloses[fila] = true;
          var resultadoTitulares = tablaTitulares(oTableDesgloses, fila, validado, numReferencia);
          var a = $compile(resultadoTitulares)($scope);
          oTableDesgloses.fnOpen(fila, a);
      }
  };

  function openEditTitularForm() {

      ngDialog.open({
          template: 'template/clearing/desgloses/editarTitular.html',
          className: 'ngdialog-theme-plain custom-width custom-height-480',
          scope: $scope,
          preCloseCallback: function () {
              if ($scope.needRefresh) {
                  $scope.needRefresh = false;
                  recargarDatosPantalla();
              }
          }, controller: ['$scope', '$document', 'growl', function ($scope, $document, growl) {
                  $document.ready(function () {
                      //
                  });
              }]
      });
  }
  ;

//editar titular
  $scope.cargoInputsTitular = function (id, bolsa, ccv, codempr, codbic, distrito, domicili, idsenc, idnac, naclidad, nifbic, razonsocial, apellido2, apellido1, nombre,
		                                numOrden, pais, pasiRe, particip, poblacio, provincia, tipodoc, tipper, tiptitu, idDesgloseRecordBean, numReferencia, bEsPersona) {

      console.log("*******************");
      console.log(" cargoInputsTitular ");
      console.log("*******************");



      $scope.dialogTitle = 'Editar el titular del desglose: '+numReferencia;
      var nombreTxt = nombre;
      var apellido1Txt = apellido1;
      var apellido2Txt = apellido2;
      if (!bEsPersona){
    	  nombreTxt = razonsocial;
          apellido1Txt = "";
          apellido2Txt = "";
      }

      $scope.editarTitular = {};
      $scope.editarTitular = {id: id,
    		  				  bolsa: '0'+bolsa,
    		  				  ccv: ccv,
    		  				  codempr: codempr,
    		  				  codbic: codbic,
    		  				  distrito: distrito,
    		  				  domicili: domicili,
    		  				  idsenc: idsenc,
    		  				  idnac: idnac,
    		  				  naclidad: naclidad,
    		  			   	  nifbic: nifbic,
    		  				  razonsocial: razonsocial,
    		  				  apellido2: apellido2,
    		  				  apellido1: apellido1,
    		  				  nombre: nombre,
    		  				  apellido2Txt: apellido2Txt,
    		  				  apellido1Txt: apellido1Txt,
  		  				      nombreTxt: nombreTxt,
    		  				  numOrden: numOrden,
    		  				  pais: pais,
    		  				  pasiRe: pasiRe,
    		  				  particip: particip,
    		  				  poblacio: poblacio,
    		  				  provincia: provincia,
    		  				  tipodoc: tipodoc,
    		  				  tipper: tipper,
    		  				  tiptitu: tiptitu,
    		  				  idDesgloseRecordBean: idDesgloseRecordBean};
    	  openEditTitularForm();
  }//$scope.cargoInputs = function () {


  function tablaTitulares(Table, nTr, validado, numReferencia) {
	  var aObjTitulares = oTitulares[nTr];
	  var aData = Table.fnGetData(nTr);
	  var resultado =
          "<div border-width:0 !important;background-color:#dfdfdf; style='float:left;;margin-left: 80px;' id='div_titular_" + nTr + "'>" +
          "<table id='tablaDesglose" + nTr + "' style=margin-top:5px;'>" +
          "<tr>";



	  resultado += "</tr>";

	  if (aObjTitulares.length == 0){

		  resultado += "<td> no hay titulares para este desglose</td>";

	  }//if (aObjTitulares.length == 0){
	  else{

		  if (validado < 2){
	          resultado += "<th class='taleft' style='background-color: #000 !important;'>Acciones</th>";
	      }

		  resultado +=
	          "<th class='taleft' style='background-color: #000 !important;'>T Doc</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>NIF_BIC</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Nombre / Razon social </th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Bolsa</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>CCV</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Cod Empr</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Cod BIC</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Distrito</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Domicilio</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>ID Senc</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>ID Nac</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Nacionalidad</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Pais</th>" +
	          "<th class='taleft' style='background-color: #000 !important;'>Pais Re</th>"+
	          "<th class='taleft' style='background-color: #000 !important;'>% Participaci&oacute;n</th>"+
	          "<th class='taleft' style='background-color: #000 !important;'>Poblacio&oacute;n</th>"+
	          "<th class='taleft' style='background-color: #000 !important;'>Provincia</th>"+
	          "<th class='taleft' style='background-color: #000 !important;'>T Persona</th>"+
	          "<th class='taleft' style='background-color: #000 !important;'>T Titular</th>";

		  for (var i in aObjTitulares) {
			  var aObjTitular = aObjDesglose[i];
			  resultado += "<tr>";
			  var id= "";
			  var bolsa = "";
			  var ccv = "";
			  var codempr = "";
			  var codbic = "";
			  var distrito = "";
			  var domicili = "";
			  var idsenc = "";
			  var idnac = "";
			  var naclidad = "";
			  var nifbic = "";
			  var nomapell = "";
			  var razonsocial = "";
			  var nombre = "";
			  var apellido1 = "";
			  var apellido2 = "";
			  var numOrden = "";
			  var pais = "";
			  var pasiRe = "";
			  var particip = "";
			  var poblacio = "";
			  var provincia = "";
			  var tipodoc = "";
			  var tipper = "";
			  var tiptitu = "";
			  var idDesgloseRecordBean = "";
			  for (var j in aObjTitular) {
				  var titular = aObjTitular[j];
				  if (j == 'idTitular')
	              {
					  id = titular;
	                  continue;
	              } else
	        	  if (j == 'bolsaTitular')
	              {
	        		  bolsa = titular;
	                  continue;
	              } else
	        	  if (j == 'ccvTitular')
	              {
	        		  ccv = titular;
	                  continue;
	              } else
	        	  if (j == 'codEmprTitular')
	              {
	        		  codempr = titular;
	                  continue;
	              }else
				  if (j == 'codBICTitular')
	              {
					  codbic = titular;
	                  continue;
	              }else
				  if (j == 'distritoTitular')
	              {
					  distrito = titular;
	                  continue;
	              }else
				  if (j == 'domiciliTitular')
	              {
					  domicili = titular;
	                  continue;
	              }else
				  if (j == 'idSencTitular')
	              {
					  idsenc = titular;
	                  continue;
	              }else
				  if (j == 'indNacTitular')
	              {
					  idnac = titular;
	                  continue;
	              }else
				  if (j == 'naclidadTitular')
	              {
					  naclidad = titular;
	                  continue;
	              }else
				  if (j == 'nifBicTitular')
	              {
					  nifbic = titular;
	                  continue;
	              }else
				  if (j == 'razonSocialTitular')
	              {
					  razonsocial = titular;
	                  continue;
	              }else
				  if (j == 'apellido2Titular')
	              {
					  apellido2 = titular;
	                  continue;
	              }else
				  if (j == 'apellido1Titular')
	              {
					  apellido1 = titular;
	                  continue;
	              }else
				  if (j == 'nombreTitular')
	              {
					  nombre = titular;
	                  continue;
	              }else
				  if (j == 'numOrden')
	              {
					  numOrden = titular;
	                  continue;
	              }else
				  if (j == 'paisTitular')
	              {
					  pais = titular;
	                  continue;
	              }else
				  if (j == 'paisReTitular')
	              {
					  pasiRe = titular;
	                  continue;
	              }else
				  if (j == 'participTitular')
	              {
					  particip = titular;
	                  continue;
	              }else
				  if (j == 'poblacioTitular')
	              {
					  poblacio = titular;
	                  continue;
	              }else
				  if (j == 'provinciaTitular')
	              {
					  provincia = titular;
	                  continue;
	              }else
				  if (j == 'tipoDocTitular')
	              {
					  tipodoc = titular;
	                  continue;
	              }else
				  if (j == 'tipperTitular')
	              {
					  tipper = titular;
	                  continue;
	              }else
				  if (j == 'tipTitular')
	              {
					  tiptitu = titular;
	                  continue;
	              }else
				  if (j == 'idDesgloseRecordBeanTitular')
	              {
					  idDesgloseRecordBean = titular;
	                  continue;
	              }
			  }//for (var j in aObjTitular) {
			  resultado += "<tr>";

			  nomapell = "";
			  var bnomapell = false;
			  var bEsPersona = true;
			  if (nombre != ""){
				  nomapell = nombre;
				  bnomapell = true;
			  }
			  if (apellido1 != ""){
				  if (bnomapell){
					  nomapell += " ";
				  }
				  nomapell += apellido1;
				  bnomapell = true;
			  }
			  if (apellido2 != ""){
				  if (bnomapell){
					  nomapell += " ";
				  }
				  nomapell += apellido2;
				  bnomapell = true;
			  }
			  if (razonsocial != ""){
				  if (bnomapell){
					  nomapell += " ";
				  }
				  nomapell += razonsocial;
				  bnomapell = true;
				  bEsPersona = false;
			  }

			  if (validado < 2)
	          {
	              resultado += "<td  width=75>";

	              resultado += "<a class=\"btn\" ng-click = \"cargoInputsTitular('" + id + "','" + bolsa + "','" + ccv + "','" + codempr + "','" + codbic + "','" + distrito + "',"+
	                           "'" + domicili + "','" + idsenc + "','" + idnac + "','" + naclidad + "','" + nifbic + "','" + razonsocial + "','" + apellido2 + "',"+
	                           "'" + apellido1 + "', '"+nombre+"', '"+numOrden+"', '"+pais+"', '"+pasiRe+"', '"+particip+"', '"+poblacio+"', '"+provincia+"', '"+tipodoc+"',"+
	                           "'"+tipper+"', '"+tiptitu+"', '"+idDesgloseRecordBean+"', '"+numReferencia+"',"+bEsPersona+" ); \"><img src='img/editp.png' title='Editar'/></a>&nbsp;<a class=\"btn\" ng-click=borrarTitular('" + id + "','" + nifbic + "');><img src='img/del.png'  title='Eliminar'/></a>";
	              resultado += "</td>";

	          }//if (validado < 2)



			  resultado += "<td  width=75>" + tipodoc + "</td>";
	          resultado += "<td  width=75>" + nifbic + "</td>";
	          resultado += "<td  width=100>" + nomapell + "</td>";
	          resultado += "<td  width=100>" + bolsa + "</td>";
	          resultado += "<td  width=75>" + ccv + "</td>";
	          resultado += "<td  width=120>" + codempr + "</td>";
	          resultado += "<td  width=75>" + codbic + "</td>";
	          resultado += "<td  width=75>" + distrito + "</td>";
	          resultado += "<td  width=75>" + domicili + "</td>";
	          resultado += "<td  width=50>" + idsenc + "</td>";
	          resultado += "<td  width=50>" + idnac + "</td>";
	          resultado += "<td  width=75>" + naclidad + "</td>";
	          resultado += "<td  width=75>" + pais + "</td>";
	          resultado += "<td  width=50>" + pasiRe + "</td>";
	          resultado += "<td  width=50>" + particip + "</td>";
	          resultado += "<td  width=75>" + poblacio + "</td>";
	          resultado += "<td  width=75>" + provincia + "</td>";
	          resultado += "<td  width=50>" + tipper + "</td>";
	          resultado += "<td  width=50>" + tiptitu + "</td>";

	          resultado += "</tr>";
		  }
	  }//for (var i in aObjTitulares) {

	  resultado += "</table></div>";

	  return resultado;

  }//function tablaTitulares(Table, nTr, validado) {


  function tablaDesgloses(nTr, validado, numOrden) {

	  var tbl = $("#tblDesglosesMQ > tbody");
      $(tbl).html("");
      oTableDesgloses.fnClearTable();
      var aObjDesglose = oDesgloses[nTr];
      oTitulares = [];
      var contador = 0;
      for (var i in aObjDesglose) {
          var aDesglose = aObjDesglose[i];
          oTitulares[contador] = aDesglose.listaTitulares;
          $scope.listaChkDesglosesValidado[i] = validado;
          $scope.listaChkDesglosesNumOperacion[i] = numOrden;
          var id = "";
          var fecha = "";
          var fechaText = "";
          var numReferencia = "";
          var sentido = "";
          var sentidoText = "";
          var bolsa = "";
          var isin = "";
          var descripcion = "";
          var titulos = "";
          var precio = "";
          var tipoSaldo = "";
          var tipoSaldoText = "";
          var ccv = "";
          var titular = "";
          var entidad = "";
          var tipoCambio = "";
          var tipoCambioText = "";
          var numOrden = "";
          var bolsaDescripcion = "";
          for (var j in aDesglose) {
              var desglose = aDesglose[j];
              if (j == 'numOrden')
              {
              	numOrden = desglose;
                continue;
              } else
              if (j == 'id')
              {
                  id = desglose;
                  continue;
              } else
              if (j == 'fejecucion')
              {
                  fecha = desglose;
                  if (desglose !== null && desglose !== undefined) {
                      desglose = desglose.substr(8, 2) + "/" + desglose.substr(5, 2) + "/" + desglose.substr(0, 4);
                  }
                  fechaText = desglose;
              } else
              if (j == 'numReferencia')
              {
                  numReferencia = desglose;
              } else
              if (j == 'tipoOperacion')
              {
                  var tipoOperacion = "Compra";
                  if (desglose == '2')
                  {
                      tipoOperacion = "Venta";
                  }
                  sentido = desglose;
                  sentidoText = tipoOperacion;
              } else
              if (j == 'bolsa')
              {
                  bolsa = desglose;
              } else
              if (j == 'isin')
              {
                  isin = desglose;
              } else
              if (j == 'desIsin')
              {
                  descripcion = desglose;
              } else
              if (j == 'titulos')
              {
                  titulos = desglose;
              } else
              if (j == 'precio')
              {
                  precio = desglose;
              } else
              if (j == 'tipoSaldo')
              {
                  var aux = "Propio";
                  if (desglose === 'T')
                  {
                      aux = "Terceros";
                  }
                  tipoSaldoText = aux;
                  tipoSaldo = desglose;
              } else
              if (j == 'ccv')
              {
                  ccv = desglose;
              } else
              if (j == 'titular')
              {
                  titular = desglose;
              } else
              if (j == 'entidad')
              {
                  entidad = desglose;
              } else
              if (j == 'tipoCambio')
              {
                  var aux = "Porcentaje";
                  if (desglose == '2')
                  {
                      aux = "Efectivo";
                  }
                  tipoCambioText = aux;
                  tipoCambio = desglose;
              }else
              if (j == 'bolsaDescripcion')
              {
              	bolsaDescripcion = desglose;
              }
          }//for (var j in aDesglose) {

          var acciones = "";
          if (validado < 2){

        	  acciones = "<span>"
              + " <img src=\"img/editp.png\" height=\"" + IMG_SIZE + "\" title=\"Editar\" ng-click=\"cargoInputs('" + id + "','" + fechaText + "','" + numReferencia + "','" + sentido + "','" + bolsa + "','" + isin + "','" + descripcion + "','" + titulos + "','" + precio + "','" + tipoSaldo + "','" + ccv + "','" + titular + "','" + entidad + "','" + tipoCambio + "', '"+numOrden+"' );\">"
              + " <img src=\"img/del.png\" height=\"" + IMG_SIZE + "\" title=\"Eliminar\" ng-click=\"borrarDesglose('" + id + "','" + numReferencia + "');\">"
              + " <img src=\"img/addUser.png\" height=\"" + IMG_SIZE + "\" title=\"Alta titular\" ng-click=\"altaTitularesEspeciales('" + id + "','" + numOrden + "', '"+numReferencia+"');\">"
              + " <img src=\"img/scont.gif\" height=\"" + IMG_SIZE + "\" title=\"Crear titulares\" ng-click=\"crearTitularesEspeciales('" + id + "', '"+numReferencia+"');\">"
              + "</span>"
          }

          var classDesplegar = "exito";
          if (validado == 0)
          {
        	  classDesplegar = "error";
          }

          var numOrden = "";
          numOrden = "ng-click=\"desplegarTitulares(" + contador + "," + validado +",'" + numReferencia +"' )\" ";
          numOrden = numOrden + " style=\"cursor: pointer; \"";
          numReferencia = "<div class='taleft "+classDesplegar+"' " + numOrden + " >" + numReferencia + "</div> "
          contador++;



          var rowIndex = oTableDesgloses.fnAddData([
                                                  acciones,         // 1 acciones
                                                  fechaText,        // 2 fecha.
                                                  numReferencia,    // 3 N. referencia.
                                                  sentidoText,      // 4 Sentido.
                                                  bolsaDescripcion, // 5 Bolsa
                                                  isin,             // 6 Isin
                                                  titulos,          // 7 titulos
                                                  precio,           // 8 Precio
                                                  tipoSaldoText,    // 9 Tipo Saldo
                                                  ccv,              //10 ccv
                                                  titular,          //11 titular
                                                  entidad,          //12 Entidad liquidadora
                                                  tipoCambioText    //13 Tipo cambio
                                              ], false);

                                              var nTr = oTableDesgloses.fnSettings().aoData[rowIndex[0]].nTr;

      }
      openTitulares = new Array(contador, false);


  }

//Activa la sublinea.//
  $scope.desplegar = function (fila, validado, numOrden) {
	  $scope.numOrden = numOrden;
	  if (oTableDesgloses !== undefined && oTableDesgloses !== null) {
    	  oTableDesgloses.fnDestroy();
      }
	  loadDataTableDesgloses();
      tablaDesgloses(fila, validado,numOrden);
  };


  function populateOld(json) {
      if (json.resultados != null && json.resultados !== undefined && json.resultados.desglosesList !== undefined) {

          var item = null;
          var datos = json.resultados.desglosesList;

          var tbl = $("#tblOrdenesMQ > tbody");
          $(tbl).html("");
          oTableOrdenes.fnClearTable();

          var contador = 0;
          for (var k in datos) {
              item = datos[k];

              var estilo = 'style="display:none;"';
              if (item.validado == 1)
              {
                  estilo = 'style="display:inline;"';
              }//if (data.validado == 1)
              $scope.listaChkOrdenesValidado[k] = item.validado;
              $scope.listaChkOrdenesNumOperacion[k] = item.numOrden;
              var idName = "check_" + contador;
              $scope.listaChkOrdenes[k] = false;
              var txtMarcar = '<input ng-model="listaChkOrdenes[' + k + ']"  ' + estilo + ' type="checkbox" class=orden' + (contador) + '  id="' + idName + '" name="' + idName + '"bvalue="N" class="editor-active">';

              var classDesplegar = "exito";
              if (item.validado == 0)
              {
            	  classDesplegar = "error";
              }

              var numOrden = "";
              numOrden = "ng-click=\"desplegar(" + (contador) + "," + item.validado + ", '"+item.numOrden+"' )\" ";
              numOrden = numOrden + " style=\"cursor: pointer; \"";
              numOrden = "<div class='taleft "+classDesplegar+"' " + numOrden + " >" + item.numOrden + "</div> "
              oDesgloses.push(item.listaDesgloses);
              contador++;

              var classFejecucion = "error";
              if (item.bokFechaEjecucion == true)
              {
                  classFejecucion = "exito";
              }
              var fejecucion = item.fejecucion;
              if (fejecucion !== null && fejecucion !== undefined) {
                  fejecucion = fejecucion.substr(8, 2) + "/" + fejecucion.substr(5, 2) + "/" + fejecucion.substr(0, 4);
              }
              var fejecucionText = "<div class='" + classFejecucion + "' id='fejecuion_" + contador + "' >" + fejecucion + "</div> ";

              fejecucion = item.fejecucion;
              fejecucion = fejecucion.substr(0, 4) + fejecucion.substr(5, 2) + fejecucion.substr(8, 2);

              var tipoOperacion = "Compra";
              var tOperacion = 2;
              if (item.tipoOperacion == '2')
              {
                  tipoOperacion = "Venta";
              } else
              {
                  tOperacion = 1;
              }

              var classTipoOperacion = "error";
              if (item.bokTipoOperacion == true)
              {
                  classTipoOperacion = "exito";
              }
              tipoOperacion = "<div class='" + classTipoOperacion + "' id='tipoOperacion_" + contador + "' >" + tipoOperacion + "</div> ";

              var classTitulos = "error";
              if (item.titulos === item.titulosSV)
              {
                  classTitulos = "exito";
              }
              var titulos = "<div class='" + classTitulos + "' id='titulos_" + contador + "' >" + item.titulos + "</div> ";
              var nominal = "<div class='taleft' id='nominal_" + contador + "' >" + item.nominal + "</div> ";

              var classIsin = "error";

              if (item.bokIsin == true)
              {
                  classIsin = "exito";
              }
              var isin = "<div class='" + classIsin + "' id='isin_" + contador + "' >" + item.isin + "</div> ";

              var classBolsa = "error";
              if (item.bokBolsa == true)
              {
                  classBolsa = "exito";
              }
              var bolsa = "<div class='" + classBolsa + "' id='bolsa_" + contador + "' >" + item.bolsaDescripcion + "</div> ";

              var tipoSaldo = "Propio";
              if (item.tipoSaldo === 'T')
              {
                  tipoSaldo = "Terceros";
              }

              var classTipoSaldo = "error";
              if (item.bokTipoSaldo == true)
              {
                  classTipoSaldo = "exito";
              }
              tipoSaldo = "<div class='" + classTipoSaldo + "' id='tipoSaldo_" + contador + "' >" + tipoSaldo + "</div> ";

              var tipoCambio = "Porcentaje";
              if (item.tipoCambio == '2')
              {
                  tipoCambio = "Efectivo";
              }
              var classTipoCambio = "error";
              if (item.bokTipoCambio == true)
              {
                  classTipoCambio = "exito";
              }
              tipoCambio = "<div class='" + classTipoCambio + "' id='tipoCambio_" + contador + "' >" + tipoCambio + "</div> ";

              var validado = "OK";
              if (item.validado == '0')
              {
                  validado = "KO";
              }
              validado = "<div class='taleft' id=val_'" + contador + "' >" + validado + "</div> ";

              var estilo = 'style=\"display:none;\"';
              var acciones = "";
              if (item.validado == '0')
              {
                  estilo = 'style="display:inline;"';
                  acciones = "ng-click=\"cargoDesglose(" + (contador - 1) + ")\" ";
                  acciones = acciones + " style=\"cursor: pointer; color: #ff0000;\"";
                  acciones = "<div class='taleft' " + acciones + " ><img src=\"img/addParam.png\"  title=\"Alta desglose\" /></div> ";
              }//if (data.validado === 1)
              desgloses[contador - 1] = {
                  tipoReg: "40",
                  refre: '',
                  titulos: 0,
                  nominej: 0,
                  entidad: item.entidad,
                  empConVal: item.empConVal,
                  numCtoOficina: item.numCtoOficina,
                  numCtrVal: item.numCtrVal,
                  numOrden: item.numOrden,
                  codsv: item.codsv,
                  fejecucion: fejecucion,
                  bolsa: item.bolsa,
                  isin: item.isin,
                  tipoOperacion: tOperacion,
                  tipoCambio: item.tipoCambio,
                  tipoSaldo: item.tipoSaldo,
                  cambioOperacion: item.cambioOperacion,
                  denominacionValor: item.denominacionValor,
                  nif_bic: item.titular,
                  refMod: item.refMod,
                  segmento: item.segmento,
                  ordCom: item.ordCom,
                  bolsaDescripcion: item.bolsaDescripcion
              }//datos de la operacion comunes a todos los desgloses.




              var rowIndex = oTableOrdenes.fnAddData([
                  txtMarcar,      // 0 lista de checks.
                  acciones,       // 1 acciones
                  numOrden,       // 2 numero de orden.
                  item.codsv,     // 3 Cod. SV.
                  fejecucionText, // 4 F ejecución.
                  tipoOperacion,  // 5 Tipo operación
                  titulos,        // 6 titulos
                  item.titulosSV, // 7 titulos
                  nominal,        // 8 nominal
                  isin,           // 9 isin
                  item.desIsin,   //10 descripcion isin
                  bolsa,          //11 bolsa de negociación
                  tipoSaldo,      //12 tipo de saldo
                  item.entidad,   //13 entidad liquidadora
                  tipoCambio,     //14 tipo de cambio
                  validado,       //15 validado
                  item.procesado  //16 procesado

              ], false);

              var nTr = oTableOrdenes.fnSettings().aoData[rowIndex[0]].nTr;
              $('td', nTr)[1].setAttribute('style', 'cursor: pointer; color: #ff0000;');
              //$('td', nTr)[2].setAttribute('style', 'cursor: pointer; color: #ff0000;');


          }
          oTableOrdenes.fnDraw();

          lookBotonesOrdenes(true);
          lookBotonesDesgloses(false);
          $.unblockUI();
          collapseSearchForm();
          seguimientoBusqueda();
          openDesgloses = new Array(contador, false);

      } else {

          if (ejecutadaCon) {
              var tipo = 2;
              var errorTxt = json.error;
              if (errorTxt == undefined) {
                  errorTxt = "Error indeterminado. Consulte con el servicio técnico.";
                  tipo = 1;
              }
              fErrorTxt(json.error, tipo);
              $.unblockUI();
              $('.collapser_search').parents('.title_section').next().slideToggle();
              $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
              $('.collapser_search').toggleClass('active');
              if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
                  $('.collapser_search').text("Mostrar opciones de búsqueda");
              } else {
                  $('.collapser_search').text("Ocultar opciones de búsqueda");
              }
          }


      }

  }
  ;//populateOld

  function onSuccessSinPaginacion(data, status, header, config) {
      console.log("onSuccessSinPaginacion: " + data);
      populateOld(data);
  }
  ;

  function onSuccessCountRequest(data, status, header, config) {
      console.log("Número de registros: " + data.resultados.registros);
      if (data.error !== undefined && data.error !== null && data.error !== "") {
          growl.addErrorMessage(data.error);
      } else {
          oDesgloses = [];
          $scope.listaChkOrdenes = [];
          $scope.listaChkOrdenesValidado = [];
          $scope.listaChkOrdenesNumOperacion = [];
          collapseSearchForm();
          seguimientoBusqueda();
          desgloses = [];
          if (oTableOrdenes !== undefined && oTableOrdenes !== null) {
        	  oTableOrdenes.fnDestroy();
          }
          loadDataTableOrdenes();
          DesgloseMQService.getDesglosesSinPaginacion(onSuccessSinPaginacion, onErrorRequest, $scope.filtroConsulta);


      }

  }
  ;

  function recargarDatosPantalla()
  {
      inicializarLoading();

      DesgloseMQService.getDesglosesCount(onSuccessCountRequest, onErrorRequest, $scope.filtroConsulta);
  }

  function cargarDatosConsulta() {

      console.log("cargarDatosConsulta");

      var fechaDe = transformaFechaInv(angular.element("#fechaDe").val());
      var fechaA = transformaFechaInv($("#fechaA").val());
      var isinCompleto = angular.element('#cdisin').val();
      var cdIsin = getIsin(isinCompleto);

      $scope.filtroConsulta = {
          "fechaDesde": fechaDe,
          "fechaHasta": fechaA,
          "isin": cdIsin
      };

      $scope.listaChkDesgloses = [];
      $scope.listaChkDesglosesValidado = [];
      $scope.listaChkDesglosesNumOperacion = [];
      oTableDesgloses.fnClearTable();

      recargarDatosPantalla();

  }
  ;

//marca/desmarca los registros de la pagina
  function toggleCheckBoxesPagOrdenes(b) {
      var inputs = angular.element('input[type=checkbox]');
      var cb = null;
      for (var i = 0; i < inputs.length; i++) {
          cb = inputs[i];
          var clase = $(cb).attr('class');
          var n = clase.indexOf(" ");
          var fila = clase.substr(0, n);
          n = fila.indexOf('orden');
          if(n != -1){
        	  fila = clase.substr(n, fila.length);
        	  var validado = $scope.listaChkOrdenesValidado[fila];
	          if (validado == 1)
	          {
	              $scope.listaChkOrdenes[fila] = b;
	          }
          }

      }

  }
  ;

  function toggleCheckBoxesOrdenes(b) {

      for (var i = 0; i < $scope.listaChkOrdenes.length; i++) {
          var validado = $scope.listaChkOrdenesValidado[i];
          if (validado == 1)
          {
              $scope.listaChkOrdenes[i] = b;
          }
      }
  }
  ;

  function toggleCheckBoxesPagDesgloses(b) {
      var inputs = angular.element('input[type=checkbox]');
      var cb = null;
      for (var i = 0; i < inputs.length; i++) {
          cb = inputs[i];
          var clase = $(cb).attr('class');
          var n = clase.indexOf(" ");
          var fila = clase.substr(0, n);
          n = fila.indexOf('desglose');
          if(n != -1){
        	  fila = clase.substr(n, fila.length);
        	  var validado = $scope.listaChkDesglosesValidado[fila];
	          if (validado == 1)
	          {
	              $scope.listaChkOrdenes[fila] = b;
	          }
          }

      }

  }
  ;

  function toggleCheckBoxesDesgloses(b) {

      for (var i = 0; i < $scope.listaChkDesgloses.length; i++) {
          var validado = $scope.listaChkDesglosesValidado[i];
          if (validado == 1)
          {
              $scope.listaChkDesgloses[i] = b;
          }
      }
  }
  ;

//Boton de marcar todos
  $scope.selececionarCheckOrdenes = function () {
      if (lMarcarOrdenes) {
    	  lMarcarOrdenes = false;
          angular.element("#MarcarTodOrdenes").val("Desmarcar Todos");
          toggleCheckBoxesOrdenes(true);
      } else {
    	  lMarcarOrdenes = true;
          angular.element("#MarcarTodOrdenes").val("Marcar Todos");
          toggleCheckBoxesOrdenes(false);
      }
  };

  //Boton de marcar página
  $scope.selececionarCheckPagOrdenes = function () {

      var valor = alPaginasOrdenes[pagina];

      toggleCheckBoxesPagOrdenes(valor);
      if (valor) {
          angular.element("#MarcarPagOrdenes").val("Desmarcar Página");
          alPaginasOrdenes[paginaOrdenes] = false;
      } else {
          angular.element("#MarcarPagOrdenes").val("Marcar Página");
          alPaginasOrdenes[paginaOrdenes] = true;
      }
  };

//Boton de marcar todos
  $scope.selececionarCheckDesgloses = function () {
      if (lMarcarDesgloses) {
          lMarcarDesgloses = false;
          angular.element("#MarcarTodDesgloses").val("Desmarcar Todos");
          toggleCheckBoxesDesgloses(true);
      } else {
          lMarcarDesgloses = true;
          angular.element("#MarcarTodDesgloses").val("Marcar Todos");
          toggleCheckBoxesDesgloses(false);
      }
  };

  //Boton de marcar página
  $scope.selececionarCheckPagDesgloses = function () {

      var valor = alPaginasDesgloses[pagina];

      toggleCheckBoxesPagDesgloses(valor);
      if (valor) {
          angular.element("#MarcarPagDesgloses").val("Desmarcar Página");
          alPaginasDesgloses[paginaDesgloses] = false;
      } else {
          angular.element("#MarcarPagDesgloses").val("Marcar Página");
          alPaginasDesgloses[paginaDesgloses] = true;
      }
  };

  function onSuccessValidado(json, status, headers, config) {
      if (SHOW_LOG)
          console.log("validados las operaciones seleccionadas ...");
      var datos = {};
      if (json !== undefined &&
              json.resultados !== undefined &&
              json.resultados !== null) {
          if (json.error !== null) {
              growl.addErrorMessage(json.error);
          } else {
              recargarDatosPantalla();
          }
      } else
      {
          if (json !== undefined && json.error !== null) {
              if (SHOW_LOG)
                  console.log("error al validar las operaciones ...");
              growl.addErrorMessage(json.error);
          }
      }
  }
  ;

//validar desgloses
  $scope.validarDesgloses = function () {

      console.log("*******************");
      console.log("validar desgloses");
      console.log("*******************");

      $scope.listaValidar = [];

      var aData = oTableOrdenes.fnGetData();
      var contador = 0;
      var input;
      var idName;
      var inputSel;
      for (var i = 0, j = aData.length; i < j; i++) {
          if ($scope.listaChkOrdenes[i] == true) {
              $scope.listaValidar.push($scope.listaChkOrdenesNumOperacion[i]);
          }
      }

      if ($scope.listaValidar.length == 0) {
      	growl.addErrorMessage("Debe seleccionar al menos uno de las operaciones.");
          return;
      }
      lookBotonesOrdenes(false);
      var filtroValidar = JSON.stringify($scope.listaValidar);

      var data = "{\"service\" :\"" + fsService + "\", \"action\"  :\"marcarValidado\"";
      data += ", \"list\" :" + filtroValidar;
      data += "}";

      JSON.stringify(data);
      DesgloseMQService.marcarValidado(onSuccessValidado, onErrorRequest, data);

  };//validarDesgloses = function () {

  function openAltaForm(desglose) {
      $scope.dialogTitle = 'Alta Desglose orden ' + desglose.numOrden;
      ngDialog.open({
          template: 'template/conciliacion/desgloses/altaDesglose.html',
          className: 'ngdialog-theme-plain custom-width custom-height-360',
          scope: $scope,
          preCloseCallback: function () {
              if ($scope.needRefresh) {
                  $scope.needRefresh = false;
                  recargarDatosPantalla();
              }
          }
      });
  }
  ;

  function successAltaTitular(json, status, headers, config) {
      if (SHOW_LOG)
          console.log("titular creado...");
      var datos = {};
      if (json !== undefined &&
              json.resultados !== undefined &&
              json.resultados !== null) {
          if (json.error !== null) {
              growl.addErrorMessage(json.error);
          } else {
              $scope.needRefresh = true;
              ngDialog.closeAll();
          }
      } else
      {
          if (json !== undefined && json.error !== null) {
              if (SHOW_LOG)
                  console.log("Error desglose creado ...");
              growl.addErrorMessage(json.error);
          }
      }
  }
  ;

  function altaTitular()
  {
	  var filtro = {
          bolsa: $scope.altaTitular.bolsa,
          ccv  : $scope.altaTitular.ccv,
          codempr: $scope.altaTitular.codempr,
          codbic : $scope.altaTitular.codbic,
          distrito: $scope.altaTitular.distrito,
          domicili: $scope.altaTitular.domicili,
          idsenc  : $scope.altaTitular.idsenc,
          nidnac   : $scope.altaTitular.idnac,
          naclidad: $scope.altaTitular.naclidad,
          nifbic  : $scope.altaTitular.nifbic,
          nomapell: $scope.altaTitular.nomapell,
          numOrden: $scope.altaTitular.numOrden,
          pais    : $scope.altaTitular.pais,
          pasiRe  : $scope.altaTitular.pasiRe,
          particip: $scope.altaTitular.particip,
          poblacio: $scope.altaTitular.poblacio,
          provincia: $scope.altaTitular.provincia,
          tipodoc  : $scope.altaTitular.tipodoc,
          tipper   : $scope.altaTitular.tipper,
          tiptitu  : $scope.altaTitular.tiptitu,
          idDesgloseRecordBean : $scope.altaTitular.idDesgloseRecordBean
      };
      DesgloseMQService.altaTitular(successAltaTitular, showError, filtro);
  }
  ;

  $scope.alta_Titular = function () {

      $scope.altaTitular.tipodoc = angular.element("#tpdocu_TI").val();
      $scope.altaTitular.nifbic  = angular.element("#nif_TI").val();
      var nombre = angular.element("#nombre_TI").val();
      var apellido1 = angular.element("#apellido1_TI").val();
      var apellido2 = angular.element("#apellido2_TI").val();
      var nomapellido = "";
      var bNombreApellido = false;
      if (nombre.trim() !== ""){
    	  nomapellido = nombre.trim();
    	  bNombreApellido = true;
      }
      if (apellido1.trim() !== ""){
    	  if (bNombreApellido){
    		  nomapellido += " * ";
    	  }
    	  nomapellido += apellido1.trim();
    	  bNombreApellido = true;
      }
      if (apellido2.trim() !== ""){
    	  if (bNombreApellido){
    		  nomapellido += " * ";
    	  }
    	  nomapellido += apellido2.trim();
    	  bNombreApellido = true;
      }
      $scope.altaTitular.nomapell  = nomapellido;
      $scope.altaTitular.bolsa     = angular.element("#bolsa_TI").val();
      $scope.altaTitular.ccv       = angular.element("#ccv_TI").val();
      $scope.altaTitular.codempr   = angular.element("#codempr_TI").val();
      $scope.altaTitular.codbic    = angular.element("#codbic_TI").val();
      $scope.altaTitular.distrito  = angular.element("#cdpost_TI").val();
      $scope.altaTitular.domicili  = angular.element("#nbdomi_TI").val();
      $scope.altaTitular.idsenc    = angular.element("#idsenc_TI").val();
      $scope.altaTitular.idnac     = angular.element("#tpnaci_TI").val();
      $scope.altaTitular.naclidad  = angular.element("#cdnaci_TI").val();
      $scope.altaTitular.pais      = angular.element("#pais_TI").val();
      $scope.altaTitular.pasiRe    = angular.element("#paires_TI").val();
      $scope.altaTitular.particip  = angular.element("#partic_TI").val();
      $scope.altaTitular.poblacio  = angular.element("#ciudad_TI").val();
      $scope.altaTitular.provincia = angular.element("#provin_TI").val();
      $scope.altaTitular.tipper    = angular.element("#tpsoci_TI").val();
      $scope.altaTitular.tiptitu   = angular.element("#tptipr_TI").val();
      altaTitular();
  };

  function openAltaTitularForm(numOrden, numReferencia) {
      $scope.dialogTitle = 'Alta titular para la orden ' + numOrden +' y desglose '+numReferencia;
      ngDialog.open({
    	  template: 'template/clearing/desgloses/altaTitular.html',
          className: 'ngdialog-theme-plain custom-width custom-height-480',
          scope: $scope,
          preCloseCallback: function () {
              if ($scope.needRefresh) {
                  $scope.needRefresh = false;
                  recargarDatosPantalla();
              }
          }
      });
  }
  ;

  //alta titular
  $scope.altaTitularesEspeciales = function (id, numOrden, numReferencia) {

	  console.log("*******************");
      console.log("alta titular");
      console.log("*******************");
      $scope.altaTitular = {};
      $scope.altaTitular = {bolsa: "",
    		  				ccv: "",
    		  				codempr: "",
    		  				codbic: "",
    		  				distrito: "",
    		  				domicili: "",
    		  				idsenc: "",
    		  				idnac: "",
    		  				naclidad: "",
    		  			   	nifbic: "",
    		  				razonsocial: "",
    		  				apellido2: "",
    		  				apellido1: "",
    		  				nombre: "",
    		  				apellido2Txt: "",
    		  				apellido1Txt: "",
  		  				    nombreTxt: "",
    		  				numOrden: numOrden,
    		  				pais: "",
    		  				pasiRe: "",
    		  				particip: "",
    		  				poblacio: "",
    		  				provincia: "",
    		  				tipodoc: "",
    		  				tipper: "",
    		  				tiptitu: "",
    		  				idDesgloseRecordBean: id};
      openAltaTitularForm(numOrden, numReferencia);

  }//$scope.altaTitularesEspeciales = function (id, numOrden) {

  //alta desgloses
  $scope.cargoDesglose = function (indice) {

      console.log("*******************");
      console.log("cargar desglose");
      console.log("*******************");

      var desglose = desgloses[indice];

      $scope.dialogTitle = 'Alta Parametrización';
      $scope.altaDesglose = desglose;
      openAltaForm(desglose);

  };//$scope.cargoDesglose = function (indice) {

  function successAltaDesglose(json, status, headers, config) {
      if (SHOW_LOG)
          console.log("desglose creado...");
      var datos = {};
      if (json !== undefined &&
              json.resultados !== undefined &&
              json.resultados !== null) {
          if (json.error !== null) {
              growl.addErrorMessage(json.error);
          } else {
              $scope.needRefresh = true;
              ngDialog.closeAll();
          }
      } else
      {
          if (json !== undefined && json.error !== null) {
              if (SHOW_LOG)
                  console.log("Error desglose creado ...");
              growl.addErrorMessage(json.error);
          }
      }
  }
  ;



  function successEditarTitular(json, status, headers, config) {
      if (SHOW_LOG)
          console.log("titular editado...");
      var datos = {};
      if (json !== undefined &&
              json.resultados !== undefined &&
              json.resultados !== null) {
          if (json.error !== null) {
              growl.addErrorMessage(json.error);
          } else {
              $scope.needRefresh = true;
              ngDialog.closeAll();
          }
      } else
      {
          if (json !== undefined && json.error !== null) {
              if (SHOW_LOG)
                  console.log("Error editando desglose ...");
              growl.addErrorMessage(json.error);
          }
      }
  }
  ;

  function successEditarDesglose(json, status, headers, config) {
      if (SHOW_LOG)
          console.log("desglose editado...");
      var datos = {};
      if (json !== undefined &&
              json.resultados !== undefined &&
              json.resultados !== null) {
          if (json.error !== null) {
              growl.addErrorMessage(json.error);
          } else {
              $scope.needRefresh = true;
              ngDialog.closeAll();
          }
      } else
      {
          if (json !== undefined && json.error !== null) {
              if (SHOW_LOG)
                  console.log("Error editando desglose ...");
              growl.addErrorMessage(json.error);
          }
      }
  }
  ;



  function successBorrarTitular(json, status, headers, config) {
      if (SHOW_LOG)
          console.log("titular borrado ...");
      var datos = {};
      if (json !== undefined &&
              json.resultados !== undefined &&
              json.resultados !== null) {
          if (json.error !== null) {
              growl.addErrorMessage(json.error);
          } else {
              recargarDatosPantalla();
          }
      } else
      {
          if (json !== undefined && json.error !== null) {
              if (SHOW_LOG)
                  console.log("Error borrando desglose ...");
              growl.addErrorMessage(json.error);
          }
      }
  }
  ;

  function successBorrarDesglose(json, status, headers, config) {
      if (SHOW_LOG)
          console.log("desglose borrado ...");
      var datos = {};
      if (json !== undefined &&
              json.resultados !== undefined &&
              json.resultados !== null) {
          if (json.error !== null) {
              growl.addErrorMessage(json.error);
          } else {
              recargarDatosPantalla();
          }
      } else
      {
          if (json !== undefined && json.error !== null) {
              if (SHOW_LOG)
                  console.log("Error borrando desglose ...");
              growl.addErrorMessage(json.error);
          }
      }
  }
  ;

  function crearDesglose()
  {
      var filtro = {tipoReg: $scope.altaDesglose.tipoReg,
          refre: $scope.altaDesglose.refre,
          titulos: $scope.altaDesglose.titulos,
          nominej: $scope.altaDesglose.nominej,
          entidad: $scope.altaDesglose.entidad,
          empConVal: $scope.altaDesglose.empConVal,
          numCtoOficina: $scope.altaDesglose.numCtoOficina,
          numCtrVal: $scope.altaDesglose.numCtrVal,
          numOrden: $scope.altaDesglose.numOrden,
          codsv: $scope.altaDesglose.codsv,
          fejecucion: $scope.altaDesglose.fejecucion,
          bolsa: $scope.altaDesglose.bolsa,
          isin: $scope.altaDesglose.isin,
          tipoOperacion: $scope.altaDesglose.tipoOperacion,
          cambioOperacion: $scope.altaDesglose.cambioOperacion,
          nif_bic: $scope.altaDesglose.nif_bic,
          tipoSaldo: $scope.altaDesglose.tipoSaldo,
          refMod: $scope.altaDesglose.refMod,
          segmento: $scope.altaDesglose.segmento,
          ordCom: $scope.altaDesglose.ordCom,
          tipoCambio: $scope.altaDesglose.tipoCambio};

      DesgloseMQService.altaDesglose(successAltaDesglose, showError, filtro);
  }
  ;

  $scope.alta_Desglose = function () {

      $scope.altaDesglose.refre = angular.element("#refre").val();
      $scope.altaDesglose.titulos = angular.element("#titulos").val();
      $scope.altaDesglose.nominej = angular.element("#nominej").val();
      $scope.altaDesglose.nif_bic = angular.element("#titular").val();
      $scope.altaDesglose.refMod = angular.element("#refMod").val();
      var efectivoPattern = new RegExp(/^\d{1,13}\b.?\d{0,2}?$/);
      var titulosPattern = new RegExp(/^\d{1,11}\b.?\d{0,0}?$/);

      var titulos = parseFloat($scope.altaDesglose.titulos);
      var efectivo = parseFloat($scope.altaDesglose.nominej);

      if (!titulosPattern.test($scope.altaDesglose.titulos))
      {
          growl.addErrorMessage("los titulos no tienen el formato correcto");
      } else if (!efectivoPattern.test($scope.altaDesglose.nominej))
      {
          growl.addErrorMessage("el efectivo no tiene el formato correcto");
      } else
      {
          $scope.altaDesglose.cambioOperacion = $scope.altaDesglose.nominej / $scope.altaDesglose.titulos;
          crearDesglose();
      }

  };

  function editarDesglose()
  {
      var fechaEjecucion = transformaFechaInv($scope.editarDesglose.fecha);
      var filtro = {
          id: $scope.editarDesglose.id,
          refre: $scope.editarDesglose.numReferencia,
          fejecucion: fechaEjecucion,
          tipoOperacion: $scope.editarDesglose.sentido,
          bolsa: $scope.editarDesglose.bolsa,
          isin: $scope.editarDesglose.isin,
          titulos: $scope.editarDesglose.titulos,
          nominej: $scope.editarDesglose.nominej,
          cambioOperacion: $scope.editarDesglose.precio,
          tipoSaldo: $scope.editarDesglose.tipoSaldo,
          ccv: $scope.editarDesglose.ccv,
          nif_bic: $scope.editarDesglose.titular,
          entidad: $scope.editarDesglose.entidad,
          tipoCambio: $scope.editarDesglose.tipoCambio
      };

      DesgloseMQService.editarDesglose(successEditarDesglose, showError, filtro);
  }
  ;

  function editarTitular()
  {
	  var filtro = {
          id: $scope.editarTitular.id,
          bolsa: $scope.editarTitular.bolsa,
          ccv  : $scope.editarTitular.ccv,
          codempr: $scope.editarTitular.codempr,
          codbic : $scope.editarTitular.codbic,
          distrito: $scope.editarTitular.distrito,
          domicili: $scope.editarTitular.domicili,
          idsenc  : $scope.editarTitular.idsenc,
          nidnac   : $scope.editarTitular.idnac,
          naclidad: $scope.editarTitular.naclidad,
          nifbic  : $scope.editarTitular.nifbic,
          nomapell: $scope.editarTitular.nomapell,
          pais    : $scope.editarTitular.pais,
          pasiRe  : $scope.editarTitular.pasiRe,
          particip: $scope.editarTitular.particip,
          poblacio: $scope.editarTitular.poblacio,
          provincia: $scope.editarTitular.provincia,
          tipodoc  : $scope.editarTitular.tipodoc,
          tipper   : $scope.editarTitular.tipper,
          tiptitu  : $scope.editarTitular.tiptitu,
          idDesgloseRecordBean : $scope.editarTitular.idDesgloseRecordBean
      };
      DesgloseMQService.editarTitular(successEditarTitular, showError, filtro);
  }
  ;

  $scope.editar_Titular = function () {

      $scope.editarTitular.tipodoc = angular.element("#tpdocu_TI").val();
      $scope.editarTitular.nifbic  = angular.element("#nif_TI").val();
      var nombre = angular.element("#nombre_TI").val();
      var apellido1 = angular.element("#apellido1_TI").val();
      var apellido2 = angular.element("#apellido2_TI").val();
      var nomapellido = "";
      var bNombreApellido = false;
      if (nombre.trim() !== ""){
    	  nomapellido = nombre.trim();
    	  bNombreApellido = true;
      }
      if (apellido1.trim() !== ""){
    	  if (bNombreApellido){
    		  nomapellido += " * ";
    	  }
    	  nomapellido += apellido1.trim();
    	  bNombreApellido = true;
      }
      if (apellido2.trim() !== ""){
    	  if (bNombreApellido){
    		  nomapellido += " * ";
    	  }
    	  nomapellido += apellido2.trim();
    	  bNombreApellido = true;
      }
      $scope.editarTitular.nomapell  = nomapellido;
      $scope.editarTitular.bolsa     = angular.element("#bolsa_TI").val();
      $scope.editarTitular.ccv       = angular.element("#ccv_TI").val();
      $scope.editarTitular.codempr   = angular.element("#codempr_TI").val();
      $scope.editarTitular.codbic    = angular.element("#codbic_TI").val();
      $scope.editarTitular.distrito  = angular.element("#cdpost_TI").val();
      $scope.editarTitular.domicili  = angular.element("#nbdomi_TI").val();
      $scope.editarTitular.idsenc    = angular.element("#idsenc_TI").val();
      $scope.editarTitular.idnac     = angular.element("#tpnaci_TI").val();
      $scope.editarTitular.naclidad  = angular.element("#cdnaci_TI").val();
      $scope.editarTitular.pais      = angular.element("#pais_TI").val();
      $scope.editarTitular.pasiRe    = angular.element("#paires_TI").val();
      $scope.editarTitular.particip  = angular.element("#partic_TI").val();
      $scope.editarTitular.poblacio  = angular.element("#ciudad_TI").val();
      $scope.editarTitular.provincia = angular.element("#provin_TI").val();
      $scope.editarTitular.tipper    = angular.element("#tpsoci_TI").val();
      $scope.editarTitular.tiptitu   = angular.element("#tptipr_TI").val();
      editarTitular();
  };


  $scope.editar_Desglose = function () {

      $scope.editarDesglose.fecha = angular.element("#fecha").val();
      $scope.editarDesglose.bolsa = angular.element("#bolsa").val();
      var isinCompleto = $scope.editarDesglose.isin;
      $scope.editarDesglose.isin = getIsin(isinCompleto);
      $scope.editarDesglose.titulos = angular.element("#titulos").val();
      $scope.editarDesglose.precio = angular.element("#precio").val();
      $scope.editarDesglose.ccv = angular.element("#ccv").val();
      $scope.editarDesglose.titular = angular.element("#titular").val();
      $scope.editarDesglose.entidad = angular.element("#entidad").val();

      var precioPattern = new RegExp(/^\d{1,6}\b.?\d{0,4}?$/);
      var titulosPattern = new RegExp(/^\d{1,11}\b.?\d{0,0}?$/);

      var titulos = parseFloat($scope.editarDesglose.titulos);
      var precio = parseFloat($scope.editarDesglose.precio);
      var efectivo = titulos * precio;

      if (!titulosPattern.test($scope.editarDesglose.titulos))
      {
          growl.addErrorMessage("los titulos no tienen el formato correcto");
      } else if (!precioPattern.test($scope.editarDesglose.precio))
      {
          growl.addErrorMessage("el precio no tiene el formato correcto");
      } else
      {
          $scope.editarDesglose.nominej = efectivo;
          editarDesglose();
      }

  };

  function openEditForm() {

      ngDialog.open({
          template: 'template/conciliacion/desgloses/editarDesglose.html',
          className: 'ngdialog-theme-plain custom-width custom-height-400',
          scope: $scope,
          preCloseCallback: function () {
              if ($scope.needRefresh) {
                  $scope.needRefresh = false;
                  recargarDatosPantalla();
              }
          }, controller: ['$scope', '$document', 'growl', function ($scope, $document, growl) {
                  $document.ready(function () {
                      //
                  });
              }]
      });
  }
  ;

  //editar desgloses
  $scope.cargoInputs = function (id, fecha, numReferencia, sentido, bolsa, isin, descripcion, titulos, precio, tipoSaldo, ccv, titular, entidad, tipoCambio, numOrden) {

      console.log("*******************");
      console.log(" cargoInputs ");
      console.log("*******************");

      if (sentido == '1')
      {
          sentido = 1;
      } else
      {
          sentido = 2;
      }

      if (tipoSaldo == 'P')
      {
          $scope.tipoSaldo = $scope.listaTipoSaldo[0];
      } else
      {
          $scope.tipoSaldo = $scope.listaTipoSaldo[1];
      }

      if (tipoCambio == '1')
      {
          tipoCambio = 1;
      } else
      {
          tipoCambio = 2;
      }

      $scope.dialogTitle = 'Editar desglose de la orden: '+numOrden;
      $scope.editarDesglose = {};
      $scope.editarDesglose = {id: id,
          fecha: fecha,
          numReferencia: numReferencia,
          sentido: sentido,
          bolsa: bolsa,
          isin: isin,
          descripcion: descripcion,
          titulos: titulos,
          precio: precio,
          tipoSaldo: tipoSaldo,
          ccv: ccv,
          titular: titular,
          entidad: entidad,
          tipoCambio: tipoCambio};
      openEditForm();

  }//$scope.cargoInputs = function () {

  $scope.borrarTitular = function (id, nifbic){

	  var filtro = {id: id};

      angular.element("#dialog-confirm").html("¿Desea borrar realmente el titular con la identificador: " + nifbic + "?");

      // Define the Dialog and its properties.
      angular.element("#dialog-confirm").dialog({
          resizable: false,
          modal: true,
          title: "Mensaje de Confirmación",
          height: 120,
          width: 540,
          buttons: {
              "Sí": function () {
                  $(this).dialog('close');
                  DesgloseMQService.borrarTitular(successBorrarTitular, showError, filtro);
              },
              "No": function () {
                  $(this).dialog('close');
              }
          }
      });

  }//$scope.borrarTitular = function (id, nifbic){


  function successCrearTitularesEspeciales(json, status, headers, config) {
      if (SHOW_LOG)
          console.log("desglose borrado ...");
      var datos = {};
      if (json !== undefined &&
              json.resultados !== undefined &&
              json.resultados !== null) {
          if (json.error !== null) {
              growl.addErrorMessage(json.error);
          } else {
              if (json.resultados.mensaje !== undefined &&  json.resultados.mensaje !== null &&  json.resultados.mensaje !== "" ){
            	  growl.addErrorMessage(json.resultados.mensaje);
              }
              else
              {
            	  var afiCreated = json.resultados.afiCreated;
            	  var afiErrorCreated = json.resultados.afiErrorCreated;
            	  growl.addSuccessMessage(" Se han creado "+afiCreated+" titulares sin error y se han credo "+afiErrorCreated +" con error");
              }
          }
      } else
      {
          if (json !== undefined && json.error !== null) {
              if (SHOW_LOG)
                  console.log("Error borrando desglose ...");
              growl.addErrorMessage(json.error);
          }
      }
  }
  ;

  $scope.crearTitularesEspeciales = function (id, numReferencia){

	  var filtro = {id: id};
	// Define the Dialog and its properties.
      angular.element("#dialog-confirm").dialog({
          resizable: false,
          modal: true,
          title: "Mensaje de Confirmación",
          height: 120,
          width: 540,
          buttons: {
              "Sí": function () {
                  $(this).dialog('close');
                  TitularesRoutingService.createTitularesEspeciales(successCrearTitularesEspeciales, showError, filtro);
              },
              "No": function () {
                  $(this).dialog('close');
              }
          }
      });

  };//$scope.crearTitularesEspeciales = function (id, numReferencia){

  $scope.borrarDesglose = function (id, numReferencia)
  {

      var filtro = {id: id};

      angular.element("#dialog-confirm").html("¿Desea borrar realmente el desglose con la referencia: " + numReferencia + "?");

      // Define the Dialog and its properties.
      angular.element("#dialog-confirm").dialog({
          resizable: false,
          modal: true,
          title: "Mensaje de Confirmación",
          height: 120,
          width: 540,
          buttons: {
              "Sí": function () {
                  $(this).dialog('close');
                  DesgloseMQService.borrarDesglose(successBorrarDesglose, showError, filtro);
              },
              "No": function () {
                  $(this).dialog('close');
              }
          }
      });

  };
  $scope.selectedIsin = function (selected) {
      console.log("selectedIsin");
      console.log("selected.originalObject:  "+selected.originalObject);
      if (selected !== undefined && selected.originalObject !== undefined) {
          console.log("selectedIsin: "+selected.originalObject.codigo);
          $scope.editarDesglose.isin = selected.originalObject.codigo;
      }
  };

}]);
