(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var MantenimientoOpController = function (service, httpParamSerializer, uiGridConstants, $scope, $filter) {
		var ctrl = this;
		// Sub controlador DynamicFiltersController
		GENERIC_CONTROLLERS.DynamicFiltersController.call(ctrl, service, httpParamSerializer, uiGridConstants);
		// Opciones del modal (480px ancho)
		var wWith = $(window).width();
		ctrl.filter = {};
		ctrl.clearResult();
		ctrl.formIds = [];
		ctrl.gridNumerations = [];
		ctrl.titularType = [];
		ctrl.selectOnOff = [];
		ctrl.autoISINComplete = [];
//		ctrl.autoIsinCompleteFilter = [];
//		ctrl.autoIsinDescCompleteFilter = [];
		ctrl.autoIdTitularComplete = [];
//		ctrl.autoTitularCompleteFilter = [];	
		ctrl.autoOrdenanteComplete = [];
		ctrl.autoCustodioComplete = [];
		ctrl.valueIdOpeParaNumeracion;
		ctrl["combo-TIPO_OP_"] = [];
		ctrl.scope = $scope;
		ctrl.saveFlag = true;
		ctrl.saveNumFlag = true;
		ctrl.textPattern = "[/\w, ,.]{0,12}";
		ctrl.errMsg = "";
//		ctrl.isinInput = "";
		
		var fatDialogOpt = {
			width: wWith * 0.6,
			top: wWith * 0.2,
			left: wWith * 0.2
		};

		var gridNumerationsColumns = [
			{ name: "Id", field: "id" },
			{ name: "Secuencia desde", field: "opsec1" },
			{ name: "Secuencia hasta", field: "opsec2" },
			{ name: "Nº de títulos", field: "optitu" }
		];

		this.dialogs = ["#modalAltaOp", "#numerationGrid", "#modalAltaNum"];

		this.dialogs.forEach(dialog => {
			var aux = $('[role=dialog]').find(dialog);
			(aux.length > 0) ? aux.dialog('destroy').remove() : undefined;
		});
		
		this.selectedQuery = {"name":"SICAVMantenimientoOpQuery","desc":"Consultar valores","accion":"SearchValues"};
		this.selectedExportQuery = {"name":"SICAVEmisionDocumentoQuery","desc":"Consultar valores","accion":"SearchValues"};
		this.selectQuery();

		// Llamadas a back para completar los combos
		ctrl.fillCombos();

		ctrl.gridNumerations = ctrl.createGridColumnsNoSelection(
			gridNumerationsColumns, function (gridApi) {
				ctrl.gridApiRefNum = gridApi;
				gridApi.selection.on.rowSelectionChanged(ctrl.scope, ctrl.onSelectionChanged);
			}
		);

		ctrl.onSelectionChanged = function (row) {
			var numerationRowLength = row.grid.api.selection.getSelectedRows().length;
			switch (numerationRowLength) {
				case 0:
					ctrl.setBtnDisable("numerationGrid", true, "Modificar");
					ctrl.setBtnDisable("numerationGrid", true, "Eliminar");
					break;
				case 1:
					ctrl.setBtnDisable("numerationGrid", false, "Modificar");
					ctrl.setBtnDisable("numerationGrid", false, "Eliminar");
					break;
				default:
					ctrl.setBtnDisable("numerationGrid", true, "Modificar");
					ctrl.setBtnDisable("numerationGrid", true, "Eliminar");
					break;
			}
		}

		// Resgistramos los modales por la id, y le asignamos una función para
		// el boton con el nombre asignado (Guardar, Modificar, etc)
		ctrl.altaOpModal = ctrl.registryModal("modalAltaOp", "Crear", {
			Aceptar: function () {
				if(ctrl.saveFlag || ctrl.asociarFlag){
					ctrl.save("operation");
				}else{
					ctrl.edit($(this)[0].id, "operation");
				}
			},
			Cancelar: function () {
				ctrl.executeQuery();
				$(this).dialog("close");
			}
		}, fatDialogOpt);

		ctrl.numerationModal = ctrl.registryModal("numerationGrid", "Numeraciones", {
			Alta: function () {
				ctrl.openModal('save', 'numeration');
			},
			Modificar: function () {
				ctrl.openModal('edit', 'numeration');
			},
			Eliminar: function () {
				ctrl.delete(ctrl.gridApiRefNum.selection.getSelectedRows(), 'id', 'numeration')
			},
			Cancelar: function () {
				ctrl.executeQuery();
				delete ctrl.GRIDNUMAS;
				delete ctrl.GRIDNUMAC;
				$(this).dialog("close");
			}
		}, fatDialogOpt);

		ctrl.altaNumModal = ctrl.registryModal("modalAltaNum", "Alta numeraciones", {
			Aceptar: function () {
				ctrl.saveNumFlag ? ctrl.save("numeration") : ctrl.edit($(this)[0].id, "numeration");
			},
			Cancelar: function () {
				$(this).dialog("close");
			}
		}, fatDialogOpt);

		$('[role=dialog]').has("#modalAltaOp").find("button:contains('Cerrar')").remove();
		$('[role=dialog]').has("#numerationGrid").find("button:contains('Cerrar')").remove();
		$('[role=dialog]').has("#modalAltaNum").find("button:contains('Cerrar')").remove();

		ctrl.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					ctrl.resultDialog.dialog("close");
					if (ctrl.result.ok) {
						ctrl.executeQuery();
					}
					ctrl.result = {
						errorMessage: []
					};
				}
			}
		});

		ctrl.refactorParams = function (direction) {
			var aux = {};
			var params;
			
			
			if(direction == "operation"){
				// Eliminamos la descripción de los autocompletables
				(ctrl.params.CDDCLAVE_AUTO) ? ctrl.params.CDDCLAVE = ctrl.params.CDDCLAVE_AUTO.split(" - ")[0] : '';
				(ctrl.params.CDORDTIT_AUTO) ? ctrl.params.CDORDTIT = ctrl.params.CDORDTIT_AUTO.split(" - ")[0] : ctrl.params.CDORDTIT = "";
				
				(ctrl.params.CDCUSTOD_AUTO) ? ctrl.params.CDCUSTOD = ctrl.params.CDCUSTOD_AUTO.split(" - ")[0] : ctrl.params.CDCUSTOD = "";
				ctrl.params.ISIN = ctrl.params.ISIN_AUTO.split(" - ")[0]; 
				if(ctrl.params.INDCOM == 'N'){
					ctrl.params.FECHA_COB = '';
				}
				if(ctrl.params.OPIMPC == '' || null == ctrl.params.OPIMPC){
					ctrl.params.OPIMPC = '0';
				}
				
				params = ctrl.params;
			} else{
				params = ctrl.numParams;
				params.ID_OPE = ctrl.valueIdOpeParaNumeracion;
			}
			Object.keys(params).forEach(function (key) {
				aux[key.replace("_", "").toLowerCase()] = angular.copy(params[key]);
			});
			return aux;
		}

		// Devuelve las fechas con el siguiente formato[dd/MM/yyyy]
		ctrl.filterDate = function (date) {
			return $filter('date')(date, 'dd/MM/yyyy');
		}

		// Metodo que pinta en un array los valores que hallan fallado en las
		// validaciones
		ctrl.fillErrMsg = function (key, validity) {
			var msgs = ctrl.result.errorMessage;

			if (!validity.valid) {
				validity.valueMissing ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es obligatorio") : '';
				validity.patternMismatch ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] no respeta el formato") : '';
				validity.tooShort ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es demasiado corto") : '';
				validity.tooLong ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es demasiado largo") : '';
			}
		}

		// Abrimos el formulario que muestra los errores del formulario
		ctrl.notValidForm = function () {
			ctrl.result.ok = false;

			ctrl.resultDialog.dialog("option", "title", "Ejecución con errores");
			ctrl.resultDialog.dialog("open");
			ctrl.scope.$applyAsync();
		}

		// Modificador del datepicker pasado por parámetros dejando el año a
		// modificar directamente y dandole rango.
		ctrl.datepickerModified = function (id) {
			$(id).datepicker({
				changeYear: true,
				yearRange: "1900:9999",
				autoSize: true
			});
		}

		// Método que devuelve comprueba los errores que se han dado en las
		// validaciones y en el que se llena una array con los valores erroneos
		// que hallan salidos
		ctrl.isFormValid = function () {
			ctrl.formIds.forEach(function (key) {
				var element = document.getElementById(key);
				var validity = element.validity;
				if (element.type != "select-one") {
					if (element.readOnly) {
						element.readOnly = false;
						ctrl.fillErrMsg(key, validity)
						element.readOnly = true;
					} else {
						ctrl.fillErrMsg(key, validity)
					}
				} else {
					(!element.disabled && element.required && element.value == "") ? ctrl.result.errorMessage.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es obligatorio") : '';
				}
			});
			return ctrl.result.errorMessage.length == 0;
		};

		ctrl.calcularEfectivo = function () {
			// Efectivo igual al Precio multiplicado por los Títulos, si el
			// precio existe
			if(ctrl.params.OPPREU){
				ctrl.params.OPEFEC = ctrl.params.OPTITU * ctrl.params.OPPREU;
				ctrl.params.OPPREU = Number(ctrl.params.OPPREU.toFixed(6));
				ctrl.params.OPEFEC = Number(ctrl.params.OPEFEC.toFixed(2));
			}

			// Efectivo igual a [Títulos X Nominal Unitario del Valor X (%
			// Precio / 100)], si el % precio existe
			if(ctrl.params.OPPCPR){
				//Calculamos el efectivo en el BACK habiendo añadido el nominal en la ecuacion
				this.service.executeActionGETNominal(ctrl.refactorParams('operation'), {}, function (result) {
					(result.opefec != undefined) ? ctrl.params.OPEFEC = result.opefec : ctrl.params.OPEFEC;
					ctrl.params.OPEFEC = Number(ctrl.params.OPEFEC.toFixed(2));
				}, this.postFail.bind(this));
//				ctrl.params.OPEFEC = ctrl.params.OPTITU * (ctrl.params.OPPCPR / 100);
			}
		}
		
		ctrl.vaciarFechaCobro = function () {
			// Si el indicador de comisión es NO, vaciamos la fecha de cobro.
			if(ctrl.params.INDCOM == 'N'){
				ctrl.params.FECHA_COB = '';
			}
		}

		ctrl.calcularPrecio = function () {
			ctrl.params.OPPREU = (ctrl.params.OPEFEC) ? (ctrl.params.OPEFEC / ctrl.params.OPTITU) : ctrl.params.OPPREU;

			if (ctrl.params.OPPREU) {
				var splitedNumber = ctrl.params.OPPREU.toString().split(".");
				if (splitedNumber.length == 2 && splitedNumber[1].includes("e-")) {
					ctrl.params.OPPREU = parseFloat("0.000000");
				} else if (splitedNumber.length != 1) {
					ctrl.params.OPPREU = parseFloat(splitedNumber[0] + '.' + splitedNumber[1].substring(0, 6));
				}
			}
			
			ctrl.params.OPEFEC = (ctrl.params.OPEFEC) ? Number(ctrl.params.OPEFEC.toFixed(2)) : undefined;
			ctrl.params.OPPCPR = undefined;
			ctrl.selectToEnable(ctrl.params.OPPREU);
		}

		ctrl.isHacienda = function (hacienda) {
			if (hacienda == "S" && !ctrl.saveFlag) {
				ctrl.setBtnDisable("modalAltaOp", true, "Aceptar");
			} else {
				ctrl.setBtnDisable("modalAltaOp", false, "Aceptar");
			}
		}

		ctrl.getModalIds = function (id) {
			var modalElement = $(id);

			ctrl.getIds(modalElement.find("input"));
			ctrl.getIds(modalElement.find("select"));
		}

		ctrl.getIds = function (elements) {
			for (var i = 0; i < elements.length; i++) {
				if (elements[i].id != "") {
					ctrl.formIds.push(elements[i].id);
				}
			};
		}

		ctrl.masiveOperation = function (rows) {
			var flag = false
			if (rows.length == 0) {
				return true;
			};
			var id = rows[0].ID_OPE;
			rows.forEach(row => {
				if (row.ID_OPE != id) {
					flag = true;
				}
			});
			return flag;
		}

		ctrl.getRowsId = function (rows) {
			var list = [];
			rows.forEach(row => {
				list.push(row.ID);
			});
			return list;
		}

		ctrl.requiredIfThisExit = function (value, idTit, valSec1, valSec2) {
			if (value != undefined && value != "") {
				$("#" + idTit).prop('required', true);
			} else {
				$("#" + valSec1).prop('disabled', false);
				$("#" + valSec2).prop('disabled', false);
				$("#" + idTit).prop('required', false);
			}
		}

		ctrl.executeActionNumeration = function () {
			ctrl.service.executeActionNumeration(ctrl.selectedRows()[0].ID_OPE, result => {
				$.unblockUI();
				ctrl.gridNumerations.data = result;
				ctrl.GRIDNUMAC = 0;
				result.forEach(row => {
					ctrl.GRIDNUMAC += parseInt((row.optitu != undefined) ? row.optitu : 0);
				});

				if (ctrl.GRIDNUMAC > ctrl.GRIDNUMOPTITU) {
					ctrl.numerationModal.dialog("close");
					fErrorTxt("Nº títulos acumulados no puede ser mayor a Nº de títulos de la operación, error en el sistema", 1);
				} else {
					ctrl.GRIDNUMAS = ctrl.GRIDNUMOPTITU - ctrl.GRIDNUMAC;
					ctrl.setBtnDisable("numerationGrid", true, "Modificar");
					ctrl.setBtnDisable("numerationGrid", true, "Eliminar");

					ctrl.numerationModal.dialog("option", "title", "Numeraciones");
					ctrl.numerationModal.dialog("open");
				}
			}, this.postFail.bind(this));
		}

		ctrl.isSecuenciaOk = function () {
			ctrl.result.errorMessage = [];
			ctrl.errMsg = "- El campo 'Secuencia desde' no puede tener un valor inferior a el campo 'Secuencia hasta'";
			
			if (Number(ctrl.numParams.OPSEC1) > Number(ctrl.numParams.OPSEC2)) {
				ctrl.result.errorMessage.push(ctrl.errMsg);
			} else {
				ctrl.result.errorMessage.forEach((msg, i) => {
					if (msg == ctrl.errMsg) {
						ctrl.result.errorMessage.splice(i, 1);
					}
				});
				this.valorTitulos();
			}
		}
		
		ctrl.valorTitulos = function () {
			if (null != ctrl.numParams.OPSEC1 && null != ctrl.numParams.OPSEC2) {
				ctrl.numParams.OPTITU = Number(ctrl.numParams.OPSEC2) - Number(ctrl.numParams.OPSEC1) + 1;
			}
		}
		
		ctrl.closeAutocompletes = function () {
			$("auto-complete-input").children().each(function(element) {
				$("#" + $("auto-complete-input").children()[element].id).autocomplete("close");
			});
		}
		
		ctrl.comboComprobarVacio = function () {
			// ISIN Y DESCRIPCION
			if(ctrl.params.NBTITREV == null){
				ctrl.params.NBTITREV = '';
			}
			if(ctrl.params.ISIN == null){
				ctrl.params.ISIN_AUTO = '';
			}else{
				ctrl.params.ISIN_AUTO = ctrl.params.ISIN + " - " + ctrl.params.NBTITREV;
			}
			// CDDCLAVE Y DESCRIPCION
			if(ctrl.params.DESTIT == null){
				ctrl.params.DESTIT = '';
			}
			if(ctrl.params.CDDCLAVE == null){
				ctrl.params.CDDCLAVE_AUTO = '';
			}else{
				ctrl.params.CDDCLAVE_AUTO = ctrl.params.CDDCLAVE + " - " + ctrl.params.DESTIT;
			}
			// CDCUSTOD Y DESCRIPCION
			if(ctrl.params.CDCUSTODDES == null){
				ctrl.params.CDCUSTODDES = '';
			}
			if(ctrl.params.CDCUSTOD == null){
				ctrl.params.CDCUSTOD_AUTO = '';
			}else{
				ctrl.params.CDCUSTOD_AUTO = ctrl.params.CDCUSTOD + " - " + ctrl.params.CDCUSTODDES;
			}
			// ISIN Y DESCRIPCION
			if(ctrl.params.CDORDTITDES == null){
				ctrl.params.CDORDTITDES = '';
			}
			if(ctrl.params.CDORDTIT == null){
				ctrl.params.CDORDTIT_AUTO = '';
			}else{
				ctrl.params.CDORDTIT_AUTO = ctrl.params.CDORDTIT + " - " + ctrl.params.CDORDTITDES;
			}	 	
		}
		
		
		ctrl.recargarNumeraciones = function () {
			inicializarLoading();
			ctrl.GRIDNUMOPTITU = 0;
			ctrl.grid.data.forEach(row => {
				if (row.ID_OPE == ctrl.valueIdOpeParaNumeracion) {
					ctrl.GRIDNUMOPTITU += row.OPTITU;
				}
			})
			
			ctrl.service.executeActionNumeration(ctrl.valueIdOpeParaNumeracion, result => {
				$.unblockUI();
				ctrl.gridNumerations.data = result;
				ctrl.GRIDNUMAC = 0;
				result.forEach(row => {
					ctrl.GRIDNUMAC += parseInt((row.optitu != undefined) ? row.optitu : 0);
				});

				if (ctrl.GRIDNUMAC > ctrl.GRIDNUMOPTITU) {
					ctrl.numerationModal.dialog("close");
					fErrorTxt("Nº títulos acumulados no puede ser mayor a Nº de títulos de la operación, error en el sistema", 1);
				} else {
					ctrl.GRIDNUMAS = ctrl.GRIDNUMOPTITU - ctrl.GRIDNUMAC;
					ctrl.setBtnDisable("numerationGrid", true, "Modificar");
					ctrl.setBtnDisable("numerationGrid", true, "Eliminar");

				}
			}, this.postFail.bind(this));
		}

//
//		$(document).ready(function(){ 
//			console.log("Esto es una prueba")
//			ctrl.titularInput = $('[ng-model="target.filter.CDDCLAVE"]').find('[ng-model="single"]');
//
//			var inputText = $(ctrl.titularInput).find("text");
//			
//			inputText.prop( "disabled", true ); 
//			
//		});
		
//		$( window ).on( "load", function() {
//	        console.log( "window loaded" );
//
//			ctrl.titularInput = $('[ng-model="target.filter.CDDCLAVE"]').find('[ng-model="single"]');
//			ctrl.isinInput = $('[ng-model="target.filter.ISIN"]').find('[ng-model="single"]');
//			ctrl.isinDescInput = $('[ng-model="target.filter.NBTITREV"]').find('[ng-model="single"]');
//			
//			ctrl.titularInput[0].setAttribute("disabled", true);
//			ctrl.isinInput[0].setAttribute("disabled", true);
//			ctrl.isinDescInput[0].setAttribute("disabled", true);
//			
//			ctrl.titularInput[0].value = "Cargando...";
//			ctrl.isinInput[0].value = "Cargando...";
//			ctrl.isinDescInput[0].value = "Cargando...";
//	    });
	};

	MantenimientoOpController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

	MantenimientoOpController.prototype.constructor = MantenimientoOpController;
	
	/**
	 * Valida las fechas y las setea correctamente
	 */
	MantenimientoOpController.prototype.validateFechas = function () {
		if ((this.filter.FECHA_OPE == undefined && this.filter.FECHA_OPE2 == undefined) == false) {
			if (this.filter.FECHA_OPE == undefined) {
				this.filter.FECHA_OPE = [];
				this.filter.FECHA_OPE.push('01/01/1900');
			} else {
				this.filter.FECHA_OPE = [this.filter.FECHA_OPE];
			}

			if (this.filter.FECHA_OPE2 != undefined) {
				this.filter.FECHA_OPE.push(angular.copy(this.filter.FECHA_OPE2));
				this.filter.FECHA_OPE2 = [];
			} else {
				this.filter.FECHA_OPE.push('31/12/2100');
			}
		}		 	
	}
	
	
	/*
	 * Exporta en Excel y/o CSV
	 */
	MantenimientoOpController.prototype.exportExcelCsvQuery = function (queryName, detail) {
		this.validateFechas();	
		
		/* Llamamos al servicio */
		this.service.executeExportExcelCsv(
				queryName,
				this.httpParamSerializer(this.filter),
				detail, 
				function (result) { 
					/* Nothing todo */
				}, 
				this.postFail.bind(this)
		);
	}
	
	/*
	 * Exporta en Excel la Emision del Documento
	 */
	MantenimientoOpController.prototype.exportEmisionDocumento = function (queryName, detail) {
		this.validateFechas(); 		
		
		/* Llamamos al servicio */
		this.service.executeExportEmisionDoc(
				queryName, 
				this.httpParamSerializer(this.filter), 
				detail, 
				function (result) { 
					/* Nothing todo */
				}, 
				this.postFail.bind(this)
		);
	}	
	
	MantenimientoOpController.prototype.clearResult = function () {
		var ctrl = this;
		ctrl.result = {
			errorMessage: []
		}
	}
	
//	/*
//	 * Rellena los combos autorellenable
//	 */
//	MantenimientoOpController.prototype.fillCombos = function () {
//		var ctrl = this;
//		
//		inicializarLoading();
//
//
//		// Cargamos el Auto del Iitular
//		ctrl.service.executeActionQueryCombos('SICAVOperacionesIdTitularQuery', function (result) {
//			ctrl.autoIdTitularComplete = [];	
//			// Autocomplete filterIdTitular Codigo
//			ctrl.autoTitularCompleteFilter = [];		
//			result.forEach(function(element) {
//				var elementStr = element.CDDCLAVE + " - " + element.DESTIT;
//				ctrl.autoIdTitularComplete.push(elementStr);
//				
//				var itemCodigo = [];
//				itemCodigo.key = element.CDDCLAVE;
//				itemCodigo.value = element.CDDCLAVE;
//				
//				ctrl.autoTitularCompleteFilter.push(itemCodigo);
//			});			
//			// Autoayuda filtro id titular
//			ctrl.titularInput = $('[ng-model="target.filter.CDDCLAVE"]').find('[ng-model="single"]');
//			ctrl.populateAutocomplete(ctrl.titularInput, ctrl.autoTitularCompleteFilter, function (option) {
//				ctrl.params.id = option.value;      
//			});
//
//			$(ctrl.titularInput[0]).removeAttr('disabled');
//			ctrl.titularInput[0].value = "";
//		}, function(error) {
//			var message;
//			ctrl.titularInput = $('[ng-model="target.filter.CDDCLAVE"]').find('[ng-model="single"]');
//			$(ctrl.titularInput[0]).removeAttr('disabled');
//			ctrl.titularInput[0].value = "";
//			
//			if(typeof error === "string" ) {
//				message = "Se produjo un error al ejecutar la acción: " + error;
//			}
//			else if(error.length == 1) {
//				message = error;
//			}
//			else {
//				message = "Se produjeron los siguientes errores: \n";
//				angular.forEach(error, function(value) {
//					message += ("- " + value + "\n");
//				});
//			}
//			fErrorTxt(message, 1);
//			$.unblockUI();
//		});
//		
//		// Cargamos el Auto del Custodio y titular
//		ctrl.service.executeActionQueryCombos('SICAVOrdenanteCustodioIdQuery', function (result) {
//			ctrl.autoOrdenanteComplete = [];
//			ctrl.autoCustodioComplete = [];
//			
//			result.forEach(function(element) {
//				var elementStr = element.CDCLEARE + " - " + element.DSCLEARE;
//				ctrl.autoOrdenanteComplete.push(elementStr);
//				ctrl.autoCustodioComplete.push(elementStr);
//			});			
//		}, this.postFail.bind(this));
//		
//		
//		
//		// Cargamos el Auto del ISIN
//		ctrl.service.executeActionQueryCombos('SICAVIsinIdQuery', function (result) {
//			// Autocomplete Alata/Mod
//			ctrl.autoISINComplete = [];	
//			// Autocomplete filterIsin Codigo
//			ctrl.autoIsinCompleteFilter = [];
//			// Autocomplete filterIsin Codigo
//			ctrl.autoIsinDescCompleteFilter = [];
//			result.forEach(function(element) {
//				var elementStr = element.CDISINVV + " - " + element.NBTITREV;
//				ctrl.autoISINComplete.push(elementStr);
//				var itemCodigo = [];
//				var itemDesc = [];
//				itemCodigo.key = element.CDISINVV;
//				itemCodigo.value = element.CDISINVV;
//				
//				itemDesc.key = element.NBTITREV;
//				itemDesc.value = element.NBTITREV;
//				
//				ctrl.autoIsinCompleteFilter.push(itemCodigo);
//				ctrl.autoIsinDescCompleteFilter.push(itemDesc);
//				
//			});			
//			
//			ctrl.isinInput = $('[ng-model="target.filter.ISIN"]').find('[ng-model="single"]');
//			ctrl.isinDescInput = $('[ng-model="target.filter.NBTITREV"]').find('[ng-model="single"]');
//			// Para el autocomplete
//			inicializarLoading();
//			ctrl.populateAutocomplete(ctrl.isinInput, ctrl.autoIsinCompleteFilter, function (option) {
//				ctrl.params.id = option.value;      
//			});
//			ctrl.populateAutocomplete(ctrl.isinDescInput, ctrl.autoIsinDescCompleteFilter, function (option) {
//				ctrl.params.id = option.value;         
//			});
//			$(ctrl.isinInput[0]).removeAttr('disabled');
//			$(ctrl.isinDescInput[0]).removeAttr('disabled');
//			ctrl.isinInput[0].value = "";
//			ctrl.isinDescInput[0].value = "";
//			
//			$.unblockUI();
//		}, function(error) {
//			var message;
//			ctrl.isinInput = $('[ng-model="target.filter.ISIN"]').find('[ng-model="single"]');
//			ctrl.isinDescInput = $('[ng-model="target.filter.NBTITREV"]').find('[ng-model="single"]');
//			$(ctrl.isinInput[0]).removeAttr('disabled');
//			$(ctrl.isinDescInput[0]).removeAttr('disabled');
//			ctrl.isinInput[0].value = "";
//			ctrl.isinDescInput[0].value = "";
//			
//			if(typeof error === "string" ) {
//				message = "Se produjo un error al ejecutar la acción: " + error;
//			}
//			else if(error.length == 1) {
//				message = error;
//			}
//			else {
//				message = "Se produjeron los siguientes errores: \n";
//				angular.forEach(error, function(value) {
//					message += ("- " + value + "\n");
//				});
//			}
//			fErrorTxt(message, 1);
//			$.unblockUI();
//		});
//		
//		ctrl.titularType.push({ label: "Titular", value: "T" });
//		ctrl.titularType.push({ label: "Representante", value: "R" });
//		ctrl.titularType.push({ label: "Usufructuario", value: "U" });
//		ctrl.titularType.push({ label: "NudoPropietario", value: "N" });
//		
//		ctrl.selectOnOff.push({ label: "Si", value: "S" });
//		ctrl.selectOnOff.push({ label: "No", value: "N" });
//		
//		ctrl["combo-TIPO_OP_"].push({ label: "COMPRA", value: "C" });
//		ctrl["combo-TIPO_OP_"].push({ label: "VENTA", value: "V" });
//		
//		ctrl.selectQuery();
//		ctrl.executeQuery();
//		
//		
//	}
	
	/*
	 * Rellena los combos autorellenable
	 */
	MantenimientoOpController.prototype.fillCombos = function () {
		var ctrl = this;
		
		inicializarLoading();
		
		// Cargamos el Auto del Iitular
		ctrl.service.executeActionQueryCombos('SICAVOperacionesIdTitularQuery', function (result) {
			ctrl.autoIdTitularComplete = [];			
			result.forEach(function(element) {
				var elementStr = element.CDDCLAVE + " - " + element.DESTIT;
				ctrl.autoIdTitularComplete.push(elementStr);
			});			
		}, this.postFail.bind(this));
		
		// Cargamos el Auto del Custodio y titular
		ctrl.service.executeActionQueryCombos('SICAVOrdenanteCustodioIdQuery', function (result) {
			ctrl.autoOrdenanteComplete = [];
			ctrl.autoCustodioComplete = [];
			
			result.forEach(function(element) {
				var elementStr = element.CDCLEARE + " - " + element.DSCLEARE;
				ctrl.autoOrdenanteComplete.push(elementStr);
				ctrl.autoCustodioComplete.push(elementStr);
			});			
		}, this.postFail.bind(this));
		
		
		// Cargamos el Auto del ISIN
		ctrl.service.executeActionQueryCombos('SICAVIsinIdQuery', function (result) {
			ctrl.autoISINComplete = [];			
			result.forEach(function(element) {
				var elementStr = element.CDISINVV + " - " + element.NBTITREV;
				ctrl.autoISINComplete.push(elementStr);
			});			
		}, this.postFail.bind(this));
		
		ctrl.titularType.push({ label: "Titular", value: "T" });
		ctrl.titularType.push({ label: "Representante", value: "R" });
		ctrl.titularType.push({ label: "Usufructuario", value: "U" });
		ctrl.titularType.push({ label: "NudoPropietario", value: "N" });
		
		ctrl.selectOnOff.push({ label: "Si", value: "S" });
		ctrl.selectOnOff.push({ label: "No", value: "N" });
		
		ctrl["combo-TIPO_OP_"].push({ label: "COMPRA", value: "C" });
		ctrl["combo-TIPO_OP_"].push({ label: "VENTA", value: "V" });
		
		ctrl.selectQuery();
		ctrl.executeQuery();
		
		$.unblockUI();
	}

	MantenimientoOpController.prototype.when = function () {
		var args = arguments;  // the functions to execute first
		return {
			then: function (done) {
				var counter = 0;
				for (var i = 0; i < args.length; i++) {
					// call each function with a function to call on done
					args[i](function () {
						counter++;
						if (counter === args.length) {  // all functions have
														// notified they're done
							done();
						}
					});
				}
			}
		};
	};

	// Función que se encarga de levantar los modales con una id que identifica
	// su tipo
	MantenimientoOpController.prototype.openModal = function (id, direction) {
		var ctrl = this;
		ctrl.formIds = [];
		ctrl.clearResult();

		if (ctrl.asociarFlag) {
			ctrl.selectToEnable("#INDCOM");
			ctrl.selectToEnable("#INDHAC");
			ctrl.asociarFlag = false;
		}

		ctrl.removeBtnsStyle("modalAltaOp");
		ctrl.removeBtnsStyle("modalAltaNum");
		ctrl.autoCompleteMaxH("modalAltaOp", 100)
		switch (id) {
			case "save":
				// Control al aceptar operación para que use la fución ctrl.save
				(direction == "operation") ? ctrl.saveFlag = true : ctrl.saveNumFlag = true;
				// Limpieza de parámetros
				(direction == "operation") ? ctrl.params = {} : ctrl.numParams = {};
				// Seteando parametros por defecto
				(direction == "operation") ? ctrl.params.INDHAC = "N" : '';
				// Control sobre los campos a validar
				(direction == "operation") ? ctrl.getModalIds("#modalAltaOp") : ctrl.getModalIds("#modalAltaNum");
				// Seteando parametros por defecto
				(direction == "operation") ? ctrl.params.INDCOM = "N" : '';
				// Seteando parametros por defecto
				(direction == "operation") ? ctrl.params.TPTIPREP = "T" : '';
				// Seteando parametros por defecto
				(direction == "operation") ? ctrl.params.OPIMPC = 0 : '';
				// Control sobre el modal a abrir
				var openToSave = (direction == "operation") ? ctrl.altaOpModal : ctrl.altaNumModal;


				ctrl.scope.$applyAsync();
				openToSave.dialog("option", "title", "Crear");
				openToSave.dialog("open");
				
				if(direction == "operation") {
					ctrl.closeAutocompletes();
				}

				break;
			case "asociar":
				ctrl.saveFlag = false;
				ctrl.saveNumFlag = false;
				ctrl.asociarFlag = true;

				ctrl.selectToDisabled("#INDCOM");
				ctrl.selectToDisabled("#INDHAC");
				ctrl.params = angular.copy(ctrl.selectedRows()[0]);
				
				ctrl.comboComprobarVacio();
				
				(ctrl.params.FECHA_OPE) ? ctrl.params.FECHA_OPE = ctrl.filterDate(ctrl.params.FECHA_OPE) : '';
				(ctrl.params.FECHA_ALTA) ? ctrl.params.FECHA_ALTA = ctrl.filterDate(ctrl.params.FECHA_ALTA) : '';
				(ctrl.params.FECHA_BAJA) ? ctrl.params.FECHA_BAJA = ctrl.filterDate(ctrl.params.FECHA_BAJA) : '';
				(ctrl.params.FECHA_MOD) ? ctrl.params.FECHA_MOD = ctrl.filterDate(ctrl.params.FECHA_MOD) : '';
				(ctrl.params.FECHA_COB) ? ctrl.params.FECHA_COB = ctrl.filterDate(ctrl.params.FECHA_COB) : '';
				// Dejamos a undefined los campos a rellenar para asociar con la operación
				var modificableInputs = ["TICOVE", "OPTITU", "OPIMPC", "CDDCLAVE", "TPTIPREP", "PCPARTI"];
				modificableInputs.forEach(function (input) {
					delete ctrl.params[input];
				});

				// Seteando parametros por defecto
				(direction == "operation") ? ctrl.params.TPTIPREP = "T" : '';
				// Seteando parametros por defecto
				(direction == "operation") ? ctrl.params.OPIMPC = 0 : '';

				ctrl.altaOpModal.dialog("option", "title", "Asociar con la operación: " + ctrl.params.ID_OPE);
				ctrl.altaOpModal.dialog("open");
				
				if(direction == "operation") {
					ctrl.closeAutocompletes();
				}

				break;
			case "edit":
				// Seteo de la fila seleccionada en la variable "rows"
				var rows = (direction == "operation") ? angular.copy(ctrl.selectedRows()) : angular.copy(ctrl.gridApiRefNum.selection.getSelectedRows());
				// Control al aceptar operación para que use la fución ctrl.edit
				(direction == "operation") ? ctrl.saveFlag = false : ctrl.saveNumFlag = false;
				// Control sobre los campos a validar
				(direction == "operation") ? ctrl.getModalIds("#modalAltaOp") : ctrl.getModalIds("#modalAltaNum");

				if (rows.length > 0) {
					// Igualamos los valores a los parámetros que recibiremos en
					// el modal
					(direction == "operation") ? ctrl.params = angular.copy(rows[0]) : ctrl.numParams = angular.copy(rows[0]);
					if(direction == "operation"){
						ctrl.comboComprobarVacio();
						
						(ctrl.params.FECHA_OPE) ? ctrl.params.FECHA_OPE = ctrl.filterDate(ctrl.params.FECHA_OPE) : '';
						(ctrl.params.FECHA_ALTA) ? ctrl.params.FECHA_ALTA = ctrl.filterDate(ctrl.params.FECHA_ALTA) : '';
						(ctrl.params.FECHA_BAJA) ? ctrl.params.FECHA_BAJA = ctrl.filterDate(ctrl.params.FECHA_BAJA) : '';
						(ctrl.params.FECHA_MOD) ? ctrl.params.FECHA_MOD = ctrl.filterDate(ctrl.params.FECHA_MOD) : '';
						(ctrl.params.FECHA_COB) ? ctrl.params.FECHA_COB = ctrl.filterDate(ctrl.params.FECHA_COB) : '';
					}else{
						(ctrl.numParams.nuidre) ? ctrl.numParams.NUIDRE = ctrl.numParams.nuidre : '';
						(ctrl.numParams.opsec1) ? ctrl.numParams.OPSEC1 = ctrl.numParams.opsec1 : '';
						(ctrl.numParams.opsec2) ? ctrl.numParams.OPSEC2 = ctrl.numParams.opsec2 : '';
						(ctrl.numParams.optitu) ? ctrl.numParams.OPTITU = Number(ctrl.numParams.optitu) : '';
					}
					// Control sobre el modal a abrir
					var openToEdit = (direction == "operation") ? ctrl.altaOpModal : ctrl.altaNumModal;

					ctrl.scope.$applyAsync();
					openToEdit.dialog("option", "title", "Modificar");
					openToEdit.dialog("open");
					
					if(direction == "operation") {
						ctrl.closeAutocompletes();
					}
				}

				break;
			case "numerations":
				inicializarLoading();
				ctrl.valueIdOpeParaNumeracion = ctrl.selectedRows()[0].ID_OPE;
				ctrl.GRIDNUMOPTITU = 0;
				ctrl.grid.data.forEach(row => {
					if (row.ID_OPE == ctrl.selectedRows()[0].ID_OPE) {
						ctrl.GRIDNUMOPTITU += row.OPTITU;
					}
				})
				ctrl.executeActionNumeration();

				break;
		}
	};

	MantenimientoOpController.prototype.selectToDisabled = function (id) {
		$(id).attr('disabled', 'disabled');
		$(id).css("background-color", "rgb(235, 235, 228)");
	}

	MantenimientoOpController.prototype.selectToEnable = function (id) {
		$(id).removeAttr('disabled')
		$(id).css("background-color", "white");
	}

	MantenimientoOpController.prototype.save = function (direction) {
		var ctrl = this;
		if (ctrl.isFormValid()) {
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [ctrl.params]
			ctrl.service.executeActionPOST(ctrl.refactorParams(direction), direction, function (result) {
				$.unblockUI();
				if (result.ok != undefined) {
					ctrl.result = result;
					// Ejecuta el modal de respuesta al servicio
					ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					ctrl.resultDialog.dialog("open");

					if (ctrl.result.ok == true) {
						if(direction == "operation"){
							ctrl.altaOpModal.dialog("close");
						}else{
							ctrl.altaNumModal.dialog("close");
							ctrl.recargarNumeraciones();
						}
					}
				} else {
					fErrorTxt("Error inesperado, consulte a un administrador", 1);
				}
			}, ctrl.postFail.bind(ctrl));
		} else {
			ctrl.notValidForm();
		}
	};

	MantenimientoOpController.prototype.edit = function (id, direction) {
		var ctrl = this;
		var rows = (id == "modalAltaOp") ? ctrl.selectedRows() : ctrl.gridApiRefNum.selection.getSelectedRows();

		if (ctrl.isFormValid()) {
			var idList = ctrl.getRowsId(rows);
			var params = ctrl.refactorParams(direction);

			if (direction == "operation") {
				var aux = angular.copy(params);
				params = [];

				idList.forEach(id => {
					aux.id = id;
					params.push(angular.copy(aux));
				});
			}

			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [ctrl.params]
			ctrl.service.executeActionPUT(params, direction, function (result) {
				$.unblockUI();
				if (result.ok != undefined) {
					ctrl.result = result;
					// Ejecuta el modal de respuesta al revicio
					ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					ctrl.resultDialog.dialog("open");

					if (ctrl.result.ok == true) {
						if(direction == "operation"){
							ctrl.altaOpModal.dialog("close");
						}else{
							ctrl.altaNumModal.dialog("close");
							ctrl.recargarNumeraciones();
						}
					}
				} else {
					fErrorTxt("Error inesperado, consulte a un administrador", 1);
				}

			}, ctrl.postFail.bind(ctrl));
		} else {
			ctrl.notValidForm();
		}
	};

	MantenimientoOpController.prototype.delete = function (rows, id, direction) {

		if (rows.length > 0) {
			var self = this;
			var deleteObj = [];
			rows.forEach(element => {
				deleteObj.push(element[id]);
			});

			angular.element("#dialog-confirm").html("¿Desea eliminar el/los registro/s seleccionado/s?");

			// Define the Dialog and its properties.
			this.confirmDialog = angular.element("#dialog-confirm").dialog({
				resizable: false,
				modal: true,
				title: "Mensaje de Confirmación",
				height: 150,
				width: 360,
				buttons: {
					" Sí ": function () {
						$(this).dialog('close');
						eliminar(direction);
					},
					" No ": function () {
						$(this).dialog('close');
					}
				}
			});

			function eliminar(direction) {
				// Inicia el "Cargando datos..."
				inicializarLoading();
				// Ejecutamos el servicio pasandole por parámetros [deleteObj]
				self.service.executeActionDEL(deleteObj, direction, function (result) {
					$.unblockUI();
					self.result = result;
					// Ejecuta el modal de respuesta al servicio
					self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					self.resultDialog.dialog("open");
					if (self.result.ok) {
						if(direction != "operation"){
							self.altaNumModal.dialog("close");
							self.recargarNumeraciones();
						}
					}
				});
			}
		}
		else {
			fErrorTxt("Debe seleccionar una o más filas para poder realizar esta opcción", 1);
		}
	};

	/**
	 * Retorna un objeto de configuración con los valores utilizados en este
	 * proyecto para mostrar un grid básico. Para las columnas consulta el
	 * método "columns".
	 */
	MantenimientoOpController.prototype.createGridColumnsNoSelection = function (
		columns, gridApiEventListener) {
		angular.forEach(columns, this.customizeColumns, this);
		return {
			paginationPageSizes: [5, 10],
			paginationPageSize: 5,
			enableFiltering: false,
			data: [],
			columnDefs: columns,
			gridMenuShowHideColumns: false,
			onRegisterApi: gridApiEventListener
		};
	};
	
	
//	MantenimientoOpController.prototype.populateAutocomplete = function (input, availableTags, callback) {
//		input.autocomplete({
//			minLength: 0,
//			source: availableTags,
//			focus: function (event, ui) {
//				return false;
//			},
//			select: function (event, ui) {
//				var option = {
//					key: ui.item.key,
//					value: ui.item.value
//				};
//				input.val(option.value);
//				callback(option);
//				//ctrl.scope.safeApply();
//				return false;
//			}
//		});
//
//	};

	sibbac20.controller("MantenimientoOpController", ["MantenimientoOpService", "$httpParamSerializer", "uiGridConstants",
		"$scope", "$filter", MantenimientoOpController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);