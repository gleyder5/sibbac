'use stric';
sibbac20.controller('CuentasEspejoController', ['$scope', '$compile', 'IsinService', 'CuentaLiquidacionService', 'MovimientoVirtualService','ngDialog',
    function ($scope, $compile, IsinService, CuentaLiquidacionService, MovimientoVirtualService, ngDialog) {
        $scope.cuentas = [];
        var cuentasEspejo = [];
        $scope.fila = {
        		auditUser : "",
        		auditDate : "",
        		titulos   : "",
        		efectivo  : "",
        		isin      : "",
        		fcontratacion : "",
        		fliquidacion  : ""
        };
        $scope.dialogTitle = "";

        /** ISINES **/
        function loadIsines() {
            IsinService.getIsines(cargarIsines, showError);
        }
        /** CUENTAS **/
        function loadCuentas() {
            CuentaLiquidacionService.getCuentasLiquidacion().success(cargarCuentas).error(showError);
            $.unblockUI();
        }
        /** END **/
        /**
         *
         * @param {type} data
         * @param {type} status
         * @param {type} headers
         * @param {type} config
         * @returns {undefined}
         */
        function showError(data, status, headers, config) {
            $.unblockUI();
            console.log("Error al inicializar datos: " + status);
        }
        function cargarCuentas(data, status, header, config) {
            if (data !== null && data.resultados !== null && data.resultados.cuentasLiquidacion !== null) {
                $scope.cuentas = data.resultados.cuentasLiquidacion;
            }
        }
        function cargarIsines(datosIsin) {
            var availableTags = [];
            var ponerTag = "";
            $.each(datosIsin, function (key, val) {
                ponerTag = val.codigo + " - " + val.descripcion;
                availableTags.push(ponerTag);
            });
            $("#cdisin").autocomplete({
                source: availableTags
            });
        }


        var oTable = undefined;
        function round(value, decimals) {
            return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
        }
        function consultar(params) {
            inicializarLoading();
            MovimientoVirtualService.getMovimientosEspejo(params, populateDataTable, showError);
        }

        function openPopup(row) {

        	$scope.dialogTitle = "Mostrar Información";
            ngDialog.open({
                template: 'template/conciliacion/cuentasEspejo/mostrarInformacion.html',
                className: 'ngdialog-theme-plain custom-width-480 custom-height-240',
                scope: $scope,
                preCloseCallback: function () {
                	$(row).css("background-color", "");
                	$scope.fila = {
                    		auditUser : "",
                    		auditDate : "",
                    		titulos   : "",
                    		efectivo  : "",
                    		isin      : "",
                    		fcontratacion : "",
                    		fliquidacion  : ""
                    };
                	$scope.dialogTitle = "";
                }
            });
        }

        function populateDataTable(datos) {
            oTable.fnClearTable();

            //
            var cdcodigocuentaliq = "";
            var isin = "";
            var descrIsin = "";
            var sistemaLiq = "";
            var descOperacion = "";
            var cdoperacion = "";
            var signoAnotacion = "";
            var fcontratacion = "";
            var fliquidacion = "";
            var titulos = "";
            var efectivo = "";
            var corretaje = "";
            var acumTitulos = 0;
            var acumTitulosNoFormat = 0;
            var acumEfectivo = 0;
            var auditUser = "";
            var auditDate = "";
            var contador = 0;
            cuentasEspejo = [];

            angular.forEach(datos, function (val, key) {
                cdcodigocuentaliq = (datos[key].cdcodigocuentaliq !== null) ? datos[key].cdcodigocuentaliq : "";
                isin = (datos[key].isin !== null) ? datos[key].isin : "";
                descrIsin = (datos[key].descrIsin != null) ? datos[key].descrIsin : "";
                sistemaLiq = (datos[key].sistemaLiq !== null) ? datos[key].sistemaLiq : "";
                cdoperacion = (datos[key].cdoperacion !== null) ? datos[key].cdoperacion : "";
                descOperacion = (datos[key].descripcionOperacion !== null) ? datos[key].descripcionOperacion : "";
                signoAnotacion = (datos[key].signoAnotacion !== null) ? datos[key].signoAnotacion : "";
                fcontratacion = (datos[key].fcontratacion !== null) ? transformaFecha(datos[key].fcontratacion) : "";
                fliquidacion = (datos[key].fliquidacion !== null) ? transformaFecha(datos[key].fliquidacion) : "";
                titulos = (datos[key].titulos !== null) ? datos[key].titulos : "0";
                titulos = $.number(titulos, 0, ',', '.');
                efectivo = (datos[key].efectivo !== null) ? a2digitos(datos[key].efectivo) : "0,00";
                /*corretaje = (datos[key].corretaje !== null) ? a2digitos(datos[key].corretaje) : "0,00";*/
                acumTitulosNoFormat = (cdoperacion.trim() === "Sald.Inicial") ? (datos[key].titulos) : (parseFloat(acumTitulosNoFormat) + parseFloat((val.titulos)));
                acumTitulos = $.number(acumTitulosNoFormat, 0, ',', '.');

                acumEfectivo = (cdoperacion.trim() === "Sald.Inicial") ? (datos[key].efectivo) : (parseFloat(acumEfectivo) + parseFloat((datos[key].efectivo)));
                auditUser = (datos[key].auditUser !== null) ? datos[key].auditUser : "";
                auditDate = (datos[key].auditDate !== null) ? transformaFecha(datos[key].auditDate) : "";
                var obj = oTable.fnAddData([
                    cdcodigocuentaliq,
                    isin,
                    descrIsin,
                    sistemaLiq,
                    cdoperacion,
                    descOperacion,
                    signoAnotacion,
                    fcontratacion,
                    fliquidacion,
                    titulos,
                    efectivo,
                    /*corretaje,*/
                    ((cdoperacion.trim() !== "Sald.Inicial" && cdoperacion.trim() !== "Sald.Final") ? acumTitulos : ''),
                    ((cdoperacion.trim() !== "Sald.Inicial" && cdoperacion.trim() !== "Sald.Final") ? a2digitos(round(acumEfectivo, 2)) : '')], false);
                var row = oTable.fnSettings().aoData[ obj[0] ].nTr;
                var aData = $("td", row);

                if ($(aData[4]).text().trim() === "Sald.Inicial")
                {
                    //Cambia el color del fondo a nivel de fila
                    $(row).css("background-color", "#E5F5F9!important");
                    $(row).css("font-weight", "bold");
                    //cambia el color del fondo a nivel de columna
                    //$('td', row).css('background-color', '#ddeeee');
                }
                else if ($(aData[4]).text().trim() === "Sald.Final")
                {
                    $(row).css("background-color", "#D8EFF7!important");
                    $(row).css("border-bottom", "1px solid #C00");
                    $(row).css("font-weight", "bold");
                } else if ($(aData[4]).text().trim() === "MAN") {
                	$(row).attr("id",contador);

                	cuentasEspejo[contador] = {
                    		auditUser : auditUser,
                    		auditDate : auditDate,
                    		titulos   : titulos,
                    		efectivo  : efectivo,
                    		isin      : isin,
                    		fcontratacion : fcontratacion,
                    		fliquidacion  : fliquidacion
                    };
                	contador++;

                    $(row).click(function (e) {
                    	$(row).css("background-color", "#FE2E2E!important");

                    	var id = $(row).attr("id");
                    	var cuentaEspejo = cuentasEspejo[id];

                    	$scope.fila = cuentaEspejo;
                    	openPopup(row);

                    });
                }
            });
            oTable.fnDraw();
            $.unblockUI();

        }
        // ///////////////////////////////////////////////
        // ////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
        function initialize() {

            //var fechas = ['fliquidacionA', 'fliquidacionD'];
            //verificarFechas([['fliquidacionD', 'fliquidacionA']]);
        	$("#ui-datepicker-div").remove();
 			$('input#fliquidacionD').datepicker({
 			    onClose: function( selectedDate ) {
 			      $( "#fliquidacionA" ).datepicker( "option", "minDate", selectedDate );
 			    } // onClose
 			  });

 			  $('input#fliquidacionA').datepicker({
 			    onClose: function( selectedDate ) {
 			      $( "#fliquidacionD" ).datepicker( "option", "maxDate", selectedDate );
 			    } // onClose
 			  });
            MovimientoVirtualService.getFechaActualizacionMovimientoAlias(cargarFechaActualizacionMovAlias, showError);
            loadCuentas();
            loadIsines();
            MovimientoVirtualService.getFechaPreviaLiquidacion(getFechaPrevia, showError);
        }
        function cargarFechaActualizacionMovAlias(fecha){
            $("#ultFechaMovAlias").html(transformaFecha(fecha));
        }
        $(document).ready(function () {
            initialize();
            ajustarAnchoDivTable();
            $('a[href="#release-history"]').toggle(function () {
                $('#release-wrapper').animate(
                        {marginTop: '0px'}, 600,
                        'linear');
            }, function () {
                $('#release-wrapper').animate({
                    marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
                }, 600, 'linear');
            });
            $('#download a').mousedown(
                    function () {
                        _gaq.push(['_trackEvent', 'download-button',
                            'clicked'])
                    });

            prepareCollapsion();
            prepareDataTable();
        });

        $scope.cargarTabla = function (event)
        {
        	if (angular.element("#fliquidacionD").val() === "") {
                alert("Por favor cumplimente los filtros con (*)");
                return false;
            }
            obtenerDatos();
            collapseSearchForm();
            seguimientoBusqueda();
            return false;
        };

        //$scope.limpiar = function (event){
    	 angular.element('#limpiar').click(function (event) {
        	//if (event !== null)
        	//{
        		event.preventDefault();
        	//}
        	 //ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fliquidacionA" ).datepicker( "option", "minDate", "");
            $( "#fliquidacionD" ).datepicker( "option", "maxDate", "");

            //
            $('input[type=text]').val('');
            $('select').val('0');

		});
    	//};
        function prepareDataTable() {
            oTable = $("#tblCuentaEspejo").dataTable({
                "dom": 'T<"clear">lfrtip',
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "scrollX": "auto",
                "pageLength": 20,
                "lengthMenu": [20, 25, 50, 75, 100],
                "tableTools": {
                    "sSwfPath": "js/swf/copy_csv_xls_pdf.swf"
                },
                "order": [],
                "columnDefs": [{
                        "targets": [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                        "orderable": false
                    }],
                "columns": [
                    {className: "centrar no-sort"},
                    {className: "monedaR", type: "formatted-num"},
                    {className: "aleft no-sort"},
                    {className: "aleft no-sort"},
                    {className: "aleft no-sort"},
                    {className: "centrar no-sort"},
                    {className: "centrar no-sort"},
                    {className: "centrar no-sort"},
                    {className: "centrar no-sort"},
                    {className: "align-right no-sort", type: 'date-eu'},
                    {className: "align-right no-sort", type: "date-eu"},
                    {className: "monedaR no-sort", type: "formatted-num"},
                    {className: "monedaR no-sort", type: "formatted-num"}
                ],
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    $compile(nRow)($scope);
                }
            });

        }
        function obtenerDatos() {

            var params = {
                "service": "SIBBACServiceMovimientoVirtual",
                "action": "getMovimientosEspejo",
                "filters": {
                    "isin": getIsin(),
                    /* "aliasDes": getAliasDes(), */
                    "cdcodigocuentaliq": ($("#selectCuenta option:selected").val() !== "") ? $(
                            "#selectCuenta option:selected").text().trim() : "",
                    "fliquidacionDe": transformaFechaInv($("#fliquidacionD").val()),
                    "fliquidacionA": transformaFechaInv($("#fliquidacionA").val())
                }
            };
            consultar(params);
        }

        function getFechaPrevia(datos) {
            $("#fliquidacionD").val(transformaFecha(datos[0]));
            $( "#fliquidacionA" ).datepicker( "option", "minDate", document.getElementById("fliquidacionD").value);
        }
        function getIsin() {
            var isinCompleto = $('#cdisin').val();
            var guion = isinCompleto.indexOf("-");

            return (guion > 0) ? isinCompleto.substring(0, guion) : isinCompleto;
        }

        function seguimientoBusqueda() {
        	$scope.menBusCuentasEspejo = "";

            if ($('#fliquidacionD').val() !== "" && $('#fliquidacionD').val() !== undefined) {
            	$scope.menBusCuentasEspejo += " fecha Desde: " + $('#fliquidacionD').val();
            }
            if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined) {
            	$scope.menBusCuentasEspejo += " fecha Hasta: " + $('#fliquidacionA').val();
            }
            if ($('#cdisin').val() !== "" && $('#cdisin').val() !== undefined) {
            	$scope.menBusCuentasEspejo += " isin: " + $('#cdisin').val();
            }
            if ($('#selectCuenta').val() !== "" && $("#selectCuenta option:selected").val() !== undefined) {
                if ($("#selectCuenta option:selected").val() === "0") {
                	$scope.menBusCuentasEspejo += " Cuenta: todas ";
                }
                else {
                	$scope.menBusCuentasEspejo += " Cuenta: " + $("#selectCuenta option:selected").val();
                }
            }
        }


    }]);

