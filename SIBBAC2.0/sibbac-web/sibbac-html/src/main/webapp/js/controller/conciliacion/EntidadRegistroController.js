'use strict';
sibbac20
    .controller('EntidadRegistroController',
                [
                 '$scope',
                 '$rootScope',
                 '$document',
                 'growl',
                 'EntidadRegistroService',
                 '$compile',
                 'ngDialog',
                 '$location',
                 'SecurityService',
                 function ($scope, $rootScope, $document, growl, EntidadRegistroService, $compile, ngDialog, $location,
                           SecurityService) {
                   //

                   var oTable = undefined;
                   $scope.filter = new EntidadRegistro();
                   $scope.selectedTipoer = "";
                   $scope.currentER = new EntidadRegistro();
                   $scope.erList = {};
                   $scope.dialogTitle = "";
                   $scope.searchFollow = "";
                   $scope.needRefresh = false;

                   function consultar (params) {
                     inicializarLoading();
                     EntidadRegistroService.getAllEntidadRegistro(onSuccessConsultaRequest, onErrorRequest, params);
                   }
                   function onSuccessConsultaRequest (json) {
                     var datos = undefined;
                     if (json !== undefined && json.error !== undefined && json.error !== null) {
                       growl.addErrorMessage(json.error);
                     } else if (json === undefined || json.resultados === undefined || json.resultados === null
                                || json.resultados.result_entidades_de_registro === undefined) {
                       datos = {};
                       growl.addInfoMessage("No se encontró ningún resultado.");
                     } else {
                       datos = json.resultados.result_entidades_de_registro;
                       growl.addInfoMessage(datos.length + " registros obtenidos.");
                       $scope.erList = datos;
                     }
                     populateDataTable(datos);
                     $.unblockUI();
                   }
                   function onErrorRequest (data) {
                     growl.addErrorMessage("No se pudo conectar al servidor de datos. Por favor inténtelo más tarde. "
                                           + data);
                     $.unblockUI();
                   }
                   function populateDataTable (datos) {
                     var difClass = 'centrado';
                     oTable.clear();
                     var tipoEntidadRegistro = "";

                     angular.forEach(datos, function (val, key) {
                       tipoEntidadRegistro = '<span data="' + datos[key].id + '">' + datos[key].nombre + '</span>';
                       // se controlan las acciones, se muestra el enlace cuando el usuario tiene el
                       // permiso.
                       var enlaceModificar = "";
                       var enlaceEliminar = "";
                       if (SecurityService.inicializated) {
                         if (SecurityService.isPermisoAccion($rootScope.SecurityActions.MODIFICAR, $location.path())) {
                           enlaceModificar = '<a class="cursor" data="' + val.id + '" ng-click="modificarCuenta('
                                             + val.id + ');"><img src="img/editp.png" title="editar" /></a>';
                         }
                         if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR, $location.path())) {
                           enlaceEliminar = '<a class="cursor" data="' + val.id + '" ng-click="borrarEntidad(' + val.id
                                            + ');"><img src="img/del.png"  title="borrar" /></a>';
                         }
                       }

                       oTable.row.add([ enlaceModificar, tipoEntidadRegistro, val.bic, val.tipoer.nombre,
                                       enlaceEliminar ]);

                     });
                     oTable.draw();
                   }

                   function getCurrentER (id) {
                     angular.forEach($scope.erList, function (val, key) {
                       if (id == val.id) {
                         $scope.currentER = val;
                         return false;
                       }
                     });
                   }
                   // ///////////////////////////////////////////////
                   // ////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
                   function initialize () {
                     cargaCombos();
                     obtenerDatos();
                   }

                   function loadDataTable () {
                     if (oTable !== null && oTable !== undefined) {
                       oTable = undefined;
                     }
                     oTable = angular.element("#tblEntidadRegistro").DataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "js/swf/copy_csv_xls_pdf.swf"
                       },
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "columns" : [ {
                         "sClass" : "centrado",
                         "bSortable" : "false"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado",
                         "bSortable" : "false"
                       } ],
                       "sort" : [ 1 ],
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "createdRow" : function (row, data, index) {
                         $compile(row)($scope);
                       }
                     });
                   }
                   $document.ready(function () {
                     if (oTable === undefined) {
                       loadDataTable();
                     }
                     initialize();
                     prepareCollapsion();
                     angular.element('#limpiar').click(function (event) {
                       $scope.filter = new EntidadRegistro();
                     });

                   });
                   $scope.submitForm = function (event) {
                     obtenerDatos();
                     collapseSearchForm();
                     seguimientoBusqueda();
                   };

                   function seguimientoBusqueda () {
                     $scope.searchFollow = "";
                     var cadenaFiltros = "";
                     if ($scope.filter.tipoer.id !== "") {
                       $scope.searchFollow += " Tipo de entidad de registro: " + $scope.filter.tipoer.id;
                     }
                     if ($scope.filter.nombre !== "") {
                       $scope.searchFollow += " Nombre: " + $scope.filter.nombre;
                     }
                     if ($scope.filter.bic !== "") {
                       $scope.searchFollow += " BIC: " + $scope.filter.bic;
                     }
                     $scope.selectedTipoer = "";
                   }
                   function getTipoerFromList (id) {
                     angular.forEach($scope.tipoerList, function (val, key) {
                       if (id == val.id) {
                         $scope.filter.tipoer = val;
                         return false;
                       }
                     });
                   }
                   function obtenerDatos () {
                     if ($scope.selectedTipoer !== "") {
                       getTipoerFromList($scope.selectedTipoer);
                     }
                     var params = {
                       "idTipoEr" : $scope.selectedTipoer,
                       "nombre" : $scope.filter.nombre,
                       "bic" : $scope.filter.bic
                     };
                     consultar(params);
                   }

                   // llamada para cargar datos combos
                   function cargaCombos () {
                     EntidadRegistroService.getAllTiposEntidadRegistro(onTipoERSuccessRequest, onErrorRequest, {});
                   }
                   function onTipoERSuccessRequest (json) {
                     if (json !== undefined && json.error !== undefined && json.error !== null) {
                       growl.addErrorMessage("Error cargando tipos de entidad registro del servidor: " + json.error);
                     } else if (json !== undefined && json.resultados !== undefined
                                && json.resultados.result_tipos_entidad_registro !== undefined) {
                       $scope.tipoerList = json.resultados.result_tipos_entidad_registro;
                     }
                   }

                   $scope.save = function (event) {

                     angular.element('#altaparam').attr('disabled', "disabled");

                     var idEr = $scope.currentER.id;
                     var TER = $scope.currentER.tipoer.id;
                     var nombre = $scope.currentER.nombre;
                     var bic = $scope.currentER.bic;
                     //
                     var filtro = {
                       nombre : nombre,
                       bic : bic,
                       idTipoEr : TER
                     };
                     if ($scope.currentER.id === "") {
                       inicializarLoading();
                       EntidadRegistroService.saveEntidadRegistro(onSaveSuccess, onErrorRequest, filtro);
                     } else {
                       inicializarLoading();
                       filtro.idEr = idEr;
                       EntidadRegistroService.updateEntidadRegistro(onUpdateSuccess, onErrorRequest, filtro);
                     }
                   };

                   function onSaveSuccess (json) {
                     if (json !== undefined && json.error !== undefined && json.error !== null) {
                       growl.addErrorMessage(json.error);
                     } else {
                       growl.addSuccessMessage("Entidad registro dada de alta correctamente.");
                       $scope.needRefresh = true;
                       ngDialog.closeAll();
                     }
                     $.unblockUI();
                   }

                   function onUpdateSuccess (json) {
                     if (json !== undefined && json.error !== undefined && json.error !== null) {
                       growl.addErrorMessage(json.error);
                     } else {
                       growl.addSuccessMessage("Datos modificados correctamente.");
                       $scope.needRefresh = true;
                       ngDialog.closeAll();
                     }
                     $.unblockUI();
                   }
                   function openEditForm () {
                     ngDialog.open({
                       template : 'template/conciliacion/entidad-registro/edit.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-480',
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           obtenerDatos();
                           $scope.needRefresh = false;
                           $scope.currentER = new EntidadRegistro();
                         }
                       }
                     });
                   }
                   $scope.createER = function () {
                     $scope.dialogTitle = 'Alta Entidad Registro';
                     $scope.currentER = new EntidadRegistro();
                     openEditForm();
                   }

                   $scope.modificarCuenta = function (id) {
                     $scope.dialogTitle = 'Modificar Entidad Registro';
                     // se mete en $scope.currentER la entidad registro seleccionada en la tabla
                     getCurrentER(id);
                     openEditForm();
                   };
                   function onSuccessDeleteHandler (json) {
                     $.unblockUI();
                     if (json.resultados === null) {
                       growl.addErrorMessage(json.error);
                     } else {
                       growl.addSuccessMessage("Entidad de Registro eliminada correctamente.");
                       obtenerDatos();
                     }
                   }
                   $scope.borrarEntidad = function (id) {
                     var filtro = {
                       idEr : id
                     };
                     angular.element("#dialog-confirm").html("¿Eliminar entidad?");

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm")
                         .dialog(
                                 {
                                   resizable : false,
                                   modal : true,
                                   title : "¿Desea borrar realmente la entidad " + id + "?",
                                   height : 150,
                                   width : 480,
                                   buttons : {
                                     "Sí" : function () {
                                       $(this).dialog('close');
                                       inicializarLoading();
                                       EntidadRegistroService.deleteEntidadRegistro(onSuccessDeleteHandler,
                                                                                    onErrorRequest, filtro);
                                     },
                                     "No" : function () {
                                       $(this).dialog('close');
                                     }
                                   }
                                 });

                   };
                 } ]);
