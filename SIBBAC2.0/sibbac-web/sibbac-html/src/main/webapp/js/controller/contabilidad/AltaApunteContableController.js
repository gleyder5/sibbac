'use strict';
sibbac20.controller('AltaApunteContableController',['$scope','$document','growl','ContaInformesService','IsinService','$compile', '$rootScope', 'SecurityService', '$location',
function($scope, $document, growl, ContaInformesService, IsinService, $compile, $rootScope, SecurityService, $location) {


    if (SecurityService.inicializated) {
 	   if (!SecurityService.isPermisoAccion($rootScope.SecurityActions.CREAR, $location.path())) {
           growl.addErrorMessage("No tiene permiso para acceder a esta funcionalidad");
           $location.path("/");
 	   }
    } else {
        $scope.$watch(security.event.initialized, function() {
           if (!SecurityService.isPermisoAccion($rootScope.SecurityActions.CREAR, $location.path())) {
                 growl.addErrorMessage("No tiene permiso para acceder a esta funcionalidad");
                 $location.path("/");
       	   }
        });
    }


	// Lista con los cdaliass
	var listaAlias = [];

	// Lista con el contenido a mostrar en el autocomplete de alias
	var availableTagsAlias = [];

	// lista de conceptos asociados a las acciones
	var actionConcepts = [];

	$(document).ready(function() {
	  console.log("Alta manual de apuntes contables document loaded");
	  initFormFields();
	  cargarAlias();
	});

	function initFormFields() {
	  var fechas = ['fechaApunte'];
	  loadpicker(fechas);

	  $('#tituloHeader').empty();
	  $('#tituloHeader').append("Alta manual de apuntes contables");

	  $('#download a').mousedown(function () {
	    _gaq.push(['_trackEvent', 'download-button', 'clicked'])
	  });

	  $.ajax({
	    type: "POST",
	    dataType: "json",
	    url:  "/sibbac20back/rest/service",
	    data: "{\"service\":\"SIBBACServiceApunteContable\",\"action\":\"init\"}",
	    beforeSend: function( x ) {
	      if(x && x.overrideMimeType) {
	        x.overrideMimeType("application/json");
	      }

	      // CORS Related
	      x.setRequestHeader("Accept", "application/json");
	      x.setRequestHeader("Content-Type", "application/json");
	      x.setRequestHeader("X-Requested-With", "HandMade");
	      x.setRequestHeader("Access-Control-Allow-Origin", "*");
	      x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	    },
	    async: true,
	    success: function(json) {
	      var filtro = json.resultados.datosInicializacion;
	      actionConcepts = filtro.actionConcept;

	      var select = $("select#actionType");
	      select.append("<option value=''>Select one action ...</option>");
	      $.each(filtro.actionType, function(index, value) {
	        select.append("<option value='" + index +"'>" + value +"</option>");
	      });
	    },
	    error: function(c) {
	      console.log( "ERROR al ejecutar SIBBACServiceApunteContable#init: " + c );
	    }
	  });

	  $("select#actionType").prop('required', true);
	  $("#fechaApunte").prop('disabled', true);
	  $("#textAlias").prop('disabled', true);
	  $("#textConcepto").prop('disabled', true);
	  $("#numberImporte").prop('disabled', true);
	  $("#textResponsable").prop('disabled', true);
	} // initFormFields

	$("#informe").submit(function(event) {
	  event.preventDefault();
	  console.log("Guardando ...");

	  var seleccionListaAlias = document.getElementById("textAlias").value;
	  var posicionAlias = availableTagsAlias.indexOf(seleccionListaAlias);

	  if(posicionAlias == "-1"){
	    alert("El alias introducido no existe. Seleccione uno de la lista de autocompletado.");
	    return false;
	  }

	  // var valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
	  var valorSeleccionadoAlias = listaAlias[posicionAlias];

	  var params = {
	    "service": "SIBBACServiceApunteContable",
	    "action": "add",
	    "filters": {
	      "actionType": $("select#actionType").val(),
	      "fechaApunte":$("input#fechaApunte").val(),
	      "alias":valorSeleccionadoAlias,
	      "concepto":$("input#textConcepto").val(),
	      "importe":$("input#numberImporte").val(),
	      "responsable":$("input#textResponsable").val()
	  }};

	  inicializarLoading();

	  $.ajax({
	    type: 'POST',
	    dataType: "json",
	    contentType: "application/json; charset=utf-8",
	    url: "/sibbac20back/rest/service",
	    data: JSON.stringify(params),
	    beforeSend: function (x) {
	      if (x && x.overrideMimeType) {
	        x.overrideMimeType("application/json");
	      }

	      // CORS Related
	      x.setRequestHeader("Accept", "application/json");
	      x.setRequestHeader("Content-Type", "application/json");
	      x.setRequestHeader("X-Requested-With", "HandMade");
	      x.setRequestHeader("Access-Control-Allow-Origin", "*");
	      x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	    },
	    success: function (json) {
	      console.log("add success");
	      var datos = undefined;

	      $.unblockUI();

	      if (json===undefined || json.resultados===undefined) {
	    	  alert("JSON de vuelta no recibido.");
	          return false;
	      } else {
	        datos = json.resultados.resultado;
	        if (datos == "SUCCESS") {
	          alert("Apunte contable insertado correctamente.");
	          $("#resetForm").trigger('click');
	          $("#actionType").focus();
	        } else if (datos == "FAIL") {
	        	alert("Error al crear el apunte contable: " + json.resultados.error_message);
	        } else {
	        	alert("Resultado no esperado: " + datos);
	        }
	      }
	    },
	    error: function(c) {
	      $.unblockUI();
	      alert( "ERROR al ejecutar SIBBACServiceContaInformes#filtra: " + c );
	    }
	  }); // $.ajax
	});

	$("input:reset").click(function() {
	  console.log("Reseteando formulario ...");
	  this.form.reset();
	  $("select#actionType").prop('required', true);
	  $("#fechaApunte").prop('disabled', true);
	  $("#textAlias").prop('disabled', true);
	  $("#textConcepto").prop('disabled', true);
	  $("#numberImporte").prop('disabled', true);
	  $("#textResponsable").prop('disabled', true);
	  return false; // prevent reset button from resetting again
	}); // $("input:reset").click

	$("select#actionType").on('change', function() {
	  if ($("select#actionType").val() == '') {
	    $("#resetForm").trigger('click');
	  } else {
	    var id = $("select#actionType").val();
		this.form.reset();
		$("select#actionType").val(id);

	    $("#fechaApunte").prop('disabled', false);
	    $("#fechaApunte").prop('required', true);

	    $("#textAlias").prop('disabled', false);
	    $("#textAlias").prop('required', true);

	    $("#textConcepto").prop('disabled', false);
	    $("#textConcepto").prop('required', true);

	    $("#numberImporte").prop('disabled', false);
	    $("#numberImporte").prop('required', true);

	    if (($("select#actionType").val() == '0') || ($("select#actionType").val() == '1')) {
	      $("#textResponsable").prop('disabled', false);
	    } else {
	      $("#textResponsable").prop('disabled', true);
	    } // else
	    $("#textResponsable").prop('required', false);

	    $("#textConcepto").val(actionConcepts[$("select#actionType").val()]);
	  } // else
	});

	$(".tipocantidad").change(function() {
	  if (($("select#actionType").val() == '0') || ($("select#actionType").val() == '1')){
	    var max = 100.00;
	    if (parseFloat($("input#numberImporte").val().replace(',', '.')) > max){
	      $("#textResponsable").prop('required', true);
	    } else {
	      $("#textResponsable").prop('required', false);
	    }
	  }
	});

	/**
	 * Carga la lista de alias y modifica el campo 'textAlias' del formulario para que sea un campo auntocompletable.
	 */
	function cargarAlias() {
	  var params = new DataBean();
	  params.setService('SIBBACServiceTmct0ali');
	  params.setAction('getAlias');

	  var request = requestSIBBAC(params);
	  request.success(function(json) {
	    var datos = undefined;
	    if (json === undefined || json.resultados === undefined || json.resultados.result_alias === undefined) {
	      datos = {};
	    } else {
	      datos = json.resultados.result_alias;
	      var item = null;

	      for ( var k in datos) {
	        item = datos[k];
	        listaAlias.push(item.alias);
	        var ponerTag = item.alias + " - " + item.descripcion;
	        availableTagsAlias.push(ponerTag);
	      }

	      $("#textAlias").autocomplete({
	        source : availableTagsAlias
	      });
	    } // else
	  });
	  request.error( function(c) {
		  alert( "ERROR al ejecutar SIBBACServiceTmct0ali#getAlias: " + c );
	  });
	} // cargarAlias

	function getAliasId() {
	  var alias = $('#textAlias').val();
	  var guion = alias.indexOf("-");
	  return (guion > 0) ? alias.substring(0, guion) : alias;
	}

	function getAliasDes() {
	  var aliasd = $('#textAlias').val();
	  var guion = aliasd.indexOf("-");
	  return (guion > 0) ? aliasd.substring(guion + 1) : aliasd;
	}


} ]);
