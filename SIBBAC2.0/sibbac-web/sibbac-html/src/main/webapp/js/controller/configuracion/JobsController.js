'use strict';
sibbac20.controller('JobsController',
                    [
                     '$scope',
                     '$compile',
                     '$interval',
                     '$rootScope',
                     'growl',
                     'JobsService',
                     '$document',
                     'ServerVarsService',
                     '$location',
                     'SecurityService',
                     function ($scope, $compile, $interval,$rootScope, growl, JobsService, $document, ServerVarsService,
                               $location, SecurityService) {


                       var idOcultos = [];
                       var availableTags = [];
                       var oTableRunningJobs = undefined;
                       var oTableCurrentJobs = undefined;
                       var listaChk = [];
                       $scope.listaCheck = [];
                       var listaCheckId = [];

                       var fsService = server.service.configuracion.jobsService;
                       var CMD_JOBS_LISTING = "LISTING";
                       var CMD_JOBS_HISTORY = "HISTORY";
                       var CMD_JOBS_FIRENOW = "FIRE_NOW";
                       var CMD_JOBS_REPLAN_JOB = "REPLAN_JOB";
                       var CMD_JOBS_START_ALL_JOBS = "START_ALL_JOBS";
                       var CMD_JOBS_START_JOB = "START_JOB";
                       var CMD_JOBS_STOP_ALL_JOBS = "STOP_ALL_JOBS";
                       var CMD_JOBS_STOP_JOB = "STOP_JOB";
                       var CMD_JOBS_KILL_JOB = "KILL_JOB";
                       // Loggers
                       var CMD_LIST_LOGGERS = "LIST_LOGGERS";
                       var CMD_CHANGE_LOG = "CHANGE_LOG";
                       var CMD_LOG_RESET = "LOG_RESET";

                       // Images
                       var IMG_JOBS_REPLAN_JOB = "schedule-job.png";
                       var IMG_JOBS_HISTORY = "hist.gif";
                       var IMG_JOBS_FIRENOW = "run.gif";
                       var IMG_JOBS_START_ALL_JOBS = "resume-all.png";
                       var IMG_JOBS_START_JOB = "resume-job.png";
                       var IMG_JOBS_STOP_ALL_JOBS = "pause-all.png";
                       var IMG_JOBS_STOP_JOB = "pause-job.png";
                       var IMG_SIZE = "14px";
                       var IMG_JOBS_SELECT_ALL = "arrow_collapsed.gif";
                       var IMG_JOBS_DESELECT_ALL = "arrow_expanded.gif";

                       // El DIV del "loading"...
                       var DIV_LOADING = "centrarLoad";
                       // El DIV del "history"...
                       var DIV_HISTORY = "jobHistory";
                       // El DIV de los "loggers"...
                       var DIV_LOGGERS = "loggers";

                       // New Line
                       var NL = "\n";
                       var BR = "<br/>";
                       var SHOW_LOG = false;

                       var timerStart = new Date();
                       var RELOAD_JOBS = false;
                       var RELOAD_JOBS_EVERY = 30; // seconds
                       var refreshJobsListener = 0;
                       $scope.serverNode = "";


                       $scope.loadingJobs = false;

                       function showError (data, status, headers, config) {
                         $.unblockUI();
                         growl.addErrorMessage("Error: " + data);
                       }

                       function cargaJobsPeriodicamente () {
                         var today = new Date();
                         var clockText = checkTime(today.getDate()) + "/" + checkTime(today.getMonth() + 1) + "/"
                                         + today.getFullYear();
                         var h = today.getHours();
                         var m = today.getMinutes();
                         var s = today.getSeconds();
                         h = checkTime(h);
                         m = checkTime(m);
                         s = checkTime(s);
                         clockText += " - " + h + ":" + m + ":" + s;
                         angular.element('#reloj').val(clockText);

                         /*
                           * Si se desea que la pagina recargue el listado automaticamente, basta con descomentar esta
                           * linea y settear: "RELOAD_JOBS" y "RELOAD_JOBS_EVERY" convenientemente.
                           */
                         if (RELOAD_JOBS) {
                           var currentMS = today.getTime();
                           var startMS = timerStart.getTime();
                           var diffMS = currentMS - startMS;
                           var ellapsed = parseInt(diffMS / 1000);
                           var gap = ellapsed % RELOAD_JOBS_EVERY;
                           if (SHOW_LOG)
                             console.log("GAP: [" + gap + "] [currentMS==" + currentMS + "] [startMS==" + startMS
                                         + "] [diffMS==" + diffMS + "] [ellapsed==" + ellapsed + "]");
                           if (gap == 0) {
                             $scope.cargaJobs();
                           }
                         }

                         if (refreshJobsListener != 0) {
                           clearTimeout(refreshJobsListener);
                         }
                         refreshJobsListener = setTimeout(cargaJobsPeriodicamente, 1000);
                       }

                       function checkTime000 (i) {
                         if (i < 10) {
                           return "00" + i;
                         } else if (i < 100) {
                           return "0" + i;
                         } else {
                           return "" + i;
                         }
                       }

                       function checkTime (i) {
                         if (i < 10) {
                           i = "0" + i
                         }
                         ; // add zero in front of numbers < 10
                         return i;
                       }

                       $scope.cargaJobs = function ($event) {
                      	 $event ? $event.preventDefault() : false; 
                      	 $scope.loadingJobs = true;
                         if (SHOW_LOG)
                           console.log("Cargando jobs...");
                         performJobCall(CMD_JOBS_LISTING, null, null, null, listingCallBack, true);
                       };



                       function initializaTables () {
                         initializaTableRunningJobs();
                         initializaTableCurrentJobs();
                       }

                       function initializaTableRunningJobs () {
                         oTableRunningJobs = $("#runningJobs").dataTable({
                           "dom" : 'T<"clear">lfrtip',
                           "tableTools" : {
                             "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                           },
                           "language" : {
                             "url" : "i18n/Spanish.json"
                           },
                           /* Disable initial sort */
                           "order" : [],
                           /* "bSort" : false, Deshabilita todos los sort */
                           "columns" : [ {
                             "width" : "auto",
                             sClass : "tacenter"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           } ],
                           "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                             $compile(nRow)($scope);
                           },
                           "scrollY" : "480px",
                           "scrollCollapse" : true,
                           "filter" : false
                         });
                       }
                       ;

                       function initializaTableCurrentJobs () {
                         oTableCurrentJobs = $("#currentJobs").dataTable({
                           "dom" : 'T<"clear">lfrtip',
                           "tableTools" : {
                             "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                           },
                           "language" : {
                             "url" : "i18n/Spanish.json"
                           },
                           /* Disable initial sort */
                           "order" : [],
                           /* "bSort" : false, Deshabilita todos los sort */
                           "columns" : [ {
                             "width" : "auto",
                             sClass : "tacenter"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           }, {
                             "width" : "auto",
                             sClass : "taleft"
                           } ],
                           "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                             if (aData.length > 8) {
                               var columnaOcho = aData[8];
                               if (aData[8].search("NORMAL") !== -1) {
                                 angular.element('td:eq(8)', nRow).css({
                                   "background-color" : "transparent",
                                   "color" : "green"
                                 });
                               } else if (aData[8].search("PAUSED") !== -1) {
                                 angular.element('td:eq(8)', nRow).css({
                                   "background-color" : "blue",
                                   "color" : "white"
                                 });
                               }
                             }
                             $compile(nRow)($scope);
                           },
                           "scrollY" : "480px",
                           "scrollCollapse" : true,
                           "filter" : true
                         });
                       }
                       ;

                       $document.ready(function () {
                         initializaTables();
                         getServerVars();
                         $(document).hotkeys('s', '2', '8', '8', 'a', 'c', showDivLoggers);
                         cargaJobsPeriodicamente();

                         $scope.cargaJobs();
                       });

                       $scope.selectAllJobs = function () {
                         toggleCheckBoxes(true);
                       }
                       $scope.deselectAllJobs = function () {
                         toggleCheckBoxes(false);
                       }
                       function resumeAllJobs () {
                         performJobCall(CMD_JOBS_START_ALL_JOBS);
                       }
                       function pauseAllJobs () {
                         performJobCall(CMD_JOBS_STOP_ALL_JOBS);
                       }

                       $scope.resumeJobs = function () {
                         var selected = detectSelectedCheckBoxes();
                         if (selected.length > 0) {
                           // JSON Call.
                           if (SHOW_LOG)
                             console.log("Requested RESUME " + selected.length + " jobs...");
                           performJobCall(CMD_JOBS_START_JOB, null, null, JSON.stringify(selected), listingCallBack,
                                          true);
                         } else {
                           alert("No se han seleccionado jobs!");
                         }
                       }
                       $scope.pauseJobs = function () {
                         var selected = detectSelectedCheckBoxes();
                         if (selected.length > 0) {
                           // JSON Call.
                           if (SHOW_LOG)
                             console.log("Requested PAUSE " + selected.length + " jobs...");
                           performJobCall(CMD_JOBS_STOP_JOB, null, null, JSON.stringify(selected), listingCallBack,
                                          true);
                         } else {
                           alert("No se han seleccionado jobs!");
                         }
                       }
                       $scope.killJobs = function () {
                         var selected = detectSelectedCheckBoxes();
                         if (selected.length > 0) {
                           // JSON Call.
                           if (SHOW_LOG)
                             console.log("Requested KILL " + selected.length + " jobs...");
                           performJobCall(CMD_JOBS_KILL_JOB, null, null, JSON.stringify(selected), listingCallBack,
                                          true);
                         } else {
                           alert("No se han seleccionado jobs!");
                         }
                       }

                       function showDivReplanJob (jobKey) {
                         // replanJob
                         angular.element('#replanJob').show();
                         angular.element('#rfade').show();
                         return false;
                       }

                       $scope.fireJobNow = function (group, name) {
                         var filters = "{ \"group\" : \"" + group + "\", \"name\" : \"" + name + "\" }";
                         performJobCall(CMD_JOBS_FIRENOW, filters, null, null, listingCallBack, null);
                         return false;
                       }

                       $scope.hideDivReplanJob = function () {
                         angular.element('#replanJob').hide();
                         angular.element('#fade').hide();
                       }

                       $scope.showDivHistory = function (group, name) {
                         // jobHistory
                         angular.element('#' + DIV_HISTORY).show();
                         angular.element('#fade').show();
                         angular.element(".scrollear").empty();
                         angular.element(".scrollear").hide();
                         angular.element("#jobHistoryName").html(group + "." + name)
                         var filters = "{ \"" + group + "\" : \"" + name + "\" }";
                         performJobCall(CMD_JOBS_HISTORY, filters, null, null, historyCallBack, false);
                         return false;
                       }

                       $scope.hideDivHistory = function () {
                         angular.element('#' + DIV_HISTORY).hide();
                         angular.element('#fade').hide();
                       }

                       function showDivLoggers (f, group, name) {
                         if (SHOW_LOG) {
                           console.log("Abriendo el DIV de los loggers...");
                         }
                         angular.element('#' + DIV_LOGGERS).show();
                         angular.element('#fade').show();
                         angular.element(".scrollear").empty();
                         angular.element(".scrollear").hide();
                         performJobCall(CMD_LIST_LOGGERS, null, null, null, loggersCallBack, false);
                         return false;
                       }

                       $scope.hideDivLoggers = function () {
                         angular.element('#' + DIV_LOGGERS).hide();
                         angular.element('#fade').hide();
                       };

                       function hayDatos (json) {
                         if (json === null || json === undefined) {
                           return false;
                         } else if (json !== null && json !== undefined && json.length == 0) {
                           return false;
                         } else {
                           return true;
                         }
                       }

                       function listingCallBack (json, status, headers, config) {
                         if (SHOW_LOG)
                           console.log("Listando los jobs...");
                         var datos = {};
                         if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                           if (json.error !== null) {
                             growl.addErrorMessage(json.error);
                           } else {
                             var resultados = json.resultados;
                             var running = resultados.running;
                             var declared = resultados.data;
                             var items = null;
                             var item = null;
                             var contador = 0;
                             mostrarOcultarCapaBotones(false);

                             // Running Jobs
                             if (hayDatos(running)) {

                               mostrarOcultarCapaBotones(true);
                               // NO HERE $('#tdRunningJobs').append( getControlButtonsHTMLLine() );
                               items = running.sort();
                               var now = (new Date()).getTime();
                               var startDate = null;
                               var ellapsed = 0;
                               for ( var k in items) {

                                 item = items[k];
                                 startDate = item.fireTime;
                                 ellapsed = Math.floor((now - startDate) / 1000);
                                 // LOG DE TIEMPO EN EJECUCION: console.log( "["+item.group+"/"+item.name+"] Running: "
                                 // + Math.floor(( now - startDate )/1000) + " secs" );
                                 chkName = item.group + "." + item.name;

                                 oTableRunningJobs.fnAddData([
                                                              "<span >" + +parseData(item.fireInstanceId) + "</span>",
                                                              "<span>" + parseData(item.group) + "</span>",
                                                              "<span>" + parseData(item.name) + "</span>",
                                                              "<span>" + toDate(parseData(item.previousFireTime))
                                                                  + "</span>",
                                                              "<span>" + toDate(parseData(item.scheduledFireTime))
                                                                  + "</span>",
                                                              "<span title=\"Ellapsed: " + ellapsed + " seconds\">"
                                                                  + toDate(parseData(item.fireTime)) + "</span>",
                                                              "<span>" + toDate(parseData(item.nextFireTime))
                                                                  + "</span>",
                                                              "<span>" + toDate(parseData(item.refireCount))
                                                                  + "</span>",
                                                              "<div class=\"trigger" + parseData(item.triggerState)
                                                                  + "\">" + parseData(item.triggerState) + "</div>" ]);
                                 oTableRunningJobs.fnDraw();
                               }
                               // NO HERE $('#tdRunningJobs').append( getControlButtonsHTMLLine() );
                             }

                             // Declared Jobs
                             listaCheckId = [];
                             if (hayDatos(declared)) {

                               var chkName = null;
                               mostrarOcultarCapaBotones(true);
                               items = declared.sort(sorting);
                               var now = (new Date()).getTime();
                               var startDate = null;
                               listaChk = [];
                               for ( var k in items) {
                                 item = items[k];
                                 startDate = item.nextFireTime;
                                 // LOG DE TIEMPO FALTANTE: console.log( "["+item.group+"/"+item.name+"] Quedan: " +
                                 // Math.floor(( startDate - now )/1000) + " secs" );
                                 chkName = item.group + "." + item.name;
                                 $scope.listaCheck[k] = false;
                                 listaCheckId[k] = chkName;
                                 var enlace = "";
                                 if (SecurityService.inicializated) {
                                   if (SecurityService.isPermisoAccion($rootScope.SecurityActions.MODIFICAR, $location
                                       .path())) {
                                     enlace = " <img src=\"img/" + IMG_JOBS_HISTORY + "\" height=\"" + IMG_SIZE
                                              + "\" title=\"Historico\" ng-click=\"showDivHistory('" + item.group
                                              + "', '" + item.name + "')\">" + " <img src=\"img/" + IMG_JOBS_FIRENOW
                                              + "\" height=\"" + IMG_SIZE
                                              + "\" title=\"Fire Now!\" ng-click=\"fireJobNow('" + item.group + "', '"
                                              + item.name + "')\">"
                                   }
                                 }

                                 oTableCurrentJobs.fnAddData([
                                                              "<input type='checkbox' ng-model=\"listaCheck[" + k
                                                                  + "]\" class='checkTabla' id='" + chkName
                                                                  + "' name='" + chkName + "'/>",
                                                              "<span >" + parseData(item.group) + "</span>",
                                                              "<span>" + parseData(item.name) + "</span>",
                                                              "<span>" + parseData(item.priority) + "</span>",
                                                              "<span>" + toDate(parseData(item.startTime)) + "</span>",
                                                              "<span>" + toDate(parseData(item.previousFireTime))
                                                                  + "</span>",
                                                              "<span>" + toDate(parseData(item.nextFireTime))
                                                                  + "</span>",
                                                              "<span>" + parseData(item.jobDetailDescription)
                                                                  + "</span>",
                                                              "<span>" + parseData(item.triggerState) + enlace
                                                                  + "</span>" ]);
                                 listaChk.push('check' + k);
                               }

                               oTableCurrentJobs.fnDraw();
                             }
                           }
                         }
                        $scope.loadingJobs = false; 
                       };

                       function historyCallBack (json, status, headers, config) {

                         var datos = {};
                         if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                           if (json.error !== null) {
                             growl.addErrorMessage(json.error);
                           } else {
                             var resultados = json.resultados;
                             var jobExecutions = resultados.data;

                             var jobExecution = null;
                             var eventTime = null;
                             var exec = null;
                             var hist = null;
                             var hKeys = null;
                             var start = null;
                             var div = angular.element("#scrollear");
                             var html = "<ul>";
                             for ( var k in resultados.data) {
                               jobExecution = resultados.data[k];
                               exec = jobExecution.execution;
                               hKeys = jobExecution.history;
                               start = toFullDate(parseData(exec.timestamp));
                               html += "<li class=\"execution\"><span>Execution(" + exec.id + "/" + exec.executionId
                                       + ")</span>" + NL;
                               html += "<ol>";
                               for ( var h in hKeys) {
                                 hist = hKeys[h];
                                 eventTime = toFullDate(parseData(hist.timestamp));
                                 html += "<li class=\"history\">" + hist.event + "" + "[" + eventTime + "]: "
                                         + hist.message + "</li>" + NL;
                               }
                               html += "</ol><br/></li>";
                             }
                             html += "</ul>";
                             angular.element(".scrollear").html(html);
                             angular.element(".scrollear").show();
                           }
                         }

                       }
                       ;

                       $scope.listLogLevel = [];
                       function loggersCallBack (json, status, headers, config) {
                         if (SHOW_LOG)
                           console.log("Listando los loggers...");
                         var datos = {};
                         if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                           if (json.error !== null) {
                             growl.addErrorMessage(json.error);
                           } else {
                             var resultados = json.resultados;
                             var loggersInfo = resultados.data;
                             loggersInfo = sortLogLevels(loggersInfo);
                             var html = "";
                             var level = null;
                             var contador = 0;
                             $scope.listLogLevel = [];
                             $scope.listLogName = [];
                             for ( var name in loggersInfo) {
                               level = loggersInfo[name];
                               $scope.listLogLevel[contador] = '';
                               $scope.listLogName[contador] = name;
                               html += "<div id=\"logger-" + name + "\" class=\"four-columns\"><div>" + name
                                       + ":</div><div>" + logLevelList(name, level, contador) + "</div></div>" + NL;
                               contador++;
                             }
                             angular.element("#divLoggers").html($compile(html)($scope));
                             angular.element(".scrollear").show();
                           }
                         }

                       }
                       ;

                       $scope.loggersChange = function (indice) {
                         var name = $scope.listLogName[indice];
                         var level = $scope.listLogLevel[indice];
                         if (SHOW_LOG)
                           console.log("[AGM] indice: " + indice + ", [name " + name + "/ level " + level + "]");
                         // Llamamos al servidor para que cambie el nivel de log del logger seleccionado.
                         var params = "[ { \"name\" : \"" + name + "\", \"level\" : \"" + level + "\" } ]";
                         performJobCall(CMD_CHANGE_LOG, null, params, null, loggersCallBack, false);
                       }

                       $scope.logReset = function () {
                         performJobCall(CMD_LOG_RESET, null, null, null, loggersCallBack, false);
                       };

                       $scope.smoothLog = function () {
                         var params = "[";
                         params += " { \"name\" : \"sibbac\", \"level\" : \"DEBUG\" }";
                         params += ", { \"name\" : \"org.springframework\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate.SQL\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate.tool.hbm2ddl\", \"level\" : \"INFO\" }";

                         params += ", { \"name\" : \"org.hibernate.engine.internal\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate.event.internal\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate.internal.util\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate.tool.hbm2ddl\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.quartz\", \"level\" : \"INFO\" }";

                         params += " ]";
                         performJobCall(CMD_CHANGE_LOG, null, params, null, loggersCallBack, false);
                       };

                       $scope.extendedLog = function () {
                         var params = "[";
                         params += " { \"name\" : \"sibbac\", \"level\" : \"ALL\" }";
                         params += ", { \"name\" : \"org.springframework\", \"level\" : \"ALL\" }";
                         params += ", { \"name\" : \"org.hibernate\", \"level\" : \"ALL\" }";
                         params += ", { \"name\" : \"org.hibernate.SQL\", \"level\" : \"ALL\" }";
                         params += ", { \"name\" : \"org.hibernate.tool.hbm2ddl\", \"level\" : \"ALL\" }";

                         params += ", { \"name\" : \"org.hibernate.engine.internal\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate.event.internal\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate.internal.util\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.hibernate.tool.hbm2ddl\", \"level\" : \"INFO\" }";
                         params += ", { \"name\" : \"org.quartz\", \"level\" : \"ALL\" }";

                         params += " ]";
                         performJobCall(CMD_CHANGE_LOG, null, params, null, loggersCallBack, false);
                       }

                       function logLevelList (name, level, indice) {
                         var LEVELS = [ "ALL", "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "OFF" ];
                         var html = "<select id=\"" + name + "\" name=\"" + name + "\" ng-model='listLogLevel["
                                    + indice + "]' ng-change=\"loggersChange( " + indice + " )\">" + NL;
                         var selected = "";
                         for ( var l in LEVELS) {
                           // selected = (LEVELS[l] == level) ? " selected=\"selected\"" : "";
                           if (LEVELS[l] == level) {
                             $scope.listLogLevel[indice] = LEVELS[l];
                           }
                           html += "<option  value=\"" + LEVELS[l] + "\">" + LEVELS[l] + "</option>" + BR + NL;
                         }
                         html += "</select>";
                         // if ( SHOW_LOG ) console.log( "[AGM] html: " + html );
                         return html;
                       }
                       ;

                       function sorting (a, b) {
                         return a.nextFireTime - b.nextFireTime;
                       }
                       function sortLogLevels (lista) {
                         var arrayTopKeys = [];
                         var arraykeys = [];
                         for ( var k in lista) {
                           if (k == "ROOT" || k == "sibbac" || k == "org.springframework" || k == "org.hibernate") {
                             arrayTopKeys.push(k);
                           }
                         }
                         for ( var k in lista) {
                           arraykeys.push(k);
                         }
                         arrayTopKeys.sort();
                         arraykeys.sort();
                         var outputarray = [];
                         for (var i = 0; i < arrayTopKeys.length; i++) {
                           outputarray[arrayTopKeys[i]] = lista[arrayTopKeys[i]];
                         }
                         for (var i = 0; i < arraykeys.length; i++) {
                           outputarray[arraykeys[i]] = lista[arraykeys[i]];
                         }
                         return outputarray;
                       }

                       // Funciones "privadas".
                       function toggleCheckBoxes (b) {
                         for (var i = 0; i < $scope.listaCheck.length; i++) {
                           $scope.listaCheck[i] = b;
                         }
                       }

                       function detectSelectedCheckBoxes () {
                         var selected = [];
                         for (var i = 0; i < listaCheckId.length; i++) {
                           if ($scope.listaCheck[i] === true) {
                             selected[i] = listaCheckId[i];
                           }
                         }
                         return selected;
                       }

                       function performJobCall (command, filters, params, lista, successCallBack, _clear) {
                         var clear = (_clear !== undefined) ? _clear : true;

                         // Limpiamos los "targets"..
                         if (clear) {
                           oTableRunningJobs.fnClearTable();
                           oTableCurrentJobs.fnClearTable();
                         }

                         var data = "{\"service\" :\"" + fsService + "\", \"action\"  :\"" + command + "\"";
                         if (lista !== null) {
                           data += ", \"list\" :" + lista;
                         }
                         if (filters !== null) {
                           data += ", \"filters\" :" + filters;
                         }
                         if (params !== null) {
                           data += ", \"params\" :" + params;
                         }
                         data += "}";

                         JSON.stringify(data);
                         JobsService.performJobCall(successCallBack, showError, data);
                       }

                       function parseData (val) {
                         if (val != null && val != undefined) {
                           return val;
                         } else {
                           return "";
                         }
                       }

                       function toFullDate (d) {
                         if (d != null && d != "") {
                           var fecha = new Date(d);
                           var r = toDate(d) + ":" + ("00" + fecha.getMilliseconds()).slice(-3);
                           return r;
                         } else {
                           return "";
                         }
                       }

                       function toDate (d) {
                         if (d != null && d != "") {
                           var fecha = new Date(d);
                           var r = ("0" + fecha.getDate()).slice(-2) + "-" + ("0" + (fecha.getMonth() + 1)).slice(-2)
                                   + "-" + fecha.getFullYear() + " " + ("0" + fecha.getHours()).slice(-2) + ":"
                                   + ("0" + fecha.getMinutes()).slice(-2) + ":" + ("0" + fecha.getSeconds()).slice(-2);
                           return r;
                         } else {
                           return "";
                         }
                       }

                       function mostrarOcultarCapaBotones (visible) {

                         angular.element('#botoneraJobs').hide();
                         if (visible === true) {
                           angular.element('#botoneraJobs').show();
                         }
                       }
                       ;
                       function onErrorRequest (err) {
                         growl.addErrorMessage(err);
                       }
                       function onSuccessServerVarsRequest (data) {
                         if (data.error !== undefined && data.error !== null && data.error !== "") {
                           growl.addErrorMessage(data.error);
                         } else if (data.resultados !== undefined && data.resultados.server_node !== "") {
                           $scope.serverNode = data.resultados.server_node;
                           growl.addInfoMessage("nodo: " + $scope.serverNode);
                         }
                       }
                       function getServerVars () {
                         ServerVarsService.getServerVars(onSuccessServerVarsRequest, onErrorRequest);
                       }

                     } ]);
