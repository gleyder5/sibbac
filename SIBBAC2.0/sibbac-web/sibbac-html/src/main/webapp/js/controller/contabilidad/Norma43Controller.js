'use strict';
sibbac20
    .controller(
                'Norma43Controller',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'ContaInformesService',
                 'IsinService',
                 '$compile',
                 '$rootScope',
                 'SecurityService',
                 '$location',
                 function ($scope, $document, growl, ContaInformesService, IsinService, $compile, $rootScope,
                           SecurityService, $location) {

                   $scope.activeAlert = false;

                   // Tabla de operaciones fallidas
                   var oTableDescuadresNorma43 = undefined;

                   // nombre de los ficheros a exportar
                   var nombreFicheros = "Descuadres_Norma43";

                   /**
                     * Carga la tabla de descuadres.
                     */
                   function loadTable_descuadresNorma43 () {
                     oTableDescuadresNorma43 = $("#tablaDescuadresNorma43").dataTable({
                       "sDom" : 'T<"clear">lfrtip',
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "scrollX" : "100%",
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "aoColumns" : [ {
                         sClass : "centrar"
                       }, // Cuenta
                       {
                         sClass : "centrar"
                       }, // Referencia fecha
                       {
                         sClass : "centrar"
                       }, // Referencia operación
                       {
                         sClass : "centrar"
                       }, // Sentido
                       {
                         sClass : "centrar",
                         sType : "date-eu"
                       }, // Fecha de contratación
                       {
                         sClass : "centrar",
                         sType : "date-eu"
                       }, // Fecha de liquidación
                       {
                         sClass : "align-right",
                         sType : "formatted-num"
                       }, // Importe Norma43
                       {
                         sClass : "align-right",
                         sType : "formatted-num"
                       }, // Importe Sibbac
                       {
                         sClass : "centrar"
                       }, // Tipo de operación
                       {
                         sClass : "centrar"
                       }, // Estado de asignación
                       {
                         sClass : "centrar"
                       }, // Estado contable
                       {
                         sClass : "centrar"
                       }, // Estado entrega/recepción
                       {
                         sClass : "centrar"
                       }, // Orden
                       {
                         sClass : "centrar"
                       }, // Booking
                       {
                         sClass : "centrar"
                       }, // Alo
                       {
                         sClass : "centrar"
                       } // Alc
                       ],
                       "bProcessing" : true,
                       "order" : [ [ 0, "asc" ] ]
                     });
                   } // loadTable_descuadresNorma43

                   /**
                     * Carga los valores de inicialización del formulario de búsqueda.
                     */
                   function initForm () {
                     var data = new DataBean();
                     data.setService('SIBBACServiceNorma43');
                     data.setAction('initDescuadres');

                     var request = requestSIBBAC(data);
                     request.success(function (json) {
                       if (json === undefined || json.resultados === undefined || json.resultados === null
                           || json.resultados.resultado === null || json.resultados.resultado === undefined) {
                         alert("ERROR :: Cadena de resultados vacía");
                       } else {
                         if (json.resultados.resultado === "SUCCESS") {

                           var select = $("#selectCuentas");
                           select.append("<option value=''>Seleccione una cuenta</option>");
                           $.each(json.resultados.initData.cuentas, function (index, value) {
                             $("#selectCuentas").append('<option value=' + index + '>&nbsp;' + value + '</option>');
                           });

                           var select = $("#selectSentido");
                           select.append("<option value='Todos'>Todos</option>");
                           $.each(json.resultados.initData.sentido, function (index, value) {
                             var sentido = (value != null ? (value == 1 ? 'Deudor' : 'Acreedor') : '-');
                             $("#selectSentido").append(
                                                        '<option value=' + (index + 1) + '>&nbsp;' + sentido
                                                            + '</option>');
                           });

                           var select = $("#selectRefOrden");
                           select.append("<option value='Todos'>Todos</option>");
                           $.each(json.resultados.initData.refOrden, function (index, value) {
                             $("#selectRefOrden").append('<option value=' + index + '>&nbsp;' + value + '</option>');
                           });

                         } else if (json.resultados.resultado === "FAIL") {
                           alert("Error cargando cuentas de liquidación descuadres Norma43 ... "
                                 + json.resultados.error_message);
                         } else {
                           alert("ERROR :: Resultado no controlado: " + json.resultados.resultado);
                         } // else
                       } // else
                     });
                     request.error(function (c) {
                       alert("ERROR al ejecutar SIBBACServiceNorma43#initDescuadres: " + c);
                     });
                   } // obtenerDatos

                   /**
                     * Obtiene y válida los datos del formulario de busqueda.
                     */
                   function searchData () {
                     var fechaDesde = $("input#textFechaDesde").val();
                     var fechaHasta = $("input#textFechaHasta").val();

                     var importeDesde = $("input#importeDesde").val();
                     var importeHasta = $("input#importeHasta").val();

                     // Se obtiene la cuenta de liquidación seleccionada
                     // var valorSeleccionadoCuentas = $("#selectCuentas option:selected").val();
                     var valorSeleccionadoCuentas0 = $("#selectCuentas option:selected").text().trim();
                     var posSeparadorCuenta = valorSeleccionadoCuentas0.indexOf('|');
                     var valorSeleccionadoCuentas = valorSeleccionadoCuentas0.substring(0, posSeparadorCuenta - 1)
                         .trim();

                     var valorSeleccionadoSentido = $("#selectSentido option:selected").val().trim();
                     var valorSeleccionadoRefOrden = $("#selectRefOrden option:selected").text().trim();

                     var filtro = "{\"fechaDesde\":\"" + fechaDesde + "\",\"fechaHasta\":\"" + fechaHasta
                                  + "\",\"importeDesde\":\"" + importeDesde + "\",\"importeHasta\":\"" + importeHasta
                                  + "\",\"cuenta\":\"" + valorSeleccionadoCuentas + "\",\"sentido\":\""
                                  + valorSeleccionadoSentido + "\",\"refOrden\":\"" + valorSeleccionadoRefOrden + "\"}";

                     console.log("valorSeleccionadoCuentas: " + valorSeleccionadoCuentas);
                     console.log("valorSeleccionadoSentido: " + valorSeleccionadoSentido);
                     console.log("valorSeleccionadoRefOrden: " + valorSeleccionadoRefOrden);
                     console.log("fechaDesde: " + fechaDesde);
                     console.log("fechaHasta: " + fechaHasta);
                     console.log("importeDesde: " + importeDesde);
                     console.log("importeHasta: " + importeHasta);

                     console.log("filtro: " + filtro);

                     var data = new DataBean();
                     data.setService('SIBBACServiceNorma43');
                     data.setAction('searchDescuadres');
                     data.setFilters(filtro);

                     var request = requestSIBBAC(data);
                     request.success(function (json) {
                       if (json === undefined || json.resultados === undefined || json.resultados === null
                           || json.resultados.resultado === null || json.resultados.resultado === undefined) {
                         alert("ERROR :: Cadena de resultados vacía");
                       } else {
                         if (json.resultados.resultado === "SUCCESS") {
                           populateDateTable(json.resultados.descuadresList);
                         } else if (json.resultados.resultado === "FAIL") {
                           alert("Error buscando descuadres Norma43 ... " + json.resultados.error_message);
                         } else {
                           alert("ERROR :: Resultado no controlado: " + json.resultados.resultado);
                         } // else
                       } // else
                     });
                     request.error(function (c) {
                       alert("ERROR al ejecutar SIBBACServiceNorma43#searchDescuadres: " + c);
                     });
                   } // obtenerDatos

                   /**
                     * Prepara la tabla de operaciones fallidas y la visualiza en pantalla.
                     *
                     * @param table
                     *          Tabla a preparar.
                     */
                   function populateDateTable (datos) {
                     var cuenta;
                     var refFecha;
                     var refOrden;
                     var sentido;
                     var tradeDate;
                     var settlementDate;
                     var importeNorma43;
                     var importeSibbac;
                     var tipoImporte;
                     var nombreEstadoAsignacionAlc;
                     var nombreEstadoContabilidadAlc;
                     var nombreEstadoEntrecAlc;
                     var nuorden;
                     var nbooking;
                     var nucnfclt;
                     var nucnfliq;

                     oTableDescuadresNorma43.fnClearTable();
                     var item = null;

                     for ( var k in datos) {
                       item = datos[k];

                       cuenta = item.cuenta != null ? item.cuenta : '-';
                       refFecha = item.refFecha != null ? item.refFecha : '-';
                       refOrden = item.refOrden != null ? item.refOrden : '-';
                       sentido = (item.sentido != null ? (item.sentido == 1 ? 'Deudor' : 'Acreedor') : '-');
                       tradeDate = item.tradeDate != null ? EpochToHuman(item.tradeDate) : '-';
                       settlementDate = item.settlementDate != null ? EpochToHuman(item.settlementDate) : '-';
                       importeNorma43 = item.importeNorma43 != null ? $.number(item.importeNorma43, 2, ',', '.') : '-';
                       importeSibbac = item.importeSibbac != null ? $.number(item.importeSibbac, 2, ',', '.') : '-';
                       tipoImporte = item.tipoImporte != null ? item.tipoImporte : '-';
                       nombreEstadoAsignacionAlc = item.nombreEstadoAsignacionAlc != null ? item.nombreEstadoAsignacionAlc
                                                                                         : '-';
                       nombreEstadoContabilidadAlc = item.nombreEstadoContabilidadAlc != null ? item.nombreEstadoContabilidadAlc
                                                                                             : '-';
                       nombreEstadoEntrecAlc = item.nombreEstadoEntrecAlc != null ? item.nombreEstadoEntrecAlc : '-';
                       nuorden = item.nuorden != null ? item.nuorden : '-';
                       nbooking = item.nbooking != null ? item.nbooking : '-';
                       nucnfclt = item.nucnfclt != null ? item.nucnfclt : '-';
                       nucnfliq = item.nucnfliq != null ? item.nucnfliq : '-';

                       oTableDescuadresNorma43
                           .fnAddData([ cuenta, refFecha, refOrden, sentido, tradeDate, settlementDate, importeNorma43,
                                       importeSibbac, tipoImporte, nombreEstadoAsignacionAlc,
                                       nombreEstadoContabilidadAlc, nombreEstadoEntrecAlc, nuorden, nbooking, nucnfclt,
                                       nucnfliq ], false);
                     } // for

                     oTableDescuadresNorma43.fnDraw();
                   } // populateDateTable

                   /**
                     * Muestra un texto con los datos por lo que se ha filtrado una busqueda.
                     */
                   function seguimientoBusqueda () {
                     $('.mensajeBusqueda').empty();
                     var cadenaFiltros = "";

                     cadenaFiltros += "<br/><ins>Filtros de búsqueda</ins><br/><br/>";

                     if ($('#textFechaDesde').val() !== "" && $('#textFechaDesde').val() !== undefined) {
                       cadenaFiltros += " :: Desde trade date: " + $('#textFechaDesde').val();
                     }

                     if ($('#textFechaHasta').val() !== "" && $('#textFechaHasta').val() !== undefined) {
                       cadenaFiltros += " :: Hasta trade date: " + $('#textFechaHasta').val();
                     }

                     if ($('#importeDesde').val() !== "" && $('#importeDesde').val() !== undefined) {
                       cadenaFiltros += " :: Importe Desde: " + $('#importeDesde').val();
                     }

                     if ($('#importeHasta').val() !== "" && $('#importeHasta').val() !== undefined) {
                       cadenaFiltros += " :: Importe Hasta: " + $('#importeHasta').val();
                     }

                     if ($('#selectCuentas').val() !== "" && $('#selectCuentas').val() !== undefined) {
                       cadenaFiltros += " :: Cuenta: "
                                        + $("#selectCuentas option[value='" + $('#selectCuentas').val() + "']").text()
                                            .trim();
                     }

                     if ($('#selectSentido').val() !== "" && $('#selectSentido').val() !== undefined) {
                       cadenaFiltros += " :: Sentido: "
                                        + $("#selectSentido option[value='" + $('#selectSentido').val() + "']").text()
                                            .trim();
                     }

                     if ($('#selectRefOrden').val() !== "" && $('#selectRefOrden').val() !== undefined) {
                       cadenaFiltros += " :: RefOrden: "
                                        + $("#selectRefOrden option[value='" + $('#selectRefOrden').val() + "']")
                                            .text().trim();
                     }

                     cadenaFiltros += " :: "

                     $('.mensajeBusqueda').append(cadenaFiltros);
                   } // seguimientoBusqueda

                   /**
                     *
                     */
                   $(document).ready(function () {

                     TableTools.DEFAULTS.aButtons = [ "copy", "csv", "xls", "pdf", "print" ];
                     TableTools.DEFAULTS.sSwfPath = "/sibbac20/js/swf/copy_csv_xls_pdf.swf";

                     prepareCollapsion();

                     $("#ui-datepicker-div").remove();

                     $('input#textFechaDesde').datepicker({
                       onClose : function (selectedDate) {
                         $("#textFechaHasta").datepicker("option", "minDate", selectedDate);
                       } // onClose
                     });

                     $('input#textFechaHasta').datepicker({
                       onClose : function (selectedDate) {
                         $("#textFechaDesde").datepicker("option", "maxDate", selectedDate);
                       } // onClose
                     });

                     initForm();
                     loadTable_descuadresNorma43();

                     $('#busquedaNorma43Form').submit(function (event) {
                       event.preventDefault();
                       if (validacion()) {
                         searchData();
                         collapseSearchForm();
                         seguimientoBusqueda();
                       }
                       return false;
                     });

                     $('#limpiar').click(function (event) {
                       event.preventDefault();
                       $("#textFechaHasta").datepicker("option", "minDate", "");
                       $("#textFechaDesde").datepicker("option", "maxDate", "");

                       $("#textFechaHasta").val("");
                       $("#textFechaDesde").val("");

                       $('input[type=number]').val('');
                       $('select').val('Todos');
                       $("#selectCuentas").val('');
                     });

                   });

                   // Valida que al menos haya un filtro activo
                   function validacion () {
                     var result = false;
                     if (($('#textFechaDesde').val() !== "" && $('#textFechaDesde').val() !== undefined)
                         || ($('#textFechaHasta').val() !== "" && $('#textFechaHasta').val() !== undefined)
                         || ($('#importeDesde').val() !== "" && $('#importeDesde').val() !== undefined)
                         || ($('#importeHasta').val() !== "" && $('#importeHasta').val() !== undefined)
                         || ($('#selectCuentas').val() !== "" && $('#selectCuentas').val() !== undefined)) {
                       $scope.activeAlert = false;
                       result = true;
                     } else {
                       $scope.activeAlert = true;
                     }
                     return result;
                   }

                 } ]);
