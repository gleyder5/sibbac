(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var ControlEfectivoController = function (service, httpParamSerializer, uiGridConstants, $scope, $timeout) {
		// Sub controlador DynamicFiltersController
		GENERIC_CONTROLLERS.DynamicFiltersController.call(this, service, httpParamSerializer, uiGridConstants);
		// Opciones del modal (480px ancho)
		var fatDialogOpt = { width: 500 }, self = this;
		this.scope = $scope;

		// Resgistramos los modales por la id, y le asignamos una función para el boton con el nombre asignado (Guardar, Modificar, etc)
		this.createModal = this.registryModal("modalSave", "Crear...", {
			Guardar: this.save.bind(this)
		}, fatDialogOpt);
		this.modifyModal = this.registryModal("modalEdit", "Modificar...", {
			Modificar: this.edit.bind(this)
		}, fatDialogOpt);
		this.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					self.resultDialog.dialog("close");
					if (self.result.ok) {
						self.executeQuery();
					}

				}
			}
		});
		// Ejecutamos la función selectQuery y executeQuery cuando obtengamos la query del back
		this.queries.$promise.then(data => {
			this.selectedQuery = data[0];
			this.selectQuery();
			this.executeQuery();
		});
	};


	ControlEfectivoController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

	ControlEfectivoController.prototype.constructor = ControlEfectivoController;

	// Función que se encarga de levantar los modales con una id que identifica su tipo 
	ControlEfectivoController.prototype.openModal = function (id) {
		switch (id) {
			case 'save':
				this.params = {
					id: "",
					descripcion: "",
					numTitulosMax: "",
					importeEfectivoMax: "",
					perTitulosMax: "",
					perEfectivoMax: ""
				};
				this.createModal.dialog("option", "title", "Crear");
				this.createModal.dialog("open");
				break;
			case 'edit':
				if (this.selectedRows().length > 0) {
					// Igualamos los valores a los parámetros que recibiremos en el modal
					this.params = {
						id: this.selectedRows()[0].ID,
						descripcion: this.selectedRows()[0].DESCRIPCION,
						numTitulosMax: this.selectedRows()[0].NUMERO_TITULOS_MAX,
						importeEfectivoMax: this.selectedRows()[0].IMPORTE_EFECTIVO_MAX,
						perTitulosMax: this.selectedRows()[0].PERCENT_TITULOS_MAX,
						perEfectivoMax: this.selectedRows()[0].PERCENT_EFECTIVO_MAX
					};
					this.modifyModal.dialog("option", "title", "Modificar");
					this.modifyModal.dialog("open");
				}
				break;
		}
	};

	// Función encargada de ejecutar el servicio guardar
	ControlEfectivoController.prototype.save = function () {
		if(this.params.id == "" || this.params.descripcion == "" || this.params.importeEfectivoMax == "" || this.params.perEfectivoMax == ""){
			fErrorTxt("Los campos ID, Descripción, Importe Efectivo Máximo y Porcentaje Efectivo Máximo son obligatorios", 1);
		}else if (this.scope.modalForm.$valid) {
			var self = this;

			
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPOST(this.params, {}, function (result) {
				$.unblockUI();
				self.result = result;
				// Ejecuta el modal de respuesta al revicio
				self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				self.resultDialog.dialog("open");
				if(self.result.ok == true){					
					self.createModal.dialog("close");
				}
			}, this.postFail.bind(this));

		}else{
			fErrorTxt("El formato de los valores introducidos es incorrecto", 1);
		} 
	};

	// Función encargada de ejecutar el servicio guardar
	ControlEfectivoController.prototype.edit = function () {
		// El form tiene que ser válido
		if (this.scope.modalForm.$valid) {
			var self = this;

			this.modifyModal.dialog("close");
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPUT(this.params, {}, function (result) {
				$.unblockUI();
				self.result = result;
				// Ejecuta el modal de respuesta al revicio
				self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				self.resultDialog.dialog("open");
				if(self.result.ok == true){	
					self.modifyModal.dialog("close");
				}
			}, this.postFail.bind(this));
		}else if(this.params.descripcion == "" || this.params.importeEfectivoMax == "" || this.params.perEfectivoMax == ""){
			fErrorTxt("Los campos Descripción, Importe Efectivo Máximo y Porcentaje Efectivo Máximo son obligatorios", 1);
		}else{
			fErrorTxt("El formato de los valores introducidos es incorrecto", 1);
		}
	};

	ControlEfectivoController.prototype.delete = function(){
		
		if (this.selectedRows().length > 0) {
			var self = this;
			var deleteObj = [];
			this.selectedRows().forEach(element =>{
				deleteObj.push(element.ID);
			});

		angular.element("#dialog-confirm").html("¿Desea eliminar el/los registro/s seleccionado/s?");

		// Define the Dialog and its properties.
			this.confirmDialog = angular.element("#dialog-confirm").dialog({
				resizable: false,
				modal: true,
				title: "Mensaje de Confirmación",
				height: 150,
				width: 360,
				buttons: {
					" Sí ": function () {
						$(this).dialog('close');
						eliminar();
					},
					" No ": function () {
						$(this).dialog('close');
					}
				}
			});
			
			function eliminar()
			{
				// Inicia el "Cargando datos..."
				inicializarLoading();
				// Ejecutamos el servicio pasandole por parámetros [deleteObj]
				self.service.executeActionDEL(deleteObj, {}, function (result) {
					$.unblockUI();
					self.result = result;
					// Ejecuta el modal de respuesta al revicio
					self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					self.resultDialog.dialog("open");
	
				}, self.postFail.bind(self));
			}
		}
		else
		{
			fErrorTxt("Debe seleccionar una o más filas para poder realizar esta opción", 1);
		}
	};

	sibbac20.controller("ControlEfectivoController", ["ControlEfectivoService", "$httpParamSerializer", "uiGridConstants",
		"$scope", "$timeout", ControlEfectivoController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);