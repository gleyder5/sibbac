'use strict';
sibbac20.controller('OperacionesFallidasController', ['$scope', '$http', '$rootScope','IsinService', function ($scope, $http, $rootScope, IsinService) {

	// Tabla de operaciones fallidas
	var oTableOperacionesFallidas = undefined;

	// Tabla de afectaciones
	var oTableAfectaciones = undefined;

	// Tabla de operación fallida seleccionada
	var oTableOperacionFallidaSeleccionada = undefined;

	//Lista con los cdaliass
	var listaAlias = [];

	// Lista con el contenido a mostrar en el autocomplete de alias
	var availableTagsAlias = [];

	//Lista con los cdisin
	var listaIsin = [];

	// Lista con el contenido a mostrar en el autocomplete de isin
	var availableTagsIsin = [];

	//Lista con los cdcamara
	var listaCamara = [];

	//Lista con el contenido a mostrar en el autocomplete de cámara
	var availableTagsCamara = [];

	/**
	 * Carga la tabla de operaciones fallidas
	 */
	function loadTable_OperacionesFallidas() {
	  console.log("loadTable_OperacionesFallidas");
	  oTableOperacionesFallidas = $("#tablaOperacionesFallidas").dataTable({
	    "sDom": 'T<"clear">lfrtip',
	    "oTableTools" : {
	      "aButtons" : [ "copy",
	                     { "sExtends" : "csv",
	                       "sFileName": "Listado_de_operaciones_fallidas.csv"
	                       },
	                       {
	                         "sExtends" : "xls",
	                      	 "sFileName": "Listado_de_operaciones_fallidas.xls"
	                       },
	                       {
	                         "sExtends" : "pdf",
	                      	 "sPdfOrientation": "landscape",
	                      	 "sTitle": " ",
	                      	 "sPdfSize": "A4",
	                      	 "sPdfMessage": "Listado de operaciones fallidas",
	                      	 "sFileName": "Listado_de_operaciones_fallidas.pdf",
	                      	 "mColumns": "visible"
	                       },
	                       "print"
	                     ]
	    },
	    "language": {
	      "url": "i18n/Spanish.json"
	    },
	    "scrollX": "100%",
	    "scrollY": "480px",
	    "scrollCollapse": true,
	    "aoColumns": [
	      {sClass: "centrar", bSortable: false}, // Opciones sobre el registro
	      {sClass: "centrar", sType: "date-eu"}, // Fecha de ejecución
	      {sClass: "centrar", sType: "date-eu"}, // Fecha de liquidación
	      {sClass: "centrar"}, // Isin
	      {sClass: "align-left", bSortable: false}, // Descipción isin
	      {sClass: "centrar"}, // Alias
	      {sClass: "align-left", bSortable: false}, // Descipción alias
	      {sClass: "centrar"}, // Número de IL ECC
	      {sClass: "align-right", sType: "formatted-num"}, // Títulos fallidos
	      {sClass: "align-right", sType: "formatted-num"}, // Importe efectivo fallido
	      {sClass: "align-right", sType: "formatted-num"}, // Corretaje
	      {sClass: "centrar"} // Cámara
	    ],
	    "bProcessing": true,
	    "order": [[ 1, "asc" ]]
	  });
	} // loadTable_OperacionesFallidas

	/**
	 * Carga la tabla operación fallida seleccionada.
	 */
	function loadTable_OperacionFallidaSeleccionada() {
	  oTableOperacionFallidaSeleccionada = $("#tablaOperacionFallidaSeleccionada").dataTable({
	    "paging":   false,
	    "ordering": false,
	    "info":     false,
	    "filter":     false,
	    "scrollY": "480px",
	    "scrollCollapse": true,
	    "aoColumns": [
	      {sClass: "centrar", sType: "date-eu"}, // Fecha de ejecución
	      {sClass: "centrar", sType: "date-eu"}, // Fecha de liquidación
	      {sClass: "centrar"}, // Isin
	      {sClass: "align-left"}, // Descipción isin
	      {sClass: "centrar"}, // Alias
	      {sClass: "align-left"}, // Descipción alias
	      {sClass: "centrar"}, // Número de IL ECC
	      {sClass: "align-right", sType: "formatted-num"}, // Títulos fallidos
	      {sClass: "align-right", sType: "formatted-num"}, // Importe efectivo fallido
	      {sClass: "align-right", sType: "formatted-num"}, // Corretaje
	      {sClass: "centrar"} // Cámara
	    ]
	  });
	} // loadTable_OperacionFallidaSeleccionada

	/**
	 * Carga la tabla de afectaciones.
	 */
	function loadTable_Afectaciones() {
	  oTableAfectaciones = $("#tablaAfectaciones").dataTable({
	    "dom": 'T<"clear">lfrtip',
	    "oTableTools" : {
	        "aButtons" : [ { "sExtends" : "copy",
	        	             "mColumns": "visible",
	        	             "sAction": "flash_copy"
	                       },
	                       { "sExtends" : "csv",
	        	             "sFileName": "Listado_de_afectaciones.csv"
	                       },
	                       {
	                         "sExtends" : "xls",
	                      	 "sFileName": "Listado_de_afectaciones.xls"
	                       },
	                       {
	                         "sExtends" : "pdf",
	                      	 "sPdfOrientation": "landscape",
	                      	 "sTitle": " ",
	                      	 "sPdfSize": "A4",
	                      	 "sPdfMessage": "Listado_de_afectaciones",
	                      	 "sFileName": "Listado_de_afectaciones.pdf",
	                      	 "mColumns": "visible"
	                       },
	                       "print"
	                     ]
	    },
	    "language": {
	      "url": "i18n/Spanish.json"
	    },
	    "scrollX": "100%",
	    "scrollY": "350px",
	    "scrollCollapse": true,
	    "paging": false,
	    "aoColumns": [
	      {sClass: "centrar", bSortable: false}, // Opciones sobre el registro
	      {sClass: "centrar", bVisible: false}, // Orden
	      {sClass: "centrar", bVisible: false}, // Booking
	      {sClass: "centrar", bVisible: false}, // Alo
	      {sClass: "centrar", bVisible: false}, // Alc
	      {sClass: "centrar", bVisible: false}, // Desglosecamara
	      {sClass: "centrar", bSortable: false}, // Desgloseclitit
	      {sClass: "align-right", sType: "formatted-num"}, // Títulos operación
	      {sClass: "align-right", sType: "formatted-num"}, // Precio
	      {sClass: "centrar"}, // Alias
	      {sClass: "centrar", sType: "date-eu"}, // Fecha de ejecución
	      {sClass: "centrar"}, // Hora de ejecución
	      {sClass: "align-right", sType: "formatted-num"}, // Títulos afectados
	      {sClass: "align-right", sType: "formatted-num"} // Saldo neto
	    ],
	    "fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
	      var totalTitulosAfectados = 0;

	      for ( var i = 0 ; i < aaData.length ; i++ ) {
	        totalTitulosAfectados += parseFloat(aaData[i][12].replace(/\./g, "").replace(",", "."));
	      }

	      var nCells = nRow.getElementsByTagName('th');
	      nCells[6].innerHTML = "Total t&iacute;tulos afectados:<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Total t&iacute;tulos fallidos:";
	      nCells[7].innerHTML = $.number(totalTitulosAfectados, 2, ',', '.') + "<br><br>" + $('#tablaOperacionFallidaSeleccionada tbody tr td').eq(7).text();
		},
		"bProcessing": true,
	    "order": [[12, 'desc']]
	  });
	} // loadTable_Afectaciones

	/**
	 * Carga la lista de alias y modifica el campo 'textAlias' del formulario para que sea un campo autocompletable.
	 */
	function cargarAlias() {
	  $("#textAlias").html("");
	  var params = new DataBean();
	  params.setService('SIBBACServiceTmct0ali');
	  params.setAction('getAlias');

	  var request = requestSIBBAC(params);
	  request.success(function(json) {
	    var datos = undefined;
	    if (json === undefined || json.resultados === undefined || json.resultados.result_alias === undefined) {
	      datos = {};
	    } else {
	      datos = json.resultados.result_alias;

	      $.each(datos, function (index, value) {
	        listaAlias.push(datos[index].alias);
	        availableTagsAlias.push(datos[index].alias + " - " + datos[index].descripcion);
	      });

	      $("#textAlias").autocomplete({
	        source : availableTagsAlias
	      });
	    } // else
	  });
	  request.error( function(c) {
		alert( "ERROR al ejecutar SIBBACServiceTmct0ali#getAlias: " + c );
	  });
	} // cargarAlias

	/**
	 * Carga la lista de isin y modifica el campo 'textIsin' del formulario para que sea un campo autocompletable.
	 */
	function cargarIsin() {
	  $("#textIsin").html("");
	  var params = new DataBean();
	  params.setService('SIBBACServiceConciliacion');
	  params.setAction('getIsin');

	  var request = requestSIBBAC(params);
	  request.success(function(json) {
	    var datos = undefined;
	    if (json === undefined || json.resultados === undefined || json.resultados.result_isin === undefined) {
	      datos = {};
	    } else {
	      datos = json.resultados.result_isin;

	      $.each(datos, function (index, value) {
	        listaIsin.push(datos[index].codigo);
	        availableTagsIsin.push(datos[index].codigo + " - " + datos[index].descripcion);
	      });

	      $("#textIsin").autocomplete({
	        source : availableTagsIsin
	      });
	    } // else
	  });
	  request.error( function(c) {
		alert( "ERROR al ejecutar SIBBACServiceConciliacion#getIsin: " + c );
	  });
	} // cargarIsin

	/**
	 * Carga la lista de cámara y modifica el campo 'textCamara' del formulario para que sea un campo autocompletable.
	 */
	function cargarCamara() {
	  $("#textCamara").html("");
	  var params = new DataBean();
	  params.setService('SIBBACServiceConciliacion');
	  params.setAction('getCamara');

	  var request = requestSIBBAC(params);
	  request.success(function(json) {
	    var datos = undefined;
	    if (json === undefined || json.resultados === undefined || json.resultados.result_camara === undefined) {
	      datos = {};
	    } else {
	      datos = json.resultados.result_camara;
	      var item = null;

	      $.each(datos, function (index, value) {
	        listaCamara.push(datos[index].cdCodigo);
	        availableTagsCamara.push(datos[index].cdCodigo + " - " + datos[index].nbDescripcion);
	      });

	      $("#textCamara").autocomplete({
	        source : availableTagsCamara
	      });
	    } // else
	  });
	  request.error( function(c) {
		alert( "ERROR al ejecutar SIBBACServiceConciliacion#getCamara: " + c );
	  });
	} // cargarCamara

	/**
	 * Carga la lista de cuentas de liquidación y modifica el campo 'selectCuentaLiquidacion' del formulario para que sea
	 * un campo autocompletable.
	 */
	function cargarCuentaLiquidacion() {
	  $("#selectCuentaLiquidacion").html("");
	  var params = new DataBean();
	  params.setService('SIBBACServiceConciliacionClearing');
	  params.setAction('getCuentasLiquidacion');

	  var request = requestSIBBAC(params);
	  request.success(function(json) {
	    var datos = undefined;
	    if (json === undefined || json.resultados === undefined || json.resultados.cuentasLiquidacion === undefined) {
	      datos = {};
	    } else {
	      datos = json.resultados.cuentasLiquidacion;

	      var select = $("#selectCuentaLiquidacion");
	      select.append("<option value=''>Seleccione una cuenta de liquidación ...</option>");
	      $.each(datos, function (index, value) {
	        $("#selectCuentaLiquidacion").append('<option value=' + datos[index].id + '>&nbsp;' + datos[index].name + '</option>');
	      });
	    } // else
	  });
	  request.error( function(c) {
		alert( "ERROR al ejecutar SIBBACServiceConciliacionClearing#getCuentasLiquidacion: " + c );
	  });
	} // cargarCuentaLiquidacion

	/**
	 * Carga la tabla con el listado de operaciones fallidas pendientes de liquidar.
	 */
	function cargarTablaOperacionesFallidas() {
	  inicializarLoading();

	  var params = new DataBean();
	  params.setService('SIBBACServiceOperacionFallida');
	  params.setAction('listOperacionesFallidas');

	  var request = requestSIBBAC(params);
	  request.success(function(json) {
	    var datos = undefined;
	    if (json === undefined || json.resultados === undefined || json.resultados.listadoOperacionFallida === undefined) {
	      datos = {};
	    } else {
	      datos = json.resultados.listadoOperacionFallida;

	      populateDateTable(oTableOperacionesFallidas, datos);
	      $.unblockUI();
	    } // else
	  });
	  request.error( function(c) {
		  alert( "ERROR al ejecutar SIBBACServiceOperacionFallida#listOperacionesFallidas: " + c );
		  $.unblockUI();
	  });
	} // cargarTablaOperacionesFallidas

	/**
	 * Prepara la tabla de operaciones fallidas y la visualiza en pantalla.
	 *
	 * @param table Tabla a preparar.
	 * @param datos Datos a insertar en la tabla.
	 */
	function populateDateTable(table, datos) {
	  var feejecuc;
	  var sttlDt;
	  var isin;
	  var descripcionIsin;
	  var alias;
	  var subCuenta;
	  var descripcionAlias;
	  var numeroOperacionIlEcc;
	  var titulosFallidosOperacion;
	  var importeEfectivoFallido;
	  var corretaje;
	  var camara;

	  table.fnClearTable();
	  var item = null;

	  for (var k in datos) {
	    item = datos[ k ];

	    feejecuc = item.feejecuc != null ? EpochToHuman(item.feejecuc) : '';
	    sttlDt = item.sttlDt != null ? EpochToHuman(item.sttlDt).split(" ")[0] : '';
	    isin = item.isin != null ? item.isin : '';
	    descripcionIsin = item.descripcionIsin != null ? item.descripcionIsin : '';
	    alias = item.alias != null ? item.alias : '';
	    descripcionAlias = item.descripcionAlias != null ? item.descripcionAlias : '';
	    numeroOperacionIlEcc = item.numeroOperacionIlEcc != null ? item.numeroOperacionIlEcc : '';
	    titulosFallidosOperacion = item.titulosFallidosOperacion != null ? $.number(item.titulosFallidosOperacion, 2, ',', '.') : '';
	    importeEfectivoFallido = item.importeEfectivoFallido != null ? $.number(item.importeEfectivoFallido, 6, ',', '.') : '';
	    corretaje = item.corretaje != null ? $.number(item.corretaje, 2, ',', '.') : '';
	    camara = item.camara != null ? item.camara : '';

	    table.fnAddData([
	      "<a style=\"cursor:pointer;\" onclick=\"afectarFallido('" + item.id + "','" + feejecuc + "','" + sttlDt
	                                                                + "','" + isin + "','" + descripcionIsin
	                                                                + "','" + alias + "','" + descripcionAlias
	                                                                + "','" + numeroOperacionIlEcc
	                                                                + "','" + titulosFallidosOperacion + "','" + importeEfectivoFallido
	                                                                + "','" + corretaje + "','" + camara
	                                                                + "');\"><img src='img/editp.png'  title='Afectar operaci&oacute;n'/></a>",
	      "<span>" + feejecuc + "</span>",
	      "<span>" + sttlDt + "</span>",
	      "<span>" + isin + "</span>",
	      "<span>" + descripcionIsin + "</span>",
	      "<span>" + alias + "</span>",
	      "<span>" + descripcionAlias + "</span>",
	      "<span>" + numeroOperacionIlEcc + "</span>",
	      "<span>" + titulosFallidosOperacion + "</span>",
	      "<span>" + importeEfectivoFallido + "</span>",
	      "<span>" + corretaje + "</span>",
	      "<span>" + camara + "</span>"
	    ], false);
	  } // for

	  table.fnDraw();
	} // populateDateTable

	/**
	 * Carga la tabla con la operación fallida seleccionada.
	 *
	 * @param id
	 * @param feejecuc
	 * @param sttlDt
	 * @param isin
	 * @param descripcionIsin
	 * @param alias
	 * @param subCuenta
	 * @param descripcionAlias
	 * @param numeroOperacionIlEcc
	 * @param titulosFallidosOperacion
	 * @param importeEfectivoFallido
	 * @param corretaje
	 * @param camara
	 */
	function afectarFallido(id, feejecuc, sttlDt, isin, descripcionIsin, alias, descripcionAlias, numeroOperacionIlEcc,
			                titulosFallidosOperacion, importeEfectivoFallido, corretaje, camara) {
		inicializarLoading();
		$("#altaOperacionFallidaForm").get(0).reset();
		if ($(".collapser_search").text().indexOf('Ocultar') !== -1) {
		  $('.collapser_search').click();
		}
	  $('#title_principal').hide();
	  $('.contenedorTablaOperacionesFallidas').hide();

	  oTableOperacionFallidaSeleccionada.fnClearTable();

	  oTableOperacionFallidaSeleccionada.fnAddData([
	    feejecuc,
	    sttlDt,
	    isin,
	    descripcionIsin,
	    alias,
	    descripcionAlias,
	    numeroOperacionIlEcc,
	    titulosFallidosOperacion,
	    importeEfectivoFallido,
	    corretaje,
	    camara
	  ], true);

	  document.getElementById('idFallido').value = id;
	  document.getElementById('titulosFallidosOperacion').value = titulosFallidosOperacion.replace(/\./g, "").replace(",", ".");

	  mostrarListaAfectaciones();
	} // afectarFallido

	/**
	 *
	 * @param id
	 * @param titulosFallidosOperacion
	 */
	function mostrarListaAfectaciones() {
	  var filtro = "{\"operacionFallidaId\" : \"" + document.getElementById('idFallido').value + "\" }";

	  var params = new DataBean();
	  params.setService('SIBBACServiceAfectaciones');
	  params.setAction('listAfectaciones');
	  params.setFilters(filtro);

	  var request = requestSIBBAC(params);
	  request.success(function(json) {
	    var datos = undefined;
	    if (json === undefined || json.resultados === undefined || json.resultados.listadoAfectaciones === undefined) {
	      datos = {};
	    } else {
	      datos = json.resultados.listadoAfectaciones;

	      oTableAfectaciones.fnClearTable();
	      var item = null;

	      document.getElementById('salvarAfectaciones').style.display = 'none';
	      for (var k in datos) {
	        item = datos[ k ];

	        if (item.editable === true) {
	          afectar = "<a data-editable='" + item.editable
	                    + "' class='btn' "
	                    + "onclick=\"modificarTitulosAfectados('" + item.nuTitAfectados + "','" + item.imtitulos + "');\"><img src='img/editp.png' title='Modificar t&iacute;tulos afectados'/></a>";
	          document.getElementById('salvarAfectaciones').style.display = 'inline';
	        } else {
	          // afectar = "<a data-editable=\"" + item.editable + "\" class='btn' onclick=\"return false;\" />";
			  afectar = "";
	          document.getElementById('salvarAfectaciones').style.display = 'none';
	       }
	        oTableAfectaciones.fnAddData([afectar,
	          item.nuorden,
	          item.nbooking,
	          item.nucnfclt,
	          item.nucnfliq,
	          item.iddesglosecamara,
	          item.nudesglose,
	          $.number(item.imtitulos, 2, ',', '.'),
	          $.number(item.imprecio, 6, ',', '.'),
	          item.cdalias,
	          transformaFechaGuion(item.feejecuc),
	          item.hoejecuc,
	          $.number(item.nuTitAfectados, 2, ',', '.'),
	          $.number(item.saldoNeto, 2, ',', '.')
	        ], false);
	      }

	      oTableAfectaciones.fnDraw();
	      $('.contenedorResultados').show();
	      oTableAfectaciones.fnAdjustColumnSizing();
	      oTableOperacionFallidaSeleccionada.fnAdjustColumnSizing();

	      oTableTools = TableTools.fnGetInstance('tablaAfectaciones');
	      // if (oTableTools != null && oTableTools.fnResizeRequired()){
	        oTableTools.fnResizeButtons();
	      // }

	      $.unblockUI();
	    } // else
	  });
	  request.error( function(c) {
	    alert( "ERROR al ejecutar SIBBACServiceAfectaciones#listAfectaciones: " + c );
	    $.unblockUI();
	    $("section").load("views/fallidos/operacionFallida.html");
	  });
	} // mostrarListaAfectaciones

	/**
	 *
	 */
	function modificarTitulosAfectados(nuTitAfectados, imtitulos) {
	  mostrarPopupModificarNuTitAfectados ();

	  document.getElementById('nuTitAfectadosPrevio').value = nuTitAfectados;
	  document.getElementById('nuTitAfectados').value = nuTitAfectados;
	  document.getElementById('imtitulos').value = imtitulos;

	  $('#tablaAfectaciones tbody').on('click', 'tr', function () {
		  document.getElementById('filaModificada').value = oTableAfectaciones.fnGetPosition( this );
	  });
	} // modificarTitulosAfectados

	/**
	 *
	 * @returns {Boolean}
	 */
	function comprobarTitulosAfectados(){
	  var nuTitAfectadosPrevio = document.getElementById('nuTitAfectadosPrevio').value;
	  var nuTitAfectados = document.getElementById('nuTitAfectados').value;
	  var imtitulos = document.getElementById('imtitulos').value;

	  if (parseFloat(nuTitAfectados) > parseFloat(imtitulos)) {
	    alert("El número de títulos afectados (" + $.number(nuTitAfectados, 0, ",", ".") + ") no puede ser mayor que el número de títulos de la operación (" + $.number(imtitulos, 0, ",", ".") + ").");
	    document.getElementById('nuTitAfectados').value = nuTitAfectadosPrevio;
	    return false;
	  }

	  oTableAfectaciones.fnUpdate($.number(nuTitAfectados, 2, ',', '.'), document.getElementById('filaModificada').value, 12 );

	  ocultarPopupModificarNuTitAfectados();
	} // comprobarTitulosAfectados

	/**
	 *
	 */
	function salvarAfectaciones(){
	  if (sumatorioNuTitAfectados()) {
	    var sendParams = new DataBean();
	    var filtro = "{\"operacionFallidaId\" : \"" + document.getElementById('idFallido').value + "\" }";

	    var params = "[ ";
	    var editable = false;
	    var nuorden;
	    var nbooking;
	    var nucnfclt;
	    var nucnfliq;
	    var iddesglosecamara;
	    var nudesglose;
	    var imtitulos;
	    var imprecio;
	    var cdalias;
	    var feejecuc;
	    var hoejecuc;
	    var nuAfec;
	    var saldoNeto;

	    // Con columnas ocultas
	    var rows = $('#tablaAfectaciones').dataTable().fnGetNodes();
	    for(var i=0;i<rows.length;i++) {
	      editable = $('a', $(rows[i]).find("td:eq(0)")).data('editable');
	      nuorden = $.trim($(oTableAfectaciones.fnGetData(rows[i]))[1]);
	      nbooking = $.trim($(oTableAfectaciones.fnGetData(rows[i]))[2]);
	      nucnfclt = $.trim($(oTableAfectaciones.fnGetData(rows[i]))[3]);
	      nucnfliq = $.trim($(oTableAfectaciones.fnGetData(rows[i]))[4]);
	      iddesglosecamara = $.trim($(oTableAfectaciones.fnGetData(rows[i]))[5]);
	      nudesglose = $.trim($(oTableAfectaciones.fnGetData(rows[i]))[6]);
	      imtitulos = $(oTableAfectaciones.fnGetData(rows[i]))[7].replace(/\./g, "").replace(",", ".");
	      imprecio = $(oTableAfectaciones.fnGetData(rows[i]))[8].replace(/\./g, "").replace(",", ".");
	      cdalias = $.trim($(oTableAfectaciones.fnGetData(rows[i]))[9]);
	      feejecuc = $(oTableAfectaciones.fnGetData(rows[i]))[10];
	      hoejecuc = $(oTableAfectaciones.fnGetData(rows[i]))[11];
	      nuAfec = $(oTableAfectaciones.fnGetData(rows[i]))[12].replace(/\./g, "").replace(",", ".");
	      saldoNeto = ($(oTableAfectaciones.fnGetData(rows[i]))[13]).replace(/\./g, "").replace(",", ".");

	      params = params + "{" + "\"editable\" : \"" + editable + "\"," +
	        "\"nuorden\" : \"" + nuorden + "\"," +
	        "\"nbooking\" : \"" + nbooking + "\"," +
	        "\"nucnfclt\" : \"" + nucnfclt + "\"," +
	        "\"nucnfliq\" : \"" + nucnfliq + "\"," +
	        "\"iddesglosecamara\" : \"" + iddesglosecamara + "\"," +
	        "\"nudesglose\" : \"" + nudesglose + "\"," +
	        "\"imtitulos\" : \"" + imtitulos + "\"," +
	        "\"imprecio\" : \"" + imprecio + "\"," +
	        "\"cdalias\" : \"" + cdalias + "\"," +
	        "\"feejecuc\" : \"" + feejecuc + "\"," +
	        "\"hoejecuc\" : \"" + hoejecuc + "\"," +
	        "\"nuTitAfectados\" : \"" + nuAfec + "\"," +
	        "\"saldoNeto\" : \"" + saldoNeto + "\"},";
	    }

	 // Sin columnas ocultas
//	    $('#tablaAfectaciones tbody tr').each(function(){
//	      editable = $('a', $('td', this).eq(0)).data('editable');
//	      console.log("editable: " + editable);
//
//	      nuorden = $.trim($('td', this).eq(1).text());
//	      nbooking = $.trim($('td', this).eq(2).text());
//	      nucnfclt = $.trim($('td', this).eq(3).text());
//	      nucnfliq = $.trim($('td', this).eq(4).text());
//	      iddesglosecamara = $.trim($('td', this).eq(5).text());
//	      nudesglose = $.trim($('td', this).eq(6).text());
//	      imtitulos = $('td', this).eq(7).text().replace(/\./g, "").replace(",", ".");
//	      imprecio = $('td', this).eq(8).text().replace(/\./g, "").replace(",", ".");
//	      cdalias = $.trim($('td', this).eq(9).text());
//	      feejecuc = $('td', this).eq(10).text();
//	      hoejecuc = $('td', this).eq(11).text();
//	      nuAfec = $('td', this).eq(12).text().replace(/\./g, "").replace(",", ".");
//	      saldoNeto = $('td', this).eq(13).text().replace(/\./g, "").replace(",", ".");
//
//	      params = params + "{" + "\"editable\" : \"" + editable + "\"," +
//	        "\"nuorden\" : \"" + nuorden + "\"," +
//	        "\"nbooking\" : \"" + nbooking + "\"," +
//	        "\"nucnfclt\" : \"" + nucnfclt + "\"," +
//	        "\"nucnfliq\" : \"" + nucnfliq + "\"," +
//	        "\"iddesglosecamara\" : \"" + iddesglosecamara + "\"," +
//	        "\"nudesglose\" : \"" + nudesglose + "\"," +
//	        "\"imtitulos\" : \"" + imtitulos + "\"," +
//	        "\"imprecio\" : \"" + imprecio + "\"," +
//	        "\"cdalias\" : \"" + cdalias + "\"," +
//	        "\"feejecuc\" : \"" + feejecuc + "\"," +
//	        "\"hoejecuc\" : \"" + hoejecuc + "\"," +
//	        "\"nuTitAfectados\" : \"" + nuAfec + "\"," +
//	        "\"saldoNeto\" : \"" + saldoNeto + "\"},";
//	    });

	    params = params.substring(0, params.length - 1);
	    params = params + "]";

	    sendParams.setService('SIBBACServiceAfectaciones');
	    sendParams.setAction('saveAfectaciones');
	    sendParams.setFilters(filtro);
	    sendParams.setParams(params);

	    var request = requestSIBBAC(sendParams);
	    request.success(function(json) {
	      var datos = undefined;
	      if (json === undefined || json.resultados === undefined || json.resultados.resultado === undefined) {
	        alert("ERROR :: Cadena de resultados vacía");
	      } else {
	        if (json.resultados.resultado === "SUCCESS") {
	          alert("Lista de afectaciones almacenada correctamente.");
	          $("section").load("views/fallidos/operacionFallida.html");
	        } else if (json.resultados.resultado === "FAIL") {
	          alert("Error almacenando afectaciones ... " + json.resultados.error_message);
	        } else {
	          alert("ERROR :: Resultado no controlado: " + json.resultados.resultado);
	        }
	        $.unblockUI();
	      } // else
	    });
	    request.error( function(c) {
	      alert( "ERROR al ejecutar SIBBACServiceAfectaciones#saveAfectaciones: " + c );
	      $.unblockUI();
	    });
	  } // if (sumatorioNuTitAfectados())
	}

	/**
	 *
	 */
	function sumatorioNuTitAfectados() {
	  var sumatorio = 0;

	  $('#tablaAfectaciones tbody tr').each(function(){
	    sumatorio += parseFloat($('td', this).eq(7).text().replace(/\./g, "").replace(",", "."));
	  });

	  var titulosFallidosOperacion = document.getElementById('titulosFallidosOperacion').value;

	  if (sumatorio > parseFloat(titulosFallidosOperacion)){
	    alert("El número de títulos afectados(" + $.number(sumatorio, 2, ",", ".") + ") es mayor que el número de títulos fallidos de la operación (" + $.number(titulosFallidosOperacion, 2, ",", ".") + ").");
	    return false;
	//  } if (sumatorio < parseFloat(titulosFallidosOperacion)){
//	    // compropara cada registro que el campo nuTitAfectados es igual al contenido del campo Imtitulos, si se cumple esta condición se le
//	    //permitirá grabar, en caso contrario se le debe informar de que quedan títulos fallidos por afectar y no permitirle continuar.bamos que cada uno
//	    contador = 0;
//	    nuAfec = 0;
//	    $('.tablaAfectaciones tr').each(function () {
//	        nuAfec = $('.tablaAfectaciones tr').find('#nuAfec' + contador).html();
//	        imtitulos = $('.tablaAfectaciones tr').find('#imtitulos' + contador).html();
//	        if (parseFloat(nuAfec) != parseFloat(imtitulos))
//	        {
//	            alert("Quedan t\u00edtulos fallidos por afectar");
//	            return false;
//	        }
//	        contador++;
//	    });
	  } else {
	    return true;
	  }
	} // sumatorioNuTitAfectados

	/**
	 * Comprueba los datos introducidos en el formulario de alta y ejecuta la acción de salvado de la misma.
	 * @param event
	 * @returns {Boolean}
	 */
	function submitForm(event) {
	  event.preventDefault();

	  // Se obtiene el alias insertado
	  var seleccionListaAlias = document.getElementById("textAlias").value;
	  var posicionAlias = availableTagsAlias.indexOf(seleccionListaAlias);
	  if(posicionAlias == "-1"){
	    alert("El alias introducido no existe. Seleccione uno de la lista de autocompletado.");
	    return false;
	  }
	  var valorSeleccionadoAlias = listaAlias[posicionAlias];
	  var descripcionAlias = getAutocompleteDescription(seleccionListaAlias);

	  // Se obtiene el isin insertado
	  var seleccionListaIsin = document.getElementById("textIsin").value;
	  var posicionIsin = availableTagsIsin.indexOf(seleccionListaIsin);
	  if (posicionIsin == "-1") {
	    alert("El isin introducido no existe. Seleccione uno de la lista de autocompletado.");
	    return false;
	  }
	  var valorSeleccionadoIsin = listaIsin[posicionIsin];
	  var descripcionIsin = getAutocompleteDescription(seleccionListaIsin);

	  // Se obtiene la camara insertada
	  var seleccionListaCamara = document.getElementById("textCamara").value;
	  var posicionCamara = availableTagsCamara.indexOf(seleccionListaCamara);
	  if (posicionCamara == "-1") {
	    alert("La cámara introducida no existe. Seleccione una de la lista de autocompletado.");
	    return false;
	  }
	  var valorSeleccionadoCamara = listaCamara[posicionCamara];

	  // Se obtiene el número de operación ECC insertado
	  var valorSeleccionadoNumOpEcc = document.getElementById("textNumOpEcc").value;
	  if (valorSeleccionadoNumOpEcc.length !== 16) {
	    alert("El número de operación ECC debe tener una lóngitud de 16 caracteres.");
	    return false;
	  }

	  // Se obtiene la cuenta de liquidación seleccionada
	  // var valorSeleccionadoCuentaLiquidacion = $("#selectCuentaLiquidacion option:selected").val();
	  var valorSeleccionadoCuentaLiquidacion = $("#selectCuentaLiquidacion option:selected").text().trim();

	  // Se obtiene la fecha de ejecución
	  var valorSeleccionadoFechaEje = transformaFechaInv(document.getElementById('textFechaEje').value);

	  // Se obtiene la fecha de ejecución
	  var valorSeleccionadoFechaLiq = transformaFechaInv(document.getElementById('textFechaLiq').value);

	  // Se obtiene el número de títulos fallidos
	  var valorSeleccionadoNumTit = document.getElementById('textNumTit').value;

	  // Se obtiene el importe efectivo fallido
	  var valorSeleccionadoImporteEfectivoFallido = document.getElementById('textImporteEfectivoFallido').value;

	  // Se obtiene el corretaje
	  var valorSeleccionadoCorretaje = document.getElementById('textCorretaje').value;

	  var params = "[{\"Alias\":\"" + valorSeleccionadoAlias
	    + "\",\"DescripcionAlias\":\"" + descripcionAlias
	    + "\",\"Isin\" : \"" + valorSeleccionadoIsin
	    + "\",\"DescripcionIsin\":\"" + descripcionIsin
	    + "\",\"TitulosFallidosOperacion\":\"" + valorSeleccionadoNumTit
	    + "\",\"TitulosFallidosOperacionIL\":\"" + valorSeleccionadoNumTit
	    + "\",\"TitulosOperacionIL\":\"" + valorSeleccionadoNumTit
	    + "\",\"Feejecuc\":\"" + valorSeleccionadoFechaEje
	    + "\",\"Sttldt\":\"" + valorSeleccionadoFechaLiq
	    + "\",\"Camara\":\"" + valorSeleccionadoCamara
	    + "\",\"cuentaLiquidacion\":\"" + valorSeleccionadoCuentaLiquidacion
	    + "\",\"NumeroOperacionIlEcc\":\"" + valorSeleccionadoNumOpEcc
	    + "\",\"importeEfectivoFallido\":\"" + valorSeleccionadoImporteEfectivoFallido
	    + "\",\"corretaje\":\"" + valorSeleccionadoCorretaje
	    + "\"}]";

	  var data = new DataBean();
	  data.setService('SIBBACServiceOperacionFallida');
	  data.setAction('createOperacionFallida');
	  data.setParams(params);

	  var request = requestSIBBAC(data);
	  request.success(function (json) {
	    alert("Operación fallida insertada correctamente.");
	    $("section").load("views/fallidos/operacionFallida.html");
	  });
	  request.error( function(c) {
	    alert( "ERROR al ejecutar SIBBACServiceOperacionFallida#createOperacionFallida: " + c );
	  });
	} // submitForm

	/**
	 * Obtiene del elemento pasado su descripción si está separada del Id por un guión.
	 * @returns
	 */
	function getAutocompleteDescription(tagValue) {
	  var indexSeparador = tagValue.indexOf("-");
	  return (indexSeparador > 0) ? tagValue.substring(indexSeparador + 1).trim() : tagValue.trim();
	} // getAutocompleteDescription

	/**
	 *
	 */
	function ocultarPopupModificarNuTitAfectados (){
	  document.getElementById('operacionFallidaForm').style.display = 'none';
	  document.getElementById('fade').style.display = 'none';
	} // ocultarPopupModificarNuTitAfectados

	/**
	 * Muestra el div popup de modificación de la columna número de titulos afectados.
	 */
	function mostrarPopupModificarNuTitAfectados () {
	  document.getElementById('operacionFallidaForm').style.display='block';
	  document.getElementById('fade').style.display='block';
	} // mostrarPopupModificarNuTitAfectados

	/**
	 *
	 */
	$(document).ready(function () {
	  TableTools.DEFAULTS.aButtons = [ "copy", "csv", "xls", "pdf", "print" ];
	  TableTools.DEFAULTS.sSwfPath = "/sibbac20/js/swf/copy_csv_xls_pdf.swf";

	  $('.collapser_search').click(function (event) {
	    event.preventDefault();
	    $(".collapser_search").parents('.title_section').next().slideToggle();
	    $(".collapser_search").parents('.title_section').next().next('.button_holder').slideToggle();
	    $(".collapser_search").toggleClass('active');
	    if ($(".collapser_search").text().indexOf('Ocultar') !== -1) {
	      $(".collapser_search").text("Mostrar formulario de alta");
	    } else {
	      $(".collapser_search").text("Ocultar formulario de alta");
	    }
	    return false;
	  });

	  $('.collapser_search').click();
	  $('.contenedorResultados').hide();

	  $("#ui-datepicker-div").remove();

	  $('input#textFechaEje').datepicker({

	    onClose: function( selectedDate ) {
	      $( "#textFechaLiq" ).datepicker( "option", "minDate", selectedDate );
	    } // onClose
	  });

	  $('input#textFechaLiq').datepicker({

	    onClose: function( selectedDate ) {
	      $( "#textFechaEje" ).datepicker( "option", "maxDate", selectedDate );
	    } // onClose
	  });

	  cargarAlias();
	  cargarCamara();
	  cargarIsin();
	  cargarCuentaLiquidacion();

	  loadTable_OperacionesFallidas();
	  loadTable_OperacionFallidaSeleccionada();
	  loadTable_Afectaciones();

	  cargarTablaOperacionesFallidas();

	  $('#altaOperacionFallidaForm').submit(submitForm);

	  $('#cancelar').click(function () {
	    $('#title_principal').show();
	    $('.contenedorTablaOperacionesFallidas').show();
	    cargarTablaOperacionesFallidas();
	    $('.contenedorResultados').hide();
	  });

	  $('#salvarAfectaciones').click(function () {
	    salvarAfectaciones();
	  });

	  $('#modificarNuTitAfectados').click(function () {
	    comprobarTitulosAfectados();
	  });

	  $('#cancelarNuTitAfectados').click(function () {
	    ocultarPopupModificarNuTitAfectados();
	  });

	});


}]);
