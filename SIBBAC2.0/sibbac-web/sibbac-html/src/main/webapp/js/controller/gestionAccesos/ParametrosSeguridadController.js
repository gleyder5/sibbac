'use strict';
sibbac20.controller('ParametrosSeguridadController',
                    [ '$scope', 'ParametrosSeguridadService', 'growl', '$compile', '$document',
                     function ($scope, ParametrosSeguridadService, growl, $compile, $document) {

                       $scope.parametros = {
                         mayusculas : null,
                         numeros : null,
                         caracteresExtranos : null,
                         longitud : null,
                         tiempoExpiracion : null,
                         avisoExpiracion : null,
                         intentosBloqueo : null,
                         cotrasenasRepetidas : null
                       };

                       $scope.consultarParametros = function () {
                         ParametrosSeguridadService.consultarParametros(function (data) {
                           $scope.parametros = data.resultados["parametros"];
                         }, function (error) {
                           fErrorTxt("Se produjo un error en la carga de los parametros de seguridad.", 1);
                         });
                       };

                       $scope.consultarParametros();

                       $scope.guardarParametros = function () {
                         ParametrosSeguridadService.guardarParametros(function (data) {
                           if (data.resultados.status === 'KO') {
                             fErrorTxt(data.error, 1);
                           } else {
                             fErrorTxt('Los parametros fueron guardados correctamente', 3);
                           }
                         }, function (error) {
                           fErrorTxt("Se produjo un error en la peticion al guardar los parametros de seguridad.", 1);
                         }, $scope.parametros);
                       };

                     } ]);
