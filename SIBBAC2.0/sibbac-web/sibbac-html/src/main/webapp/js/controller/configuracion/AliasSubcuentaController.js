'use strict';
sibbac20.controller('AliasSubcuentaController',
                    [
                     '$scope',
                     '$rootScope',
                     'AliasService',
                     'growl',
                     '$compile',
                     'AliasSubcuentaService',
                     '$location',
                     'SecurityService',
                     function ($scope, $rootScope, AliasService, growl, $compile, AliasSubcuentaService, $location,
                               SecurityService) {
                       var idOcultos = [];
                       var availableTags = [];
                       var oTable = undefined;
                       function showError (data, status, headers, config) {
                         $.unblockUI();
                         growl.addErrorMessage("Error " + data);
                       }
                       function initializeTable () {
                         oTable = $("#tablaListado").dataTable({
                           "dom" : 'T<"clear">lfrtip',
                           "language" : {
                             "url" : "i18n/Spanish.json"
                           },
                           "tableTools" : {
                             "sSwfPath" : "js/swf/copy_csv_xls_pdf.swf"
                           },
                           "order" : [],
                           "scrollY" : "480px",
                           "scrollCollapse" : true,
                           "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                             $compile(nRow)($scope);
                           }
                         });

                       }
                       function initializeAlias () {
                         inicializarLoading();
                         AliasService.getAliasFactura(cargarAlias, showError);
                       }
                       $(document).ready(function () {
                         initializeTable();
                         initializeAlias();
                         prepareCollapsion();
                         /* BUSCADOR */
                         $('.btn.buscador').click(function () {
                           collapseSearchForm();
                           var nombreAlias = document.getElementById("nombreAlias").value;
                           var indexOfAlias = availableTags.indexOf(nombreAlias);
                           var idAlias = idOcultos[indexOfAlias];
                           cargarTabla(idAlias);
                           return false;
                         });

                       });
                       function onLoadAliasFacturacion (resultados) {
                         var item = null;
                         $('#idAliasFacturacionSubcuenta').empty();
                         for ( var k in resultados) {
                           item = resultados[k];
                           // Rellenamos el combo de idiomas para modificar
                           $('#idAliasFacturacionSubcuenta').append(
                                                                    "<option value='" + item.id + "'>" + item.nombre
                                                                        + "</option>"

                           );

                         }
                       }
                       function cargarAliasFacturacion (idSubcuenta) {
                         var filter = {};
                         filter.idSubcuenta = idSubcuenta;
                         AliasService.getListaAliasFacturacionSubcuenta(onLoadAliasFacturacion, showError, filter);
                       }

                       function cargarAlias (datos) {
                         var item = null;

                         for ( var k in datos) {
                           item = datos[k];
                           idOcultos.push(item.id);
                           availableTags.push(item.nombre);
                         }
                         // código de autocompletar

                         $("#nombreAlias").empty();
                         $("#nombreAlias").autocomplete({
                           source : availableTags
                         });
                         $.unblockUI();
                       }

                       function onLoadedAjaxDataTable (data) {
                         if (data.error) {
                           alert(data.error);
                         } else {
                           var resultados = data.resultados.listaSubcuentas;
                           var now = Date.now();
                           var aliasSubcuentaItem = null;
                           oTable.fnClearTable();
                           for ( var k in resultados) {
                             aliasSubcuentaItem = resultados[k];

                             // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                             var enlace = "";
                             if (SecurityService.inicializated) {
                               if (SecurityService.isPermisoAccion($rootScope.SecurityActions.MODIFICAR, $location
                                   .path())) {
                                 enlace = "<a class=\"btn\" ng-click = \"cargoInputs('" + aliasSubcuentaItem.id + "','"
                                          + aliasSubcuentaItem.idAliasFacturacion + "','" + aliasSubcuentaItem.nombre
                                          + "'); \"><img src='img/editp.png' title='Editar'/></a>";
                               }
                             }

                             oTable.fnAddData([ aliasSubcuentaItem.nombre, aliasSubcuentaItem.nombreAliasFacturacion,
                                               enlace ], false);

                           }
                           oTable.fnDraw();
                           console.log("Se ha tardado en pintar la tabla :" + ((Date.now() - now) / 1000));
                         }
                         $.unblockUI();
                       }
                       function cargarTabla (idAlias) {
                         inicializarLoading();
                         var filter = {};
                         filter.idAlias = idAlias;
                         AliasSubcuentaService.getListaSubcuentas(onLoadedAjaxDataTable, showError, filter);
                       }
                       $scope.clearAll = function () {
                         oTable.fnClearTable();
                         $('#nombreSubcuenta').empty();
                         $('#idSubcuenta').empty();
                         $('#idAliasFacturacionSubcuenta').empty();
                         $("#nombreAlias").val('');
                       };
                       $scope.cargoInputs = function (idSubcuenta, idAliasFacturacion, nombreSubcuenta) {
                         document.getElementById('formularioSubcuenta').style.display = 'block';
                         document.getElementById('fade').style.display = 'block';
                         cargarAliasFacturacion(idSubcuenta);
                         $('#nombreSubcuenta').empty();
                         $('#nombreSubcuenta').append(nombreSubcuenta);
                         document.getElementById('idSubcuenta').value = idSubcuenta;
                         document.getElementById('idAliasFacturacionSubcuenta').value = idAliasFacturacion;
                       };
                       function onSubcuentaChangedSuccess (data, status, headers, config) {
                         $scope.clearAll();
                         document.getElementById('formularioSubcuenta').style.display = 'none';
                         document.getElementById('fade').style.display = 'none';
                         growl.addSuccessMessage("Datos modificados correctamente.");
                       }
                       // cambio datos
                       $('#btnEditar').click(function () {
                         // alert("Paso por aqui");
                         var idSubcuenta = document.getElementById('idSubcuenta').value;
                         var idAliasFacturacion = document.getElementById('idAliasFacturacionSubcuenta').value;
                         alert(idSubcuenta);
                         alert(idAliasFacturacion);
                         var filter = {};
                         filter.idSubcuenta = idSubcuenta;
                         filter.idAliasFacturacion = idAliasFacturacion;
                         AliasSubcuentaService.modificarSubcuenta(onSubcuentaChangedSuccess, showError, filter);
                       });

                     } ]);
