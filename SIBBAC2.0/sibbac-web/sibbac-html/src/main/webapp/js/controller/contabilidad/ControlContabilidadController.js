sibbac20
.controller(
		'ControlContabilidadController',
		[
		 '$scope',
		 '$filter',
		 '$document',
		 'growl',
		 'ControlContabilidadService',
		 'ConciliacionBancariaCombosService',
		 '$compile',
		 'SecurityService',
		 '$location',
		 'Logger',
		 '$rootScope',
		 function ($scope, $filter, $document, growl, ControlContabilidadService, ConciliacionBancariaCombosService, $compile, SecurityService, $location, Logger, $rootScope) {

			 SecurityService.redirectToLoginIfNotLoggedIn();
			 
			 $scope.safeApply = function (fn) {
				 var phase = this.$root.$$phase;
				 if (phase === '$apply' || phase === '$digest') {
					 if (fn && (typeof (fn) === 'function')) {
						 fn();
					 }
				 } else {
					 this.$apply(fn);
				 }
			 };	

			 var hoy = new Date();
			 var dd = hoy.getDate();
			 var mm = hoy.getMonth() + 1;
			 var yyyy = hoy.getFullYear();
			 hoy = yyyy + "_" + mm + "_" + dd;

			 $scope.listCodigosPlantillas = [];
			 
			 $scope.showingFilter = true;

			 $scope.listApuntesPendientes = [];

			 $scope.listApuntesAprobados = [];

			 $scope.listApuntesRechazados = [];

			 $scope.apuntesPendientesSeleccionados = [];

			 $scope.apuntesAuditoriaSeleccionados = [];

			 $scope.listApuntesAuditoria = [];

			 $scope.mostrarErrorAuditApuntes = false;

			 $scope.mostrarTablaPartidasCasadas = false;

			 $scope.mostrarErrorFiltroAuditApuntes = false;

			 $scope.mostrarErrorFiltroAuditTolerance = false;

			 $scope.mostrarErrorModificarTolerance = false;

			 $scope.oTableTLMCreada = false;

			 $scope.oTableSIBBACCreada = false;

			 $scope.auditUser = "";

			 $scope.pestana = "TLM";

			 $scope.desglosesSIBBAC = [];
			 $scope.partidasCasadas = [];

			 $scope.isActive = function (pestana) { 
				 return pestana === $scope.pestana;
			 };

			 $scope.activarPestana = function(pestana){
				 $scope.pestana = pestana;
			     $scope.createOTableSIBBAC();
			 };

			 $scope.resetPlantilla = function () {
				 $scope.toleranceForm = null;
				 $scope.plantillaForm = {}; 
			 };

			 $scope.resetFiltroAuditoriaApunte = function () {
				 $scope.filtroAuditoria = {
						 usuario : "",
						 fechaApunteDesde : "",
						 fechaApunteHasta : "" 
				 };
			 };

			 $scope.resetFiltroAuditoriaTolerance = function () {
				 $scope.filtroTolerance = {
						 usuario : "",
						 fechaToleranceDesde : "",
						 fechaToleranceHasta : "", 
						 codigoPlantilla : ""
				 };
			 };
			 
			 $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col ) {
				  return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
				    return $('input', td).prop('checked') ? '1' : '0';
				  })
				};

			 $scope.plantilla = {codigoPlantilla: "", tolerance: null, auditUser: ""};

			 $scope.resetPlantilla();

			 $scope.oTable = $("#datosApuntesPndts").dataTable({
				 "dom" : 'T<"clear">lfrtip',
				 "tableTools" : {
					 "aButtons" : [ "print" ]
				 },
				 "aoColumns" : [ {
					 sClass : "centrar",
					 width : "10%", 
					 orderDataType: "dom-checkbox"
				 }, {
					 sClass : "centrar",
					 width : "15%",
					 sType : "date-eu"
				 }, {
					 sClass : "centrar",
					 width : "15%"
				 }, {
					 sClass : "centrar",
					 width : "15%"
				 }, {
					 sClass : "centrar",
					 width : "15%",
					 sClass : "monedaR",
					 type : "formatted-num"
				 }, {
					 sClass : "centrar",
					 width : "15%",
					 sClass : "monedaR",
					 type : "formatted-num"
				 }, {
					 sClass : "centrar",
					 width : "15%",
					 sClass : "monedaR",
					 type : "formatted-num"
				 }],
				 "fnCreatedRow" : function (nRow, aData, iDataIndex) {

					 $compile(nRow)($scope);
				 },

				 "scrollY" : "480px",
				 "scrollX" : "100%",
				 "scrollCollapse" : true,
				 "language" : {
					 "url" : "i18n/Spanish.json"
				 }
			 });

			 $scope.createTableAuditoriaApuntes = function () {
				 $scope.oTableAuditoriaApuntes = $("#datosAuditoriaApuntes").dataTable({
					 "pageLength": 5,
					 "lengthMenu": [ 5, 10 ],
					 "dom" : 'T<"clear">lfrtip',
					 "tableTools" : {
						 "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
						 "aButtons" : [{
							 "sExtends" : "xls",
							 "sFileName" : "Listado_Auditoria_Apuntes_" + hoy + ".xls",
							 "mColumns" : [ 1, 2, 3, 4, 5, 6, 7]
						 } ]
					 },
					 "aoColumns" : [ {
						 sClass : "centrar",
						 width : "9%",
						 orderDataType: "dom-checkbox"
					 }, {
						 sClass : "centrar",
						 width : "13%",
						 sType : "date-eu"
					 }, {
						 sClass : "centrar",
						 width : "13%"
					 }, {
						 sClass : "centrar",
						 width : "13%"
					 }, {
						 sClass : "centrar",
						 width : "13%",
						 sClass : "monedaR",
						 type : "formatted-num"
					 }, {
						 sClass : "centrar",
						 width : "13%",
						 sClass : "monedaR",
						 type : "formatted-num"
					 }, {
						 sClass : "centrar",
						 width : "13%",
						 sClass : "monedaR",
						 type : "formatted-num"
					 }, {
						 sClass : "centrar",
						 width : "13%"
					 }],
					 "fnCreatedRow" : function (nRow, aData, iDataIndex) {

						 $compile(nRow)($scope);
					 },

					 "scrollY" : "480px",
					 "scrollX" : "100%",
					 "scrollCollapse" : true,
					 "language" : {
						 "url" : "i18n/Spanish.json"
					 },
					 "bDestroy": true
				 });
			 }

			 $scope.createTableAuditoriaTolerance = function () {
				 $scope.oTableToleranceApuntes = $("#datosAuditoriaTolerance").dataTable({
					 "pageLength": 5,
					 "lengthMenu": [ 5, 10 ],
					 "dom" : 'T<"clear">lfrtip',
					 "tableTools" : {
						 "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
						 "aButtons" : [{
							 "sExtends" : "xls",
							 "sFileName" : "Listado_Auditoria_Tolerance_" + hoy + ".xls",
							 "mColumns" : [ 0, 1, 2, 3]
						 } ]
					 },
					 "aoColumns" : [ {
						 sClass : "centrar",
						 width : "25%"
					 }, {
						 sClass : "centrar",
						 width : "25%"
					 }, {
						 sClass : "centrar",
						 width : "25%",
						 sType : "date-eu"
					 }, {
						 sClass : "centrar",
						 width : "25%"
					 }],
					 "fnCreatedRow" : function (nRow, aData, iDataIndex) {

						 $compile(nRow)($scope);
					 },

					 "scrollY" : "480px",
					 "scrollX" : "100%",
					 "scrollCollapse" : true,
					 "language" : {
						 "url" : "i18n/Spanish.json"
					 },
					 "bDestroy": true
				 });
			 }

			 $scope.createOTableTLM = function(){
				 $scope.oTableTLM = $("#datosTLM").dataTable({
					 "dom" : 'T<"clear">lfrtip',
					 "tableTools" : {
						 "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
						 "aButtons" : [ "copy"]
					 },
					 "aoColumns" : [ {
						 sClass : "centrar",
						 width : "17%"
					 }, {
						 sClass : "centrar",
						 width : "17%"
					 }, {
						 sClass : "centrar",
						 width : "17%"
					 }, {
						 sClass : "centrar",
						 width : "17%"
					 }, {
						 sClass : "centrar",
						 width : "16%"
					 }, {
						 sClass : "centrar",
						 width : "16%",
						 sClass : "monedaR",
						 type : "formatted-num"
					 }  ],
					 "fnCreatedRow" : function (nRow, aData, iDataIndex) {
						 $compile(nRow)($scope);
					 },

					 "scrollY" : "480px",
					 "scrollX" : "100%",
					 "scrollCollapse" : true,
					 "language" : {
						 "url" : "i18n/Spanish.json"
					 },
					 "bDestroy": true
				 });
			 }

			 $scope.createOTableSIBBAC = function(){
				 if ($scope.oTableSIBBAC !== undefined && $scope.oTableSIBBAC !== null) {
					$scope.oTableSIBBAC.fnDestroy();
				 }
				 $scope.oTableSIBBAC = $("#datosSIBBAC").dataTable({
					 "dom" : 'T<"clear">lfrtip',
					 "tableTools" : {
						 "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
						 "aButtons" : [ "copy" ]
					 },
					 "aoColumns" : [ {
						 sClass : "centrar",
						 width : "20%"
					 }, {
						 sClass : "centrar",
						 width : "20%"
					 }, {
						 sClass : "centrar",
						 width : "20%"
					 }, {
						 sClass : "centrar",
						 width : "10%"
					 }, {
						 sClass : "centrar",
						 width : "20%"
					 }, {
						 sClass : "centrar",
						 width : "10%",
						 sClass : "monedaR",
						 type : "formatted-num"
					 } ],
					 "fnCreatedRow" : function (nRow, aData, iDataIndex) {
						 $compile(nRow)($scope);
					 },

					 "scrollY" : "480px",
					 "scrollX" : "100%",
					 "scrollCollapse" : true,
					 "language" : {
						 "url" : "i18n/Spanish.json"
					 },
					 "bDestroy": true
				 });
			 }

			 $scope.seleccionarElemento = function (row) {
				 if ($scope.listApuntesPendientes[row].selected) {
					 $scope.listApuntesPendientes[row].selected = false;
					 for (var i = 0; i < $scope.apuntesPendientesSeleccionados.length; i++) {
						 if ($scope.apuntesPendientesSeleccionados[i].id === $scope.listApuntesPendientes[row].id) {
							 $scope.apuntesPendientesSeleccionados.splice(i, 1);
						 }
					 }
				 } else {
					 $scope.listApuntesPendientes[row].selected = true;
					 $scope.apuntesPendientesSeleccionados.push($scope.listApuntesPendientes[row]);
				 }
			 };

			 $scope.seleccionarElementoApuntesAuditoria = function (row) {
				 if ($scope.listApuntesAuditoria[row].selected) {
					 $scope.listApuntesAuditoria[row].selected = false;
					 for (var i = 0; i < $scope.apuntesAuditoriaSeleccionados.length; i++) {
						 if ($scope.apuntesAuditoriaSeleccionados[i].id === $scope.listApuntesAuditoria[row].id) {
							 $scope.apuntesAuditoriaSeleccionados.splice(i, 1);
						 }
					 }
				 } else {
					 $scope.listApuntesAuditoria[row].selected = true;
					 $scope.apuntesAuditoriaSeleccionados.push($scope.listApuntesAuditoria[row]);
				 }
			 };

			 $scope.consultarApuntesPndts = function () {

				 inicializarLoading();

				 $scope.apuntesPendientesSeleccionados = [];

				 ControlContabilidadService.consultarApuntesPndts(function (data) {

					 if (data.resultados.status === 'KO') {
						 $.unblockUI();
						 fErrorTxt(
								 "Se produjo un error en la carga del listado de apuntes pendientes.",
								 1);
					 } else {

						 $scope.listApuntesPendientes = data.resultados["listaApuntesPendientes"];

						 // borra el contenido del body de la tabla
						 var tbl = $("#datosApuntesPndts > tbody");
						 $(tbl).html("");
						 $scope.oTable.fnClearTable();

						 for (var i = 0; i < $scope.listApuntesPendientes.length; i++) {

							 var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listApuntesPendientes['
								 + i
								 + '].selected" ng-click="seleccionarElemento('
								 + i + ');"/>';

							 var apuntesPendientesList = [
							                              check,
							                              $filter('date')($scope.listApuntesPendientes[i].feApunte, "dd/MM/yyyy"),
							                              $scope.listApuntesPendientes[i].usuApunte,
							                              $scope.listApuntesPendientes[i].concepto,
							                              $.number($scope.listApuntesPendientes[i].importeDebe, 2, ',', '.'),
							                              $.number($scope.listApuntesPendientes[i].importeHaber, 2, ',', '.'),
							                              $.number($scope.listApuntesPendientes[i].importePyG, 2, ',', '.')];

							 $scope.oTable.fnAddData(apuntesPendientesList, false);

						 }
						 $scope.oTable.fnDraw();
						 $scope.safeApply();
						 $.unblockUI();
					 }
				 },
				 function (error) {
					 $.unblockUI();
					 fErrorTxt(
							 "Se produjo un error en la carga del listado de apuntes pendientes.",
							 1);
				 }, null);
			 };

			 if($rootScope.isAuthenticated()){
				 $scope.consultarApuntesPndts();
			 }

			 $scope.consultarAuditoriaApuntes = function () {

				 $scope.mostrarErrorFiltroAuditApuntes = $scope.validacionFiltroAuditApuntes();
				 if (!$scope.mostrarErrorFiltroAuditApuntes) {
					 $scope.showingFilter = false;
					 inicializarLoading();

					 $scope.apuntesAuditoriaSeleccionados = [];

					 ControlContabilidadService.consultarAuditoriaApuntes(function (data) {

						 if (data.resultados.status === 'KO') {
							 $.unblockUI();
							 fErrorTxt(
									 "Se produjo un error en la carga del listado de apuntes auditados.",
									 1);
						 } else {

							 $scope.listApuntesAuditoria = data.resultados["listaApuntesAuditoria"];

							 // borra el contenido del body de la tabla
							 var tbl = $("#datosAuditoriaApuntes > tbody");
							 $(tbl).html("");
							 $scope.oTableAuditoriaApuntes.fnClearTable();

							 for (var i = 0; i < $scope.listApuntesAuditoria.length; i++) {

								 var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listApuntesAuditoria['
									 + i
									 + '].selected" ng-click="seleccionarElementoApuntesAuditoria('
									 + i + ');"/>';

								 var apuntesAuditoriaList = [
								                             check,
								                             $filter('date')($scope.listApuntesAuditoria[i].feApunte, "dd/MM/yyyy"),
								                             $scope.listApuntesAuditoria[i].usuApunte,
								                             $scope.listApuntesAuditoria[i].concepto,
								                             $.number($scope.listApuntesAuditoria[i].importeDebe, 2, ',', '.'),
								                             $.number($scope.listApuntesAuditoria[i].importeHaber, 2, ',', '.'),
								                             $.number($scope.listApuntesAuditoria[i].importePyG, 2, ',', '.'),
								                             $scope.listApuntesAuditoria[i].estado,];

								 $scope.oTableAuditoriaApuntes.fnAddData(apuntesAuditoriaList, false);

							 }
							 $scope.oTableAuditoriaApuntes.fnDraw();
							 $scope.safeApply();
							 $.unblockUI();
						 }
					 },
					 function (error) {
						 $.unblockUI();
						 fErrorTxt(
								 "Se produjo un error en la carga del listado de apuntes de auditoria.",
								 1);
					 }, $scope.filtroAuditoria);
				 }
			 };

			 $scope.consultarAuditoriaTolerance = function () {

				 $scope.mostrarErrorFiltroAuditTolerance = $scope.validacionFiltroAuditTolerance();
				 if (!$scope.mostrarErrorFiltroAuditTolerance) {
					 inicializarLoading();

					 ControlContabilidadService.consultarAuditoriaTolerance(function (data) {

						 if (data.resultados.status === 'KO') {
							 $.unblockUI();
							 fErrorTxt(
									 "Se produjo un error en la carga del listado de tolerances auditados.",
									 1);
						 } else {

							 $scope.listToleranceAuditoria = data.resultados["listaToleranceAuditoria"];

							 // borra el contenido del body de la tabla
							 var tbl = $("#datosAuditoriaTolerance > tbody");
							 $(tbl).html("");
							 $scope.oTableToleranceApuntes.fnClearTable();

							 for (var i = 0; i < $scope.listToleranceAuditoria.length; i++) {

								 var toleranceAuditoriaList = [
								                               $scope.listToleranceAuditoria[i].codPlantilla,
								                               $scope.listToleranceAuditoria[i].tolerance,
								                               $filter('date')($scope.listToleranceAuditoria[i].auditDate, "dd/MM/yyyy"),
								                               $scope.listToleranceAuditoria[i].auditUser];

								 $scope.oTableToleranceApuntes.fnAddData(toleranceAuditoriaList, false);

							 }
							 $scope.oTableToleranceApuntes.fnDraw();
							 $scope.safeApply();
							 $.unblockUI();
						 }
					 },
					 function (error) {
						 $.unblockUI();
						 fErrorTxt(
								 "Se produjo un error en la carga del listado de tolerances auditados.",
								 1);
					 }, $scope.filtroTolerance);
				 }
			 };

			 $scope.consultarPartidasCasadas = function () {


				 inicializarLoading();

				 ControlContabilidadService.consultarPartidasCasadas(function (data) {

					 if (data.resultados.status === 'KO') {
						 $.unblockUI();
						 fErrorTxt(
								 "Se produjo un error en la carga del listado de partidas casadas.",
								 1);
					 } else {

						 $scope.historico = angular.copy($scope.apuntesAuditoriaSeleccionados[0]);

						 $scope.partidasCasadas = data.resultados["listaPartidasCasadas"];

						 // borra el contenido del body de la tabla
						 var tbl = $("#datosTLM > tbody");
						 $(tbl).html("");
						 $scope.oTableTLM.fnClearTable();

						 for (var i = 0; i < $scope.partidasCasadas.length; i++) {

							 var partidasCasadasList = [
							                            $scope.partidasCasadas[i].referencia,
							                            $scope.partidasCasadas[i].gin,
							                            $scope.partidasCasadas[i].tipoMovimiento,
							                            $scope.partidasCasadas[i].auxiliar,
							                            $scope.partidasCasadas[i].concepto,
							                            $.number($scope.partidasCasadas[i].importe, 2, ',', '.')];

							 $scope.oTableTLM.fnAddData(partidasCasadasList, false);

						 }
						 $scope.oTableTLM.fnDraw();
						 $scope.safeApply();
						 $.unblockUI();
					 }
				 },
				 function (error) {
					 $.unblockUI();
					 fErrorTxt(
							 "Se produjo un error en la carga del listado de partidas casadas.",
							 1);
				 }, $scope.apuntesAuditoriaSeleccionados[0]);
				 
				 $scope.consultarOur();
			 };

			 $scope.consultarOur = function () {


				 inicializarLoading();

				 ControlContabilidadService.consultarOur(function (data) {

					 if (data.resultados.status === 'KO') {
						 $.unblockUI();
						 fErrorTxt(
								 "Se produjo un error en la carga del listado de Our.",
								 1);
					 } else {

						 $scope.desglosesSIBBAC = data.resultados["listaOur"];

						 // borra el contenido del body de la tabla
						 var tbl = $("#datosSIBBAC > tbody");
						 $(tbl).html("");
						 $scope.oTableSIBBAC.fnClearTable();

						 for (var i = 0; i < $scope.desglosesSIBBAC.length; i++) {

							 var ourList = [
							                $scope.desglosesSIBBAC[i].nuorden,
							                $scope.desglosesSIBBAC[i].nbooking,
							                $scope.desglosesSIBBAC[i].nucnfclt,
							                $filter('number')($scope.desglosesSIBBAC[i].nucnfliq, 2),
							                $scope.desglosesSIBBAC[i].sentido,
							                $.number($scope.desglosesSIBBAC[i].importe, 2, ',', '.')];

							 $scope.oTableSIBBAC.fnAddData(ourList, false);

						 }
						 $scope.oTableSIBBAC.fnDraw();
						 $scope.safeApply();
						 $.unblockUI();
					 }
				 },
				 function (error) {
					 $.unblockUI();
					 fErrorTxt(
							 "Se produjo un error en la carga del listado de Our.",
							 1);
				 }, $scope.apuntesAuditoriaSeleccionados[0]);
			 };

			 $scope.aprobarApuntes = function () {
				 if ($scope.apuntesPendientesSeleccionados.length > 0) {
					 if ($scope.apuntesPendientesSeleccionados.length == 1) {
						 angular.element("#dialog-confirm").html("¿Desea aprobar el apunte seleccionado?");
					 } else {
						 angular.element("#dialog-confirm")
						 .html(
								 "¿Desea aprobar los " + $scope.apuntesPendientesSeleccionados.length
								 + " apuntes seleccionados?");
					 }

					 // Define the Dialog and its properties.
					 angular
					 .element("#dialog-confirm")
					 .dialog(
							 {
								 resizable : false,
								 modal : true,
								 title : "Mensaje de Confirmación",
								 height : 150,
								 width : 360,
								 buttons : {
									 " Sí " : function () {
										 ControlContabilidadService.aprobarApuntes(function (data) {
											 if (data.resultados.status === 'KO') {
												 $.unblockUI();
												 fErrorTxt(data.error, 1);
											 } else {
												 $scope.consultarApuntesPndts();
												 fErrorTxt('Apuntes aprobados correctamente',
														 3);
											 }

										 },
										 function (error) {
											 $.unblockUI();
											 fErrorTxt(
													 "Ocurrió un error durante la petición de datos al aprobar apuntes.",
													 1);
										 },
										 {
											 listApuntesAprobados : $scope.apuntesPendientesSeleccionados,
											 auditUser : $rootScope.userName
										 });

										 $(this).dialog('close');
									 },
									 " No " : function () {
										 $(this).dialog('close');
									 }
								 }
							 });
					 $('.ui-dialog-titlebar-close').remove();
				 } else {
					 fErrorTxt("Debe seleccionar al menos un elemento de la tabla de apuntes pendientes.", 2)
				 }
			 };

			 $scope.rechazarApuntes = function () {
				 if ($scope.apuntesPendientesSeleccionados.length > 0) {
					 if ($scope.apuntesPendientesSeleccionados.length == 1) {
						 angular.element("#dialog-confirm").html("¿Desea rechazar el apunte seleccionado?");
					 } else {
						 angular.element("#dialog-confirm")
						 .html(
								 "¿Desea rechazar los " + $scope.apuntesPendientesSeleccionados.length
								 + " apuntes seleccionados?");
					 }

					 // Define the Dialog and its properties.
					 angular
					 .element("#dialog-confirm")
					 .dialog(
							 {
								 resizable : false,
								 modal : true,
								 title : "Mensaje de Confirmación",
								 height : 150,
								 width : 360,
								 buttons : {
									 " Sí " : function () {
										 ControlContabilidadService.rechazarApuntes(function (data) {
											 if (data.resultados.status === 'KO') {
												 $.unblockUI();
												 fErrorTxt(data.error, 1);
											 } else {
												 $scope.consultarApuntesPndts();
												 fErrorTxt('Apuntes rechazados correctamente',
														 3);
											 }

										 },
										 function (error) {
											 $.unblockUI();
											 fErrorTxt(
													 "Ocurrió un error durante la petición de datos al rechazar apuntes.",
													 1);
										 },
										 {
											 listApuntesRechazados : $scope.apuntesPendientesSeleccionados,
											 auditUser : $rootScope.userName
										 });

										 $(this).dialog('close');
									 },
									 " No " : function () {
										 $(this).dialog('close');
									 }
								 }
							 });
					 $('.ui-dialog-titlebar-close').remove();
				 } else {
					 fErrorTxt("Debe seleccionar al menos un elemento de la tabla de apuntes pendientes.", 2)
				 }
			 };

			 $scope.verPartidasCasadas = function () {
				 if ($scope.apuntesAuditoriaSeleccionados.length > 0) {
					 if ($scope.apuntesAuditoriaSeleccionados.length == 1) {
						 $scope.pestana = "TLM";
						 $scope.createOTableTLM();
						 $scope.mostrarErrorAuditApuntes = false;
						 $scope.consultarPartidasCasadas();
						 $scope.mostrarTablaPartidasCasadas = true;
					 } else {
						 $scope.mostrarErrorAuditApuntes = true;
					 }
				 } else {
					 $scope.mostrarErrorAuditApuntes = true;
				 }
			 };

			 $scope.consultarCodigosPlantillasTheir = function () {
				 if($rootScope.isAuthenticated()){
					 ConciliacionBancariaCombosService.codigosPlantillas(function (data) {
						 if (data.resultados.status == 'KO') {
							 fErrorTxt("Se produjo un error en la carga del combo Códigos de plantillas", 1);
						 } else {
							 $scope.listCodigosPlantillas = data.resultados["listaCodigosPlantillas"];
						 }
					 }, function (error) {
						 fErrorTxt("Se produjo un error en la carga del combo Códigos de plantillas", 1);
					 });					 
				 }
			 };

			 $scope.consultarCodigosPlantillasTheir();

			 $scope.consultarUsuarios = function () {
				 if($rootScope.isAuthenticated()){
					 ConciliacionBancariaCombosService.consultarUsuarios(function (data) {
						 if (data.resultados.status == 'KO') {
							 fErrorTxt("Se produjo un error en la carga del combo de usuarios", 1);
						 } else {
							 $scope.listUsuarios = data.resultados["listaUsuarios"];
						 }
					 }, function (error) {
						 fErrorTxt("Se produjo un error en la carga del combo de usuarios", 1);
					 });					 
				 }
			 };

			 $scope.consultarUsuarios();

			 $scope.abrirModificarTolerance = function () {
				 $scope.mostrarErrorModificarTolerance = false;
				 $scope.resetPlantilla();
				 angular.element('#modalTolerance').modal({
					 backdrop : 'static'
				 });
				 $(".modal-backdrop").remove();
			 };

			 $scope.abrirAuditoriaApuntes = function () {
				 $scope.mostrarErrorAuditApuntes = false;
				 $scope.mostrarTablaPartidasCasadas = false;
				 $scope.mostrarErrorFiltroAuditApuntes = false;
				 $scope.apuntesAuditoriaSeleccionados = [];
				 $scope.resetFiltroAuditoriaApunte();
				 $scope.createTableAuditoriaApuntes();
				 $scope.oTableAuditoriaApuntes.fnClearTable();
				 angular.element('#modalAuditoriaApuntes').modal({
					 backdrop : 'static'
				 });
				 $(".modal-backdrop").remove();
			 };

			 $scope.abrirAuditoriaTolerance = function () {
				 $scope.resetFiltroAuditoriaTolerance();
				 $scope.createTableAuditoriaTolerance();
				 $scope.oTableToleranceApuntes.fnClearTable();
				 angular.element('#modalAuditoriaTolerance').modal({
					 backdrop : 'static'
				 });
				 $(".modal-backdrop").remove();
			 };



             $scope.cargarTolerance = function () {
                 if($scope.plantillaForm!=null){
                     $scope.toleranceForm = parseInt($scope.plantillaForm.value,10);
                 }

             };

			 $scope.modificarTolerance = function () {
				 $scope.mostrarErrorModificarTolerance = $scope.validacionModificarTolerance();
				 if (!$scope.mostrarErrorModificarTolerance) {
					 $scope.plantilla.tolerance = angular.copy($scope.toleranceForm);
					 $scope.plantilla.codigoPlantilla = angular.copy($scope.plantillaForm.key);
					 $scope.plantilla.auditUser = $rootScope.userName;
					 ControlContabilidadService
					 .modificarTolerance(function (data) {
						 if (data.resultados.status === 'KO') {
							 $.unblockUI();
							 fErrorTxt(data.error, 1);
						 } else {
							 angular.element('#modalTolerance').modal("hide");
							 fErrorTxt('El tolerance correspondiente al código de plantilla ' + $scope.plantilla.codigoPlantilla
									 + ' ha sido modificado correctamente', 3);
							 $scope.consultarCodigosPlantillasTheir();
						 }
					 }, function (error) {
						 $.unblockUI();
						 angular.element('#modalTolerance').modal("hide");
						 fErrorTxt("Ocurrió un error durante la petición de datos al modificar el tolerance.",
								 1);
					 }, $scope.plantilla
					 );
				 }
			 };

			 $scope.reiniciarTablaAuditoriaApuntes = function () {
				 $scope.oTableAuditoriaApuntes.fnClearTable();
			 };

			 $scope.reiniciarTablaAuditoriaTolerance = function () {
				 $scope.oTableToleranceApuntes.fnClearTable();
			 };

			 $scope.mostrarFiltroYTablaAuditApuntes = function () {
				 $scope.mostrarTablaPartidasCasadas = false;
			 };

			 /** Validacion de campos del filtro de Auditoría apuntes. */
			 $scope.validacionFiltroAuditApuntes = function () {
				 if ($scope.filtroAuditoria.fechaApunteDesde == '' && $scope.filtroAuditoria.fechaApunteHasta != '') {
					 return true;
				 } else {
					 return false;
				 }
			 };

			 /** Validacion de campos de la modificación de tolerance. */
			 $scope.validacionModificarTolerance = function () {
				 if ($scope.plantillaForm.key && $scope.toleranceForm != null) {
					 return false;
				 } else {
					 return true;
				 }
			 };

			 /** Validacion de campos del filtro de Auditoría tolerance. */
			 $scope.validacionFiltroAuditTolerance = function () {
				 if ($scope.filtroTolerance.fechaToleranceDesde == '' && $scope.filtroTolerance.fechaToleranceHasta != '') {
					 return true;
				 } else {
					 return false;
				 }
			 };

			 $scope.seleccionarTodos = function () {
				 for (var i = 0; i < $scope.listApuntesPendientes.length; i++) {
					 $scope.listApuntesPendientes[i].selected = true;
				 }
				 $scope.apuntesPendientesSeleccionados = angular.copy($scope.listApuntesPendientes);
			 };

			 $scope.deseleccionarTodos = function () {
				 for (var i = 0; i < $scope.listApuntesPendientes.length; i++) {
					 $scope.listApuntesPendientes[i].selected = false;
				 }
				 $scope.apuntesPendientesSeleccionados = [];
			 };

			 $scope.exportarXLS = function () {
				 ControlContabilidadService.exportarXLS(function (data) {
					 onSuccessExportXLS(data);
				 }, function (error) {
					 $.unblockUI();
					 fErrorTxt("Ocurrió un error durante la petición de datos al exportar Excel Partidas Casadas", 1);

				 }, $scope.historico);
			 };

			 function onSuccessExportXLS (json) {

				 $.unblockUI();

				 if (json.resultados.excelPartidasCasadas !== undefined) {

					 var ExcelBytes = atob([ json.resultados.excelPartidasCasadas ]);

					 var byteNumbers = new Array(ExcelBytes.length);
					 for (var i = 0; i < ExcelBytes.length; i++) {
						 byteNumbers[i] = ExcelBytes.charCodeAt(i);
					 }

					 var byteArray = new Uint8Array(byteNumbers);

					 var blob = new Blob([ byteArray ], {
						 type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
					 });

					 var nav = navigator.userAgent.toLowerCase();

					 if (navigator.msSaveBlob) {
						 navigator.msSaveBlob(blob, "Partidas_Casadas_" + hoy + ".xlsx");
					 } else {
						 var blobUrl = URL.createObjectURL(blob);
						 var link = document.createElement('a');
						 link.href = blobUrl = URL.createObjectURL(blob);
						 link.download = "Partidas_Casadas_" + hoy + ".xlsx";
						 document.body.appendChild(link);
						 link.click();
						 document.body.removeChild(link);
					 }

				 } else {
					 fErrorTxt(
							 "Se produjo un error en la generacion del fichero Excel. Contacte con el Administrador.",
							 1);
				 }

			 }

		 } ]);
