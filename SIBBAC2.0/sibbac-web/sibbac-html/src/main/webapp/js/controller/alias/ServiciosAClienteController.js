sibbac20.controller('ServiciosAClienteController', ['$scope', '$document', 'growl', 'blockUI', 'AliasService', '$compile', function ($scope, $document, growl, blockUI, AliasService, $compile) {
        var idOcultos = [];
        var availableTags = [];
        $document.ready(function () {
            blockUI.start();
            AliasService.getListaAlias().success(cargaAlias).error(showError);
        });
        function cargaAlias(json, status, headers, config)
        {
            if (json.resultados !== undefined && angular.isArray(json.resultados.listaAlias)) {
                var resultados = json.resultados.listaAlias;
                angular.forEach(resultados, function (item, key) {
                    idOcultos.push(item.id);
                    availableTags.push(item.nombre.trim());
                });
                //código de autocompletar
                angular.element("#textAlias").autocomplete({
                    source: availableTags
                });
                blockUI.stop();
            } else if (json.error !== null && json.error !== "") {
                blockUI.stop();
                growl.addErrorMessage("Error al cargar los Alias: " + json.error);
            }

        }
        function showError(data, status, headers, config) {
            if (blockUI.isBlocking) {
                blockUI.stop();
            }
            growl.addErrorMessage("Error en la petición de datos al servidor, puede que se haya perdido la conexión con el servidor de datos. ");
        }
        /*cargar tabla alias*/
        angular.element('.btn.buscador').click(function () {
            //event.preventDefault();

            var listaAlias = angular.element("#textAlias").val();

            var posicionAlias = availableTags.indexOf(listaAlias);
            if (posicionAlias === "-1") {
                alert("El alias introducido no existe");
                return false;
            }
            else
            {
                var valorSeleccionadoAlias = idOcultos[posicionAlias];
                cargarTabla(valorSeleccionadoAlias);
            }
            return false;
        });
        function onSuccessGetContactosByService(json) {
            //validar si hay contactos y/o servicios
            if (json.resultados !== null) {
                var contactos = json.resultados.contactos;
                var servicios = json.resultados.servicios;
                var numServicios = servicios.length;
                var numContactos = contactos.length;

                if (numServicios !== 0 && numContactos !== 0) {
                    //éxito, cargo toda la tabla
                    //cargo las combinaciones
                    var combinacion = json.resultados.combinacion;
                    var item0 = null;
                    //creo array con los resultados contacto-servicio-regla
                    var datosCombinaciones = new Array();
                    //
                    var valorContacto = "";
                    var item1 = "";
                    var elvalor = "";
                    for (var z in combinacion) {
                        item0 = combinacion[ z ];

                        valorContacto = item0.idContacto;
                        var combinacion2 = json.resultados.combinacion[z].servicios;
                        for (var h in combinacion2) {
                            item1 = combinacion2[ h ];
                            //push al array con contacto-servicio-regla
                            elvalor = valorContacto + '-' + item1.idServicio + '-' + item1.idRegla;
                            datosCombinaciones.push(elvalor);
                        }
                    }

                    //comprobación array
                    var longitud = datosCombinaciones.length;
                    for (cont = 0; cont < longitud; cont++) {
                        //alert(datosCombinaciones[cont]);
                    }

                    //cargo los servicios
                    //miro los length de los resultados para el número de columnas y filas
                    var columnasS = numServicios + 1;
                    var filaS = numContactos + 2;
                    var item = null;
                    //cabecera de la tabla
                    angular.element('.lanzador').empty();
                    angular.element('.piePagina').empty();
                    //pinto lo que está fuera del bucle
                    //una variable para ir metiendo el contenido

                    var contTabla = "<table class='tablaPrueba ancha'><tr class='cabeceraLanzador'><td rowspan='" + filaS + "' class='titContactos'></td><td colspan='" + columnasS + "' class='titServicios'></td></tr><tr>" +
                            "<th class='taleft grisclaro'></th>";

                    //pinto los servicios y cargo los ids en un array
                    var losServicios = new Array();
                    for (var k in servicios) {
                        item = servicios[ k ];
                        losServicios.push(item.idServicio);
                        contTabla = contTabla + "<th class='taleft' ng-mouseover='mostrarServicio(\"tip" + item.idServicio + "\", $event)'  ng-mouseout='ocultarServicio(\"tip" + item.idServicio + "\", $event)' id='elemento" + item.idServicio + "'>" + item.nombreServicio + "</th>" +
                                "<div class='tip' id='tip" + item.idServicio + "' style='display: none;'><span class='negrita subrayado'>Datos del Servicio</span></br><span class='negrita'>ID: </span>" + item.idServicio + "</br><span class='negrita'>Nombre del Servicio: </span>" + item.nombreServicio + "</br><span class='negrita'>Comentarios del Servicio: </span>" + item.descripcionServicio + "</div>";
                    }
                    contTabla += "</tr>";

                    //cargo los contactos

                    var item = null;
                    //filas para cada contacto
                    //tengo que mirar en el array datosCombinaciones las coincidencias de contacto y servicio (idContacto,losServicios) para marcar la regla en caso de coincidencia
                    contTabla += "<tr>";
                    for (var l in contactos) {
                        item = contactos[ l ];
                        contTabla += "<td class='titular' ng-mouseover='mostrarContacto(\"tep" + item.idContacto + "\", $event)' ng-mouseout='ocultarContacto(\"tep" + item.idContacto + "\", $event)'>" + item.nombreContacto + " " + item.pApellidoContacto + "</td>" +
                                "<div class='tep' id='tep" + item.idContacto + "' style='display: none;'><span class='negrita subrayado'>Datos del Contacto</span></br><span class='negrita'>ID: </span>" + item.idContacto + "</br><span class='negrita'>Nombre del Contacto: </span>" + item.nombreContacto + " " + item.pApellidoContacto + "</br><span class='negrita'>Teléfono del Contacto: </span>" + item.telefonoAContacto + "</br><span class='negrita'>Email del Contacto: </span>" + item.emailContacto + "</div>";
                        //hago un bucle de 'numServicios' para colocar las celdas de cada contacto correspondientes a cada servicio
                        //primero el bucle de 'numServicios'
                        var serviciosCombinacion = "";
                        for (r = 1; r <= numServicios; r++) {
                            var td = "";
                            var isChecked = "";
                            var combinacion = json.resultados.combinacion;
                            for (var m in combinacion) {
                                var union = combinacion[ m ];
                                if (union.idContacto == item.idContacto) {
                                    serviciosCombinacion = union.servicioActivoDatas;
                                    for (var z in serviciosCombinacion) {
                                        var unionServicios = serviciosCombinacion[ z ];
                                        if (unionServicios.idServicio === losServicios[r - 1]) {
                                            if (unionServicios.active) {
                                                isChecked = "checked";
                                            }
                                        }
                                    }
                                }
                            }
                            td += "<td class='centrar' id='td-" + item.idContacto + "-" + losServicios[r - 1] + "'>";
                            td += "<input type='checkbox' id='" + item.idContacto + "-" + losServicios[r - 1] + "' value='" + item.idContacto + "-" + losServicios[r - 1] + "' ";
                            td += isChecked + " modified='false' >";
                            td += "</td>";
                            contTabla += td;
                        }
                        contTabla += "</tr>";
                    }
                    contTabla += "</table>";
                    var compTabla = $compile(contTabla)($scope);
                    angular.element('.lanzador').append(compTabla);
                    var btnAsignar = '<button class="mybutton" ng-click="btnAsignar()">Asignar Servicio - Contacto</button>';
                    var btnDeshacer = '<button class="mybutton" ng-click="btnDeshacer()">Deshacer cambios</button>';
                    //var botonera = $compile(trozo)($scope);
                    var compAsignar = $compile(btnAsignar)($scope);
                    var compDeshacer = $compile(btnDeshacer)($scope);
                    angular.element('.piePagina').append(compAsignar);
                    angular.element('.piePagina').append(compDeshacer);

                    angular.element(':checkbox').unbind("click");
                    angular.element(':checkbox').click(function (e) {
                        var tdId = "#td-" + angular.element(e.target).attr('id');
                        console.log('¿ Esta modificado el campo ' + angular.element(e.target).attr('id') + ' ? ' + angular.element(e.target).attr('modified'));
                        console.log("Se modifica el estilo a: " + tdId);
                        if (angular.element(e.target).attr('modified') === 'true') {
                            angular.element(e.target).attr('modified', 'false');
                            angular.element(tdId).removeClass("cambioCombo");
                            angular.element(tdId).removeClass("nocambioCombo");
                        } else {
                            angular.element(e.target).attr('modified', 'true');
                            if (angular.element(e.target).is(':checked')) {
                                angular.element(tdId).addClass("cambioCombo");
                                angular.element(tdId).removeClass("nocambioCombo");
                            } else {
                                angular.element(tdId).removeClass("cambioCombo");
                                angular.element(tdId).addClass("nocambioCombo");
                            }
                        }
                        console.log('Se modifica a: ' + angular.element(e.target).attr('modified'));
                    });
                } else {
                    //aviso de que no hay datos para mostrar la tabla correctamente
                    angular.element('.lanzador').empty();
                    var error = "<div class='error'>No hay datos suficientes para mostrar el Servicio a Cliente</div>";
                    $compile(error)($scope);
                    angular.element('.lanzador').append(error);
                }
            } else {
                alert("No hay resultados");
                growl.addErrorMessage("ERROR: No hay resultados");
            }
            blockUI.stop();
        }
        function cargarTabla(alias) {
            // Ponemos el DIV de "Cargando"...
            console.log("cargarTable, alias: "+alias);
            blockUI.start();
            angular.element('#aliasOculto').val(alias);
            // console.log("entro en el método [alias=="+alias+"]");
            var filtro = {alias: alias};
            AliasService.getContactosByService(onSuccessGetContactosByService, showError, filtro);
        }

        /*asignar reglas*/
        $scope.btnAsignar = function() {

            angular.forEach(angular.element("[modified='true']"), function (elem, key) {
                var value = angular.element(elem).val();
                var position = value.split("-");
                var idServicio = position[1];
                var idContacto = position[0];
                if (angular.element(elem).is(':checked')) {
                    console.log("Esta modificado y chequeado " + value);
                    altaServicioContacto(idServicio, idContacto);
                } else {
                    console.log("Esta modificado y no chequeado " + value);
                    bajaServicioContacto(idServicio, idContacto);
                }
            });
            var valorSeleccionado = angular.element('#aliasOculto').val();
            setTimeout(function () {
                cargarTabla(valorSeleccionado);
            }, 3000);
        };
        function onBajaServicioContactoSuccessRequest(json){
            if(json.error !== null && json.error !== ""){
                blockUI.stop();
                growl.addErrorMessage(json.error);
            }else{
                blockUI.stop();
                growl.addSuccessMessage("El contacto ha sido dado de baja correctamente.");
            }

        }
        function bajaServicioContacto(idServicio, idContacto) {
            blockUI.start();
            var filtro = {idServicio : idServicio, idContacto : idContacto };
            AliasService.bajaServicioContacto(onBajaServicioContactoSuccessRequest, showError, filtro);
        }
        function altaServicioContacto(idServicio, idContacto) {
            var filtro = "{\"idServicio\" : \"" + idServicio + "\", \"idContacto\" : \"" + idContacto + "\" }";
            console.log(filtro);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/sibbac20back/rest/service",
                data: "{\"service\" : \"SIBBACServiceServicios\", \"action\"  : \"altaServicioContacto\", \"filters\"  : " + filtro + "}",
                beforeSend: function (x) {
                    if (x && x.overrideMimeType) {
                        x.overrideMimeType("application/json");
                    }
                    x.setRequestHeader("Accept", "application/json");
                    x.setRequestHeader("Content-Type", "application/json");
                    x.setRequestHeader("X-Requested-With", "HandMade");
                    x.setRequestHeader("Access-Control-Allow-Origin", "*");
                    x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
                },
                async: true,
                success: function (json) {
                    growl.addSuccessMessage("Contacto creado correctamente.");
                },
                error: function (c) {
                    console.log("ERROR: " + c);
                }
            });
        }
        $scope.btnDeshacer = function() {
            var valorSeleccionado = angular.element('#aliasOculto').val();
            cargarTabla(valorSeleccionado);
        };

        //mostrar y ocultar tips
        $scope.mostrarServicio = function(id, event) {
            console.log("mostrarServicio id: "+id);
            angular.element('#'+id).show();
            angular.element('#'+id).css('left', "500px");
            angular.element('#'+id).css('top',+"160px");
        };

        $scope.ocultarServicio  = function(id, event) {
            angular.element('#'+id).hide();
        };

        $scope.mostrarContacto = function(id, event) {
            angular.element('#'+id).show();
            angular.element('#'+id).css('left',"500px");
            angular.element('#'+id).css('top', +"160px");
        };

        $scope.ocultarContacto = function(id, event) {
            angular.element('#'+id).hide();
        };

    }]);

