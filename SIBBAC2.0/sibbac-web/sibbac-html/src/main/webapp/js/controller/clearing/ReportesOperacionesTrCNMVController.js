'use strict';
sibbac20
    .controller(
                'ReportesOperacionesTrCNMVController',
                [
                 '$scope',
                 '$document',
                 '$location',
                 'growl',
                 'ReportesCNMVService',
                 'IsinService',
                 '$compile',
                 function ($scope, $document, $location , growl, ReportesCNMVService, IsinService, $compile) {
                	 
                	 $scope.ejecutaOperacionesTR = false;

                	 
                     ReportesCNMVService.getParametrosEjecucion(function (data) {
                    	 if (data.resultados["crearOperacionesTR"] === 'S'){
                    		 $scope.ejecutaOperacionesTR = true;
                    	 }

                       }, function (error) {
                         fErrorTxt("Se produjo un error al obtener los parametros de ejecucion.", 1);
                       }, null);                	 
                	 
                		 
                	 
                	 
                	 // Generar datos para las Operaciones TR
						$scope.GenerarOperacionesTR = function () {
                    	 	if ($scope.ejecutaOperacionesTR){
                                inicializarLoading();
                                var filters = "{ \"group\" : \"" + 'Fallidos' + "\", \"name\" : \"" + 'CreacionAutomaticaOperacionesTR' + "\" }";
                                performJobCall('FIRE_NOW', filters, null, null, null, null);
                                $.unblockUI();
                                fErrorTxt("Generacion automatica de Operaciones TR iniciada", 3);
                                return false;                    	 		
                    	 	}else{
                    	 		fErrorTxt("La generacion automatica de Operaciones TR no esta habilitada", 1);
                    	 	}
                    	 	
                     }
                                    
                                        
                     $scope.ResultadosEjecucionProcesos = function () {
                    	 $location.path('/clearing/ResultadosEjecucionesCNMV');
                     }
                     
                     
                     
                     function performJobCall (command, filters, params, lista, successCallBack, _clear) {
                         var clear = (_clear !== undefined) ? _clear : true;

                         // Limpiamos los "targets"..
                         if (clear) {
                           $('#tdRunningJobs').empty();
                           $('#tdCurrentJobs').empty();
                         }


                         var data = new DataBean();
                         data.setService('SIBBACServiceJobsManagement');
                         data.setAction(command);
                         if (lista != null) {
                           data.list = lista;
                         }
                         if (filters != null) {
                           data.filters = filters;
                         }
                         if (params != null) {
                           data.params = params;
                         }
                         var SHOW_LOG = false;
                         if (SHOW_LOG)
                           console.log("Llamando al servidor[" + command + "]...");
                         var request = requestSIBBAC(data);
                         request.success(successCallBack);
                         request.error(function (jqXHR, textStatus, errorThrown) {
                           console.log("ERROR: " + jqXHR + ", " + textStatus + ": " + errorThrown);
                           console.log("Headers: " + jqXHR.getAllResponseHeaders());
                           console.log("Response text: " + jqXHR.responseText);
                           hideLoadingLayer(JSON.parse(jqXHR.responseText).error);
                         });
                       }
                     
} ]);