'use strict';
sibbac20
    .controller(
                'RecordatorioController',
                [
                 '$scope',
                 '$compile',
                 '$http',
                 '$rootScope',
                 'IsinService',
                 function ($scope, $compile, $http, $rootScope, IsinService) {
                   var idOcultos = [];
                   var availableTags = [];

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   $(document).ready(function () {

                     /* Eva:Función para cambiar texto */
                     jQuery.fn.extend({
                       toggleText : function (a, b) {
                         var that = this;
                         if (that.text() != a && that.text() != b) {
                           that.text(a);
                         } else if (that.text() == a) {
                           that.text(b);
                         } else if (that.text() == b) {
                           that.text(a);
                         }
                         return this;
                       }
                     });
                     /* FIN Función para cambiar texto */

                     $('a[href="#release-history"]').toggle(function () {
                       $('#release-wrapper').animate({
                         marginTop : '0px'
                       }, 600, 'linear');
                     }, function () {
                       $('#release-wrapper').animate({
                         marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
                       }, 600, 'linear');
                     });

                     $('#download a').mousedown(function () {
                       _gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
                     });

                     $('.collapser').click(function () {
                       $(this).parents('.title_section').next().slideToggle();
                       $(this).toggleClass('active');
                       $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

                       return false;
                     });
                     $('.collapser_search').click(function () {
                       $(this).parents('.title_section').next().slideToggle();
                       $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                       $(this).toggleClass('active');
                       // $(this).toggleText( "Mostrar Alta de Grupo Identificador", "Ocultar
                       // Alta de Grupo Identificador" );
                       if ($(this).text() == "Ocultar Alta de Grupo Identificador") {
                         $(this).text("Mostrar Alta de Grupo Identificador");
                       } else {
                         $(this).text("Ocultar Alta de Grupo Identificador");
                       }
                       return false;
                     });

                     /*
                       * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA
                       */
                     if ($('.contenedorTabla').length >= 1) {
                       var anchoBotonera;
                       $('.contenedorTabla').each(function (i) {
                         anchoBotonera = $(this).find('table').outerWidth();
                         $(this).find('.botonera').css('width', anchoBotonera + 'px');
                         $(this).find('.resumen').css('width', anchoBotonera + 'px');
                         $('.resultados').hide();
                       });
                     }

                     cargarAlias();

                   });
                   $scope.modificarAliasRecordatorio = function () {
                     var importeMinimo = document.getElementById('importeMinimo').value;
                     var idAliasRecordatorio = document.getElementById('idAliasRecordatorio').value;
                     var frecuencia = document.getElementById('periodicidad').value;
                     var idAlias = document.getElementById('idAlias').value;

                     // return false;
                     var filtro = "{ \"idAliasRecordatorio\" : \"" + idAliasRecordatorio + "\""
                                  + ", \"importeMinimo\" : \"" + importeMinimo + "\"" + ", \"frecuencia\" : \""
                                  + frecuencia + "\"" +
                                  // ", \"recordatorio\" : \""+ frecuencia + "\""+
                                  ", \"idAlias\": \"" + idAlias + "\" }";

                     var jsonData = "{" + "\"service\" : \"SIBBACServiceAliasRecordatorio\", "
                                    + "\"action\"  : \"modificacionAliasRecordatorio\", " + "\"filters\"  : " + filtro
                                    + "}";

                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : jsonData,
                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {
                         if (json.resultados == null) {
                           alert(json.error);
                         } else {
                           alert(json.resultados.resultado);
                         }
                       },
                       error : function (error) {
                         alert("ERROR: " + error);
                       }

                     });

                   }

                   /* cargar tabla alias */
                   $('.btn.buscador').click(function () {

                     var listaAlias = document.getElementById("textAlias").value;

                     var posicionAlias = availableTags.indexOf(listaAlias);
                     if (posicionAlias == "-1") {
                       alert("El alias introducido no existe");
                       return false;
                     } else {
                       var valorSeleccionadoAlias = idOcultos[posicionAlias];
                       document.getElementById('idAlias').value = valorSeleccionadoAlias;
                       cargarTabla(valorSeleccionadoAlias);
                     }
                     return false;

                   });

                   // function cargarAlias
                   function cargarAlias () {

                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacion\"}",

                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {

                         var resultados = json.resultados.listaAlias;
                         var item = null;

                         for ( var k in resultados) {
                           item = resultados[k];
                           idOcultos.push(item.id);
                           var ponerTag = item.nombre.trim();
                           availableTags.push(ponerTag);
                         }
                         // código de autocompletar

                         $("#textAlias").autocomplete({
                           source : availableTags
                         });

                       },
                       error : function (c) {
                         console.log("ERROR: " + c);
                       }
                     });
                   }

                   function limpiarDatos () {
                     document.getElementById('idAlias').value = "";
                     document.getElementById('idAliasRecordatorio').value = "";
                     document.getElementById('importeMinimo').value = "";
                     $("#periodicidad").val('0');

                   }

                   // esta es la tabla que carga todos los datos
                   function cargarTabla (idAlias) {
                     // borrar los campos de la búsqueda anterior
                     $("#periodicidad").val('0');
                     document.getElementById('importeMinimo').value = "";
                     var filtro = "{\"idAlias\" : \"" + idAlias + "\" }";
                     $
                         .ajax({
                           type : "POST",
                           dataType : "json",
                           url : "/sibbac20back/rest/service",
                           data : "{\"service\" : \"SIBBACServiceAliasRecordatorio\", \"action\"  : \"getAliasRecordatorio\", \"filters\"  : "
                                  + filtro + "}",

                           beforeSend : function (x) {
                             if (x && x.overrideMimeType) {
                               x.overrideMimeType("application/json");
                             }
                             x.setRequestHeader("Accept", "application/json");
                             x.setRequestHeader("Content-Type", "application/json");
                             x.setRequestHeader("X-Requested-With", "HandMade");
                             x.setRequestHeader("Access-Control-Allow-Origin", "*");
                             x.setRequestHeader('Access-Control-Allow-Headers',
                                                'Origin, X-Requested-With, Content-Type, Accept')
                           },
                           async : true,
                           success : function (json) {
                             $('#aliasRecordatorio').css("display", "block");
                             $("#botoneraAliasRecordatorio").empty();
                             if (json === null || json.resultados === null || json.resultados === undefined
                                 || json.resultados === "") {
                               $('#botoneraAliasRecordatorio')
                                   .append(
                                           '<input class="btn buscador mybutton" type="button"  id="crearRecordatorio" value="Dar de alta"  ng-click="modificarAliasRecordatorio()" />');
                             } else {
                               var datos = json.resultados[idAlias];
                               $('#botoneraAliasRecordatorio')
                                   .append(
                                           '<input class="btn buscador mybutton" type="button"  id="modificarRecordatorio" value="Modificar"  ng-click="modificarAliasRecordatorio()" />');
                               document.getElementById('importeMinimo').value = datos.importeMinimo;
                               document.getElementById('idAliasRecordatorio').value = datos.idAliasRecordatorio;
                               document.getElementById('idAlias').value = datos.idAlias;
                               // cargo en el combo la que es
                               var frecuencia = datos.frecuencia;
                               $("#periodicidad").val(frecuencia);
                             }

                             $scope.safeApply();
                             $compile($('#botoneraAliasRecordatorio'))($scope);

                             $('#botoneraAliasRecordatorio')
                                 .append('<input class="mybutton" type="reset"  id="limpiar" value="Borrar campos" />');
                           },
                           error : function (c) {
                             console.log("ERROR: " + c);
                           }
                         });
                   }

                 } ]);
