(function(angular, sibbac20, gc){
	
	"use strict";
	
	var ImportaFinalHoldersDialogController, ImportaFinalHolders;
	
	ImportaFinalHolders = function(afis, afiErrors) {
		this.listAfiIds = afis;
		this.listAfiErrorIds = afiErrors;
		this.finalHolders = [];
		this.loadFinalHolders = function(selectedRows)  {
			var r, fh, i, l = selectedRows.length;
			for(i = 0; i < l; i++) {
				r = selectedRows[i];
				fh = {cddclave: r.entity.CDDCLAVE, numsec: r.entity.NUMSEC};
				this.finalHolders.push(fh);
			}
		};
		this.isReady = function(sustituye) {
			if(sustituye) {
				return this.finalHolders.length === 1;
			}
			return this.finalHolders.length > 0;
		}
	};
	
	ImportaFinalHoldersDialogController = function(scope, service, httpParamSerializer) {
		gc.FilteredGridController.call(this, service, httpParamSerializer);
		this.scope = scope;
		this.filter = {};
		scope.$on("agregaImportacion", this.agregaImportacion.bind(this));
		scope.$on("sustituyeImportacion", this.sustituyeImportacion.bind(this));
	};
	
	ImportaFinalHoldersDialogController.prototype = Object.create(gc.FilteredGridController.prototype);
	
	ImportaFinalHoldersDialogController.prototype.constructor = ImportaFinalHoldersDialogController;
	
	ImportaFinalHoldersDialogController.prototype.columns = function() {
		return [
			{field: "CDDCLAVE", displayName: "Documento"},
			{field: "NBCLIENT", displayName: "Nombre"},
			{field: "NBCLIEN1", displayName: "1er Apellido"},
			{field: "NBCLIEN2", displayName: "2o Apellido"},
			{field: "TPSOCIED", displayName: "T.Persona"},
			{field: "TPTIPREP", displayName: "T.Titular"},
			{field: "TPIDENTI", displayName: "T.Documento"},
			{field: "TPNACTIT", displayName: "T.Nacionalidad"},
			{field: "CDNACTIT", displayName: "Nacionalidad"},
			{field: "TPDOMICI", displayName: "T.Dirección"},
			{field: "NBDOMICI", displayName: "Dirección"},
			{field: "NUDOMICI", displayName: "Número"},
			{field: "NBCIUDAD", displayName: "Ciudad"},
			{field: "NBPROVIN", displayName: "Provincia"},
			{field: "CDPOSTAL", displayName: "C.Postal"},
			{field: "CDDEPAIS", displayName: "C.País"},
			{field: "NBPAIS", displayName: "País"}
		];
	}
	
	ImportaFinalHoldersDialogController.prototype.executeQuery = function() {
		inicializarLoading();
		this.service.consultaFinalHolders(this.filter, this.queryReceiveData.bind(this), this.queryFail.bind(this));
	}
	
	ImportaFinalHoldersDialogController.prototype.agregaImportacion = function(event, afis, afiErrors) {
		var importaFinalHolders = new ImportaFinalHolders(afis, afiErrors);
		importaFinalHolders.loadFinalHolders(this.gridApiRef.selection.getSelectedGridRows());
		if(importaFinalHolders.isReady(false)) {
			inicializarLoading();
			this.service.agregaTitularesImportados(importaFinalHolders, this.agregacionExitosa.bind(this), 
					this.postFail.bind(this));
			return;
		}
		fErrorTxt("Debe seleccionar por lo menos un Final Holder");
	};
	
	ImportaFinalHoldersDialogController.prototype.sustituyeImportacion = function(event, afis, afiErrors) {
		var importaFinalHolders = new ImportaFinalHolders(afis, afiErrors);
		importaFinalHolders.loadFinalHolders(this.gridApiRef.selection.getSelectedGridRows());
		if(importaFinalHolders.isReady(true)) {
			inicializarLoading();
			this.service.sustituyeTitularesImportados(importaFinalHolders, this.sustitucionExitosa.bind(this), 
					this.postFail.bind(this));
			return;
		}
		fErrorTxt("Debe seleccionar solo un Final Holder");
	};
	
	ImportaFinalHoldersDialogController.prototype.agregacionExitosa = function(respuesta) {
		if(!respuesta.esBloqueante) {
			this.gridApiRef.selection.clearSelectedRows();
		}
		this.scope.$emit("closeImportacion", {accion: "agregacion", resultados: respuesta});
	};
	
	ImportaFinalHoldersDialogController.prototype.sustitucionExitosa = function(respuesta) {
		if(!respuesta.esBloqueante) {
			this.gridApiRef.selection.clearSelectedRows();
		}
		this.scope.$emit("closeImportacion", {accion: "sustitucion", resultados: respuesta});
	};

	sibbac20.controller("ImportaFinalHoldersDialogController", ["$scope", "TitularesRoutingService", 
		"$httpParamSerializer",	ImportaFinalHoldersDialogController]);
	
})(angular, sibbac20, GENERIC_CONTROLLERS);