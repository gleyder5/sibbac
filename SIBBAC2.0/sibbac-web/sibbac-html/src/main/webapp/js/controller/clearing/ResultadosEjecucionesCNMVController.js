'use strict';
sibbac20
    .controller(
                'ResultadosEjecucionesCNMVController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'ReportesCNMVService',
                 '$compile',
                 function ($scope, $document, growl, ReportesCNMVService, $compile) {
                	 
                	 $document.ready(function () {
                         prepareCollapsion();
                    });
                	 
                	 var datos = [];
                	 
                	 $scope.filtro = {
                			 fejecucionDesde : "",
                			 fejecucionHasta : "" };
                	 
                	 
                     var hoy = new Date();
                     var dd = hoy.getDate();
                     var mm = hoy.getMonth() + 1;
                     var yyyy = hoy.getFullYear();
                     hoy = yyyy + "_" + mm + "_" + dd;	 
                	 
                	
                	 $scope.oTable = $("#tablaEjecucionesCNMV").dataTable({
                          "dom" : 'T<"clear">lfrtip',
                          "tableTools" : {
                            "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                            "aButtons" : [ "copy", {
                              "sExtends" : "csv",
                              "sFileName" : "Listado_Ejecuciones_Procesos_" + hoy + ".csv",
                              "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8]
                            }, {
                              "sExtends" : "xls",
                              "sFileName" : "Listado_Ejecuciones_Procesos_" + hoy + ".xls",
                              "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8 ]
                            }, {
                              "sExtends" : "pdf",
                              "sPdfOrientation" : "landscape",
                              "sTitle" : " ",
                              "sPdfSize" : "A3",
                              "sPdfMessage" : "Listado Final Holders",
                              "sFileName" : "Listado_Ejecuciones_Procesos_" + hoy + ".pdf",
                              "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8]
                            }, "print" ]
                          },
                          "aoColumns" : [ {
                            sClass : "centrar",
                            width : "10%"
                          }, {
                            sClass : "centrar",
                            width : "10%"
                          }, {
                            sClass : "centrar",
                            width : "10%"
                          }, {
                            sClass : "centrar",
                            width : "30%"
                          }, {
                            sClass : "centrar",
                            width : "10%"
                          }, {
                            sClass : "centrar",
                            width : "10%"
                          }, {
                            sClass : "centrar",
                            width : "10%"
                          }, {
                            sClass : "centrar",
                            width : "10%"
                          } ],
                          "fnCreatedRow" : function (nRow, aData, iDataIndex) {

                            $compile(nRow)($scope);
                          },

                          "scrollY" : "480px",
                          "scrollX" : "100%",
                          "scrollCollapse" : true,
                          "language" : {
                            "url" : "i18n/Spanish.json"
                          }
                        });
                	 
                	 $scope.Consultar = function () {
                	  
	                	  if ( $scope.filtro.fejecucionDesde === '' ){
	                		  fErrorTxt("Fecha desde es obligatoria para iniciar la consulta", 3);
	                		  return;
	                	   }
	                	  
	                	  if ( $scope.filtro.fejecucionHasta != '' ){
	                		  // La fecha hasta tiene que ser mayor a la fecha desde.
	                		  var bFechas = compare_dates($scope.filtro.fejecucionDesde,$scope.filtro.fejecucionHasta);
	                		  if (bFechas) {
	                			  fErrorTxt("Fecha hasta tiene que ser mayor o igual a la fecha desde", 3);
		                		  return;
	                		  }
	                		  
	                	   }
	                	  
	                	  inicializarLoading();
	                	  // si las validaciones son correctas obtenemos los datos.
	                	  ReportesCNMVService.getResultadosEjecuciones(onSuccessResultadosEjecucionesCNMV, onErrorRequest, $scope.filtro);
                	  }	  

                	 
                	 $scope.LimpiarFiltros = function () {
                       	 $scope.filtro = {
                    			 fejecucionDesde : "",
                    			 fejecucionHasta : "" };
                	 }
                	  
                	  function onSuccessResultadosEjecucionesCNMV (json) {
                          if (json !== null && json !== undefined) {
                        	  datos = json.resultados.listaResultados;
                        	  
                              // borra el contenido del body de la tabla
                              var tbl = $("#tablaEjecucionesCNMV > tbody");
                              $(tbl).html("");
                              $scope.oTable.fnClearTable();

                              for (var i = 0; i < datos.length; i++) {
                                
                            	var enlaceComentario = ''; 
                            	
                            	if   (datos[i].comentario != null){
                            		enlaceComentario = "<img src='/sibbac20/images/add_verde.png' ng-click='muestraComentario("
                                        + i + ")' id='comentario_" + i + "' >";
                            	}
                            	var filaEjecucion =[datos[i].fEjecucion,
                            	                    datos[i].hEjecucion, 
                            	                    datos[i].usuario,
                            	                    datos[i].proceso, 
                            	                    datos[i].numAltas, 
                            	                    datos[i].numBajas,
                            	                    datos[i].bIncidencia,
                            	                    enlaceComentario
                            	                    ];  
                            	  
                                $scope.oTable.fnAddData(filaEjecucion, false);

                              }
                              $scope.oTable.fnDraw();
                             // $scope.safeApply();
                              $.unblockUI();                        	  
                        	  
                          }else{
                        	  fErrorTxt("No se ha recuperado ningun registro", 3);
                        	  $.unblockUI();
                          }  	  
                      }
                	  
                	  $scope.muestraComentario = function (fila){
                		  $('#comentarioEjecucion').val( datos[fila].comentario);
                		  $('#ComentarioEjecucionH47').css('display', 'inline');
                		  
                	  }
                	  
                	  $scope.cerrar = function () {
                		  $('#comentarioEjecucion').val('');
                		  $('#ComentarioEjecucionH47').css('display', 'none');
                	  }
                	  
                      function onErrorRequest (data) {
                          $.unblockUI();
                          fErrorTxt("Ocurrió un error durante la petición de datos.", 1);
                        } // onErrorRequest


                	  
} ]);
