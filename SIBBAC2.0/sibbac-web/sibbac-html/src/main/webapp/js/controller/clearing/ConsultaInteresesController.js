sibbac20.controller('ConsultaInteresesController', ['$scope', '$document', 'growl', '$compile', 'AliasService', 'FinanciacionInteresesService',
    function ($scope, $document, growl, $compile, AliasService, FinanciacionInteresesService) {
        var idOcultosAlias = [];
        var availableTagsAlias = [];
        var popupsInteresAlta = undefined;
        var oTable = undefined;
        var cont   = 0;

        $scope.listCheck = [];
        var listaCheckId = [];

        $scope.filtro = {
            fechaInicio: "",
            fechaHasta: "",
            importeInteresesDesde: "",
            importeInteresesHasta: "",
            estado: "",
            alias: "",
            chestado: true,
            chalias: true
        };

        $scope.listaAlias = {};
        $scope.followSearch = "";

        function initialize() {

            //verificarFechas(["fechaInicio", "fechaHasta"]);

            inicializarLoading();
            loadAlias();
            render();
        }

        $document.ready(function () {
        	//var fechas = ['fechaInicio', 'fechaHasta'];
            //loadpicker(fechas);
        	 $("#ui-datepicker-div").remove();
             $('input#fechaInicio').datepicker({
                 onClose: function( selectedDate ) {
                   $( "#fechaHasta" ).datepicker( "option", "minDate", selectedDate );
                 } // onClose
               });

               $('input#fechaHasta').datepicker({
                 onClose: function( selectedDate ) {
                   $( "#fechaInicio" ).datepicker( "option", "maxDate", selectedDate );
                 } // onClose
               });
            loadDataTable();
            initialize();
            angular.element('#tablaIntereses tbody').on('click', 'td.checkbox', function () {

                var nTr = $(this).parents('tr')[0];
                var aData = oTable.fnGetData(nTr);

                var valor = $('input', nTr)[0].getAttribute('value');
                if (valor == "N") {
                    $('input', nTr)[0].setAttribute('value', aData[0]);
                } else {
                    $('input', nTr)[0].setAttribute('value', 'N');
                }

            });
            prepareCollapsion();

            angular.element('#canceloEnvioCalculoIntereses').click(function (event) {
                popupsInteresAlta.close();
            });

            angular.element('#aceptoEnvioCalculoIntereses').click(function (event) {
                var data = new DataBean();
                var filter = {cdalias: angular.element("#enviarCalculoIntereses").attr("idAlias")};
                inicializarLoading();
                FinanciacionInteresesService.enviarCalculoIntereses(onSuccessEnviarCalculoInteresesRequest, onErrorRequest, filter);
            });


            angular.element('#enviarCalculoIntereses').click(function (event) {
                angular.element("#textoDescriptivoAlias").text($("#enviarCalculoIntereses").attr("descripcionAlias"));
                popupsInteresAlta = $("#avisoEnvioCalculoIntereses").bPopup();
            });

            angular.element("#limpiar").click(function (event) {
            	//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
                $( "#fechaHasta" ).datepicker( "option", "minDate", "");
                $( "#fechaInicio" ).datepicker( "option", "maxDate", "");

                //
             	$scope.filtro = {
                        fechaInicio: "",
                        fechaHasta: "",
                        importeInteresesDesde: "",
                        importeInteresesHasta: "",
                        estado: "",
                        alias: "",
                        chestado: false,
                        chalias: false
                };
             	angular.element("#estado").val('');
        		$scope.btnInvGenerico('Estado');
        		$scope.btnInvGenerico('Alias');

                angular.element("#enviarCalculoIntereses").attr("idAlias", "");
                angular.element("#enviarCalculoIntereses").attr("descripcionAlias", "");
                angular.element("#enviarCalculoIntereses").hide();
            });

        });
        function cargarFechas(){
          	$scope.filtro.fcontratacionDe = $('#fechaInicio').val();
          	$scope.filtro.fcontratacionA = $('#fechaHasta').val();

        }
        function onSuccessEnviarCalculoInteresesRequest(json) {
            popupsInteresAlta.close();
        }
        function loadDataTable() {
            oTable = angular.element("#tablaIntereses").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    if (aData[2] == "A") {
                    	listaCheckId[cont] = aData[0];
                        $scope.listCheck[cont] = false;
                        angular.element('td:eq(0)', nRow).html('<input type="checkbox" ng-model="listCheck[' + cont + ']" id="checkTabla" name="checkTabla" class="checkTabla" value="N" >');
                        cont++;
                    } else {
                        angular.element('td:eq(0)', nRow).html('<input type="checkbox" id="checkTabla" name="checkTabla" class="checkTabla" value="N" checked="checked" disabled readonly>');
                    }
                    $compile(nRow)($scope);
                },
                "order": []
            });
        }
//        $scope.submitForm = function (event) {
        $scope.Consultar = function (event) {
        	cargarFechas();
            cargarDatosIntereses();
            seguimientoBusqueda();
            collapseSearchForm();
        };
        function fnEnviarCalculoIntereses(idAlias, desc) {
            console.log(idAlias);
            if (idAlias === undefined || idAlias === null || idAlias === "") {
                angular.element("#enviarCalculoIntereses").attr("idAlias", "");
                angular.element("#enviarCalculoIntereses").attr("descripcionAlias", "");
                angular.element("#enviarCalculoIntereses").hide();
            } else {
                angular.element("#enviarCalculoIntereses").attr("idAlias", idAlias);
                try {
                    angular.element("#enviarCalculoIntereses").attr("descripcionAlias", desc);
                } catch (err) {
                    growl.addErrorMessage("La estructura del alias no contiene descripcion " + err.message);
                }
                angular.element("#enviarCalculoIntereses").show();
            }
        }

        function render() {
            var input = "<input id='enviarCalculoIntereses' type='button' class='mybutton' value='Enviar Calculo de Intereses'>";
            angular.element('#paraalias').append(input);
            angular.element("#enviarCalculoIntereses").hide();
        }

        function cargarDatosIntereses() {
        	inicializarLoading();
            var estado = (!$scope.filtro.chestado && $scope.filtro.estado !== "" ? "#" : "" ) +  $scope.filtro.estado;
            var alias = (!$scope.filtro.chalias && $scope.filtro.alias !== "" ? "#" : "") + getAliasId();

            var filtro = {
                fechaInicio: $scope.filtro.fechaInicio,
                fechaHasta: $scope.filtro.fechaHasta,
                importeInteresesDesde: $scope.filtro.importeInteresesDesde,
                importeInteresesHasta: $scope.filtro.importeInteresesHasta,
                cdalias: alias,
                estado: estado

            };

            FinanciacionInteresesService.getListaIntereses(onSuccessListaInteresesRequest, onErrorRequest, filtro);
        }
        function onSuccessListaInteresesRequest(json) {

            if (json !== null && json !== undefined && json.resultados !== undefined && json.resultados.lista !== undefined) {
                var datos = json.resultados.lista;
                oTable.fnClearTable();
                cont = 0;
                $scope.listCheck = [];
                listaCheckId = [];

                angular.forEach(datos, function (item, k) {

                    var rowIndex = oTable.fnAddData([item.idInteres,
                        item.referencia,
                        item.estado,
                        item.sentido,
                        item.cdalias,
                        item.mercado,
                        item.isin,
                        item.titular,
                        item.titulos,
                        transformaFechaGuion(item.fechaContratacion),
                        transformaFechaGuion(item.fechaOperacion),
                        transformaFechaGuion(item.fechaLiquidacion),
                        $.number(item.importeliquidado, 2, ',', '.'),
                        $.number(item.tipoInteres, 2, ',', '.'),
                        item.base,
                        $.number(item.intereses, 2, ',', '.')
                    ], false);
                });
                oTable.fnDraw();
            }
            $.unblockUI();
            if (json !== null && json.error !== undefined && json.error !== undefined && json.error !== "") {
                growl.addErrorMessage(json.error);
            }
        }


        function MarcarEstadoBaja(valores)
        {
        	var bucleVal = valores.length;
            console.log("entro en el metodo");

            var params = [];
            for (var v = 0; v < bucleVal; v++) {
                params[v] = ({idInteres: valores[v]});
            }
            inicializarLoading();
            FinanciacionInteresesService.MarcarBajaIntereses(onSuccessMarcarBajaInteresesRequest, onErrorRequest, params);
        }

        //Evento para marcar gastos
        $scope.marcar = function () {
            var suma = 0;
            var valores = new Array();
            for (var i = 0 ; i < $scope.listCheck.length; i++) {

                if ($scope.listCheck[i] === true) {
                    valores.push(listaCheckId[i]);
                    suma++;
                }
            }

            if (suma == 0) {
                alert('Marque al menos una línea');
                return false;
            } else {
                MarcarEstadoBaja(valores);
            }
        };


        function onSuccessMarcarBajaInteresesRequest(json) {
            cargarDatosIntereses();
            $.unblockUI();
        }
        function seguimientoBusqueda() {

            $scope.followSearch = "";
            if ($scope.filtro.fechaInicio !== "") {
                $scope.followSearch += " F. Contratacion Desde: " + $scope.filtro.fechaInicio;
            }
            if ($scope.filtro.fechaHasta !== "") {
                $scope.followSearch += " F. Contratacion Hasta: " + $scope.filtro.fechaHasta;
            }
            if ($scope.filtro.importeInteresesDesde !== "") {
                $scope.followSearch += " Importe Intereses Desde: " + $scope.filtro.importeInteresesDesde;
            }
            if ($scope.filtro.importeInteresesHasta !== "") {
                $scope.followSearch += " Importe Intereses Hasta: " + $scope.filtro.importeInteresesHasta;
            }

            if ($scope.filtro.estado !== "" ) {
          	  if($scope.filtro.chestado)
          		  $scope.followSearch += "Estado: " + document.getElementById("estado").value;
          	  else
          		  $scope.followSearch += "Estado distinto: " + document.getElementById("estado").value;;
            }

            if ($scope.filtro.alias !== "") {
            	if($scope.filtro.chalias)
            		$scope.followSearch += " Alias: " + document.getElementById("alias").value;
            	else
            		$scope.followSearch += " Alias distinto: " + document.getElementById("alias").value;;
            }
        }
        function loadAlias() {
            inicializarLoading();
            AliasService.getAlias().success(cargaAlias).error(onErrorRequest);
        }
        function cargaAlias(data) {
            $scope.listaAlias = data.resultados.result_alias;
            growl.addInfoMessage($scope.listaAlias.length + " Alias cargados.");

            angular.forEach($scope.listaAlias, function (val, key) {
                var ponerTag = {label : val.alias + " - " + val.descripcion,
                id : val.alias, descripcion : val.descripcion};
                availableTagsAlias.push(ponerTag);
            });
            // código de autocompletar
            angular.element("#alias").autocomplete({
                source: availableTagsAlias,
                select: function(event, ui){
                    fnEnviarCalculoIntereses(ui.item.id, ui.item.descripcion);
                }
            });
            $.unblockUI();
        }

        function getAliasId() {
            var alias = $("#alias").val();

            var guion = alias.indexOf("-");
            return (guion > 0) ? alias.substring(0, guion).trim() : alias;
        }
        function onErrorRequest(data) {
            $.unblockUI();
            growl.addErrorMessage("Ocurrió un error durante la petición de datos.");
        }

    	$scope.btnInvEstado = function() {
    		$scope.btnInvGenerico('Estado');
      	}

    	$scope.btnInvAlias = function() {
    		$scope.btnInvGenerico('Alias');
    	}
    	$scope.btnInvGenerico = function( nombre ) {

      	  var check = '$scope.filtro.ch' + nombre.toLowerCase();

        	  if(eval(check)){
        		  	  var valor2 = check + "= false";
        		  	  eval(valor2 ) ;

          		  document.getElementById('textBtnInv'+nombre).innerHTML = "<>";
          		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "red"});
          		  document.getElementById('textInv'+nombre).innerHTML = "DISTINTO";
          	  }else{
                 	  var valor2 = check + "= true";
              	  eval(valor2 ) ;

          		  document.getElementById('textBtnInv'+nombre).innerHTML = "=";
          		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "green"});
          		  document.getElementById('textInv'+nombre).innerHTML = "IGUAL";
          	  }
          }
    }]);

