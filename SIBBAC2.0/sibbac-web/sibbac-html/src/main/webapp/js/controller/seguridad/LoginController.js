'use strict';
sibbac20.controller('LoginController',
                    [
                     '$rootScope',
                     '$scope',
                     '$location',
                     'SecurityService',
                     'ParametrosSeguridadService',
                     'growl',
                     '$interval',
                     '$timeout',
                     function ($rootScope, $scope, $location, SecurityService, ParametrosSeguridadService, growl,$interval,$timeout) {

                       console.log("LoginController");

                       /***********************************************************************************************
                         * ** FUNCIONES GENERALES ***
                         **********************************************************************************************/

                       //ponemos por defecto el foco en el input de usuario
                       angular.element('#usuarioInput').focus();
                       
                      
                       $timeout( function(){
                    	   console.log("$rootScope.isAuthenticated() is:"+$rootScope.isAuthenticated())
                    	   console.log("PATH IS: "+ $location.path())
                    	   if($rootScope.isAuthenticated() && $location.path()=='/login'){
                        	   console.log("path is login yet the user is authenticated... going to /")
                        	   $location.path('/');
                           }
                       }, 1000 );
                       
                       const numeros = /[0-9]/g;
                       const mayusculas = /[A-Z]/g;
                       const caracteresExtranos = /[|@#&*$€?¿Ç!Ñ=(){}%]/g;

                       $scope.alerta = function () {
                         alert("");
                       }
                       
                       $scope.user = {
                         username : null,
                         password : null,
                         nuevoPassword : null
                       };

                       
                       var abreModalContrasena = function(){
                           $scope.nuevoPassword = null;
                           $scope.passwordActual = null;
                           $scope.confirmacionPassword = null;
                           $scope.mensajeAlert = '';
                       };
                       
                       $scope.$on('abreModalContraseña', abreModalContrasena);
                       
                       $scope.usuarioVerificado = false;
                       $scope.passwordReseteado = false;

                       $scope.verificarUsuario = function () {
                         $scope.mensaje = null;
                         SecurityService.verificarUsuario(function (data) {
                           if (data.resultados.passwordReseteado === true) {
                             $scope.passwordReseteado = true;
                             $scope.usuarioVerificado = true;
                           } else {
                             $scope.passwordReseteado = false;
                             $scope.usuarioVerificado = true;
                           }
                         }, function (error) {
                           growl.addErrorMessage("No se pudo verificar sesión usuario");
                         }, $scope.user);
                       };

                       $scope.mostrarMensajeError = function (mensaje){
                         $scope.mensajeError = mensaje;
                         angular.element('#mesajeError').modal({
                           backdrop : 'static'
                         });

                         $(".modal-backdrop").remove();
                       }


                       /***********************************************************************************************
                         * ** REESTABLECER CONTRASEÑA Y ACCESO ***
                         **********************************************************************************************/
                       $scope.doRestablecerLogin = function () {
                         SecurityService.parametrosComplejidad(function (data) {
                           $scope.parametros = data.resultados["parametros"];
                           
                          
                           if($scope.user.nuevoPassword.length >= $scope.parametros.longitud){

                             var numDigitos = contarOcurrencias(numeros, $scope.user.nuevoPassword);
                             var numMayusculas = contarOcurrencias(mayusculas, $scope.user.nuevoPassword);
                             var numCarExtranos = contarOcurrencias(caracteresExtranos, $scope.user.nuevoPassword);

                             if(numMayusculas >= $scope.parametros.mayusculas
                                 && numDigitos >= $scope.parametros.numeros
                                 && numCarExtranos >= $scope.parametros.caracteresExtranos){
                               $scope.mensajeAlert = "";

                               if ($scope.confirmacionPassword === $scope.user.nuevoPassword) {
                                 SecurityService.validarHistoricoCrontrasenias(function (data) {
                                   if (data.resultados.repetido){
                                     $scope.mostrarMensajeError("La contraseña ha sido utilizada , inserte una contraseña nueva.");
                                   }else{
                                     $scope.doLogin();  // Nueva contraseña ok se accede al sistema
                                   }
                                 }, function (error) {
                                   fErrorTxt("Se produjo un error al validar el historico de contraseñas.", 1);
                                 }, $scope.user);
                               } else {
                                 $scope.mostrarMensajeError("El campo Confirmación Password debe coincidir con el campo Nuevo Password.");
                               }
                             }else{
                               var mensaje = "La contraseña debe contener un minimo de " ;
                               if ($scope.parametros.mayusculas > 1){
                                 mensaje = mensaje + $scope.parametros.mayusculas + " mayúsculas " ;
                               }

                               if ($scope.parametros.mayusculas === 1){
                                   mensaje = mensaje + $scope.parametros.mayusculas + " mayúscula " ;
                                 }
                               
                               
                               if ($scope.parametros.numeros > 1){
                                 mensaje = mensaje + $scope.parametros.numeros + " números " ;
                               }

                               if ($scope.parametros.numeros === 1){
                                   mensaje = mensaje + $scope.parametros.numeros + " número " ;
                                 }                               
                               
                               if ($scope.parametros.caracteresExtranos > 1){
                                 mensaje = mensaje + $scope.parametros.caracteresExtranos + " caracteres especiales validos: [|@#&*$€?¿Ç!Ñ=(){}%] " ;
                               }

                               if ($scope.parametros.caracteresExtranos === 1){
                                   mensaje = mensaje + $scope.parametros.caracteresExtranos + " caracter especial valido: [|@#&*$€?¿Ç!Ñ=(){}%] " ;
                                 }
                               
                               
                               $scope.mostrarMensajeError(mensaje);

                             }
                           }else{
                             $scope.mostrarMensajeError("la longitud del password debe ser superior a " + $scope.parametros.longitud + " carateres");
                           }
                         }, function (error) {
                           fErrorTxt("Se produjo un error al obtener los parametros de seguridad.", 1);
                         });
                       };


                       /***********************************************************************************************
                         * ** ACCESO ***
                         **********************************************************************************************/
                       $scope.doLogin = function () {
                         $scope.mensaje = null;
                         SecurityService.login(function (data) {
                           if (data.resultados.existeUsuario === true) {
                             if (data.resultados.autenticado === true) {
                               if (data.resultados.diasExpira >= 0) {

                                 if (data.resultados.diasExpira === 0){
                                   $scope.mensajeCaducidad = "Su contraseña expira hoy. Renuevela accediendo a la opción: Cambio Contraseña. ";
                                 }else{
                                   $scope.mensajeCaducidad = "Su contraseña deberá ser renovada en un plazo máximo de " + data.resultados.diasExpira + " días. ";
                                 }
                                   angular.element('#mesajeContrasena').modal({
                                       backdrop : 'static'
                                     });

                                     $(".modal-backdrop").remove();

                               } else {
                                 location.href = server.frontUrl;
                               }

                             } else {
                               growl.addErrorMessage("Error de autenticacion");

                               if (data.resultados.passwordVigente) {
                                 if (data.resultados.usuarioBloqueado) {
                                   $scope.mostrarMensajeError("Usuario bloqueado, Contacte con el administrador");
                                 } else {
                                   if (data.resultados.numIntentos > 0) {
                                     $scope.mostrarMensajeError("Contraseña incorrecta, Numero de intentos restantes: " + data.resultados.numIntentos );
                                   } else {
                                     $scope.mostrarMensajeError("Contraseña incorrecta,  El usuario se ha bloqueado");
                                   }
                                 }
                               } else {
                                 $scope.mostrarMensajeError("El password ha expirado, Contacte con el administrador");
                               }
                             }
                           } else {
                             $scope.mostrarMensajeError("El usuario no existe");
                           }
                         }, function (error) {
                           growl.addErrorMessage("No se pudo verificar sesión usuario");
                         }, $scope.user);
                       };

                       $scope.doLogout = function () {
                         SecurityService.logout(function (data) {
                           $rootScope.authenticated = false;
                           $location.path('/login');
                         }, function (error) {
                           growl.addErrorMessage("No se pudo finalizar sesión usuario");
                         }, {});
                       };

                       console.log("Path red:" + $location.path());
                       if ($location.path() == "/logout") {
                         $scope.doLogout();
                       }

                       $scope.aceptarMensaje = function(){
                    	   location.href = server.frontUrl;
                    	   angular.element('#mesajeContrasena').modal('hide');
                       };


                       /***********************************************************************************************
                         * ** RENOVACION CONTRASENA ***
                         **********************************************************************************************/

                       $scope.mensajeAlert = "";
                       $scope.nuevoPassword = null;
                       $scope.passwordActual = null;
                       $scope.confirmacionPassword = null;

                       $scope.parametros = {
                         mayusculas : null,
                         numeros : null,
                         caracteresExtranos : null,
                         longitud : null,
                         tiempoExpiracion : null,
                         avisoExpiracion : null,
                         intentosBloqueo : null,
                         cotrasenasRepetidas : null
                       };

                       $scope.validarContrasenaNueva = function () {

                         $scope.mensajeAlert = "";
                         $scope.user.username =  $rootScope.userName;
                         $scope.user.password =  $scope.passwordActual;
                         $scope.user.nuevoPassword =  $scope.nuevoPassword;
                         
                         if  ($scope.user.nuevoPassword == undefined   ||  $scope.user.nuevoPassword == ''){
                        	  $scope.mensajeAlert = "La contraseña nueva no puede estar vacia " 
                         }else{

	                         SecurityService.validaContraseniaActual(function (data) {
	                           if (data.resultados.passwordActual){
	
	                           ParametrosSeguridadService.consultarParametros(function (data) {
	                                 $scope.parametros = data.resultados["parametros"];
	                                 
	                                 
	                                 
	                                 if($scope.nuevoPassword.length >= $scope.parametros.longitud){
	                                   $scope.mensajeAlert = "";
	                                   var numDigitos = contarOcurrencias(numeros, $scope.nuevoPassword);
	                                   var numMayusculas = contarOcurrencias(mayusculas, $scope.nuevoPassword);
	                                   var numCarExtranos = contarOcurrencias(caracteresExtranos, $scope.nuevoPassword);
	
	                                   if(numMayusculas >= $scope.parametros.mayusculas
	                                       && numDigitos >= $scope.parametros.numeros
	                                       && numCarExtranos >= $scope.parametros.caracteresExtranos){
	                                     $scope.mensajeAlert = "";
	
	                                     if ($scope.confirmacionPassword === $scope.nuevoPassword) {
	                                       $scope.mensajeAlert = "";
	                                       $scope.user.username =  $rootScope.userName;
	                                       $scope.user.password =  $scope.nuevoPassword;
	
	                                       SecurityService.validarHistoricoCrontrasenias(function (data) {
	                                         if (data.resultados.repetido){
	                                           $scope.mensajeAlert = "La contraseña ha sido utilizada, inserte una contraseña nueva.";
	                                         }else{
	                                           SecurityService.resetearPassword(function (data) {
	                                             if (data.resultados.reseteado){
	                                               fErrorTxt("la contraseña se modifico correctamente.", 3);
	
	                                             }else{
	                                               fErrorTxt("Se ha producido un error al modificar el password. Contacte con el administrador", 1);
	                                             }
	
	                                             angular.element('#cambio_contrasena_modal').modal('hide');
	
	                                           }, function (error) {
	                                             fErrorTxt("Se produjo un error al guardar la nueva cotraseña.", 1);
	                                           }, $scope.user);
	                                         }
	                                       }, function (error) {
	                                         fErrorTxt("Se produjo un error al validar el historico de contraseñas.", 1);
	                                       }, $scope.user);
	                                     } else {
	                                       $scope.mensajeAlert = "El campo Confirmación Password debe coincidir con el campo Nuevo Password.";
	                                     }
	                                   }else{
	
	                                     $scope.mensajeAlert = "La contraseña debe contener un minimo de " ;
	                                     if ($scope.parametros.mayusculas > 1){
	                                    	 $scope.mensajeAlert = $scope.mensajeAlert + $scope.parametros.mayusculas + " mayúsculas " ;
	                                       }
	
	                                       if ($scope.parametros.mayusculas === 1){
	                                    	   $scope.mensajeAlert = $scope.mensajeAlert + $scope.parametros.mayusculas + " mayúscula " ;
	                                         }
	                                       
	                                       
	                                       if ($scope.parametros.numeros > 1){
	                                    	   $scope.mensajeAlert = $scope.mensajeAlert + $scope.parametros.numeros + " números " ;
	                                       }
	
	                                       if ($scope.parametros.numeros === 1){
	                                    	   $scope.mensajeAlert = $scope.mensajeAlert + $scope.parametros.numeros + " número " ;
	                                         }                               
	                                       
	                                       if ($scope.parametros.caracteresExtranos > 1){
	                                    	   $scope.mensajeAlert = $scope.mensajeAlert + $scope.parametros.caracteresExtranos + " caracteres especiales validos: [|@#&*$€?¿Ç!Ñ=(){}%] " ;
	                                       }
	
	                                       if ($scope.parametros.caracteresExtranos === 1){
	                                    	   $scope.mensajeAlert = $scope.mensajeAlert + $scope.parametros.caracteresExtranos + " caracter especial valido: [|@#&*$€?¿Ç!Ñ=(){}%] " ;
	                                         }
	
	                                   }
	                                 }else{
	                                   $scope.mensajeAlert = "La longitud del password debe ser superior a " + $scope.parametros.longitud + " carateres";
	                                 }
	                           }, function (error) {
	                             fErrorTxt("Se produjo un error en la carga de los parametros de seguridad.", 1);
	                           });
	
	
	                           }else{
	                             $scope.mensajeAlert = "La contraseña actual no es correcta " ;
	                           }
	
	                         }, function (error) {
	                           growl.addErrorMessage("Error al validar el password actual");
	                         }, $scope.user);
                         }   

                       };

                        var contarOcurrencias = function(regex,str){
                          var cont = 0;
                          let m;

                          while ((m = regex.exec(str)) !== null) {
                            // This is necessary to avoid infinite loops with zero-width matches
                            if (m.index === regex.lastIndex) {
                              regex.lastIndex++;
                            }

                            // The result can be accessed through the `m`-variable.
                            m.forEach((match, groupIndex) => {
                              cont++;
                            });
                        }

                          return cont;
                        };

         }]);
