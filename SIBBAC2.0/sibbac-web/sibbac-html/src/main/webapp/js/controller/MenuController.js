sibbac20
    .controller(
                'MenuController',
                [
                 '$scope',
                 '$location',
                 '$document',
                 'ConfigManagerService',
                 'growl',
                 'SecurityService',
                 'Logger',
                 function ($scope, $location, $document, ConfigManagerService, growl, SecurityService, Logger) {
                   console.log("location: " + location);
                   $scope.urlSibbac = "";
                   $scope.urlRetrieveFilter = {
                     aplication : 'SBRMV',
                     process : 'SIBBAC',
                     keyname : 'URL'
                   };
                   function onSuccessRetrieveSibbacUrl (data, status, headers, config) {
                     if (data !== undefined && data.error !== null && data.error !== "") {
                       growl.addErrorMessage(data.error);
                     } else if (data.resultados !== undefined && data.resultados.result_url !== undefined) {
                       $scope.urlSibbac = data.resultados.result_url;
                     } else {
                       growl
                           .addErrorMessage('Ocurrió un error en una petición al servidor. Por favor inténtelo más tarde.');
                     }
                   }
                   function onErrorHandler (data, status, headers, config) {
                     growl.addErrorMessage("Request error " + data);
                   }
                   $scope.navigateTo = function ($event) {
                     if (typeof refreshDataListener != 'undefined') {
                       clearInterval(refreshDataListener);
                     }
                     var clicked = $event.target;
                     // console.log("clicked element: "+clicked);
                     angular.element(clicked).toggleClass('activo');
                     if (angular.element(clicked).hasClass('activo')) {
                       angular.element(clicked).siblings().animate({
                         height : "toggle"
                       }, 100);
                       angular.element(clicked).closest('ul').find('li a.activo').not(this).siblings('div').hide();
                       angular.element(clicked).closest('ul').find('li a.activo').not(this).removeClass('activo');
                     } else {
                       angular.element(clicked).siblings().animate({
                         height : "toggle"
                       }, 100);
                     }
                     changeTitle(clicked);
                   }
                   changeTitle = function (elem) {
                     var titulo = $(elem).attr("title");
                     if (titulo === "" || titulo === undefined) {
                       titulo = "SIBBAC20";
                     }
                     document.title = titulo;
                     angular.element('#tituloHeader').empty();
                     angular.element('#tituloHeader').append(titulo);
                   };

                   $scope.isActiveLink = function (link) {
                     // console.log(link);
                     if (security.active) {
                       return SecurityService.isPermisoPagina(link.trim());
                     } else {
                       return true;
                     }
                   };

                   $document.ready(function () {
                     // ConfigManagerService.getSibbacUrl(onSuccessRetrieveSibbacUrl, onErrorHandler,
                      // $scope.urlRetrieveFilter);
                   });

                 } ]);
