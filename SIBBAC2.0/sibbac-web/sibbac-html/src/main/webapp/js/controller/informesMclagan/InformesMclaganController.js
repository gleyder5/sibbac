(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var InformesMclaganController = function (service, httpParamSerializer, uiGridConstants, $scope) {

		var ctrl = this;
		ctrl.scope = $scope;
		ctrl.service = service;
		ctrl.params = {};
		ctrl.task = {};
		ctrl.dialogs = ["#resultDialog"];


		ctrl.dialogs.forEach(dialog => {
			var aux = $('[role=dialog]').find(dialog);
			(aux.length > 0) ? aux.dialog('destroy').remove() : undefined;
		});

		ctrl.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					ctrl.resultDialog.dialog("close");
				}
			}
		});

		ctrl.resultDialogTask = angular.element("#resultDialogTask").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Confirmar": function () {

					inicializarLoading();
					ctrl.service.generate(ctrl.params, function (result) {
						ctrl.resultDialogTask.dialog("close");

						$.unblockUI();
						ctrl.result = result;

						// Ejecuta el modal de respuesta al revicio
						ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
						ctrl.isNullReturned();
						ctrl.resultDialog.dialog("open");
					}, ctrl.postFail.bind(ctrl));
				},
				"Cancelar": function () {
					ctrl.resultDialogTask.dialog("close");
				}
			}
		});

		jQuery(document).ready(function () {
			jQuery('#fefrom').datepicker({
				minDate: new Date(1900, 0, 1),
				maxDate: new Date(2100, 11, 31),
				dateFormat: 'dd/mm/yy',
				constrainInput: true,
				beforeShowDay: desdeMenorQueHasta
			});
		});

		jQuery(document).ready(function () {
			jQuery('#feto').datepicker({
				minDate: new Date(1900, 0, 1),
				maxDate: new Date(2100, 11, 31),
				dateFormat: 'dd/mm/yy',
				constrainInput: true,
				beforeShowDay: desdeMenorQueHasta
			});
		});

		function desdeMenorQueHasta(date) {
			if ($(this)[0].id === 'fefrom') {
				if (ctrl.params.fechaHasta != undefined) {
					return [date <= ctrl.formatDates(ctrl.params.fechaHasta)];
				} else if (ctrl.params.fechaHasta === undefined) {
					return [true];
				}
			} else if ($(this)[0].id === 'feto') {
				if (ctrl.params.fechaDesde != undefined) {
					return [date >= ctrl.formatDates(ctrl.params.fechaDesde)];
				} else if (ctrl.params.fechaDesde === undefined) {
					return [true];
				}
			}
			return [false];
		};

		ctrl.postFail = function (error) {
			var message;
			if (typeof error === "string") {
				message = "Se produjo un error al ejecutar la acción: " + error;
			}
			else if (error.length == 1) {
				message = error;
			}
			else {
				message = "Se produjeron los siguientes errores: \n";
				angular.forEach(error, function (value) {
					message += ("- " + value + "\n");
				});
			}
			fErrorTxt(message, 1);
			$.unblockUI();
		};



		ctrl.generarInformes = function () {
			var ctrl = this;
			ctrl.params.trimestre = 5;
			
			var fdesde = ctrl.formatDates(ctrl.params.fechaDesde);
			var fhasta = ctrl.formatDates(ctrl.params.fechaHasta);
			
			var desdeMonth = fdesde.getMonth() + 1; 
			var hastaMonth = fhasta.getMonth() + 1;
						
			if (fdesde.getFullYear() === fhasta.getFullYear()) {
				if (ctrl.validateTrimestre(desdeMonth, hastaMonth, 1, 3)) {
					ctrl.params.trimestre = 1;
				} else if (ctrl.validateTrimestre(desdeMonth, hastaMonth, 4, 6)) {
					ctrl.params.trimestre = 2;
				} else if (ctrl.validateTrimestre(desdeMonth, hastaMonth, 7, 9)) {
					ctrl.params.trimestre = 3;
				} else if (ctrl.validateTrimestre(desdeMonth, hastaMonth, 10, 12)) {
					ctrl.params.trimestre = 4;
				}

				if (ctrl.params.trimestre != 5) {
					ctrl.generateAllRight();
				} else {
					ctrl.generateBad();
				}
			} else {
				ctrl.generateBad();
			}
		};

		ctrl.validateTrimestre = function(intMonthDesde, intMonthHasta, being, end){
			return ctrl.dateBetween(intMonthDesde, being, end) && ctrl.dateBetween(intMonthHasta, being, end);
		}

		ctrl.dateBetween = function (month, being, end){
			return month >= being && month <= end;
		};

		ctrl.generateAllRight = function () {
			inicializarLoading();
			ctrl.service.generate(ctrl.params, function (result) {
				$.unblockUI();
				ctrl.result = result;

				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				ctrl.isNullReturned();
				ctrl.resultDialog.dialog("open");
			}, ctrl.postFail.bind(this));
		};
		
		ctrl.generateBad = function () {
			ctrl.task.message = "Las fechas introducidas no se corresponden con un trimestre ¿Quiere continuar con el proceso?";
			ctrl.resultDialogTask.dialog("option", "title", "Confirmación");
			ctrl.resultDialogTask.dialog("open");
		};

		ctrl.isNullReturned = function (){
			if(ctrl.result.ok == undefined) { 
				ctrl.result = {};
				ctrl.result.ok = false;
				ctrl.result.errorMessage = [];
				ctrl.result.errorMessage.push("Error en la ejecución del proceso. Consulte con un administrador.")
			}
		}

		ctrl.formatDates = function (date) {
			var dateArray = date.split("/");
			
			var day = parseInt(dateArray[0]);
			var month = parseInt(dateArray[1]) - 1;
			var year = parseInt(dateArray[2]);		
		
			return new Date(year, month, day, 0,0,0,0);
		};
	};


	InformesMclaganController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

	InformesMclaganController.prototype.constructor = InformesMclaganController;

	sibbac20.controller("InformesMclaganController", ["InformesMclaganService", "$httpParamSerializer", "uiGridConstants",
		"$scope", InformesMclaganController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);