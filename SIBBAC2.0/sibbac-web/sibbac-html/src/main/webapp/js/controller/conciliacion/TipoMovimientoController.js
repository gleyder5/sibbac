sibbac20.controller('TipoMovimientoController', ['$scope', '$document', 'growl', 'ConciliacionTipoMovimientoService', function ($scope, $document, growl, ConciliacionTipoMovimientoService) {

        var oTable = undefined;
        function consultar() {
            inicializarLoading();
            ConciliacionTipoMovimientoService.getTipoMovimiento(onSuccessConsultarRequest, onErrorRequest, {});
        }
        function onSuccessConsultarRequest(json) {
            var datos = undefined;
            if (json.error !== null && json.error !== "") {
                growl.addErrorMessage(json.error);
            } else {
                if (json === undefined || json.resultados === undefined || json.resultados.result_tipo_movimiento === undefined) {
                    datos = {};
                } else {
                    datos = json.resultados.result_tipo_movimiento;
                }
                var difClass = 'centrado';
                oTable.fnClearTable();
                angular.forEach(datos, function (val, key) {
                    var codigo = '';
                    var titulo = '';
                    var efectivo = '';
                    if (datos[key].tipoOperacion !== null && datos[key].tipoOperacion != undefined) {
                        codigo = datos[key].tipoOperacion;
                    } else if (datos[key].codigo !== null && datos[key].codigo != undefined) {
                        codigo = datos[key].codigo;
                    }
                    if (datos[key].afectaTituloContado !== null && datos[key].afectaTituloContado != undefined) {
                        titulo = datos[key].afectaTituloContado;
                    } else if (datos[key].afectacionTitulos !== null && datos[key].afectacionTitulos != undefined) {
                        titulo = datos[key].afectacionTitulos;
                    }
                    if (datos[key].afectaSaldoEfectivo !== null && datos[key].afectaSaldoEfectivo != undefined) {
                        efectivo = datos[key].afectaSaldoEfectivo;
                    } else if (datos[key].afectacionSaldo !== null && datos[key].afectacionSaldo != undefined) {
                        efectivo = datos[key].afectacionSaldo;
                    }
                    oTable.fnAddData([
                        codigo,
                        datos[key].descripcion,
                        titulo,
                        efectivo
                    ]);
                });
                $.unblockUI();
            }
        }
        function onErrorRequest(data, status, headers, config) {
            $.unblockUI();
            growl.addErrorMessage("Ocurrió un error en la petición de datos al servidor. " + data);
        }

        /////////////////////////////////////////////////
        //////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
        function initialize() {
            consultar();

        }
        $document.ready(function () {
            if (oTable === undefined) {
                loadDataTable();
            }
            initialize();

        });
        function loadDataTable() {
            oTable = angular.element("#tblTipoMovimiento").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "language": {
                    "url": "i18n/Spanish.json"
                }
            });
        }

    }]);

