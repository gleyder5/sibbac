'use strict';
sibbac20
    .controller(
                'IdentificadorController',
                [
                 '$scope',
                 '$compile',
                 '$http',
                 '$rootScope',
                 'IsinService',
                 function ($scope, $compile, $http, $rootScope, IsinService) {

                   var oTable = undefined;
                   $(document).ready(
                                     function () {

                                       oTable = $("#tablaIdentificador").dataTable({
                                         "dom" : 'T<"clear">lfrtip',
                                         "tableTools" : {
                                           "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                                         },
                                         "scrollY" : "480px",
                                         "scrollCollapse" : true,
                                         "language" : {
                                           "url" : "i18n/Spanish.json"
                                         }
                                       });
                                       /* Eva:Función para cambiar texto */
                                       jQuery.fn.extend({
                                         toggleText : function (a, b) {
                                           var that = this;
                                           if (that.text() != a && that.text() != b) {
                                             that.text(a);
                                           } else if (that.text() == a) {
                                             that.text(b);
                                           } else if (that.text() == b) {
                                             that.text(a);
                                           }
                                           return this;
                                         }
                                       });
                                       /* FIN Función para cambiar texto */

                                       $('a[href="#release-history"]').toggle(function () {
                                         $('#release-wrapper').animate({
                                           marginTop : '0px'
                                         }, 600, 'linear');
                                       }, function () {
                                         $('#release-wrapper').animate({
                                           marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
                                         }, 600, 'linear');
                                       });

                                       $('#download a').mousedown(function () {
                                         _gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
                                       });

                                       $('.collapser').click(function () {
                                         $(this).parents('.title_section').next().slideToggle();
                                         // $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                                         $(this).toggleClass('active');
                                         $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

                                         return false;
                                       });
                                       $('.collapser_search').click(function () {
                                         $(this).parents('.title_section').next().slideToggle();
                                         $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                                         $(this).toggleClass('active');
                                         // $(this).toggleText( "Mostrar Alta de Grupo Identificador", "Ocultar Alta de
                                         // Grupo Identificador" );
                                         if ($(this).text() == "Ocultar Alta de Grupo Identificador") {
                                           $(this).text("Mostrar Alta de Grupo Identificador");
                                         } else {
                                           $(this).text("Ocultar Alta de Grupo Identificador");
                                         }
                                         return false;
                                       });

                                       /* BUSCADOR */
                                       $('.btn.buscador').click(
                                                                function () {
                                                                  // event.preventDefault();
                                                                  $('.resultados').show();
                                                                  $('.collapser_search').parents('.title_section')
                                                                      .next().slideToggle();
                                                                  $('.collapser_search').toggleClass('active');
                                                                  $('.collapser_search')
                                                                      .toggleText('Mostrar opciones de búsqueda',
                                                                                  'Ocultar opciones de búsqueda');
                                                                  return false;
                                                                });
                                       /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
                                       if ($('.contenedorTabla').length >= 1) {
                                         var anchoBotonera;
                                         $('.contenedorTabla').each(function (i) {
                                           anchoBotonera = $(this).find('table').outerWidth();
                                           $(this).find('.botonera').css('width', anchoBotonera + 'px');
                                           $(this).find('.resumen').css('width', anchoBotonera + 'px');

                                         });
                                       }

                                       cargarTabla();

                                     });

                   function cargarTablaBody () {
                     console.log("entro en el método");
                     $
                         .ajax({
                           type : "POST",
                           dataType : "json",
                           url : "/sibbac20back/rest/service",
                           data : "{\"service\" : \"SIBBACServiceIdentificadores\", \"action\"  : \"getListaIdentificadores\"}",
                           beforeSend : function (x) {
                             if (x && x.overrideMimeType) {
                               x.overrideMimeType("application/json");
                             }
                             // CORS Related
                             x.setRequestHeader("Accept", "application/json");
                             x.setRequestHeader("Content-Type", "application/json");
                             x.setRequestHeader("X-Requested-With", "HandMade");
                             x.setRequestHeader("Access-Control-Allow-Origin", "*");
                             x.setRequestHeader('Access-Control-Allow-Headers',
                                                'Origin, X-Requested-With, Content-Type, Accept')
                           },
                           async : true,
                           success : function (json) {
                             var datos = undefined;
                             if (json === undefined || json.resultados === undefined) {
                               datos = {};
                             } else {
                               datos = json.resultados;
                             }
                             // $('.resultados').hide();
                             oTable.fnClearTable();
                             for ( var k in datos) {
                               var item = datos[k];
                               oTable.fnAddData([ item.indentificadores ]);

                             }

                             // $('.resultados').show();
                             oTable.fnDraw();
                             // var resultados = json.resultados;
                             // var item = null;
                             // var contador = 0;
                             // if (json.resultados !== undefined){
                             // $('.contenedorTabla').append(
                             // "<table class='tablaPrueba ancha'>" +
                             // "<tr>" +
                             // "<th class='taleft'>Descripci&oacute;n</th>" +
                             // "</tr>" +
                             // "</table>"
                             // );
                             // } else {
                             // $('.contenedorTabla').append(
                             // "<span>No existen datos de identificadores</span>"
                             // );
                             // }
                             //
                             //
                             // for ( var k in resultados ) {
                             // item = resultados[ k ];
                             // if (contador%2==0) {
                             // estilo = "even";
                             // } else {
                             // estilo = "odd";
                             // }
                             // // Pintamos la tabla "padre". La cabecera...
                             // $('.tablaPrueba').append(
                             // "<tr class='"+ estilo +"'>" +
                             // "<td class='taleft'>"+item.indentificadores + "</td>" +
                             // "</tr>"
                             //
                             // );
                             // contador++;
                             // }
                             //
                             // // "Footer" de la tabla "padre".
                             // $('.tablaPrueba').append( "</table>" );

                           },
                           error : function (c) {
                             console.log("ERROR: " + c);
                           }

                         });
                     $.unblockUI();
                   }

                   // funcion para cargar la tabla
                   function cargarTabla () {
                     inicializarLoading();
                     cargarTablaBody();
                   }

                   // Evento para dar de alta la regla
                   $scope.crearIdentificador = function () {
                     var descripcion = document.getElementById('textDesc').value;

                     // alert("fin de datos");
                     if (isNaN(descripcion) || descripcion == "") {
                       alert("El campo 'Descripción' no es correcto.");
                       return false;
                     } else {
                       // alert("entro en dar alta");
                       altaIdentificador(descripcion);
                     }
                   }

                   // Funcion para el alta de la regla
                   function altaIdentificador (descripcion) {
                     // alert("entro en el metodo");
                     var filtro = "{\"identificadores\" : \"" + descripcion + "\" }";

                     var jsonData = "{" + "\"service\" : \"SIBBACServiceIdentificadores\", "
                                    + "\"action\"  : \"createIdentificadores\", " + "\"filters\"  : " + filtro + "}";
                     // alert(jsonData);
                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : jsonData,
                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {
                         cargarTabla();
                       },
                       error : function (c) {
                         alert("ERROR: " + c);
                       }

                     });
                   }

                   $scope.refrescarIdentificador = function () {
                     cargarTabla();
                   }

                 } ]);
