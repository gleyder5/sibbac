'use strict';
sibbac20
    .controller(
                'NumeradoresController',
                [
                 '$scope',
                 '$compile',
                 '$http',
                 '$rootScope',
                 'IsinService',
                 function ($scope, $compile, $http, $rootScope, IsinService) {

                   var oTable = undefined;

                   $(document).ready(
                                     function () {

                                       oTable = $("#tablaPrueba").dataTable({
                                         "dom" : 'T<"clear">lfrtip',
                                         "language" : {
                                           "url" : "i18n/Spanish.json"
                                         },
                                         "scrollY" : "480px",
                                         "scrollCollapse" : true,
                                         "tableTools" : {
                                           "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                                         }
                                       });

                                       $('a[href="#release-history"]').toggle(function () {
                                         $('#release-wrapper').animate({
                                           marginTop : '0px'
                                         }, 600, 'linear');
                                       }, function () {
                                         $('#release-wrapper').animate({
                                           marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
                                         }, 600, 'linear');
                                       });

                                       $('#download a').mousedown(function () {
                                         _gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
                                       });

                                       $('.collapser').click(function () {
                                         $(this).parents('.title_section').next().slideToggle();
                                         // $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                                         $(this).toggleClass('active');
                                         $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

                                         return false;
                                       });
                                       $('.collapser_search')
                                           .click(
                                                  function () {
                                                    $(this).parents('.title_section').next().slideToggle();
                                                    $(this).parents('.title_section').next().next('.button_holder')
                                                        .slideToggle();
                                                    $(this).toggleClass('active');
                                                    $(this).toggleText('Mostrar Alta de Numeradores',
                                                                       'Ocultar Alta de Numeradores');
                                                    return false;
                                                  });

                                       /* BUSCADOR */
                                       $('.btn.buscador').click(
                                                                function () {
                                                                  // event.preventDefault();
                                                                  $('.resultados').show();
                                                                  $('.collapser_search').parents('.title_section')
                                                                      .next().slideToggle();
                                                                  $('.collapser_search').toggleClass('active');
                                                                  $('.collapser_search')
                                                                      .toggleText('Mostrar opciones de búsqueda',
                                                                                  'Ocultar opciones de búsqueda');

                                                                  return false;
                                                                });

                                       /*
                                         * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA
                                         */
                                       if ($('.contenedorTabla').length >= 1) {
                                         var anchoBotonera;
                                         $('.contenedorTabla').each(function (i) {
                                           anchoBotonera = $(this).find('table').outerWidth();
                                           $(this).find('.botonera').css('width', anchoBotonera + 'px');
                                           $(this).find('.resumen').css('width', anchoBotonera + 'px');
                                           // $('.resultados').hide();
                                         });
                                       }

                                       cargarTabla();
                                       cargarTipoDocumento();

                                       cargarIdentificador();

                                     });

                   function cargarIdentificador () {
                     $
                         .ajax({
                           type : "POST",
                           dataType : "json",
                           url : "/sibbac20back/rest/service",
                           data : "{\"service\" : \"SIBBACServiceIdentificadores\", \"action\"  : \"getListaIdentificadores\"}",

                           beforeSend : function (x) {
                             if (x && x.overrideMimeType) {
                               x.overrideMimeType("application/json");
                             }
                             // CORS Related
                             x.setRequestHeader("Accept", "application/json");
                             x.setRequestHeader("Content-Type", "application/json");
                             x.setRequestHeader("X-Requested-With", "HandMade");
                             x.setRequestHeader("Access-Control-Allow-Origin", "*");
                             x.setRequestHeader('Access-Control-Allow-Headers',
                                                'Origin, X-Requested-With, Content-Type, Accept')
                           },
                           async : true,
                           success : function (json) {
                             var resultados = json.resultados;
                             var item = null;

                             for ( var k in resultados) {
                               var item = resultados[k];
                               // Rellenamos el combo
                               $('#textIdentificador').append(
                                                              "<option value='" + item.id + "'>"
                                                                  + item.indentificadores + "</option>"

                               );

                             }
                           },
                           error : function (c) {
                             console.log("ERROR: " + c);
                           }
                         });

                   }

                   function cargarTipoDocumento () {
                     $
                         .ajax({
                           type : "POST",
                           dataType : "json",
                           url : "/sibbac20back/rest/service",
                           data : "{\"service\" : \"SIBBACServiceTipoDeDocumento\", \"action\"  : \"getListaTipoDeDocumentos\"}",

                           beforeSend : function (x) {
                             if (x && x.overrideMimeType) {
                               x.overrideMimeType("application/json");
                             }
                             // CORS Related
                             x.setRequestHeader("Accept", "application/json");
                             x.setRequestHeader("Content-Type", "application/json");
                             x.setRequestHeader("X-Requested-With", "HandMade");
                             x.setRequestHeader("Access-Control-Allow-Origin", "*");
                             x.setRequestHeader('Access-Control-Allow-Headers',
                                                'Origin, X-Requested-With, Content-Type, Accept')
                           },
                           async : true,
                           success : function (json) {
                             var resultados = json.resultados;
                             var item = null;

                             for ( var k in resultados) {
                               item = resultados[k];
                               // Rellenamos el combo
                               $('#textTipoDocumento').append(
                                                              "<option value='" + item.id + "'>" + item.descripcion
                                                                  + "</option>"

                               );

                             }
                           },
                           error : function (c) {
                             console.log("ERROR: " + c);
                           }
                         });

                   }

                   $scope.crearNumerador = function () {

                     event.preventDefault();

                     var lista = document.getElementById("textTipoDocumento");
                     var indiceSeleccionado = lista.selectedIndex;
                     var opcionSeleccionada = lista.options[indiceSeleccionado];
                     var textoSeleccionado = opcionSeleccionada.text;
                     var valorSeleccionado = opcionSeleccionada.value;

                     var lista2 = document.getElementById("textIdentificador");
                     var indiceSeleccionado2 = lista2.selectedIndex;
                     var opcionSeleccionada2 = lista2.options[indiceSeleccionado2];
                     var textoSeleccionado2 = opcionSeleccionada2.text;
                     var valorSeleccionado2 = opcionSeleccionada2.value;

                     var inicio = document.getElementById('textInicio').value;
                     var fin = document.getElementById('textFin').value;

                     var siguiente = document.getElementById('textSiguiente').value;

                     if (textInicio == "" || valorSeleccionado == "0" || valorSeleccionado2 == "0") {
                       alert("Complete los campos necesarios");
                       return false;
                     }

                     var filtro = "{\"tipoDeDocumentoId\" : \"" + valorSeleccionado + "\"," + "\"inicio\" : \""
                                  + inicio + "\"," + "\"fin\" : \"" + fin + "\"," + "\"identificadoresId\" : \""
                                  + valorSeleccionado2 + "\"," + "\"siguiente\" : \"" + siguiente + "\" }";

                     var jsonData = "{" + "\"service\" : \"SIBBACServiceNumeradores\", "
                                    + "\"action\"  : \"createNumeradores\", " + "\"filters\"  : " + filtro + "}";

                     // alert(jsonData);
                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : jsonData,
                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {
                         if (json.resultados == null) {
                           alert(json.error);
                           return false;
                         }
                         cargarTabla();
                       },
                       error : function (c) {

                       }

                     });
                   }

                   function cargarTabla () {
                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : "{\"service\" : \"SIBBACServiceNumeradores\", \"action\"  : \"getNumeradores\"}",
                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {

                         var resultados = json.resultados;

                         var item = null;
                         var estilo = null;
                         var contador = 0;
                         var nParametrizaciones = 0;
                         oTable.fnClearTable();
                         for ( var k in resultados) {
                           // En "k" tengo lo de la izquierda, por ejemplo: "Id_Regla_1".
                           // En "item" tengo todo lo de la derecha.
                           // En "item.parametrizacion.length" tenemos el numero de
                           // "hijos".
                           item = resultados[k];

                           if (contador % 2 == 0) {
                             estilo = "even";
                           } else {
                             estilo = "odd";
                           }

                           oTable.fnAddData([ item.tipodedocumento, item.inicio, item.fin, item.identificadores,
                                             item.siguiente ]);

                           /*
                             * $('#tablaPrueba').append( "<tr class='"+ estilo +"'>" + "<td class='taleft'>"+item.tipodedocumento + "</td>" + "<td class='taleft'>"+item.inicio + "</td>" + "<td class='taleft'>"+item.fin + "</td>" + "<td class='taleft'>"+item.identificadores + "</td>" + "<td class='taleft'>"+item.siguiente + "</td>" + "</tr>" );
                             */
                           contador++;
                         }

                       }
                     });

                   }

                 } ]);
