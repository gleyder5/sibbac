'use strict';
sibbac20
    .controller(
                'PlantillasInformesController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'PlantillasInformesService',
                 '$compile',
                 'SecurityService',
                 '$location',
                 'Logger',
                 function ($scope, $document, growl, PlantillasInformesService, $compile, SecurityService, $location,
                           Logger) {

                   inicializarLoading();

                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   /***************************************************************************************************
                     * ** INICIALIZACION DE DATOS ***
                     **************************************************************************************************/
                   $scope.showingFilter = true;

                   // Variables y listas general
                   $scope.checked = true;
                   $scope.filtro = {};
                   $scope.listPlantillas = [];
                   $scope.listIdiomas = [];
                   $scope.listCamposDetalle = [];
                   $scope.listTotalizar = [];
                   $scope.listAlias = [];
                   $scope.listIsin = [];
                   $scope.activeAlert = false;
                   $scope.mensajeAlert = "";
                   $scope.borrarCampoAgrup = false;

                   $scope.listPlantillasInformes = [];
                   $scope.plantillasInformesSeleccionadas = [];
                   $scope.plantillasInformesSeleccionadasBorrar = [];

                   $scope.modal = {
                     titulo : "",
                     showCrear : false,
                     showModificar : false,
                     showAlias : false,
                     showIsin : false
                   };

                   $scope.ocultarAlert = function () {
                     if (!$scope.borrarCampoAgrup) {
                       $scope.mensajeAlert = "";
                     } else {
                       $scope.borrarCampoAgrup = false;
                     }
                   };

                   // Reset objeto plantilla
                   $scope.resetPlantilla = function () {
                     $scope.plantilla = {
                       idInforme : "",
                       nbdescription : "",
                       nbtxtlibre : "",
                       nbidioma : "",
                       totalizar : "",
                       camposDetalle : "",
                       listaCamposDetalle : [],
                       listaCamposAgrupacion : [],
                       listaCamposOrdenacion : [],
                       listaCamposCabecera : [],
                       listaCamposSeparacion : [],
                       listaAlias : [],
                       listaCdisin : [],
                       nbalias : "",
                       cdisin : "",
                       selected : false
                     };
                     $scope.activeAlert = false;
                     $scope.mensajeAlert = "";
                     $scope.checked = true;
                   };

                   $scope.resetPlantilla();

                   $scope.form = {};
                   $scope.resetForms = function () {
                     $scope.form = {
                       idioma : "",
                       totalizar : "",
                       campoDetalleFilter : "",
                       camposDetalleFilter : "",
                       campoDetallePlantilla : "",
                       camposDetallePlantilla : "",
                       campoAgrupacionPlantilla : "",
                       camposAgrupacionPlantilla : "",
                       campoOrdenacionPlantilla : "",
                       camposOrdenacionPlantilla : "",
                       campoSeparacionPlantilla : "",
                       camposSeparacionPlantilla : "",
                       campoCabeceraPlantilla : "",
                       camposCabeceraPlantilla : "",
                       aliasInput : "",
                       aliasSelect : "",
                       isinInput : "",
                       isinSelect : ""
                     }
                   };

                   $scope.resetForms();

                   $scope.resetButtonFilters = function () {
                     $scope.btnsFilter = {
                       textPlantilla : "IGUAL",
                       btnPlantilla : "=",
                       colorPlantilla : {
                         'color' : 'green'
                       },
                       textIdioma : "IGUAL",
                       btnIdioma : "=",
                       colorIdioma : {
                         'color' : 'green'
                       },
                       textCamposDetalle : "INCLUIDOS",
                       btnCamposDetalle : "=",
                       colorCamposDetalle : {
                         'color' : 'green'
                       },
                       textTotalizar : "IGUAL",
                       btnTotalizar : "=",
                       colorTotalizar : {
                         'color' : 'green'
                       },
                       textAlias : "IGUAL",
                       btnAlias : "=",
                       colorAlias : {
                         'color' : 'green'
                       },
                       textIsin : "IGUAL",
                       btnIsin : "=",
                       colorIsin : {
                         'color' : 'green'
                       }
                     };
                   };

                   $scope.resetButtonFilters();

                   // Reset del filtro
                   $scope.resetFiltro = function () {
                     $scope.filtro = {
                       plantilla : "",
                       notPlantilla : false,
                       idioma : "",
                       notIdioma : false,
                       camposDetalle : [],
                       notCamposDetalle : false,
                       totalizar : "",
                       notTotalizar : false,
                       alias : "",
                       notAlias : false,
                       isin : "",
                       notIsin : false
                     };
                     $scope.plantillaFiltro = {
                       key : "",
                       value : ""
                     };
                     $scope.totalizarPlantilla = {
                       key : "",
                       value : ""
                     };
                     $scope.idiomaFiltro = {
                       key : "",
                       value : ""
                     };
                     $scope.totalizarFiltro = {
                       key : "",
                       value : ""
                     };
                     $scope.aliasFiltro = "";
                     $scope.isinFiltro = "";
                     $scope.resetButtonFilters();
                   };

                   $scope.resetFiltro();

                   /***************************************************************************************************
                     * ** DEFINICION TABLA ***
                     **************************************************************************************************/

                   $scope.oTable = $("#datosPlantillasInformes").dataTable({
                     "dom" : 'T<"clear">lfrtip',
                     "tableTools" : {
                       "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                       "aButtons" : [ "copy", {
                         "sExtends" : "csv",
                         "sFileName" : "Listado_Plantillas_Informes_" + hoy + ".csv",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6 ]
                       }, {
                         "sExtends" : "xls",
                         "sFileName" : "Listado_Plantillas_Informes_" + hoy + ".xls",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6 ]
                       }, {
                         "sExtends" : "pdf",
                         "sPdfOrientation" : "landscape",
                         "sTitle" : " ",
                         "sPdfSize" : "A3",
                         "sPdfMessage" : "Listado Final Holders",
                         "sFileName" : "Listado_Plantillas_Informes_" + hoy + ".pdf",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6 ]
                       }, "print" ]
                     },
                     "aoColumns" : [ {
                       sClass : "centrar",
                       bSortable : false,
                       width : "7%"
                     }, {
                       sClass : "centrar",
                       width : "18%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "25%"
                     }, {
                       sClass : "centrar",
                       width : "18%"
                     }, {
                       sClass : "centrar",
                       width : "12%"
                     } ],
                     "fnCreatedRow" : function (nRow, aData, iDataIndex) {

                       $compile(nRow)($scope);
                     },

                     "scrollY" : "480px",
                     "scrollX" : "100%",
                     "scrollCollapse" : true,
                     "language" : {
                       "url" : "i18n/Spanish.json"
                     }
                   });

                   $scope.seleccionarElemento = function (row) {
                     if ($scope.listPlantillasInformes[row].selected) {
                       $scope.listPlantillasInformes[row].selected = false;
                       for (var i = 0; i < $scope.plantillasInformesSeleccionadas.length; i++) {
                         if ($scope.plantillasInformesSeleccionadas[i].idInforme === $scope.listPlantillasInformes[row].idInforme) {
                           $scope.plantillasInformesSeleccionadas.splice(i, 1);
                           $scope.plantillasInformesSeleccionadasBorrar.splice(i, 1);
                         }
                       }
                     } else {
                       $scope.listPlantillasInformes[row].selected = true;
                       $scope.plantillasInformesSeleccionadas.push($scope.listPlantillasInformes[row]);
                       $scope.plantillasInformesSeleccionadasBorrar.push($scope.listPlantillasInformes[row].idInforme);

                     }
                   };

                   /***************************************************************************************************
                     * ** FUNCIONES GENERALES ***
                     **************************************************************************************************/

                   var pupulateAutocomplete = function (input, availableTags) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {

                         var option = {
                           key : ui.item.key,
                           value : ui.item.value
                         };

                         switch (input) {
                           case "#filtro_campoDetalle":
                             var existe = false;
                             angular.forEach($scope.filtro.camposDetalle, function (campo) {
                               if (campo.value === option.value) {
                                 existe = true;
                               }
                             });
                             $scope.form.campoDetalleFilter = "";
                             if (!existe) {
                               $scope.filtro.camposDetalle.push(option);
                             }
                             break;
                           case "#filtro_alias":
                             $scope.filtro.alias = option.key;
                             $scope.aliasFiltro = option.value;
                             break;
                           case "#filtro_isin":
                             $scope.filtro.isin = option.key;
                             $scope.isinFiltro = option.value;
                             break;
                           case "#plantilla_isin":
                             var existe = false;
                             for (var i = 0; i < $scope.plantilla.listaCdisin.length; i++) {
                               if (option.value === $scope.plantilla.listaCdisin[i].value) {
                                 existe = true;
                               }
                             }
                             $scope.form.isinInput = "";
                             if (!existe) {
                               $scope.plantilla.listaCdisin.push(option);
                             }
                             break;
                           case "#plantilla_alias":
                             var existe = false;
                             for (var i = 0; i < $scope.plantilla.listaAlias.length; i++) {
                               if (option.value === $scope.plantilla.listaAlias[i].value) {
                                 existe = true;
                               }
                             }
                             $scope.form.aliasInput = "";
                             if (!existe) {
                               $scope.plantilla.listaAlias.push(option);
                             }
                             break;
                           default:
                             break;
                         }

                         $scope.safeApply();

                         return false;
                       }
                     });
                   };

                   $scope.invertirValores = function (option) {

                     switch (option) {

                       case "plantilla":
                         if (!$scope.filtro.notPlantilla) {
                           $scope.filtro.notPlantilla = true;
                           $scope.btnsFilter.textPlantilla = "DISTINTO";
                           $scope.btnsFilter.btnPlantilla = "<>";
                           $scope.btnsFilter.colorPlantilla = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notPlantilla = false;
                           $scope.btnsFilter.textPlantilla = "IGUAL";
                           $scope.btnsFilter.btnPlantilla = "=";
                           $scope.btnsFilter.colorPlantilla = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "idioma":
                         if (!$scope.filtro.notIdioma) {
                           $scope.filtro.notIdioma = true;
                           $scope.btnsFilter.textIdioma = "DISTINTO";
                           $scope.btnsFilter.btnIdioma = "<>";
                           $scope.btnsFilter.colorIdioma = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notIdioma = false;
                           $scope.btnsFilter.textIdioma = "IGUAL";
                           $scope.btnsFilter.btnIdioma = "=";
                           $scope.btnsFilter.colorIdioma = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "camposDetalle":
                         if (!$scope.filtro.notCamposDetalle) {
                           $scope.filtro.notCamposDetalle = true;
                           $scope.btnsFilter.textCamposDetalle = "NO INCLUIDOS";
                           $scope.btnsFilter.btnCamposDetalle = "<>";
                           $scope.btnsFilter.colorCamposDetalle = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notCamposDetalle = false;
                           $scope.btnsFilter.textCamposDetalle = "INCLUIDOS";
                           $scope.btnsFilter.btnCamposDetalle = "=";
                           $scope.btnsFilter.colorCamposDetalle = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "totalizar":
                         if (!$scope.filtro.notTotalizar) {
                           $scope.filtro.notTotalizar = true;
                           $scope.btnsFilter.textTotalizar = "DISTINTO";
                           $scope.btnsFilter.btnTotalizar = "<>";
                           $scope.btnsFilter.colorTotalizar = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notTotalizar = false;
                           $scope.btnsFilter.textTotalizar = "IGUAL";
                           $scope.btnsFilter.btnTotalizar = "=";
                           $scope.btnsFilter.colorTotalizar = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "alias":
                         if (!$scope.filtro.notAlias) {
                           $scope.filtro.notAlias = true;
                           $scope.btnsFilter.textAlias = "DISTINTO";
                           $scope.btnsFilter.btnAlias = "<>";
                           $scope.btnsFilter.colorAlias = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notAlias = false;
                           $scope.btnsFilter.textAlias = "IGUAL";
                           $scope.btnsFilter.btnAlias = "=";
                           $scope.btnsFilter.colorAlias = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "isin":
                         if (!$scope.filtro.notIsin) {
                           $scope.filtro.notIsin = true;
                           $scope.btnsFilter.textIsin = "DISTINTO";
                           $scope.btnsFilter.btnIsin = "<>";
                           $scope.btnsFilter.colorIsin = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notIsin = false;
                           $scope.btnsFilter.textIsin = "IGUAL";
                           $scope.btnsFilter.btnIsin = "=";
                           $scope.btnsFilter.colorIsin = {
                             'color' : 'green'
                           };
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   /***************************************************************************************************
                     * ** CARGA DE DATOS INICIAL ***
                     **************************************************************************************************/

                   // Carga de los combos Totalizar e Idiomas
                   $scope.cargarEstaticos = function () {
                     PlantillasInformesService.cargaSelectsEstaticos(function (data) {
                       $scope.listTotalizar = data.resultados["listaTotalizar"];
                       $scope.listIdiomas = data.resultados["listaIdiomas"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga de combos estaticos.", 1);
                     });
                   }

                   // Carga del combo Plantillas
                   $scope.cargaPlantillas = function () {
                     PlantillasInformesService.cargaPlantillas(function (data) {
                       $scope.listPlantillas = data.resultados["listaPlantillas"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo de plantillas.", 1);
                     });
                   }

                   // Carga de los combos Isin
                   $scope.cargarIsin = function () {
                     PlantillasInformesService.cargaIsin(function (data) {
                       $scope.listIsin = data.resultados["listaIsin"];
                       pupulateAutocomplete("#filtro_isin", $scope.listIsin);
                       pupulateAutocomplete("#plantilla_isin", $scope.listIsin);

                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga de los combos ISIN.", 1);
                     });
                   }

                   // Carga de los combos Alias
                   $scope.cargarAlias = function () {
                     PlantillasInformesService.cargaAlias(function (data) {
                       $scope.listAlias = data.resultados["listaAlias"];
                       pupulateAutocomplete("#filtro_alias", $scope.listAlias);
                       pupulateAutocomplete("#plantilla_alias", $scope.listAlias);
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga de los combos Alias.", 1);
                     });
                   }

                   // Carga del combo campo detalle
                   $scope.cargarCamposDetalle = function () {
                     PlantillasInformesService.cargaCamposDetalle(function (data) {
                       $scope.listCamposDetalle = data.resultados["listInformesCampos"];
                       pupulateAutocomplete("#filtro_campoDetalle", $scope.listCamposDetalle);
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Campos Detalle.", 1);
                     });
                   }

                   $scope.consultar = function() {
                	   $scope.showingFilter = false;
                	   $scope.getPlantillasInformes();
                   }

                   $scope.getPlantillasInformes = function () {
                     inicializarLoading();
                     $scope.plantillasInformesSeleccionadasBorrar = [];
                     $scope.plantillasInformesSeleccionadas = [];
                     $scope.filtro.plantilla = $scope.plantillaFiltro.key;
                     $scope.filtro.idioma = $scope.idiomaFiltro.key;
                     $scope.filtro.totalizar = $scope.totalizarFiltro.key;

                     PlantillasInformesService
                         .cargaPlantillasInformes(
                                                  function (data) {

                                                    $scope.listPlantillasInformes = data.resultados["listaPlantillasInformes"];

                                                    // borra el contenido del body de la tabla
                                                    var tbl = $("#datosPlantillasInformes > tbody");
                                                    $(tbl).html("");
                                                    $scope.oTable.fnClearTable();

                                                    for (var i = 0; i < $scope.listPlantillasInformes.length; i++) {

                                                      var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listPlantillasInformes['
                                                                  + i
                                                                  + '].selected" ng-click="seleccionarElemento('
                                                                  + i + ');"/>';
                                                      var totalizarTexto = "Si";
                                                      if ($scope.listPlantillasInformes[i].totalizar === "N") {
                                                        totalizarTexto = "No";
                                                      }

                                                      var camposDetalle = $scope.listPlantillasInformes[i].camposDetalle;
                                                      if ($scope.listPlantillasInformes[i].camposDetalle.length > 30) {
                                                        camposDetalle = camposDetalle.substring(0, 30);
                                                        camposDetalle += "...";
                                                      }

                                                      var plantillaList = [
                                                                           check,
                                                                           $scope.listPlantillasInformes[i].nbdescription,
                                                                           $scope.listPlantillasInformes[i].nbidioma,
                                                                           totalizarTexto, camposDetalle,
                                                                           $scope.listPlantillasInformes[i].nbalias,
                                                                           $scope.listPlantillasInformes[i].cdisin ];

                                                      $scope.oTable.fnAddData(plantillaList, false);

                                                    }
                                                    $scope.oTable.fnDraw();
                                                    $.unblockUI();
                                                    if ($scope.listIdiomas.length == 0) {
                                                      $scope.cargarEstaticos();
                                                      $scope.cargaPlantillas();
                                                      $scope.cargarCamposDetalle();
                                                      $scope.cargarAlias();
                                                      $scope.cargarIsin();
                                                    }
                                                  },
                                                  function (error) {
                                                    $.unblockUI();
                                                    fErrorTxt(
                                                              "Se produjo un error en la carga del listado de plantillas informes.",
                                                              1);
                                                  }, $scope.filtro);
                   };

                   $scope.getPlantillasInformes();

                   /***************************************************************************************************
                     * ** ACCIONES MODALES ***
                     **************************************************************************************************/

                   // Abrir modal creacion informes
                   $scope.abrirCrearPlantillaInforme = function () {
                     $scope.modal.showIsin = false;
                     $scope.modal.showAlias = false;
                     $scope.modal.showCrear = true;
                     $scope.modal.showModificar = false;
                     $scope.modal.titulo = "Crear Plantilla Informe";
                     $scope.resetPlantilla();
                     $scope.form.idioma = $scope.listIdiomas[0];
                     $scope.form.totalizar = $scope.listTotalizar[1];
                     angular.element('#formularios').modal({
                       backdrop : 'static'
                     });
                     $(".modal-backdrop").remove();
                   };

                   // Abrir modal modificar informes
                   $scope.abrirModificarPlantillaInforme = function () {

                     $scope.modal.showIsin = false;
                     $scope.modal.showAlias = false;

                     if ($scope.plantillasInformesSeleccionadas.length > 1
                         || $scope.plantillasInformesSeleccionadas.length == 0) {
                       if ($scope.plantillasInformesSeleccionadas.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de plantillas de informe", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de plantillas de informe.", 2)
                       }
                     } else {
                       $scope.resetPlantilla();

                       $scope.plantilla = angular.copy($scope.plantillasInformesSeleccionadas[0]);

                       for (var i = 0; i < $scope.listIdiomas.length; i++) {
                         if ($scope.listIdiomas[i].key === $scope.plantilla.nbidioma) {
                           $scope.form.idioma = $scope.listIdiomas[i];
                         }
                       }

                       for (var j = 0; j < $scope.listTotalizar.length; j++) {
                         if ($scope.listTotalizar[j].key === $scope.plantilla.totalizar) {
                           $scope.form.totalizar = $scope.listTotalizar[j];
                         }
                       }

                       for (var i = 0; i < $scope.plantilla.listaCamposDetalle.length; i++) {
                         var value = $scope.plantilla.listaCamposDetalle[i].value;
                         if (value.indexOf("ALIAS") > -1) {
                           $scope.modal.showAlias = true;
                         }
                         if (value.indexOf("ISIN") > -1) {
                           $scope.modal.showIsin = true;
                         }
                       }

                       if ($scope.plantilla.listaCamposAgrupacion.length > 0) {
                         $scope.checked = true;
                       }

                       $scope.modal.showCrear = false;
                       $scope.modal.showModificar = true;
                       $scope.modal.titulo = "Modificar Plantilla Informe";

                       angular.element('#formularios').modal({
                         backdrop : 'static'
                       });
                       $(".modal-backdrop").remove();
                     }
                   };

                   $scope.procesarCampoDetalle = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposDetalle, function (campo) {
                       if (campo.value === $scope.form.campoDetallePlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       var value = $scope.form.campoDetallePlantilla.value;
                       if (value.indexOf("ISIN") > -1) {
                         $scope.modal.showIsin = true;
                       }
                       if (value.indexOf("ALIAS") > -1) {
                         $scope.modal.showAlias = true;
                       }
                       $scope.plantilla.listaCamposDetalle.push($scope.form.campoDetallePlantilla);
                       if ($scope.form.campoDetallePlantilla.description === "N" && $scope.checked) {
                         $scope.plantilla.listaCamposAgrupacion.push($scope.form.campoDetallePlantilla);
                       }
                     }
                     $scope.form.campoDetallePlantilla = "";
                   };

                   $scope.procesarCampoAgrup = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposAgrupacion, function (campo) {
                       if (campo.value === $scope.form.campoAgrupacionPlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.plantilla.listaCamposAgrupacion.push($scope.form.campoAgrupacionPlantilla);
                     }
                     $scope.form.campoAgrupacionPlantilla = "";
                   };

                   $scope.procesarCampoOrden = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposOrdenacion, function (campo) {
                       if (campo.value === $scope.form.campoOrdenacionPlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.plantilla.listaCamposOrdenacion.push($scope.form.campoOrdenacionPlantilla);
                     }
                     $scope.form.campoOrdenacionPlantilla = "";
                   };

                   $scope.procesarCampoCabecera = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposCabecera, function (campo) {
                       if (campo.value === $scope.form.campoCabeceraPlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.plantilla.listaCamposCabecera.push($scope.form.campoCabeceraPlantilla);
                     }
                     $scope.form.campoCabeceraPlantilla = "";
                   };

                   $scope.procesarCampoSep = function () {
                     var existe = false;
                     angular.forEach($scope.plantilla.listaCamposSeparacion, function (campo) {
                       if (campo.value === $scope.form.campoSeparacionPlantilla.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.plantilla.listaCamposSeparacion.push($scope.form.campoSeparacionPlantilla);
                     }
                     $scope.form.campoSeparacionPlantilla = "";
                   };

                   $scope.subirElementoTabla = function (tabla) {
                     var elemento = {};
                     switch (tabla) {
                       case "detalle":
                         for (var i = 0; i < $scope.plantilla.listaCamposDetalle.length; i++) {
                           if ($scope.plantilla.listaCamposDetalle[i].value === $scope.form.camposDetallePlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposDetalle[i];
                             $scope.plantilla.listaCamposDetalle[i] = $scope.plantilla.listaCamposDetalle[i - 1];
                             $scope.plantilla.listaCamposDetalle[i - 1] = elemento;
                           }
                         }
                         break;
                       case "agrupacion":
                         for (var i = 0; i < $scope.plantilla.listaCamposAgrupacion.length; i++) {
                           if ($scope.plantilla.listaCamposAgrupacion[i].value === $scope.form.camposAgrupacionPlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposAgrupacion[i];
                             $scope.plantilla.listaCamposAgrupacion[i] = $scope.plantilla.listaCamposAgrupacion[i - 1];
                             $scope.plantilla.listaCamposAgrupacion[i - 1] = elemento;
                           }
                         }
                         break;
                       case "ordenacion":
                         for (var i = 0; i < $scope.plantilla.listaCamposOrdenacion.length; i++) {
                           if ($scope.plantilla.listaCamposOrdenacion[i].value === $scope.form.camposOrdenacionPlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposOrdenacion[i];
                             $scope.plantilla.listaCamposOrdenacion[i] = $scope.plantilla.listaCamposOrdenacion[i - 1];
                             $scope.plantilla.listaCamposOrdenacion[i - 1] = elemento;
                           }
                         }
                         break;
                       case "separacion":
                         for (var i = 0; i < $scope.plantilla.listaCamposSeparacion.length; i++) {
                           if ($scope.plantilla.listaCamposSeparacion[i].value === $scope.form.camposSeparacionPlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposSeparacion[i];
                             $scope.plantilla.listaCamposSeparacion[i] = $scope.plantilla.listaCamposSeparacion[i - 1];
                             $scope.plantilla.listaCamposSeparacion[i - 1] = elemento;
                           }
                         }
                         break;
                       case "cabecera":
                         for (var i = 0; i < $scope.plantilla.listaCamposCabecera.length; i++) {
                           if ($scope.plantilla.listaCamposCabecera[i].value === $scope.form.camposCabeceraPlantilla.value
                               && i > 0) {
                             elemento = $scope.plantilla.listaCamposCabecera[i];
                             $scope.plantilla.listaCamposCabecera[i] = $scope.plantilla.listaCamposCabecera[i - 1];
                             $scope.plantilla.listaCamposCabecera[i - 1] = elemento;
                           }
                         }
                         break;
                       case "alias":
                         for (var i = 0; i < $scope.plantilla.listaAlias.length; i++) {
                           if ($scope.plantilla.listaAlias[i].value === $scope.form.aliasSelect.value && i > 0) {
                             elemento = $scope.plantilla.listaAlias[i];
                             $scope.plantilla.listaAlias[i] = $scope.plantilla.listaAlias[i - 1];
                             $scope.plantilla.listaAlias[i - 1] = elemento;
                           }
                         }
                         break;
                       case "isin":
                         for (var i = 0; i < $scope.plantilla.listaCdisin.length; i++) {
                           if ($scope.plantilla.listaCdisin[i].value === $scope.form.isinSelect.value && i > 0) {
                             elemento = $scope.plantilla.listaCdisin[i];
                             $scope.plantilla.listaCdisin[i] = $scope.plantilla.listaCdisin[i - 1];
                             $scope.plantilla.listaCdisin[i - 1] = elemento;
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.bajarElementoTabla = function (tabla) {
                     var elemento = {};
                     switch (tabla) {
                       case "detalle":
                         var listLength = $scope.plantilla.listaCamposDetalle.length;
                         for (var i = $scope.plantilla.listaCamposDetalle.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposDetalle[i].value === $scope.form.camposDetallePlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposDetalle[i];
                             $scope.plantilla.listaCamposDetalle[i] = $scope.plantilla.listaCamposDetalle[i + 1];
                             $scope.plantilla.listaCamposDetalle[i + 1] = elemento;
                           }
                         }

                         break;
                       case "agrupacion":
                         var listLength = $scope.plantilla.listaCamposAgrupacion.length;
                         for (var i = $scope.plantilla.listaCamposAgrupacion.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposAgrupacion[i].value === $scope.form.camposAgrupacionPlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposAgrupacion[i];
                             $scope.plantilla.listaCamposAgrupacion[i] = $scope.plantilla.listaCamposAgrupacion[i + 1];
                             $scope.plantilla.listaCamposAgrupacion[i + 1] = elemento;
                           }
                         }
                         break;
                       case "ordenacion":
                         var listLength = $scope.plantilla.listaCamposOrdenacion.length;
                         for (var i = $scope.plantilla.listaCamposOrdenacion.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposOrdenacion[i].value === $scope.form.camposOrdenacionPlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposOrdenacion[i];
                             $scope.plantilla.listaCamposOrdenacion[i] = $scope.plantilla.listaCamposOrdenacion[i + 1];
                             $scope.plantilla.listaCamposOrdenacion[i + 1] = elemento;
                           }
                         }
                         break;
                       case "separacion":
                         var listLength = $scope.plantilla.listaCamposSeparacion.length;
                         for (var i = $scope.plantilla.listaCamposSeparacion.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposSeparacion[i].value === $scope.form.camposSeparacionPlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposSeparacion[i];
                             $scope.plantilla.listaCamposSeparacion[i] = $scope.plantilla.listaCamposSeparacion[i + 1];
                             $scope.plantilla.listaCamposSeparacion[i + 1] = elemento;
                           }
                         }
                         break;
                       case "cabecera":
                         var listLength = $scope.plantilla.listaCamposCabecera.length;
                         for (var i = $scope.plantilla.listaCamposCabecera.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCamposCabecera[i].value === $scope.form.camposCabeceraPlantilla.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCamposCabecera[i];
                             $scope.plantilla.listaCamposCabecera[i] = $scope.plantilla.listaCamposCabecera[i + 1];
                             $scope.plantilla.listaCamposCabecera[i + 1] = elemento;
                           }
                         }
                         break;
                       case "alias":
                         var listLength = $scope.plantilla.listaAlias.length;
                         for (var i = $scope.plantilla.listaAlias.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaAlias[i].value === $scope.form.aliasSelect.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaAlias[i];
                             $scope.plantilla.listaAlias[i] = $scope.plantilla.listaAlias[i + 1];
                             $scope.plantilla.listaAlias[i + 1] = elemento;
                           }
                         }
                         break;
                       case "isin":
                         var listLength = $scope.plantilla.listaCdisin.length;
                         for (var i = $scope.plantilla.listaCdisin.length - 1; i >= 0; i--) {
                           if ($scope.plantilla.listaCdisin[i].value === $scope.form.isinSelect.value
                               && i < (listLength - 1)) {
                             elemento = $scope.plantilla.listaCdisin[i];
                             $scope.plantilla.listaCdisin[i] = $scope.plantilla.listaCdisin[i + 1];
                             $scope.plantilla.listaCdisin[i + 1] = elemento;
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.eliminarElementoTabla = function (tabla) {
                     switch (tabla) {
                       case "detalle":

                         $scope.form.camposAgrupacionPlantilla = {
                           value : $scope.form.camposDetallePlantilla.value
                         };
                         $scope.eliminarElementoTabla("agrupacion");
                         $scope.form.camposOrdenacionPlantilla = {
                           value : $scope.form.camposDetallePlantilla.value
                         };
                         $scope.eliminarElementoTabla("ordenacion");

                         for (var i = 0; i < $scope.plantilla.listaCamposDetalle.length; i++) {
                           if ($scope.plantilla.listaCamposDetalle[i].value === $scope.form.camposDetallePlantilla.value) {
                             $scope.plantilla.listaCamposDetalle.splice(i, 1);
                           }
                         }

                         var contCamposAlias = 0;
                         var contCamposIsin = 0;
                         for (var i = 0; i < $scope.plantilla.listaCamposDetalle.length; i++) {
                           var value = $scope.plantilla.listaCamposDetalle[i].value;
                           if (value.indexOf("ISIN") > -1) {
                             contCamposIsin++;
                           }
                           if (value.indexOf("ALIAS") > -1) {
                             contCamposAlias++;
                           }
                         }
                         if (contCamposAlias == 0) {
                           $scope.modal.showAlias = false;
                           $scope.plantilla.listaAlias = [];
                         }
                         if (contCamposIsin == 0) {
                           $scope.modal.showIsin = false;
                           $scope.plantilla.listaCdisin = [];
                         }

                         break;
                       case "agrupacion":
                         if ($scope.form.camposAgrupacionPlantilla.description !== "N") {
                           for (var i = 0; i < $scope.plantilla.listaCamposAgrupacion.length; i++) {
                             if ($scope.plantilla.listaCamposAgrupacion[i].value === $scope.form.camposAgrupacionPlantilla.value) {
                               $scope.plantilla.listaCamposAgrupacion.splice(i, 1);
                             }
                           }
                         } else {
                           $scope.mensajeAlert = "El campo que desea eliminar, no totaliza. No se puede eliminar de los campos de agrupacion";
                           $scope.borrarCampoAgrup = true;
                         }
                         break;
                       case "ordenacion":
                         $scope.form.camposSeparacionPlantilla = {
                           value : $scope.form.camposOrdenacionPlantilla.value
                         };
                         $scope.eliminarElementoTabla("separacion");
                         for (var i = 0; i < $scope.plantilla.listaCamposOrdenacion.length; i++) {
                           if ($scope.plantilla.listaCamposOrdenacion[i].value === $scope.form.camposOrdenacionPlantilla.value) {
                             $scope.plantilla.listaCamposOrdenacion.splice(i, 1);
                           }
                         }
                         break;
                       case "separacion":
                         $scope.form.camposCabeceraPlantilla = {
                           value : $scope.form.camposSeparacionPlantilla.value
                         };
                         $scope.eliminarElementoTabla("cabecera");
                         for (var i = 0; i < $scope.plantilla.listaCamposSeparacion.length; i++) {
                           if ($scope.plantilla.listaCamposSeparacion[i].value === $scope.form.camposSeparacionPlantilla.value) {
                             $scope.plantilla.listaCamposSeparacion.splice(i, 1);
                           }
                         }
                         break;
                       case "cabecera":
                         for (var i = 0; i < $scope.plantilla.listaCamposCabecera.length; i++) {
                           if ($scope.plantilla.listaCamposCabecera[i].value === $scope.form.camposCabeceraPlantilla.value) {
                             $scope.plantilla.listaCamposCabecera.splice(i, 1);
                           }
                         }
                         break;
                       case "alias":
                         for (var i = 0; i < $scope.plantilla.listaAlias.length; i++) {
                           if ($scope.plantilla.listaAlias[i].value === $scope.form.aliasSelect.value) {
                             $scope.plantilla.listaAlias.splice(i, 1);
                           }
                         }
                         break;
                       case "isin":
                         for (var i = 0; i < $scope.plantilla.listaCdisin.length; i++) {
                           if ($scope.plantilla.listaCdisin[i].value === $scope.form.isinSelect.value) {
                             $scope.plantilla.listaCdisin.splice(i, 1);
                           }
                         }
                         break;
                       case "detalleFiltro":
                         for (var i = 0; i < $scope.filtro.camposDetalle.length; i++) {
                           if ($scope.filtro.camposDetalle[i].value === $scope.form.camposDetalleFilter.value) {
                             $scope.filtro.camposDetalle.splice(i, 1);
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.vaciarTabla = function (tabla) {
                     switch (tabla) {
                       case "detalle":
                         $scope.plantilla.listaCamposDetalle = [];
                         $scope.plantilla.listaCamposAgrupacion = [];
                         $scope.plantilla.listaCamposOrdenacion = [];
                         $scope.plantilla.listaCamposSeparacion = [];
                         $scope.plantilla.listaCamposCabecera = [];
                         $scope.plantilla.listaAlias = [];
                         $scope.plantilla.listaCdisin = [];
                         $scope.modal.showAlias = false;
                         $scope.modal.showIsin = false;
                         break;
                       case "agrupacion":
                         if (!$scope.checked) {
                           $scope.plantilla.listaCamposAgrupacion = [];
                         } else {
                           for (var i = 0; i < $scope.plantilla.listaCamposDetalle.length; i++) {
                             if ($scope.plantilla.listaCamposDetalle[i].description === "N") {
                               $scope.plantilla.listaCamposAgrupacion.push($scope.plantilla.listaCamposDetalle[i]);
                             }
                           }
                         }
                         break;
                       case "ordenacion":
                         $scope.plantilla.listaCamposOrdenacion = [];
                         $scope.plantilla.listaCamposSeparacion = [];
                         break;
                       case "separacion":
                         $scope.plantilla.listaCamposSeparacion = [];
                         $scope.plantilla.listaCamposCabecera = [];
                         break;
                       case "cabecera":
                         $scope.plantilla.listaCamposCabecera = [];
                         break;
                       case "alias":
                         $scope.plantilla.listaAlias = [];
                         break;
                       case "isin":
                         $scope.plantilla.listaCdisin = [];
                         break;
                       case "detalleFiltro":
                         $scope.filtro.camposDetalle = [];
                         break;
                       default:
                         break;
                     }
                   };

                   /***************************************************************************************************
                     * ** ACCIONES CRUD / MODALES ***
                     **************************************************************************************************/

                   $scope.srcImage = "images/warning.png";
                   $scope.crearPlantillaInforme = function (salir) {
                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       $scope.plantilla.totalizar = $scope.form.totalizar.key;
                       $scope.plantilla.nbidioma = $scope.form.idioma.key;
                       PlantillasInformesService.crearPlantilla(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           if (salir) {
                             $scope.getPlantillasInformes();
                             angular.element('#formularios').modal("hide");
                             $scope.activeAlert = false;
                             fErrorTxt('La plantilla de informe ' + $scope.plantilla.nbdescription
                                       + ' creada correctamente', 3);
                           } else {
                             $scope.plantilla.nbdescription = "";
                             $scope.srcImage = "images/accept.png";
                             $scope.mensajeAlert = "Plantilla de informe  " + $scope.plantilla.nbdescription
                                                   + " creada correctamente";
                           }
                         }
                       }, function (e) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al crear plantilla informe.", 1);
                       }, $scope.plantilla);
                     }
                   };

                   $scope.modificarPlantillaInforme = function (modificar) {

                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       if (modificar) {
                         $scope.plantilla.totalizar = $scope.form.totalizar.key;
                         $scope.plantilla.nbidioma = $scope.form.idioma.key;

                         PlantillasInformesService
                             .modificarPlantilla(
                                                 function (data) {
                                                   if (data.resultados.status === 'KO') {
                                                     $.unblockUI();
                                                     $scope.srcImage = "images/warning.png";
                                                     $scope.mensajeAlert = data.error;
                                                   } else {
                                                     $scope.getPlantillasInformes();
                                                     angular.element('#formularios').modal("hide");
                                                     fErrorTxt('Plantilla de informe ' + $scope.plantilla.nbdescription
                                                               + ' modificada correctamente', 3);
                                                   }
                                                 },
                                                 function (error) {
                                                   $.unblockUI();
                                                   angular.element('#formularios').modal("hide");
                                                   fErrorTxt(
                                                             "Ocurrió un error durante la petición de datos al modificar la plantilla de informe.",
                                                             1);
                                                 }, $scope.plantilla);
                       } else {
                         $scope.plantilla.totalizar = $scope.form.totalizar.key;
                         $scope.plantilla.nbidioma = $scope.form.idioma.key;
                         $scope.plantilla.idInforme = "";
                         PlantillasInformesService.crearPlantilla(function (data) {
                           if (data.resultados.status === 'KO') {
                             $.unblockUI();
                             $scope.srcImage = "images/warning.png";
                             $scope.mensajeAlert = data.error;
                           } else {
                             $scope.plantilla.idInforme = data.resultados.idInforme;
                             $scope.srcImage = "images/accept.png";
                             $scope.mensajeAlert = "Plantilla de informe  " + $scope.plantilla.nbdescription
                                                   + " creada correctamente";

                           }
                         }, function (e) {
                           $.unblockUI();
                           angular.element('#formularios').modal("hide");
                           fErrorTxt("Ocurrió un error durante la petición de datos al crear plantilla informe.", 1);
                         }, $scope.plantilla);
                       }
                     }
                   };

                   $scope.validacionFormulario = function () {
                     if (!$scope.plantilla.nbdescription || $scope.plantilla.listaCamposDetalle.length == 0
                         || $scope.plantilla.listaCamposOrdenacion.length == 0) {
                       return true;
                     } else {
                       return false;
                     }
                   }

                   $scope.borrarPlantillaInforme = function () {
                     if ($scope.plantillasInformesSeleccionadasBorrar.length > 0) {
                       if ($scope.plantillasInformesSeleccionadasBorrar.length == 1) {
                         angular.element("#dialog-confirm")
                             .html("¿Desea eliminar la plantilla de informe seleccionada?");
                       } else {
                         angular.element("#dialog-confirm")
                             .html(
                                   "¿Desea eliminar las " + $scope.plantillasInformesSeleccionadasBorrar.length
                                       + " plantillas de informe seleccionadas?");
                       }

                       // Define the Dialog and its properties.
                       angular
                           .element("#dialog-confirm")
                           .dialog(
                                   {
                                     resizable : false,
                                     modal : true,
                                     title : "Mensaje de Confirmación",
                                     height : 150,
                                     width : 360,
                                     buttons : {
                                       " Sí " : function () {
                                         PlantillasInformesService
                                             .eliminarPlantillas(
                                                                 function (data) {
                                                                   $scope.getPlantillasInformes();
                                                                 },
                                                                 function (error) {
                                                                   $.unblockUI();
                                                                   fErrorTxt(
                                                                             "Ocurrió un error durante la petición de datos al eliminar plantillas de informes.",
                                                                             1);
                                                                 },
                                                                 {
                                                                   listaPlantillasBorrar : $scope.plantillasInformesSeleccionadasBorrar
                                                                 });

                                         $(this).dialog('close');
                                       },
                                       " No " : function () {
                                         $(this).dialog('close');
                                       }
                                     }
                                   });
                       $('.ui-dialog-titlebar-close').remove();
                     } else {
                       fErrorTxt("Debe seleccionar al menos un elemento de la tabla de plantillas de informe.", 2)
                     }
                   };

                   /***************************************************************************************************
                     * ** ACCIONES TABLA ***
                     **************************************************************************************************/
                   $scope.seleccionarTodos = function () {
                     // Se inicializa los elementos que ya tuviera la lista de elementos a borrar.
                     $scope.plantillasInformesSeleccionadasBorrar = [];
                     for (var i = 0; i < $scope.listPlantillasInformes.length; i++) {
                       $scope.listPlantillasInformes[i].selected = true;
                       $scope.plantillasInformesSeleccionadasBorrar.push($scope.listPlantillasInformes[i].idInforme);
                     }
                     $scope.plantillasInformesSeleccionadas = angular.copy($scope.listPlantillasInformes);
                   };

                   $scope.deseleccionarTodos = function () {
                     for (var i = 0; i < $scope.listPlantillasInformes.length; i++) {
                       $scope.listPlantillasInformes[i].selected = false;
                     }
                     $scope.plantillasInformesSeleccionadas = [];
                     $scope.plantillasInformesSeleccionadasBorrar = [];
                   };

                 } ]);
