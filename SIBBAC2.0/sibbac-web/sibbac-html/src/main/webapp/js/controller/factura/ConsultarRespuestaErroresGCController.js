'use strict';
sibbac20
	.controller(
        'ConsultarRespuestaErroresGCController',
        [
         '$scope',
         '$document',
         'growl',
         'ConsultarRespuestaErroresGCService',
         'CatalogoService',
         '$compile',
         'SecurityService',
         '$rootScope',
         'moment',
         function ($scope, $document, growl, ConsultarRespuestaErroresGCService, CatalogoService, $compile, SecurityService, $rootScope, moment) {

        	 console.log(' +++++ ConsultarRespuestaErroresGCController +++++ ');

             /***************************************************************************************************
              * ** INICIALIZACION DE DATOS ***
              **************************************************************************************************/
        	 var hoy = new Date();
             var dd = hoy.getDate();
             var mm = hoy.getMonth() + 1;
             var yyyy = hoy.getFullYear();
             hoy = yyyy + "_" + mm + "_" + dd;

        	 $scope.catalogos = [];
        	 var oTableResErrGC = null;
        	 var oTableErroresFichero = null;
        	 $scope.filtro = {};
        	 $scope.followSearch = "";// contiene las migas de los filtros de búsqueda
             $scope.showingFilters = true;

        	 /***************************************************************************************************
              * ** CARGA DE DATOS INICIAL ***
              **************************************************************************************************/
        	// Carga del combo Estados Fichero.
            $scope.cargarCatalogos = function () {
            	ConsultarRespuestaErroresGCService.cargaCatalogos(function (data) {
            		$scope.catalogos = data.resultados["catalogos"];
            		populateAutocomplete("#fichero", $scope.catalogos.listaNombreFicheros, function (option) {
                        $scope.filtro.compania = $.trim(option.value);
                    });
            	}, function (error) {
            		fErrorTxt("Se produjo un error en la carga del combo de estados de fichero.", 1);
            	});
            }

            // Reset del filtro
            $scope.resetFiltro = function () {
              $scope.estadoFicheroFiltro = {
                key : "",
                value : ""
              };
              $scope.filtro = {
            	nombreFichero : "",
            	estadoFichero : "",
            	tipoFichero : "",
            	fechaDesde: "",
            	fechaHasta: ""
              };
              $scope.ficherosSeleccionados = [];
              $scope.modalErroresFichero = {};
              $scope.sentidoFiltro = [];
              for ( var key in $scope.filtro) {
                $scope.sentidoFiltro[key] = "IGUAL";
              }
              $scope.ficherosSeleccionadosBorrar = [];
            };

            // Setea el filtro que se pasará al back
            $scope.getFiltro = function () {
            	var filtro = {};
            	for ( var key in $scope.filtro) {
            		if ($scope.filtro[key] != null && $scope.filtro[key] != "") {
	        			if ($scope.sentidoFiltro[key] == "IGUAL") {
	        				filtro[key] = $scope.filtro[key];
	        			} else {
	        				filtro[key + "Not"] = $scope.filtro[key];
	                        filtro[key] = null;
	                    }
            		}
            	}
            	return filtro;
            };

            var labelFiltro = {
            		nombreFichero : "Nombre de Fichero",
            		estadoFichero : "Estado",
            		tipoFichero : "Tipo de Fichero",
            		fechaDesde : "Fecha desde",
            		fechaHasta : "Fecha hasta"
            };


            $scope.getValorDescriptivo = function (filtro, valor) {

              return valor;

            }

            $scope.getFollowSearch = function () {

                var texto = "";

                for ( var key in $scope.filtro) {
                  if ($scope.filtro[key] != null && $scope.filtro[key] != "") {

	            	  var operador = "";
	                  if ($scope.sentidoFiltro[key] == "IGUAL") {
	                    operador = "=";
	                  } else {
	                    operador = "<>";
	                  }

                    var valorDescriptivo = $scope.getValorDescriptivo(key, $scope.filtro[key]);
                    if (valorDescriptivo == "") {
                      valorDescriptivo = $scope.filtro[key];
                    }

                    if (texto == "") {
                      texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo;
                    } else {
                      texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo;
                    }

                  }
                }
                $scope.followSearch = texto;
                return texto;

              }

            // Carga del listado principal
            $scope.busqueda = function () {
                // Se oculta el panel de búsqueda y se muestra el detalle
                $scope.showingFilter = false;
                //.getFollowSearch();
            	// Se muestra el loader.
                inicializarLoading();
                ConsultarRespuestaErroresGCService.buscarRespErroresGC(function (data) {
                	// Se oculta el loader.
                	$.unblockUI();
                	$scope.pintarTabla(data);
                }, function (error) {
                	// Se oculta el loader.
                	$.unblockUI();
                	fErrorTxt("Se produjo un error en la carga del listado de respuestas de error.", 1);
                }, $scope.getFiltro());
            }

            // Borrado de tabla
            $scope.borrarTabla = function () {
            	var tbl = $("#datosRespErrsGC > tbody");
                $(tbl).html("");
                oTableResErrGC.fnClearTable();
            };

            // Borrado de tabla Ver Errores
            $scope.borrarTablaVerErrores = function () {
            	var tbl = $("#datosErroresFichero > tbody");
                $(tbl).html("");
                oTableErroresFichero.fnClearTable();
            };

            // Seleccion de un elemento de la tabla
            $scope.seleccionarElemento = function (row) {
                if ($scope.listaRespErrGC[row] != null && $scope.listaRespErrGC[row].selected) {
                  $scope.listaRespErrGC[row].selected = false;
                  for (var i = 0; i < $scope.ficherosSeleccionados.length; i++) {
                    if ($scope.ficherosSeleccionados[i].id === $scope.listaRespErrGC[row].id) {
                      $scope.ficherosSeleccionados.splice(i, 1);
                      $scope.ficherosSeleccionadosBorrar.splice(i, 1);
                    }
                  }
                } else {
                  $scope.listaRespErrGC[row].selected = true;
                  $scope.ficherosSeleccionados.push($scope.listaRespErrGC[row]);
                  $scope.ficherosSeleccionadosBorrar.push($scope.listaRespErrGC[row].id);
                }
              };


            // Se pinta el datatable resultado de la busqueda.
            $scope.pintarTabla = function (data) {

                // borra el contenido del body de la tabla
            	if (oTableResErrGC != null) {
                    $scope.borrarTabla();
            	}

            	var list = [];

            	// Se pasa resultado del servicio al scope
            	$scope.listaRespErrGC = data.resultados["listaBusquedaRespuestaErrGCDTO"];

            	// Se inicializa tabla
            	inicializarTabla();

            	// Se popula tabla
            	if ($scope.listaRespErrGC.length > 0) {
	            	for (var i = 0; i < $scope.listaRespErrGC.length; i++) {

	            		var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listaRespErrGC['
	                        + i + '].selected" ng-click="seleccionarElemento(' + i + ');"/>';

	            		list[i] = [check,
	            		           $scope.listaRespErrGC[i].nombreFichero,
	            		           $scope.listaRespErrGC[i].tipoFichero,
	            		           moment($scope.listaRespErrGC[i].fechaEnvioRecepcion).format('DD/MM/YYYY'),
	            		           $scope.listaRespErrGC[i].estado,
	            		           $scope.listaRespErrGC[i].asociacion
	            		];
	            	}
	            	// Se pintan datos de la tabla
	            	oTableResErrGC.fnAddData(list, false);
	            	oTableResErrGC.fnDraw();
            	}

            }

            // Se pinta el datatable resultado de la busqueda.
            $scope.pintarTablaVerErrores = function (data) {
            	// borra el contenido del body de la tabla
            	if (oTableErroresFichero != null) {
                    $scope.borrarTablaVerErrores();
            	}

            	var listErrores = [];

            	// Se pasa resultado del servicio al scope
            	$scope.modalErroresFichero.listaErroresFichero = data.resultados["listaErroresFicheroModalDTO"];

            	// Se inicializa tabla
            	inicializarTablaErrsFichero();

            	// Se popula tabla
            	if ($scope.modalErroresFichero.listaErroresFichero.length > 0) {
            		for (var i = 0; i < $scope.modalErroresFichero.listaErroresFichero.length; i++) {
            			listErrores[i] = [$scope.modalErroresFichero.listaErroresFichero[i].numeroFacturaEmisor,
            			               moment($scope.modalErroresFichero.listaErroresFichero[i].fechaExpedicionFactura).format('DD/MM/YYYY'),
            			               $scope.modalErroresFichero.listaErroresFichero[i].codigoRetorno,
            			               $scope.modalErroresFichero.listaErroresFichero[i].tipoError,
            			               $scope.modalErroresFichero.listaErroresFichero[i].claseError,
            			               $scope.modalErroresFichero.listaErroresFichero[i].descripcion
            			];
            		}
            		// Se pintan datos de la tabla
            		oTableErroresFichero.fnAddData(listErrores, false);
            		oTableErroresFichero.fnDraw();
            	}
            }

            /***************************************************************************************************
             * ** DEFINICION TABLA **
             **************************************************************************************************/
            var inicializarTabla = function () {
            	if (oTableResErrGC == null) {
                	oTableResErrGC = $("#datosRespErrsGC").dataTable({
                        "dom" : 'T<"clear">lfrtip',
                        "tableTools" : {
                        	"sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                            "aButtons" : [ "copy", {
                              "sExtends" : "csv",
                              "sFileName" : "Listado_Errores_Gestor_Corporativo_" + hoy + ".csv",
                              "mColumns" : [ 1, 2, 3, 4, 5 ]
                            }, {
                              "sExtends" : "xls",
                              "sFileName" : "Listado_Errores_Gestor_Corporativo_" + hoy + ".xls",
                              "mColumns" : [ 1, 2, 3, 4, 5 ]
                            }, {
                              "sExtends" : "pdf",
                              "sPdfOrientation" : "landscape",
                              "sTitle" : " ",
                              "sPdfSize" : "A3",
                              "sPdfMessage" : "Histórico Facturas Comunicadas SII",
                              "sFileName" : "Listado_Errores_Gestor_Corporativo_" + hoy + ".pdf",
                              "mColumns" : [ 1, 2, 3, 4, 5 ]
                            }, "print" ]
                        },
                        "aoColumns" : [ {
                          sClass : "centrar",
                          bSortable : false,
                          width : "50px"
                        }, {
                          width : "300px"
                        }, {
                          width : "200px"
                        }, {
                          width : "200px"
                        }, {
                          width : "200px"
                        }, {
                          width : "300px"
                        }],
                        "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                          $compile(nRow)($scope);
                        },
                        "scrollY" : "480px",
                        "scrollX" : "100%",
                        "scrollCollapse" : true,
                        "language" : {
                          "url" : "i18n/Spanish.json"
                        }
                      });
                	  console.log(' +++++ oTableResErrGC : ' + oTableResErrGC + ' +++++ ');
            	}
            }

            // Tabla que va a estar en la modal de errores de fichero.
            var inicializarTablaErrsFichero = function () {
            	if (oTableErroresFichero == null) {
            		oTableErroresFichero = $("#datosErroresFichero").dataTable({
            			"dom" : '<"clear">lrtip',
                        "aoColumns" : [ {
                          width : "100px"
                        }, {
                          width : "100px"
                        }, {
                          width : "100px"
                        }, {
                          width : "100px"
                        }, {
                          width : "100px"
                        }, {
                          width : "100px"
                        }],
                        "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                          $compile(nRow)($scope);
                        },
                        "scrollY" : "300px",
                        "scrollX" : "100%",
                        "scrollCollapse" : true,
                        "language" : {
                          "url" : "i18n/Spanish.json"
                        }
                      });
                	  console.log(' +++++ oTableErroresFichero : ' + oTableErroresFichero + ' +++++ ');
            	}
            }

            $scope.resetFiltro();
            $scope.cargarCatalogos();
            $scope.filtro.estadoFichero = $scope.estadoFicheroFiltro.key;

            $scope.safeApply = function (fn) {
              var phase = this.$root.$$phase;
              if (phase === '$apply' || phase === '$digest') {
                if (fn && (typeof (fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };


            var populateAutocomplete = function (input, availableTags, callback) {

              $(input).autocomplete({
                minLength : 0,
                source : availableTags,
                focus : function (event, ui) {
                  return false;
                },
                select : function (event, ui) {
                  var option = {
                    key : ui.item.key,
                    value : ui.item.value
                  };
                  callback(option);
                  $scope.safeApply();
                  return false;
                }
              });

            };


            /***************************************************************************************************
             * ** CARGA DE DATOS INICIALES
             **************************************************************************************************/

            $document.ready(function () {
            	inicializarTabla();
            });

	         /***************************************************************************************************
	          * ** ACCIONES CRUD / MODALES
	          **************************************************************************************************/

	         // Abrir modal Ver Errores
	         $scope.abrirModalVerErrores = function () {

	        	 $scope.errores = [];
	        	// Solo se permite abrir modal para respuestas con error
	        	 if ($scope.ficherosSeleccionados.length == 1
	        	 	 && !isFicheroRespuestaConErrores($scope.ficherosSeleccionados[0])) {
	        	    fErrorTxt("Funcionalidad aplicable solo a ficheros de respuesta con error.", 2);
	        	 } else if ($scope.ficherosSeleccionados.length > 1 || $scope.ficherosSeleccionados.length == 0) {
                   if ($scope.ficherosSeleccionados.length == 0) {
                     fErrorTxt("Debe seleccionar al menos un elemento de la tabla.", 2)
                   } else {
                     fErrorTxt("Debe seleccionar solo un elemento de la tabla.", 2)
                   }
                 } else {
                	 inicializarLoading();
                	 ConsultarRespuestaErroresGCService.verErroresFichero(
                			 function (data) {
                				 if (data.resultados == null) {
                            		 $.unblockUI();
                                     fErrorTxt(
                                    		 "Ocurrió un error durante la petición de datos al consultar el fichero.",
                                             1);
                                     return;
                                 }
                                 $(".modal-backdrop").remove();

                                 // Se oculta el loader.
                             	 $.unblockUI();
                             	 $scope.pintarTablaVerErrores(data);

//                               $scope.modalErroresFichero.listaErroresFichero = data.resultados;
                                 $scope.modalErroresFichero.titulo = "Errores Fichero";
                                 angular.element('#formularioErroresFichero').modal({
                                	 backdrop : 'static'
                                 });
                			 },
                             function (error) {
                				 $.unblockUI();
                                 angular.element('#formularioErroresFichero').modal("hide");
                                 fErrorTxt(
                                           "Ocurrió un error durante la petición de datos al consultar el fichero.",
                                           1);
                             }, {
                                 "nombreFichero" : $scope.ficherosSeleccionados[0].nombreFichero
                             }
                	 )
                 }
	        }

	         // Hace la llamada a la descarga del fichero
	         $scope.descargarFichero = function () {

	        	 $scope.errores = [];
	        	// Solo se permite abrir modal para respuestas con error
                if ($scope.ficherosSeleccionados.length > 1 || $scope.ficherosSeleccionados.length == 0) {
                   if ($scope.ficherosSeleccionados.length == 0) {
                     fErrorTxt("Debe seleccionar al menos un elemento de la tabla.", 2)
                   } else {
                     fErrorTxt("Debe seleccionar solo un elemento de la tabla.", 2)
                   }
                 } else {
                	 inicializarLoading();
    	        	 ConsultarRespuestaErroresGCService.descargarFichero(function (data) {
    	        		 if(data.error) {
    	        			 fErrorTxt("Error en la descarga del fichero");
    	        		 }
    	        		 else {
      	                   var decodedString = atob([ data.resultados.fileContent ]);
    	                   $scope.descargar(data.resultados.fileName, decodedString);
    	        		 }
    		             $.unblockUI();
    	             }, function (error) {
    	               $.unblockUI();
    	               fErrorTxt("Error de comunicación al descargar el fichero");
    	             }, { id : $scope.ficherosSeleccionados[0].id });
                 }

	         };

	         // Descarga un archivo excel con el contenido pasado como parametro
	         $scope.descargar = function (fileName, fileBytes) {

	           var byteNumbers = new Array(fileBytes.length);
	           for (var i = 0; i < fileBytes.length; i++) {
	             byteNumbers[i] = fileBytes.charCodeAt(i);
	           }

	           var byteArray = new Uint8Array(byteNumbers);

	           var blob = new Blob([ byteArray ], {
	             type : 'txt'
	           });

	           var nav = navigator.userAgent.toLowerCase();

	           if (navigator.msSaveBlob) {
	             navigator.msSaveBlob(blob, fileName);
	           } else {
	             var blobUrl = URL.createObjectURL(blob);
	             var link = document.createElement('a');
	             link.href = blobUrl = URL.createObjectURL(blob);
	             link.download = fileName;
	             document.body.appendChild(link);
	             link.click();
	             document.body.removeChild(link);
	           }

	         }

         } ]);
