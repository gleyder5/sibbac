'use strict';
sibbac20.controller('CestaOrdenesFidessaController',
                    [
                     '$scope',
                     '$document',
                     'growl',
                     'CestaOrdenesFidessaService',
                     '$compile',
                     function ($scope, $document, growl, CestaOrdenesFidessaService, $compile) {

                       var hoy = new Date();
                       var dd = hoy.getDate();
                       var mm = hoy.getMonth() + 1;
                       var yyyy = hoy.getFullYear();
                       hoy = yyyy + "_" + mm + "_" + dd;

                       $scope.listFicheros = [];

                       $scope.filtro = {
                         fichero : ""
                       };

                       $scope.ficheroSelect = {
                         key : "",
                         value : ""
                       };

                       $scope.cargaFicheros = function () {
                         // Carga del combo Ficheros
                         CestaOrdenesFidessaService.obtenerFicheros(function (data) {
                           $scope.listFicheros = data.resultados["listaFicheros"];
                         }, function (error) {
                           fErrorTxt("Se produjo un error en la carga del combo de ficheros disponibles.", 1);
                         });
                       }

                       $scope.ficheroFaseDos = {};
                       // Se ocultan las fases
                       $('#cargarVentanaFaseUno').css("display", "none");
                       $('#cargarVentanaFaseDos').css("display", "none");

                       $scope.EjecutarFaseUno = function () {
                         // Carga del combo Ficheros

                         CestaOrdenesFidessaService.obtenerFicheros(function (data) {
                           $scope.listFicheros = data.resultados["listaFicheros"];

                           $('#cargarVentanaFaseDos').css("display", "none");
                           if ($scope.listFicheros.length == 0) {
                             fErrorTxt("No hay ningún fichero con el que iniciar la fase.", 1);
                           } else {
                             $('#cargarVentanaFaseUno').css("display", "block");

                           }

                         }, function (error) {
                           fErrorTxt("Se produjo un error en la carga del combo de ficheros disponibles.", 1);
                         });

                       }

                       $scope.EjecutarFaseDos = function () {
                         $('#cargarVentanaFaseUno').css("display", "none");
                         $('#cargarVentanaFaseDos').css("display", "block");
                       }

                       $scope.ContinuarFaseUno = function () {

                         if ($scope.ficheroSelect.key != "") {
                           $scope.filtro.fichero = $scope.ficheroSelect.key;

                           CestaOrdenesFidessaService.obtenerNumeroDesgloses(function (data) {
                             if (data.resultados.status === 'KO') {
                               $.unblockUI();
                               fErrorTxt(data.error, 1);
                             } else {
                               // Se muestra el numero de desgloses obtenidos y se le da la
                               // opción de continuar o
                               // cancelar.
                               var numDesgloses = data.resultados.numeroDesgloses;

                               // Define el dialogo para continuar.
                               angular.element("#dialog-confirm").dialog({
                                 resizable : false,
                                 modal : true,
                                 title : "Confirmación Continuar Fase Uno ",
                                 height : 150,
                                 width : 360,
                                 buttons : {
                                   "Continuar Fase1" : function () {
                                     $(this).dialog('close');
                                     inicializarLoading();
                                     CestaOrdenesFidessaService.ejecutarFaseUno(function (data) {
                                       if (data.resultados.status === 'KO') {
                                         $.unblockUI();
                                         $scope.filtro.fichero = "";
                                         $("#cargarVentanaFaseUno").hide();
                                         fErrorTxt(data.error, 1);

                                       } else {
                                         $scope.ficheroSelect = {
                                           key : "",
                                           value : ""
                                         };

                                         $scope.filtro.fichero = "";
                                         $("#cargarVentanaFaseUno").hide();
                                         $.unblockUI();
                                         fErrorTxt("Fase Ejecutada correctamente", 3);

                                       }
                                     }, function (error) {
                                       $.unblockUI();
                                       fErrorTxt("La ejecucion del proceso es muy larga y ha producido un timeout", 1);
                                       $(this).dialog('close');
                                     }, $scope.filtro);

                                   },
                                   "Cancelar Fase1" : function () {
                                     $(this).dialog('close');
                                   }
                                 }
                               });

                               angular.element("#dialog-confirm").html(
                                                                       " Se van a procesar " + numDesgloses
                                                                           + " Desgloses,  ¿Desea continuar?");

                             }
                           }, function (error) {
                             $.unblockUI();
                             fErrorTxt("Ocurrió un error al procesar el fichero y obtener el numero de desgloses.", 1);
                           }, $scope.filtro);

                         } else {
                           fErrorTxt("No ha seleccionado ningun fichero.", 1);
                         }

                       }

                       $scope.ContinuarFaseDos = function () {
                         // Se comprueba que ha seleccionado un fichero.
                         var file = $scope.files[0];
                         if (file) {
                           // el tipo de fichero tiene que ser .DAT
                           var extension = file.name.substring(file.name.length - 4).toUpperCase();

                           if (extension != "XLSX") {
                             fErrorTxt("Solo se tratan ficheros con extension .XLSX", 1);
                             return;
                           }

                           // Si todo ok, paso el fichero y si inicia la fase 2
                           // create reader
                           var reader = new FileReader();
                           var btoaTxt;
                           reader.readAsArrayBuffer(file);
                           // Cuando tenemos cargado en memoria todo el fichero, se lo enviamos al servicio rest
                           reader.onload = function () {
                             var binary = "";
                             var bytes = new Uint8Array(reader.result);
                             var length = bytes.byteLength;

                             for (var i = 0; i < length; i++) {
                               binary += String.fromCharCode(bytes[i]);
                             }

                             btoaTxt = btoa(binary);
                             // var datos = readerEvt.target.result;
                             var filtro = {
                               file : btoaTxt,
                               filename : file.name
                             };
                             inicializarLoading();
                             CestaOrdenesFidessaService.ejecutarFaseDos(function (data) {
                               // $scope.listFicheros = data.resultados["listaFicheros"];
                               if (data.resultados.status === 'KO') {
                                 $.unblockUI();
                                 fErrorTxt(data.error, 1);

                               } else {
                                 $.unblockUI();
                                 fErrorTxt("Fase dos ejecutada correctamente", 3);
                                 $('#ficheroFaseDos').val('');
                                 $("#cargarVentanaFaseDos").hide();
                               }
                             }, function (error) {
                               $.unblockUI();
                               fErrorTxt("La ejecucion del proceso es muy larga y ha producido un timeout", 1);
                             }, filtro);
                           }

                         } else {
                           fErrorTxt("No ha seleccionado ningún fichero.", 1);
                         }

                       }

                       $('#ficheroFaseDos').change(function (event) {
                         event.preventDefault();
                         $scope.files = event.target.files;
                       });

                       /** Procesa un error durante la petición de dartos al servidor. */
                       function onErrorRequest (data) {
                         $.unblockUI();
                         // growl.addErrorMessage("Ocurrió un error durante la petición de datos.");
                         fErrorTxt("Ocurrió un error durante la petición de datos.", 1);
                       } // onErrorRequest

                     } ]);
