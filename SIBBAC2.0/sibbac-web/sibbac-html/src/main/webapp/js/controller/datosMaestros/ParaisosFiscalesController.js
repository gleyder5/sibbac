(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var ParaisosFiscalesController = function (service, httpParamSerializer, uiGridConstants, $scope) {
		// Sub controlador DynamicFiltersController
		GENERIC_CONTROLLERS.DynamicFiltersController.call(this, service, httpParamSerializer, uiGridConstants);

		this.queries.$promise.then(data => {
			this.selectedQuery = data[0];
			this.selectQuery();
		});

		// Opciones del modal (480px ancho)
		var fatDialogOpt = { width: 480 }, self = this;
		this.scope = $scope;
		// Resgistramos los modales por la id, y le asignamos una función para el boton con el nombre asignado (Guardar, Modificar, etc)
		this.createModal = this.registryModal("modalSave", "Crear...", {
			Guardar: this.save.bind(this)
		}, fatDialogOpt);
		this.modifyModal = this.registryModal("modalEdit", "Modificar...", {
			Modificar: this.edit.bind(this)
		}, fatDialogOpt);
		this.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					self.resultDialog.dialog("close");
					if (self.result.ok) {
						self.executeQuery();
					}

				}
			}
		});
	};

	ParaisosFiscalesController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

	ParaisosFiscalesController.prototype.constructor = ParaisosFiscalesController;

	ParaisosFiscalesController.prototype.ctrlExecuteQuery = function () {
		this.executeQuery();
	}

	// Función que se encarga de levantar los modales con una id que identifica su tipo 
	ParaisosFiscalesController.prototype.openModal = function (id) {
		switch (id) {
			case "save":
				this.params = {
					nbpais: "",
					cdisoalf2: "",
					cdisoalf3: "",
					cdisonum: "",
					cdhacienda: "",
					cdcodkgr: "",
					fecInicioParFisc: "",
					fecFinParFisc: "",
					cdTipoIdMifid: "",
					indicadorDMO: ""
				};
				this.createModal.dialog("option", "title", "Crear");
				this.createModal.dialog("open");

				break;
			case "edit":
				if (this.selectedRows().length > 0) {
					this.value = this.selectedRows()[0];

					if (this.value.indicador_DMO === "SI") {
						this.value.indicador_DMO = "1"
					} else if (this.value.indicador_DMO === "NO") {
						this.value.indicador_DMO = "0"
					}
					// Igualamos los valores a los parámetros que recibiremos en el modal
					this.params = {
						nbpais: (this.value.NBPAIS != undefined) ? this.value.NBPAIS.trim() : null,
						cdisoalf2: this.value.CDISOALF2.trim(),
						cdisoalf3: this.value.CDISOALF3.trim(),
						cdisonum: this.value.CDISONUM.trim(),
						cdhacienda: (this.value.CDHACIENDA != undefined) ? this.value.CDHACIENDA.trim() : null,
						cdcodkgr: (this.value.CDCODKGR != undefined) ? this.value.CDCODKGR.trim() : null,
						cdTipoIdMifid: (this.value.CD_TIPO_ID_MIFID != undefined) ? this.value.CD_TIPO_ID_MIFID.trim() : null,
						indicadorDMO: (this.value.INDICADOR_DMO == "SI"),
						fecInicioParFisc: new Date(this.value.FECHA_INICIO_PAR_FISC),
						fecFinParFisc: new Date(this.value.FECHA_FIN_PAR_FISC),
						cdestado: this.value.CDESTADO
					}
					this.modifyModal.dialog("option", "title", "Modificar");
					this.modifyModal.dialog("open");
				}

				break;
		}
	}

	// Función encargada de ejecutar el servicio guardar
	ParaisosFiscalesController.prototype.save = function () {
		console.log(this.scope.modalForm.cdisoalf2.$valid);
		console.log(this.scope.modalForm.cdisoalf3.$valid);
		if (this.params.nbpais == "" || this.params.cdisoalf2 == "" || this.params.cdisoalf3 == "" || this.params.cdisonum == "") {
			fErrorTxt("Los campos Nombre de pais, ISO Alfanumérico(2), ISO Alfanumérico(3) y ISO Numérico son obligatorios", 1);
		}
		else if (this.scope.modalForm.$valid) {
			var self = this;

			for (const param in this.params) {
				this.params[param] = (this.params[param] === "") ? undefined : this.params[param];
			}
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPOST(this.params, {}, function (result) {
				$.unblockUI();
				self.result = result;
				// Ejecuta el modal de respuesta al revicio
				self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				self.resultDialog.dialog("open");
				if (self.result.ok == true) {
					self.createModal.dialog("close");
				}

			}, this.postFail.bind(this));
		}
		else {
			fErrorTxt("El formato de los datos introducidos no son correctos", 1);
		}
	};

	ParaisosFiscalesController.prototype.edit = function () {
		if (this.params.nbpais == "" || this.params.cdisoalf2 == "" || this.params.cdisoalf3 == "" || this.params.cdisonum == "") {
			fErrorTxt("Los campos Nombre de pais, ISO Alfanumérico(2), ISO Alfanumérico(3) y ISO Numérico son obligatorios", 1);
		}
		// El form tiene que ser válido
		else if (this.scope.modalForm.$valid) {
			var self = this;

			this.modifyModal.dialog("close");
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPUT(this.params, {}, function (result) {
				$.unblockUI();
				self.result = result;
				// Ejecuta el modal de respuesta al revicio
				self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				self.resultDialog.dialog("open");

			}, this.postFail.bind(this));
		}
		else {
			fErrorTxt("El formato de los datos introducidos no son correctos", 1);
		}
	};

	ParaisosFiscalesController.prototype.delete = function () {
		// El form tiene que ser válido
		if (this.selectedRows().length > 0) {
			var self = this;
			var deleteObj = [];
			this.selectedRows().forEach(element => {
				var paisesPKDTO = {
					cdisoalf2: (element.CDISOALF2 != undefined) ? element.CDISOALF2.trim() : null,
					cdisoalf3: (element.CDISOALF3 != undefined) ? element.CDISOALF3.trim() : null,
					cdisonum: (element.CDISONUM != undefined) ? element.CDISONUM.trim() : null
				}
				deleteObj.push(paisesPKDTO);
			});

			angular.element("#dialog-confirm").html("¿Desea eliminar el/los registro/s seleccionado/s?");

			// Define the Dialog and its properties.
			this.confirmDialog = angular.element("#dialog-confirm").dialog({
				resizable: false,
				modal: true,
				title: "Mensaje de Confirmación",
				height: 150,
				width: 360,
				buttons: {
					" Sí ": function () {
						$(this).dialog('close');
						eliminar();
					},
					" No ": function () {
						$(this).dialog('close');
					}
				}
			});

			function eliminar() {
				// Inicia el "Cargando datos..."
				inicializarLoading();
				// Ejecutamos el servicio pasandole por parámetros [this.params]
				self.service.executeActionDEL(deleteObj, {}, function (result) {
					$.unblockUI();
					self.result = result;
					// Ejecuta el modal de respuesta al revicio
					self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					if (!self.result.ok) {
						fErrorTxt("Ha ocurrido un error. Consulte con el administrador.", 1);
					}
					self.resultDialog.dialog("open");

				}, self.postFail.bind(self));
			}
		} else {
			fErrorTxt("Debe seleccionar una o más filas para poder realizar esta opcción", 1);
		}
	};

	sibbac20.controller("ParaisosFiscalesController", ["ParaisosFiscalesService", "$httpParamSerializer", "uiGridConstants",
		"$scope", ParaisosFiscalesController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);