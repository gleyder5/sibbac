'use strict';
sibbac20.controller('DesgloseHerfanosControler', ['$scope', '$compile', 'growl', 'IsinService', '$document', 'DesgloseMQService', function ($scope, $compile, growl, IsinService, $document, DesgloseMQService) {

	var SHOW_LOG = true;

	var oTable = undefined;
	$scope.chisin = true;
	$scope.isines = [];
	$scope.filtroConsulta = {};

	var oDesgloses = [];

    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();
    var openDesgloses;
    var paramsCrear = [];
    var ejecutadaCon = false;
    var alPaginas = [];
    var pagina = 0;
    var paginasTotales = 0;
    var ultimaPagina = 0;
    var desgloses = [];

    hoy = yyyy + "_" + mm + "_" + dd;

    function  processInfo(info) {

        pagina = info.page;
        paginasTotales = info.pages;
        var regInicio = info.start;
        var regFin = info.end;
        return regFin - regInicio;
    }

    var calcDataTableHeight = function (sY) {
        console.log("calcDataTableHeight: " + $('#tblDesglosesHuerfanos').height());
        console.log("sY: " + sY);
        var numeroRegistros = processInfo(oTable.api().page.info());
        var tamanno = 50 * numeroRegistros;
        if ($('#tblDesglosesHuerfanos').height() > 510) {
            return 480;
        } else {
            if ($('#tblDesglosesHuerfanos').height() > tamanno)
            {
                return $('#tblDesglosesHuerfanos').height() + 30;
            } else
            {
                if (tamanno > 510)
                {
                    return 480;
                } else
                {
                    return tamanno;
                }
            }
        }
    };

	function cargarIsines(datosIsin) {
        var availableTags = [];
        angular.forEach(datosIsin, function (val, key) {
            var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
            availableTags.push(ponerTag);
        });
        angular.element("#cdisin").autocomplete({
            source: availableTags
        });
	}//function cargarIsines(datosIsin) {

	function showError(data, status, headers, config) {
        $.unblockUI();
        console.log("Error al inicializar datos: " + status);
    }

	function loadIsines() {
        IsinService.getIsines(cargarIsines, showError);
    }

	// Inicializa la página. Carga los combos y autocompletar del filtro.
    function initialize() {

        //var fechas = ["fechaDe", "fechaA"];
        //verificarFechas([["fechaDe", "fechaA"]]);
        $("#ui-datepicker-div").remove();
        $('input#fechaDe').datepicker({
            onClose: function( selectedDate ) {
              $( "#fechaA" ).datepicker( "option", "minDate", selectedDate );
            } // onClose
          });

          $('input#fechaA').datepicker({
            onClose: function( selectedDate ) {
              $( "#fechaDe" ).datepicker( "option", "maxDate", selectedDate );
            } // onClose
          });
        loadIsines();
        prepareCollapsion();

    }



    /**
     * Se crea la tabla de forma normal
     * @returns {undefined}
     */
    function loadDataTableClientSidePagination() {
        oTable = $("#tblDesglosesHuerfanos").dataTable({
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": ["/sibbac20/js/swf/copy_csv_xls_pdf.swf"],
                "aButtons": ["copy",
                    {"sExtends": "csv",
                        "sFileName": "Listado_de_desgloses_" + hoy + ".csv",
                        //"mColumns": [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                    },
                    {
                        "sExtends": "xls",
                        "sFileName": "Listado_de_desgloses_" + hoy + ".csv",
//                      	 "sCharSet" : "utf8",
                        //"mColumns": [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                    },
                    {
                        "sExtends": "pdf",
                        "sPdfOrientation": "landscape",
                        "sTitle": " ",
                        "sPdfSize": "A4",
                        "sPdfMessage": "Listado de Listado_de_desgloses",
                        "sFileName": "Listado_de_desgloses_" + hoy + ".pdf",
                        //"mColumns": [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                    },
                    "print"
                ]
            },
            "language": {
                "url": "i18n/Spanish.json"
            },

            "aoColumns": [
                {sClass: "centrar", width: 100},                         // 1 N. Orden.
                //{sClass: "centrar", visible: false},                     // columna oculta para la subtabla con los desgloses.
                {sClass: "centrar", width: 100},                         // 3 Cod. SV
                {sClass: "centrar", width: 50},                          // 4 F. ejecución
                {sClass: "centrar", width: 50},                          // 5 Tipo operación
                {sClass: "align-right", "sType": "numeric", width: 75},  // 6 Titulos
                {sClass: "align-right", "sType": "numeric", width: 105}, // 7 Nominal
                {sClass: "centrar", width: 100},                         // 8 Isin
                {sClass: "centrar", width: 250},                         // 9 descripcion
                {sClass: "centrar", width: 75},                          //10 Bolsa de negociación
                {sClass: "centrar", width: 75},                          //11 Tipo saldo
                {sClass: "centrar", width: 75},                          //12 Entidad liquidadora
                {sClass: "centrar", width: 100}                        //13 Tipo cambio
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $compile(nRow)($scope);
            },
            "order": [],
            "scrollY": "480px",
            "scrollX": "auto",
            "scrollCollapse": true,
//	"rowHeight" : "44px",

            drawCallback: function () {
                processInfo(this.api().page.info());
            }
        });

        angular.element("#fechaDe, #fechaA").datepicker({
            dateFormat: 'dd/mm/yy',
            defaultDate: new Date()
        }); // datepick

    }//function loadDataTableClientSidePagination() {

    $document.ready(function () {

        initialize();
        loadDataTableClientSidePagination();
    });

    $scope.LimpiarFiltros = function () {
    	 //ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
        $( "#fechaA" ).datepicker( "option", "minDate", "");
        $( "#fechaDe" ).datepicker( "option", "maxDate", "");

        //
        angular.element('input[type=text]').val('');
        angular.element('#fechaDe').val('');
        angular.element('#fechaA').val('');
        angular.element('#cdisin').val('');

        $scope.chisin = false;
        $scope.btnInvCdIsin();
    };

  //Botón de ejecutar la consulta.
    $scope.cargarTabla = function () {
        ejecutadaCon = true;

        cargarDatosConsulta();
        return false;
    };

    function getIsin(isinCompleto) {

        var guion = isinCompleto.indexOf("-");
        if (guion < 0) {
            return isinCompleto;
        } else {
            return isinCompleto.substring(0, guion);
        }
    }

    function onErrorRequest(data) {
        growl.addErrorMessage("Ocurrió un error de comunicación con el servidor, por favor inténtelo más tarde.");
        $.unblockUI();
    }

  //carga la tablita de los desgloses de un registro.
    function tablaDesgloses(Table, nTr, validado) {
        var aData = Table.fnGetData(nTr);
        var aObjDesglose = oDesgloses[nTr];
        var resultado =
                "<div border-width:0 !important;background-color:#dfdfdf; style='float:left;;margin-left: 80px;' id='div_desglose_" + nTr + "'>" +
                "<table id='tablaDesglose" + nTr + "' style=margin-top:5px;'>" +
                "<tr>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Fecha</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>N. referencia</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Sentido</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Bolsa negociaci&oacute;n</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Isin</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Descripci&oacute;n</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Titulos</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Precio</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Tipo Saldo</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>CCV</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Titular</th>" +
	                "<th class='taleft' style='background-color: #000 !important;'>Entidad liquidadora</th>"+
	                "<th class='taleft' style='background-color: #000 !important;'>Tipo cambio</th>";
                resultado += "</tr>";


        for (var i in aObjDesglose) {

        	var aDesglose = aObjDesglose[i];

            var id = "";
            var fecha = "";
            var numReferencia = "";
            var sentido       = "";
            var bolsa         = "";
            var isin          = "";
            var descripcion   = "";
            var titulos       = "";
            var precio        = "";
            var tipoSaldo     = "";
            var ccv           = "";
            var titular       = "";
            var entidad       = "";
            var tipoCambio    = "";
            for (var j in aDesglose) {

            	var desglose = aDesglose[j];
            	if (j == 'id')
                {
                    id = desglose;
            		continue;
                }else
                if (j == 'fejecucion')
                {

    				if(desglose !== null && desglose !== undefined ){
    					desglose = desglose.substr(8,2)+"/"+desglose.substr(5,2)+"/"+desglose.substr(0,4);
    				}
                	fecha = desglose;
                }
                else
                if (j == 'numReferencia')
                {
                    numReferencia = desglose;
                }else
                if (j == 'tipoOperacion')
                {
                	var tipoOperacion = "Compra";
                	if (desglose == '2')
                	{
                		tipoOperacion = "Venta";
                	}
                	sentido = tipoOperacion;
                }else
                if (j == 'bolsaDescripcion')
                {
                	bolsa = desglose;
                }else
                if (j == 'isin')
                {
                	isin = desglose;
                }else
                if (j == 'desIsin')
                {
                	descripcion = desglose;
                }else
                if (j == 'titulos')
                {
                	titulos = desglose;
                }else
                if (j == 'precio')
                {
                	precio = desglose;
                }else
                if (j == 'tipoSaldo')
                {
                	var aux = "Propio";
                	if (desglose === 'T')
                	{
                		aux = "Terceros";
                	}
                	tipoSaldo = aux;
                }else
                if (j == 'ccv')
                {
                	ccv = desglose;
                }else
                if (j == 'titular')
                {
                	titular = desglose;
                }else
                if (j == 'entidad')
                {
                	entidad = desglose;
                }
                else
                if (j == 'tipoCambio')
                {
                	var aux = "Porcentaje";
                	if (desglose == '2')
                	{
                		aux = "Efectivo";
                	}
                	tipoCambio = aux;
                }
            }
            resultado += "<tr>";
	            resultado += "<td  width=75>"+fecha+"</td>";
	            resultado += "<td  width=75>"+numReferencia+"</td>";
	            resultado += "<td  width=50>"+sentido+"</td>";
	            resultado += "<td  width=75>"+bolsa+"</td>";
	            resultado += "<td  width=75>"+isin+"</td>";
	            resultado += "<td  width=120>"+descripcion+"</td>";
	            resultado += "<td  width=75>"+titulos+"</td>";
	            resultado += "<td  width=75>"+precio+"</td>";
	            resultado += "<td  width=75>"+tipoSaldo+"</td>";
	            resultado += "<td  width=120>"+ccv+"</td>";
	            resultado += "<td  width=75>"+titular+"</td>";
	            resultado += "<td  width=75>"+entidad+"</td>";
	            resultado += "<td  width=75>"+tipoCambio+"</td>";
            resultado += "</tr>";


        }
        resultado += "</table></div>";

        return resultado;
    }

 // Activa la sublinea.//
    $scope.desplegar = function (fila, validado) {
        if (oTable.fnIsOpen(fila)) {
            if (openDesgloses[fila]) {
                oTable.fnClose(fila);
                openDesgloses[fila] = false;
            }
        } else {
            var aData = oTable.fnGetData(fila);
            openDesgloses[fila] = true;
            oTable.fnOpen(fila, tablaDesgloses(oTable, fila, validado));
        }
    };

    function collapseSearchForm()
    {
    	$('.collapser_search').parents('.title_section').next().slideToggle();
        $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
        $('.collapser_search').toggleClass('active');
        if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
            $('.collapser_search').text("Mostrar opciones de búsqueda");
        } else {
            $('.collapser_search').text("Ocultar opciones de búsqueda");
        }
    }

    function seguimientoBusqueda() {
        $scope.menBusDesgloseMQ = "";
        if ($scope.filtroConsulta.fechaDesde !== "") {
            $scope.menBusDesgloseMQ += " fecha desde: " + $scope.filtroConsulta.fechaDesde;
        }
        if ($scope.filtroConsulta.fechaHasta !== "") {
            $scope.menBusDesgloseMQ += " fecha hasta: " + $scope.filtroConsulta.fechaHasta;
        }
        if ($scope.filtroConsulta.isin !== "") {
        	if($scope.chisin)
        		$scope.menBusDesgloseMQ += " isin: " + $scope.filtroConsulta.isin;
        	else
        		$scope.menBusDesgloseMQ += " isin distinto : " + $scope.filtroConsulta.isin;
        }
    }

    function populateOld(json) {


    	if (json.resultados != null && json.resultados !== undefined && json.resultados.desglosesList !== undefined) {

			var item = null;
			var datos = json.resultados.desglosesList;

			var tbl = $("#tblDesglosesHuerfanos > tbody");
			$(tbl).html("");
			oTable.fnClearTable();

			var contador = 0;
			for ( var k in datos) {
				item = datos[k];

				var numOrden ="";
                numOrden = "ng-click=\"desplegar("+contador+","+item.validado+")\" ";
                numOrden = numOrden + " style=\"cursor: pointer; color: #ff0000;\"";
                numOrden =  "<div class='taleft' " + numOrden + " >" +item.numOrden + "</div> "

				contador++;
            	var idName = "check_"+contador;
            	oDesgloses.push(item.listaDesgloses);

            	var classFejecucion = "error";
            	if (item.bokFechaEjecucion == true)
            	{
            		classFejecucion = "exito";
            	}
            	var fejecucion = item.fejecucion;
            	if(fejecucion !== null && fejecucion !== undefined ){
            		fejecucion = fejecucion.substr(8,2)+"/"+fejecucion.substr(5,2)+"/"+fejecucion.substr(0,4);
				}
				fejecucion = "<div class='"+classFejecucion+"' id='fejecuion_"+contador+"' >" +fejecucion + "</div> ";

                var tipoOperacion = "Compra";
            	if (item.tipoOperacion == '2')
            	{
            		tipoOperacion = "Venta";
            	}

            	var classTipoOperacion = "error";
				if (item.bokTipoOperacion == true)
            	{
            		classTipoOperacion = "exito";
            	}
            	tipoOperacion = "<div class='"+classTipoOperacion+"' id='tipoOperacion_"+contador+"' >" +tipoOperacion + "</div> ";

            	var titulos = "<div class='taleft' id='titulos_"+contador+"' >" +item.titulos + "</div> ";
				var nominal =  "<div class='taleft' id='nominal_"+contador+"' >" +item.nominal + "</div> ";

				var classIsin = "error";
				if (item.bokIsin == true)
            	{
            		classIsin = "exito";
            	}
				var isin =  "<div class='"+classIsin+"' id='isin_"+contador+"' >" +item.isin + "</div> ";

				var classBolsa = "error";
				if (item.bokBolsa == true)
            	{
            		classBolsa = "exito";
            	}
				var bolsa = "<div class='"+classBolsa+"' id='bolsa_"+contador+"' >" +item.bolsaDescripcion + "</div> ";

				var tipoSaldo = "Propio";
            	if (item.tipoSaldo === 'T')
            	{
            		tipoSaldo = "Terceros";
            	}

            	var classTipoSaldo = "error";
				if (item.bokTipoSaldo == true)
            	{
					classTipoSaldo = "exito";
            	}
				tipoSaldo = "<div class='"+classTipoSaldo+"' id='tipoSaldo_"+contador+"' >" +tipoSaldo + "</div> ";

            	var tipoCambio = "Porcentaje";
            	if (item.tipoCambio == '2')
            	{
            		tipoCambio = "Efectivo";
            	}

            	var classTipoCambio = "error";
				if (item.bokTipoCambio == true)
            	{
            		classTipoCambio = "exito";
            	}
            	tipoCambio = "<div class='"+classTipoCambio+"' id='tipoCambio_"+contador+"' >" +tipoCambio + "</div> ";

            	var rowIndex = oTable.fnAddData([
				        numOrden,       // 1 numero de orden.
				        //item.listaDesgloses, // columna oculta para la subtabla desgloses.
				        item.codsv,          // 3 Cod. SV.
				        fejecucion,          // 4 F ejecución.
				        tipoOperacion,       // 5 Tipo operación
				        titulos,             // 6 titulos
				        nominal,             // 7 nominal
				        isin,                // 8 isin
				        item.desIsin,        // 9 descripcion isin
				        bolsa,               //10 bolsa de negociación
				        tipoSaldo,           //11 tipo de saldo
				        item.entidad,        //12 entidad liquidadora
				        tipoCambio           //13 tipo de cambio
				        ], false);

				var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
				$('td', nTr)[0].setAttribute( 'style', 'cursor: pointer; color: #ff0000;' );


			}
			oTable.fnDraw();
			$.unblockUI();
			collapseSearchForm();
			seguimientoBusqueda();
			openDesgloses = new Array(contador,false);

		}else{

			if(ejecutadaCon){
				var tipo = 2;
				var errorTxt = json.error;
				if(errorTxt == undefined){
					errorTxt = "Error indeterminado. Consulte con el servicio técnico.";
					tipo = 1;
				}
				fErrorTxt(json.error,tipo);
				$.unblockUI();
		        $('.collapser_search').parents('.title_section').next().slideToggle();
		        $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
		        $('.collapser_search').toggleClass('active');
		        if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
		            $('.collapser_search').text("Mostrar opciones de búsqueda");
		        } else {
		            $('.collapser_search').text("Ocultar opciones de búsqueda");
		        }
			}


		}

    }//populateOld

    function onSuccessSinPaginacion(data, status, header, config) {
        console.log("onSuccessSinPaginacion: " + data);
        populateOld(data);
    }

    function loadDataTableFromServer() {

        oTable = $("#tblDesglosesHuerfanos").dataTable({
            "dom": 'B<"clear">lrtip', /*Para mostrar los controles requeridos del componente datatable*/
            "buttons": [
                {
                    text: 'Excel',
                    action: function (e, dt, node, config) {

                        var data = {action: "getDesglosesHuerfanoExport", service : "SIBBACServiceConciliacionDesglose" ,filters : $scope.filtroConsulta};
                        console.log("data: "+data);
                        // Optional static additional parameters
                        // data.customParameter = ...;

                        if (config.fnData) {
                            config.fnData(data);
                        }

                        var iframe = $('<iframe/>', {
                            id: "RemotingIFrame"
                        }).css({
                            border: 'none',
                            width: 0,
                            height: 0
                        }).appendTo('body');

                        var contentWindow = iframe[0].contentWindow;
                        contentWindow.document.open();
                        contentWindow.document.close();
                        var form = contentWindow.document.createElement('form');
                        form.setAttribute('method', "POST");
                        form.setAttribute('action', "/sibbac20back/rest/export.xlsx");

                        var input = contentWindow.document.createElement('input');
                        input.name = 'webRequest';
                        input.value = JSON.stringify(data);
                        form.appendChild(input);
                        contentWindow.document.body.appendChild(form);
                        form.submit();

                    }
                },
                {
                    text: 'Pdf',
                    action: function (e, dt, node, config) {

                    	var data = {action: "getDesglosesHuerfanoExport", service : "SIBBACServiceConciliacionDesglose" ,filters : $scope.filtroConsulta};
                        console.log("data: "+data);
                        // Optional static additional parameters
                        // data.customParameter = ...;

                        if (config.fnData) {
                            config.fnData(data);
                        }

                        var iframe = $('<iframe/>', {
                            id: "RemotingIFrame"
                        }).css({
                            border: 'none',
                            width: 0,
                            height: 0
                        }).appendTo('body');

                        var contentWindow = iframe[0].contentWindow;
                        contentWindow.document.open();
                        contentWindow.document.close();
                        var form = contentWindow.document.createElement('form');
                        form.setAttribute('method', "POST");
                        form.setAttribute('action', "/sibbac20back/rest/export.pdf");
                        var input = contentWindow.document.createElement('input');
                        input.name = 'webRequest';
                        input.value = JSON.stringify(data);
                        form.appendChild(input);
                        contentWindow.document.body.appendChild(form);
                        form.submit();

                    }
                }
            ],
            "serverSide": true,
            "processing": true,
            "pageLength": datatable.pageLength,
            "ajax": $.fn.dataTable.pipeline({
                "url": '/sibbac20back/rest/service/datatable',
                "pages": datatable.pagesForCache, //Paginas que deja en caché por cada llamada AJAX
                "method": "POST",
                "dataType": "json",
                "data": function (d) {
                    //return $.extend({}, d, {service: 'SIBBACServiceConciliacionDesglose', action: 'getDesglosesPageable', filters: $scope.filtro});
                    d.service = 'SIBBACServiceConciliacionDesglose';
                    d.action = 'getDesglosesHuerfanosPageable';
                    d.filters = $scope.filtroConsulta;
                }
//            	,
//                "dataSrc": function (json) {
//                    return populateTitulares(json);
//                }
            }),
            "language": {
                "url": "i18n/Spanish.json"
            },
            "columns": [
                {data: null, sClass: "centrar nodetails", targets: 1, width: 100,
                    render: function (data, type, row, meta) {
                    	oDesgloses.push(row.listaDesgloses);
                    	var enlace ="";
    					enlace = "ng-click=\"desplegar("+meta.row + 1+","+row.validado+")\" ";
    					enlace = enlace + " style=\"cursor: pointer; color: #ff0000;\"";
    					return "<div class='taleft' " + enlace + " >" +row.numOrden + "</div> ";
                    }
                }, // 1 numero de orden
               // {data: 'listaDesgloses', sClass: "centrar", visible: false}, // columna oculta para la subtabla desgloses.
                {data: 'codsv', sClass: "centrar", width: 100}, // 2 Cod. SV.
                {data: null, sClass: "centrar", width:  50,
                    render: function (data, type, row, meta) {
                    	var classFejecucion = "error";
                    	if (row.bokFechaEjecucion == true)
                    	{
                    		classFejecucion = "exito";
                    	}
                    	var fejecucion = row.fejecucion;
                    	if(fejecucion !== null && fejecucion !== undefined ){
                    		fejecucion = fejecucion.substr(8,2)+"/"+fejecucion.substr(5,2)+"/"+fejecucion.substr(0,4);
        				}
    					return "<div class='"+classFejecucion+"' id='fejecuion_"+meta.row + 1+"' >" +fejecucion+ "</div> ";
                    }
                }, // 3 F ejecución.
                {data: null, sClass: "centrar", width: 50,
                    render: function (data, type, row, meta) {
                    	var tipoOperacion = "Compra";
                    	if (row.tipoOperacion == '2')
                    	{
                    		tipoOperacion = "Venta";
                    	}
                    	var classTipoOperacion = "error";
                    	if (row.bokTipoOperacion == true)
                    	{
                    		classTipoOperacion = "exito";
                    	}
    					return "<div class='"+classTipoOperacion+"' id='tipoOperacion_"+meta.row + 1+"' >" +tipoOperacion + "</div> ";
                    }
                },// 4 Tipo operación
                {data: null, sClass: "align-right", "sType": "numeric", width:  75,
                    render: function (data, type, row, meta) {


    					return "<div class='"+classTitulos+"' id='titulos_"+meta.row + 1+"' >" +row.titulos + "</div> ";
                    }
                }, // 5 Titulos.
                {data: null, sClass: "align-right", "sType": "numeric", width: 105,
                    render: function (data, type, row, meta) {
    					return "<div class='align-right' id='nominal_"+meta.row + 1+"' >" +row.nominal + "</div> ";
                    }
                }, // 6 Nominal.
                {data: null, sClass: "centrar", width:  100,
                    render: function (data, type, row, meta) {

                    	var classIsin = "error";
                    	if (row.bokIsin == true)
                    	{
                    		classIsin = "exito";
                    	}
    					return "<div class='"+classIsin+"' id='isin_"+meta.row + 1+"' >" +row.isin + "</div> ";
                    }
                }, // 7 Isin.
                {data: 'desIsin', sClass: "centrar", width:  250}, // 8 descripción isin.
                {data: null, sClass: "centrar",   width:   75,
                    render: function (data, type, row, meta) {

                    	var classBolsa = "error";
                    	if (row.bokBolsa == true)
                    	{
                    		classBolsa = "exito";
                    	}
    					return "<div class='"+classBolsa+"' id='bolsa_"+meta.row + 1+"' >" +row.bolsaDescripcion + "</div> ";
                    }
                }, // 9 Bolsa de negociación.
                {data: null, sClass: "centrar", width: 50,
                    render: function (data, type, row, meta) {
                    	var tipoSaldo = "Propio";
                    	if (row.tipoSaldo === 'T')
                    	{
                    		tipoSaldo = "Terceros";
                    	}

                    	var classTipoSaldo = "error";
                    	if (row.bokTipoSaldo == true)
                    	{
                    		classTipoSaldo = "exito";
                    	}
    					return "<div class='"+classTipoSaldo+"' id='tipoSaldo_"+meta.row + 1+"' >" +tipoSaldo + "</div> ";
                    }
                },//10 Tipo saldo
                {data: 'entidad', sClass: "centrar",   width:   75}, //11 Entidad liquidadora
                {data: null, sClass: "centrar", width: 50,
                    render: function (data, type, row, meta) {
                    	var tipoCambio = "Porcentaje";
                    	if (row.tipoCambio == '2')
                    	{
                    		tipoCambio = "Efectivo";
                    	}
                    	var classTipoCambio = "error";
                    	if (row.bokTipoCambio == true)
                    	{
                    		classTipoCambio = "exito";
                    	}
    					return "<div class='"+classTipoCambio+"' id='tipoCambio_"+meta.row + 1+"' >" +tipoCambio + "</div> ";
                    }
                }//12 Tipo Cambio
            ],
            "createdRow": function (nRow, aData, iDataIndex) {
                $compile(nRow)($scope);
            },
            "order": [],
            "scrollY": "480px",
            "scrollX": "100%",
            "scrollCollapse": true,
//	"rowHeight" : "44px",

            drawCallback: function () {
                console.log("drawCallback");
                processInfo(this.api().page.info());
                var numFilas = this.api().rows().nodes().length;
                openDesgloses = new Array(numFilas, false);
                $.unblockUI();
            }
        });



        angular.element("#fechaDe, #fechaA").datepicker({
            dateFormat: 'DD/MM/YYYY',
            defaultDate: new Date(),
        }); // datepick
    }

    function onSuccessCountRequest(data, status, header, config) {
    	console.log("Número de registros: " + data.resultados.registros);

        if (data.error !== undefined && data.error !== null && data.error !== "") {
            growl.addErrorMessage(data.error);
        } else {
        	oDesgloses = [];
            collapseSearchForm();
            seguimientoBusqueda();
            desgloses = [];
            if (oTable !== undefined && oTable !== null) {
                oTable.fnDestroy();
            }
            if (data.resultados.registros > (datatable.pageLength * datatable.pagesForCache)) {
                growl.addWarnMessage("¡Atención!. El número de registros en la base de datos, es mayor que el permitido, por tanto se deshabilitará el campo de búsqueda sobre la tabla.");
                loadDataTableFromServer();
            } else {
                loadDataTableClientSidePagination();
                DesgloseMQService.getDesglosesHuerfanosSinPaginacion(onSuccessSinPaginacion, onErrorRequest, $scope.filtroConsulta);
            }

        }

    }

    function recargarDatosPantalla()
    {
    	inicializarLoading();

        DesgloseMQService.getDesglosesHuerfanosCount(onSuccessCountRequest, onErrorRequest, $scope.filtroConsulta);
    }

 // Funcion en la que captura el filtro y muestra todos las operaciones que cumplan las condiciones
    function cargarDatosConsulta() {

        console.log("cargarDatosConsulta");

        var fechaDe = transformaFechaInv(angular.element("#fechaDe").val());
        var fechaA = transformaFechaInv($("#fechaA").val());
        var isinCompleto = angular.element('#cdisin').val();
        var cdIsin = ((!$scope.chisin && $('#cdisin').val() !== "" ? "#" : "") ) + getIsin(isinCompleto);
//        var cdIsin = getIsin(isinCompleto);

        $scope.filtroConsulta = {
            "fechaDesde": fechaDe,
            "fechaHasta": fechaA,
            "isin": cdIsin
        };

        recargarDatosPantalla();

    }

	$scope.btnInvCdIsin = function() {

    	  if($scope.chisin){
		  	  $scope.chisin = false;

  		  document.getElementById('textBtnInvCdisin').innerHTML = "<>";
  		  angular.element('#textInvCdisin').css({"background-color": "transparent", "color": "red"});

  		  document.getElementById('textInvCdisin').innerHTML = "DISTINTO";

    	  }else{
    		  $scope.chisin = true;
      	  document.getElementById('textBtnInvCdisin').innerHTML = "=";
      	  angular.element('#textInvCdisin').css({"background-color": "transparent", "color": "green"});
 			  document.getElementById('textInvCdisin').innerHTML = "IGUAL";

      }
	}

}]);
