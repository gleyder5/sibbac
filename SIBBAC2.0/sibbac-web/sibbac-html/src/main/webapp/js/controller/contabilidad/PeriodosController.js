sibbac20.controller('PeriodosController',['$rootScope', '$scope', '$compile', 'PeriodosService', '$document', 'growl',
						function($rootScope, $scope, $compile, PeriodosService, $document, growl) {
	
	
	$scope.titulo = "Calendario Contable";
	
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();
    hoy = yyyy + "_" + mm + "_" + dd;
	
	$scope.safeApply = function(fn) {
		var phase = this.$root.$$phase;
		if (phase === '$apply' || phase === '$digest') {
			if (fn && (typeof (fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	};
	
	$scope.resetFiltro = function(){
		$scope.filtro = {
			anio : "",
			periodo : "",
			fhIniDesde : "",
			fhIniHasta : ""
		};
	};
	
	$scope.resetFiltro();
	
	$scope.resetPeriodo = function(){
		$scope.periodo = {
			id : "",
			anio : "",
			periodo : "",
			fhIniPeriodo : "",
			fhFinPeriodo : ""
		};
	};
	
	$scope.resetPeriodo();
	


   $scope.oTable = $("#datosPeriodos").dataTable({
	   "dom" : 'T<"clear">lfrtip',
	   "tableTools" : {
		   "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
		   "aButtons" : [ "copy", {
			   "sExtends" : "csv",
			   "sFileName" : "Listado_Periodos_" + hoy + ".csv",
			   "mColumns" : [ 0, 1, 2, 3]
		   }, {
			   "sExtends" : "xls",
			   "sFileName" : "Listado_Periodos_" + hoy + ".xls",
			   "mColumns" : [ 0, 1, 2, 3]
		   }, {
			   "sExtends" : "pdf",
			   "sPdfOrientation" : "landscape",
			   "sTitle" : " ",
			   "sPdfSize" : "A3",
			   "sPdfMessage" : "Listado Final Holders",
			   "sFileName" : "Listado_Periodos_" + hoy + ".pdf",
			   "mColumns" : [ 0, 1, 2, 3]
		   }, "print" ]
	   },
	   "aoColumns" : 
		[ {sClass : "centrar", width : "25%"}, {sClass : "centrar", width : "25%"}, 
		  {sClass : "centrar", width : "25%", sType : "date-eu"}, {sClass : "centrar", width : "25%", sType : "date-eu"} 
	    ],
	   "fnCreatedRow" : function (nRow, aData, iDataIndex) {
		   $compile(nRow)($scope);
	   },
	   "scrollY" : "480px",
	   "scrollX" : "100%",
	   "scrollCollapse" : true,
	   "language" : {
		   "url" : "i18n/Spanish.json"
	   }
   });
   
   
   $scope.consultarPeriodos = function () {
	   
       inicializarLoading();
       
       PeriodosService.consultarPeriodos(
            function (data) {

              $scope.listPeriodos = data.resultados["listaPeriodos"];

              // borra el contenido del body de la tabla
              var tbl = $("#datosPeriodos > tbody");
              $(tbl).html("");
              $scope.oTable.fnClearTable();

              for (var i = 0; i < $scope.listPeriodos.length; i++) {

                var periodosList = [$scope.listPeriodos[i].anio,
                                    $scope.listPeriodos[i].periodo,
                                    $scope.listPeriodos[i].fhIniPeriodo,
                                    $scope.listPeriodos[i].fhFinPeriodo];

                $scope.oTable.fnAddData(periodosList, false);

              }
              $scope.oTable.fnDraw();
              $.unblockUI();
            },
            function (error) {
              $.unblockUI();
              fErrorTxt("Se produjo un error en la carga del listado de periodos.", 1);
            }, $scope.filtro);
     };
     
     $scope.crearPeriodo = function () {
    	 PeriodosService.crearPeriodo(
			  function (data) {
				  if(data.resultados["OK"]){
					  fErrorTxt(data.resultados["OK"], 3);
					  $scope.consultarPeriodos();
				  }else{
					  fErrorTxt(data.resultados["KO"], 1); 
				  }
	              $.unblockUI();
	          },
	          function (error) {
	              $.unblockUI();
	              fErrorTxt("Se produjo un error al crear el periodo.", 1);
	           }
	      );
    	 
     };
	
	
	
} ]);