'use strict';
sibbac20
    .controller(
                'GestionClientesController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'GestionClientesService',
                 'CatalogoService',
                 '$compile',
                 'SecurityService',
                 '$rootScope',
                 function ($scope, $document, growl, GestionClientesService, CatalogoService, $compile,
                           SecurityService, $rootScope) {

                   var oTable = null;
                   var oTablePorCliente = null;
                   var oTablePorContacto = null;
                   var oTablePorMIFID = null;

                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;
                   
                   SecurityService.redirectToLoginIfNotLoggedIn();
                   
                   /**
                    * inspects the selected date and checks that it is not previous to actual date
                    */
                   $scope.checkSelectedDate = function(){
                	   var input = $scope.cliente.fhRevRiesgoPbc;
                	   
                	   var today = new Date();
                       
                	   var dd = today.getDate();
                       var mm = today.getMonth() + 1;
                       var yyyy = today.getFullYear();
                       
                       today = dd +"/"+ mm +"/"+ yyyy;
                	   
                	   var date = new Date();
                	   
                	   //input dd/mm/aaaa
                	   var dateSelected = new Date (input.substr(6,4), input.substr(3,2)-1, input.substr(0,2)); //aaaa - mm - dd
                	   console.log(">>> checkSelectedDate: "+$scope.cliente.fhRevRiesgoPbc);
                	   console.log(">>> dateSelected: "+dateSelected);
                	   if(dateSelected < date) {
                		   alert("La fecha seleccionada no puede ser anterior a la fecha actual");
                		   $scope.cliente.fhRevRiesgoPbc = today;
                	   }
                   }
                   

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   /***************************************************************************************************
                     * ** INICIALIZACION DE DATOS
                     **************************************************************************************************/

                   $(document).ready(function () {
                     $("tituloHeader").html("Gestión de Clientes");
                   });

                   // Variables y listas general
                   $scope.mostrarBuscador = true;
                   $scope.filtro = {};
                   $scope.listClientes = [];
                   $scope.listadoClientes = [];
                   $scope.filtrarPor = "datosCliente";

                   $scope.activeAlert = false;

                   $scope.modal = {
                     titulo : "",
                     showCrear : false,
                     showModificar : false
                   };

                   $scope.followSearch = "";// contiene las migas de los filtros de búsqueda
                   $scope.datosInicializados = false;
                   $scope.showingFilters = true;

                   $scope.isDatosInicializados = function () {
                     return $scope.datosInicializados;
                   }

                   $scope.isFiltroPorCliente = function () {
                     return $scope.filtrarPor == "datosCliente";
                   }

                   $scope.isFiltroPorContacto = function () {
                     return $scope.filtrarPor == "datosContacto";
                   }

                   $scope.isFiltroPorMIFID = function () {
                     return $scope.filtrarPor == "datosMIFID";
                   }

                   $scope.isGrillaPorCliente = function () {
                     return $scope.filtrarPor == "datosCliente";
                   }

                   $scope.isGrillaPorContacto = function () {
                     return $scope.filtrarPor == "datosContacto";
                   }

                   $scope.isGrillaPorMIFID = function () {
                     return $scope.filtrarPor == "datosMIFID";
                   }

                   /***************************************************************************************************
                     * ** DEFINICION TABLA **
                     **************************************************************************************************/

                   var inicializarTablas = function () {

                     if (oTablePorCliente == null)
                       oTablePorCliente = $("#datosClientes").dataTable({
                         "dom" : 'T<"clear">lfrtip',
                         "tableTools" : {
                           "aButtons" : [ "print" ]
                         },
                         "aoColumns" : [ {
                           sClass : "centrar",
                           bSortable : false,
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                	     } , {
                           width : "150px"}],
                         "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                           $compile(nRow)($scope);
                         },
                         "scrollY" : "480px",
                         "scrollX" : "100%",
                         "scrollCollapse" : true,
                         "language" : {
                           "url" : "i18n/Spanish.json"
                         }
                       });

                     if (oTablePorContacto == null)
                       oTablePorContacto = $("#datosContacto").css('width', '100%').dataTable({
                         "dom" : 'T<"clear">lfrtip',
                         "autoWidth" : false,
                         "tableTools" : {
                           "aButtons" : [ "print" ]
                         },
                         "aoColumns" : [ {
                           sClass : "centrar",
                           bSortable : false,
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         } ],
                         "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                           $compile(nRow)($scope);
                         },
                         "scrollY" : "480px",
                         "scrollX" : "100%",
                         "scrollCollapse" : true,
                         "language" : {
                           "url" : "i18n/Spanish.json"
                         }
                       });

                     if (oTablePorMIFID == null)
                       oTablePorMIFID = $("#datosMIFID").css('width', '100%').dataTable({
                         "dom" : 'T<"clear">lfrtip',
                         "autoWidth" : false,
                         "tableTools" : {
                           "aButtons" : [ "print" ]
                         },
                         "aoColumns" : [ {
                           sClass : "centrar",
                           bSortable : false,
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         }, {
                           width : "150px"
                         } ],
                         "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                           $compile(nRow)($scope);
                         },
                         "scrollY" : "480px",
                         "scrollX" : "100%",
                         "scrollCollapse" : true,
                         "language" : {
                           "url" : "i18n/Spanish.json"
                         }
                       });

                     var oTable = oTablePorCliente;

                   };

                   $scope.seleccionarTodos = function () {
                     // Se inicializa la lista de clientes a borrar.
                     $scope.clientesSeleccionadosBorrar = [];
                     for (var i = 0; i < $scope.listadoClientes.length; i++) {
                       $scope.listadoClientes[i].selected = true;
                       $scope.clientesSeleccionadosBorrar.push($scope.listadoClientes[i].id);
                     }
                     $scope.clientesSeleccionados = angular.copy($scope.listadoClientes);
                   };

                   $scope.deseleccionarTodos = function () {
                     for (var i = 0; i < $scope.listadoClientes.length; i++) {
                       $scope.listadoClientes[i].selected = false;
                     }
                     $scope.clientesSeleccionados = [];
                     $scope.clientesSeleccionadosBorrar = [];
                   };

                   $scope.cambioVista = function () {

                     $scope.resetFiltro();
                     var i = 0;

                     // borra el contenido del body de la tabla
                     $scope.borrarTabla();

                     if ($scope.isFiltroPorCliente()) {

                       oTable = oTablePorCliente;
                       i = 0;

                     }

                     if ($scope.isFiltroPorContacto()) {

                       oTable = oTablePorContacto;
                       i = 1;

                     }

                     if ($scope.isFiltroPorMIFID()) {

                       oTable = oTablePorMIFID;
                       i = 2;

                     }

                     var jqTable = $('.fullTable');
                     if (jqTable.length > 0) {
                       var oTableTools = TableTools.fnGetInstance(jqTable[0]);
                       if (oTableTools != null && oTableTools.fnResizeRequired()) {
                         /*
                           * A resize of TableTools' buttons and DataTables' columns is only required on the first
                           * visible draw of the table
                           */
                         // jqTable.dataTable().fnAdjustColumnSizing(false);
                         oTableTools.fnResizeButtons();
                       }
                     }

                   }

                   $scope.borrarTabla = function () {
                     // borra el contenido del body de la tabla
                     if ($scope.isFiltroPorCliente()) {
                       var tbl = $("#datosClientes > tbody");
                       $(tbl).html("");
                       oTablePorCliente.fnClearTable();
                     }
                     if ($scope.isFiltroPorContacto()) {
                       var tbl = $("#datosContacto > tbody");
                       $(tbl).html("");
                       oTablePorContacto.fnClearTable();
                     }
                     if ($scope.isFiltroPorMIFID()) {
                       var tbl = $("#datosMIFID > tbody");
                       $(tbl).html("");
                       oTablePorMIFID.fnClearTable();
                     }
                   };

                   $scope.seleccionarElemento = function (row) {
                     if ($scope.listadoClientes[row] != null && $scope.listadoClientes[row].selected) {
                       $scope.listadoClientes[row].selected = false;
                       for (var i = 0; i < $scope.clientesSeleccionados.length; i++) {
                         if ($scope.clientesSeleccionados[i].id === $scope.listadoClientes[row].id) {
                           $scope.clientesSeleccionados.splice(i, 1);
                           $scope.clientesSeleccionadosBorrar.splice(i, 1);
                         }
                       }
                     } else {
                       $scope.listadoClientes[row].selected = true;
                       $scope.clientesSeleccionados.push($scope.listadoClientes[row]);
                       $scope.clientesSeleccionadosBorrar.push($scope.listadoClientes[row].id);
                     }
                   };

                   /***************************************************************************************************
                     * ** INICIALIZACION DE DATOS - FUNCIONES
                     **************************************************************************************************/

                   $scope.sentidoFiltro = [];

                   $scope.resetButtonFilters = function () {
                     $scope.resetFiltro();
                   };

                   // Setea el filtro que se pasará al back
                   $scope.getFiltro = function () {

                     var filtro = {};
                     for ( var key in $scope.filtro) {
                       if ($scope.filtro[key] != null && $scope.filtro[key] != "") {

                         if ($scope.sentidoFiltro[key] == "IGUAL") {
                           filtro[key] = $scope.filtro[key];
                         } else {
                           filtro[key + "Not"] = $scope.filtro[key];
                           filtro[key] = null;
                         }

                       }
                     }
                     console.log(filtro);

                     return filtro;

                   };

                   $scope.formatoFecha = function (fecha) {
                     if (fecha != null) {
                       fecha = fecha.toString();
                       return fecha.substr(6, 2) + "/" + fecha.substr(4, 2) + "/" + fecha.substr(0, 4);
                     }
                     return "";
                   }

                   $scope.formatoFechaDesdeISO = function (fecha) {
                     if (fecha != null) {
                       fecha = fecha.toString();
                       return fecha.substr(8, 2) + "/" + fecha.substr(5, 2) + "/" + fecha.substr(0, 4);
                     }
                     return "";
                   }

                   // Reset del filtro
                   $scope.resetFiltro = function () {

                     $scope.desCliente = "";

                     $scope.filtro = {
                       idCliente : null,
                       tipoDocumento : null,
                       numDocumento : null,
                       tipoResidencia : null,
                       codPersonaBDP : null,
                       numeroFactura : null,
                       tipoCliente : null,
                       naturalezaCliente : null,
                       estadoCliente : null,
                       fechaAlta : null,
                       fechaBaja : null,
                       codigoKGR : null,
                       tipoNacionalidad : null,
                       codigoPais : null,
                       categorizacion : null,
                       controlPBC : null,
                       motivoPBC : null,
                       riesgoPBC : null,
                       fhRevRiesgoPbc : null,
                       nombre : null,
                       posicion : null,
                       telefono : null,
                       email : null,
                       fax : null,
                       direccion : null,
                       categoria : null,
                       testConveniencia : null,
                       testIdoneidad : null,
                       fechaFinVigConveniencia : null,
                       fechaFinVigIdoneidad : null
                     };

                     $scope.sentidoFiltro = [];
                     for ( var key in $scope.filtro) {
                       $scope.sentidoFiltro[key] = "IGUAL";
                     }

                     $scope.deseleccionarTodos();
                     $scope.clientesSeleccionados = [];
                     $scope.clientesSeleccionadosBorrar = [];

                   };

                   $scope.getFollowSearch = function () {

                     var labelFiltro = {
                       idCliente : "Cliente",
                       tipoDocumento : "Tipo de Documento",
                       numDocumento : "Nro de Documento",
                       tipoResidencia : "Tipo de Residencia",
                       codPersonaBDP : "Código de Persona BDP",
                       numeroFactura : "nro Factura",
                       tipoCliente : "Tipo de Cliente",
                       naturalezaCliente : "Naturaleza de Cliente",
                       estadoCliente : "Estado de Cliente",
                       fechaAlta : "Fecha de Alta",
                       fechaBaja : "Fecha de Baja",
                       codigoKGR : "Código KGR",
                       tipoNacionalidad : "Tipo de Nacionalidad",
                       codigoPais : "Código del país de Nacionalidad",
                       categorizacion : "Categorización",
                       controlPBC : "Control PBC",
                       motivoPBC : "Motivo PBC",
                       riesgoPBC : "Riesgo PBC",
                       fhRevRiesgoPbc : "Próx. Revisión Riesgo PBC",
                       nombre : "Nombre",
                       posicion : "Posición",
                       telefono : "Teléfono",
                       email : "Email",
                       fax : "Fax",
                       direccion : "Dirección",
                       categoria : "Categoría",
                       testConveniencia : "Test de conveniencia",
                       testIdoneidad : "Test de idoneidad",
                       fechaFinVigConveniencia : "Fin vcia. de conv.",
                       fechaFinVigIdoneidad : "Fin vcia. de idoneidad"
                     };

                     var texto = "Filtro por ";
                     if ($scope.isFiltroPorCliente()) {
                       texto += "Datos cliente "
                     }
                     if ($scope.isFiltroPorContacto()) {
                       texto += "Datos contacto "
                     }
                     if ($scope.isFiltroPorMIFID()) {
                       texto += "Datos MIFID "
                     }

                     for ( var key in $scope.filtro) {
                       if ($scope.filtro[key] != null && $scope.filtro[key] != "") {

                         var operador = "";
                         if ($scope.sentidoFiltro[key] == "IGUAL") {
                           operador = "=";
                         } else {
                           operador = "<>";
                         }

                         var valorDescriptivo = getValorDescriptivo(key, $scope.filtro[key]);
                         if (valorDescriptivo == "") {
                           valorDescriptivo = $scope.filtro[key];
                         }

                         if (texto == "") {
                           texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo;
                         } else {
                           texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo;
                         }

                       }
                     }
                     return texto;

                   }

                   var getValorDescriptivo = function (filtro, valor) {

                     if (filtro == "categoria") {
                       return getDescripcionCatalogo($scope.catalogos.categorias, valor);
                     }
                     if (filtro == "categorizacion") {
                       return getDescripcionCatalogo($scope.catalogos.categorizacionesCliente, valor);
                     }
                     if (filtro == "codigoPais") {
                       return getDescripcionCatalogo($scope.catalogos.nacionalidades, valor);
                     }
                     if (filtro == "controlPBC") {
                       return getDescripcionCatalogo($scope.catalogos.siNo, valor);
                     }
                     if (filtro == "riesgoPBC") {
                         return getDescripcionCatalogo($scope.catalogos.riesgosPBC, valor);
                     }
                     if (filtro == "siNo") {
                       return getDescripcionCatalogo($scope.catalogos.siNo, valor);
                     }
                     if (filtro == "estadoCliente") {
                       return getDescripcionCatalogo($scope.catalogos.estadosDeCliente, valor);
                     }
                     if (filtro == "idCliente") {
                       return getDescripcionCatalogo($scope.catalogos.clientes, valor);
                     }
                     if (filtro == "motivoPBC") {
                       return getDescripcionCatalogo($scope.catalogos.motivosPBC, valor);
                     }
                     if (filtro == "naturalezaCliente") {
                       return getDescripcionCatalogo($scope.catalogos.naturalezaDeCliente, valor);
                     }
                     if (filtro == "testConveniencia") {
                       return getDescripcionCatalogo($scope.catalogos.testDeConveniencia, valor);
                     }
                     if (filtro == "testIdoneidad") {
                       return getDescripcionCatalogo($scope.catalogos.testDeIdoneidad, valor);
                     }
                     if (filtro == "tipoCliente") {
                       return getDescripcionCatalogo($scope.catalogos.tiposDeCliente, valor);
                     }
                     if (filtro == "tipoDocumento") {
                       return getDescripcionCatalogo($scope.catalogos.tiposDeDocumento, valor);
                     }
                     if (filtro == "tipoNacionalidad") {
                       return getDescripcionCatalogo($scope.catalogos.tiposDeNacionalidad, valor);
                     }
                     if (filtro == "tipoResidencia") {
                       return getDescripcionCatalogo($scope.catalogos.tiposDeResidencia, valor);
                     }
                     return "";

                   }

                   var getDescripcionCatalogo = function (catalogo, key) {
                     
                     if(catalogo!=null && catalogo != undefined){
                    	 for (var i = 0; i < catalogo.length; i++) {
	                       if (catalogo[i].key == key) {
	                         return catalogo[i].value;
	                       }
	                     }
                     }
                     else{
                    	 console.log("Catalogo is null for KEY: " + key);	 
                     }
                     return "";
                   }

                   /***************************************************************************************************
                     * ** FUNCIONES GENERALES
                     **************************************************************************************************/

                   $scope.controlError = function (txterror) {
                     if (txterror.length == 0) {
                       txterror = "Error genérico, por favor contacte con Soporte IT.";
                     }
                     fErrorTxt(txterror, 1);
                     $scope.mensajeError = txterror;
                     $scope.mostrarErrores = true;
                   }

                   /***************************************************************************************************
                     * ** CARGA DE DATOS INICIALES
                     **************************************************************************************************/

                   $document.ready(function () {

                	   if($rootScope.isAuthenticated()){

  	                     inicializarTablas();
  	                     $scope.resetFiltro();
  	
  	                     CatalogoService.todos(function (data) {
  	
  	                       $scope.catalogos = data.resultados;
  	                       // console.log($scope.catalogos);
  	                       populateAutocomplete("#provinciaCliente", $scope.catalogos.provincias, function (option) {
  	                         $scope.cliente.address.nbProvin = $.trim(option.value);
  	                         $scope.cliente.address.cdPostal = $.trim(option.key);
  	                       });
  	
  	                       populateAutocomplete("#provinciaContacto", $scope.catalogos.provincias, function (option) {
  	                         $scope.contacto.address.nbProvin = $.trim(option.value);
  	                         $scope.contacto.address.cdPostal = $.trim(option.key);
  	                       });
  	
  	                       cargaClientes();
  	
  	                       $scope.datosInicializados = true;
  	
  	                     }, function (error) {
  	                       fErrorTxt("Se produjo un error en la carga de los combos", 1);
  	                     });
                  	   }

                   });

                   var seleccionadoCliente = function (option) {
                     $scope.filtro.idCliente = option.key;
                     $scope.desCliente = option.value;
                   }

                   var cargaClientes = function () {

                     /* Carga combo Clientes */
                     GestionClientesService.cargaClientes(function (data) {
                       $scope.catalogos.clientes = data.resultados.listaClientes;
                       populateAutocomplete("#filtro_cliente1", $scope.catalogos.clientes, seleccionadoCliente);
                       populateAutocomplete("#filtro_cliente2", $scope.catalogos.clientes, seleccionadoCliente);
                       populateAutocomplete("#filtro_cliente3", $scope.catalogos.clientes, seleccionadoCliente);
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga de los clientes.", 1);
                     });

                   }

                   // Control de la provincia cuando modifica
                   // el pais de residencia en el alta y la
                   // modificación.
                   $scope.changePaisResidenciaCliente = function () {
                     $scope.cliente.address.cdPostal = "";
                     $scope.cliente.address.nbProvin = "";
                     if ($scope.cliente.address.cdDePais.trim() == '011') { // Si es españa se habilita autocompletar y
                       // se inicializan la provicina y el codigo
                       // postal
                       $("#provinciaCliente").autocomplete("enable");
                     } else { // Se desabilita y se deja que inserta un texto fijo. En este caso el codigo postal es 99
                       // + los 3 digitos del pais.
                       $("#provinciaCliente").autocomplete("disable");
                       $scope.cliente.address.cdPostal = "99" + $scope.cliente.address.cdDePais;
                     }
                   };

                   // Control de la provincia cuando modifica
                   // el pais de residencia en el alta y la
                   // modificación.
                   $scope.changePaisResidenciaContacto = function () {
                     $scope.contacto.address.cdPostal = "";
                     $scope.contacto.address.nbProvin = "";
                     if ($scope.contacto.address.cdDePais.trim() == '011') { // Si es españa se habilita autocompletar
                       // y se inicializan la provicina y el
                       // codigo postal
                       $("#provinciaContacto").autocomplete("enable");
                     } else { // Se desabilita y se deja que inserta un texto fijo. En este caso el codigo postal es 99
                       // + los 3 digitos del pais.
                       $("#provinciaContacto").autocomplete("disable");
                       $scope.contacto.address.cdPostal = "99" + $scope.contacto.address.cdDePais;
                     }
                   };

                   var populateAutocomplete = function (input, availableTags, callback) {

                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {
                         var option = {
                           key : ui.item.key,
                           value : ui.item.value
                         };
                         callback(option);
                         $scope.safeApply();
                         return false;
                       }
                     });

                   };

                   $scope.getCatalogoDirecciones = function () {
                     return $scope.cliente.listadoDireccionesRelacionadas;
                   }

                   /***************************************************************************************************
                     * ** ACCIONES
                     **************************************************************************************************/

                   $scope.pintarTablaClientes = function (data) {

                     inicializarTablas();

                     if (data == null || data === "undefined") {
                       fErrorTxt("Se produjo un error en la carga del listado.", 1);
                       return false;
                     }

                     if (typeof data === 'string') {
                       fErrorTxt("Se produjo un error en la carga del listado.", 1);
                       return false;
                     }

                     $scope.listadoClientes = data.resultados.listaClientes;

                     // se inicializan las variables de clientes seleccionados
                     $scope.clientesSeleccionados = [];
                     $scope.clientesSeleccionadosBorrar = [];

                     // borra el contenido del body de la tabla
                     $scope.borrarTabla();

                     var list = [];

                     if ($scope.listadoClientes.length == 0) {

                       $scope.isResultados = false;

                     } else {

                       $scope.isResultados = true;

                       for (var i = 0; i < $scope.listadoClientes.length; i++) {

                         var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listadoClientes['
                                     + i + '].selected" ng-click="seleccionarElemento(' + i + ');"/>';

                         if ($scope.isGrillaPorCliente()) {
                           list[i] = [ check, $scope.listadoClientes[i].descli,
                                      getValorDescriptivo("tipoDocumento", $scope.listadoClientes[i].tpidenti),
                                      $scope.listadoClientes[i].cif,
                                      getValorDescriptivo("tipoResidencia", $scope.listadoClientes[i].tpreside),
                                      $scope.listadoClientes[i].cdbdp,
                                      getValorDescriptivo("tipoCliente", $scope.listadoClientes[i].tpclienn),
                                      getValorDescriptivo("naturalezaCliente", $scope.listadoClientes[i].tpfisjur),
                                      getValorDescriptivo("estadoCliente", $scope.listadoClientes[i].cdestado),
                                      $scope.formatoFecha($scope.listadoClientes[i].fhinicio),
                                      $scope.formatoFecha($scope.listadoClientes[i].fhfinal),
                                      $scope.listadoClientes[i].cdkgr,
                                      getValorDescriptivo("tipoNacionalidad", $scope.listadoClientes[i].tpNacTit),
                                      getValorDescriptivo("codigoPais", $scope.listadoClientes[i].cdNacTit),
                                      getValorDescriptivo("categorizacion", $scope.listadoClientes[i].cdcat),
                                      getValorDescriptivo("controlPBC", $scope.listadoClientes[i].cddeprev),
                                      getValorDescriptivo("motivoPBC", $scope.listadoClientes[i].motiPrev),
                                      getValorDescriptivo("riesgoPBC", $scope.listadoClientes[i].riesgoPbc),
                                      $scope.formatoFechaDesdeISO($scope.listadoClientes[i].fhRevRiesgoPbc)];
                         }

                         else if ($scope.isGrillaPorContacto()) {
                           list[i] = [ check, $scope.listadoClientes[i].nameAndDescClient,
                                      $scope.listadoClientes[i].nombre, $scope.listadoClientes[i].apellidos,
                                      $scope.listadoClientes[i].descripcion, $scope.listadoClientes[i].telefonos,
                                      $scope.listadoClientes[i].movil, $scope.listadoClientes[i].fax,
                                      $scope.listadoClientes[i].email, $scope.listadoClientes[i].comentarios,
                                      $scope.listadoClientes[i].posicionCargo,
                                      getValorDescriptivo("siNo", $scope.listadoClientes[i].activo),
                                      ($scope.listadoClientes[i].gestor) ? "Si" : "No",
                                      $scope.listadoClientes[i].direccionPrincipal ];
                         }

                         else if ($scope.isGrillaPorMIFID()) {
                           list[i] = [ check, $scope.listadoClientes[i].nameAndDescClient,
                                      getValorDescriptivo("categoria", $scope.listadoClientes[i].category),
                                      getValorDescriptivo("testConveniencia", $scope.listadoClientes[i].tConven),
                                      getValorDescriptivo("testIdoneidad", $scope.listadoClientes[i].tIdoneid),
                                      $scope.formatoFechaDesdeISO($scope.listadoClientes[i].fhConv),
                                      $scope.formatoFechaDesdeISO($scope.listadoClientes[i].fhIdon) ];
                         }

                       }

                       if ($scope.isGrillaPorCliente()) {

                         oTablePorCliente.fnAddData(list, false);
                         oTablePorCliente.fnDraw();
                       }

                       else if ($scope.isGrillaPorContacto()) {

                         oTablePorContacto.fnAddData(list, false);
                         oTablePorContacto.fnDraw();
                         $("#datosContacto").css("width", "100%");
                       }

                       else if ($scope.isGrillaPorMIFID()) {

                         oTablePorMIFID.fnAddData(list, false);
                         oTablePorMIFID.fnDraw();
                         $("#datosMIFID").css("width", "100%");
                       }

                       $.unblockUI();

                     }

                   }

                   $scope.mensajeErrorFormularioBusqueda;

                   $scope.isFormularioBusquedaValido = function () {
                     return true;
                   }

                   // Carga del listado principal
                   $scope.busqueda = function () {

                     $scope.clientesSeleccionados = [];
                     $scope.clientesSeleccionadosBorrar = [];

                     if (!$scope.isFormularioBusquedaValido()) {
                       return false;
                     }

                     // Se oculta el panel de búsqueda y se muestra el detalle
                     $scope.showingFilter = false;
                     // Se muestra la capa cargando
                     inicializarLoading();

                     if ($scope.isFiltroPorCliente()) {
                       GestionClientesService.busquedaClientes(function (data) {
                         $.unblockUI();
                         $scope.pintarTablaClientes(data);
                       }, function (error) {
                         $.unblockUI();
                         fErrorTxt("Se produjo un error en la carga del listado de clientes.", 1);
                       }, $scope.getFiltro());
                     }
                     else if ($scope.isFiltroPorContacto()) {
                       GestionClientesService.busquedaContactos(function (data) {
                         $.unblockUI();
                         $scope.pintarTablaClientes(data);
                       }, function (error) {
                         $.unblockUI();
                         fErrorTxt("Se produjo un error en la carga del listado de clientes.", 1);
                       }, $scope.getFiltro());
                     }
                     else if ($scope.isFiltroPorMIFID()) {
                       GestionClientesService.busquedaMifids(function (data) {
                         $.unblockUI();
                         $scope.pintarTablaClientes(data);
                       }, function (error) {
                         $.unblockUI();
                         fErrorTxt("Se produjo un error en la carga del listado de clientes.", 1);
                       }, $scope.getFiltro());
                     }

                   };

                   $scope.borrarClientes = function () {

                     var entidad = "";
                     if ($scope.isGrillaPorCliente()) {
                       entidad = "Cliente";
                     }
                     else if ($scope.isGrillaPorContacto()) {
                       entidad = "Contacto";
                     }
                     else if ($scope.isGrillaPorMIFID()) {
                       entidad = "Cliente";
                       $scope.clientesSeleccionadosBorrar = [];
                       // Selecciono los clientes asociados al los mifids seleccionados
                       for (var i = 0; i < $scope.listadoClientes.length; i++) {
                         $scope.clientesSeleccionadosBorrar.push($scope.listadoClientes[i].idCliente);
                       }
                     }

                     if ($scope.clientesSeleccionadosBorrar.length > 0) {
                       if ($scope.clientesSeleccionadosBorrar.length == 1) {
                         angular.element("#dialog-confirm").html("¿Desea eliminar el " + entidad + " seleccionado?");
                       } else {
                         angular.element("#dialog-confirm").html(
                                                                 "¿Desea eliminar los "
                                                                     + $scope.clientesSeleccionadosBorrar.length + "  "
                                                                     + entidad + "s seleccionados?");
                       }

                       // Define the Dialog and its properties.
                       angular.element("#dialog-confirm").dialog({
                         resizable : false,
                         modal : true,
                         title : "Mensaje de Confirmación",
                         height : 150,
                         width : 360,
                         buttons : {
                           " Sí " : function () {

                             inicializarLoading();

                             if ($scope.isGrillaPorCliente()) {

                               GestionClientesService.eliminarClientes(function (data) {
                                 fErrorTxt("Se ha eliminado el/los clientes correctamente.", 3);
                                 $scope.busqueda();
                               }, function (error) {
                                 $.unblockUI();
                                 fErrorTxt("Ocurrió un error al eliminar los clientes.", 1);
                               }, {
                                 "listaBorrar" : $scope.clientesSeleccionadosBorrar,
                                 "userName" : $rootScope.userName
                               });

                             }
                             else if ($scope.isGrillaPorContacto()) {

                               GestionClientesService.eliminarContactos(function (data) {
                                 fErrorTxt("Se ha eliminado el/los contactos correctamente.", 3);
                                 $scope.busqueda();
                               }, function (error) {
                                 $.unblockUI();
                                 fErrorTxt("Ocurrió un error al eliminar los contactos.", 1);
                               }, {
                                 "listaBorrar" : $scope.clientesSeleccionadosBorrar,
                                 "userName" : $rootScope.userName
                               });

                             }
                             else if ($scope.isGrillaPorMIFID()) {

                               GestionClientesService.eliminarClientes(function (data) {
                                 fErrorTxt("Se ha eliminado el/los clientes correctamente.", 3);
                                 $scope.busqueda();
                               }, function (error) {
                                 $.unblockUI();
                                 fErrorTxt("Ocurrió un error al eliminar los clientes.", 1);
                               }, {
                                 "listaBorrar" : $scope.clientesSeleccionadosBorrar,
                                 "userName" : $rootScope.userName
                               });

                             }

                             $(this).dialog('close');

                           },
                           " No " : function () {
                             $(this).dialog('close');
                           }
                         }
                       });
                       $('.ui-dialog-titlebar-close').remove();
                     } else {
                       fErrorTxt("Debe seleccionar al menos un elemento de la tabla de clientes.", 2)
                     }
                   };

                   /***************************************************************************************************
                     * ** ACCIONES CRUD / MODALES
                     **************************************************************************************************/

                   // Reset objeto usuario
                   $scope.resetCliente = function () {
                     $scope.cliente = {
                       cdbrocli : "",
                       tpidenti : "",
                       cif : "",
                       tpreside : "",
                       tpclienn : "",
                       tpfisjur : "F",
                       tpNacTit : "",
                       cdCacTit : "",
                       cdcat : "",
                       cddeprev : "",
                       motiprev : "",
                       riesgoPbc : "",
                       fhRevRiesgoPbc : "",
                       address : {

                       },
                       direccionesRelacionadas : [],
                       contactos : [],
                       mifid : [ {
                         id : null,
                         category : "",
                         tConven : "",
                         tIdoneid : "",
                         fhIdon : "",
                         fhConv : ""
                       } ]
                     };
                     $scope.activeAlert = false;
                     $scope.mensajeAlert = "";
                   };

                   $scope.eliminarElementoTabla = function (tabla) {
                     switch (tabla) {
                       case "perfil":
                         for (var i = 0; i < $scope.cliente.listaProfiles.length; i++) {
                           if ($scope.cliente.listaProfiles[i].value === $scope.cliente.perfil.value) {
                             $scope.cliente.listaProfiles.splice(i, 1);
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.vaciarTabla = function (tabla) {
                     switch (tabla) {
                       case "perfil":
                         $scope.cliente.listaProfiles = [];
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.siguientePaso = function () {
                     $scope.errores = [];
                     if ($scope.paso == 'formularioDatosCliente') {

                    	 if ($scope.cliente.listadoDireccionesRelacionadas === undefined) {
                    		 $scope.cliente.listadoDireccionesRelacionadas = [];
                    	 }
                         if (!$scope.isConsultarCliente() && $scope.cliente.listadoDireccionesRelacionadas.length == 0) {
                           if ($scope.cliente.address != null && !$scope.vacio($scope.cliente.address.nbDomici)) {
                             $scope.cliente.listadoDireccionesRelacionadas.push($scope.cliente.address);
                           }
                         }

                       if ($scope.isConsultarCliente() || $scope.isValidoFormularioDatosCliente()) {
                         $scope.paso = 'formularioDatosContacto';
                         $scope.activeAlert = false;
                         return;
                       } else {
                         $scope.activeAlert = true;
                         $scope.errores.push("Debe completar los campos obligatorios resaltados en pantalla");
                         return false;
                       }
                     }
                     if ($scope.paso == 'formularioDatosContacto') {
                       if ($scope.isConsultarCliente() || $scope.isValidoFormularioDatosContacto()) {
                         $scope.paso = 'formularioDatosMIFID';
                         return;
                       } else {
                         fErrorTxt("Debe especificar al menos un contacto para el cliente", 2);
                       }
                     }
                   };

                   $scope.anteriorPaso = function () {
                     if ($scope.paso == 'formularioDatosContacto') {
                       $scope.paso = 'formularioDatosCliente';
                       return;
                     }
                     if ($scope.paso == 'formularioDatosMIFID') {
                       $scope.paso = 'formularioDatosContacto';
                       return;
                     }
                   };

                   $scope.isSiguientePaso = function () {
                     return $scope.paso == 'formularioDatosCliente' || $scope.paso == 'formularioDatosContacto';
                   };

                   $scope.isAnteriorPaso = function () {
                     return $scope.paso == 'formularioDatosMIFID' || $scope.paso == 'formularioDatosContacto';
                   };

                   // Abrir modal creacion clientes
                   $scope.abrirCrearCliente = function () {
                     // se inicializa el cliente
                     $scope.errores = [];
                     $scope.paso = 'formularioDatosCliente';
                     $scope.resetCliente();
                     $scope.modal.showCrear = true;
                     $scope.modal.showModificar = false;
                     $scope.contactosSeleccionados = [];
                     $scope.modal.titulo = "Crear Cliente";
                     angular.element('#formularios').modal({
                       backdrop : 'static'
                     });
                     $(".modal-backdrop").remove();
                   };

                   $scope.isConsultarCliente = function () {

                     return (!$scope.modal.showCrear && !$scope.modal.showModificar);

                   }

                   // Abrir modal modificar clientes
                   $scope.abrirConsultarCliente = function () {

                     $scope.errores = [];
                     if ($scope.clientesSeleccionados.length > 1 || $scope.clientesSeleccionados.length == 0) {
                       if ($scope.clientesSeleccionados.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla.", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla.", 2)
                       }
                     } else {

                       if ($scope.isGrillaPorContacto()) {
                         $scope.modalContacto.showConsultar = true;
                         $scope.abrirModificarContactoClienteDesdeGrillaPrincipal();
                         return false;
                       }

                       if ($scope.isGrillaPorMIFID()) {

                         // Obtengo el cliente asociado
                         inicializarLoading();
                         GestionClientesService
                             .recuperarCliente(
                                               function (data) {

                                                 if (data.resultados == null) {
                                                   $.unblockUI();
                                                   fErrorTxt("Ocurrió un error durante la petición de datos al consultar el contacto.", 1);
                                                   return;
                                                 }
                                                 $(".modal-backdrop").remove();
                                                 $.unblockUI();
                                                 // se copian los datos del cliente seleccionado en la variable cliente
                                                 $scope.cliente = angular.copy(data.resultados.cliente);
                                                 $scope.paso = 'formularioDatosMIFID';
                                                 $scope.cliente.fhNacimi = $scope
                                                 	.formatoFechaDesdeISO($scope.cliente.fhNacimi);
                                                 $scope.cliente.fhRevRiesgoPbc = $scope
                                                 	.formatoFechaDesdeISO($scope.cliente.fhRevRiesgoPbc);
                                                 $scope.cliente.mifid[0].fhConv = $scope
                                                 	.formatoFechaDesdeISO($scope.cliente.mifid[0].fhConv);
                                                 $scope.cliente.mifid[0].fhIdon = $scope
                                                 	.formatoFechaDesdeISO($scope.cliente.mifid[0].fhIdon);

                                               },
                                               function (error) {
                                                 $.unblockUI();
                                                 angular.element('#formularioContacto').modal("hide");
                                                 fErrorTxt("Ocurrió un error durante la petición de datos al modificar el contacto.", 1);
                                               }, {
                                                 "id" : $scope.clientesSeleccionados[0].idCliente
                                               });

                       } else {

                    	// Obtengo el cliente asociado
                           inicializarLoading();
                           GestionClientesService
                               .recuperarCliente(
                                                 function (data) {

                                                   if (data.resultados == null) {
                                                     $.unblockUI();
                                                     fErrorTxt("Ocurrió un error durante la petición de datos al consultar el contacto.", 1);
                                                     return;
                                                   }
                                                   $(".modal-backdrop").remove();
                                                   $.unblockUI();
                                                   // se copian los datos del cliente seleccionado en la variable cliente
                                                   $scope.cliente = angular.copy(data.resultados.cliente);
                                                   $scope.cliente.fhNacimi = $scope
                                                       .formatoFechaDesdeISO($scope.cliente.fhNacimi);
                                                   $scope.cliente.fhRevRiesgoPbc = $scope
                                                	.formatoFechaDesdeISO($scope.cliente.fhRevRiesgoPbc);

                                                   if ($scope.cliente.mifid === undefined) {
                                                	   $scope.cliente.mifid = [];
                                                   } else if ($scope.cliente.mifid.length > 0) {
	                                                   $scope.cliente.mifid[0].fhConv = $scope
	                                                       .formatoFechaDesdeISO($scope.cliente.mifid[0].fhConv);
	                                                   $scope.cliente.mifid[0].fhIdon = $scope
	                                                       .formatoFechaDesdeISO($scope.cliente.mifid[0].fhIdon);
	                                                }

                                                 },
                                                 function (error) {
                                                   $.unblockUI();
                                                   angular.element('#formularioContacto').modal("hide");
                                                   fErrorTxt("Ocurrió un error durante la petición de datos al modificar el contacto.",1);
                                                 }, {
                                                   "id" : $scope.clientesSeleccionados[0].id
                                                 });

                       }

                       $scope.mensajeAlert = "";
                       $scope.modal.showCrear = false;
                       $scope.modal.showModificar = false;
                       $scope.modal.titulo = "Consultas Cliente";
                       $scope.paso = "formularioDatosCliente";

                       angular.element('#formularios').modal({
                         backdrop : 'static'
                       });

                       if ($scope.isGrillaPorCliente()) {
                         $scope.paso = "formularioDatosCliente";
                       }

                       $(".modal-backdrop").remove();

                     }

                   };

                   // Abrir modal modificar clientes
                   $scope.abrirModificarCliente = function () {

                     $scope.errores = [];
                     if ($scope.clientesSeleccionados.length > 1 || $scope.clientesSeleccionados.length == 0) {
                       if ($scope.clientesSeleccionados.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla.", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla.", 2)
                       }
                     } else {

                       if ($scope.isGrillaPorContacto()) {
                         $scope.modalContacto.showConsultar = false;
                         $scope.abrirModificarContactoClienteDesdeGrillaPrincipal();
                         return false;
                       }

                       if ($scope.isGrillaPorMIFID()) {

                         // Obtengo el cliente asociado
                         inicializarLoading();
                         GestionClientesService.recuperarCliente(function (data) {

                           if (data.resultados == null) {
                             $.unblockUI();
                             fErrorTxt("Ocurrió un error durante la petición de datos al consultar el contacto.", 1);
                             return;
                           }
                           $(".modal-backdrop").remove();
                           $.unblockUI();
                           // se copian los datos del cliente seleccionado en la variable cliente
                           $scope.cliente = angular.copy(data.resultados.cliente);
                           if ($scope.cliente.mifid[0] !== undefined && $scope.cliente.mifid[0] != null) {
                             $scope.cliente.mifid[0].fhConv = $scope
                                 .formatoFechaDesdeISO($scope.cliente.mifid[0].fhConv);
                             $scope.cliente.mifid[0].fhIdon = $scope
                                 .formatoFechaDesdeISO($scope.cliente.mifid[0].fhIdon);
                           } else {
                             $scope.cliente.mifid = [ {} ];
                           }
                           $scope.cliente.fhNacimi = $scope.formatoFechaDesdeISO($scope.cliente.fhNacimi);
                           $scope.paso = 'formularioDatosMIFID';

                         }, function (error) {
                           $.unblockUI();
                           angular.element('#formularioContacto').modal("hide");
                           fErrorTxt("Ocurrió un error durante la petición de datos al modificar el contacto.", 1);
                         }, {
                           "id" : $scope.clientesSeleccionados[0].idCliente
                         });

                       } else {

                    	// Obtengo el cliente asociado
                           inicializarLoading();
                           GestionClientesService
                               .recuperarCliente(
                                                 function (data) {

                                                   if (data.resultados == null) {
                                                     $.unblockUI();
                                                     fErrorTxt("Ocurrió un error durante la petición de datos al consultar el cliente.", 1);
                                                     return;
                                                   }
                                                   $(".modal-backdrop").remove();
                                                   $.unblockUI();
                                                   // se copian los datos del cliente seleccionado en la variable cliente
                                                   $scope.cliente = angular.copy(data.resultados.cliente);
                                                   if ($scope.cliente.mifid[0] !== undefined && $scope.cliente.mifid[0] != null) {
                                                	   $scope.cliente.mifid[0].fhConv = $scope
                                                       .formatoFechaDesdeISO($scope.cliente.mifid[0].fhConv);
                                                	   $scope.cliente.mifid[0].fhIdon = $scope
                                                         .formatoFechaDesdeISO($scope.cliente.mifid[0].fhIdon);
                                                   } else {
                                                     $scope.cliente.mifid = [ {} ];
                                                   }
                                                   $scope.cliente.fhNacimi = $scope.formatoFechaDesdeISO($scope.cliente.fhNacimi);
                                                   $scope.cliente.fhRevRiesgoPbc = $scope
                                                   	.formatoFechaDesdeISO($scope.cliente.fhRevRiesgoPbc);
                                                 },
                                                 function (error) {
                                                   $.unblockUI();
                                                   angular.element('#formularioContacto').modal("hide");
                                                   fErrorTxt("Ocurrió un error durante la petición de datos al modificar el contacto.",1);
                                                 }, {
                                                   "id" : $scope.clientesSeleccionados[0].id
                                                 });
                       }

                       $scope.mensajeAlert = "";
                       $scope.contactosSeleccionados = [];

                       $scope.modal.showCrear = false;
                       $scope.modal.showModificar = true;
                       $scope.modal.titulo = "Modificar Cliente";

                       angular.element('#formularios').modal({
                         backdrop : 'static'
                       });

                       if ($scope.isGrillaPorCliente()) {
                         $scope.paso = "formularioDatosCliente";
                       }
                       if ($scope.isGrillaPorContacto()) {
                         $scope.paso = "formularioDatosContacto";
                       }
                       if ($scope.isGrillaPorMIFID()) {
                         $scope.paso = "formularioDatosMIFID";
                       }

                       $(".modal-backdrop").remove();

                     }

                   };

                   $scope.vacio = function (valor) {
                     if (valor == null || $.trim(valor) == "" || valor === undefined) {
                       return true;
                     }
                     return false;
                   }

                   var isEntero = function (numero) {
                     if (isNaN(numero)) {
                       return false;
                     } else {
                       if (numero % 1 == 0) {
                         return true;
                       } else {
                         return false;
                       }
                     }
                   }

                   $scope.isValidoFormularioDatosCliente = function () {

                     if ($scope.vacio($scope.cliente.cif)) {
                       return false;
                     } else {/*
                               * if (!isEntero($scope.cliente.cif)) { //fErrorTxt("Ingrese un valor numérico para el Nro
                               * de documento", 1); return false; }
                               */
                     }
                     if ($scope.vacio($scope.cliente.cdbrocli))
                       return false;
                     if ($scope.vacio($scope.cliente.tpidenti))
                       return false;
                     if ($scope.vacio($scope.cliente.tpreside))
                       return false;
                     if ($scope.vacio($scope.cliente.tpclienn))
                       return false;
                     if ($scope.vacio($scope.cliente.tpfisjur))
                       return false;
                     if ($scope.vacio($scope.cliente.tpNacTit))
                       return false;
                     if ($scope.vacio($scope.cliente.cdNacTit))
                       return false;
                     if ($scope.vacio($scope.cliente.cdcat))
                       return false;
                     if ($scope.vacio($scope.cliente.cdkgr))
                       return false;
                     if ($scope.vacio($scope.cliente.cddeprev)) {
                       return false;
                     } else {
                       if ($scope.cliente.cddeprev == 'S') {
                         if ($scope.vacio($scope.cliente.motiPrev))
                           return false;
                       }
                     }
                     if ($scope.vacio($scope.cliente.tpManCat))
                       return false;
                     if ($scope.vacio($scope.cliente.ctFiscal))
                       return false;

                     if ($scope.vacio($scope.cliente.address)) {
                       return false;
                     } else {
                       var isEspana = $scope.vacio($scope.cliente.address.cdDePais);
                       if ($scope.vacio($scope.cliente.address.nbDomici))
                         return false;
                       if ($scope.vacio($scope.cliente.address.nuDomici))
                         return false;
                       if ($scope.vacio($scope.cliente.address.cdDePais))
                         return false;
                       if ($scope.vacio($scope.cliente.address.nbProvin))
                         return false;
                       if ($scope.vacio($scope.cliente.address.nbCiudad))
                         return false;
                       if ($scope.vacio($scope.cliente.address.cdPostal))
                         return false;
                     }

                     return true;

                   }

                   $scope.isValidoFormularioDatosContacto = function () {

                     if ($scope.cliente.contactos.length == 0) {
                       return false;
                     } else {
                       for (i = 0; i < $scope.cliente.contactos.length; i++) {
                         if ($scope.cliente.contactos[i].activo == "S") {
                           return true;
                         }
                       }
                     }
                     return false;

                   }

                   $scope.isValidoFormularioDatosMIFID = function () {
                     if ($scope.vacio($scope.cliente.mifid[0].category))
                       return false;
                     if ($scope.vacio($scope.cliente.mifid[0].tConven))
                       return false;
                     if ($scope.vacio($scope.cliente.mifid[0].tIdoneid))
                       return false;
                     // Fecha de conveniencia es obligatoria
                     if ($scope.vacio($scope.cliente.mifid[0].fhConv) && $scope.cliente.mifid[0].tConven != "No aplica")
                       return false;
                     if ($scope.vacio($scope.cliente.mifid[0].fhIdon) && $scope.cliente.mifid[0].tIdoneid != "N/A no aplica")
                       return false;
                     return true;
                   }

                   $scope.changeNaturalezaCliente = function () {
                     if (!$scope.isClienteTipoFisico()) {
                       $scope.cliente.fhNacimi = null;
                     }
                   }

                   $scope.changeControlPBC = function () {
                	   $scope.cliente.motiPrev = null;
                	   $scope.cliente.riesgoPbc = null;
                   };

                   $scope.isClienteTipoFisico = function () {
                     // Realizado de esta forma porque el acento afecta al condicional en el HTML. Ideal sería levantar
                     // constante con el valor.
                     if ($scope.cliente.tpfisjur != null && $scope.cliente.tpfisjur != "") {
                       return $scope.cliente.tpfisjur == "F";
                     }
                     return false;
                   }

                   $scope.validacionFormulario = function () {
                     console.log("cantidad contactos" + $scope.cliente.contactos.length);
                     return $scope.isValidoFormularioDatosCliente() && $scope.isValidoFormularioDatosContacto()
                            && $scope.isValidoFormularioDatosMIFID();
                   }

                   $scope.guardarCliente = function () {

                     $scope.cliente.userName = $rootScope.userName;
                     $scope.errores = [];
                     $scope.activeAlert = !$scope.validacionFormulario();
                     if (!$scope.activeAlert) {

                       inicializarLoading();
                       GestionClientesService.crearCliente(function (data) {

                         if (data.resultados == null) {
                           $.unblockUI();
                           fErrorTxt("Ocurrió un error durante la petición de datos al crear el cliente.", 1);
                           return;
                         }

                         if (data.resultados.errores != null && data.resultados.errores.length > 0) {
                           $.unblockUI();
                           $scope.errores = data.resultados.errores;
                         } else if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           $scope.busqueda();
                           // cargaClientes();
                           angular.element('#formularios').modal("hide");
                           fErrorTxt('Cliente ' + $scope.cliente.descli + ' creado correctamente', 3);
                         }

                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al crear el cliente.", 1);
                       }, $scope.cliente);
                     } else {
                       $scope.errores = [];
                       $scope.errores.push("Debe completar los campos obligatorios resaltados en pantalla");
                     }
                   };

                   $scope.modificarCliente = function () {

                     $scope.cliente.userName = $rootScope.userName;
                     $scope.errores = [];

                     $scope.activeAlert = !$scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       inicializarLoading();
                       GestionClientesService.modificarCliente(function (data) {

                         if (data.resultados == null) {
                           $.unblockUI();
                           fErrorTxt("Ocurrió un error durante la petición de datos al modificar el cliente.", 1);
                           return;
                         }

                         if (data.resultados.errores != null && data.resultados.errores.length > 0) {
                           $.unblockUI();
                           $scope.errores = data.resultados.errores;
                         } else if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           $scope.busqueda();
                           // cargaClientes();
                           angular.element('#formularios').modal("hide");
                           fErrorTxt('Cliente ' + $scope.cliente.descli + ' modificado correctamente', 3);
                         }
                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al modificar el cliente.", 1);
                       }, $scope.cliente);
                     } else {
                       $scope.errores = [];
                       $scope.errores.push("Debe completar los campos obligatorios resaltados en pantalla");
                     }

                   };

                   var getTextoErrores = function (errores) {
                     var texto = "";
                     for (var i = 0; i < errores.length; i++) {
                       texto += errores[i] + "<br>";
                     }
                     return texto;
                   }

                   /* FUNCIONALIDAD CRUD CONTACTO */

                   $scope.contactosSeleccionados = [];
                   $scope.modalContacto = {};
                   $scope.clientesContactoSeleccionados = [];
                   $scope.clientesContactoSeleccionadosBorrar = [];

                   // Abrir modal creacion clientes
                   $scope.abrirCrearContactoCliente = function () {

                     $scope.catalogos.direcciones = $scope.cliente.listadoDireccionesRelacionadas;
                     $scope.contactosSeleccionados = [];

                     // se inicializa el cliente
                     $scope.resetContactoCliente();
                     $scope.modalContacto.otraDireccion = false;
                     $scope.modalContacto.showConsultar = false;
                     $scope.modalContacto.showCrear = true;
                     $scope.modalContacto.showModificar = false;
                     $scope.modalContacto.isModificacionDirecta = false;
                     $scope.modalContacto.titulo = "Crear Contacto Cliente";
                     angular.element('#formularioContacto').modal({
                       backdrop : 'static'
                     });
                     $(".modal-backdrop").remove();

                   };

                   $scope.resetContactoCliente = function () {
                     $scope.contacto = {
                       nombre : null,
                       posicionCargo : null,
                       telefono1 : null,
                       cdEtrali : null,
                       email : null,
                       fax : null,
                       address : {
                         cdDePais : "011",

                       }
                     };
                     $scope.activeAlert = false;
                     $scope.mensajeAlert = "";
                   };

                   $scope.resetCliente();

                   // Abrir modal modificar clientes
                   $scope.abrirModificarContactoClienteDesdeGrillaPrincipal = function () {

                     $scope.catalogos.direcciones = $scope.cliente.listadoDireccionesRelacionadas;

                     if ($scope.clientesSeleccionados.length > 1 || $scope.clientesSeleccionados.length == 0) {
                       if ($scope.clientesSeleccionados.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un contacto elemento de la tabla.", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un contacto de la tabla.", 2)
                       }
                     } else {

                       $scope.mensajeAlert = "";

                       // se copian los datos del cliente seleccionado en la variable cliente
                       $scope.contacto = angular.copy($scope.clientesSeleccionados[0]);

                       // Obtengo el cliente asociado
                       inicializarLoading();
                       GestionClientesService.recuperarCliente(function (data) {

                         if (data.resultados == null) {
                           $.unblockUI();
                           fErrorTxt("Ocurrió un error durante la petición de datos al modificar el contacto.", 1);
                           return;
                         }
                         $scope.cliente.listadoDireccionesRelacionadas = [ $scope.contacto.address ];

                         $scope.modalContacto.otraDireccion = false;
                         $scope.modalContacto.showCrear = false;
                         $scope.modalContacto.showModificar = ($scope.modalContacto.showConsultar) ? false : true;
                         $scope.modalContacto.titulo = "Modificar Contacto";
                         if ($scope.modalContacto.showConsultar) {
                           $scope.modalContacto.titulo = "Consultar Contacto";
                         }
                         $scope.modalContacto.isModificacionDirecta = true;
                         angular.element('#formularioContacto').modal({
                           backdrop : 'static'
                         });
                         $(".modal-backdrop").remove();
                         $.unblockUI();

                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularioContacto').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al modificar el contacto.", 1);
                       }, {
                         "id" : $scope.contacto.idCliente
                       });

                     }

                   };

                   // Abrir modal modificar clientes
                   $scope.abrirModificarContactoCliente = function () {

                     $scope.modalContacto.isModificacionDirecta = false;
                     $scope.modalContacto.showConsultar = false;

                     $scope.catalogos.direcciones = $scope.cliente.listadoDireccionesRelacionadas;

                     if ($scope.contactosSeleccionados.length > 1 || $scope.contactosSeleccionados.length == 0) {
                       if ($scope.contactosSeleccionados.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de contactos.", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de contactos.", 2)
                       }
                     } else {
                       $scope.mensajeAlert = "";
                       // se copian los datos del cliente seleccionado en la variable cliente
                       $scope.contacto = angular.copy($scope.contactosSeleccionados[0]);
                       $scope.contacto.address = $scope.contactosSeleccionados[0].address;

                       if (isMismaAddress($scope.contacto.address, $scope.cliente.address)) {
                         $scope.contacto.address = $scope.cliente.address;
                       }

                       $scope.modalContacto.otraDireccion = false;
                       $scope.modalContacto.showConsultar = false;
                       $scope.modalContacto.showCrear = false;
                       $scope.modalContacto.showModificar = true;
                       $scope.modalContacto.titulo = "Modificar Contacto";
                       $scope.modalContacto.isModificacionDirecta = false;
                       angular.element('#formularioContacto').modal({
                         backdrop : 'static'
                       });
                       $(".modal-backdrop").remove();

                     }

                   };

                   var isMismaAddress = function (ad1, ad2) {
                     var is = true;
                     if (ad1 != null && ad2 != null) {
                       for ( var key in ad1) {
                         // if (ad2[key] === undefined) is = false;
                         if (ad1[key] !== ad2[key]) {
                           is = false;
                         }
                       }
                     }
                     return is;
                   }

                   $scope.isFormularioContactoValido = function () {

                     if ($scope.vacio($scope.contacto.nombre))
                       return false;
                     if ($scope.vacio($scope.contacto.apellido1))
                       return false;
                     if ($scope.vacio($scope.contacto.apellido2))
                       return false;
                     if ($scope.vacio($scope.contacto.posicionCargo))
                       return false;
                     if ($scope.vacio($scope.contacto.telefono1))
                       return false;
                     if ($scope.vacio($scope.contacto.email))
                       return false;

                     if ($scope.vacio($scope.contacto.address) || $scope.vacio($scope.contacto.address.nbDomici)
                         || $scope.vacio($scope.contacto.address.nuDomici)) {
                       fErrorTxt("Debe especificar una dirección para el contacto.", 2);
                       return false;
                     }

                     return true;

                   }

                   $scope.guardarContactoCliente = function () {

                     $scope.activeAlert = !$scope.isFormularioContactoValido();
                     if (!$scope.activeAlert) {

                       for (var i = 0; i < $scope.contactosSeleccionados.length; i++) {
                         var contacto = $scope.contactosSeleccionados[i];
                         var idx = $scope.cliente.contactos.indexOf(contacto);
                         if (idx > -1) {
                           $scope.cliente.contactos.splice(idx, 1);
                         }
                       }
                       $scope.contacto.activo = 'S';
                       $scope.cliente.contactos.push($scope.contacto);
                       angular.element('#formularioContacto').modal("hide");
                       $scope.pintarTablaContactoClientes();
                       $scope.contactosSeleccionados = [];

                     }

                   };

                   $scope.borrarContactoSeleccionado = function () {

                     if ($scope.contactosSeleccionados.length > 1 || $scope.contactosSeleccionados.length == 0) {
                       if ($scope.contactosSeleccionados.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de contactos.", 2)
                       }
                     }
                     for (var i = 0; i < $scope.contactosSeleccionados.length; i++) {
                       var contacto = $scope.contactosSeleccionados[i];
                       var idx = $scope.cliente.contactos.indexOf(contacto);
                       if (idx > -1) {
                         console.log("marcando contacto como no activo");
                         var elcontacto = $scope.cliente.contactos[idx];
                         contacto.activo = "N";
                         elcontacto.activo = "N";
                         $scope.cliente.contactos[idx].activo = "N";
                         console.log($scope.cliente.contactos[idx]);
                       }
                     }

                   };

                   $scope.modificarContactoCliente = function () {

                     $scope.activeAlert = !$scope.isFormularioContactoValido();
                     if (!$scope.activeAlert) {
                       inicializarLoading();
                       GestionClientesService.modificarContactoCliente(function (data) {

                         if (data.resultados == null) {
                           $.unblockUI();
                           fErrorTxt("Ocurrió un error durante la petición de datos al modificar el contacto.", 1);
                           return;
                         }

                         if (data.resultados.errores != null && data.resultados.errores.length > 0) {
                           $.unblockUI();
                           fErrorTxt(getTextoErrores(data.resultados.errores), 1);
                         } else if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           $.unblockUI();
                           $scope.busqueda();
                           angular.element('#formularioContacto').modal("hide");
                           fErrorTxt('Contacto ' + $scope.contacto.nombre + ' modificado correctamente', 3);
                         }
                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularioContacto').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al modificar el contacto.", 1);
                       }, $scope.contacto);
                     }
                     $scope.clientesSeleccionados = [];

                   };

                   // toggle selection for a given fruit by name
                   $scope.toggleSelectionContacto = function toggleSelection (contacto) {
                     var idx = $scope.contactosSeleccionados.indexOf(contacto);
                     // is currently selected
                     if (idx > -1) {
                       $scope.contactosSeleccionados.splice(idx, 1);
                     }
                     // is newly selected
                     else {
                       $scope.contactosSeleccionados.push(contacto);
                     }
                   };

                   $scope.seleccionarTodosContacto = function () {
                     $scope.contactosSeleccionados = [];
                     for (var i = 0; i < $scope.cliente.contactos.length; i++) {
                       $scope.contactosSeleccionados.push($scope.cliente.contactos[i]);
                     }
                   };

                   $scope.deseleccionarTodosContacto = function () {
                     $scope.contactosSeleccionados = [];
                   };

                   $scope.changeNuevaDireccion = function () {
                     $scope.contacto.address = {};
                   };

                   // Hace la llamada a la generación de informe en excel
                   $scope.exportarClientesExcel = function (opcion) {

                     inicializarLoading();
                     if (opcion == "todos") {
                       GestionClientesService.exportarClientesExcel(function (data) {
                         onSuccessExportClientes(data);
                       }, function (error) {
                         $.unblockUI();
                       }, {});
                     } else {
                       GestionClientesService.exportarClientesExcel(function (data) {
                         onSuccessExportClientes(data);
                       }, function (error) {
                         $.unblockUI();
                       }, $scope.getFiltro());
                     }

                   };

                   function onSuccessExportClientes (json) {

                     $.unblockUI();

                     if (json.resultados.ExcelClientes !== undefined) {

                       var ExcelBytes = atob([ json.resultados.ExcelClientes ]);

                       var byteNumbers = new Array(ExcelBytes.length);
                       for (var i = 0; i < ExcelBytes.length; i++) {
                         byteNumbers[i] = ExcelBytes.charCodeAt(i);
                       }

                       
                       
                       var byteArray = new Uint8Array(byteNumbers);

                       var blob = new Blob([ byteArray ], {
                         type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                       });

                       var nav = navigator.userAgent.toLowerCase();

                       if (navigator.msSaveBlob) {
                         navigator.msSaveBlob(blob, "Listado_Clientes_" + hoy + ".xlsx");
                       } else {
                         var blobUrl = URL.createObjectURL(blob);
                         var link = document.createElement('a');
                         link.href = blobUrl = URL.createObjectURL(blob);
                         link.download = "Listado_Clientes_" + hoy + ".xlsx";
                         document.body.appendChild(link);
                         link.click();
                         document.body.removeChild(link);
                       }

                     } else {
                       fErrorTxt("Se produjo un error en la generacion del fichero Excel. Contacte con el Administrador.", 1);
                     }
                   }

                   $scope.pintarTablaContactoClientes = function () {

                   };

                   /* HISTORICO */

                   var oTableHistorico = $("#datosHistorico").css('width', '100%').dataTable({
                     "dom" : 'T<"clear">lfrtip',
                     "tableTools" : {
                       "sSwfPath" : "",
                       "aButtons" : []
                     },
                     "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                       $compile(nRow)($scope);
                     },
                     "aoColumns" : [ {
                       width : "16%"
                     }, {
                       width : "16%"
                     }, {
                       width : "16%"
                     }, {
                       width : "16%"
                     }, {
                       width : "20%"
                     }, {
                       width : "16%"
                     } ],
                     "scrollY" : "300px",
                     "scrollX" : "100%",
                     "autoWidth" : false,
                     "scrollCollapse" : true,
                     "language" : {
                       "url" : "i18n/Spanish.json"
                     }
                   });

                   $scope.pintarTablaHistorico = function (data) {

                     if (data == null || data === "undefined") {
                       fErrorTxt("Se produjo un error en la carga del listado.", 1);
                       return false;
                     }

                     if (typeof data === 'string') {
                       fErrorTxt("Se produjo un error en la carga del listado.", 1);
                       return false;
                     }

                     $scope.listadoHistorico = data.resultados.listadoHistoricoCliente;

                     // borra el contenido del body de la tabla
                     var tbl = $("#datosHistorico > tbody");
                     $(tbl).html("");
                     oTableHistorico.fnClearTable();

                     var list = [];

                     if ($scope.listadoHistorico.length > 0) {

                       for (var i = 0; i < $scope.listadoHistorico.length; i++) {

                         list[i] = [ $scope.listadoHistorico[i].tpdata, $scope.listadoHistorico[i].data,
                                    $scope.listadoHistorico[i].valueprev, $scope.listadoHistorico[i].valuenew,
                                    $scope.listadoHistorico[i].fechaModificacion, $scope.listadoHistorico[i].usumod ];

                       }

                       oTableHistorico.fnAddData(list, false);
                       oTableHistorico.fnDraw();

                       $("#datosHistorico").css("width", "100%");
                       oTableHistorico.DataTable().columns.adjust();

                       $.unblockUI();

                     }

                   };

                   $scope.filtroHistorico = {
                     id : null,
                     fechaDesde : null,
                     tipoDato : null
                   };

                   $scope.resetFiltroHistorico = function () {
                     var id = $scope.filtroHistorico.id;
                     $scope.filtroHistorico = {
                       id : id,
                       fechaDesde : null,
                       tipoDato : null
                     };
                   }

                   $scope.abrirConsultarHistoricoCliente = function () {

                     if ($scope.clientesSeleccionados.length > 1 || $scope.clientesSeleccionados.length == 0) {
                       if ($scope.clientesSeleccionados.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla.", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla.", 2)
                       }
                     } else {

                       $scope.filtroHistorico = {};
                       $scope.filtroHistorico.id = $scope.clientesSeleccionados[0].id;

                       $scope.busquedaHistorico();
                       angular.element('#historico').modal({
                         backdrop : 'static'
                       });

                     }

                   };

                   // Carga del listado principal
                   $scope.busquedaHistorico = function () {

                     // borra el contenido del body de la tabla
                     var tbl = $("#datosHistorico > tbody");
                     $(tbl).html("");
                     oTableHistorico.fnClearTable();

                     // Se muestra la capa cargando
                     inicializarLoading();

                     if ($scope.filtroHistorico.tipoDato == "")
                       $scope.filtroHistorico.tipoDato = null;

                     GestionClientesService.verHistoricoCliente(function (data) {

                       if (data.resultados == null) {
                         fErrorTxt("Ocurrió un error durante la petición de datos al ver histórico de cliente.", 1);
                         return;
                       }
                       $.unblockUI();

                       $scope.pintarTablaHistorico(data);

                     }, function (error) {
                       $.unblockUI();
                       fErrorTxt("Ocurrió un error durante la petición de datos al ver histórico de cliente.", 1);
                     }, $scope.filtroHistorico);

                   };

                 } ]);
