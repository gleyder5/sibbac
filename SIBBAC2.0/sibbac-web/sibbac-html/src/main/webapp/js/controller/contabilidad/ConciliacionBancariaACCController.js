sibbac20.controller('ConciliacionBancariaACCController',
		['$rootScope',
		 '$scope',
		 '$compile', 
		 'ConciliacionBancariaACCService', 
		 'ConciliacionBancariaCombosService',
		 '$document',
		 'growl',
		 'SecurityService',
		 function($rootScope, $scope, $compile, ConciliacionBancariaACCService, 
				 ConciliacionBancariaCombosService, $document, growl, SecurityService) {
			
			SecurityService.redirectToLoginIfNotLoggedIn();
	
		$scope.titulo = "Asignación Cuenta Contable";
		
		$scope.safeApply = function(fn) {
			var phase = this.$root.$$phase;
			if (phase === '$apply' || phase === '$digest') {
				if (fn && (typeof (fn) === 'function')) {
					fn();
				}
			} else {
				this.$apply(fn);
			}
		};
        
		var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth() + 1;
        var yyyy = hoy.getFullYear();
        hoy = yyyy + "_" + mm + "_" + dd;
        
		 $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col ) {
			  return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
			    return $('input', td).prop('checked') ? '1' : '0';
			  })
			};

		var abreACC = function(event, importe, apuntesFromConciliacionBancaria){
		
			$scope.importeTotalApuntes = 0;
			$scope.importeTotalPendientes = importe;
			$scope.importePendiente = importe;
			$scope.importePendiente2 = importe;
			$scope.importePendiente2 = $.number($scope.importePendiente2, 2, ',', '.');
			$scope.resetApunte();
			$scope.listaApuntes = [];
			$scope.apuntesSeleccionados = [];
			$scope.apuntesSeleccionadosBorrar = [];
			$scope.apuntesFromConciliacionBancaria = apuntesFromConciliacionBancaria;
			if ($scope.oTableACC !== undefined && $scope.oTableACC !== null) {
				$scope.oTableACC.fnDestroy();
			}
			$scope.oTableACC = $("#datosACC").dataTable({
					"dom" : 'T<"clear">lfrtip',
					"tableTools" : {
						"aButtons" : [ "print" ]
					},
					"aoColumns" : [ {sClass : "centrar", width : "30px", orderDataType: "dom-checkbox"},{width : "220px"}, {width : "220px"}, {width : "220px",sClass : "monedaR",	type : "formatted-num"}, {width : "220px"}],
					"fnCreatedRow" : function(nRow, aData, iDataIndex) {
						$compile(nRow)($scope);
					},
					"scrollX" : "100%",
					"scrollCollapse" : true,
					"language" : {
						"url" : "i18n/Spanish.json"
					}
			});
					
			$scope.consultarCuentasContablesACC();
			$scope.consultarPlantillasTheir();
			$scope.consultarConceptosACC();
			$scope.consultarAuxiliaresACC();
			$scope.recargarTablaApuntes();
		};
		
		$scope.$on("abreACC", abreACC);
		
		$scope.auxiliarVisible = false;
		$scope.$watch('apunte.cuenta', function (newVal, oldVal) {
  			if (newVal !== oldVal && newVal) {
  				for(var i=0;i<$scope.listaCuentasACC.length;i++){
  					if($scope.listaCuentasACC[i].key == $scope.apunte.cuenta && $scope.listaCuentasACC[i].description == "true"){
  						$scope.auxiliarVisible = true;
  						break;
  					}else{
  						$scope.auxiliarVisible = false;
  					}
  				}
  			}
		});
		
		$scope.calculaImporteTotalApuntes = function(operacion){
    		$scope.importeTotalApuntes = 0;
			for(var i=0;i<$scope.listaApuntes.length;i++){
				$scope.importeTotalApuntes = $scope.importeTotalApuntes + $scope.listaApuntes[i].importe;
			}
			$scope.importePendiente = $scope.importeTotalPendientes - $scope.importeTotalApuntes;
			$scope.importePendiente2 = $.number($scope.importePendiente, 2, ',', '.');
	    };
		
		var calculaImporteACC = function(event, importe){
			$scope.importeTotalPendientes = importe;
			$scope.importePendiente = $scope.importeTotalPendientes - $scope.importeTotalApuntes;
			$scope.importePendiente2 = $.number($scope.importePendiente, 2, ',', '.');
			$scope.apunte.importe = $scope.importePendiente2;
		};
		
		$scope.$on("calculaImporteACC", calculaImporteACC);
		
		$scope.listaCuentasACC = [];
		$scope.listaConceptosACC = [];
		$scope.listaAuxiliaresACC = [];
		$scope.listaPlantillasTheir = [];
		
		$scope.resetApunte = function(){
			$scope.apunte = {
				cuenta: "",
				tolerance: 0.0,
				conceptoSelect: "",
				importe: $.number($scope.importePendiente+"", 2, ',', '.'),
				auxiliar: "",
				concepto: ""
			};
			$scope.activeAlert = false;
		};
		
		$scope.resetApunte();
		
		$scope.consultarCuentasContablesACC = function () {
			ConciliacionBancariaCombosService.consultarCuentasContables(function (data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo de Cuentas Contables", 1);
				} else {
					$scope.listaCuentasACC = data.resultados["listaCuentasContables"];
				}
			}, function (error) {
				fErrorTxt("Se produjo un error en la carga del combo de Cuentas Contables", 1);
			});
		};
		
		/**
		 * Method that retrieves all the templates with codigo like 'THEIR_%'
		 */
		$scope.consultarPlantillasTheir = function () {
			ConciliacionBancariaCombosService.consultarPlantillasTheir(function (data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga de la lista de Plantillas Contables THEIR", 1);
				} else {
					$scope.listaPlantillasTheir = data.resultados["listaCodigosPlantillasTheir"];
				}
			}, function (error) {
				fErrorTxt("Se produjo un error en la carga de las plantillas their", 1);
			});
		};		

		$scope.consultarConceptosACC = function () {
			ConciliacionBancariaCombosService.conceptos(function (data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo de Conceptos", 1);
				} else {
					$scope.listaConceptosACC = data.resultados["listaConceptos"];
				}
			}, function (error) {
				fErrorTxt("Se produjo un error en la carga del combo de Conceptos", 1);
			});
		};

		$scope.consultarAuxiliaresACC = function(){
			ConciliacionBancariaCombosService.consultarAuxiliares(function(data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo de Auxiliares", 1);
				} else {
					$scope.listaAuxiliaresACC = data.resultados.listaAuxiliaresBancario;
				}
			},
			function(error) {
				fErrorTxt("Se produjo un error en la carga del combo de Auxiliares",1);
			});
		};

		
		$scope.recargarTablaApuntes = function (){
			
			var tbl = $("#datosACC > tbody");
			$(tbl).html("");
			$scope.oTableACC.fnClearTable();

			if ($scope.listaApuntes.length != 0) {

				for (var i = 0; i < $scope.listaApuntes.length; i++) {

					var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listaApuntes['
							+ i
							+ '].selected" ng-click="seleccionarApunte('
							+ i + ');"/>';

					var fila = [
							check,
							$scope.listaApuntes[i].cuenta,
							$scope.listaApuntes[i].concepto,
							$.number($scope.listaApuntes[i].importe, 2, ',', '.'),
							$scope.listaApuntes[i].auxiliar];

					$scope.oTableACC.fnAddData(fila, false);

				}

			}
			$scope.oTableACC.fnDraw();
			$.unblockUI();
		};
		
		$scope.seleccionarApunte = function(row) {
			if ($scope.listaApuntes[row] != null
					&& $scope.listaApuntes[row].selected) {
				$scope.listaApuntes[row].selected = false;
				for (var i = 0; i < $scope.seleccionados.length; i++) {
					if ($scope.apuntesSeleccionados[i] === $scope.listaApuntes[row]) {
						$scope.apuntesSeleccionados.splice(i, 1);
						$scope.apuntesSeleccionadosBorrar.splice(i, 1);
					}
				}
			} else {
				$scope.listaApuntes[row].selected = true;
				$scope.apuntesSeleccionados.push($scope.listaApuntes[row]);
				$scope.apuntesSeleccionadosBorrar.push($scope.listaApuntes[row]);
			}
		};
			
		$scope.anadirApunte = function(){
			
			$scope.setToleranceFromPlantilla();
			$scope.activeAlert = true;
			$scope.importePendiente = Math.round($scope.importePendiente*100)/100;

			var importeFloat = parseFloat($scope.apunte.importe.replace(/\./g, "").replace(/,/g, '.'));
			$scope.importeFloatAbs = Math.abs($scope.importePendiente);

			if(importeFloat <= $scope.importePendiente && importeFloat > 0 && $scope.apunte.cuenta && $scope.apunte.importe && $scope.apunte.concepto && (($scope.apunte.auxiliar && $scope.auxiliarVisible) || (!$scope.auxiliarVisible))){
				if($scope.apunte.tolerance != null &&
					$scope.apunte.tolerance != undefined &&
					$scope.apunte.tolerance < $scope.importeFloatAbs &&
					$scope.apunte.cuenta == "4021102"){
					
					fErrorTxt("Asignación a P&G Cta 4021102 supera el tolerance de "+$scope.apunte.tolerance+"€. Obtenga conformidad. ", 2);
					$.unblockUI();				
				}
				$scope.apunte.importe = importeFloat;
				$scope.listaApuntes.push($scope.apunte);
				$scope.calculaImporteTotalApuntes();
				$scope.recargarTablaApuntes();
				$scope.resetApunte();
			}
		};	
		
		$scope.setToleranceFromPlantilla = function(){
			if($scope.apuntesFromConciliacionBancaria && $scope.apunte.cuenta == "4021102"){
				var first = $scope.apuntesFromConciliacionBancaria[0].auxiliarBancario;
				var tolerance = 0.0;
				for ( var plantilla in $scope.listaPlantillasTheir) {
					var tipo = "";
					var item = $scope.listaPlantillasTheir[plantilla];

					if(item.codigo == 'THEIR_' + first){
						tolerance = item.tolerance;
						break;
					}
				}
				$scope.apunte.tolerance = tolerance;
			}
		} 		
		
		var eliminarApunte = function(){
            if ($scope.apuntesSeleccionadosBorrar.length > 0) {
                  for (var i = 0; i < $scope.apuntesSeleccionadosBorrar.length; i++) {
                	  for (var j = 0; j < $scope.listaApuntes.length; j++) {
                		  if ($scope.listaApuntes[j] === $scope.apuntesSeleccionadosBorrar[i]) {
                			  $scope.listaApuntes.splice(j, 1);
                			  $scope.calculaImporteTotalApuntes();
                			  $scope.safeApply();
                    	  }
                	  }
                  }
                  $scope.apuntesSeleccionadosBorrar = [];
                  $scope.recargarTablaApuntes();
                  $scope.apunte.importe = $scope.importePendiente2;
            } else {
            	fErrorTxt("Debe seleccionar al menos un elemento de la tabla de Apuntes.", 2);
            }
        };
        
        $scope.$on("eliminarApunteACC", eliminarApunte);
        
	    $scope.asignarValueInputACC = function(){
	    	$scope.apunte.concepto = $scope.apunte.conceptoSelect.value;
	    };
	    
	    $scope.resetConceptoACC = function(){
	    	$scope.apunte.conceptoSelect = "";
	    };
	    
		var generarApunteACC  = function(event, listSeleccionadosIzq){
			$scope.importePendiente = Math.round($scope.importePendiente*100)/100;
    		if(listSeleccionadosIzq.length>0 && $scope.listaApuntes.length>0 && $scope.importePendiente <= 0.009){
   			 angular.element("#dialog-confirm").html("¿Esta seguro de que desea realizar el apunte contable?");
			 angular.element("#dialog-confirm").dialog({
				 resizable : false,
				 modal : true,
				 title : "Mensaje de Confirmación",
				 height : 150,
				 width : 360,
				 buttons : {
					 " Sí " : function () {
						 inicializarLoading();
						 ConciliacionBancariaACCService.generarApuntes(function (data) {
								if(data.arrayData[0].status == 'OK' && data.arrayData[0].status == 'OK'){
									fErrorTxt("Los apuntes se generaron correctamente.", 3);
									$scope.$emit("resetPantalla");
								}															 	
								if(data.arrayData[0].status == 'KO' && data.arrayData[0].status == 'OK'){
									fErrorTxt("Error en la peticion de datos al generar apuntes.", 1);
								}	
								if(data.arrayData[0].status == 'OK' && data.arrayData[0].status == 'KO'){
									fErrorTxt("Error en la generacion de apuntes pendientes.", 1);
								}																		
								if(data.arrayData[0].status == 'KO' && data.arrayData[0].status == 'KO'){
									fErrorTxt("Error en la peticion de datos al generar apuntes y al generar apuntes pendientes.", 1);
								}																															 	
							    /*if (data.resultados.status === 'KO') {
			  						fErrorTxt("Error en la peticion de datos al generar apuntes.", 1);
			  					}else if (data.resultados.status === 'KOSemaforo') {
			  						fErrorTxt(data.error, 1);
			  					}else{
			        				fErrorTxt("Los apuntes se generaron correctamente.", 3);
			        				$scope.$emit("resetPantalla");
			        			}*/
			        			$.unblockUI();
			    	    	}, function (error) {
			    	    		fErrorTxt("Se produjo un error al generar los apuntes en el fichero XLS.", 1);
			    	    		$.unblockUI();
			    	    	}, {listaPartidasTLM: listSeleccionadosIzq, listaApuntes: $scope.listaApuntes, auditUser : $rootScope.userName});
						 $(this).dialog('close');
					 }," No " : function () {$(this).dialog('close');}
				 }
            });
			 $('.ui-dialog-titlebar-close').remove();
        	}else{
        		fErrorTxt("Para poder generar el apunte el importe pendiente debe de ser igual a cero.", 2);
        	}
        };
        
        $scope.$on("generarApunteACC", generarApunteACC);
        
} ]);