'use strict';
sibbac20.controller('PropiaTitulosController', ['$scope', '$compile', 'growl', 'IsinService', 'PropiaService', '$document', function ($scope, $compile, growl, IsinService, PropiaService, $document) {

	$scope.mensajeBusqueda = '';
        /** ISINES **/
	 var availableTags = [];
	 var SHOW_LOG = false;

	 function cargarIsines(datosIsin) {
			availableTags = [];
			angular.forEach(datosIsin, function(val, key) {
				var ponerTag = datosIsin[key].codigo
						+ " - "
						+ datosIsin[key].descripcion;
				availableTags.push(ponerTag);
			});
			angular.element("#cdisin").autocomplete({
				source : availableTags
			});
		}

	 function loadIsines() {
		IsinService.getIsines(cargarIsines,
				showError);
	 }

        var oTable = undefined;
        //var isLoading = false;
        var refreshDataListener = 0;
        var REFRESH = 60000;	// Segundos de refresco de la pagina
        //var isLoadingRequired = true;
        var isLoadingRequired = true;
        var ultimoJson = {};



        /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
        if ($('.contenedorTabla').length >= 1) {
            var anchoBotonera;
            $('.contenedorTabla').each(function (i) {
                anchoBotonera = $(this).find('table').outerWidth();
                $(this).find('.botonera').css('width', anchoBotonera + 'px');
                $(this).find('.resumen').css('width', anchoBotonera + 'px');
                $('.resultados').hide();
            });
        }

        function showError(data, status, headers, config) {
        	if (isLoadingRequired) {
                $.unblockUI();
            }
            growl.addErrorMessage("Error: " + data);
        }

        function successGetAnotacioneseccByTipoCuenta(json, status, headers, config){
    		if ( SHOW_LOG ) console.log( "Listar anotaciones propia ..." );
    		var datos = {};
    		if (json !== undefined &&
    	        json.resultados !== undefined &&
    	        json.resultados !== null ) {
    	        if (json.error !== null) {
    	           growl.addErrorMessage(json.error);
    	        } else {
    	        	if (JSON.stringify(ultimoJson) !== JSON.stringify(json) || isLoadingRequired) {
                        console.log("La peticion actual es diferente a la ultima realizada");
                        var difClass = '';
                        var colDif = '';
                        var rownum = 0;
                        oTable.fnClearTable();
                        datos = json.resultados.result_cuenta_propia;
                        angular.forEach(datos, function (val, key) {
                            if (datos[key].imprecio !== null && datos[key].imprecio >= 0 && datos[key].nutitulos !== null && datos[key].nutitulos !== 0 && datos[key].cdCuenta !== "") {

                                //fila tabla izquierda
                                colDif = (datos[key].nutitulos);
                                var difClass = (colDif > 0) ? 'colorFondo' : 'colorRojo';
                                var titulos = $.number(datos[key].nutitulos, 0, ',', '.');
                                var descIsin = (datos[key].isinDescripcion === null) ? "" : datos[key].isinDescripcion;
                                oTable.fnAddData([
                                    datos[key].cdisin,
                                    descIsin,
                                    datos[key].cdCuenta,
                                    a6digitos(datos[key].imprecio),
                                    titulos,
                                    a2digitos(datos[key].imefectivo)
                                ], false);
                                angular.element(oTable.fnGetNodes(rownum)).addClass(difClass);
                                rownum++;
                            }

                        });
                        oTable.fnDraw();
                        growl.addInfoMessage("listadas todas las anotaciones.");
                    } else {
                        console.log("La peticion actual es igual a la ultima realizada");
                    }
                    ultimoJson = json;

    			}
    	    }
    		else
    		{
    			if (json !== undefined && json.error !== null) {
    			   if ( SHOW_LOG ) console.log( "Error listando anotaciones propia ..." );
    		       growl.addErrorMessage(json.error);
    		    }
    		}
            $.unblockUI();
    		isLoadingRequired = false;
    	};

    	function getIsin() {
            var isinCompleto = angular.element('#cdisin').val();

            if (isinCompleto !== undefined )
            {
            	inicializarLoading();
            	var guion = isinCompleto.indexOf("-");
            	return (guion != -1) ? isinCompleto.substring(0, guion) : isinCompleto;
            }

        }

    	//rellena los filtros
        function obtenerFiltros() {
            return {cdisin: getIsin()};
        }

        function obtenerDatosPeriodicamente (){

        	var filter = obtenerFiltros();
        	PropiaService.getAnotacioneseccByTipoCuenta(successGetAnotacioneseccByTipoCuenta, showError, filter);
        	if (refreshDataListener != 0) {
                clearTimeout(refreshDataListener);
            }
            var refresco = parseInt(REFRESH) * 1000;
            refreshDataListener = setTimeout(obtenerDatosPeriodicamente, refresco);
        }


        /////////////////////////////////////////////////
        //////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
        function initialize_propiaTitulos() {
            //cargaIsin();
        }

        function initializaTable() {

        	oTable = $("#tblCtaPropia").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "deferRender": true,
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "aoColumns": [
                    {sClass: "align-left"},
                    {sClass: "align-left"},
                    {sClass: "centrar"},
                    {sClass: "monedaR", type: "formatted-num"},
                    {sClass: "monedaR", type: "formatted-num"},
                    {sClass: "monedaR", type: "formatted-num"}
                ],
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    $compile(nRow)($scope);
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "filter": false
            });
    	}

        $(document).ready(function () {
        	initializaTable();
        	loadIsines();

            /*Eva:Función para cambiar texto*/
            jQuery.fn.extend({
                toggleText: function (a, b) {
                    var that = this;
                    if (that.text() !== a && that.text() !== b) {
                        that.text(a);
                    } else if (that.text() === a) {
                        that.text(b);
                    } else if (that.text() === b) {
                        that.text(a);
                    }
                    return this;
                }
            });
            /*FIN Función para cambiar texto*/

            $('a[href="#release-history"]').toggle(function () {
                $('#release-wrapper').animate({
                    marginTop: '0px'
                }, 600, 'linear');
            }, function () {
                $('#release-wrapper').animate({
                    marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
                }, 600, 'linear');
            });

            $('#download a').mousedown(function () {
                _gaq.push(['_trackEvent', 'download-button', 'clicked'])
            });

            $('.collapser').click(function (event) {
                event.preventDefault();
                $(this).parents('.title_section').next().slideToggle();
                $(this).toggleClass('active');
                $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

                return false;
            });
            $('.collapser_search').click(function (event) {
                event.preventDefault();
                $(this).parents('.title_section').next().slideToggle();
                $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                $(this).toggleClass('active');
                if ($(this).text() == "Ocultar opciones de búsqueda") {
                    $(this).text("Mostrar opciones de búsqueda");
                } else {
                    $(this).text("Ocultar opciones de búsqueda");
                }
                return false;
            });

        });

        $scope.limpiarFiltro = function() {
            angular.element('input[type=text]').val('');
            angular.element('select').val('');
        };

        //para ver el filtro al minimizar la búsqueda
        function seguimientoBusqueda() {
        	$scope.mensajeBusqueda = '';
            var cadenaFiltros = "";
            if ($('#cdisin').val() !== "" && $('#cdisin').val() !== undefined) {
            	$scope.mensajeBusqueda += " Isin: " + angular.element('#cdisin').val();
            }
        }

        $scope.buscar = function() {
        	isLoadingRequired = true;
            $("#tblCtaPropia > tbody").empty();
            //$("#tblCtaPropia").trigger("update");
            obtenerDatosPeriodicamente();

            $('.collapser_search').parents('.title_section').next().slideToggle();
            $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
            $('.collapser_search').toggleClass('active');
            if ($('.collapser_search').text() == "Ocultar opciones de búsqueda") {
                $('.collapser_search').text("Mostrar opciones de búsqueda");
            } else {
                $('.collapser_search').text("Ocultar opciones de búsqueda");
            }
            seguimientoBusqueda();
            return false;
        }
        function render() {
            //
        }




        //exportar
        function SendAsExport(format) {
            var c = this.document.forms['propiaTitulos'];
            var f = this.document.forms["export"];
            f.action = "/sibbac20back/rest/export." + format;
            var json = {
                "service": "SIBBACServiceConciliacionPropia",
                "action": "getAnotacioneseccByTipoCuentaExport",
                "filters": {"cdisin": getIsin()}};
            if (json != null && json != undefined && json.length == 0) {
                alert("Introduzca datos");
            } else {
                f.webRequest.value = JSON.stringify(json);
                f.submit();
            }
        }


        /** END JS **/
    }]);
