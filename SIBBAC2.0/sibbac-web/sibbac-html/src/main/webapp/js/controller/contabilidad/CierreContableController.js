'use strict';
sibbac20.controller('CierreContableController',['$scope','$document','growl','ContaInformesService','IsinService','$compile', '$rootScope', 'SecurityService', '$location',
function($scope, $document, growl, ContaInformesService, IsinService, $compile, $rootScope, SecurityService, $location) {


    if (SecurityService.inicializated) {
 	   if (!SecurityService.isPermisoAccion($rootScope.SecurityActions.CREAR, $location.path())) {
           growl.addErrorMessage("No tiene permiso para acceder a esta funcionalidad");
           $location.path("/");
 	   }
    } else {
        $scope.$watch(security.event.initialized, function() {
           if (!SecurityService.isPermisoAccion($rootScope.SecurityActions.CREAR, $location.path())) {
                 growl.addErrorMessage("No tiene permiso para acceder a esta funcionalidad");
                 $location.path("/");
       	   }
        });
    }

    $(function () {
    	$("#ui-datepicker-div").remove();
    	$("input#fechaCierre").datepicker();
    	$("input#fechaCierre").prop('required', true);
    })

    $("input#guardar").click(function(event) {
    	if ($('input#fechaCierre').val()=="") {
    		mensajeError("Tienes que meter un valor en 'Fecha de Cierre'");
    		return;
    	}
    	inicializarLoading();
    	var params = {
    			"service": "SIBBACServiceCierreContable",
    			"action": "update",
    			"filters": {
    				"fechaCierre":$("input#fechaCierre").val()
    			}};
    	$.ajax({
    		type: 'POST',
    		dataType: "json",
    		contentType: "application/json; charset=utf-8",
    		url: "/sibbac20back/rest/service",
    		data: JSON.stringify(params),
    		beforeSend: function (x) {
    			if (x && x.overrideMimeType) {
    				x.overrideMimeType("application/json");
    			}
    			// CORS Related
    			x.setRequestHeader("Accept", "application/json");
    			x.setRequestHeader("Content-Type", "application/json");
    			x.setRequestHeader("X-Requested-With", "HandMade");
    			x.setRequestHeader("Access-Control-Allow-Origin", "*");
    			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    		},
    		success: function (json) {
    			if (json.error!==null) {
    				mensajeError(json.error);
    			}
    			$.unblockUI();
    		},
    		error: function(c) {
    			$.unblockUI();
    			mensajeError( "ERROR al ejecutar SIBBACServiceCierreContable#update: " + c );
    		}
    	});
    	return false;
    });



} ]);
