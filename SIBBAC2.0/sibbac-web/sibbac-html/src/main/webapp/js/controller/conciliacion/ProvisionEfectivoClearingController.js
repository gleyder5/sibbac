sibbac20.controller('ProvisionEfectivoClearingController', ['$scope', 'CuentaLiquidacionService', '$document', 'growl', 'ConciliacionClearingService',
    function ($scope, CuentaLiquidacionService, $document, growl, ConciliacionClearingService) {
        var oTable = undefined;
        $scope.filtro = {fcontratacionA: "",
            fcontratacionDe: "",
            codCtaDeCompensacion: "",
            chcodCtaDeCompensacion : true
        };
        $scope.percent = "";
        $scope.migaBusqueda = "";
        function consultar(params) {
            inicializarLoading();
            ConciliacionClearingService.getDesgloseCamaraBySentidoCuentasClearing(onConsultarSuccessRequest, onRequestErrorHandler, params);
        }
        function onConsultarSuccessRequest(json, status, headers, config) {
            var datos = undefined;



            if (json === undefined || json.resultados === undefined || json.resultados === null || json.resultados.result_provision_efectivo === undefined) {
                datos = {};
            } else if (json !== null && json.error !== null) {
                growl.addErrorMessage(json.error);
            } else {
                datos = json.resultados.result_provision_efectivo;
                var errors = cargarDatos(datos);
            }
            $.unblockUI();
            //console.log("ya estamos cargados");
        }
        function cargarDatos(datos) {

            var difClass = 'centrado';
            var isin = "";
            var cdCta = "";
            var corretaje = "";
            var efectivo = "";
            var efectivoFav = "";
            var efectivoDesf = "";
            var sumaDesf = "";
            var sumaFav = "";
            var sumaPct = "";
            oTable.clear();
            var row = [];
            angular.forEach(datos, function (val, key) {
                isin = datos[key].isin;
                cdCta = datos[key].codCtaDeCompensacion;
                efectivo = datos[key].efectivo;
                corretaje = datos[key].corretaje;
                sumaFav = corretaje;
                sumaDesf = corretaje + efectivo;
                var column = oTable.column(6);

                /*Si se introduce un porcentaje, se muestra la columna con el correspondiente valor
                 * de lo contrario, la columna se oculta */
                if ($scope.percent !== "" && $scope.filtro.codCtaDeCompensacion === "" || $scope.filtro.codCtaDeCompensacion === datos[key].codCtaDeCompensacion) {
                    sumaPct = ((efectivo * parseFloat($scope.percent)) / 100) + corretaje;
                    column.visible(true);
                }else{
                    column.visible(false);
                }
                /*Rellena fila*/
                row.push(isin);
                row.push(cdCta);
                row.push($.number(efectivo, 2, ',', '.'));
                row.push($.number(corretaje, 2, ',', '.'));
                row.push($.number(sumaFav, 2, ',', '.'));
                row.push($.number(sumaDesf, 2, ',', '.'));
                row.push($.number(sumaPct, 2, ',', '.'));
                oTable.row.add(row, false);
                row = [];
            });
            oTable.draw();

        }
        /////////////////////////////////////////////////
        //////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
        function initialize() {
            //Si se ha llamado initialize desde un setTimeout,
            //se comprueba y se elimina
            //var fechas = ['fcontratacionDe', 'fcontratacionA'];
            //verificarFechas([["fcontratacionDe", "fcontratacionA"]]);
            //loadpicker(fechas);
        	 $("#ui-datepicker-div").remove();
 			$('input#fcontratacionDe').datepicker({
 			    onClose: function( selectedDate ) {
 			      $( "#fcontratacionA" ).datepicker( "option", "minDate", selectedDate );
 			    } // onClose
 			  });

 			  $('input#fcontratacionA').datepicker({
 			    onClose: function( selectedDate ) {
 			      $( "#fcontratacionDe" ).datepicker( "option", "maxDate", selectedDate );
 			    } // onClose
 			  });
            cargarFechaActualizacionMovimientoAlias();
            cargarFechasLiquidacion();
        }
        function initializeTable() {
            oTable = angular.element("#tblProvision").DataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "aoColumns": [
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "align-right", type: "formatted-num"},
                    {sClass: "align-right", type: "formatted-num"},
                    {sClass: "align-right", type: "formatted-num"},
                    {sClass: "align-right", type: "formatted-num"},
                    {sClass: "align-right", type: "formatted-num"}
                ]
            });
        }
        $document.ready(function () {
            initializeTable();
            initialize();
            ajustarAnchoDivTable();


            prepareCollapsion();
            //preparamos los campos de fecha para limitar las fechas a seleccionar
            //console.log("tenemos: ",document.getElementById("fcontratacionDe").value);
            //$( "#fcontratacionA" ).datepicker( "option", "minDate", document.getElementById("fcontratacionDe").value);


        });

//        $scope.submitForm = function (event) {
        $scope.Consultar = function() {
            obtenerDatos();
            collapseSearchForm();
            seguimientoBusqueda();
            return false;
        };

        function obtenerDatos() {
        	var cuenta = (!$scope.filtro.chcodCtaDeCompensacion && $scope.filtro.codCtaDeCompensacion.trim() !== "" ? "#" : "")  + $scope.filtro.codCtaDeCompensacion.trim();
            var params = {"fcontratacionA": transformaFechaInv($scope.filtro.fcontratacionA),
                "fcontratacionDe": transformaFechaInv($scope.filtro.fcontratacionDe),
                "codCtaDeCompensacion": cuenta};

            consultar(params);
        }
        function onRequestErrorHandler(data, status, headers, config) {
            growl.addErrorMessage("Error en la petición de datos: " + data);
            $.unblockUI();
        }
        function onRequestFechasLiq(json, status, headers, config) {
            var datos = undefined;
            if (json !== undefined && json.resultados !== undefined && json.resultados.result_fcontratacion_between !== undefined) {
                datos = json.resultados.result_fcontratacion_between;
                $scope.filtro.fcontratacionDe = transformaFecha(datos.fcontratacionDe);
                $scope.filtro.fcontratacionA = transformaFecha(datos.fcontratacionA);
            }



        }
        //cargar fechas liquidacion between
        function cargarFechasLiquidacion() {
            growl.addInfoMessage("Cargando fechas ...");

            ConciliacionClearingService.getInitialFliquidacionBetween(onRequestFechasLiq, onRequestErrorHandler);

        }
        $scope.cuentas = [];
        CuentaLiquidacionService.getCuentasLiquidacionClearingNoPropiaNoIndividual().success(function (data, status, headers, config) {
            if (data != null && data.resultados != null &&
                    data.resultados.result_cuentas_liquidacion_clearing != null) {
                $scope.cuentas = data.resultados.result_cuentas_liquidacion_clearing;
                console.log("cuentas clearing obtenidas: " + $scope.cuentas.lenght);
                //console.log("tenemos: ",document.getElementById("fcontratacionDe").value);
                //ALEX 5 feb, de esta manera logramos cargar los limites de fechas
                $( "#fcontratacionA" ).datepicker( "option", "minDate", document.getElementById("fcontratacionDe").value);
            }
        }).error(onRequestErrorHandler);

        //para ver el filtro al minimizar la búsqueda
        function seguimientoBusqueda() {
            $scope.migaBusqueda = "";

            if ($scope.filtro.codCtaDeCompensacion === "") {
                $scope.migaBusqueda += " Codigo: todas ";

            } else {
            	if($scope.filtro.chcodCtaDeCompensacion)
            		$scope.migaBusqueda += " Codigo: " + $scope.filtro.codCtaDeCompensacion;
            	else
            		$scope.migaBusqueda += " Codigo distinto: " + $scope.filtro.codCtaDeCompensacion;
            }
            if ($scope.filtro.fcontratacionDe !== "") {
                $scope.migaBusqueda += " fcontratacionDe: " + $scope.filtro.fcontratacionDe;
            }
            if ($scope.filtro.fcontratacionA !== "") {
                $scope.migaBusqueda += " fcontratacionA: " + $scope.filtro.fcontratacionA;
            }
            if ($scope.percent !== "") {
                $scope.migaBusqueda += " % fallidos : " + $scope.percent;
            }
        }
        $scope.cleanForm = function (event) {
        	//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fcontratacionA" ).datepicker( "option", "minDate", "");
            $( "#fcontratacionDe" ).datepicker( "option", "maxDate", "");

            //
            $scope.filtro = {fcontratacionA: "",
                fcontratacionDe: "",
                codCtaDeCompensacion: "",
                chchcodCtaDeCompensacion : false
             };
            $scope.percent = "";
            $scope.btnInvselectCuenta();
        };

    	$scope.btnInvCdCodigo = function() {

      	  if($scope.filtro.chcodCtaDeCompensacion){
  		  	  $scope.filtro.chcodCtaDeCompensacion = false;
      		  document.getElementById('textBtnInvCdCodigo').innerHTML = "<>";
      		  angular.element('#textInvCdCodigo').css({"background-color": "transparent", "color": "red"});
      		  document.getElementById('textInvCdCodigo').innerHTML = "DISTINTO";

      	  }else{
      		  $scope.filtro.chcodCtaDeCompensacion = true;
      		  document.getElementById('textBtnInvCdCodigo').innerHTML = "=";
      		  angular.element('#textInvCdCodigo').css({"background-color": "transparent", "color": "green"});
   			  document.getElementById('textInvCdCodigo').innerHTML = "IGUAL";

      	  }
    	}

    }]);


