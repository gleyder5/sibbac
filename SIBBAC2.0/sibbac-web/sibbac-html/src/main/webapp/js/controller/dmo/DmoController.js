(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	'use strict';
	sibbac20.controller('DmoController', ['$scope', '$http', 'growl', '$timeout', function ($scope, $http, growl, $timeout) {
		/**VAR**/
		$scope.dateInit = "";
		$scope.dateEnd = "";
		$scope.errorMsg = "Datos incorrectos";
		$scope.httpMsgHide = true;
		$scope.httpMsgHide = "Cargando...";
		
		
		this.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {"Cerrar" : function() {
				self.resultDialog.dialog("close");
				if(self.result.ok) {
					alert("s");
				}
				 
			}}
		});
		
		var showInfoError = true;
		var valid = true;


		/**LISTENERS**/
		$scope.submitInfo = submitInfo;
		angular.element(document.getElementById("collapsableSearch")).click(function (event) {
			event.preventDefault();
			$(this).parents('.title_section').next().slideToggle();
			$(this).parents('.title_section').next().next('.button_holder').slideToggle();
			$(this).toggleClass('active');
			if ($(this).text() == "Ocultar opciones de búsqueda") {
				$(this).text("Mostrar opciones de búsqueda");
			} else {
				$(this).text("Ocultar opciones de búsqueda");
			}
			return false;
		});

		/**WATCHERS**/
		//$scope.$watchGroup(['dateInit', 'dateEnd'], function (newValues, oldValues, scope) {
		//	updateErrorMsg();
		//});

		/**FUNCTIONS**/
		//Check and call service
		function submitInfo() {			
			var uri = '/sibbac20back/rest/dmo/datadmocollection';
			var payload = { "params": { "fechaDesde": $scope.dateInit, "fechaHasta": $scope.dateEnd } };
			valid = true;
			showInfoError = true;
			validateForm();
			if (valid) {
				inicializarLoading();
				$http({
					method: 'POST',
					url: uri,
					headers: { "Content-Type": "application/json" },
					data: JSON.stringify(payload),
				}).then(function successCallback(response) {
					$scope.httpMsgHide = false;
					console.log(response)
					$scope.httpMsg = response.data.message[0];
					angular.element(document.getElementById("collapsableSearch")).trigger('click');
					$.unblockUI();
					showInfoError = false;

				}, function errorCallback(response) {
					$scope.httpMsgHide = false;
					$scope.httpMsg = response.data.message[0];
					angular.element(document.getElementById("collapsableSearch")).trigger('click');
					$.unblockUI();
					showInfoError = false;

				});
			} else {
				
				showErrorModal();
				
			}
		}
		
		//Return date formatted
		function processDate(date) {
			try {
				var parts = date.split("/");	
			} catch (error) {
				valid = false;
				$scope.errorMsg = "El formato de fechas no es correcto";
				showErrorModal();
			}
			
			return new Date(parts[2], parts[1] - 1, parts[0]);
		}
		//Show error modal
		function showErrorModal() {
			var errorMsg;
			if ($scope.dateInit === '' || $scope.dateEnd === '') {
				errorMsg = "Debe seleccionar un rango de fechas";
			} else {
				errorMsg = "La fecha final debe ser superior a la inicial";
			}
			
			fErrorTxt(errorMsg, 2);
			
			//angular.element(document.getElementById("errorDmoModal")).trigger("click");
		}
		//Update modal error msg
		function updateErrorMsg() {
			if ($scope.dateInit === '' || $scope.dateEnd === '') {
				$scope.errorMsg = "Debe seleccionar un rango de fechas";
			} else {
				$scope.errorMsg = "La fecha final debe ser superior a la inicial";
			}
		}
		//Check if end-date is superior to init-date
		function validateForm() {			
			var dateInit = processDate($scope.dateInit);
			var dateEnd = processDate($scope.dateEnd);
			if (valid) {
				valid = (dateEnd.getTime() >= dateInit.getTime());
			}		
			
		}

	}]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);