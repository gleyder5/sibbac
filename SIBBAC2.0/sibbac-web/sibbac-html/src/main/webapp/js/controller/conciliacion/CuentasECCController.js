sibbac20.controller('CuentasECCController', ['$scope', 'IsinService', 'CuentaLiquidacionService', 'CuentasECCService', '$document', 'growl', '$filter','$routeParams', function ($scope, IsinService, CuentaLiquidacionService, CuentasECCService, $document, growl, $filter,$routeParams) {
        var filter;
        $scope.cuentas = [];
        $scope.rastroBusqueda = "";
        //
        function initFilter() {
          $scope.fliquidacionDe = $filter('date')(new Date(), 'dd/MM/yyyy');
          $scope.fliquidacionA = undefined;
          $scope.isinCompleto = undefined;
          angular.element("#selectCuenta").prop("selectedIndex",0);
          angular.element("#selectCamara").prop("selectedIndex",0);
          filter = {};
        }
        var oTable = undefined;
        /** ISINES **/
        function loadIsines() {
            IsinService.getIsines().success(cargarIsines).error(showError);
        }
        /** CUENTAS **/
        function loadCuentas() {
            CuentaLiquidacionService.getCuentasLiquidacion().success(cargarCuentas).error(showError);
            $.unblockUI();
        }
        /** END **/
        function loadCamarasCompensacion() {
          CuentasECCService.getCamarasCompensacion(cargaCamarasCompensacion, showError);
        }
        function showError(data, status, headers, config) {
            $.unblockUI();
            var er = data.error ? data.error : data;
            growl.addErrorMessage(er);
        }
        function cargarCuentas(data, status, header, config) {
            if (data !== null && data.resultados !== null && data.resultados.cuentasLiquidacion !== null) {
                $scope.cuentas = data.resultados.cuentasLiquidacion;
            }
        }
        function cargaCamarasCompensacion(data) {
          if(data) {
            $scope.camarasCompensacion = data;
          }
        }
        function cargarIsines(data, status, headers, config) {
            if (data != null && data.resultados != null &&
                    data.resultados.result_isin != null) {
                var datosTsin = data.resultados.result_isin;
                var availableTags = [];
                var ponerTag = "";
                angular.element.each(datosTsin, function (key, val) {
                    ponerTag = datosTsin[key].codigo + " - " + datosTsin[key].descripcion;
                    availableTags.push(ponerTag);
                });
                angular.element("#isin").autocomplete({
                    source: availableTags
                });

            }
        }
        function consultar() {
            inicializarLoading();
            filter.fliquidacionDe = transformaFechaInv($scope.fliquidacionDe);
            filter.fliquidacionA = transformaFechaInv($scope.fliquidacionA);
            filter.idCuentaLiq = $("#selectCuenta option:selected").val();
            filter.camaraCompensacion = $("#selectCamara option:selected").val();
            filter.isin = getIsin();
            CuentasECCService.getMovimientosECC(filter, populateDataTables, showError);
        }
        function populateDataTables(datos, fecha) {
            populateDataTable(datos);
            $.unblockUI();
        }
        function populateDataTable(datos) {
            var difClassI = '';
            oTable.fnClearTable();
            if (datos !== null)
            {
            	var rownum = 0;
            	angular.forEach(datos, function (val, key) {
            		var difClassI = (rownum % 2 === 0 ) ? 'odd' : 'even';
	                var titulos = val.titulos;
	                var efectivo = val.efectivo;
	                var settlementdate = val.settlementdate;
	                settlementdate = settlementdate !== null ? EpochToHuman(settlementdate) : '';
	                var sentido = 'C';
	                var signoAnotacion = val.signoAnotacion;
	                if (signoAnotacion === 'S')
	                {
	                	sentido = 'V';
	                }
	                oTable.fnAddData([
	                                 "<span>" + val.isin + "</span>",
	                                 "<span>" + val.descrIsin + "</span>",
	                                 "<span>" + val.codCtaLiquidacion + "</span>",
	                                 "<span>" + settlementdate + "</span>",
	                                 "<span>" + sentido + "</span>",
	                                 "<span>" + val.mercado + "</span>",
	                                 "<span>" + $.number(titulos, 2, ',', '.') + "</span>",
	                                 "<span>" + $.number(efectivo, 2, ',', '.') + "</span>"
	                ]);
	                $(oTable.fnGetNodes(rownum)).addClass(difClassI);
	                rownum++;

                });
            }
        }

        function initialize() {
            loadDataTable();
            verificarFechas([ ['fliquidacionDe', 'fliquidacionA']])
            inicializarLoading();
            loadIsines();
            loadCuentas();
            loadCamarasCompensacion();
            initFilter();
            $("#ui-datepicker-div").remove();
            $('input#fliquidacionDe').datepicker({
              onClose: function( selectedDate ) {
                $( "#fliquidacionA" ).datepicker( "option", "minDate", selectedDate );
              } // onClose
            });
            $('input#fliquidacionA').datepicker({
              onClose: function( selectedDate ) {
                $( "#fliquidacionDe" ).datepicker( "option", "maxDate", selectedDate );
              } // onClose
            });
        }
        function loadDataTable() {
            oTable = angular.element("#tblClearingSaldosAN").dataTable({
                "dom": 'T<"clear">lfrtip',
                "bSort": true,
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "aoColumns": [
                    {"width": "auto","sClass": "align-left"},//ISIN
                    {"width": "auto","sClass": "align-left"},//Desc Isin
                    {"width": "auto","sClass": "align-left"},//Cuenta Liquidación
                    {"width": "auto","sClass": "centrado", "type": 'centrado'},//Fecha Liquidación
                    {"width": "auto","sClass": "centrado"},//Sentido
                    {"width": "auto","sClass": "centrado"},//Cámara
                    {"width": "auto","sClass": "monedaR", "type": 'formatted-num'},//titulos
                    {"width": "auto","sClass": "monedaR", "type": 'formatted-num'}//efectivo
                ]
            });
        }

        $scope.cleanFilter = initFilter;

        $scope.submitForm = function (event) {
            if($scope.fliquidacionDe) {
              consultar();
              collapseSearchForm();
              seguimientoBusqueda();
            }
        }

        //para ver el filtro al minimizar la búsqueda
        function seguimientoBusqueda() {
            var rastroBusqueda = "";
            if ($scope.fliquidacionDe) {
                rastroBusqueda += " Fecha Liquidación Desde: " + $scope.fliquidacionDe;
            }
            if ($scope.fliquidacionA) {
                rastroBusqueda += " Fecha Liquidación Hasta: " + $scope.fliquidacionA;
            }
            if (filter.isin) {
                rastroBusqueda += " isin: " + filter.isin;
            }
            if (filter.idCuentaLiq !== "") {
                rastroBusqueda += " Cuenta: " + $('#selectCuenta option:selected').text();
            }
            if (filter.camaraCompensacion != "") {
                rastroBusqueda += " Cámara: " + $("#selectCamara option:selected").text();
            }
            $scope.rastroBusqueda = rastroBusqueda;
        }

        //exportar
        function SendAsExport(format) {
            var c = this.document.forms['clearingtitulos'];
            var f = this.document.forms["export"];
            console.log("Forms: " + f.name + "/" + c.name);
            f.action = "/sibbac20back/rest/export." + format;
            var json = {
                "service": "SIBBACServiceConciliacionCuentaECC",
                "action": "getMovimientosECC",
                "filters": {
                    "fliquidacionDe": transformaFechaInv($scope.fliquidacionDe),
                    "fliquidacionA": transformaFechaInv($scope.fliquidacionA),
                    "idCuentaLiq": $("#selectCuenta option:selected").val(),
                    "camaraCompensacion": $("#selectCamara option:selected").val(),
                    "isin": getIsin()
                }
            };
            if (json != null && json != undefined && json.length == 0) {
                alert("Introduzca datos");
            } else {
                f.webRequest.value = JSON.stringify(json);
                f.submit();
            }
        }

        function getIsin() {
            var isinCompleto = $scope.isinCompleto;
            if(isinCompleto) {
              var guion = isinCompleto.indexOf("-");
              if (guion < 0) {
                  return isinCompleto;
              } else {
                  return isinCompleto.substring(0, guion);
              }
            }
        }
        
        initialize();
        prepareCollapsion();
    }]);
