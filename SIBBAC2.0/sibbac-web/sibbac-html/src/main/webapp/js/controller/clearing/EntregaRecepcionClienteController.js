'use strict';
sibbac20
    .controller(
                'EntregaRecepcionClienteController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'EntregaRecepcionService',
                 'Tmct0cleService',
                 'IsinService',
                 'AliasService',
                 '$compile',
                 function ($scope, $document, growl, EntregaRecepcionService, Tmct0cleService, IsinService,
                           AliasService, $compile) {

                   // orden del sub menu
                   var ordenSubMenu = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ];
                   // 0 = Estado
                   // 1 = Fecha Recepción
                   // 2 = Fecha Operación
                   // 3 = Fecha Liquidación
                   // 4 = Titulos
                   // 5 = Efectivo Recibido
                   // 6 = Efectivo Enviado
                   // 7 = Corretaje
                   // 8 = Contabilizado
                   // 9 = Fecha contabilizado
                   //
                   // orden de Menu principal
                   var ordenMenuPpal = [ 0, 1 ];
                   $scope.titulosMenuPpal = [ "uno", "dos" ];

                   // definicion de variables para la sumatoria de datos borrados
                   // alex 28dic
                   $scope.borrarSi = false;
                   $scope.afiPro = 0;
                   $scope.afiErrorPro = 0;

                   var calcDataTableHeight = function (sY) {
                     if ($('#tablaEntregaRecepcion').height() > 500) {
                       return 480;
                     } else {
                       return $('#tablaEntregaRecepcion').height() + 20;
                     }
                   };

                   function ajustarAltoTabla () {
                     console.log("ajustando inicio");
                     var oSettings = oTable.fnSettings();
                     console.log("ajustando 1");
                     var scrill = calcDataTableHeight(oTable.fnSettings().oScroll.sY);
                     console.log("cuanto: " + scrill);
                     oSettings.oScroll.sY = scrill;
                     console.log("tamanio antes: " + $('.dataTables_scrollBody').height());
                     $('.dataTables_scrollBody').height(scrill);
                     console.log("tamanio despues: " + $('.dataTables_scrollBody').height());
                     console.log("ajustando 2");
                     console.log("ajustando fin");
                   }

                   var tamInicial = 0;

                   var idOcultosContrapartida = [];
                   var availableTagsContrapartida = [];
                   var oTable = undefined;
                   $scope.numero_fila = 0;
                   $scope.fechaliq = [];
                   $scope.filtro = {
                     fcontratacionDe : "",
                     fcontratacionA : "",
                     fliquidacionDe : "",
                     fliquidacionA : "",
                     estado : "",
                     nbooking : "",
                     alias : "",
                     isin : "",
                     sentido : "",
                     nif : "",
                     nombretitular : "",
                     biccle : "",
                     importeNetoDesde : "",
                     importeNetoHasta : "",
                     titulosDesde : "",
                     titulosHasta : "",
                     referencias3 : "",
                     chestado : true,
                     chnbooking : true,
                     chalias : true,
                     chisin : true,
                     chsentido : true,
                     chnif : true,
                     chnombretitular : true,
                     chbiccle : true,
                     chimporteNetoDesde : true,
                     chimporteNetoHasta : true,
                     chtitulosDesde : true,
                     chtitulosHasta : true,
                     chreferencias3 : true,
                     estadoInstruccion : "A",
                     cuenta : ""
                   };
                   $scope.listaEstados = {};
                   $scope.followSearch = "";// contiene las migas de los filtros de búsqueda
                   $scope.nombreExportacionFicheros = "EntregaRecepcionCliente";
                   $scope.listaChk = [];
                   $scope.cifSvb = "";
                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   var lMarcar = true;

                   var alPaginas = [];
                   var pagina = 0;
                   var paginasTotales = 0;
                   var ultimaPagina = 0;
                   $scope.posicion = 0;
                   $scope.listaErrores = [];

                   $document.ready(function () {
                     initialize();
                     loadDataTable();
                     prepareCollapsion();

                     angular.element('#importeNetoDesde').blur(function (event) {
                       $scope.filtro.importeNetoDesde = $scope.filtro.importeNetoDesde;
                     });

                     angular.element('#importeNetoHasta').blur(function (event) {
                       $scope.filtro.importeNetoHasta = $scope.filtro.importeNetoHasta;
                     });

                     angular.element('#titulosDesde').blur(function (event) {
                       $scope.filtro.titulosDesde = formatearTitulos($scope.filtro.titulosDesde);
                     });

                     angular.element('#titulosHasta').blur(function (event) {
                       $scope.filtro.titulosHasta = formatearTitulos($scope.filtro.titulosHasta);
                     });

                     angular.element('#limpiar').click(function (event) {
                       event.preventDefault();
                       $("#fcontratacionA").datepicker("option", "minDate", "");
                       $("#fcontratacionDe").datepicker("option", "maxDate", "");
                       $("#fliquidacionA").datepicker("option", "minDate", "");
                       $("#fliquidacionDe").datepicker("option", "maxDate", "");
                       //
                       angular.element('input[type=text]').val('');
                       angular.element("#estado").val('');
                       angular.element("#sentido").val('');
                       angular.element("#isin").val('');
                       angular.element("#alias").val('');
                       angular.element("#fcontratacionDe").val('');
                       angular.element("#fcontratacionA").val('');
                       angular.element("#fliquidacionDe").val('');
                       angular.element("#fliquidacionA").val('');
                       angular.element("#importeNetoDesde").val('');
                       angular.element("#importeNetoHasta").val('');
                       angular.element("#titulosDesde").val('');
                       angular.element("#titulosHasta").val('');
                       angular.element("#referencias3").val('');
                       angular.element("#estadoInstruccion").val('A');
                       angular.element("#cuenta").val('');
                       //
                       $('input[type=text]').val('');
                       $scope.filtro = {
                         fcontratacionDe : "",
                         fcontratacionA : "",
                         fliquidacionDe : "",
                         fliquidacionA : "",
                         estado : "",
                         nbooking : "",
                         alias : "",
                         isin : "",
                         sentido : "",
                         nif : "",
                         nombretitular : "",
                         biccle : "",
                         importeNetoDesde : "",
                         importeNetoHasta : "",
                         titulosDesde : "",
                         titulosHasta : "",
                         referencias3 : "",
                         chestado : false,
                         chnbooking : false,
                         chalias : false,
                         chisin : false,
                         chsentido : false,
                         chnif : false,
                         chnombretitular : false,
                         chbiccle : false,
                         chimporteNetoDesde : false,
                         chimporteNetoHasta : false,
                         chtitulosDesde : false,
                         chtitulosHasta : false,
                         chreferencias3 : false,
                         estadoInstruccion : "A",
                         cuenta : ""
                       };

                       $scope.btnInvGenerico('Estado');
                       $scope.btnInvGenerico('Sentido');
                       $scope.btnInvGenerico('Alias');
                       $scope.btnInvGenerico('Isin');
                       $scope.btnInvGenerico('Biccle');
                       $scope.btnInvGenerico('Nbooking');
                       $scope.btnInvGenerico('Nif');
                       $scope.btnInvGenerico('Referencias3');
                       $scope.btnInvGenerico('NombreTitular');

                     });

                     $scope.Consultar = function (event) {
                       if (!validarIntervaloNumerico($scope.filtro.importeNetoDesde, $scope.filtro.importeNetoHasta)) {
                         fErrorTxt("El intervalo de importes no es correcto. Por favor, revíselo", 1);
                         return false;
                       }
                       if (!validarIntervaloNumerico($scope.filtro.titulosDesde, $scope.filtro.titulosHasta)) {
                         fErrorTxt("El intervalo de titulos no es correcto. Por favor, revíselo", 1);
                         return false;
                       }
                       cargarFechas();
                       cargarDatosEntregaRecepcion();
                       seguimientoBusqueda();
                       collapseSearchForm();
                       $('#CancelacionParcial').css('display', 'inline');
                       $('#CancelacionTotal').css('display', 'inline');
                       $('#MarcarTod').css('display', 'inline');
                       $('#MarcarPag').css('display', 'inline');
                       $('#BorrarEntrega').css('display', 'inline');
                     }
                     //

                     // funcion para desplegar la tabla de parametrizacion
                     $scope.desplegarErrores = function (idLiquidacion, posicion) {

                       $scope.posicion = posicion;
                       var existe_tabla = angular.element('#filaErrorRecuperada' + posicion);
                       var estado = $('#subtabla_a_desplegar' + posicion).css('display');
                       if (estado === "inline") {
                         $('#subtabla_a_desplegar' + posicion).css('display', 'none');
                         return;
                       }

                       var filters = {
                         "idliquidacion" : idLiquidacion
                       };

                       var table = document.getElementById('subtablaParam' + posicion);
                       var rowCount = table.rows.length;
                       for (var i = 1; i < rowCount; i++) {
                         var row = table.rows[i];
                         table.deleteRow(i);
                         rowCount--;
                         i--;

                       }
                       inicializarLoading();
                       $scope.listaErrores[posicion] = true;
                       EntregaRecepcionService.listaErroresS3(onSuccessListaErroresS3Request, onErrorRequest, filters);
                     };

                     // van aqui las fechas
                     $("#ui-datepicker-div").remove();
                     $('input#fcontratacionDe').datepicker({
                       onClose : function (selectedDate) {
                         $("#fcontratacionA").datepicker("option", "minDate", selectedDate);
                       } // onClose
                     });

                     $('input#fcontratacionA').datepicker({
                       onClose : function (selectedDate) {
                         $("#fcontratacionDe").datepicker("option", "maxDate", selectedDate);
                       } // onClose
                     });

                     $('input#fliquidacionDe').datepicker({
                       onClose : function (selectedDate) {
                         $("#fliquidacionA").datepicker("option", "minDate", selectedDate);
                       } // onClose
                     });

                     $('input#fliquidacionA').datepicker({
                       onClose : function (selectedDate) {
                         $("#fliquidacionDe").datepicker("option", "maxDate", selectedDate);
                       } // onClose
                     });
                   }); // $document.ready
                   function cargarFechas () {
                     $scope.filtro.fcontratacionDe = $('#fcontratacionDe').val();
                     $scope.filtro.fcontratacionA = $('#fcontratacionA').val();
                     $scope.filtro.fliquidacionDe = $('#fliquidacionDe').val();
                     $scope.filtro.fliquidacionA = $('#fliquidacionA').val();
                   }
                   /** Inicialización de la página. */
                   function initialize () {
                     inicializarLoading();
                     AliasService.getAlias().success(onSuccessRequestAlias).error(onErrorRequest);
                     IsinService.getIsines(onSuccessRequestIsines, onErrorRequest);
                     Tmct0cleService.getLista(onSuccessListarTmct0Cle, onErrorRequest);
                     EntregaRecepcionService.EstadosClearing(onSuccessEstadosClearingRequest, onErrorRequest, {});
                     EntregaRecepcionService.CifSvb(onSuccessCifSvb, onErrorRequest, []);

                     EntregaRecepcionService.CuentasCompensacion(function(data) {
                    	 $scope.cuentas = data.resultados.cuentasCompensacion;
                     }, onErrorRequest, []);

                     $.unblockUI();
                   } // initialize

                   /** Procesa un error durante la petición de dartos al servidor. */
                   function onErrorRequest (data) {
                     $.unblockUI();
                     fErrorTxt("Ocurrió un error durante la petición de datos.", 1);
                   } // onErrorRequest
                   
                   function onProcessErrorRequest(data) {
                	   $.unblockUI();
                	   fErrorTxt("Error durante el proceso de la operacion: " + data.error);
                   }

                   /** Carga el autocompletable de isines. */
                   function onSuccessRequestIsines (datosIsin) {
                     var availableTags = [];
                     angular.forEach(datosIsin, function (val, key) {
                       var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                       availableTags.push(ponerTag);
                     });
                     angular.element("#isin").autocomplete({
                       source : availableTags
                     });
                   } // onSuccessRequestIsines

                   /** Carga el autocompletable de alias. */
                   function onSuccessRequestAlias (datosAlias) {
                     var availableTagsAlias = [];
                     angular.forEach(datosAlias.resultados.result_alias, function (val, key) {
                       var ponerTag = val.alias + " - " + val.descripcion;
                       availableTagsAlias.push(ponerTag);
                     });
                     angular.element("#alias").autocomplete({
                       source : availableTagsAlias
                     });
                   } // onSuccessRequestAlias

                   /** Carga el autocompletable de las contrapartidas. */
                   function onSuccessListarTmct0Cle (json) {
                     var datos = {};
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultdos !== null
                                  && json.resultados.listado !== undefined) {
                         datos = json.resultados.listado;
                         angular.forEach(datos, function (val, key) {
                           idOcultosContrapartida.push(val.value);
                           var tag = val.label;
                           availableTagsContrapartida.push(tag);
                         });
                         angular.element("#contrapartida").autocomplete({
                           source : availableTagsContrapartida
                         });
                       } // else if
                     } // if
                   } // onSuccessListarTmct0Cle

                   /** Carga el desplegable de estados. */
                   function onSuccessEstadosClearingRequest (json) {
                     if (json.resultados === null) {
                       // growl.addErrorMessage(json.error);
                       fErrorTxt(json.error, 1);
                     } else {
                       $scope.listaEstados = json.resultados.lista;
                     }
                   } // onSuccessEstadosClearingRequest

                   /** Carga la tabla de datos. */
                   function loadDataTable () {
                     oTable = angular.element("#tablaEntregaRecepcion").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : [ "/sibbac20/js/swf/copy_csv_xls_pdf.swf" ],
                         "aButtons" : [ "copy", {
                           "sExtends" : "csv",
                           "sFileName" : "Listado_de_Entrega_recepcion_cliente_" + hoy + ".csv",
                         }, {
                           "sExtends" : "xls",
                           "sFileName" : "Listado_de_Entrega_recepcion_cliente_" + hoy + ".csv",
                         }, {
                           "sExtends" : "pdf",
                           "sPdfOrientation" : "landscape",
                           "sTitle" : " ",
                           "sPdfSize" : "A4",
                           "sPdfMessage" : "Listado de Entrega_recepcion_cliente.",
                           "sFileName" : "Listado_de_Entrega_recepcion_cliente_" + hoy + ".pdf",
                         }, "print" ]
                       },

                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "aoColumns" : [ {
                         sClass : "centrar",
                         targets : 0,
                         bSortable : false,
                         name : "active",
                         width : 50
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar details-control",
                         width : 150
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar",
                         width : 275
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar",
                         width : 150
                       }, {
                         sClass : "centrar",
                         "type" : "date-eu"
                       },// Fecha de contratación
                       {
                         sClass : "centrar",
                         "type" : "date-eu"
                       },// FTL
                       {
                         sClass : "centrar",
                         "type" : "date-eu"
                       },// Fecha de liquidación
                       {
                         sClass : "centrar",
                         type : "formatted-num"
                       }, {
                         sClass : "monedaR",
                         type : "formatted-num"
                       }, {
                         sClass : "monedaR",
                         type : "formatted-num"
                       }, {
                         sClass : "monedaR",
                         type : "formatted-num"
                       }, {
                         sClass : "monedaR",
                         type : "formatted-num"
                       }, {
                         sClass : "monedaR",
                         type : "formatted-num"
                       }, {
                         sClass : "monedaR",
                         type : "formatted-num"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar",
                         width : 300
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar",
                         visible : false
                       }, {
                         sClass : "centrar",
                         visible : false
                       }, {
                         sClass : "centrar",
                         visible : false
                       } ],
                       "order" : [ [ 1, "asc" ] ],
                       "scrollX" : "auto",
                       "scrollCollapse" : true,
                       "bProcessing" : true,
                       "fnCreatedRow" : function (row, data, index) {
                         $compile(row)($scope);
                       }, // "fnCreatedRow"
                       drawCallback : function () {
                         ajustarAltoTabla();
                         processInfo(this.api().page.info());
                         if (alPaginas.length !== paginasTotales) {
                           alPaginas = new Array(paginasTotales);
                           for (var i = 0; i < alPaginas.length; i++) {
                             alPaginas[i] = true;
                           }
                         }

                         if (ultimaPagina !== pagina) {

                           var valor = alPaginas[pagina]

                           if (valor) {
                             document.getElementById("MarcarPag").value = "Marcar Página";
                           } else {
                             if (lMarcar) {
                               document.getElementById("MarcarPag").value = "Desmarcar Página";
                             }
                           }

                         }
                         ultimaPagina = pagina;

                       }
                     }); // oTable

                     angular.element('#tablaEntregaRecepcion tbody').on('click', 'td.details-control', function () {
                       var nTr = angular.element(this).parents('tr')[0];
                       var aData = oTable.fnGetData(nTr);
                       if (oTable.fnIsOpen(nTr)) {
                         angular.element(this).removeClass('open');
                         oTable.fnClose(nTr);
                         ajustarAltoTabla();
                       } else {
                         if (aData[1] != "PDTE. FICHERO" && aData[1] != "ENVIADO FICHERO") {
                           angular.element(this).addClass('open');
                           oTable.fnOpen(nTr, TablaLiquidaciones(oTable, nTr), 'details');
                         }
                       }
                     });
                   } // loadDataTable

                   /** Prepara los datos para la petición de datos al servidor. */
                   function cargarDatosEntregaRecepcion () {
                     inicializarLoading();
                     console.log("cargarDatosEntregaRecepcion");
                     oTable.fnClearTable();

                     var importeNetoDesde = $scope.filtro.importeNetoDesde;
                     var importeNetoHasta = $scope.filtro.importeNetoHasta;
                     var titulosDesde = $scope.filtro.titulosDesde;
                     var titulosHasta = $scope.filtro.titulosHasta;
                     var estado = (!$scope.filtro.chestado && $scope.filtro.estado !== "" ? "#" : "")
                                  + $scope.filtro.estado;
                     var nbooking = (!$scope.filtro.chnbooking && $scope.filtro.nbooking !== "" ? "#" : "")
                                    + $scope.filtro.nbooking;
                     var alias = (!$scope.filtro.chalias && $scope.filtro.alias !== "" ? "#" : "") + getAliasId();
                     var isin = (!$scope.filtro.chisin && $scope.filtro.isin !== "" ? "#" : "") + getIsin();
                     var sentido = (!$scope.filtro.chsentido && $scope.filtro.sentido !== "" ? "#" : "")
                                   + $scope.filtro.sentido;
                     var nif = (!$scope.filtro.chnif && $scope.filtro.nif !== "" ? "#" : "") + $scope.filtro.nif;
                     var nombretitular = (!$scope.filtro.chnombretitular && $scope.filtro.nombretitular !== "" ? "#"
                                                                                                              : "")
                                         + $scope.filtro.nombretitular;
                     var biccle = (!$scope.filtro.chbiccle && $scope.filtro.biccle !== "" ? "#" : "")
                                  + $scope.filtro.biccle;
                     var referencias3 = (!$scope.filtro.chreferencias3 && $scope.filtro.referencias3 !== "" ? "#" : "")
                                        + $scope.filtro.referencias3;

                     var filtro = {
                       fcontratacionDe : $scope.filtro.fcontratacionDe,
                       fcontratacionA : $scope.filtro.fcontratacionA,
                       fliquidacionDe : $scope.filtro.fliquidacionDe,
                       fliquidacionA : $scope.filtro.fliquidacionA,
                       estado : estado,
                       nbooking : nbooking,
                       alias : alias,
                       isin : isin,
                       sentido : sentido,
                       nif : nif,
                       nombretitular : nombretitular,
                       biccle : biccle,
                       importeNetoDesde : importeNetoDesde,
                       importeNetoHasta : importeNetoHasta,
                       titulosDesde : titulosDesde,
                       titulosHasta : titulosHasta,
                       referencias3 : referencias3,
                       estadoInstruccion : $scope.filtro.estadoInstruccion,
                       cuenta : $scope.filtro.cuenta
                     };
                     EntregaRecepcionService.ListaEntregaRecepcion(onSuccessListaEntregaRecepcion, onErrorRequest,
                                                                   filtro);

                   } // cargarDatosEntregaRecepcion

                   /** Carga el tabla de datos. */
                   function onSuccessListaEntregaRecepcion (json) {
                     if (json !== null && json !== undefined) {
                       if (json.error !== null && json.error === "") {
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== null && json.resultados !== undefined
                                  && json.resultados.lista !== undefined) {
                         var ImporteTemp;
                         var item = null;
                         var datos = json.resultados.lista;
                         oTable.fnClearTable();
                         var contador = 0;
                         angular
                             .forEach(
                                      datos,
                                      function (item, key) {
                                        contador++;
                                        $scope.listaChk[key] = false;
                                        var idName = "check_" + contador;
                                        if (item.editable) {
                                          var txtMarcar = '<input ng-model="listaChk[' + key
                                                          + ']" type="checkbox" class=' + (contador - 1) + '  id="'
                                                          + idName + '" name="' + idName
                                                          + '"bvalue="N" class="editor-active">';
                                        } else {
                                          var txtMarcar = '';
                                        }
                                        switch (item.cdtpoper) {
                                          case 'E':
                                            var sentido = 'ENTREGA';
                                            break;
                                          case 'R':
                                            var sentido = 'RECEPCION';
                                            break;
                                          case 'C':
                                            var sentido = 'COMPRA';
                                            break;
                                          case 'V':
                                            var sentido = 'VENTA';
                                            break;
                                        }
                                        var estadoInstruccion = "";
                                        if (item.estadoInstruccion == 'A') {
                                        	estadoInstruccion = 'ALTA';
                                        }
                                        if (item.estadoInstruccion == 'B') {
                                        	estadoInstruccion = 'BAJA';
                                        }
                                        var rowIndex = oTable.fnAddData([ txtMarcar, // 0
                                        item.estdesglose, // 1
                                        item.estadoentrec, // 2
                                        sentido, // 3
                                        item.cdalias, // 4
                                        item.descrali, // 5 Introducida a posteriori
                                        item.cdisin, // 6
                                        item.nbvalors, // 7
                                        transformaFecha(item.fecontra), // 8
                                        transformaFecha(item.FTL), // 9
                                        transformaFecha(item.fechaLiq), // 10
                                        $.number(item.nutitliq, 0, ',', '.'), // 11
                                        $.number(item.imefeagr, 2, ',', '.'), // 12
                                        $.number(item.imcomisin, 2, ',', '.'), // 13
                                        $.number(item.imnfiliq, 2, ',', '.'), // 14
                                        $.number(item.canon_contratacion, 2, ',', '.'), // 15
                                        $.number(item.canon_compensacion, 2, ',', '.'), // 16
                                        $.number(item.canon_liquidacion, 2, ',', '.'), // 17
                                        item.cdniftit, // 18
                                        item.nbtitliq, // 19
                                        item.cdcustod, // 20
                                        item.nbooking, // 21
                                        item.nucnfclt, // 22
                                        item.nucnfliq, // 23
                                        item.cdrefban, // 24
                                        item.ourpar, // 25
                                        item.theirpar, // 26
                                        item.referencias3, // 27
                                        estadoInstruccion, // 28
                                        item.cuentaCompensacionDestino, // 29
                                        item.nuorden, // 28 -> 30
                                        item.pata, // 29 -> 31
                                        item.desglosecamara, // 30 ->32
                                        item.alcOrdenesId,
                                        item.alcOrdenMercadoId
                                        ],
                                        false);
                                        var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
                                        if (item.estadoentrec != "PDTE. FICHERO"
                                            && item.estadoentrec != "ENVIADO FICHERO") {
                                          $('td', nTr)[2].setAttribute('style', 'cursor: pointer; color: #ff0000;');
                                        }
                                      }); // angular.forEach
                         oTable.fnDraw();
                         ajustarAltoTabla();

                       } // else if
                     } // if
                     $.unblockUI();
                   } // onSuccessListaEntregaRecepcion

                   /** Crea la subtabla de datos. */
                   function TablaLiquidaciones (Table, nTr) {
                     // Alex 05Feb. modificacion realizada para facilitar su modificacion
                     // con este array podemos cambiar el orden de los campos y datos del submenu
                     // es importante que se cambie el orden solo en el array de orden que esta en el tope del todo
                     var titulosSubMenu = [
                                           "<th class='taleft' style='background-color: #000 !important;'>Estado</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Fecha Recepción</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Fecha Operación</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Fecha Liquidación</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Titulos</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Efectivo Recibido</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Efectivo Enviado</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Corretaje</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Contabilizado</th>",
                                           "<th class='taleft' style='background-color: #000 !important;'>Fecha contabilizado</th>" ];
                     //

                     console.log("TablaLiquidaciones");
                     var aData = Table.fnGetData(nTr);
                     var contador = Table.fnGetPosition(nTr);
                     var resultado = "<tr id='tabla_a_desplegar" + contador + "' style='display:block;'>"
                                     + "<td style='border-width:0 !important;background-color:#dfdfdf;'>"
                                     + "<table id='tablaParam" + contador + "' style=width:100%; margin-top:5px;'>"
                                     + "<tr>" + titulosSubMenu[ordenSubMenu[0]] + titulosSubMenu[ordenSubMenu[1]]
                                     + titulosSubMenu[ordenSubMenu[2]] + titulosSubMenu[ordenSubMenu[3]]
                                     + titulosSubMenu[ordenSubMenu[4]] + titulosSubMenu[ordenSubMenu[5]]
                                     + titulosSubMenu[ordenSubMenu[6]] + titulosSubMenu[ordenSubMenu[7]]
                                     + titulosSubMenu[ordenSubMenu[8]] + titulosSubMenu[ordenSubMenu[9]]
                                     //
                                     + "</tr></table></td></tr>";
                     desplegarLiquidaciones(aData[21], aData[22], aData[23], contador, aData[31], aData[32]);
                     return resultado;
                   } // TablaLiquidaciones

                   /** Despliega la tabla de liquidaciones. */
                   function desplegarLiquidaciones (nbooking, nucnfclt, nucnfliq, numero_fila, pata, iddesglosecamara) {
                     inicializarLoading();
                     if (pata === 'C') {
                       console.log("[AGM::EntregaRecepcionClienteController.js::desplegarLiquidaciones] Controller");
                       console.log("[AGM::EntregaRecepcionClienteController.js::desplegarLiquidaciones] [nbooking=="
                                   + nbooking + "]");
                       console.log("[AGM::EntregaRecepcionClienteController.js::desplegarLiquidaciones] [nucnfclt=="
                                   + nucnfclt + "]");
                       console.log("[AGM::EntregaRecepcionClienteController.js::desplegarLiquidaciones] [nucnfliq=="
                                   + nucnfliq + "]");
                       console.log("[AGM::EntregaRecepcionClienteController.js::desplegarLiquidaciones] [numero_fila=="
                                   + numero_fila + "]");
                       var filters = {
                         "nbooking" : nbooking,
                         "nucnfclt" : nucnfclt,
                         "nucnfliq" : nucnfliq
                       };
                     } else {
                       var filters = {
                         "iddesglosecamara" : iddesglosecamara
                       };
                     }
                     $scope.numero_fila = numero_fila;
                     EntregaRecepcionService.ListaLiquidacionS3(onRequestListaLiquidacionesS3, onErrorRequest, filters);
                   } // desplegarLiquidaciones

                   function formatearImporte (importe) {
                     var strImporte = new String(importe);
                     if (strImporte === null || strImporte === undefined || strImporte === '') {
                       strImporte = null;
                     } else {
                       //
                       importe = importe.replace('.', '').replace(',', '.');
                       if (isNaN(importe)) {
                         mensajeError("El importe introducido no es correcto.");
                         importe = null;
                       } else {
                         importe = $.number(importe, 2, ',', '.');
                       }
                     }
                     return strImporte;
                   }

                   function formatearTitulos (titulos) {
                     if (titulos === null || titulos === undefined || titulos === '') {
                       titulos = null;
                     } else {
                       titulos = titulos.replace('.', '').replace(',', '.');
                       if (isNaN(titulos)) {
                         mensajeError("Los títutlos introducidos no son correctos.");
                         titulos = null;
                       } else {
                         titulos = $.number(titulos, 0, '', '.');
                       }
                     }
                     return titulos;
                   }

                   function validarIntervaloNumerico (campoDesde, campoHasta) {
                     var numeroDesde = unformatNumber(campoDesde);
                     var numeroHasta = unformatNumber(campoHasta);
                     var validado = false;
                     if (numeroDesde === '' || numeroHasta === '') {
                       validado = true;
                     } else {
                       if (numeroDesde <= numeroHasta) {
                         validado = true;
                       }
                     }
                     return validado;
                   }
                   function unformatNumber (numero) {
                     if (numero === null || numero === undefined || numero === '') {
                       numero = '';
                     } else {
                       numero = numero.replace('.', '').replace(',', '.');
                       if (isNaN(numero)) {
                         numero = '';
                       } else {
                         numero = parseFloat(numero);
                       }
                     }
                     return numero;
                   }
                   function getNumero (campo) {
                     var numero = $(campo).val();
                     return unformatNumber(numero);
                   }

                   function onRequestListaLiquidacionesS3 (json) {
                     console.log(json);
                     console.log(json.resultados);
                     if (json.resultados != null && json.resultados !== undefined && json.resultados.lista !== undefined) {
                       var datos = json.resultados.lista;
                       var item = null;
                       var contador = 0;
                       var estilo = null;
                       var numero_fila = $scope.numero_fila;
                       var nombreDeTabla = "#tablaParam" + numero_fila;
                       angular
                           .forEach(
                                    datos,
                                    function (item, k) {
                                      if (contador % 2 == 0) {
                                        estilo = "even";
                                      } else {
                                        estilo = "odd";
                                      }

                                      contador++;
                                      var enlace = "";
                                      if (item.cdestados3 == "ERR") {
                                        enlace = "ng-click=\"desplegarErrores(" + item.idliquidacion + ", '"
                                                 + numero_fila + contador + "')\" ";
                                        enlace = enlace + " style=\"cursor: pointer; color: #ff0000;\"";
                                      } else {
                                        enlace = "";
                                      }

                                      $scope.fechaliq[contador] = item.fechaliquidacion
                                      // Alex 05Feb. modificacion realizada para facilitar su modificacion
                                      // con este array podemos cambiar el orden de los campos y datos del submenu
                                      // es importante que se cambie el orden solo en el array de orden que esta en el
                                      // tope del todo
                                      var datosSubMenu = [
                                                          "<td class=\"taleft\"" + enlace
                                                              + " style='background-color: #f5ebc5 !important;'>"
                                                              + item.cdestados3 + "</td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + "<span>{{fechaliq[" + contador
                                                              + "] | amDateFormat:'YYYY/MM/DD H:mm:ss'}}</span></td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + transformaFechaGuion(item.fechaoperacion) + "</td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + transformaFechaGuion(item.fechavalor) + "</td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + item.nutiuloliquidados + "</td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + $.number(item.importeefectivo, 2, ',', '.') + "</td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + $.number(item.importenetoliquidado, 2, ',', '.')
                                                              + "</td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + $.number(item.importecorretaje, 2, ',', '.') + "</td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + item.contabilizado + "</td>",
                                                          "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                                              + transformaFechaGuion(item.fechacontabilidad) + "</td>" ];
                                      //
                                      angular.element(nombreDeTabla)
                                          .append(

                                                  "<tr class='" + estilo + "' id='filaLiqRecuperada" + numero_fila
                                                      + "' style='background-color: #f5ebc5 !important;' >"
                                                      // + "<td class=\"taleft\"" + enlace + " style='background-color:
                                                      // #f5ebc5 !important;'>" + datosSubMenu[0] + "</td>"
                                                      + datosSubMenu[ordenSubMenu[0]] + datosSubMenu[ordenSubMenu[1]]
                                                      + datosSubMenu[ordenSubMenu[2]] + datosSubMenu[ordenSubMenu[3]]
                                                      + datosSubMenu[ordenSubMenu[4]] + datosSubMenu[ordenSubMenu[5]]
                                                      + datosSubMenu[ordenSubMenu[6]] + datosSubMenu[ordenSubMenu[7]]
                                                      + datosSubMenu[ordenSubMenu[8]] + datosSubMenu[ordenSubMenu[9]]
                                                      + "</tr>");
                                      if (item.cdestados3 == "ERR") {
                                        $scope.listaErrores.push(false);
                                        angular
                                            .element(nombreDeTabla)
                                            .append(
                                                    "<tr id='subtabla_a_desplegar"
                                                        + numero_fila
                                                        + contador
                                                        + "' style='display:none;'>"
                                                        + "<td colspan='10' style='border-width:0 !important;background-color:#dfdfdf;'>"
                                                        + "<table id='subtablaParam" + numero_fila + contador
                                                        + "' style=width:100%; margin-top:5px;'>" + "<tr>"
                                                        + "<th class='taleft' >Error cliente</th>"
                                                        + "<th class='taleft' >Error SV</th>"
                                                        + "<th class='taleft' >Descripción</th>"
                                                        + "</tr></table></td></tr>");
                                      }
                                    });
                       angular.element(nombreDeTabla).append("</table>");
                       desplegar('tabla_a_desplegar' + numero_fila, 'estadoT' + numero_fila, 'estadoT' + numero_fila);
                       $compile(angular.element(nombreDeTabla).contents())($scope);
                       ajustarAltoTabla();
                       document.getElementById("tablaParam" + numero_fila).scrollIntoView();
                     }
                     $.unblockUI();
                   }
                   function onSuccessListaErroresS3Request (json) {
                     if (json.resultados !== undefined && json.resultados.lista !== undefined) {
                       var datos = json.resultados.lista;
                       var item = null;
                       var contador = 0;
                       var estilo = null;
                       angular.forEach(datos, function (item, k) {

                         angular.element("#subtablaParam" + $scope.posicion).append(
                                                                                    "<tr class='odd' id='filaErrorRecuperada"
                                                                                        + $scope.posicion + "' >"
                                                                                        + "<td class='taleft' >"
                                                                                        + item.datoerrorcliente
                                                                                        + "</td>"
                                                                                        + "<td class='taleft' >"
                                                                                        + item.datoerrorsv + "</td>"
                                                                                        + "<td class='taleft' >"
                                                                                        + item.dserror + "</td>"
                                                                                        + "</tr>");
                       });

                       angular.element("#subtablaParam" + $scope.posicion).append("</table>");
                       // angular.element("#subtablaParam" + posicion).append($compile(rowTitu)($scope));
                     }
                     // desplegar('subtabla_a_desplegar' + posicion, 'estadoT' + posicion, 'estadoT' + posicion);
                     desplegar('subtabla_a_desplegar' + $scope.posicion, 'estadoT' + $scope.posicion, 'estadoT'
                                                                                                      + $scope.posicion);
                     $.unblockUI();
                   }

                   // funcion para desplegar las subtablas
                   function desplegar (tabla_a_desplegar, e1, estadoTfila) {
                     console.log("[AGM::EntregaRecepcionClienteController.js::desplegar] [tabla_a_desplegar=="
                                 + tabla_a_desplegar + "]");
                     console.log("[AGM::EntregaRecepcionClienteController.js::desplegar] [e1==" + e1 + "]");
                     console.log("[AGM::EntregaRecepcionClienteController.js::desplegar] [estadoTfila==" + estadoTfila
                                 + "]");
                     var tablA = angular.element('#' + tabla_a_desplegar);
                     var estadOt = angular.element('#' + e1);
                     var fila = angular.element('#' + estadoTfila);
                     $('#' + tabla_a_desplegar).css('display', 'inline');
                     ajustarAltoTabla();
                     // recorremos las filas y ocultamos todas menos esta
                   }

                   // funcion para desplegar las subtablas
                   function contraer (tabla_a_desplegar, e1, estadoTfila) {
                     $('#' + tabla_a_desplegar).css('display', 'none');
                   }

                   /** Muesta la leyenda con los filtros aplicados a la búsqueda. */
                   function seguimientoBusqueda () {
                     cargarFechas();
                     $scope.followSearch = "";
                     if ($scope.filtro.estado !== "") {
                       if ($scope.filtro.chestado)
                         $scope.followSearch += "Estado: " + getEstadoSeleccionado($scope.filtro.estado).descripcion;
                       else
                         $scope.followSearch += "Estado distinto: "
                                                + getEstadoSeleccionado($scope.filtro.estado).descripcion;
                     }

                     if ($('#sentido option:selected').text() !== "" && $('#sentido').val() !== undefined) {
                       if ($scope.filtro.chestado)
                         $scope.followSearch += " Sentido: " + $('#sentido option:selected').text();
                       else
                         $scope.followSearch += " Sentido distinto: " + $('#sentido option:selected').text();
                     }

                     if ($scope.filtro.alias !== "") {
                       if ($scope.filtro.chalias)
                         $scope.followSearch += " Alias: " + document.getElementById("alias").value;
                       else
                         $scope.followSearch += " Alias distinto: " + document.getElementById("alias").value;
                       ;
                     }
                     if ($scope.filtro.isin !== "") {
                       if ($scope.filtro.chisin)
                         $scope.followSearch += " Isin: " + document.getElementById("isin").value;
                       else
                         $scope.followSearch += " Isin distinto: " + document.getElementById("isin").value;
                     }
                     if ($scope.filtro.biccle !== "") {
                       if ($scope.filtro.chbiccle)
                         $scope.followSearch += " Contrapartida: " + $scope.filtro.biccle;
                       else
                         $scope.followSearch += " Contrapartida distinta: " + $scope.filtro.biccle;
                     }
                     if ($('#booking').val() !== "" && $('#booking').val() !== undefined) {
                       if ($scope.filtro.chnbooking)
                         $scope.followSearch += " Booking: " + $('#booking').val();
                       else
                         $scope.followSearch += " Booking distinto: " + $('#booking').val();
                     }
                     if ($scope.filtro.fcontratacionDe !== "") {
                       $scope.followSearch += " F. Contratacion Desde: " + $scope.filtro.fcontratacionDe;
                     }
                     if ($scope.filtro.fcontratacionA !== "") {
                       $scope.followSearch += " F. Contratacion Hasta: " + $('#fcontratacionA').val();
                     }
                     if ($scope.filtro.fliquidacionDe !== "") {
                       $scope.followSearch += " F. liquidacion Desde: " + $scope.filtro.fliquidacionDe;
                     }
                     if ($scope.filtro.fliquidacionA !== "") {
                       $scope.followSearch += " F. liquidacion Hasta: " + $scope.filtro.fliquidacionA;
                     }
                     if ($scope.filtro.importeNetoDesde !== "" && $scope.filtro.importeNetoDesde !== null) {
                       $scope.followSearch += " Importe Neto Desde: " + $scope.filtro.importeNetoDesde;
                     }
                     if ($scope.filtro.importeNetoHasta !== "" && $scope.filtro.importeNetoHasta !== null) {
                       $scope.followSearch += " Importe Neto Hasta: " + $scope.filtro.importeNetoHasta;
                     }
                     if ($scope.filtro.titulosDesde !== "" && $scope.filtro.titulosDesde !== null) {
                       $scope.followSearch += " Titulos Desde: " + $scope.filtro.titulosDesde;
                     }
                     if ($scope.filtro.titulosHasta !== "" && $scope.filtro.titulosHasta !== null) {
                       $scope.followSearch += " Titulos Hasta: " + $scope.filtro.titulosHasta;
                     }
                     if ($scope.filtro.nif !== "") {
                       if ($scope.filtro.chnif)
                         $scope.followSearch += " Documento: " + $scope.filtro.nif;
                       else
                         $scope.followSearch += " Documento distinto: " + $scope.filtro.nif;
                     }
                     if ($scope.filtro.nombretitular !== "") {
                       if ($scope.filtro.chnombretitular)
                         $scope.followSearch += " Nombre Titular: " + $scope.filtro.nombretitular;
                       else
                         $scope.followSearch += " Nombre Titular distinto: " + $scope.filtro.nombretitular;
                     }
                     if ($scope.filtro.referencias3 !== "" && $scope.filtro.referencias3 !== null) {
                       if ($scope.filtro.chreferencias3)
                         $scope.followSearch += " Referencia S3: " + $scope.filtro.referencias3;
                       else
                         $scope.followSearch += " Referencia S3 distinto: " + $scope.filtro.referencias3;
                     }

                     if ($scope.filtro.cuenta !== "" && $scope.filtro.cuenta !== null) {
                    	 $scope.followSearch += " Cuenta: " + $scope.filtro.cuenta;
                     }

                     if ($scope.filtro.estadoInstruccion !== "" && $scope.filtro.estadoInstruccion !== null) {
                    	 $scope.followSearch += " Estado: " + $scope.filtro.estadoInstruccion;
                     }
                   } // seguimientoBusqueda

                   /** Devuelve el alias seleccionado en el desplegable. */
                   function getAliasId () {
                     // var alias = $scope.filtro.alias.trim();
                     var alias = $("#alias").val();
                     var guion = alias.indexOf("-");
                     return (guion > 0) ? alias.substring(0, guion).trim() : alias;
                   } // getAliasId

                   /** Devuelve el isin seleccionado en el desplegable. */
                   function getIsin () {
                     // var isinCompleto = $scope.filtro.isin.trim();
                     var isinCompleto = document.getElementById("isin").value; // modificado por alex 19Jan para
                     // realizar la busqueda correcta en
                     // Chrome
                     var guion = isinCompleto.indexOf("-");
                     if (guion > 0) {
                       return isinCompleto.substring(0, guion);
                     } else {
                       return isinCompleto;
                     }
                   } // getIsin

                   /** Devuelve el estado seleccionado en el desplegable. */
                   function getEstadoSeleccionado (idEstado) {
                     var estado = {};
                     angular.forEach($scope.listaEstados, function (val, key) {
                       if (idEstado == val.id) {
                         estado = val;
                         return false;
                       }
                     });
                     return estado;
                   } // getEstadoSeleccionado

                   // Boton de marcar todos
                   $scope.selececionarCheck = function () {
                     if (lMarcar) {
                       lMarcar = false;
                       document.getElementById("MarcarTod").value = "Desmarcar Todos";
                       document.getElementById("MarcarPag").value = "Marcar Página";
                       toggleCheckBoxes(true);
                       for (var i = 0; i < alPaginas.length; i++) {
                         alPaginas[i] = false;
                       }
                     } else {

                       lMarcar = true;
                       document.getElementById("MarcarTod").value = "Marcar Todos";
                       document.getElementById("MarcarPag").value = "Marcar Página";
                       toggleCheckBoxes(false);
                       for (var i = 0; i < alPaginas.length; i++) {
                         alPaginas[i] = true;
                       }
                     }
                   };

                   // Boton de marcar página
                   $scope.selececionarCheckPag = function () {

                     if (lMarcar) {
                       var valor = alPaginas[pagina]

                       toggleCheckBoxesPag(valor);

                       if (valor) {
                         document.getElementById("MarcarPag").value = "Desmarcar Página";
                         alPaginas[pagina] = false;
                       } else {
                         document.getElementById("MarcarPag").value = "Marcar Página";
                         alPaginas[pagina] = true;
                       }
                     }
                   };

                   // marca/desmarca todos los registros
                   function toggleCheckBoxes (b) {

                     var aData = oTable.fnGetData();
                     for (var i = 0; i < $scope.listaChk.length; i++) {
                       if (aData[i] && aData[i][0] != '') {
                         $scope.listaChk[i] = b;
                       }
                     }
                   }

                   // marca/desmarca los registros de la pagina
                   function toggleCheckBoxesPag (b) {
                     var inputs = angular.element('input[type=checkbox]');
                     var cb = null;
                     for (var i = 0; i < inputs.length; i++) {
                       cb = inputs[i];

                       var clase = $(cb).attr('class');
                       var n = clase.indexOf(" ");
                       var fila = clase.substr(0, n);

                       $scope.listaChk[fila] = b;

                     }

                   }

                   $scope.reenvia = function() {
                	   console.log("Se solicita reenvio");
                	   var params = operationParams();
                	   if(params.length > 0) {
                		   inicializarLoading();
                		   EntregaRecepcionService.reenvio(onSuccessReenvio, onProcessErrorRequest, params);
                	   }
                	   else {
                		   fErrorTxt("Debe seleccionar al menos una fila", 1);
                	   }
                   };
                   
                   $scope.CancelacionCliente = function () {
                     console.log('Entra en la función de Cancelación Cliente');
                     if(seleccionadosSonEntregaoRecepcion()) {
                    	 var params = operationParams();
	                     if(params.length > 0) {
	                    	 inicializarLoading();
	                    	 EntregaRecepcionService.CancelacionCliente(onSuccessCancelacion, onProcessErrorRequest, params);
	                     }
	                     else {
	                       fErrorTxt("Debe seleccionar al menos una fila", 1);
	                     }
                     }
                     else {
                  	   fErrorTxt("Se ha seleccionado una fila que no es Entrega o Recepción");
                     }
                   };

                   $scope.CancelacionTotal = function () {
                       console.log('Entra en la función de Cancelación Total');
                       var params = operationParams();
                       if(params.length > 0) {
                           EntregaRecepcionService.CancelacionTotal(onSuccessCancelacion, onProcessErrorRequest, params);
                       }    
                       else { 
                         fErrorTxt("Debe seleccionar al menos una fila", 1);
                       }
                   };
                   
                   function seleccionadosSonEntregaoRecepcion() {
                	   var aData = oTable.fnGetData(), l = aData.length, valor;
                	   for(var i = 0; i < l; i++) {
                		   if($scope.listaChk[i]) {
                			   valor = aData[i][3];
                			   if(valor !== 'ENTREGA' && valor !== 'RECEPCION') {
                				   console.log("El valor " + valor + " no se considera entrega o recepcion");
                				   return false;
                			   }
                		   }
                	   }
                	   return true;
                   }
                   
                   function operationParams() {
                	   var aData = oTable.fnGetData(), l = aData.length, params = [];
                	   for(var i = 0; i < l; i++) 
                		   if($scope.listaChk[i]) 
                			   params.push({
                				   pata: aData[i][31],
                				   alcOrdenesId: aData[i][33],
                				   alcOrdenMercadoId: aData[i][34]
                			   });
                		return params;   
                   }

                   function onSuccessCancelacion (json) {
                     if (json !== null && json !== undefined) {
                       if (json.resultados.procesados != undefined) {
                         var procesados = json.resultados.procesados;
                         if(procesados > 0) {
	                         fErrorTxt("Se han cancelado " + procesados + " referencia/s", 3);
	                         $scope.Consultar();
                         }
                         else {
                        	 fErrorTxt("No se ha realizado ninguna cancelacion.", 1);
                         }
                       } else {
                    	 fErrorTxt("Error en " + json.resultados.messages.length + " registro(s) en el proceso: " 
                    			 + json.error + ".", 1);
                    	 if(json.resultados.messages) {
                        	 angular.forEach(json.resultados.messages, function(v,k) {console.log(v);});
                    	 }
                       }
                     }
                     $.unblockUI();
                   }
                   
                   function onSuccessReenvio(json) {
                       if (json !== null && json !== undefined) {
                           if (json.resultados.procesados != undefined) {
                             var procesados = json.resultados.procesados;
                             if(procesados > 0) {
    	                         fErrorTxt("Se han reenviado " + procesados + " referencia/s", 3);
    	                         $scope.Consultar();
                             }
                             else {
                            	 fErrorTxt("No se ha realizado ningun reenvío.", 1);
                             }
                           } else {
                        	 fErrorTxt("Error en " + json.resultados.messages.length + " registro(s) en el proceso: " 
                        			 + json.error + ".", 1);
                        	 if(json.resultados.messages) {
                            	 angular.forEach(json.resultados.messages, function(v,k) {console.log(v);});
                        	 }
                           }
                         }
                         $.unblockUI();
                   }

                   function onSuccessCifSvb (json) {
                     if (json !== null && json !== undefined) {
                       if (json.resultados.cif != null && json.resultados.cif !== undefined) {
                         $scope.cifSvb = json.resultados.cif;

                       } else {
                         var datos = json.resultados.error.message;
                         fErrorTxt(datos, 1);
                       }
                     }
                   }
                   ;
                   function ajustarAltoTabla () {
                     var oSettings = oTable.fnSettings();
                     var scrill = calcDataTableHeight(oTable.fnSettings().oScroll.sY);
                     oSettings.oScroll.sY = scrill;
                     $('.dataTables_scrollBody').height(scrill);
                   }

                   // Devuelve información de la tabla-.
                   function processInfo (info) {
                     pagina = info.page;
                     paginasTotales = info.pages;
                     var regInicio = info.start;
                     var regFin = info.end;
                     return regFin - regInicio;
                   }

                   function getEstadoSeleccionado (idEstado) {
                     var estado = {};
                     angular.forEach($scope.listaEstados, function (val, key) {
                       if (idEstado == val.id) {
                         estado = val;
                         return false;
                       }
                     });
                     return estado;
                   } // getEstadoSeleccionado
                   /***************************************************************************************************
                     * //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Agregado por
                     * alex para eliminar datos de forma masiva el 28dic
                     */
                   // Botón de Eliminar titulares.
                   $scope.EliminarEntrega = function () {
                     $scope.afiPro = 0;
                     $scope.afiErrorPro = 0;
                     borrarEntrega(oTable, false);

                   };
                   function borrarEntrega (oTable, saltaFiltro) {
                     cargarFechas()
                     console.log("*******************");
                     console.log("Eliminación de Entrega Recepcion Cliente");
                     console.log("*******************");
                     $scope.listaBorr = [];
                     var params = [];
                     var filter = [];
                     var aData = oTable.fnGetData();
                     var contador = 0;
                     var input;
                     var idName;
                     var inputSel;
                     for (var i = 0, j = aData.length; i < j; i++) {
                       if ($scope.listaChk[i] == true) {
                         $scope.listaBorr.push(aData[i]);
                         console.log("checked: ", aData[i]);
                         // * * * * * * * * * * * * * * * * * * * * *
                         // falta agregar el hasErrors y nusecuen
                         // * * * * * * * * * * * * * * * * * * * * *
                       }
                     }

                     if ($scope.listaBorr.length == 0) {
                       fErrorTxt("Debe seleccionar al menos uno de ellos.", 2);
                       return;
                     }

                     $scope.saltaFiltro = saltaFiltro;
                     var filtro = {
                       "saltaFiltro" : $scope.saltaFiltro
                     };

                     //
                     for (var i = 0, j = $scope.listaBorr.length; i < j; i++) {
                       if ($scope.listaBorr[i][36]) {
                         var param = {
                           "hasErrors" : $scope.listaBorr[i][36],
                           "id" : $scope.listaBorr[i][33]
                         };
                       } else {
                         var param = {
                           "hasErrors" : $scope.listaBorr[i][36],
                           "id" : "",
                           "nbooking" : $scope.listaBorr[i][19],
                           "nuorden" : $scope.listaBorr[i][26],
                           "nucnfclt" : $scope.listaBorr[i][20],
                           "nucnfliq" : $scope.listaBorr[i][21],
                           "nusecuen" : $scope.listaBorr[i][35]
                         };
                         console.log("vamos a agregar datos al array");
                         console.log("hasErrors", $scope.listaBorr[i][36], "id", "", "nbooking",
                                     $scope.listaBorr[i][19], "nuorden", $scope.listaBorr[i][26], "nucnfclt",
                                     $scope.listaBorr[i][20], "nucnfliq", $scope.listaBorr[i][21], "nusecuen",
                                     $scope.listaBorr[i][35]);

                       }
                       params.push(param);
                     }
                     //
                     EntregaRecepcionService.deleteEntregaRecepcionList(onSuccessDeleteTitulares, onErrorRequest,
                                                                        filtro, params);

                   }
                   function onSuccessDeleteTitulares (json) {
                     console.log("enviado con exito");
                     // descomentar para probar el exito del envio de los datos y el borrado
                     //
                     if (json !== null && json !== undefined) {
                       if (json.error !== null && json.error === "") {
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== null && json.resultados !== undefined) {
                         // agregado por alex 28dic para testear
                         if (json.resultados.mensaje !== null && json.resultados.mensaje !== undefined) {
                           console.log("--> Mensaje: ", json.resultados.mensaje);
                         }
                         // declaramos las variables
                         var datosAfiNoProcesados = json.resultados.nAfiNotProcessed;
                         var datosAfiErrorNoProcesados = json.resultados.nAfiErrorNotProcessed;
                         var datosAfiProcesados = json.resultados.nAfiProcessed;
                         var datosAfiErrorProcesados = json.resultados.nAfiErrorProcessed;
                         // Agregador por alex el 28dic para poner los datos en el mensaje final al cliente
                         $scope.afiPro += datosAfiProcesados;
                         $scope.afiErrorPro += datosAfiErrorProcesados;
                         // hasta aqui
                         if (datosAfiNoProcesados === 0 && datosAfiErrorNoProcesados === 0) {
                           console.log("mensaje: se han procesado " + $scope.afiPro + " de AFI y " + $scope.afiErrorPro
                                       + " de AFI ERROR, Mensaje: ", json.resultados.mensaje);
                           var texto = "Se han procesado " + $scope.afiPro + " de AFI y " + $scope.afiErrorPro
                                       + " de AFI ERROR";
                           fErrorTxt(texto, 3);

                         } else {
                           console.log("tenemos: ", datosAfiNoProcesados, " y ", datosAfiErrorNoProcesados);
                           $scope.afiPro = datosAfiProcesados;
                           $scope.afiErrorPro = datosAfiErrorProcesados;
                           var error = fErrorTxtRespuesta(json.resultados.mensaje);
                           if ($scope.borrarSi) {
                             // borrarTitulares(oTable, true); comentado por alex 28dic. ya esta opcion la agregé
                             // en el boton de borrar que aparece en la ventana de error
                           }
                         }
                       } else {

                         console.log("error: ", json.error, " - resultado: ", json.resultado);
                       }
                     }
                     //

                   }
                   $("#mensajeBorrado").dialog({
                     autoOpen : false,
                     modal : false,
                     width : 500,
                     buttons : [ {
                       text : " Continuar ",
                       icons : {
                         primary : "ui-icon-trash",
                       },
                       class : "ui-button",
                       click : function () {
                         $(this).dialog("close");
                         $scope.borrarSi = true;
                         borrarEntrega(oTable, true);
                       }
                     }, {
                       text : " Cancelar ",
                       icons : {
                         primary : "ui-icon-closethick",
                       },
                       class : "ui-button",
                       click : function () {
                         $(this).dialog("close");
                         $scope.borrarSi = false;
                       }
                     } ],

                     show : {
                       effect : "blind",
                       duration : 1000
                     },
                     hide : {
                       effect : "explode",
                       duration : 1000
                     }

                   });
                   function fErrorTxtRespuesta (msgError) {

                     $('.ui-dialog-titlebar-close').remove();

                     var option = $(".mensajeBorrado").dialog("option");

                     $("#mensajeBorrado").dialog(option, "title", "Advertencia");
                     $('#mensajeBorrado')
                         .append("<div class='dialogMensajeIncon'> <img src='/sibbac20/images/warning.png'/></div>");

                     var btnAceptar = $('.ui-dialog-buttonpane').find('button:contains("Aceptar")');

                     btnAceptar.width(100);
                     btnAceptar.height(25);
                     $('#mensajeBorrado').append("<p class='dialogMensajeText'ALIGN=center>" + msgError + "</p>");
                     $('#mensajeBorrado').dialog("open");

                   }

                   $scope.btnInvEstado = function () {
                     $scope.btnInvGenerico('Estado');
                   }

                   $scope.btnInvSentido = function () {
                     $scope.btnInvGenerico('Sentido');
                   }

                   $scope.btnInvAlias = function () {
                     $scope.btnInvGenerico('Alias');
                   }

                   $scope.btnInvIsin = function () {
                     $scope.btnInvGenerico('Isin');
                   }

                   $scope.btnInvBiccle = function () {
                     $scope.btnInvGenerico('Biccle');
                   }

                   $scope.btnInvNbooking = function () {
                     $scope.btnInvGenerico('Nbooking');
                   }

                   $scope.btnInvNif = function () {
                     $scope.btnInvGenerico('Nif');
                   }

                   $scope.btnInvReferencias3 = function () {
                     $scope.btnInvGenerico('Referencias3');
                   }

                   $scope.btnInvNombreTitular = function () {
                     $scope.btnInvGenerico('NombreTitular');
                   }

                   $scope.btnInvEstadoIntruccion = function () {
                     $scope.btnInvGenerico('EstadoIntruccion');
                   }

                   $scope.btnInvGenerico = function (nombre) {

                     var check = '$scope.filtro.ch' + nombre.toLowerCase();

                     if (eval(check)) {
                       var valor2 = check + "= false";
                       eval(valor2);

                       document.getElementById('textBtnInv' + nombre).innerHTML = "<>";
                       angular.element('#textInv' + nombre).css({
                         "background-color" : "transparent",
                         "color" : "red"
                       });
                       document.getElementById('textInv' + nombre).innerHTML = "DISTINTO";
                     } else {
                       var valor2 = check + "= true";
                       eval(valor2);

                       document.getElementById('textBtnInv' + nombre).innerHTML = "=";
                       angular.element('#textInv' + nombre).css({
                         "background-color" : "transparent",
                         "color" : "green"
                       });
                       document.getElementById('textInv' + nombre).innerHTML = "IGUAL";
                     }
                   }

                 } ]);
