(function(angular, sibbac20, gc, undefined) {
	"use strict";

	var BusquedaAlosDesgloseManualController = function(service, httpParamSerializer, 
			uiGridConstants, $location, $scope, $interval) {
		gc.DynamicFiltersController.call(this, service, httpParamSerializer,
				uiGridConstants);
		var gricAlcBajaColumns = [ {name : "Orden Id", field : "nuorden"}, 
		                           {name : "Booking Id", field : "nbooking"}, 
		                           {name : "Allo Id", field : "nucnfclt"}, 
		                           {name : "Alc Id", field : "nucnfliq"} 
		                          ];
		
		var gridListaTitularesColumns = [
		                               {name : "Tipo Titular", field : "tptiprep"}, 
		                               {name : "Tipo Identificador", field : "tpidenti"},
		                               {name : "Indentificacion", field : "cdholder"},
		                               {name : "Fisica/Juridica", field : "tpsocied"},
		                               {name : "Nombre", field : "nbclient"}, 
		                               {name : "Apellido/Razon Social",	field : "nbclien1"}, 
		                               {name : "Segundo Apellido", field : "nbclien2"},
		                               {name : "Direccion", field : "nbdomici"},
		                               {name : "Ciudad", field : "nbciudad"}
		                              ];
		
		var fatDialogOpt = {
			width : 1024
		};

		var self = this, lastFilter;
		this.scope = $scope;
		this.location = $location;
		this.interval = $interval;
		this.selectedQuery = {
			name : "alos"
		};
		this.selectQuery();
		this["combo-CDTPOPER"] = [ {
			label : ""
		}, {
			label : "Compra",
			value : "C"
		}, {
			label : "Venta",
			value : "V"
		} ];
		this["auto-CDISIN"] = {
			minLength : 2,
			source : service.autocompleter("isin")
		};
		this["auto-CDALIAS"] = {
			minLength : 2,
			source : service.autocompleter("alias")
		};
		this.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen : false,
			options : fatDialogOpt,
			modal : true,
			buttons : {
				"Cerrar" : function() {
					self.resultDialog.dialog("close");
					if (self.result.ok) {
						self.executeQuery();
					}
				}
			}
		});
		angular.element("#selectFichero").change(function(event) {
			event.preventDefault();
			self.ficheroDesglose = event.target.files[0];
			self.cargaFichero();
		});

		$scope.$on('$locationChangeStart', this.locationChangeListener
				.bind(this));
		lastFilter = service.getLastAlosFilter();
		if (lastFilter) {
			this.filter = lastFilter;
		}

		this.listAlcsBajaDialog = this.registryModal("listAlcsBajaDialog",
				"Allocation's to Clearer...", {
					"Delete Alc's" : this.seleccionarAlgoritmoDesglose
							.bind(this)
				}, fatDialogOpt);

		this.seleccionarAlgoritmoDesgloseDialog = this.registryModal(
				"seleccionarAlgoritmoDesgloseDialog",
				"Seleccion Algoritmo Desglose...", {
					"Revisar Desgloses" : this.recuperarListSolicitudDesglose
							.bind(this)
				}, fatDialogOpt);

		this.revisarDesgloseDialog = this.registryModal(
				"revisarDesgloseDialog", "Revisar Desglose...", {
					"Anterior" : this.anteriorDesglose.bind(this),
					"Siguiente" : this.siguienteDesglose.bind(this),
					"Aniadir" : this.aniadirDesglose.bind(this),
					"Quitar" : this.quitarDesglose.bind(this),
					"Desglosar" : this.desglosar.bind(this)
				}, fatDialogOpt);
		
		
		this.viewListaTitularesDialog = this.registryModal("viewListaTitularesDialog",
				"Titulares...", {"Quitar Titulares" : this.quitarTitular.bind(this), 
								"Revisar Titulares" : this.openRevisarTitular.bind(this)}, fatDialogOpt);
		
		this.revisarTitularDialog = this.registryModal(
				"revisarTitularDialog", "Titular...", {
					"Anterior" : this.anteriorTitular.bind(this),
					"Siguiente" : this.siguienteTitular.bind(this),
					"Ok" : this.okRevisarTitular.bind(this)
				}, fatDialogOpt);
		
		this.addListaTitularDialog = this.registryModal(
				"addListaTitularesDialog", "Add Titulares...", {
					"Add" : this.addTitular.bind(this)
				}, fatDialogOpt);

		

		this.listAlgoritmoDesglose = this.service.listAlgoritmoDesglose();

		this.gridAlcBaja = this.createGridColumnsNoSelection(
				gricAlcBajaColumns, function(gridApi) {
					self.gridApiRefAlcBaja = gridApi;
				});
		
		this.gridListaTitulares = this.createGridColumnsNoSelection(
				gridListaTitularesColumns, function(gridApi) {
					self.gridApiRefListaTitulares = gridApi;
				});
		
		this.gridAddListaTitulares = this.createGridColumnsNoSelection(
				gridListaTitularesColumns, function(gridApi) {
					self.gridApiRefAddListaTitulares = gridApi;
				});
		
		
		this.listCuentasCompensacion = this.service.listCuentasCompensacion();
		this.listMiembrosCompensadores = this.service.listMiembrosCompensadores();
		this.listReglasAsignacion = this.service.listReglasAsignacion();
		this.listClearersNacional = this.service.listClearersNacional();

		this.algoritmoSeleccionado = "";
		this.clearerSeleccionado = {};
		this.custodianSeleccionado = {};
		this.reglaSeleccionada = {};
		this.compensadorSeleccionado = {};
		this.cuentaCompensacionSeleccionada = {};
		this.titularSeleccionado = {};
		this.listSolicitudDesgloses = [];
		this.desgloseSeleccionado = {};
		this.altaSeleccionada = {};
		this.posicionSolicitudDesglose = 0;
		this.posicionDesgloseSolicitudDesgloseSeleccionado = 0;
		this.posicionTitularSeleccionado = 0;
		this.buscadorTitulares = "";
	};

	BusquedaAlosDesgloseManualController.prototype = Object.create(gc.DynamicFiltersController.prototype);

	BusquedaAlosDesgloseManualController.prototype.constructor = BusquedaAlosDesgloseManualController;
	
	BusquedaAlosDesgloseManualController.prototype.locationChangeListener = function(
			event, newUrl) {
		if (!newUrl.includes("desglose/manual")) {
			this.service.resetLastAlosFilter();
		}
	};
	
	BusquedaAlosDesgloseManualController.prototype.resetearEstado = function (){
		this.algoritmoSeleccionado = "";
		this.clearerSeleccionado = {};
		this.custodianSeleccionado = {};
		this.reglaSeleccionada = {};
		this.compensadorSeleccionado = {};
		this.cuentaCompensacionSeleccionada = {};
		this.titularSeleccionado = {};
		this.listSolicitudDesgloses = [];
		this.desgloseSeleccionado = {};
		this.altaSeleccionada = {};
		this.posicionSolicitudDesglose = 0;
		this.posicionDesgloseSolicitudDesgloseSeleccionado = 0;
		this.titularSeleccionado = {};
		this.posicionTitularSeleccionado = 0;
		this.buscadorTitulares = "";
	};
	
	BusquedaAlosDesgloseManualController.prototype.seleccionaComboCleare = function(coleccion, modelo){
		var i = 0;
		var seleccionado;
		if(coleccion != undefined && modelo != undefined && modelo.cdcleare != undefined){
			while(seleccionado == undefined && i < coleccion.length){
//				console.log("Buscando valor combo para modelo: " + modelo.cdcleare + " valor colleccion: " + coleccion[i].cdcleare)
				if(modelo.cdcleare == coleccion[i].cdcleare){
					seleccionado = coleccion[i];
				}
				i = i + 1;
			}
			if(seleccionado != undefined){
//				console.log("Encontrado valor combo para modelo: " + modelo.cdcleare + " valor colleccion: " + seleccionado.cdcleare)
			}
		}
		return seleccionado;
	};
	
	BusquedaAlosDesgloseManualController.prototype.seleccionaComboDefault = function(coleccion, modelo){
		var i = 0;
		var seleccionado;
		if(coleccion != undefined && modelo != undefined){
			while(seleccionado == undefined && i < coleccion.length){
//				console.log("Buscando valor combo para modelo: " + modelo + " valor colleccion: " + coleccion[i].value)
				if(modelo == coleccion[i].value){
					seleccionado = coleccion[i];
				}
				i = i + 1;
			}
			if(seleccionado != undefined){
//				console.log("Encontrado valor combo para modelo: " + modelo + " valor colleccion: " + seleccionado.value)
			}
		}
		return seleccionado;
	};
	
	
	
	BusquedaAlosDesgloseManualController.prototype.seleccionaCombosPantalla = function(){
		var clearer = this.seleccionaComboCleare(this.listClearersNacional, this.altaSeleccionada.settlementData.clearer);
		var custodian = this.seleccionaComboCleare(this.listClearersNacional, this.altaSeleccionada.settlementData.custodian);
		var regla = this.seleccionaComboDefault(this.listReglasAsignacion, this.altaSeleccionada.clearingRuleTo);
		var compensador = this.seleccionaComboDefault(this.listMiembrosCompensadores, this.altaSeleccionada.clearingMemberTo);
		var cuentaCompensacion = this.seleccionaComboDefault(this.listCuentasCompensacion, this.altaSeleccionada.clearingAccountCodeTo);
		
		this.clearerSeleccionado = clearer;	
		
		this.custodianSeleccionado = custodian;
		
		this.reglaSeleccionada = regla;
		
		this.compensadorSeleccionado = compensador;
		
		this.cuentaCompensacionSeleccionada = cuentaCompensacion;
	}
	
	BusquedaAlosDesgloseManualController.prototype.seleccionaClearer = function(){
		this.altaSeleccionada.settlementData.clearer = this.clearerSeleccionado;
		
		if(this.altaSeleccionada.settlementData.clearer != undefined && this.altaSeleccionada.settlementData.clearer != ''){
			this.altaSeleccionada.settlementData.dataClearer = true;
		}else{
			this.altaSeleccionada.settlementData.dataClearer = false;
		}
	}
	
	BusquedaAlosDesgloseManualController.prototype.seleccionaCustodian = function(){
		this.altaSeleccionada.settlementData.custodian = this.custodianSeleccionado;
		
		if(this.altaSeleccionada.settlementData.custodian != undefined && this.altaSeleccionada.settlementData.custodian.cdcleare != ''){
			this.altaSeleccionada.settlementData.dataCustodian = true;
		}else{
			this.altaSeleccionada.settlementData.dataCustodian = false;
		}
	}
	
	

	BusquedaAlosDesgloseManualController.prototype.cargaFichero = function() {
		var reader, fileName, self;

		self = this;
		if (!this.ficheroDesglose) {
			return;
		}
		fileName = this.ficheroDesglose.name;
		reader = new FileReader();
		reader.onload = function() {
			inicializarLoading();
			var data = new Uint8Array(reader.result);
			self.service.upload(fileName, data, self.uploadSuccess.bind(self),
					self.uploadFail.bind(self));
		};
		reader.readAsArrayBuffer(this.ficheroDesglose);
	};

	BusquedaAlosDesgloseManualController.prototype.loadAlo = function(entity) {
		this.location.path("/desglose/manual/" + entity.NUORDEN.trim() + "/"
				+ entity.NBOOKING.trim() + "/" + entity.NUCNFCLT.trim());
	};

	BusquedaAlosDesgloseManualController.prototype.customizeColumns = function(
			value) {
		gc.DynamicFiltersController.prototype.customizeColumns
				.call(this, value);
		if (value.field === 'NUCNFCLT') {
			value.width = 200;
			value.cellTemplate = "<div class='ui-grid-cell-contents'> "
					+ "<a ng-click='grid.appScope.ctrl.loadAlo(row.entity)'>{{ grid.getCellValue(row,col) }}</a>"
					+ "</div>";
		}
	};

	BusquedaAlosDesgloseManualController.prototype.uploadSuccess = function(
			result) {
		var self = this;
		$.unblockUI();
		self.result = result.data;
		self.resultDialog.dialog("option", "title",
				self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
		self.resultDialog.dialog("open");
		self.resultDialog.focus();
	};

	BusquedaAlosDesgloseManualController.prototype.uploadFail = function() {
		this.result = {
			ok : false,
			errorMessage : [ "Error de comunicación con el servidor" ]
		};
		this.resultDialog.dialog("option", "title", "Error de comunicación");
		this.resultDialog.dialog("open");
		self.resultDialog.focus();
	}
	
	

	/**
	 * Retorna un objeto de configuración con los valores utilizados en este
	 * proyecto para mostrar un grid básico. Para las columnas consulta el
	 * método "columns".
	 */
	BusquedaAlosDesgloseManualController.prototype.createGridColumnsNoSelection = function(
			columns, gridApiEventListener) {
		var ctrl = this;
		angular.forEach(columns, this.customizeColumns, this);
		return {
			paginationPageSizes : [ 10, 25, 50 ],
			paginationPageSize : 25,
			enableFiltering : false,
			data : [],
			columnDefs : columns,
			gridMenuShowHideColumns : false,
			onRegisterApi : gridApiEventListener
		};
	};

	BusquedaAlosDesgloseManualController.prototype.recuperarAlosSeleccionados = function(
			selectedRows) {
		var element;
		var listAlos = [];
		for (var i = 0; i < selectedRows.length; i++) {
			element = selectedRows[i];
			listAlos.push({
				nuorden : element.NUORDEN,
				nbooking : element.NBOOKING,
				nucnfclt : element.NUCNFCLT
			});
		}
		return listAlos;
	}

	BusquedaAlosDesgloseManualController.prototype.recuperaListaAlcsBaja = function() {

		var self = this;
		inicializarLoading();
		this.resetearEstado();
		var listAlos = this.recuperarAlosSeleccionados(this.selectedRows());
		this.service.listAlcsBaja(listAlos, function(result) {
			console.log("recuperaListaAlcsBaja con Exito");
			$.unblockUI();
			self.gridAlcBaja.data = result;
			if(result != undefined && result.length > 0 ){
				self.listAlcsBajaDialog.dialog("open");
				self.listAlcsBajaDialog.focus();
				self.interval(function() {
					self.gridApiRefAlcBaja.core.handleWindowResize();
				}, 500, 10);
			}else{
				self.seleccionarAlgoritmoDesglose();
			}
		}, this.queryFail.bind(this));

	};

	BusquedaAlosDesgloseManualController.prototype.seleccionarAlgoritmoDesglose = function() {
		this.listAlcsBajaDialog.dialog("close");
		this.seleccionarAlgoritmoDesgloseDialog.dialog("open");
		this.seleccionarAlgoritmoDesgloseDialog.focus();
		console.log("seleccionarAlgoritmoDesglose '" + this.algoritmoSeleccionado + "' con Exito ");
	};

	BusquedaAlosDesgloseManualController.prototype.recuperarListSolicitudDesglose = function() {
		var self = this;
		this.seleccionarAlgoritmoDesgloseDialog.dialog("close");
		inicializarLoading();
		var listAlos = this.recuperarAlosSeleccionados(this.selectedRows());

		this.service
				.listSolicitudDesgloseAlgoritmo(
						this.algoritmoSeleccionado,
						listAlos,
						function(result) {
							console.log("listSolicitudDesgloseAlgoritmo '" + self.algoritmoSeleccionado	+ "' con Exito");
							$.unblockUI();
							self.listSolicitudDesgloses = result;
							if (self.listSolicitudDesgloses.length > 0) {
								self.posicionSolicitudDesglose = 0;
								self.posicionDesgloseSolicitudDesgloseSeleccionado = 0;
								self.desgloseSeleccionado = self.listSolicitudDesgloses[0];
								if (self.desgloseSeleccionado.altas.length > 0) {
									self.altaSeleccionada = self.desgloseSeleccionado.altas[0];
								}
								self.seleccionaCombosPantalla();
								
								self.revisarDesgloseDialog.dialog("open");
								self.revisarDesgloseDialog.focus();
							}
							console.log("listSolicitudDesgloseAlgoritmo ");
						}, this.queryFail.bind(this));

	}

	BusquedaAlosDesgloseManualController.prototype.siguienteDesglose = function() {
		if (this.posicionDesgloseSolicitudDesgloseSeleccionado < this.desgloseSeleccionado.altas.length - 1) {
			this.posicionDesgloseSolicitudDesgloseSeleccionado = this.posicionDesgloseSolicitudDesgloseSeleccionado + 1;
			this.altaSeleccionada = this.desgloseSeleccionado.altas[this.posicionDesgloseSolicitudDesgloseSeleccionado];
		} else {
			if (this.posicionSolicitudDesglose < this.listSolicitudDesgloses.length - 1) {
				this.posicionDesgloseSolicitudDesgloseSeleccionado = 0;
				this.posicionSolicitudDesglose = this.posicionSolicitudDesglose + 1;
				this.desgloseSeleccionado = this.listSolicitudDesgloses[this.posicionSolicitudDesglose];
				this.altaSeleccionada = this.desgloseSeleccionado.altas[this.posicionDesgloseSolicitudDesgloseSeleccionado];
			}
		}
		this.seleccionaCombosPantalla();
		this.scope.$apply();
	}

	BusquedaAlosDesgloseManualController.prototype.anteriorDesglose = function() {
		if (this.posicionDesgloseSolicitudDesgloseSeleccionado > 0) {
			this.posicionDesgloseSolicitudDesgloseSeleccionado = this.posicionDesgloseSolicitudDesgloseSeleccionado - 1;
			this.altaSeleccionada = this.desgloseSeleccionado.altas[this.posicionDesgloseSolicitudDesgloseSeleccionado];
		} else {
			if (this.posicionSolicitudDesglose > 0) {
				this.posicionSolicitudDesglose = this.posicionSolicitudDesglose - 1;
				this.desgloseSeleccionado = this.listSolicitudDesgloses[this.posicionSolicitudDesglose];
				this.posicionDesgloseSolicitudDesgloseSeleccionado = this.desgloseSeleccionado.altas.length - 1;
				this.altaSeleccionada = this.desgloseSeleccionado.altas[this.posicionDesgloseSolicitudDesgloseSeleccionado];
			}
		}
		this.seleccionaCombosPantalla();
		this.scope.$apply();
	}
	
	BusquedaAlosDesgloseManualController.prototype.clonarLista = function(origen){
		var destino = [];
		for(var i = 0; i < origen.length; i++){
			destino[i] = Object.assign({}, origen[i], {});
		}
		return destino;
	}
	
	BusquedaAlosDesgloseManualController.prototype.clonarAlta = function(){
		var newAlta = Object.assign({}, this.altaSeleccionada, {});
		newAlta.repartoEjecuciones = [];
		newAlta.settlementData = Object.assign({}, this.altaSeleccionada.settlementData, {});
		if(this.altaSeleccionada.settlementData.clearer != undefined){
			newAlta.settlementData.clearer = Object.assign({}, this.altaSeleccionada.settlementData.clearer, {});
		}
		if(this.altaSeleccionada.settlementData.custodian != undefined){
			newAlta.settlementData.custodian = Object.assign({}, this.altaSeleccionada.settlementData.custodian, {});
		}
		if(this.altaSeleccionada.settlementData.tmct0ilq != undefined){
			newAlta.settlementData.tmct0ilq = Object.assign({}, this.altaSeleccionada.settlementData.tmct0ilq, {});
		}
		if(this.altaSeleccionada.settlementData.holder != undefined){
			newAlta.settlementData.holder = Object.assign({}, this.altaSeleccionada.settlementData.holder, {});
		}
		if(this.altaSeleccionada.settlementData.settlementDataClient != undefined){
			newAlta.settlementData.settlementDataClient = Object.assign({}, this.altaSeleccionada.settlementData.settlementDataClient, {});
		}
		if(this.altaSeleccionada.settlementData.listHolder != undefined){
			newAlta.settlementData.listHolder = this.clonarLista(this.altaSeleccionada.settlementData.listHolder);
		}
		if(this.altaSeleccionada.settlementData.listTmct0est != undefined){
			newAlta.settlementData.listTmct0est = this.clonarLista(this.altaSeleccionada.settlementData.listTmct0est);
		}
		if(this.altaSeleccionada.settlementData.listTmct0dir != undefined){
			newAlta.settlementData.listTmct0dir = this.clonarLista(this.altaSeleccionada.settlementData.listTmct0dir);
		}
		
		return newAlta;
	}
	
	
	
	BusquedaAlosDesgloseManualController.prototype.aniadirDesglose = function() {
		var titulosAlta = this.desgloseSeleccionado.titulosAlo;
		var newAlta = this.clonarAlta(this.altaSeleccionada);
		for(var i = 0; i < this.desgloseSeleccionado.altas.length; i++ ){
			titulosAlta = titulosAlta - this.desgloseSeleccionado.altas[i].titulos;
		}
		newAlta.titulos = titulosAlta;
		
		if(titulosAlta > 0){
			this.desgloseSeleccionado.altas[this.desgloseSeleccionado.altas.length] = newAlta;
			this.altaSeleccionada = newAlta;
			this.posicionDesgloseSolicitudDesgloseSeleccionado = this.posicionDesgloseSolicitudDesgloseSeleccionado + 1;
			this.seleccionaCombosPantalla();
			this.scope.$apply();
		}
		
	}
	
	
	
	BusquedaAlosDesgloseManualController.prototype.quitarDesglose = function() {
		if(this.desgloseSeleccionado.altas.length > 1){
			this.desgloseSeleccionado.altas.splice(this.posicionDesgloseSolicitudDesgloseSeleccionado, 1);
			this.posicionDesgloseSolicitudDesgloseSeleccionado = this.posicionDesgloseSolicitudDesgloseSeleccionado - 1;
			this.altaSeleccionada = this.desgloseSeleccionado.altas[this.posicionDesgloseSolicitudDesgloseSeleccionado];
		}else{
			if(this.posicionSolicitudDesglose > 1){
				this.posicionSolicitudDesglose = this.posicionSolicitudDesglose - 1;
				this.desgloseSeleccionado = this.listSolicitudDesgloses[this.posicionSolicitudDesglose];
				this.posicionDesgloseSolicitudDesgloseSeleccionado = this.desgloseSeleccionado.altas.length - 1;
				this.altaSeleccionada = this.desgloseSeleccionado.altas[this.posicionDesgloseSolicitudDesgloseSeleccionado];
			}else{
				this.listSolicitudDesgloses.splice(0, 1);
				this.desgloseSeleccionado = this.listSolicitudDesgloses[this.posicionSolicitudDesglose];
				this.posicionDesgloseSolicitudDesgloseSeleccionado = this.desgloseSeleccionado.altas.length - 1;
				this.altaSeleccionada = this.desgloseSeleccionado.altas[this.posicionDesgloseSolicitudDesgloseSeleccionado];
			}
		}
		this.seleccionaCombosPantalla();
		this.scope.$apply();
	}
	
	BusquedaAlosDesgloseManualController.prototype.quitarTitular = function(holder){
		var index = this.altaSeleccionada.settlementData.listHolder.indexOf(holder);
		
		if(index > 0 ){
			this.altaSeleccionada.settlementData.listHolder.splice(index, 1);
		}else{
			if(this.altaSeleccionada.settlementData.listHolder.length > 1){
				this.altaSeleccionada.settlementData.listHolder.splice(index, 1);
				this.altaSeleccionada.settlementData.holder = this.altaSeleccionada.settlementData.listHolder[index];
			}else{
				this.altaSeleccionada.settlementData.listHolder = [];
				this.altaSeleccionada.settlementData.holder = {};
			}
		}
		
		if(this.altaSeleccionada.settlementData.listHolder.length == 0){
			this.altaSeleccionada.settlementData.dataTFI = false;
		}
		
		this.scope.$apply();
	}

	BusquedaAlosDesgloseManualController.prototype.desglosar = function() {
		var self = this;
		inicializarLoading();
		this.revisarDesgloseDialog.dialog("close");
		
		this.service.desglosar(this.listSolicitudDesgloses, function(result) {
			console.log("desglosar con Exito");
			$.unblockUI();
			self.result = result;
			self.resultDialog.dialog("open");
			self.resultDialog.focus();
			self.resetearEstado();
		}, this.queryFail.bind(this))
	}

	BusquedaAlosDesgloseManualController.prototype.openViewListTitulares = function() {
		var self = this;
		this.gridListaTitulares.data = this.altaSeleccionada.settlementData.listHolder;
		this.interval(function() {
			self.gridApiRefListaTitulares.core.handleWindowResize();
		}, 500, 10);
		this.viewListaTitularesDialog.dialog("open");
		this.viewListaTitularesDialog.focus();
		
	}
	
	BusquedaAlosDesgloseManualController.prototype.openRevisarTitular = function() {
		this.posicionTitularSeleccionado = 0;
		this.listTitularesRevisar = this.gridApiRefListaTitulares.selection.getSelectedRows();
		if(this.listTitularesRevisar.length > 0){
			this.titularSeleccionado = this.listTitularesRevisar[this.posicionTitularSeleccionado];
			this.revisarTitularDialog.dialog("open");
			this.revisarTitularDialog.focus();
			this.scope.$apply();
		}
	}
	
	BusquedaAlosDesgloseManualController.prototype.siguienteTitular = function() {
		if(this.posicionTitularSeleccionado < this.listTitularesRevisar.length -1){
			this.posicionTitularSeleccionado = this.posicionTitularSeleccionado + 1;
			this.titularSeleccionado = this.listTitularesRevisar[this.posicionTitularSeleccionado];
			this.scope.$apply();
		}
		
	}
	
	BusquedaAlosDesgloseManualController.prototype.anteriorTitular = function() {
		if(this.posicionTitularSeleccionado  > 0){
			this.posicionTitularSeleccionado = this.posicionTitularSeleccionado - 1;
			this.titularSeleccionado = this.listTitularesRevisar[this.posicionTitularSeleccionado];
			this.scope.$apply();
		}
	}
	
	BusquedaAlosDesgloseManualController.prototype.okRevisarTitular = function() {
		this.titularSeleccionado = {};
		this.posicionTitularSeleccionado = 0;
		this.revisarTitularDialog.dialog("close");
	}
	
	BusquedaAlosDesgloseManualController.prototype.addTitular = function() {
		var selectAdd = this.gridApiRefAddListaTitulares.selection.getSelectedRows();
		if(selectAdd.length > 0){
			if(this.altaSeleccionada.settlementData.listHolder.length == 0){
				this.altaSeleccionada.settlementData.holder = selectAdd[0];
				this.altaSeleccionada.settlementData.dataTFI = true;
			}
			for(var i = 0; i < selectAdd.length; i++){
				this.altaSeleccionada.settlementData.listHolder[this.altaSeleccionada.settlementData.listHolder.length] = selectAdd[i];
			}
			this.scope.$apply();
		}
		
		this.addListaTitularDialog.dialog("close");
		self.gridAddListaTitulares.data = [];
	}

	BusquedaAlosDesgloseManualController.prototype.titularesAutocompleter = function() {
		var self = this;
		if(self.buscadorTitulares && self.buscadorTitulares.length > 3 ){
			inicializarLoading();
			this.service
					.holderAutocompleter(
							self.buscadorTitulares,
							self.desgloseSeleccionado.fechaContratacion,
							function(result) {
								$.unblockUI();
								self.gridAddListaTitulares.data = result;
								self.interval(function() {
									self.gridApiRefAddListaTitulares.core.handleWindowResize();
								}, 500, 10);
								self.addListaTitularDialog.dialog("open");
								self.addListaTitularDialog.focus();
								
							}, this.queryFail.bind(this));
		}
	}

	sibbac20.controller("BusquedaAlosDesgloseManualController", [
			"BusquedaAlosDesgloseManualService", "$httpParamSerializer",
			"uiGridConstants", "$location", "$scope", "$interval",
			BusquedaAlosDesgloseManualController ]);

})(angular, sibbac20, GENERIC_CONTROLLERS);