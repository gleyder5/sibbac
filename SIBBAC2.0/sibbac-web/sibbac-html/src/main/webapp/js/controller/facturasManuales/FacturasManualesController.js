'use strict';
sibbac20
    .controller(
                'FacturasManualesController',
                [
                 '$scope',
                 '$filter',
                 '$document',
                 'growl',
                 'FacturasManualesService',
                 'CombosFacturasManualesService',
                 '$compile',
                 'SecurityService',
                 '$location',
                 'Logger',
                 '$rootScope',

                 function ($scope, $filter, $document, growl, FacturasManualesService, CombosFacturasManualesService,
                           $compile, SecurityService, $location, Logger, $rootScope) {

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   $scope.modalFactura = {
                     titulo : "",
                     showCrear : false,
                     showModificar : false,
                     showConsultar : false
                   };

                   $scope.showingFilter = true;

                   $scope.listFacturasManuales = [];

                   $scope.facturasManualesSeleccionadas = [];
                   $scope.facturasManualesSeleccionadasBorrar = [];

                   $scope.listAlias = [];
                   $scope.listCodigosImpuestos = [];
                   $scope.listCausasExencion = [];
                   $scope.listClaveRegimen = [];
                   $scope.listTiposFactura = [];
                   $scope.listMoneda = [];
                   $scope.listEntregaBien = [];
                   $scope.listEstados = [];
                   $scope.listExentas = [];
                   $scope.listSujetas = [];

                   $scope.aliasFiltro = null;
                   $scope.aliasFactura = null;

                   $scope.resetFiltro = function () {
                     $scope.filtro = {
                       nbDocNumero : "",
                       idAlias : "",
                       fechaHasta : "",
                       fechaDesde : "",
                       idEstado : ""
                     };
                     $scope.aliasFiltro = "";
                   };

                   $scope.resetFiltro();
                   
                   $scope.resetCliente = function () {
	                   $scope.cliente = {
	                     tpdocumento : null,
	                     numdocumento : null,
	                     paisResidencia : null,
	                     tipoResidencia : null,
	                     provincia : null
	                   };
                   };

                   $scope.today = $filter('date')(new Date(), "dd/MM/yyyy");
                   // Reset objeto plantilla
                   $scope.resetFacturaManual = function () {
                     $scope.factura = {
                       id : null,
                       auditUser : $rootScope.userName,
                       nbDocNumero : null,
                       idAlias : null,
                       idEstado : "",
                       idMoneda : "",
                       idTipoFactura : "",
                       idCausaExencion : "",
                       idClaveRegimen : "",
                       impBaseImponible : 0,
                       impImpuesto : 0,
                       fhFechaCre : $scope.today,
                       fhFechaFac : null,
                       fhInicio : $scope.today,
                       idCodImpuesto : null,
                       idEntregaBien : "",
                       coefImpCostes : 0,
                       baseImponCostes : 0,
                       cuotaRepercut : 0,
                       nbComentarios : "",
                       imFinSvb : 0,
                       imFinDiv : 0,
                       idExenta : "",
                       idSujeta : "",
                       esModificable : true
                     };
                     $scope.aliasFactura = null;
                     $scope.activeAlert = false;
                   };

                   $scope.resetFacturaManual();

                   $scope.oTable = $("#datosFacturasManuales").dataTable({
                     "dom" : 'T<"clear">lfrtip',
                     "tableTools" : {
                       "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                       "aButtons" : [ "copy", {
                         "sExtends" : "csv",
                         "sFileName" : "Listado_Facturas_Manuales_Informes_" + hoy + ".csv",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                       }, {
                         "sExtends" : "xls",
                         "sFileName" : "Listado_Facturas_Manuales_Informes_" + hoy + ".xls",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                       }, {
                         "sExtends" : "pdf",
                         "sPdfOrientation" : "landscape",
                         "sTitle" : " ",
                         "sPdfSize" : "A3",
                         "sPdfMessage" : "Listado Final Holders",
                         "sFileName" : "Listado_Facturas_Manuales_Informes_" + hoy + ".pdf",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                       }, "print" ]
                     },
                     "aoColumns" : [ {
                       sClass : "centrar",
                       bSortable : false,
                       width : "7%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     } ],
                     "fnCreatedRow" : function (nRow, aData, iDataIndex) {

                       $compile(nRow)($scope);
                     },

                     "scrollY" : "480px",
                     "scrollX" : "100%",
                     "scrollCollapse" : true,
                     "language" : {
                       "url" : "i18n/Spanish.json"
                     }
                   });

                   $scope.seleccionarElemento = function (row) {
                     if ($scope.listFacturasManuales[row].selected) {
                       $scope.listFacturasManuales[row].selected = false;
                       for (var i = 0; i < $scope.facturasManualesSeleccionadas.length; i++) {
                         if ($scope.facturasManualesSeleccionadas[i].id === $scope.listFacturasManuales[row].id) {
                           $scope.facturasManualesSeleccionadas.splice(i, 1);
                           $scope.facturasManualesSeleccionadasBorrar.splice(i, 1);
                         }
                       }
                     } else {
                       $scope.listFacturasManuales[row].selected = true;
                       $scope.facturasManualesSeleccionadas.push($scope.listFacturasManuales[row]);
                       var factura = {
                         id : $scope.listFacturasManuales[row].id,
                         esModificable : $scope.listFacturasManuales[row].esModificable
                       };
                       $scope.facturasManualesSeleccionadasBorrar.push(factura);

                     }
                   };

                   var pupulateAutocomplete = function (input, availableTags) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {

                         var option = {
                           key : ui.item.key,
                           value : ui.item.value
                         };

                         switch (input) {
                           case "#filtro_alias":
                             $scope.filtro.idAlias = option.key;
                             $scope.aliasFiltro = option.value;
                             break;
                           case "#factura_alias":
                             $scope.factura.idAlias = option.key;
                             $scope.aliasFactura = option.value;
                             break;
                           default:
                             break;
                         }

                         $scope.safeApply();
                         return false;
                       }
                     });
                   };

                   $scope.consultarAlias = function () {
                     CombosFacturasManualesService.consultarAlias(function (data) {
                       $scope.listAlias = data.resultados["listaAlias"];
                       pupulateAutocomplete("#filtro_alias", $scope.listAlias);
                       pupulateAutocomplete("#factura_alias", $scope.listAlias);
                       $scope.safeApply();
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo de Alias.", 1);
                     });
                   };

                   $scope.consultarAlias();

                   $scope.consultarCausasExencion = function () {
                     CombosFacturasManualesService.consultarCausasExencion(function (data) {
                       $scope.listCausasExencion = data.resultados["listaCausasExencion"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Causa Exencion.", 1);
                     });
                   };

                   $scope.consultarCausasExencion();

                   $scope.consultarEstados = function () {
                     CombosFacturasManualesService.consultarEstados(function (data) {
                       $scope.listEstados = data.resultados["listaEstados"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Estados.", 1);
                     });
                   };

                   $scope.consultarEstados();

                   $scope.consultarClaveRegimen = function () {
                     CombosFacturasManualesService.consultarClaveRegimen(function (data) {
                       $scope.listClaveRegimen = data.resultados["listaClaveRegimen"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Clave Regimen.", 1);
                     });
                   };

                   $scope.consultarClaveRegimen();

                   $scope.consultarCodigosImpuestos = function () {
                     CombosFacturasManualesService.consultarCodigosImpuestos(function (data) {
                       $scope.listCodigosImpuestos = data.resultados["listaCodigosImpuestos"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Codigos Impuestos.", 1);
                     });
                   };

                   $scope.consultarCodigosImpuestos();

                   $scope.consultarTiposFactura = function () {
                     CombosFacturasManualesService.consultarTiposFactura(function (data) {
                         // MFG solo se muestran todas las facturas que no sean rectificativas: las que no empiezan por R de rectificativa	 
                         var lAux =  data.resultados["listaTiposFacturas"];
                         var valor = "";
                         for (var i = 0; i < lAux.length; i++) {
                      	   valor = lAux[i].value;
                      	   if (!valor.startsWith("R")){
                      		   $scope.listTiposFactura.push(lAux[i]);
                      	   }
                         }
                    	 
                    	 
                       //$scope.listTiposFactura = data.resultados["listaTiposFacturas"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Tipos Facturas.", 1);
                     });
                   };

                   $scope.consultarTiposFactura();

                   $scope.consultarMonedas = function () {
                     CombosFacturasManualesService.consultarMonedas(function (data) {
                       $scope.listMonedas = data.resultados["listaMonedas"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del Combo Monedas.", 1);
                     });
                   };

                   $scope.consultarMonedas();

                   $scope.consultarEntregaBien = function () {
                     CombosFacturasManualesService.consultarEntregaBien(function (data) {
                       $scope.listEntregaBien = data.resultados["listaEntregaBien"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Entrega Bien.", 1);
                     });
                   };

                   $scope.consultarEntregaBien();

                   $scope.consultarExentas = function () {
                     CombosFacturasManualesService.consultarExentas(function (data) {
                       $scope.listExentas = data.resultados["listaExentas"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Facturas Exentas.", 1);
                     });
                   };

                   $scope.consultarExentas();

                   $scope.consultarSujetas = function () {
                     CombosFacturasManualesService.consultarSujetas(function (data) {
                       $scope.listSujetas = data.resultados["listaSujetas"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Facturas no Sujetas.", 1);
                     });
                   };

                   $scope.consultarSujetas();

                   $scope.consultar = function () {
                     $scope.showingFilter = false;
                     $scope.consultarFacturasManuales();
                   }

                   $scope.consultarFacturasManuales = function () {

                     inicializarLoading();

                     $scope.facturasManualesSeleccionadasBorrar = [];
                     $scope.facturasManualesSeleccionadas = [];

                     FacturasManualesService
                         .consultarFacturasManuales(
                                                    function (data) {
                                                      if (data.resultados.status === 'KO') {
                                                        $.unblockUI();
                                                        fErrorTxt(
                                                                  "Se produjo un error en la carga del listado de factura manuales.",
                                                                  1);
                                                      } else {

                                                        $scope.listFacturasManuales = data.resultados["listaFacturasManuales"];

                                                        // borra el contenido del body de la tabla
                                                        var tbl = $("#datosFacturasManuales > tbody");
                                                        $(tbl).html("");
                                                        $scope.oTable.fnClearTable();

                                                        for (var i = 0; i < $scope.listFacturasManuales.length; i++) {

                                                          var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listFacturasManuales['
                                                                      + i
                                                                      + '].selected" ng-click="seleccionarElemento('
                                                                      + i + ');"/>';

                                                          var facturasList = [
                                                                              check,
                                                                              $scope.listFacturasManuales[i].nbDocNumero,
                                                                              $scope.listFacturasManuales[i].empContraparte,
                                                                              $filter('date')($scope.listFacturasManuales[i].fhFechaCre, "dd/MM/yyyy"),
                                                                              $filter('date')($scope.listFacturasManuales[i].fhFechaFac, "dd/MM/yyyy"),
                                                                              $scope.listFacturasManuales[i].estado,
                                                                              $filter('number')($scope.listFacturasManuales[i].impBaseImponible, 2),
                                                                              $filter('number')($scope.listFacturasManuales[i].impImpuesto, 2),
                                                                              $filter('number')($scope.listFacturasManuales[i].imFinSvb, 2),
                                                                              $filter('number')($scope.listFacturasManuales[i].coefImpCostes, 2),
                                                                              $filter('number')($scope.listFacturasManuales[i].baseImponCostes, 2),
                                                                              $filter('number')($scope.listFacturasManuales[i].cuotaRepercut, 2)];

                                                          $scope.oTable.fnAddData(facturasList, false);

                                                        }
                                                        $scope.oTable.fnDraw();
                                                        $scope.safeApply();
                                                        $.unblockUI();
                                                      }
                                                    },
                                                    function (error) {
                                                      $.unblockUI();
                                                      fErrorTxt(
                                                                "Se produjo un error en la carga del listado de factura manuales.",
                                                                1);
                                                    }, $scope.filtro);
                   };

                   // Abrir modal creacion informes
                   $scope.abrirCrearFacturaManual = function () {
                     $scope.modalFactura.showCrear = true;
                     $scope.modalFactura.showModificar = false;
                     $scope.modalFactura.showConsultar = false;
                     $scope.modalFactura.titulo = "Crear Factura Manual";
                     $scope.consultarProximoNumeroFacturaManual();
                     $scope.resetFacturaManual();
                     $scope.resetCliente();
                     angular.element('#formularios').modal({
                       backdrop : 'static'
                     });
                     $(".modal-backdrop").remove();
                   };

                   $scope.consultarCliente = function () {
                     FacturasManualesService.consultarCliente(function (data) {
                       $scope.cliente = data.resultados["cliente"];
                     }, function (e) {
                       $.unblockUI();
                       fErrorTxt("Ocurrió un error durante la petición de datos buscar cliente por alias.", 1);
                     }, $scope.factura);

                   };

                   $scope.eliminarIdAlias = function () {
                     $scope.factura.idAlias = null;
                     $scope.consultarCliente();
                   }

                   $scope.$watch('factura.idAlias', function (newVal, oldVal) {
                     if (newVal !== oldVal && newVal) {
                       $scope.consultarCliente();
                     }
                   }, true);
                   
                   $scope.$watch('factura.imFinSvb', function (newVal, oldVal) {
                       if ($scope.factura.idExenta == 1) {
                    	   $scope.factura.impBaseImponible = $scope.factura.imFinSvb;
                       }
                     }, true);

                   $scope.srcImage = "images/warning.png";
                   $scope.crearFacturaManual = function () {
                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       FacturasManualesService.crearFacturaManual(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           fErrorTxt(data.error, 1);
                         } else {
                           $scope.consultarFacturasManuales();
                           angular.element('#formularios').modal("hide");
                           $scope.activeAlert = false;
                           fErrorTxt('La factura manual ' + $scope.factura.nbDocNumero + ' creada correctamente', 3);
                         }
                       }, function (e) {
                         $.unblockUI();
                         fErrorTxt("Ocurrió un error durante la petición de datos al crear la factura manual.", 1);
                       }, $scope.factura);
                       angular.element('#formularios').modal("hide");
                     }
                   };

                   // Abrir modal modificar informes
                   $scope.abrirModificarFacturasManuales = function () {

                     if ($scope.facturasManualesSeleccionadas.length > 1
                         || $scope.facturasManualesSeleccionadas.length == 0) {
                       if ($scope.facturasManualesSeleccionadas.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de facturas manuales", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de de facturas manuales.", 2)
                       }
                     } else {
                       if ($scope.facturasManualesSeleccionadas[0].esModificable) {

                         $scope.resetFacturaManual();
                         $scope.factura = angular.copy($scope.facturasManualesSeleccionadas[0]);

                         FacturasManualesService
                             .detalleFacturaManual(
                                                   function (data) {

                                                     $scope.factura = data.resultados["detalleFacturaManual"];

                                                     $scope.factura.fhFechaCre = $filter('date')
                                                         ($scope.factura.fhFechaCre, "dd/MM/yyyy");

                                                     for (var j = 0; j < $scope.listAlias.length; j++) {
                                                       if ($scope.listAlias[j].key == $scope.factura.idAlias) {
                                                         $scope.aliasFactura = $scope.listAlias[j].value;
                                                       }
                                                     }

                                                     $scope.modalFactura.showCrear = false;
                                                     $scope.modalFactura.showModificar = true;
                                                     $scope.modalFactura.showConsultar = false;
                                                     $scope.modalFactura.titulo = "Modificar Factura Manual";

                                                     angular.element('#formularios').modal({
                                                       backdrop : 'static'
                                                     });
                                                     $(".modal-backdrop").remove();

                                                   },
                                                   function (e) {
                                                     $.unblockUI();
                                                     fErrorTxt(
                                                               "Ocurrió un error durante la petición de datos al recuperar el objeto factura.",
                                                               1);
                                                   }, $scope.factura);
                       } else {
                         fErrorTxt(
                                   "Las facturas manuales con estado pdte de contabilizar, anulada o contabilizada no pueden ser modificadas.",
                                   1);
                       }
                     }
                   };

                   // Abrir modal modificar informes
                   $scope.abrirConsultarFacturasManuales = function () {

                     if ($scope.facturasManualesSeleccionadas.length > 1
                         || $scope.facturasManualesSeleccionadas.length == 0) {
                       if ($scope.facturasManualesSeleccionadas.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de facturas manuales", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de de facturas manuales.", 2)
                       }
                     } else {

                       $scope.resetFacturaManual();
                       $scope.factura = angular.copy($scope.facturasManualesSeleccionadas[0]);

                       FacturasManualesService.detalleFacturaManual(function (data) {

                         $scope.factura = data.resultados["detalleFacturaManual"];

                         $scope.factura.fhFechaCre = $filter('date')($scope.factura.fhFechaCre, "dd/MM/yyyy");

                         for (var j = 0; j < $scope.listAlias.length; j++) {
                           if ($scope.listAlias[j].key == $scope.factura.idAlias) {
                             $scope.aliasFactura = $scope.listAlias[j].value;
                           }
                         }

                         $scope.modalFactura.showCrear = false;
                         $scope.modalFactura.showModificar = false;
                         $scope.modalFactura.showConsultar = true;
                         $scope.modalFactura.titulo = "Consultar Factura Manual";

                         angular.element('#formularios').modal({
                           backdrop : 'static'
                         });
                         $(".modal-backdrop").remove();

                       }, function (e) {
                         $.unblockUI();
                         fErrorTxt("Ocurrió un error durante la petición de datos al recuperar el objeto factura.", 1);
                       }, $scope.factura);

                     }
                   };

                   $scope.modificarFacturaManual = function () {

                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       $scope.factura.auditUser = $rootScope.userName;

                       FacturasManualesService
                           .modificarFacturaManual(function (data) {
                             if (data.resultados.status === 'KO') {
                               $.unblockUI();
                               fErrorTxt(data.error, 1);
                             } else {
                               $scope.consultarFacturasManuales();
                               angular.element('#formularios').modal("hide");
                               fErrorTxt('La factura manual ' + $scope.factura.nbDocNumero
                                         + ' modificada correctamente', 3);
                             }
                           }, function (error) {
                             $.unblockUI();
                             angular.element('#formularios').modal("hide");
                             fErrorTxt("Ocurrió un error durante la petición de datos al modificar la factura manual.",
                                       1);
                           }, $scope.factura);
                     }
                   };

                   /** Validacion de campos de creacion/modificacion de factura manual. */
                   $scope.validacionFormulario = function () {
                     var $scFactura = $scope.factura;
                     
                     // MFG si la factura es exenta el campo causa de exencion es obligatorio
                     if ($scFactura.idExenta == 1 &&  !$scFactura.idCausaExencion){
                    	 return true;
                     }
                     
                     // mfg si la factura es NO exenta y el campo clave reg especial es "Regimen especial grupo iva avanzado", es obligatorio los campos:
                     // campo Coeficiente Imputación de Costes , Base imponible y Cuota Repercutida 
                     if ($scFactura.idExenta == 2 &&  $scFactura.idClaveRegimen == 7 && (!$scFactura.coefImpCostes != 0 || !$scFactura.baseImponCostes  != 0 || !$scFactura.cuotaRepercut != 0 )){
                    	 return true;
                     }
                     
                     // Si la factura es exenta son obligatorios Base imponible y Cuota de impuesto:
                     if ($scFactura.idExenta == 2 && ( !$scFactura.impBaseImponible  != 0 || (!$scFactura.impImpuesto != 0 && $scFactura.idClaveRegimen!=6) )){
                    	 return true;
                     }
                     
                     if ($scFactura.idClaveRegimen==6 && $scFactura.idExenta!=2) {
                    	 return true;
                     }
                     
                     if ($scFactura.idClaveRegimen==6 && $scFactura.idSujeta!=2) {
                    	 return true;
                     }
                     
                     // Si el comentario está vacío se muestra error
                     if (!$scFactura.nbComentarios) {
                    	 return true;
                     }

                     if ($scFactura.nbDocNumero && $scFactura.idAlias && $scFactura.idMoneda
                         && $scFactura.idTipoFactura  && $scFactura.idClaveRegimen
                         && ($scFactura.impBaseImponible || $scFactura.impBaseImponible==0) && $scFactura.idEntregaBien
                         && ($scFactura.imFinSvb || !$scFactura.imFinSvb==0) && $scFactura.idCodImpuesto && ($scFactura.imFinDiv || $scFactura.imFinDiv == 0)
                         && $scFactura.idExenta) {

                       /** Esta validacion solo aplica a factura sujeta NO EXENTA. */
                       if ($scFactura.idExenta == 2) {
                         if (($scFactura.impImpuesto || $scFactura.impImpuesto == 0) && ($scFactura.coefImpCostes || $scFactura.coefImpCostes == 0) && ($scFactura.baseImponCostes || $scFactura.baseImponCostes == 0)  && $scFactura.idSujeta
                             && ($scFactura.cuotaRepercut || $scFactura.cuotaRepercut == 0)) {
                           return false;
                         } else {
                           return true;
                         }
                       }
                       return false;
                     } else {
                       return true;
                     }
                   };

                   $scope.borrarFacturasManuales = function () {
                     if ($scope.facturasManualesSeleccionadasBorrar.length > 0) {
                       var modificable = true;
                       for (var i = 0; i < $scope.facturasManualesSeleccionadasBorrar.length; i++) {
                         if (!$scope.facturasManualesSeleccionadasBorrar[i].esModificable) {
                           modificable = false;
                         }
                       }
                       if (modificable) {

                         if ($scope.facturasManualesSeleccionadasBorrar.length == 1) {
                           angular.element("#dialog-confirm").html("¿Desea eliminar la factura manual seleccionada?");
                         } else {
                           angular.element("#dialog-confirm")
                               .html(
                                     "¿Desea eliminar las " + $scope.facturasManualesSeleccionadasBorrar.length
                                         + " facturas manuales seleccionadas?");
                         }

                         // Define the Dialog and its properties.
                         angular
                             .element("#dialog-confirm")
                             .dialog(
                                     {
                                       resizable : false,
                                       modal : true,
                                       title : "Mensaje de Confirmación",
                                       height : 150,
                                       width : 360,
                                       buttons : {
                                         " Sí " : function () {
                                           FacturasManualesService
                                               .borrarFacturasManuales(
                                                                       function (data) {
                                                                         if (data.resultados.status === 'KO') {
                                                                           $.unblockUI();
                                                                           fErrorTxt(data.error, 1);
                                                                         } else {
                                                                           $scope.consultarFacturasManuales();
                                                                           fErrorTxt('Facturas borradas correctamente',
                                                                                     3);
                                                                         }

                                                                       },
                                                                       function (error) {
                                                                         $.unblockUI();
                                                                         fErrorTxt(
                                                                                   "Ocurrió un error durante la petición de datos al eliminar las facturas manuales.",
                                                                                   1);
                                                                       },
                                                                       {
                                                                         listaFacturasManualesBorrar : $scope.facturasManualesSeleccionadasBorrar
                                                                       });

                                           $(this).dialog('close');
                                         },
                                         " No " : function () {
                                           $(this).dialog('close');
                                         }
                                       }
                                     });
                         $('.ui-dialog-titlebar-close').remove();
                       } else {
                         fErrorTxt(
                                   "Las facturas manuales anuladas o contabilizadas no pueden ser eliminadas. Revise la selección",
                                   2)
                       }
                     } else {
                       fErrorTxt("Debe seleccionar al menos un elemento de la tabla de facturas Manuales.", 2)
                     }
                   };

                   $scope.contabilizar = function (accion) {

                     if (accion == 'modificar' || accion == 'crear') {
                       $scope.activeAlert = $scope.validacionFormulario();
                       if (!$scope.activeAlert) {
                         if (accion == 'modificar') {
                           $scope.factura.auditUser = $rootScope.userName;

                           FacturasManualesService
                               .modificarContabilizar(
                                                      function (data) {
                                                        if (data.resultados.status === 'KO') {
                                                          $.unblockUI();
                                                          fErrorTxt(data.error, 1);
                                                        } else {
                                                          $scope.consultarFacturasManuales();
                                                          angular.element('#formularios').modal("hide");
                                                          fErrorTxt('La factura manual ' + $scope.factura.nbDocNumero
                                                                    + ' fue modificada y contabilizada correctamente',
                                                                    3);
                                                        }
                                                      },
                                                      function (error) {
                                                        $.unblockUI();
                                                        angular.element('#formularios').modal("hide");
                                                        fErrorTxt(
                                                                  "Ocurrió un error durante la petición de datos al modificar y contabilizar la factura manual.",
                                                                  1);
                                                      }, $scope.factura);
                         } else {
                           FacturasManualesService
                               .crearContabilizar(
                                                  function (data) {
                                                    if (data.resultados.status === 'KO') {
                                                      $.unblockUI();
                                                      fErrorTxt(data.error, 1);
                                                    } else {
                                                      $scope.consultarFacturasManuales();
                                                      angular.element('#formularios').modal("hide");
                                                      fErrorTxt('La factura manual ' + $scope.factura.nbDocNumero
                                                                + ' fue creada y contabilizada correctamente', 3);
                                                    }
                                                  },
                                                  function (error) {
                                                    $.unblockUI();
                                                    angular.element('#formularios').modal("hide");
                                                    fErrorTxt(
                                                              "Ocurrió un error durante la petición de datos al crear y contabilizar la factura manual.",
                                                              1);
                                                  }, $scope.factura);
                         }
                       }
                     } else {
                       FacturasManualesService.contabilizar(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           fErrorTxt(data.error, 1);
                         } else {
                           $scope.consultarFacturasManuales();
                           angular.element('#formularios').modal("hide");
                           fErrorTxt('La factura manual ' + $scope.factura.nbDocNumero
                                     + ' fue contabilizada correctamente', 3);
                         }
                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al contabilizar la factura manual.",
                                   1);
                       }, $scope.factura);
                     }
                   };

                   $scope.consultarProximoNumeroFacturaManual = function () {

                     FacturasManualesService.consultarProximoNumeroFacturaManual(function (data) {
                       if (data.resultados.status === 'KO') {
                         $.unblockUI();
                         fErrorTxt(data.error, 1);
                       } else {
                         $scope.factura.nbDocNumero = data.resultados['proximoNumeroFacturaManual'];
                       }
                     }, function (error) {
                       $.unblockUI();
                       fErrorTxt("Ocurrió un error durante la petición de datos al crear el numero la factura manual.",
                                 1);
                     }, $scope.factura);
                   };
                   
                   $scope.consultarProximoNumeroFacturaManualGrupoIva = function () {

                       FacturasManualesService.consultarProximoNumeroFacturaManualGrupoIva(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           fErrorTxt(data.error, 1);
                         } else {
                           $scope.factura.nbDocNumero = data.resultados['proximoNumeroFacturaManual'];
                         }
                       }, function (error) {
                         $.unblockUI();
                         fErrorTxt("Ocurrió un error durante la petición de datos al crear el numero la factura manual.",
                                   1);
                       }, $scope.factura);
                     };
                     
                   $scope.obtenerNumeroFacturaGrupoIva = function () {
                	   if ($scope.factura.idClaveRegimen==6) {
                		   $scope.consultarProximoNumeroFacturaManualGrupoIva();
                	   } else {
                		   $scope.consultarProximoNumeroFacturaManual();
                	   }
                   };

                   $scope.vaciarCamposNoExenta = function () {
                     if ($scope.factura.idExenta != 2) {
                       $scope.factura.impImpuesto = 0;
                       $scope.factura.coefImpCostes = 0;
                       $scope.factura.baseImponCostes = 0;
                       $scope.factura.cuotaRepercut = 0;
                       $scope.factura.idSujeta = null;
                       $scope.factura.impBaseImponible = $scope.factura.imFinSvb;
                     } else {
                    	 $scope.factura.impBaseImponible = 0;
                     }
                   }
                   
                   $scope.evitarCaracter = function(event){
                	   
                		   if(        event.keyCode == 35 		// #
                				   || event.keyCode == 124 		// |
                				   || event.keyCode == 191	 	// ¿
                				   || event.keyCode == 63 		// ?
                				   || event.keyCode == 8364 	// €
                				   || event.keyCode == 60 		// <
                				   || event.keyCode == 62 		// >
                				   || event.keyCode == 92 		// \
                				   || event.keyCode == 253 		// ý
                				   || event.keyCode == 221 		// Ý
                				   || event.keyCode == 180 		// ´
                				   || event.keyCode == 225 		// á
                				   || event.keyCode == 233 		// é
                				   || event.keyCode == 237 		// ì
                				   || event.keyCode == 243 		// ò
                				   || event.keyCode == 250 		// ù
                				   || event.keyCode == 193 		// Á
                				   || event.keyCode == 200 		// É
                				   || event.keyCode == 205 		// Í
                				   || event.keyCode == 211 		// Ó
                				   || event.keyCode == 218 		// Ú
                				   || event.keyCode == 94 		// ^
                				   || event.keyCode == 226 		// â
                				   || event.keyCode == 234 		// ê
                				   || event.keyCode == 238 		// î
                				   || event.keyCode == 244 		// ô
                				   || event.keyCode == 251		// û
                				   || event.keyCode == 194 		// Â
                				   || event.keyCode == 202 		// Ê
                				   || event.keyCode == 206 		// Î
                				   || event.keyCode == 212 		// Ô
                				   || event.keyCode == 219 		// Û
                				   || event.keyCode == 168 		// ¨
                				   || event.keyCode == 228 		// ä
                				   || event.keyCode == 235 		// ë
                				   || event.keyCode == 239 		// ï
                				   || event.keyCode == 246 		// ö
                				   || event.keyCode == 252		// ü
                				   || event.keyCode == 196 		// Ä
                				   || event.keyCode == 203 		// Ë
                				   || event.keyCode == 207 		// Ï
                				   || event.keyCode == 214 		// Ö
                				   || event.keyCode == 220 		// Ü
                				   || event.keyCode == 96 		// `
                				   || event.keyCode == 224 		// á
                				   || event.keyCode == 232 		// é
                				   || event.keyCode == 236 		// ì
                				   || event.keyCode == 242 		// ò
                				   || event.keyCode == 249		// ù
                				   || event.keyCode == 192 		// À
                				   || event.keyCode == 200 		// È
                				   || event.keyCode == 204 		// Ì
                				   || event.keyCode == 210 		// Ò
                				   || event.keyCode == 217 		// Ù
                				   ){
                			   event.preventDefault();
                		   }else{
                			   return
                		   }
                   }

                   /***************************************************************************************************
                     * ** ACCIONES TABLA ***
                     **************************************************************************************************/
                   $scope.seleccionarTodos = function () {
                     // Se inicializa los elementos que ya tuviera la lista de elementos a borrar.
                     $scope.facturasManualesSeleccionadasBorrar = [];
                     for (var i = 0; i < $scope.listFacturasManuales.length; i++) {
                       $scope.listFacturasManuales[i].selected = true;
                       var factura = {
                         id : $scope.listFacturasManuales[i].id,
                         esModificable : $scope.listFacturasManuales[i].esModificable
                       };
                       $scope.facturasManualesSeleccionadasBorrar.push(factura);
                     }
                     $scope.facturasManualesSeleccionadas = angular.copy($scope.listFacturasManuales);
                   };

                   $scope.deseleccionarTodos = function () {
                     for (var i = 0; i < $scope.listFacturasManuales.length; i++) {
                       $scope.listFacturasManuales[i].selected = false;
                     }
                     $scope.facturasManualesSeleccionadas = [];
                     $scope.facturasManualesSeleccionadasBorrar = [];
                   };

                   // ExportarPdf
                   $scope.exportarPdf = function () {
                	   
                     if ($scope.facturasManualesSeleccionadas.length > 1
                         || $scope.facturasManualesSeleccionadas.length == 0) {
                       if ($scope.facturasManualesSeleccionadas.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de facturas manuales", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de de facturas manuales.", 2)
                       }
                     } else {
                    	 inicializarLoading();
                    	 $scope.resetFacturaManual();
                    	 $scope.factura = angular.copy($scope.facturasManualesSeleccionadas[0]);

                    	 FacturasManualesService
                    	 .exportarPdf(
                    			 function (data) {

                    				 if (data.resultados.status === 'KO') {
                    					 $.unblockUI();
                    					 fErrorTxt("Ocurrió un error durante la exportación a PDF de la factura.", 1);
                    				 } else{

                    					 var decodedString = atob([ data.resultados.file ]);
                    					 var byteNumbers = new Array(decodedString.length);
                    					 for (var i = 0; i < decodedString.length; i++) {
                    						 byteNumbers[i] = decodedString.charCodeAt(i);
                    					 }

                    					 var byteArray = new Uint8Array(byteNumbers);

                    					 var blob = new Blob([ byteArray ], {
                    						 type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    					 });

                    					 var nav = navigator.userAgent.toLowerCase();

                    					 if (navigator.msSaveBlob) {
                    						 navigator.msSaveBlob(blob, data.resultados.nombreFichero);
                    					 } else {
                    						 var blobUrl = URL.createObjectURL(blob);
                    						 var link = document.createElement('a');
                    						 link.href = blobUrl = URL.createObjectURL(blob);
                    						 link.download = data.resultados.nombreFichero;
                    						 document.body.appendChild(link);
                    						 link.click();
                    						 document.body.removeChild(link);
                    					 }
                    					 $.unblockUI();
                    				 }

                    			 },
                    			 function (e) {
                    				 $.unblockUI();
                    				 fErrorTxt(
                    						 "Ocurrió un error durante la exportación a PDF de la factura.",
                    						 1);
                    			 }, $scope.factura);
                     }
                   };

                 } ]);
