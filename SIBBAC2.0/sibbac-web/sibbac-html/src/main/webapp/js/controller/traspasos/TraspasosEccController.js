(function(GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";
	
	var TraspasosEccController = function(service, httpParamSerializer, uiGridConstants, $scope) {
		GENERIC_CONTROLLERS.DynamicFiltersController.call(this, service, httpParamSerializer, uiGridConstants);
		var fatDialogOpt = { width: 480 }, self = this;
		this.scope = $scope;
		this.enviaDialog = this.registryModal("enviaDialog", "Envia...", {
			"Envía": this.envia.bind(this)
		}, fatDialogOpt);
		this.aceptaRechazaDialog = this.registryModal("aceptaRechazaDialog", "Acepta/Rechaza entradas", {
			Acepta: this.acepta.bind(this),
			Rechaza: this.rechaza.bind(this)
		}, fatDialogOpt); 
		this.cancelaDialog = this.registryModal("cancelaDialog", "Cancela pendientes", {
			"Ejecuta Cancelación": this.cancelaPendientes.bind(this)
		});
		this.rejectBookingDialog = this.registryModal("rejectBookingDialog", "Rechaza bookings", {
			Rechaza: this.rechazaBookings.bind(this)
		}, {width: 320});
		this.traspasosFueraHoraDialog = this.registryModal("traspasosFueraHoraDialog", "Traspasos fuera de hora", {
			"Ejecutar": this.traspasosFueraHora.bind(this)
		});
		this.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {"Cerrar" : function() {
				self.resultDialog.dialog("close");
				if(self.result.ok) {
					self.executeQuery();
				}
				 
			}}
		});
		this["combo-CDTPOPER"] = [
			{label: ""},
			{label: "Compra", value: "C"},
			{label: "Venta", value: "V"}
		];
		this["auto-CDISIN"] = {minLength : 2, 
		  source: service.autocompleter("isin")
		};
		this["auto-CDALIAS"] = {
		  minLength: 2,
		  source: service.autocompleter("alias")
		}
		
		this.listaCuentaCompensacion =  service.autocompleter("cuentaCompensacion");
		this.listaMiembroCompensador =  service.autocompleter("miembroCompensador");
	};
	
	TraspasosEccController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);
	
	TraspasosEccController.prototype.constructor = TraspasosEccController;

	TraspasosEccController.prototype.openModal = function() {
		var accion = this.selectedQuery.accion;
		this.params = {};
		switch(accion) {
		case "trapasoDesgloseClitit":
			this.enviaDialog.dialog("option", "title", "Desgloses clitit");
			this.enviaDialog.dialog("open");
			break;
		case "traspasoAlcCuenta":
			this.enviaDialog.dialog("option", "title", "Compensación");
			this.enviaDialog.dialog("open");
			break;
		case "traspasoEntrada":
			this.aceptaRechazaDialog.dialog("open");
		 	break;
		case "traspasoSalidaPdteAceptar":
			this.cancelaDialog.dialog("open");
			break;
		case "bookingsRechazables":
			this.rejectBookingDialog.dialog("open");
			break;
		case "desglosesPendienteActualizarCodigosOperacionEcc":
			this.traspasosFueraHoraDialog.dialog("open");
			break;
		}
	};
	
	TraspasosEccController.prototype.envia = function() {
		var valido = (this.params.cuentaCompensacion && this.params.cuentaCompensacion.trim().length > 0) || 
				(this.params.miembroCompensador && this.params.miembroCompensador.trim().length > 0 &&
				this.params.refGiveUp && this.params.refGiveUp.trim().length > 0);
		if(valido) {
			this.executeAction(this.enviaDialog);
		}
		else {
			fErrorTxt("Se debe rellenar el campo Cuenta Compensación o los campos Miembro Compensador y " +
					"Referencia Give-up conjuntamente", 1);
		}
	};
	
	TraspasosEccController.prototype.acepta = function() {
		var valido = this.params.cuentaCompensacion && this.params.cuentaCompensacion.trim().length > 0;
		if(valido) {
			this.params.acepta = true;
			this.executeAction(this.aceptaRechazaDialog);
		}
		else {
			fErrorTxt("El campo Cuenta Compensación es obligatorio", 1);
		}
	};
	
	TraspasosEccController.prototype.rechaza = function() {
		this.params.acepta = false;
		this.executeAction(this.aceptaRechazaDialog);
	};

	TraspasosEccController.prototype.cancelaPendientes = function() {
		this.executeAction(this.cancelaDialog);
	};
	
	TraspasosEccController.prototype.traspasosFueraHora = function() {
		this.executeAction(this.traspasosFueraHoraDialog);
	};
	
	TraspasosEccController.prototype.rechazaBookings = function() {
		this.executeAction(this.rejectBookingDialog);
	};
	
	TraspasosEccController.prototype.executeAction = function(dialogToClose) {
		var self = this;
		dialogToClose.dialog("close");
		inicializarLoading();
		this.service.executeAction(this.selectedQuery.accion, this.params, this.selectedRows(), function(result) {
			$.unblockUI();
			self.result = result;
			self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
			self.resultDialog.dialog("open");
		}, this.postFail.bind(this));
	};
	
	TraspasosEccController.prototype.customizeColumns = function(value) {
		GENERIC_CONTROLLERS.DynamicFiltersController.prototype.customizeColumns.call(this, value);
		var accion = this.selectedQuery.accion;
		if(accion === "traspasoEntrada" && value.field === 'CDREFMOVIMIENTOECC') {
		  value.width = 200;
			value.cellTemplate = "<div class='ui-grid-cell-contents'>{{ grid.getCellValue(row,col) }} " 
				+ "<a ng-click='grid.appScope.ctrl.openSubquery(row.entity,\"Detalle\")'>[Detalle]</a>"
				+ "<a ng-click='grid.appScope.ctrl.openSubquery(row.entity,\"Bookings\")'>[Bookings]</a></div>";
		}
	};
	
	TraspasosEccController.prototype.openSubquery = function(entity, name) {
		inicializarLoading();
		this.scope.$broadcast("openSubquery", entity, name);
	};
	
	sibbac20.controller("TraspasosEccController", ["TraspasosEccService", "$httpParamSerializer", "uiGridConstants", 
		"$scope", TraspasosEccController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);