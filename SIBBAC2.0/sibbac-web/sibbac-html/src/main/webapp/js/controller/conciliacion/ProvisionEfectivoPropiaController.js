sibbac20.controller('ProvisionEfectivoPropiaController', ['$scope', 'IsinService', '$document', 'ConciliacionClearingService', 'growl', function ($scope, IsinService, $document, ConciliacionClearingService, growl) {
        $scope.isines = [];
        $scope.filtro = {
        	fContratacionDe: "",
            fContratacionA: "",
            cdisin: "",
            cuenta: "00P",
            chcdisin : true
        };
        $scope.isines = {};
        $scope.rastroBusqueda = "";
        /** ISINES **/
        IsinService.getIsines().success(function (data, status, headers, config) {
            console.log("sisines cargados: " + data);
            if (data != null && data.resultados != null &&
                    data.resultados.result_isin != null) {
                $scope.isines = data.resultados.result_isin;
                var datosIsin = $scope.isines;
                angular.forEach(datosIsin, function (val, key) {
                    listaIsin.push(datosIsin[key].codigo);
                    ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                    availableTags.push(ponerTag);
                });
                angular.element("#cdisin").autocomplete({
                    source: availableTags
                });
            }
          //ALEX 5 feb, de esta manera logramos cargar los limites de fechas
            $( "#fContratacionA" ).datepicker( "option", "minDate", document.getElementById("fContratacionDe").value);
        }).error(onRequestErrorHandler);

        var listaIsin = [];
        var availableTags = [];
        var oTable = undefined;

        $document.ready(function () {
            initializeTable();
            //verificarFechas([["fContratacionDe", "fContratacionA"]]);
            initializeDateField();
            cargarFechaActualizacionMovimientoAlias();
            prepareCollapsion();
            //var fechas = ['fContratacionDe', 'fContratacionA'];
            //loadpicker(fechas);
            $("#ui-datepicker-div").remove();
 			$('input#fContratacionDe').datepicker({
 			    onClose: function( selectedDate ) {
 			      $( "#fContratacionA" ).datepicker( "option", "minDate", selectedDate );
 			    } // onClose
 			  });

 			  $('input#fContratacionA').datepicker({
 			    onClose: function( selectedDate ) {
 			      $( "#fContratacionDe" ).datepicker( "option", "maxDate", selectedDate );
 			    } // onClose
 			  });
        });
        function initializeDateField() {
            var f = new Date();
            var dia = f.getDate();
            var mes = f.getMonth() + 1;
            if (dia < 10) {
                dia = "0" + dia;
            }
            if (mes < 10)
            {
                mes = "0" + mes;
            }
            var fechaFinal = (dia + "/" + (mes) + "/" + f.getFullYear());
            $scope.filtro.fContratacionDe = fechaFinal;
        }
        function initializeTable() {
            oTable = angular.element("#tblClearingTitulos").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "aoColumns": [
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "monedaR", type: "num-html"}
                ]
            });
        }

//        $scope.submitForm = function (event) {
        $scope.Consultar = function(){
            collapseSearchForm();
            obtenerDatos();
            seguimientoBusqueda();
        };

        function obtenerDatos() {

            var gFiltros = {};
            if (getIsin() !== "" && getIsin() !== undefined) {
                gFiltros.isin =  (!$scope.filtro.chcdisin && getIsin() !== "" ? "#" : "") + getIsin();
            }
            gFiltros.fromdate = transformaFechaInv($scope.filtro.fContratacionDe);
            if ($scope.filtro.fContratacionA !== "") {
                gFiltros.ftodate = transformaFechaInv($scope.filtro.fContratacionA);
            }

            gFiltros.cuenta = $scope.filtro.cuenta;

            consultar(gFiltros);
        }
        function consultar(params) {
            growl.addInfoMessage("Enviando consulta.");
            inicializarLoading();
            ConciliacionClearingService.getMovimientoUnificadoEfectivo(onGetMovimientoUnificadoHandler, onRequestErrorHandler, params);
        }
        function onGetMovimientoUnificadoHandler(json, status, header, config) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined || json.resultados === null ||
                    json.resultados.result_mov_unificados_efectivo === undefined) {
                datos = {};
            } else {
                if (json.error !== null && json.error !== "") {
                    growl.addErrorMessage(json.error);
                    $.unblockUI();
                    return false;
                }
                datos = json.resultados.result_mov_unificados_efectivo;
                growl.addInfoMessage(datos.length + " registros obtenidos.");
                var errors = cargarDatos(datos);
            }
            $.unblockUI();
        }
        function onRequestErrorHandler(data, status, header, config) {
            growl.addErrorMessage("Se produjo un error al intentar coger datos del servidor: " + status + " " + data);
        }
        function cargarDatos(datos) {
            oTable.fnClearTable();
            var valorDescr = "";
            //var row;
            angular.forEach(datos, function (val, key) {
                if (val.efectivo !== 0) {
                    if (val.descrIsin == null) {
                        valorDescr = "";
                    } else {
                        valorDescr = val.descrIsin;
                    }
                    oTable.fnAddData([
                        "<div class='centrado'>" + val.isin + "</div>",
                        "<div class='centrado'>" + valorDescr + "</div>",
                        "<div class='centrado'>" + val.cdcodigocuentaliq + "</div>",
                        "<div class=''>" + $.number(val.efectivo, 2, ',', '.') + "</div>"
                    ]);
                }
            });
            return false;
        }
        function seguimientoBusqueda() {
            $scope.rastroBusqueda = "";
            var cadenaFiltros = "";
            if ($scope.filtro.cdisin !== "") {
            	if($scope.filtro.chcdisin)
            		cadenaFiltros += " isin: " + document.getElementById("cdisin").value;
            	else
            		cadenaFiltros += " isin distinto: " + document.getElementById("cdisin").value;
            }
            if ($scope.filtro.fContratacionDe !== "") {
                cadenaFiltros += " fContratacionDe: " + $scope.filtro.fContratacionDe;
            }
            if ($scope.filtro.fContratacionA !== "") {
                cadenaFiltros += " fContratacionA: " + $scope.filtro.fContratacionA;
            }
            $scope.rastroBusqueda = cadenaFiltros;
        }
        function getIsin() {
        	var isinCompleto = document.getElementById("cdisin").value; //modificado por alex 19Jan para realizar la busqueda correcta en Chrome
            //var isinCompleto = $scope.filtro.cdisin.trim();
            var guion = isinCompleto.indexOf("-");
            if (guion < 0) {
                return isinCompleto;
            } else {
                return isinCompleto.substring(0, guion).trim();
            }
        }

        $scope.cleanFilter = function ($event) {
        	//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fContratacionA" ).datepicker( "option", "minDate", "");
            $( "#fContratacionDe" ).datepicker( "option", "maxDate", "");

            //
            $scope.filtro = {
            	fContratacionDe: "",
                fContratacionA: "",
                cdisin: "",
                cuenta: "00P",
                chcdisin : false
            };
            $scope.btnInvIsin();
        };

    	$scope.btnInvIsin = function() {

        	  if($scope.filtro.chcdisin){
        		  $scope.filtro.chcdisin = false;

          		  document.getElementById('textBtnInvCdIsin').innerHTML = "<>";
          		  angular.element('#textInvCdIsin').css({"background-color": "transparent", "color": "red"});
          		  document.getElementById('textInvCdIsin').innerHTML = "DISTINTO";
          	  }else{
          		  $scope.filtro.chcdisin = true;

          		  document.getElementById('textBtnInvCdIsin').innerHTML = "=";
          		  angular.element('#textInvCdIsin').css({"background-color": "transparent", "color": "green"});
          		  document.getElementById('textInvCdIsin').innerHTML = "IGUAL";
          	  }
    	}

    }]);
