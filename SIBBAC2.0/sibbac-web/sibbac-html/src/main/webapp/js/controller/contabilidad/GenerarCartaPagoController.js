sibbac20.controller('GenerarCartaPagoController', ['$scope', 'growl', '$document', '$compile', 'GenerarCartaPagoService', 'AliasService', 'ContactosService', function ($scope, growl, $document, $compile, GenerarCartaPagoService, AliasService, ContactosService) {

  //Lista con los cdaliass
  var listaAlias = [];

  // Lista con el contenido a mostrar en el autocomplete de alias
  var availableTagsAlias = [];

  // Tabla de datos
  var oTable = undefined;

  // Filtros del formulario de búsqueda
  var filter = {};

  $(document).ready(function () {
    console.log("GenerarCartaPagoController ready");
    $('#tituloHeader').empty();
    $('#tituloHeader').append("Generación de cartas de pago");

    prepareCollapsion();
    initialize();

    $('.generarCartaPago').prop('disabled', true);

    $("#ui-datepicker-div").remove();

    angular.element( "#fechaDesde, #fechaHasta" ).datepicker ( {
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat: 'MM yy',
      defaultDate: new Date(),
      onClose: function(dateText, inst) {
        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();

        $(this).datepicker('setDate', new Date(year, month, 1));

        if (this.id == "fechaDesde"){
          $("#fechaDesde").focusout();
          $("#fechaHasta").focus();
        }
      }, // onClose
      beforeShow : function(input, inst) {
        $('#ui-datepicker-div').toggleClass('hide-calendar', true);

        if ((datestr = $(this).val()).length > 0) {
          year = datestr.substring(datestr.length-4, datestr.length);
          month = jQuery.inArray(datestr.substring(0, datestr.length-5), $(this).datepicker('option', 'monthNames'));
          $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
          $(this).datepicker('setDate', new Date(year, month, 1));
        }

        var other = this.id == "fechaDesde" ? "#fechaHasta" : "#fechaDesde";
        var option = this.id == "fechaDesde" ? "maxDate" : "minDate";

        if ((selectedDate = $(other).val()).length > 0) {
          year = selectedDate.substring(selectedDate.length-4, selectedDate.length);
          month = jQuery.inArray(selectedDate.substring(0, selectedDate.length-5), $(this).datepicker('option', 'monthNames'));
          $(this).datepicker( "option", option, new Date(year, month, 1));
        }
      } // beforeShow
    }); // datepicker

    $('input#fechaPago').datepicker({
      defaultDate: new Date()
    }); //  $('input#fechaPago').datepicker

    oTable = $("#tblOperacionesPago").dataTable({
      "dom": 'T<"clear">lfrtip',
      "tableTools": {
        "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
      },
      "language": {
        "url": "i18n/Spanish.json"
      },
      "scrollY": "300px",
      "scrollCollapse": true,
      "paging": false,
      "aoColumns" : [
        {"sClass": "centrar", "type": "date-eu"}, // Fecha de ejecución
        {"sClass": "centrar", "bSortable": false}, // Referencia
        {"sClass": "centrar", "bSortable": false}, // Tipo de operación
        {"sClass": "align-left", "bSortable": false}, // Valor
        {"sClass": "centrar", "bSortable": false}, // Isin
        {"sClass": "align-right", "type": "formatted-num"}, // Número de títulos
        {"sClass": "align-right", "type": "formatted-num"}, // Efectivo
        {"sClass": "align-right", "type": "formatted-num"} // Devolución
      ],
      "fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
        var totalTitulos = 0;
        var totalEfectivo = 0;
        var totalDevolucion = 0;

        for ( var i = 0 ; i < aaData.length ; i++ ) {
          totalTitulos += parseFloat(aaData[i][5].replace(".", "").replace(",", "."));
          totalEfectivo += parseFloat(aaData[i][6].replace(".", "").replace(",", "."));
          totalDevolucion += parseFloat(aaData[i][7].replace(".", "").replace(",", "."));
        }

        var nCells = nRow.getElementsByTagName('th');
        nCells[5].innerHTML = $.number(totalTitulos, 2, ',', '.');
        nCells[6].innerHTML = $.number(totalEfectivo, 2, ',', '.');
        nCells[7].innerHTML = $.number(totalDevolucion, 2, ',', '.');
      },
      "fnCreatedRow" : function(row, data, index) {
        $compile(row)($scope);
      },
      "sort":[1]
    }); // $("#tblOperacionesPago").dataTable

    // Botón de buscar del dormulario de búsqueda
    $('#formBusqueda').submit(function (event) {
      event.preventDefault();
      inicializarLoading();
      prepareSearchFilter();
      collapseSearchForm();
      seguimientoBusqueda();
      GenerarCartaPagoService.findPayLetter(findPayLetterSuccessHandler, onAjaxErrorHandler, filter);
    });

    // Botón de limpieza de los campos del formulario de búsqueda
    $('#limpiar').click(function (event) {
      event.preventDefault();
      $('input[type=text]').val('');
      $('select').prop('selectedIndex',0);
      $('input[type=checkbox]').attr('checked', false);
      $('.mensajeBusqueda').empty();
      $('.generarCartaPago').prop('disabled', true);
    });

    $('.generarCartaPago').click(function (event) {
      event.preventDefault();

      if (typeof filter.alias !== 'undefined') {
        $('#ui-datepicker-div').toggleClass('hide-calendar');

        document.getElementById('textAlias2').value = filter.aliasConTexto;
        document.getElementById('comissionType2').value = $("#comissionType option[value="+filter.comissionType+"]").text();
        document.getElementById('fechaDesde2').value = filter.fechaDesde;
        document.getElementById('fechaHasta2').value = filter.fechaHasta;

        $("#formularioGenerarCartaDePago").show();
      } else {
        fErrorTxt("Debe realizar un filtrado previo mediante el formulario de búsqueda", 2);
      }
    });
  }); // $(document).ready

  /**
   * Inicializa los campos del formulario.
   */
  function initialize() {
    inicializarLoading();
    GenerarCartaPagoService.initPayLetter(initPayLetterSuccessHandler, onAjaxErrorHandler);
    AliasService.getAlias().success(aliasSuccessHandler).error(onAjaxErrorHandler);
    $.unblockUI();
  } // initialize

  /**
   * Manejador para procesar los datos recibidos de inicialización del formulario.
   */
  function initPayLetterSuccessHandler(json, status, headers, config){
    var filtro = json.resultados.datosInicializacion;

    var select = $("select#comissionType");
    select.append("<option value=''>Select one comission type ...</option>");
    $.each(filtro.comissionType, function(index, value) {
      select.append("<option value='" + index +"'>" + value +"</option>");
    });
  } // initPayLetterSuccessHandler

  /**
   * Manejador genérico para tratar los errores en las llamadas ajax.
   */
  function onAjaxErrorHandler(data, status, headers, config) {
    growl.addErrorMessage('Ocurrió un error al intentar hacer la peticion ajax: ' + data);
    $.unblockUI();
  } // onAjaxErrorHandler

  /**
   * Carga la lista de alias y modifica el campo 'textAlias' del formulario para que sea un campo auntocompletable.
   */
  function aliasSuccessHandler(json, status, headers, config) {
    var datos = undefined;
    if (json === undefined || json.resultados === undefined || json.resultados.result_alias === undefined) {
      datos = {};
    } else {
      datos = json.resultados.result_alias;
      var item = null;

      for ( var k in datos) {
        item = datos[k];
        listaAlias.push(item.alias);
        ponerTag = item.alias + " - " + item.descripcion;
        availableTagsAlias.push(ponerTag);
      } // for

      $("#textAlias").autocomplete({
        source : availableTagsAlias
      });
    } // else
  } // aliasSuccessHandler

  /**
   * Manejador para procesar los datos recibidos de la búsqueda.
   */
  function findPayLetterSuccessHandler(json, status, headers, config){
	  //aqui debo conectar este alias con el dato descreli o el que corresponsa
    console.log("findPayLetterSuccessHandler");
    var datos = undefined;
    if (json === undefined
        || json.resultados === undefined
        || json.resultados === null
        || json.resultados.resultado === null
        || json.resultados.resultado === undefined) {
      console.log("findPayLetterSuccessHandler VACIO");
      datos={};
    } else {
      console.log("findPayLetterSuccessHandler entra");
      datos = json.resultados.resultado;
      var errors = cargarTabla(datos);
    }
    $.unblockUI();
  } // findPayLetterSuccessHandler

  /**
   * Cierra la ventana modal con el formulario.
   */
  $scope.cerrarModal = function() {
    angular.element('#formularioGenerarCartaDePago').hide();
  } // $scope.cerrarModal

  /**
   * Obtiene y válida los datos del formulario de busqueda.
   */
  function prepareSearchFilter() {
    var seleccionListaAlias = document.getElementById("textAlias").value;
    var posicionAlias = availableTagsAlias.indexOf(seleccionListaAlias);

    if(posicionAlias == "-1") {
      alert("El alias introducido no existe. Seleccione uno de la lista de autocompletado.");
      return false;
    }

    var valorSeleccionadoAlias = listaAlias[posicionAlias];

    // Fecha desde
    var datestr = $("#fechaDesde").val();
    var yearDesde = datestr.substring(datestr.length-4, datestr.length);
    var monthDesde = jQuery.inArray(datestr.substring(0, datestr.length-5), $("#fechaDesde").datepicker('option', 'monthNames'));
    var dateDesde = new Date(yearDesde, monthDesde, 1);

    // Fecha hasta
    datestr = $("#fechaHasta").val();
    var yearHasta = datestr.substring(datestr.length-4, datestr.length);
    var monthHasta = jQuery.inArray(datestr.substring(0, datestr.length-5), $("#fechaHasta").datepicker('option', 'monthNames'));
    var dateHasta = new Date(yearHasta, parseInt(monthHasta) + 1, 0);

    filter = {
        "alias":valorSeleccionadoAlias,
        "aliasConTexto":document.getElementById("textAlias").value,
        "comissionType":$("select#comissionType").val(),
        "fechaDesde":("0" + dateDesde.getDate()).slice(-2)+"/"+("0" + (dateDesde.getMonth()+1)).slice(-2)+"/"+dateDesde.getFullYear(),
        "fechaHasta":("0" + dateHasta.getDate()).slice(-2)+"/"+("0" + (dateHasta.getMonth()+1)).slice(-2)+"/"+dateHasta.getFullYear(),
        "searchType":$('input#searchType').is(':checked')
    };

    $("#tblOperacionesPago > tbody").html("");
  } // prepareSearchFilter

  /**
   *
   * @param datos
   * @returns {Boolean}
   */
  function cargarTabla(datos){
    console.log("cargarTabla inicio");

    var feejeliq;
    var nbooking;
    var tpoper;
    var nbvalor;
    var isin;
    var titulos;
    var efectivo;
    var devolucion;

    oTable.fnClearTable();

    $.each(datos, function (key, val) {
      feejeliq = (val.feejeliq != null && val.feejeliq != undefined) ? val.feejeliq : '-';
      nbooking = (val.nbooking != null && val.nbooking != undefined) ? val.nbooking : '-';
      tpoper = (val.tpoper != null && val.tpoper != undefined) ? val.tpoper : '-';
      nbvalor = (val.nbvalor != null && val.nbvalor != undefined) ? val.nbvalor : '-';
      isin = (val.isin != null && val.isin != undefined) ? val.isin : '-';
      titulos = (val.titulos != null && val.titulos != undefined) ? val.titulos : 0;
      efectivo = (val.efectivo != null && val.efectivo != undefined) ? val.efectivo : 0;
      devolucion = (val.devolucion != null && val.devolucion != undefined) ? val.devolucion : 0;

      oTable.fnAddData([
        transformaFechaGuion(feejeliq),
        nbooking,
        tpoper,
        nbvalor,
        isin,
        $.number(titulos, 2, ',', '.'),
        $.number(efectivo, 2, ',', '.'),
        $.number(devolucion, 2, ',', '.')
      ], false);
    });

    oTable.$('tr:odd').css('backgroundColor', '#EFEFEF');
    oTable.fnAdjustColumnSizing();
    oTable.fnDraw();

    if (datos.length == 0){
      $('.generarCartaPago').prop('disabled', true);
    } else {
      $('.generarCartaPago').prop('disabled', false);
    }
  } // cargarTabla

  $scope.generarCarta = function(){
		console.log("Entra en la funcion que generara la cartas de Pagos.");
		$('#halias').val( filter.alias );
		$('#hcomissionType').val(filter.comissionType);

		$('#hfechaDesde').val(filter.fechaDesde);
		$('#hfechaHasta').val(filter.fechaHasta);
		$('#hsearchType').val(filter.searchType);

		console.log($('#halias').val());
		console.log($('#hcomissionType').val());
		console.log($('#hfechaDesde').val());
		console.log($('#hfechaHasta').val());
		console.log($('#hsearchType').val());

		fErrorTxt("Se va ha iniciar la generación de Carta de Pago este proceso puede tardar varios minutos", 2);

		$("#formularioGenerarCartaDePago").css('display','none');

		oTable.fnClearTable();
		$("#generarCartaDePago2").submit();
		$(".generarCartaPago").prop('disabled', true);
		return false;
  }

  /**
   * Muestra un texto con los datos por lo que se ha filtrado una busqueda.
   */
  function seguimientoBusqueda(){
    $('.mensajeBusqueda').empty();
    var  cadenaFiltros = "";

    cadenaFiltros += "<br/><ins>Filtros de búsqueda</ins><br/><br/>";

    if ($('#textAlias').val() !== "" && $('#textAlias').val() !== undefined){
      cadenaFiltros += " Alias: "+ $('#textAlias').val();
    }
    if ($('#comissionType').val() !== "" && $('#comissionType').val() !== undefined){
      cadenaFiltros += " :: Tipo de comisi&oacute;n: "+ $("#comissionType option[value='"+$('#comissionType').val()+"']").text();
    }
    if ($('#fechaDesde').val() !== "" && $('#fechaDesde').val() !== undefined){
      cadenaFiltros += " :: Fecha desde: "+ $('#fechaDesde').val();
    }
    if ($('#fechaHasta').val() !== "" && $('#fechaHasta').val() !== undefined){
      cadenaFiltros += " :: Fecha hasta: "+ $('#fechaHasta').val();
    }
    if ($('#searchType').val() !== "" && $('#searchType').val() !== undefined){
      cadenaFiltros += " :: Tipo de b&uacutesqueda (ya cobrados): "+ $('input#searchType').is(':checked');
    }
    $('.mensajeBusqueda').append(cadenaFiltros);
  } // seguimientoBusqueda

}]); // sibbac20.controller
