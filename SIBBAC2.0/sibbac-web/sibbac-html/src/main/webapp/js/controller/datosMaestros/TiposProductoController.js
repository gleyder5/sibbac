sibbac20.controller('TiposProductoController', ['$scope', 'DatosMaestrosService', '$document', 'growl', function ($scope, DatosMaestrosService, $document, growl) {
        var oTable = undefined;
        function consultar() {
            inicializarLoading();
            DatosMaestrosService.getTiposProducto(onSuccessConsultaRequest, onErrorRequest, {});
        }
        function onErrorRequest(data, status, headers, config) {
            $.unblockUI();
            growl.addErrorMessage("Ocurrió un error durante la petición de datos al servidor de datos." + data);
        }
        function onSuccessConsultaRequest(json) {
            var datos = undefined;
            if (json === undefined || json === null || json.error === undefined || (json.error !== null && json.error.error !== "")) {
                if (json === null || json.error === undefined) {
                    growl.addErrorMessage("Ocurrió un error de conexión con el servidor.");
                } else {
                    growl.addErrorMessage(json.error);
                }
            } else {
                if (json === undefined || json.resultados === undefined || json.resultados.datosMaestros === undefined) {
                    datos = {};
                } else {
                    datos = json.resultados.datosMaestros;
                }
                populateData(datos);
            }
            $.unblockUI();
        }
        function populateData(datos) {
            var difClass = 'centrado';
            oTable.fnClearTable();
            angular.forEach(datos, function (val, key) {
                var codigo = '';
                var descripcion = '';
                var trdType = '';
                var trdSubType = '';
                if (val.codigo !== undefined && val.codigo !== null) {
                    codigo = val.codigo;
                }
                if (val.descripcion !== undefined && val.descripcion !== null) {
                    descripcion = val.descripcion;
                }
                if (val.trdType !== undefined && val.trdType !== null) {
                    trdType = val.trdType;
                }
                if (val.trdSubType !== undefined && val.trdSubType !== null) {
                    trdSubType = datos[key].trdSubType;
                }
                oTable.fnAddData([
                    codigo,
                    descripcion,
                    trdType,
                    trdSubType
                ]);
            });
        }
        $document.ready(function () {
            if (oTable === undefined) {
                loadData();
            }
            consultar();
        });
        function loadData() {
            oTable = $("#tblTiposProducto").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "js/swf/copy_csv_xls_pdf.swf"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "language": {
                    "url": "i18n/Spanish.json"
                }
            });
        }
    }]);



