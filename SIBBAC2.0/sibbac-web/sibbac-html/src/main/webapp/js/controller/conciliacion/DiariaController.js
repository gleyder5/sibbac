sibbac20.controller('DiariaController', ['$scope', 'CamaraService', 'IsinService', '$document', 'ConciliacionDiariaService', 'growl', 'CuentaLiquidacionService',
    function ($scope, CamaraService, IsinService, $document, ConciliacionDiariaService, growl, CuentaLiquidacionService) {
	    $scope.cuentas = [];
	    var idOcultosIsin = [];
	    var valorIsin = [];
	    $scope.rutaBusqueda = "";
	    $scope.filtroCuenta = {cdCodigo: "CD"};
        function initFilter() {
            $scope.filters = {
            	"isin": "",
                "camara": "",
                "sentido": "",
                "estado": "",
                "fcontratacionDe": "",
                "fcontratacionA": "",
                "fliquidacionDe": "",
                "fliquidacionA": "",
                "cuentaLiquidacion": "",
                "chisin" : true,
                "chcamara" : true,
                "chsentido" : true,
                "chestado" : true,
                "chcuentaliquidacion" : true,
            };
        }

        /** ISINES **/
        function loadIsines() {
            IsinService.getIsines().success(cargarIsines).error(showError);
        }
        /** CAMARAS **/
        function loadCamaras() {
            CamaraService.getCamaras().success(cargarCamaras).error(showError);
        }
        /** Fecha Inicial **/
        function loadFechaInicial() {
            ConciliacionDiariaService.getFechaInicial().success(cargarFechaInicial).error(showError);
        }

        /** CUENTAS **/
        CuentaLiquidacionService.getCuentasLiquidacionFiltrado($scope.filtroCuenta).success(function (data, status, header, config) {
            if (data !== null && data.resultados !== null && data.resultados.cuentasLiquidacion !== null) {
                $scope.cuentas = data.resultados.cuentasLiquidacion;
            }
        }).error(function (data, status, headers, config) {
            console.log("error al cargar cuentas: " + status);
        });
        //
        function showError(data, status, headers, config) {
            growl.addErrorMessage("No se pudieron extraer datos.");
            $.unblockUI();
        }

        var oTable = undefined;

        function mostrarDatos(datos) {
            oTable.fnClearTable();
            angular.forEach(datos, function (val, key) {
                oTable.fnAddData([
                    "<span>" + datos[key].cdCuenta + "</span>",
                    "<span>" + datos[key].isin + "</span>",
                    "<span>" + datos[key].descIsin + "</span>",
                    "<span>" + datos[key].idAlias + "</span>",
                    "<span>" + datos[key].alias + "</span>",
                    "<span>" + datos[key].camara + "</span>",
                    "<span>" + datos[key].estado.toUpperCase() + "</span>",
                    "<span>" + transformaFecha(datos[key].tradedate) + "</span>",
                    "<span>" + transformaFecha(datos[key].settlementdate) + "</span>",
                    "<span>" + datos[key].sentido + "</span>",
                    "<span>" + $.number(datos[key].nuTitulos, 0, ',', '.') + "</span>",
                    "<span>" + $.number(datos[key].precio, 6, ',', '.') + "</span>"
                ], false);
            });
            oTable.fnDraw();
            $.unblockUI();

        }
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
        function initialize() {

            //var fechas = ['fcontratacionA', 'fcontratacionDe', 'fliquidacionA', 'fliquidacionDe'];
            //verificarFechas([['fcontratacionDe', 'fcontratacionA'], ['fliquidacionDe', 'fliquidacionA']]);
            inicializarLoading();
            $("#ui-datepicker-div").remove();
 			$('input#fcontratacionDe').datepicker({
 			    onClose: function( selectedDate ) {
 			      $( "#fcontratacionA" ).datepicker( "option", "minDate", selectedDate );
 			    } // onClose
 			  });

 			  $('input#fcontratacionA').datepicker({
 			    onClose: function( selectedDate ) {
 			      $( "#fcontratacionDe" ).datepicker( "option", "maxDate", selectedDate );
 			    } // onClose
 			  });
 			 //
  			$('input#fliquidacionDe').datepicker({
  			    onClose: function( selectedDate ) {
  			      $( "#fliquidacionA" ).datepicker( "option", "minDate", selectedDate );
  			    } // onClose
  			  });

  			  $('input#fliquidacionA').datepicker({
  			    onClose: function( selectedDate ) {
  			      $( "#fliquidacionDe" ).datepicker( "option", "maxDate", selectedDate );
  			    } // onClose
  			  });
  			//cargarFechas();
            loadCamaras();
            loadFechaInicial();
            loadIsines();

        }
        function cargarFechas(){
        	$scope.filters.fcontratacionDe = $('#fcontratacionDe').val();
        	$scope.filters.fcontratacionA = $('#fcontratacionA').val();
        	$scope.filters.fliquidacionDe = $('#fliquidacionDe').val();
        	$scope.filters.fliquidacionA = $('#fliquidacionA').val();
        }
        function loadDataTable() {
            oTable = angular.element("#tblDiaria").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "aoColumns": [
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "centrar"},
                    {sClass: "centrar", type: 'date-eu'},
                    {sClass: "centrar", type: 'date-eu'},
                    {sClass: "centrar"},
                    {sClass: "monedaR", type: 'formatted-num'},
                    {sClass: "monedaR", type: 'formatted-num'}
                ]
            });
        }
        $document.ready(function () {
            loadDataTable();
            initFilter();
            initialize();
            ajustarAnchoDivTable();

            prepareCollapsion();

        });
        $scope.cleanFilter = function(event){
        	//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fcontratacionA" ).datepicker( "option", "minDate", "");
            $( "#fcontratacionDe" ).datepicker( "option", "maxDate", "");
            $( "#fliquidacionA" ).datepicker( "option", "minDate", "");
            $( "#fliquidacionDe" ).datepicker( "option", "maxDate", "");
            //
            initFilter();
            $scope.filters.chisin = true;
            $scope.filters.chcamara = true;
            $scope.filters.chsentido = true;
            $scope.filters.chestado = true;
            $scope.filters.chcuentaliquidacion = true;

    		$scope.btnInvGenerico('Isin');
    		$scope.btnInvGenerico('Camara');
    		$scope.btnInvGenerico('Sentido');
    		$scope.btnInvGenerico('Estado');
    		$scope.btnInvGenerico('cuentaLiquidacion');
        }
//        $scope.submitForm = function (event) {
        $scope.Consultar = function(event) {
            obtenerDatos();
            event.preventDefault();
            collapseSearchForm();
            seguimientoBusqueda();
            return false;
        }

        function seguimientoBusqueda() {
        	cargarFechas();
            $scope.rutaBusqueda = "";
            var cadenaFiltros = "";
            if ($scope.filters.fcontratacionDe !== "") {
                $scope.rutaBusqueda += " fcontratacionDe: " + $scope.filters.fcontratacionDe;
            }
            if ($scope.filters.fcontratacionA !== "") {
                $scope.rutaBusqueda += " fcontratacionA: " + $scope.filters.fcontratacionA;
            }
            if ($scope.filters.fliquidacionDe !== "") {
                $scope.rutaBusqueda += " fliquidacionDe: " + $scope.filters.fliquidacionDe;
            }
            if ($scope.filters.fliquidacionA !== "") {
                $scope.rutaBusqueda += " fliquidacionA: " + $scope.filters.fliquidacionA;
            }
            if ($scope.filters.isin !== "") {
            	if ($scope.filters.chisin)
                	$scope.rutaBusqueda += " isin: " + document.getElementById("cdisin").value;
            	else
            		$scope.rutaBusqueda += " isin distinto: " + document.getElementById("cdisin").value;
            }
            if ($scope.filters.camara !== "") {
            	if ($scope.filters.chcamara)
            		$scope.rutaBusqueda += " camara: " + $scope.filters.camara;
            	else
            		$scope.rutaBusqueda += " camara distinta: " + $scope.filters.camara;
            }
            if ($scope.filters.sentido !== "") {
            	if ($scope.filters.chsentido)
            		$scope.rutaBusqueda += " sentido: " + $scope.filters.sentido;
            	else
            		$scope.rutaBusqueda += " sentido distinto: " + $scope.filters.sentido;
            }
            if ($scope.filters.estado !== "") {
            	if ($scope.filters.chestado)
            		$scope.rutaBusqueda += " estado: " + $scope.filters.estado;
            	else
            		$scope.rutaBusqueda += " estado distinto: " + $scope.filters.estado;
            }
            if ($scope.filters.cuentaLiquidacion !== "") {
            	if ($scope.filters.chcuentaliquidacion)
            		$scope.rutaBusqueda += " cuentaLiquidacion: " + $scope.filters.cuentaLiquidacion;
            	else
            		$scope.rutaBusqueda += " cuentaLiquidacion distinta: " + $scope.filters.cuentaLiquidacion;
            }

        }

        function obtenerDatos() {
        	cargarFechas();

        	var isin = (!$scope.filters.chisin && getIsin() !== "" ? "#" : "") + getIsin();
        	var camara = (!$scope.filters.chcamara && getCamara() !== "" ? "#" : "") + getCamara();
        	var estado = (!$scope.filters.chestado && $scope.filters.estado !== "" ? "#" : "") + $scope.filters.estado;
        	var sentido = (!$scope.filters.chsentido && $scope.filters.sentido !== "" ? "#" : "") + $scope.filters.sentido;
        	var cuentaLiquidacion = (!$scope.filters.chcuentaliquidacion && $scope.filters.cuentaLiquidacion !== "" ? "#" : "") + $scope.filters.cuentaLiquidacion;

            var params = {"isin": isin,
                "camara": camara,
                "sentido":sentido,
                "estado": estado,
                "fcontratacionDe": transformaFechaInv($scope.filters.fcontratacionDe),
                "fcontratacionA": transformaFechaInv($scope.filters.fcontratacionA),
                "fliquidacionDe": transformaFechaInv($scope.filters.fliquidacionDe),
                "fliquidacionA": transformaFechaInv($scope.filters.fliquidacionA),
                "idCuentaLiquidacion":cuentaLiquidacion
            };

            inicializarLoading();
            ConciliacionDiariaService.getOperaciones(params, mostrarDatos, showError);

        }

        function cargarFechaInicial(data, status, headers, config) {
            if (data.resultados.fcontratacionIni !== null) {
                var fecha = data.resultados.fcontratacionIni;
                $scope.filters.fcontratacionDe = transformaFecha(fecha);
                $scope.filters.fcontratacionA = transformaFecha(fecha);
            }



        }

        function cargarCamaras(data, status, headers, config) {
            if (data.resultados.result_camara !== null) {
                var datosCamara = data.resultados.result_camara;
                var ponerTagCamara = "";
                var availableTagsCamara = [];
                angular.forEach(datosCamara, function (val, key) {
                    ponerTagCamara = datosCamara[key].cdCodigo + " - " + datosCamara[key].nbDescripcion;
                    availableTagsCamara.push(ponerTagCamara);
                });
                //código de autocompletar
                angular.element("#cdcamara").autocomplete({
                    source: availableTagsCamara

                });
            }
        }
        function cargarIsines(data, status, headers, config) {
            if (data.resultados.result_isin !== null) {
                var datosIsin = data.resultados.result_isin;
                var ponerTag = "";
                var availableTags = [];
                angular.forEach(datosIsin, function (val, key) {
                    ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                    availableTags.push(ponerTag);
                    //código de autocompletar
                    angular.element("#cdisin").autocomplete({
                        source: availableTags
                    });
                });
                console.log("availableTags: "+availableTags.length);
                $.unblockUI();
            }
			//$.unblockUI();
            //ALEX 5 feb, de esta manera logramos cargar los limites de fechas
            //console.log("tenemos: ",document.getElementById("fcontratacionDe").value);
            $( "#fcontratacionA" ).datepicker( "option", "minDate", document.getElementById("fcontratacionDe").value);

        }

        //exportar
        function SendAsExport(format) {
            var c = this.document.forms['diaria'];
            var f = this.document.forms["export"];
            console.log("Forms: " + f.name + "/" + c.name);
            f.action = "/sibbac20back/rest/export." + format;
            var json = {
                "service": "SIBBACServiceConciliacionDiaria",
                "action": "getOperacionesDiaria",
                "filters": {"isin": getIsin(),
                    "camara": getCamara(),
                    "sentido": $("#sentido").val(),
                    "estado": $("#estado").val(),
                    "fcontratacionDe": transformaFechaInv($("#fcontratacionDe").val()),
                    "fcontratacionA": transformaFechaInv($("#fcontratacionA").val()),
                    "fliquidacionDe": transformaFechaInv($("#fliquidacionDe").val()),
                    "fliquidacionA": transformaFechaInv($("#fliquidacionA").val())}};
            if (json != null && json != undefined && json.length == 0) {
                alert("Introduzca datos");
            } else {
                f.webRequest.value = JSON.stringify(json);
                f.submit();
            }
        }

        function getIsin() {
        	var isinCompleto = document.getElementById("cdisin").value; //modificado por alex 19Jan para realizar la busqueda correcta en Chrome
            //var isinCompleto = $scope.filters.isin.trim();
            var guion = isinCompleto.indexOf("-");
            if (guion < 0) {
                return isinCompleto;
            } else {
                return isinCompleto.substring(0, guion).trim();
            }
        }

        function getCamara() {
            var camaraCompleto = $scope.filters.camara.trim();
            var guion = camaraCompleto.indexOf("-");
            if (guion < 0) {
                return camaraCompleto;
            } else {
                return camaraCompleto.substring(0, guion).trim();
            }
        }

    	$scope.btnInvIsin = function() {
    		$scope.btnInvGenerico('Isin');
    	}

    	$scope.btnInvCamara = function() {
    		$scope.btnInvGenerico('Camara');
    	}

    	$scope.btnInvSentido = function() {
    		$scope.btnInvGenerico('Sentido');
    	}

    	$scope.btnInvEstado = function() {
    		$scope.btnInvGenerico('Estado');
    	}

    	$scope.btnInvCuentaLiquidacion = function() {
    		$scope.btnInvGenerico('cuentaLiquidacion');
    	}

    	$scope.btnInvGenerico = function( nombre ) {

    	  var check = '$scope.filters.ch' + nombre.toLowerCase();
    	  console.log("check: " + check)

      	  if(eval(check)){
      		  	  var valor2 = check + "= false";
      		  	  eval(valor2 ) ;

        		  document.getElementById('textBtnInv'+nombre).innerHTML = "<>";
        		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "red"});
        		  document.getElementById('textInv'+nombre).innerHTML = "DISTINTO";
        	  }else{
               	  var valor2 = check + "= true";
            	  eval(valor2 ) ;

        		  document.getElementById('textBtnInv'+nombre).innerHTML = "=";
        		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "green"});
        		  document.getElementById('textInv'+nombre).innerHTML = "IGUAL";
        	  }
        }


    }]);
