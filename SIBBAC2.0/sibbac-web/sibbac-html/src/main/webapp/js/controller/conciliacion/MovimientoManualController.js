sibbac20.controller('MovimientoManualController', ['$rootScope', '$location', '$scope', 'MovimientoManualService', '$compile', '$document', 'growl', 'CuentaLiquidacionService', 'IsinService', 'CamaraService', 'AliasService', 'SecurityService', function ($rootScope, $location, $scope, MovimientoManualService, $compile, $document, growl, CuentaLiquidacionService, IsinService, CamaraService, AliasService, SecurityService) {


    if (SecurityService.inicializated) {
 	   if (!SecurityService.isPermisoAccion($rootScope.SecurityActions.CREAR, $location.path())) {
           growl.addErrorMessage("No tiene permiso para acceder a esta funcionalidad");
           $location.path("/");
 	   }
    } else {
        $scope.$watch(security.event.initialized, function() {
           if (!SecurityService.isPermisoAccion($rootScope.SecurityActions.CREAR, $location.path())) {
                 growl.addErrorMessage("No tiene permiso para acceder a esta funcionalidad");
                 $location.path("/");
       	   }
        });
    }

        var listaIsin = [];
        var listaCamara = [];
        var availableTags = [];
        var availableTagsCamara = [];
        var listaAlias = [];
        var availableTagsAlias = [];

        $scope.cuentaLiqArray = [];
        $scope.cuentaLiqEfectivoArray = [];

        $scope.fliquidacion = "";
        $scope.fcontratacion = "";

        $scope.filtro = {"titulos": "",
            "cdisin": "",
            "sentido": "",
            "cdcamara": "",
            "fliquidacion": "",
            "fcontratacion": "",
            "efectivo": "",
            "alias": "",
            "idCuentaLiq": 0,
            "desCuentaLiq": "",
            "idCuentaLiqEfectivo": 0,
            "desCuentaLiqEfectivo": ""};

        $scope.cuentas = "";

        function onErrorHandler(data, status, headers, config) {
            growl.addErrorMessage('Ocurrió un error al intentar hacer la consulta: ' + data);
            $.unblockUI();
        }

        function insertMovimientoManualSuccess(json, status, headers, config) {
            var datos = {};
            var error = false;

            if (json !== undefined &&
                    json.resultados !== undefined &&
                    json.resultados !== null) {
                if (json.error !== null) {
                    growl.addErrorMessage(json.error);
                } else {

                    if (json.resultados.result_movimiento_manual !== undefined && json.resultados.result_movimiento_manual !== null) {
                        datos = json.resultados.result_movimiento_manual;
                    }


                }
            }
            else
            {
                if (json !== undefined && json.error !== null) {
                    growl.addErrorMessage(json.error);
                    error = true;
                }
            }

            if (error !== true)
            {
                growl.addSuccessMessage(datos);
                $scope.limpiarFiltro();

            }

            $.unblockUI();
        }

        function insertado(params) {

            inicializarLoading();
            MovimientoManualService.insertMovimientoManual(insertMovimientoManualSuccess, onErrorHandler, params);
        }




        function obtenerDatos() {

            $scope.filtro.fliquidacion = transformaFechaInv($scope.fliquidacion);
            $scope.filtro.fcontratacion = transformaFechaInv($scope.fcontratacion);
            $scope.filtro.desCuentaLiq = $scope.filtro.idCuentaLiq;
            $scope.filtro.desCuentaLiqEfectivo = $scope.filtro.idCuentaLiqEfectivo;
            $scope.filtro.cdcamara = angular.element("#cdcamara").val();
            $scope.filtro.alias = angular.element("#alias").val();
            $scope.filtro.cdisin = angular.element("#cdisin").val();
            var params = [];
            params.push($scope.filtro);
            insertado(params);
        }

        function cargarIsines(datosIsin) {
            var availableTags = [];
            angular.forEach(datosIsin, function (val, key) {
                var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                availableTags.push(ponerTag);
            });
            angular.element("#cdisin").autocomplete({
                source: availableTags
            });
        }

        function cargarCamaras(data, status, headers, config) {
            if (data.resultados.result_camara !== null) {
                var datosCamara = data.resultados.result_camara;
                var ponerTagCamara = "";
                var availableTagsCamara = [];
                angular.forEach(datosCamara, function (val, key) {
                    ponerTagCamara = datosCamara[key].cdCodigo + " - " + datosCamara[key].nbDescripcion;
                    availableTagsCamara.push(ponerTagCamara);
                });
                //código de autocompletar
                angular.element("#cdcamara").autocomplete({
                    source: availableTagsCamara

                });
            }
        }


        function cargaAlias(data) {
            var listaAlias = data.resultados.result_alias;
            growl.addInfoMessage(listaAlias.length + " Alias cargados.");

            angular.forEach(listaAlias, function (val, key) {
                var ponerTag = {label: val.alias + " - " + val.descripcion,
                    id: val.alias, descripcion: val.descripcion};
                availableTagsAlias.push(ponerTag);
            });
            // código de autocompletar
            angular.element("#alias").autocomplete({
                source: availableTagsAlias,
            });
            $.unblockUI();
        }

        function cargaCombos() {
            CuentaLiquidacionService.getCuentasLiquidacion().success(function (data, status, header, config) {
                if (data !== null && data.resultados !== null && data.resultados.cuentasLiquidacion !== null) {
                    $scope.cuentas = data.resultados.cuentasLiquidacion;
                }
            }).error(function (data, status, headers, config) {
                console.log("error al cargar cuentas: " + status);
            });

            IsinService.getIsines(cargarIsines, onErrorHandler);
            CamaraService.getCamaras().success(cargarCamaras).error(onErrorHandler);
            AliasService.getAlias().success(cargaAlias).error(onErrorHandler);
        }

        function initialize() {
            var fechas = ['fliquidacion', 'fcontratacion'];
            verificarFechas([['fcontratacion', 'fliquidacion']]);
            cargaCombos();
            angular.element("#efectivo").keypress(justNumbersD)

            angular.element("#titulos").keypress(justNumbersD);
            angular.element('#idCuentaLiqEfectivo').val("0");
            angular.element('#idCuentaLiq').val("0");

        }

        $(document).ready(function () {
            initialize();



            /*Eva:Función para cambiar texto*/
            jQuery.fn.extend({
                toggleText: function (a, b) {
                    var that = this;
                    if (that.text() != a && that.text() != b) {
                        that.text(a);
                    } else if (that.text() == a) {
                        that.text(b);
                    } else if (that.text() == b) {
                        that.text(a);
                    }
                    return this;
                }
            });
            /*FIN Función para cambiar texto*/

        });

        $scope.insertar = function (event) {
            var efectivoPattern = new RegExp(/^\d{1,15}\b.?\d{0,2}?$/);
            var titulosPattern = new RegExp(/^\d{1,12}\b.?\d{0,6}?$/);

            var alias = angular.element("#alias").val();
            var cdisin = angular.element("#cdisin").val();

            //console.log( '$scope.filtro.idCuentaLiq '+$scope.filtro.idCuentaLiq );
            //console.log( '$scope.filtro.idCuentaLiqEfectivo '+$scope.filtro.idCuentaLiqEfectivo );

            var hayValidarTitulos = false;
            var hayValidarEfectivo = false;

            if ($scope.filtro.titulos !== null && $scope.filtro.titulos !== "")
            {
                var titulos = parseFloat($scope.filtro.titulos);
                if (titulos === 0)
                {
                    $scope.filtro.titulos = "";
                }
                else
                {
                    hayValidarTitulos = true;
                }
            }

            if ($scope.filtro.efectivo !== null && $scope.filtro.efectivo !== "")
            {
                var efectivo = parseFloat($scope.filtro.efectivo);
                if (efectivo === 0)
                {
                    $scope.filtro.efectivo = "";
                }
                else
                {
                    hayValidarEfectivo = true;
                }
            }

            var fContratacion = angular.element("#fcontratacion").val();

            if (fContratacion !== "") {
                var now = new Date();

                if(moment(fContratacion, "DD/MM/YYYY").isAfter(now)){
                    alert("La fecha de contratación introducida, no es correcta, no se permiten contratacines a futuro.");
                    return false;
                }
            }
            if ((hayValidarTitulos || hayValidarEfectivo) && $scope.filtro.sentido !== null && $scope.filtro.sentido !== "" && cdisin !== null && cdisin !== "" &&
                    alias !== null && alias !== "" &&
                    ($scope.filtro.idCuentaLiq !== null && $scope.filtro.idCuentaLiq !== 0 && $scope.filtro.idCuentaLiq !== "0" || $scope.filtro.idCuentaLiqEfectivo !== null && $scope.filtro.idCuentaLiqEfectivo !== 0) &&
                    $scope.fliquidacion !== null && $scope.fliquidacion !== "") {

                if (($scope.filtro.idCuentaLiqEfectivo !== 0 && $scope.filtro.idCuentaLiqEfectivo !== "0") && !hayValidarEfectivo) {
                    alert("No se ha introducido ningun valor en efectivo pero se ha seleccionado un valor del combo 'Cuenta Liquidacion Efectivo'");
                } else if (($scope.filtro.idCuentaLiqEfectivo === 0 || $scope.filtro.idCuentaLiqEfectivo === "0") && hayValidarEfectivo) {
                    alert("Se ha introducido un valor en efectivo pero no se ha seleccionado un valor del combo 'Cuenta Liquidacion Efectivo'");
                } else if (($scope.filtro.idCuentaLiq === 0 || $scope.filtro.idCuentaLiq === "0") && hayValidarTitulos) {
                    alert("Se ha introducido un valor en titulos pero no se ha seleccionado un valor del combo 'Cuenta Liquidacion Titulos'");
                } else if (($scope.filtro.idCuentaLiq !== 0 && $scope.filtro.idCuentaLiq !== "0") && !hayValidarTitulos) {
                    alert("No se ha introducido ningun valor en titulos pero se ha seleccionado un valor del combo 'Cuenta Liquidacion Titulos'");
                } else if (hayValidarTitulos && !titulosPattern.test($scope.filtro.titulos)) {
                    alert("El valor de titulos tiene que ser numérico y tener un maximo de 12 enteros y 6 decimales");
                } else if (hayValidarEfectivo && !efectivoPattern.test($scope.filtro.efectivo)) {
                    alert("El valor de efectivo tiene que ser numérico y tener un maximo de 15 enteros y 2 decimales");
                } else {
                    obtenerDatos();
                }
            } else {
                alert("Se deben informar todos los campos.");
            }
        };

        $scope.limpiarFiltro = function () {
            angular.element('input[type=text]').val('');
            angular.element('select').val('0');
            $scope.filtro = {"titulos": "",
                "cdisin": "",
                "sentido": "",
                "cdcamara": "",
                "fliquidacion": "",
                "fcontratacion": "",
                "efectivo": "",
                "alias": "",
                "idCuentaLiq": 0,
                "desCuentaLiq": "",
                "idCuentaLiqEfectivo": 0,
                "desCuentaLiqEfectivo": ""};
        };

    }]);
