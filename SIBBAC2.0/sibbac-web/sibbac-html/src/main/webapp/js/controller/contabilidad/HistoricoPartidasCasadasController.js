sibbac20
		.controller(
				'HistoricoPartidasCasadasController',
				[
						'$scope',
						'$filter',
						'GestionUsuariosService',
						'HistoricoPartidasCasadasService',
						'ConciliacionBancariaCombosService',
						'$document',
						'growl',
						'$compile',
						function($scope, $filter, GestionUsuariosService, HistoricoPartidasCasadasService, ConciliacionBancariaCombosService, $document, growl, $compile) {
							
							
           $scope.safeApply = function (fn) {
               var phase = this.$root.$$phase;
               if (phase === '$apply' || phase === '$digest') {
                 if (fn && (typeof (fn) === 'function')) {
                   fn();
                 }
               } else {
                 this.$apply(fn);
               }
             };

             var hoy = new Date();
             var dd = hoy.getDate();
             var mm = hoy.getMonth() + 1;
             var yyyy = hoy.getFullYear();
             hoy = yyyy + "_" + mm + "_" + dd;

             $scope.modalHistorico = {
               titulo : "Partidas Casadas",
               showSIBBAC : true,
             };

             $scope.showingFilter = true;

             $scope.listHistoricos = [];
             $scope.listHistoricosSeleccionados = [];
             $scope.listUsuarios = [];
             
             $scope.pestana = "TLM";
             $scope.oTableTLMCreada = false;
             
             $scope.desglosesSIBBAC = [];
             $scope.partidasCasadas = [];
             
             $scope.isActive = function (pestana) { 
                 return pestana === $scope.pestana;
             };
             
             $scope.activarPestana = function(pestana){
            	 $scope.pestana = pestana;
            	 $scope.createOTableSIBBAC();
            	 
             };
             
             $scope.resetFiltro = function () {
                 $scope.filtro = {
                   usuario : "",
                   fechaHasta : "",
                   fechaDesde : ""
                 };
                 $scope.usuarioFiltro = null;
             };
             
             $scope.resetFiltro();
             
			 $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col ) {
				  return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
				    return $('input', td).prop('checked') ? '1' : '0';
				  })
				};
             
             $scope.oTable = $("#datosHistoricos").dataTable({
                 "dom" : 'T<"clear">lfrtip',
                 "tableTools" : {
                   "aButtons" : [ "print" ]
                 },
                 "aoColumns" : [ {
                   sClass : "centrar",
                   width : "10%", 
                   orderDataType: "dom-checkbox"
                 }, {
                   sClass : "centrar",
                   width : "30%"
                 }, {
                   sClass : "centrar",
                   width : "30%"
                 }, {
                   sClass : "centrar",
                   width : "30%",
                   sType : "date-eu"
                 } ],
                 "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                   $compile(nRow)($scope);
                 },

                 "scrollY" : "480px",
                 "scrollX" : "100%",
                 "scrollCollapse" : true,
                 "language" : {
                   "url" : "i18n/Spanish.json"
                 }
             });
             
			 $scope.consultarUsuarios = function () {
				 ConciliacionBancariaCombosService.consultarUsuarios(function (data) {
					 if (data.resultados.status == 'KO') {
						 fErrorTxt("Se produjo un error en la carga del combo de usuarios", 1);
					 } else {
						 $scope.listUsuarios = data.resultados["listaUsuarios"];
					 }
				 }, function (error) {
					 fErrorTxt("Se produjo un error en la carga del combo de usuarios", 1);
				 });
			 };

			 $scope.consultarUsuarios();
             
					
             $scope.seleccionarElemento = function (row) {
                 if ($scope.listHistoricos[row].selected) {
                   $scope.listHistoricos[row].selected = false;
                   for (var i = 0; i < $scope.listHistoricosSeleccionados.length; i++) {
                     if ($scope.listHistoricosSeleccionados[i].id === $scope.listHistoricos[row].id) {
                       $scope.listHistoricosSeleccionados.splice(i, 1);
                     }
                   }
                 } else {
                   $scope.listHistoricos[row].selected = true;
                   $scope.listHistoricosSeleccionados.push($scope.listHistoricos[row]);
                   $scope.idListHistoricos = row;
                 }
            };
            
            var pupulateAutocomplete = function (input, availableTags) {
                $(input).autocomplete({
                  minLength : 0,
                  source : availableTags,
                  focus : function (event, ui) {
                    return false;
                  },
                  select : function (event, ui) {

                    var option = {
                      key : ui.item.key,
                      value : ui.item.value
                    };

                    switch (input) {
                      case "#filtro_usuario":
                    	  $scope.filtro.usuario = option.key;
                          $scope.usuarioFiltro = option.value;
                        break;
                      default:
                        break;
                    }

                    $scope.safeApply();
                    return false;
                  }
                });
            };          
            
            $scope.consultarHistorico = function () {
            	$scope.mostrarErrorFiltro = $scope.validacionFiltroHistorico();
            	if (!$scope.mostrarErrorFiltro) {
            		$scope.showingFilter = false;
	                inicializarLoading();
	
	                $scope.listHistoricosSeleccionados = [];
	
	                HistoricoPartidasCasadasService.consultarHistorico(function (data) {
	                	
	                     if (data.resultados.status === 'KO') {
	                       $.unblockUI();
	                       fErrorTxt("Se produjo un error en la carga del listado de Historico de Partidas Casadas.",1);
	                     } else {
	                       $scope.listHistoricos = data.resultados["listHistorico"];
	
	                       // borra el contenido del body de la tabla
	                       var tbl = $("#datosHistoricos > tbody");
	                       $(tbl).html("");
	                       $scope.oTable.fnClearTable();
	
	                       for (var i = 0; i < $scope.listHistoricos.length; i++) {
	
	                         var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listHistoricos['
	                                     + i
	                                     + '].selected" ng-click="seleccionarElemento('
	                                     + i + ');"/>';
	
	                         var historicoList = [
	                                             check,
	                                             $scope.listHistoricos[i].usuario,
	                                             $scope.listHistoricos[i].origen,
	                                             $filter('date')($scope.listHistoricos[i].fechaAudioria, "dd/MM/yyyy HH:mm:ss")];
	
	                         $scope.oTable.fnAddData(historicoList, false);
	
	                       }
	                       $scope.oTable.fnDraw();
	                       $scope.safeApply();
	                       $.unblockUI();
	                     }
	                   },
	                   function (error) {
	                     $.unblockUI();
	                     fErrorTxt("Se produjo un error en la carga del listado de historico de partidas casadas.",1);
	                   }, $scope.filtro);
            	}
            };

            /** Validacion de campos del filtro de Auditoría apuntes. */
			 $scope.validacionFiltroHistorico = function () {
				 if ($scope.filtro.fechaDesde == '' && $scope.filtro.fechaHasta != '') {
					 return true;
				 } else {
					 return false;
				 }
			 };

            $scope.createOTableTLM = function(){
            	
            	if ($scope.oTableTLM !== undefined && $scope.oTableTLM !== null) {
    				$scope.oTableTLM.fnDestroy();
    			}
            	 $scope.oTableTLM = $("#datosTLM").dataTable({
                     "dom" : 'T<"clear">lfrtip',
                     "tableTools" : {
                     "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                     "aButtons" : [ "copy"]
                   },
                   "aoColumns" : [ {
                     sClass : "centrar",
                     width : "17%"
                   }, {
                     sClass : "centrar",
                     width : "17%"
                   }, {
  	               sClass : "centrar",
  	               width : "17%"
                   }, {
    	               sClass : "centrar",
  	               width : "17%"
                   }, {
                     sClass : "centrar",
                     width : "16%"
                   }, {
                     sClass : "centrar",
                     width : "16%",
					 sClass : "monedaR",
					 type : "formatted-num"
                   } ],
                   "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                     $compile(nRow)($scope);
                   },

                   "scrollY" : "480px",
                   "scrollX" : "100%",
                   "scrollCollapse" : true,
                   "language" : {
                     "url" : "i18n/Spanish.json"
                   }
               });
            }
            
            $scope.createOTableSIBBAC = function(){
            	
    			if ($scope.oTableSIBBAC !== undefined && $scope.oTableSIBBAC !== null) {
    				$scope.oTableSIBBAC.fnDestroy();
    			}
            	 $scope.oTableSIBBAC = $("#datosSIBBAC").dataTable({
    	 		  	 "dom" : 'T<"clear">lfrtip',
    	 		  	 "tableTools" : {
                     "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                     "aButtons" : [ "copy" ]
                 },
                 "aoColumns" : [ {
                   sClass : "centrar",
                   width : "20%"
                 }, {
	               sClass : "centrar",
	               width : "20%"
                 }, {
  	               sClass : "centrar",
  	               width : "20%"
                 }, {
                   sClass : "centrar",
                   width : "10%"
                 }, {
                   sClass : "centrar",
                   width : "20%"
                 }, {
                   sClass : "centrar",
                   width : "10%",
					 sClass : "monedaR",
					 type : "formatted-num"
                 } ],
                 "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                   $compile(nRow)($scope);
                 },

                 "scrollY" : "480px",
                 "scrollX" : "100%",
                 "scrollCollapse" : true,
                 "language" : {
                   "url" : "i18n/Spanish.json"
                 }
            	 });
            }
            
             
             $scope.consultarPartidasCasadas = function () {
        	 	  $scope.pestana = "TLM";
                  
                 if ($scope.listHistoricosSeleccionados.length > 1
                     || $scope.listHistoricosSeleccionados.length == 0) {
                   if ($scope.listHistoricosSeleccionados.length == 0) {
                     fErrorTxt("Debe seleccionar un elemento de la tabla de historico de partidas pendientes.", 2)
                   } else {
                     fErrorTxt("Debe seleccionar solo un elemento de la tabla de historico de partidas pendientes", 2)
                   }
                 } else {
                
           	 	  angular.element('#formularios').modal({backdrop : 'static'});
        	 	  $(".modal-backdrop").remove();
                  
        		   $scope.createOTableTLM();
        		   $scope.oTableTLMCreada = true;
                  
                	 
                   $scope.historico = angular.copy($scope.listHistoricosSeleccionados[0]);

                   HistoricoPartidasCasadasService.consultarPartidasTLM(function (data) {

                	 $scope.partidasCasadas = data.resultados["partidasCasadas"];
                	 
                  	 var tbl = $("#datosTLM > tbody");
                     $(tbl).html("");
                     $scope.oTableTLM.fnClearTable();
                     
                     for (var i = 0; i < $scope.partidasCasadas.length; i++) {

                       var partidasCasadas = [
                                           $scope.partidasCasadas[i].referencia,
                                           $scope.partidasCasadas[i].gin,
                                           $scope.partidasCasadas[i].tipoMovimiento,
                                           $scope.partidasCasadas[i].auxiliar,
                                           $scope.partidasCasadas[i].concepto,
                                           $.number($scope.partidasCasadas[i].importe, 2, ',', '.')];

                       $scope.oTableTLM.fnAddData(partidasCasadas, false);

                     }
                     $scope.oTableTLM.fnDraw();
                     $scope.safeApply();
                     $.unblockUI();

                   }, function (e) {
                     $.unblockUI();
                     fErrorTxt("Ocurrió un error durante la petición de datos al recuperar el objeto partidas casadas.", 1);
                   }, $scope.historico);
                   
                   HistoricoPartidasCasadasService.consultarDesglosesSIBBAC(function (data) {

                  	 $scope.desglosesSIBBAC = data.resultados["desglosesSIBBAC"];
                  	 
                     if ($scope.desglosesSIBBAC.length > 0) {
                    	 $scope.modalHistorico.showSIBBAC = true;
                     } else {
                    	 $scope.modalHistorico.showSIBBAC = false;
                     }
                  	 
                  	 var tbl = $("#datosSIBBAC > tbody");
                     $(tbl).html("");
                     $scope.oTableSIBBAC.fnClearTable();

                     for (var i = 0; i < $scope.desglosesSIBBAC.length; i++) {

                       var desglosesSIBBAC = [
                                           $scope.desglosesSIBBAC[i].nuorden,
                                           $scope.desglosesSIBBAC[i].nbooking,
                                           $scope.desglosesSIBBAC[i].nucnfclt,
                                           $scope.desglosesSIBBAC[i].nucnfliq,
                                           $scope.desglosesSIBBAC[i].sentido,
                                           $.number($scope.desglosesSIBBAC[i].importe, 2, ',', '.')];

                       $scope.oTableSIBBAC.fnAddData(desglosesSIBBAC, false);

                     }
                     $scope.oTableSIBBAC.fnDraw();
                     $scope.safeApply();
                     $.unblockUI();

               	   }, function (e) {
               		 $.unblockUI();
               		 fErrorTxt("Ocurrió un error durante la petición de datos al recuperar el objeto desgloses SIBBAC", 1);
               	   }, $scope.historico);
                 }
                 $scope.activarPestana('TLM');
             };
             
             
             $scope.exportarXLS = function () {
	               HistoricoPartidasCasadasService.exportarXLS(function (data) {
	            	   onSuccessExportXLS(data);
//	            	 fErrorTxt("El excel se formo bien falta la implementacion para transformacion de datos", 2);
//	            	 $.unblockUI();
	               }, function (error) {
	                 $.unblockUI();
	                 fErrorTxt("Ocurrió un error durante la petición de datos al exportar Excel Partidas Casadas", 1);
	
	               }, $scope.historico);
	           };

               function onSuccessExportXLS (json) {

                   $.unblockUI();

                   if (json.resultados.excelPartidasCasadas !== undefined) {

                     var ExcelBytes = atob([ json.resultados.excelPartidasCasadas ]);

                     var byteNumbers = new Array(ExcelBytes.length);
                     for (var i = 0; i < ExcelBytes.length; i++) {
                       byteNumbers[i] = ExcelBytes.charCodeAt(i);
                     }

                     var byteArray = new Uint8Array(byteNumbers);

                     var blob = new Blob([ byteArray ], {
                       type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                     });

                     var nav = navigator.userAgent.toLowerCase();

                     if (navigator.msSaveBlob) {
                       navigator.msSaveBlob(blob, "Partidas_Casadas_" + hoy + ".xlsx");
                     } else {
                       var blobUrl = URL.createObjectURL(blob);
                       var link = document.createElement('a');
                       link.href = blobUrl = URL.createObjectURL(blob);
                       link.download = "Partidas_Casadas_" + hoy + ".xlsx";
                       document.body.appendChild(link);
                       link.click();
                       document.body.removeChild(link);
                     }

                   } else {
                     fErrorTxt(
                               "Se produjo un error en la generacion del fichero Excel. Contacte con el Administrador.",
                               1);
                   }

                 }
               
  			 $scope.deseleccionarTodos = function () {
				 for (var i = 0; i < $scope.listHistoricos.length; i++) {
					 $scope.listHistoricos[i].selected = false;
				 }
				 $scope.listHistoricosSeleccionados = [];
			 };
			 
  			 $scope.anteriorPartidasCasadas = function () {
				//Deseleccionamos el registro marcado
  				$scope.listHistoricos[$scope.idListHistoricos].selected = false;
  				//Seleccionamos el nuevo registro (el anterior)
  				$scope.listHistoricos[$scope.idListHistoricos-1].selected = true;
  				//Quitamos de la lista de historicos seleccionados el actual
  				$scope.listHistoricosSeleccionados.splice(0, 1);
  				//Añadimos a la lista de historicos seleccionados el anterior
  				$scope.listHistoricosSeleccionados.push($scope.listHistoricos[$scope.idListHistoricos-1]);
  				//Llamamos a la consulta de histórico
  				$scope.consultarPartidasCasadas();
  				//Restamos 1 al id seleccionado para prepararlo para la siguiente iteracción
  				$scope.idListHistoricos = $scope.idListHistoricos-1;
			 };
			 
  			 $scope.posteriorPartidasCasadas = function () {
 				//Deseleccionamos el registro marcado
   				$scope.listHistoricos[$scope.idListHistoricos].selected = false;
   				//Seleccionamos el nuevo registro (el anterior)
   				$scope.listHistoricos[$scope.idListHistoricos+1].selected = true;
   				//Quitamos de la lista de historicos seleccionados el actual
   				$scope.listHistoricosSeleccionados.splice(0, 1);
   				//Añadimos a la lista de historicos seleccionados el anterior
   				$scope.listHistoricosSeleccionados.push($scope.listHistoricos[$scope.idListHistoricos+1]);
   				//Llamamos a la consulta de histórico
   				$scope.consultarPartidasCasadas();
   				//Restamos 1 al id seleccionado para prepararlo para la siguiente iteracción
   				$scope.idListHistoricos = $scope.idListHistoricos+1;
 			 };

 			 $scope.checkSiguiente = function() {
 				 if ($scope.listHistoricos.length <= $scope.idListHistoricos+1) {
 					 return true;
 				 }
 				 else {
 					 return false;
 				 }
 			 };
 			 
 			 $scope.checkAnterior = function() {
 				 if (0 > $scope.idListHistoricos-1) {
 					 return true;
 				 }
 				 else {
 					 return false;
 				 }
 			 };
} ]);