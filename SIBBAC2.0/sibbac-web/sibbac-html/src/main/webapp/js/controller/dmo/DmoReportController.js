(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	'use strict';
	sibbac20.controller('DmoReportController', ['$scope', '$http', 'growl', '$timeout', function ($scope, $http, growl, $timeout) {
		/**VAR**/
		$scope.dateInit = "";
		$scope.dateEnd = "";
		$scope.errorMsg = "Debe seleccionar una fecha, el primer día y el último del mes seleccionado se tomarán como referencia.";
		$scope.httpMsgHide = true;
		$scope.httpMsg = "Cargando...";
		var firstDay = "";
		var lastDay = "";
		var valid = true;
		var showInfoError = true;

		/**LISTENERS**/
		$scope.submitInfo = submitInfo;
		angular.element(document.getElementById("collapsableSearchReport")).click(function (event) {
			console.log('*Collapse*');
			event.preventDefault();
			$(this).parents('.title_section').next().slideToggle();
			$(this).parents('.title_section').next().next('.button_holder').slideToggle();
			$(this).toggleClass('active');
			if ($(this).text() == "Ocultar opciones de búsqueda") {
				$(this).text("Mostrar opciones de búsqueda");
			} else {
				$(this).text("Ocultar opciones de búsqueda");
			}
			return false;
		});

		/**WATCHERS**/
		$scope.$watchGroup(['dateInit'], function (newValues, oldValues, scope) {

			var pickedDate = processDate($scope.dateInit);
			var DateFirstDay = new Date(pickedDate.getFullYear(), pickedDate.getMonth(), 1);
			var DateLastDay = new Date(pickedDate.getFullYear(), pickedDate.getMonth() + 1, 0);
			firstDay = formattedDate(DateFirstDay);
			lastDay = formattedDate(DateLastDay);
			if (lastDay !== "NaN/NaN/NaN") {
				$scope.dateInit = firstDay;
				$scope.dateEnd = lastDay;
			}

		});

		/**FUNCTIONS**/
		//Check and call service
		function submitInfo() {
			var uri = '/sibbac20back/rest/dmo/dmoReport';
			var payload = { "params": { "fechaDesde": firstDay, "fechaHasta": lastDay, "importeAcumulado": "30000" } };
			showInfoError = true;
			validateInputs();
			if (valid) {
				inicializarLoading();
				$http({
					method: 'POST',
					url: uri,
					headers: { "Content-Type": "application/json" },
					data: JSON.stringify(payload),
				}).then(function successCallback(response) {
					$scope.httpMsgHide = false;
					$scope.httpMsg = response.data.message[0];
					angular.element(document.getElementById("collapsableSearchReport")).trigger('click');
					$.unblockUI();
					showInfoError = false;

				}, function errorCallback(response) {
					$scope.httpMsgHide = false;
					$scope.httpMsg = response.data.message[0];
					angular.element(document.getElementById("collapsableSearchReport")).trigger('click');
					$.unblockUI();
					showInfoError = false;

				});
			} else {
				showErrorModal();
			}
		}
		
		//Return date formatted
		function processDate(date) {
			var parts = date.split("/");
			return new Date(parts[2], parts[1] - 1, parts[0]);
		}
		//Format dd/mm/YYYY
		function formattedDate(d = new Date) {
			let month = String(d.getMonth() + 1);
			let day = String(d.getDate());
			const year = String(d.getFullYear());

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return `${day}/${month}/${year}`;
		}
		//Show error modal
		function showErrorModal() {
			
			fErrorTxt($scope.errorMsg, 2);
		}
		//Validate before sending
		function validateInputs() {
			if ($scope.dateInit === '') {
				$scope.errorMsg = "Debe seleccionar una fecha, el primer día y el último del mes seleccionado se tomarán como referencia.";
				valid = false;
			} else {
				valid = true;
			}
		}
	}]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);