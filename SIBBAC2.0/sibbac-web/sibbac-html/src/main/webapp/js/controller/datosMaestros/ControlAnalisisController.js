(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var ControlAnalisisController = function (service, httpParamSerializer, uiGridConstants, $scope, $timeout) {
		// Sub controlador DynamicFiltersController
		GENERIC_CONTROLLERS.DynamicFiltersController.call(this, service, httpParamSerializer, uiGridConstants);
		// Opciones del modal (480px ancho)
		var fatDialogOpt = { width: 480 }, self = this;
		this.scope = $scope;
		// Resgistramos los modales por la id, y le asignamos una función para el boton con el nombre asignado (Guardar, Modificar, etc)
		this.createModal = this.registryModal("modalSave", "Crear...", {
			Guardar: this.save.bind(this)
		}, fatDialogOpt);
		this.modifyModal = this.registryModal("modalEdit", "Modificar...", {
			Modificar: this.edit.bind(this)
		}, fatDialogOpt);
		this.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					self.resultDialog.dialog("close");
					if (self.result.ok) {
						self.executeQuery();
					}

				}
			}
		});
		// Ejecutamos la función selectQuery y executeQuery cuando obtengamos la query del back
		this.queries.$promise.then(data => {
			this.selectedQuery = data[0];
			this.selectQuery();
			this.executeQuery();
		});
	};


	ControlAnalisisController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

	ControlAnalisisController.prototype.constructor = ControlAnalisisController;

	// Función que se encarga de levantar los modales con una id que identifica su tipo 
	ControlAnalisisController.prototype.openModal = function (id) {
		switch (id) {
			case "save":
				this.params = {
					id: "",
					descripcion: ""
				};
				this.createModal.dialog("option", "title", "Crear");
				this.createModal.dialog("open");

				break;
			case "edit":
				if (this.selectedRows().length > 0) {
					// Igualamos los valores a los parámetros que recibiremos en el modal
					this.params = {
						id: this.selectedRows()[0].ID,
						descripcion: this.selectedRows()[0].DESCRIPCION
					};
					this.modifyModal.dialog("option", "title", "Modificar");
					this.modifyModal.dialog("open");
				}

				break;
		}
	};

	// Función encargada de ejecutar el servicio guardar
	ControlAnalisisController.prototype.save = function () {
		if(this.params.id == "" || this.params.descripcion == "")
		{
			fErrorTxt("Los campos ID y Descripción son obligatorios", 1);
		}else if(this.scope.modalForm.$valid)
		{
			var self = this;
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPOST(this.params, {}, function (result) {
				$.unblockUI();
				self.result = result;
				// Ejecuta el modal de respuesta al revicio
				self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				self.resultDialog.dialog("open");
				if(self.result.ok == true){					
					self.createModal.dialog("close");
				}
			}, this.postFail.bind(this));
		}else
		{
			fErrorTxt("El formato de los datos introducidos no son correctos", 1);
		}
	};

	// Función encargada de ejecutar el servicio guardar
	ControlAnalisisController.prototype.edit = function () {
		if(this.scope.modalForm.$valid)
		{
			var self = this;
			this.modifyModal.dialog("close");
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPUT(this.params, {}, function (result) {
				$.unblockUI();
				self.result = result;
				// Ejecuta el modal de respuesta al revicio
				self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				self.resultDialog.dialog("open");

			}, this.postFail.bind(this));
		}
		else if(this.params.id == "" || this.params.descripcion == "")
		{
			fErrorTxt("Los campos ID y Descripción son obligatorios", 1);
		}
		else
		{
			fErrorTxt("El formato de los datos introducidos no son correctos", 1);
		}
	};
	
	ControlAnalisisController.prototype.delete = function(){
		
		if (this.selectedRows().length > 0) {
			var self = this;
			var deleteObj = [];
			this.selectedRows().forEach(element =>{
				deleteObj.push(element.ID);
			});

		angular.element("#dialog-confirm").html("¿Desea eliminar el/los registro/s seleccionado/s?");

		// Define the Dialog and its properties.
			this.confirmDialog = angular.element("#dialog-confirm").dialog({
				resizable: false,
				modal: true,
				title: "Mensaje de Confirmación",
				height: 150,
				width: 360,
				buttons: {
					" Sí ": function () {
						$(this).dialog('close');
						eliminar();
					},
					" No ": function () {
						$(this).dialog('close');
					}
				}
			});
			
			function eliminar()
			{
				// Inicia el "Cargando datos..."
				inicializarLoading();
				// Ejecutamos el servicio pasandole por parámetros [deleteObj]
				self.service.executeActionDEL(deleteObj, {}, function (result) {
					$.unblockUI();
					self.result = result;
					// Ejecuta el modal de respuesta al revicio
					self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					self.resultDialog.dialog("open");
	
				}, self.postFail.bind(self));
			}
		}
		else
		{
			fErrorTxt("Debe seleccionar una o más filas para poder realizar esta opcción", 1);
		}
	};

	sibbac20.controller("ControlAnalisisController", ["ControlAnalisisService", "$httpParamSerializer", "uiGridConstants",
		"$scope", "$timeout", ControlAnalisisController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);