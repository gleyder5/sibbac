'use strict';
sibbac20.controller('GenerarInformesController',

[
 '$scope',
 '$document',
 'growl',
 'GenerarInformesService',
 '$compile',
 'SecurityService',
 '$location',
 function ($scope, $document, growl, GenerarInformesService, $compile, SecurityService, $location) {

   $scope.safeApply = function (fn) {
     var phase = this.$root.$$phase;
     if (phase === '$apply' || phase === '$digest') {
       if (fn && (typeof (fn) === 'function')) {
         fn();
       }
     } else {
       this.$apply(fn);
     }
   };

   /*******************************************************************************************************************
     * ** INICIALIZACION DE DATOS ***
     *
     ******************************************************************************************************************/

   // Variables y listas general
   $scope.plantilla = {};
   $scope.fecContratDesd = "";
   $scope.fecContratHast = "";
   $scope.fecLiquiDesd = "";
   $scope.fecLiquiHast = "";

   $scope.filtro = {};

   $scope.listPlantillas = [];
   $scope.listAlias = [];
   $scope.listIsin = [];

   $scope.mostrarBuscador = true;
   $scope.mostrarErrores = false;
   $scope.mensajeError = "";
   $scope.mensajeErrorQuery = "";
   $scope.mostrardescargarexcel = false;
   $scope.mostrardescargarexcelSimple = false;
   $scope.activeAlert = false;

   $scope.aliasFiltro = "";
   $scope.isinFiltro = "";

   $scope.file = "";
   $scope.fileSimpleXLS = "";

   // Reset del filtro
   $scope.resetFiltro = function () {
     $scope.filtro = {
       fecContratDesd : "",
       fecContratHast : "",
       fecLiquiDesd : "",
       fecLiquiHast : "",
       listaAlias : [],
       listaCdisin : [],
       alias : "",
       isin : "",
       plantilla : "0",
       especiales : "EXCLROUTING"
     };

     $scope.plantilla = {
       key : "0",
       value : ""
     };

     $scope.fecContratDesd = "";
     $scope.fecContratHast = "";
     $scope.fecLiquiDesd = "";
     $scope.fecLiquiHast = "";

     $scope.activeAlert = false;

     $scope.aliasFiltro = "";
     $scope.isinFiltro = "";
     $scope.aliasSelect = "";
     $scope.isinSelect = "";

     $scope.limiteRegistrosExcel = 0;
     $scope.superaLimite = false;

   };

   $scope.resetFiltro();

   $scope.vaciar = function (campo) {
     switch (campo) {
       case "filtro_fecContratDesd":
         $scope.fecContratDesd = "";
         break;
       case "filtro_fecContratHast":
         $scope.fecContratHast = "";
         break;
       case "filtro_fecLiquiDesd":
         $scope.fecLiquiDesd = "";
         break;
       case "filtro_fecLiquiHast":
         $scope.fecLiquiHast = "";
         break;
       default:
         break;
     }

   }

   /*******************************************************************************************************************
     * ** INICIALIZACION DE DATEPICKERS ***
     *
     ******************************************************************************************************************/
   var datepickerCheck = 0;
   $(document).ready(function () {
     renderDatePickers();
   });
   function renderDatePickers () {
     if (datepickerCheck !== undefined && datepickerCheck > 0) {
       clearTimeout(datepickerCheck);
     }
     if ($.datepicker !== undefined) {
       cargarCalendarioEsp();
       // se activan los datepickers necesarios en la página

       $("#filtro_fecContratDesd").datepicker({
         dateFormat : 'dd/mm/yy'
       });
       $("#filtro_fecLiquiDesd").datepicker({
         dateFormat : 'dd/mm/yy'
       });
       $("#filtro_fecContratHast").datepicker({
         dateFormat : 'dd/mm/yy'
       });
       $("#filtro_fecLiquiHast").datepicker({
         dateFormat : 'dd/mm/yy'
       });
     } else {
       datepickerCheck = setTimeout("renderDatePickers()", 300);
     }
   }

   /*******************************************************************************************************************
     * ** FUNCIONES GENERALES ***
     *
     ******************************************************************************************************************/
   $scope.showingFilter = true;

   // Control para ocultar o presentar el panel de busqueda
   $scope.mostrarBusqueda = function() {
     $scope.filtro.alias = "";
     i = 0;
     angular.forEach($scope.filtro.listaAlias, function (campo) {
       $scope.filtro.alias += campo.value;
       i++;
       if (i < $scope.filtro.listaAlias.length) {
         $scope.filtro.alias += ", ";
       }
     });
     $scope.filtro.isin = "";
     i = 0;
     angular.forEach($scope.filtro.listaCdisin, function (campo) {
       $scope.filtro.isin += campo.value;
       i++;
       if (i < $scope.filtro.listaCdisin.length) {
         $scope.filtro.isin += ", ";
       }
     });
     $('.mensajeBusqueda').empty();
     var cadenaFiltros = "";
     if ($scope.plantilla.value !== "") {
       cadenaFiltros += " Plantilla: " + $scope.plantilla.value;
     }
     if ($scope.fecContratDesd !== "") {
       cadenaFiltros += " Fecha Contratación Desde: " + $scope.fecContratDesd;
     }
     if ($scope.fecContratHast !== "") {
       cadenaFiltros += " Fecha Contratación Hasta: " + $scope.fecContratHast;
     }
     if ($scope.fecLiquiDesd !== "") {
       cadenaFiltros += " Fecha Liquidación Desde: " + $scope.fecLiquiDesd;
     }
     if ($scope.fecLiquiHast !== "") {
       cadenaFiltros += " Fecha Liquidación Hasta: " + $scope.fecLiquiHast;
     }
     if ($scope.filtro.alias !== "") {
       cadenaFiltros += " Alias: " + $scope.filtro.alias;
     }
     if ($scope.filtro.isin !== "") {
       cadenaFiltros += " Isin: " + $scope.filtro.isin;
     }
     $('.mensajeBusqueda').append(cadenaFiltros);
   }

   // Borrar el DataTable y los resultados de la página
   $scope.borrarResultadosAnt = function () {
     $scope.mostrarErrores = false;
     if ($scope.oTable != null) {
       $scope.oTable.fnClearTable();
       var table = $('#datosConsultaInforme').DataTable();
       table.destroy();

     }
   }

   // Convertir una cadena con el formato dd/mm/yyyy a Date
   $scope.strToDate = function (strDate) {
     var parts = strDate.split('/');
     var mydate = new Date(parts[2], parts[1] - 1, parts[0]);
     return moment(mydate).format('YYYY-MM-DD');
   }

   // Valida los campos obligatorios de la pantilla de Generacion de informes
   $scope.validacion = function () {
     var result = false;
     $scope.borrarResultadosAnt();

     $('#datosConsultaInforme').remove();

     if (($scope.plantilla.key !== "0" && $scope.fecContratDesd.length > 0)
         || ($scope.plantilla.key !== "0" && $scope.fecLiquiDesd.length > 0)) {

       $scope.filtro.plantilla = $scope.plantilla.key;

       if ($scope.fecContratDesd !== "" && $scope.fecContratDesd != null) {
         $scope.filtro.fecContratDesd = $scope.strToDate($scope.fecContratDesd);
       } else {
         $scope.filtro.fecContratDesd = "";
       }

       if ($scope.fecLiquiDesd !== "" && $scope.fecLiquiDesd != null) {
         $scope.filtro.fecLiquiDesd = $scope.strToDate($scope.fecLiquiDesd);
       } else {
         $scope.filtro.fecLiquiDesd = "";
       }

       if ($scope.fecContratHast !== "" && $scope.fecContratHast != null) {
         $scope.filtro.fecContratHast = $scope.strToDate($scope.fecContratHast);
       } else {
         $scope.filtro.fecContratHast = "";
       }
       if ($scope.fecLiquiHast !== "" && $scope.fecLiquiHast != null) {
         $scope.filtro.fecLiquiHast = $scope.strToDate($scope.fecLiquiHast);
       } else {
         $scope.filtro.fecLiquiHast = "";
       }
       // Se oculta el panel de búsqueda y se muestra el detalle
       $scope.showingFilter = false;
       // Se muestra la capa cargando
       inicializarLoading();
       $scope.activeAlert = false;
       result = true;
     } else {
       $scope.activeAlert = true;
     }
     return result;
   }

   $scope.controlError = function (txterror) {
     if (txterror.length == 0) {
       txterror = "La generación del informe ha devuelto un error, por favor contacte con Soporte IT.";
     }
     fErrorTxt(txterror, 1);
     $scope.mensajeError = txterror;
     $scope.mostrarErrores = true;
   }

   var pupulateAutocomplete = function (input, availableTags) {
     $(input).autocomplete({
       minLength : 0,
       source : availableTags,
       focus : function (event, ui) {
         return false;
       },
       select : function (event, ui) {

         var option = {
           key : ui.item.key,
           value : ui.item.value
         };

         switch (input) {
           case "#filtro_alias":
             var existe = false;
             for (var i = 0; i < $scope.filtro.listaAlias.length; i++) {
               if (option.value === $scope.filtro.listaAlias[i].value) {
                 existe = true;
               }
             }
             $scope.aliasFiltro = "";
             if (!existe) {
               $scope.filtro.listaAlias.push(option);
             }
             break;
           case "#filtro_isin":
             var existe = false;
             for (var i = 0; i < $scope.filtro.listaCdisin.length; i++) {
               if (option.value === $scope.filtro.listaCdisin[i].value) {
                 existe = true;
               }
             }
             $scope.isinFiltro = "";
             $scope.filtro.listaCdisin.push(option);
             break;
         }

         $scope.safeApply();

         return false;
       }
     });
   };

   $scope.subirElementoTabla = function (tabla) {
     var elemento = {};
     switch (tabla) {
       case "alias":
         for (var i = 0; i < $scope.filtro.listaAlias.length; i++) {
           if ($scope.filtro.listaAlias[i].value === $scope.aliasSelect.value && i > 0) {
             elemento = $scope.filtro.listaAlias[i];
             $scope.filtro.listaAlias[i] = $scope.filtro.listaAlias[i - 1];
             $scope.filtro.listaAlias[i - 1] = elemento;
           }
         }
         break;
       case "isin":
         for (var i = 0; i < $scope.filtro.listaCdisin.length; i++) {
           if ($scope.filtro.listaCdisin[i].value === $scope.isinSelect.value && i > 0) {
             elemento = $scope.filtro.listaCdisin[i];
             $scope.filtro.listaCdisin[i] = $scope.filtro.listaCdisin[i - 1];
             $scope.filtro.listaCdisin[i - 1] = elemento;
           }
         }
         break;
       default:
         break;
     }
   };

   $scope.bajarElementoTabla = function (tabla) {
     var elemento = {};
     switch (tabla) {
       case "alias":
         var listLength = $scope.filtro.listaAlias.length;
         for (var i = $scope.filtro.listaAlias.length - 1; i >= 0; i--) {
           if ($scope.filtro.listaAlias[i].value === $scope.aliasSelect.value && i < (listLength - 1)) {
             elemento = $scope.filtro.listaAlias[i];
             $scope.filtro.listaAlias[i] = $scope.filtro.listaAlias[i + 1];
             $scope.filtro.listaAlias[i + 1] = elemento;
           }
         }
         break;
       case "isin":
         var listLength = $scope.filtro.listaCdisin.length;
         for (var i = $scope.filtro.listaCdisin.length - 1; i >= 0; i--) {
           if ($scope.plantilla.listaCdisin[i].value === $scope.isinSelect.value && i < (listLength - 1)) {
             elemento = $scope.filtro.listaCdisin[i];
             $scope.filtro.listaCdisin[i] = $scope.filtro.listaCdisin[i + 1];
             $scope.filtro.listaCdisin[i + 1] = elemento;
           }
         }
         break;
       default:
         break;
     }
   };

   $scope.eliminarElementoTabla = function (tabla) {
     switch (tabla) {
       case "alias":
         for (var i = 0; i < $scope.filtro.listaAlias.length; i++) {
           if ($scope.filtro.listaAlias[i].value === $scope.aliasSelect.value) {
             $scope.filtro.listaAlias.splice(i, 1);
           }
         }
         break;
       case "isin":
         for (var i = 0; i < $scope.filtro.listaCdisin.length; i++) {
           if ($scope.filtro.listaCdisin[i].value === $scope.isinSelect.value) {
             $scope.filtro.listaCdisin.splice(i, 1);
           }
         }
         break;
       default:
         break;
     }
   };

   $scope.vaciarTabla = function (tabla) {
     switch (tabla) {
       case "alias":
         $scope.filtro.listaAlias = [];
         break;
       case "isin":
         $scope.filtro.listaCdisin = [];
         break;
       default:
         break;
     }
   };

   /*******************************************************************************************************************
     * ** CARGA DE DATOS INICIAL ***
     *
     *
     ******************************************************************************************************************/
   // Carga de los combos Isin
   $scope.cargarIsin = function () {
     GenerarInformesService.cargaIsin(function (data) {

       $scope.listIsin = data.resultados["listaIsin"];
       pupulateAutocomplete("#filtro_isin", $scope.listIsin);
     }, function (error) {

       fErrorTxt("Se produjo un error en la carga de los combos ISIN.", 1);
     });
   }

   // Carga de los combos Alias
   $scope.cargarAlias = function () {
     GenerarInformesService.cargaAlias(function (data) {
       $scope.listAlias = data.resultados["listaAlias"];
       pupulateAutocomplete("#filtro_alias", $scope.listAlias);
     }, function (error) {
       fErrorTxt("Se produjo un error en la carga de los combos Alias.", 1);
     });
   }
   // Carga del combo Plantillas
   GenerarInformesService.cargaPlantillas(function (data) {
     $scope.listPlantillas = data.resultados.listaInformes;
     // Se inicializa tambien el limite total de registros
     $scope.limiteRegistrosExcel = data.resultados.limite_total
   }, function (error) {
     console.log("Error en la carga del combo listPlantillas: " + error);
   });

   $scope.cargarAlias();
   $scope.cargarIsin();

   /*******************************************************************************************************************
     * ** ACCIONES ***
     *
     ******************************************************************************************************************/
   // Descarga un archivo excel con el contenido pasado como parametro
   $scope.descargarExcel = function (ExcelBytes) {
     var byteNumbers = new Array(ExcelBytes.length);
     for (var i = 0; i < ExcelBytes.length; i++) {
       byteNumbers[i] = ExcelBytes.charCodeAt(i);
     }

     var byteArray = new Uint8Array(byteNumbers);

     var blob = new Blob([ byteArray ], {
       type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
     });

     var nav = navigator.userAgent.toLowerCase();

     if (navigator.msSaveBlob) {
       navigator.msSaveBlob(blob, "reporte.xlsx");
     } else {
       var blobUrl = URL.createObjectURL(blob);
       var link = document.createElement('a');
       link.href = blobUrl = URL.createObjectURL(blob);
       link.download = "reporte.xlsx";
       document.body.appendChild(link);
       link.click();
       document.body.removeChild(link);
     }

   }

   // Funcion general para pintar datatable y tratar errores
   $scope.tratarDatos = function (data, descargarExcelTerceros, descargarExcelSimple) {
     $scope.mensajeErrorQuery = "";
     if (data.hasOwnProperty("resultados")) {
       if (data.resultados.status === 'KO') {
         $scope.controlError("");
       } else {

         // se obtienen los registros del informe. Se espera una lista de listas de ColumnaInformeDTO
         // (columna-valor)
         var list = data.resultados.informe.resultados;
         if (list.length > 0) {
           $scope.listRegistros = list;

           var contenidoColumnas = [];
           var contenidoFilas = [];

           // se recorren las filas devueltas por la consulta
           $.each($scope.listRegistros, function (index_i, registro) {
             var contenidoFila = [];
             // se recorren las columnas de cada fila
             $.each(registro, function (index_j, contenido) {
               // para la primera fila nos quedamos con las keys como cabeceras
               if (index_i == 0) {
                 var ao = {
                   sTitle : contenido.columna
                 };
                 contenidoColumnas.push(ao);
               }
               contenidoFila.push(contenido.valor);
             });
             contenidoFilas.push(contenidoFila);
           });

           $('#contenedordatos')
               .append('<table id="datosConsultaInforme" class="fullTable ancha" cellspacing="0"></table>');
           // Se pintan las columnas en la tabla
           $scope.oTable = $("#datosConsultaInforme").dataTable({
             "dom" : 'T<"clear">lfrtip',
             "tableTools" : {
               "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
               "aButtons" : []
             },
             "aoColumns" : contenidoColumnas,
             "order" : [],
             "scrollY" : "480px",
             "scrollX" : "100%",
             "scrollCollapse" : true,
             "language" : {
               "url" : "i18n/Spanish.json"
             }
           });

           // Se pinta el contenido en la tabla
           $scope.oTable.fnAddData(contenidoFilas, false);

           // Se controla que haya resultados pero no se haya generado la excel
           if (data.resultados.informe.errores.length > 0) {
             $scope.controlError(data.resultados.informe.errores[0]);
           } else {
             if (descargarExcelTerceros) {
               // se obtiene el contenido en excel de terceros y se descarga
               var decodedString = atob([ data.resultados.file ]);
               $scope.descargarExcel(decodedString);
             } else {
               $scope.file = data.resultados.file;
             }

             if (descargarExcelSimple) {
               // se obtiene el contenido en excel de terceros y se descarga
               var decodedString = atob([ data.resultados.fileSimpleXLS ]);
               $scope.descargarExcel(decodedString);
             } else {
               $scope.fileSimpleXLS = data.resultados.fileSimpleXLS;
             }
           }
         } else {
           if (data.resultados.informe.errores.length > 0) {
             // Error controlado
             $scope.controlError(data.resultados.informe.errores[0]);
           } else {
             $scope.controlError("Los filtros seleccionados no han generado resultados.");
           }
         }
       }
       // Se desbloquea la página
       $.unblockUI();
     } else {
       // Se hace una llamada para capturar la query que ha dado el problema
       GenerarInformesService.obtenerQueryInforme(function (data) {
         // Se obtienen los errores controlados
         $scope.mensajeErrorQuery = data.resultados.informe.query;
         $scope.controlError("");

         // Se desbloquea la página
         $.unblockUI();
       }, function (error) {
         $.unblockUI();
         $scope.controlError("");
       }, $scope.filtro);
     }
   }

   // Hace la llamada a generar informe
   $scope.generarInforme = function () {
     if ($scope.validacion()) {
       GenerarInformesService.generarInforme(function (data) {
         $scope.tratarDatos(data, false, false);

         // Comprobacion si supera el limite maximo de registros y no se le va permitir generar la excel
         if ($scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
           $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length
                               + " recuperados no permite la generacion de la excel. Limite maximo establecido "
                               + $scope.limiteRegistrosExcel);
         } else {
           $scope.mostrardescargarexcel = true;
           $scope.mostrardescargarexcelSimple = true;
         }
       }, function (error) {
         $.unblockUI();
         $scope.controlError("");

       }, $scope.filtro);
     }
   };

   // Hace la llamada a la generación de informe en excel
   $scope.generarInformeExcel = function () {
     if ($scope.validacion()) {
       GenerarInformesService.generarInformeExcel(function (data) {

         // Comprobacion si supera el limite maximo de registros y no se le va permitir generar la excel
         if ($scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
           $scope.tratarDatos(data, false, false); // Muestro los resultados pero no permito la descarga
           $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length
                               + " recuperados no permite la generacion de la excel. Limite maximo establecido "
                               + $scope.limiteRegistrosExcel);
         } else {
           $scope.tratarDatos(data, false, true); // Muestro los resultados y permito la descarga
           $scope.mostrardescargarexcel = true;
         }

       }, function (error) {
         $.unblockUI();
         $scope.controlError("");

       }, $scope.filtro);
     }
   };

   // Hace la llamada a la generación de informe en excel para terceros
   $scope.generarInformeExcelTerceros = function () {
     if ($scope.validacion()) {
       GenerarInformesService.generarInformeExcelTerceros(function (data) {

         // Comprobacion si supera el limite maximo de registros y no se le va permitir generar la excel
         if ($scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
           $scope.tratarDatos(data, false, false); // Muestro los resultados pero no permito la descarga
           $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length
                               + " recuperados no permite la generacion de la excel. Limite maximo establecido "
                               + $scope.limiteRegistrosExcel);
         } else {
           $scope.tratarDatos(data, true, false); // Muestro los resultados y permito la descarga
           $scope.mostrardescargarexcelSimple = true;
         }
       }, function (error) {
         $.unblockUI();
         $scope.controlError("");

       }, $scope.filtro);
     }
   };

   // Hace la llamada a la generación de informe en excel para terceros pasansole los datos del datatb
   $scope.generarInformeExcelTercerosDatatable = function () {

     // Si el fichero tiene un valor, se descarga.
     if ($scope.file != "" && $scope.file !== undefined) {
       // Se muestra la capa cargando
       inicializarLoading();

       // se obtiene el contenido en excel y se descarga
       var decodedString = atob([ $scope.file ]);
       $scope.descargarExcel(decodedString);

       // Se desbloquea la página
       $.unblockUI();

     } else {
       $scope.generarInformeExcelTerceros();

       $scope.showingFilter = true;
       // Si es menor y el fichero esta vacio, no se ha generado en la consulta hay que generarlo.
       $scope.mostrardescargarexcel = true;
       $scope.mostrardescargarexcelSimple = true;

     }

   }

   // Hace la llamada a la generación de informe en excel para terceros pasansole los datos del datatb
   $scope.generarInformeExcelSimpleDatatable = function () {

     // Si el fichero tiene un valor, se descarga.
     if ($scope.fileSimpleXLS != "" && $scope.fileSimpleXLS !== undefined) {
       // Se muestra la capa cargando
       inicializarLoading();
       // se obtiene el contenido en excel y se descarga
       var decodedString = atob([ $scope.fileSimpleXLS ]);
       $scope.descargarExcel(decodedString);

       // Se desbloquea la página
       $.unblockUI();

     } else {
       $scope.generarInformeExcel();
       // Si es menor y el fichero esta vacio, no se ha generado en la consulta hay que generarlo.
       $scope.mostrardescargarexcel = true;
       $scope.mostrardescargarexcelSimple = true;
       // Oculto la busqueda
       $scope.showingFilter = true;

     }

   }

 } ]);
