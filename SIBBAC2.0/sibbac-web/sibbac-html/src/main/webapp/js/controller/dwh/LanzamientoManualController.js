(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var LanzamientoManualController = function (service, httpParamSerializer, uiGridConstants, $scope) {

		var ctrl = this;
		ctrl.scope = $scope;
		ctrl.service = service;
		ctrl.params = {};
		ctrl.task = {};
		ctrl.dialogs = ["#resultDialog"];


		ctrl.dialogs.forEach(dialog => {
			var aux = $('[role=dialog]').find(dialog);
			(aux.length > 0) ? aux.dialog('destroy').remove() : undefined;
		});

		ctrl.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					ctrl.resultDialog.dialog("close");
				}
			}
		});

		ctrl.resultDialogTask = angular.element("#resultDialogTask").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Confirmar": function () {

					inicializarLoading();
					ctrl.service.generate(ctrl.params, function (result) {
						ctrl.resultDialogTask.dialog("close");

						$.unblockUI();
						ctrl.result = result;

						// Ejecuta el modal de respuesta al revicio
						ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
						ctrl.isNullReturned();
						ctrl.resultDialog.dialog("open");
					}, ctrl.postFail.bind(ctrl));
				},
				"Cancelar": function () {
					ctrl.resultDialogTask.dialog("close");
				}
			}
		});

		ctrl.postFail = function (error) {
			var message;
			if (typeof error === "string") {
				message = "Se produjo un error al ejecutar la acción: " + error;
			}
			else if (error.length == 1) {
				message = error;
			}
			else {
				message = "Se produjeron los siguientes errores: \n";
				angular.forEach(error, function (value) {
					message += ("- " + value + "\n");
				});
			}
			fErrorTxt(message, 1);
			$.unblockUI();
		};


		ctrl.ejecuteFicherosNacionalTask = function () {
			inicializarLoading();
			var ctrl = this;
			console.log(ctrl.params.fecha);
			ctrl.service.generate(ctrl.params, function (result) {
				$.unblockUI();
				ctrl.result = result;

				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				ctrl.isNullReturned();
				ctrl.resultDialog.dialog("open");
			}, ctrl.postFail.bind(this));
		};


		ctrl.isNullReturned = function (){
		if(ctrl.result.ok == undefined) { 
			ctrl.result = {};
			ctrl.result.ok = false;
			ctrl.result.errorMessage = [];
			ctrl.result.errorMessage.push("Error en la ejecución del proceso. Consulte con un administrador.")
		}
	}

		ctrl.formatDates = function (date) {
		date = date + ' 00:00:00';
		return new Date(date);
	};
};

LanzamientoManualController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

LanzamientoManualController.prototype.constructor = LanzamientoManualController;

	sibbac20.controller("LanzamientoManualController", ["LanzamientoManualService", "$httpParamSerializer", "uiGridConstants",
		"$scope", LanzamientoManualController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);