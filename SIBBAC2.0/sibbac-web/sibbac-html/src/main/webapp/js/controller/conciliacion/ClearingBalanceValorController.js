'use strict';
sibbac20.controller('ClearingBalanceValorController', ['$scope', 'CuentaLiquidacionService', 'IsinService', 'AliasService', '$document', 'ConciliacionClearingService', 'growl',
    function ($scope, CuentaLiquidacionService, IsinService, AliasService, $document, ConciliacionClearingService, growl) {

        var oTable = undefined;
        $scope.cuentas = [];
        $scope.rastroBusqueda = "";

        function initFilter(){
            $scope.filter = {"isin": "",
            "alias": "",
            "fecha": "",
            "cdcodigocuentaliq": "",
            "chisin" : true,
            "chalias" : true,
            "chcdcodigocuentaliq" : true
            };
        }

        $scope.fechaHoy = "";
        /** Alias **/
        function loadAlias() {
            inicializarLoading();
            AliasService.getAlias().success(cargarAlias).error(showError);
        }
        /** ISINES **/
        function loadIsines() {
            IsinService.getIsines().success(cargarIsines).error(showError);
        }
        /** CUENTAS **/
        function loadCuentas() {
            CuentaLiquidacionService.getCuentasLiquidacionClearing().success(cargarCuentas).error(showError);
        }
        function showError(data, status, headers, config) {
            $.unblockUI();
            console.log("Error al inicializar datos: " + status);
        }
        function onSuccessConsultarRequestHandler(json, status, headers, config) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
                    json.resultados === null ||
                    json.resultados.result_balance_clearing === undefined) {
                datos = {};
            } else {
                datos = json.resultados.result_balance_clearing;
                if (json.error !== null) {
                    $.unblockUI();
                    growl.addErrorMessage(json.error);
                } else {
                    var errors = cargarDatos(datos);
                    $.unblockUI();
                }

            }
        }
        function consultar(params) {
            inicializarLoading();
            ConciliacionClearingService.getBalanceMovimientosCuentaVirtualPorIsin(onSuccessConsultarRequestHandler, showError, params);
        }
        function cargarDatos(datos) {

            var srcValid = "img/activo.png";
            var srcWarning = "img/desactivo.png";
            oTable.fnClearTable();
            var lineaMostrada = false;
            var cabeceraMostrada = false;
            var totalEfectivoTag = "";
            var totalTitulosTag = "";
            var totalTitulos = 0;
            //
            var isin = "";
            var descisin = "";
            var alias = "";
            var descAlias = "";
            var titulos = 0;
            var efectivo = 0;
            var src = "";
            //
            angular.forEach(datos, function (val, key) {
                lineaMostrada = false;
                cabeceraMostrada = false;
                totalEfectivoTag = "";
                totalTitulosTag = "";
                totalTitulos = 0;
                //
                var grupo = val.grupoBalanceISINList;
                if (grupo !== null && grupo !== undefined) {
                    isin = (val.isin !== null && val.isin !== undefined) ? val.isin : '';
                    descisin = (val.descrisin !== null && val.descrisin !== undefined) ? val.descrisin : '';
                    alias = (grupo[0].alias !== "" && grupo[0].alias !== null) ? grupo[0].alias : grupo[0].descAli;
                    descAlias = (grupo[0].descAli !== "" && grupo[0].descAli !== null) ? grupo[0].descAli : grupo[0].alias;
                    titulos = (grupo[0].titulos != null && grupo[0].titulos != undefined) ? grupo[0].titulos : '';
                    totalTitulos += titulos;
                    efectivo = (grupo[0].efectivo !== null && grupo[0].efectivo !== undefined) ? grupo[0].efectivo : "";
                    src = (grupo[0].valido === false) ? srcWarning : srcValid;
                    if (titulos !== 0 || efectivo !== 0) {
                        lineaMostrada = true;
                        cabeceraMostrada = true;
                        oTable.fnAddData([
                            isin,
                            descisin,
                            alias,
                            descAlias,
                            $.number(titulos, 0, ',', '.'),
                            $.number(efectivo, 2, ',', '.')
                        ], false);
                    }
                    var i = 1;
                    angular.forEach(grupo, function (valor, indice) {

                        if (i < grupo.length) {
                            alias = (grupo[i].alias !== "" && grupo[i].alias !== null) ? grupo[i].alias : "";
                            descAlias = (grupo[i].descAli !== "" && grupo[i].descAli !== null) ? grupo[i].descAli : grupo[i].alias;
                            titulos = (grupo[i].titulos != null && grupo[i].titulos != undefined) ? grupo[i].titulos : '';
                            totalTitulos += titulos;
                            efectivo = (grupo[i].efectivo !== null && grupo[i].efectivo !== undefined) ? grupo[i].efectivo : "";
                            src = (grupo[i].valido === false) ? srcWarning : srcValid;
                            if (titulos !== 0 || efectivo !== 0) {
                                lineaMostrada = true;

                                oTable.fnAddData([
                                    ((!cabeceraMostrada) ? isin : ''),
                                    ((!cabeceraMostrada) ? descisin : ''),
                                    alias,
                                    descAlias,
                                    $.number(titulos, 0, ',', '.'),
                                    $.number(efectivo, 2, ',', '.')
                                ], false);
                            }
                            i++;
                        }
                    });
                    if (lineaMostrada && (totalTitulos !== 0 || val.totalEfectivos !== 0)) {
                        totalTitulosTag = "<div class=\"negrita celda-tabla-245\"><span class=\"total-efectivo-label\">" +
                                "Total T&iacute;tulos: </span><span style=\"float:right;\">" + $.number(totalTitulos, 0, ',', '.') + "</span></div>"
                        totalEfectivoTag = "<div class=\"negrita celda-tabla-245\"><span class=\"total-efectivo-label\">" +
                                "Total Efectivo: </span><span style=\"float:right;\">" + $.number(val.totalEfectivos, 2, ',', '.') + "</span></div>";
                        oTable.fnAddData([
                            '',
                            '',
                            '',
                            '',
                            (totalTitulos !== 0) ? totalTitulosTag : '',
                            (val.totalEfectivos !== 0) ? totalEfectivoTag : ''
                        ], false);
                    }
                }
            });
            oTable.fnDraw();
            return false;
        }
        /////////////////////////////////////////////////
        ////// SE EJECUTA NADA MÁS CARGAR LA PÁGINA ////
        function initialize() {
            initFilter();
            $scope.filter.fecha = getfechaHoy();
            cargarFechaActualizacionMovimientoAlias();
            loadIsines();
            loadCuentas();
            loadAlias();
            $scope.fechaHoy = getfechaHoy();
            //obtenerDatos();
        }

        $document.ready(function () {
            loadDataTable();
            prepareCollapsion();
            initialize();
        });
//        $scope.submitForm = function(event){
        $scope.Consultar = function(){
        	if (!validarFecha("fecha")) {
                alert("La Fecha no tiene el formato correcto");
            } else {

	        	if (comprobarfecha($scope.filter.fecha,$scope.fechaHoy) == 1)
	        	{
	        		alert("No se permiten fechas mayores a hoy");
	        	}
	        	else
	        	{
	        		obtenerDatos();
	        		collapseSearchForm();
	        		seguimientoBusqueda();
	        	}
            }
        }
        $scope.cleanFilter = function(event){
            initFilter();
            $scope.filter.chisin = false;
            $scope.filter.chalias = false;
            $scope.filter.chcdcodigocuentaliq = false;

            $scope.btnInvGeneric('Alias');
    		$scope.btnInvGenerico('Isin');
    		$scope.btnInvGenerico('Cdcodigocuentaliq');
        }
        function loadDataTable() {
            oTable = $("#tblClearingTitulosValor").dataTable({
                "dom": 'T<"clear">lfrtip',
                "bSort": false,
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "deferRender": true, /*solo renderiza los registros que se muestran*/
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "aoColumns": [
                    {"sClass": "align-left"},
                    {"sClass": "align-left"},
                    {"sClass": "align-left"},
                    {"sClass": "align-left"},
                    {"sClass": "align-right", "type": 'formatted-num'},
                    {"sClass": "align-right", "type": 'formatted-num'}
                ]
            });
        }
        function obtenerDatos() {

        	var isin = (!$scope.filter.chisin && getIsin() !== "" ? "#" : "") + getIsin();
        	var alias = (!$scope.filter.chalias && getAliasId() !== "" ? "#" : "") + getAliasId();
        	var cuenta = (!$scope.filter.chcdcodigocuentaliq && $scope.filter.cdcodigocuentaliq.trim() !== "" ? "#" : "")  + $scope.filter.cdcodigocuentaliq.trim();

            var params = {"isin": getIsin(),
                "alias": getAliasId(),
                "fecha": transformaFechaInv($scope.filter.fecha),
                "cdcodigocuentaliq": $scope.filter.cdcodigocuentaliq.trim()
            };
            consultar(params);
        }
        //para ver el filtro al minimizar la búsqueda
        function seguimientoBusqueda() {

        	$scope.rastroBusqueda = "";

            if ($scope.filter.isin !== "") {
            	if ($scope.filter.chisin)
            		$scope.rastroBusqueda += " isin: " + document.getElementById("isin").value;
            	else
            		$scope.rastroBusqueda += " isin distinto: " + document.getElementById("isin").value;
            }
            if ($scope.filter.alias !== "") {
            	if ($scope.filter.chalias)
            		$scope.rastroBusqueda += " alias: " + document.getElementById("alias").value;
            	else
            		$scope.rastroBusqueda += " alias distinto: " + document.getElementById("alias").value;
            }

            if ($scope.filter.fecha !== "") {
                $scope.rastroBusqueda += " fecha: " + $scope.filter.fecha;
            }
            if ($scope.filter.cdcodigocuentaliq !== "") {
            	if ($scope.filter.chcdcodigocuentaliq)
            		$scope.rastroBusqueda += " cuenta: " + $scope.filter.cdcodigocuentaliq;
            	else
            		$scope.rastroBusqueda += " cuenta distinto: " + $scope.filter.cdcodigocuentaliq;
            }
        }

        function getIsin() {
            //var isinCompleto = $scope.filter.isin.trim();
        	var isinCompleto = document.getElementById("isin").value; //modificado por alex 19Jan para realizar la busqueda correcta en Chrome
            var guion = isinCompleto.indexOf("-");
            if (guion > 0) {
                return isinCompleto.substring(0, guion).trim();
            } else {
                return isinCompleto;
            }
        }
        function cargarCuentas(data, status, header, config) {
            if (data !== null && data.resultados !== null && data.resultados.result_cuentas_liquidacion_clearing !== null) {
                $scope.cuentas = data.resultados.result_cuentas_liquidacion_clearing;
            }
        }
        function cargarIsines(data, status, headers, config) {
            console.log("isines cargados: " + data);
            if (data != null && data.resultados != null &&
                    data.resultados.result_isin != null) {
                var datosIsin = data.resultados.result_isin;
                var availableTags = [];
                var ponerTag = "";
                angular.forEach(datosIsin, function (val, key) {
                    ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                    availableTags.push(ponerTag);
                });
                angular.element("#isin").autocomplete({
                    source: availableTags
                });

            }
        }
        function cargarAlias(data, status, headers, config) {
            var availableTagsAlias = [];
            var ponerTag = "";
            angular.forEach(data.resultados.result_alias, function (val, key) {
                ponerTag = val.alias + " - " + val.descripcion;
                availableTagsAlias.push(ponerTag);
            });
            // código de autocompletar
            angular.element("#alias").autocomplete({
                source: availableTagsAlias
            });
            $.unblockUI();
        }


        function getAliasId() {
        	var alias = $("#alias").val();  //modificado por alex el 19Jan para que pueda realizarse correctamente la busqueda en chrome
            //var alias = $scope.filter.alias.trim();
            var guion = alias.indexOf("-");
            return (guion > 0) ? alias.substring(0, guion).trim() : alias;
        }

        $scope.btnInvAlias = function(){
        	$scope.btnInvGeneric('Alias');
        }

    	$scope.btnInvIsin = function() {
    		$scope.btnInvGenerico('Isin');
    	}

    	$scope.btnInvCdcodigocuentaliq = function() {
    		$scope.btnInvGenerico('Cdcodigocuentaliq');
    	}

    	$scope.btnInvGenerico = function( nombre ) {

    	  var check = '$scope.filter.ch' + nombre.toLowerCase();

      	  if(eval(check)){
      		  	  var valor2 = check + "= false";
      		  	  eval(valor2 ) ;

        		  document.getElementById('textBtnInv'+nombre).innerHTML = "<>";
        		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "red"});
        		  document.getElementById('textInv'+nombre).innerHTML = "DISTINTO";
        	  }else{
               	  var valor2 = check + "= true";
            	  eval(valor2 ) ;

        		  document.getElementById('textBtnInv'+nombre).innerHTML = "=";
        		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "green"});
        		  document.getElementById('textInv'+nombre).innerHTML = "IGUAL";
        	  }
        }
    }]);


