sibbac20.controller('ClearingBalanceClienteController', ['$scope', 'CuentaLiquidacionService', 'IsinService', 'AliasService', 'ConciliacionClearingService', 'growl', '$document', function ($scope, CuentaLiquidacionService, IsinService, AliasService, ConciliacionClearingService, growl, $document) {
        $scope.isines = [];
        $scope.cuentas = [];
        $scope.filter = {
        	"isin": "",
            "alias": "",
            "fecha": "",
            "cdcodigocuentaliq": "",
            "chisin" : true,
            "chalias" : true,
            "chcdcodigocuentaliq" : true
        };
        $scope.rastroBusqueda = "";

        $scope.fechaHoy = "";
        /** ISINES **/
        function loadIsines() {
            IsinService.getIsines(cargarIsines, showError);
        }
        /** CUENTAS **/
        function loadCuentas() {
            CuentaLiquidacionService.getCuentasLiquidacionClearing().success(cargarCuentas).error(showError);
        }
        /** Alias **/
        function loadAlias() {
            inicializarLoading();
            AliasService.getAlias().success(cargaAlias).error(showError);
        }
        /** END **/
        function showError(data, status, headers, config) {
            $.unblockUI();
            console.log("Error al inicializar datos: " + status);
        }
        var oTable = undefined;
        function consultar(json) {

            inicializarLoading();

            collapseSearchForm();
            var datos = undefined;
            if (json === undefined ||
                    json.resultados === undefined ||
                    json.resultados === null ||
                    json.resultados.result_balance_clearing === undefined) {
                datos = {};
            } else {
                datos = json.resultados.result_balance_clearing;
                var errors = cargarDatos(datos);
            }
            $.unblockUI();
        }
        function cargarDatos(datos) {
            var tbl = $("#tblClearingTitulos > tbody");
            $(tbl).html("");
            var srcValid = "img/activo.png";
            var srcWarning = "img/desactivo.png";
            oTable.fnClearTable();
            var alias = "";
            var descAli = "";
            var src = "";
            var isin = "";
            var descIsin = "";
            var titulos = 0;
            var efectivo = 0;
            var lineaMostrada = false;
            var cabeceraMostrada = false;
            var totalTitulosTag = "";
            var totalEfectivoTag = "";
            var totalTitulos = 0;
            $.each(datos, function (key, val) {
                var grupo = datos[key].grupoBalanceClienteList;
                lineaMostrada = false;
                totalTitulos = 0;
                totalEfectivoTag = "";
                totalTitulosTag = "";
                cabeceraMostrada = false;
                if (grupo !== null) {
                    alias = (datos[key].alias !== "" && datos[key].alias !== null) ? datos[key].alias : datos[key].descralias;
                    descAli = (datos[key].descralias !== "" && datos[key].descralias !== null) ? datos[key].descralias : datos[key].alias;
                    src = (grupo[0].valido === false) ? srcWarning : srcValid;
                    isin = (grupo[0].isin !== null && grupo[0].isin !== undefined) ? grupo[0].isin : "";
                    descIsin = (grupo[0].descrisin !== null && grupo[0].descrisin !== undefined) ? grupo[0].descrisin : "";
                    titulos = (grupo[0].titulos !== null && grupo[0].titulos !== undefined) ? grupo[0].titulos : 0;
                    totalTitulos += titulos;
                    efectivo = (grupo[0].efectivo !== null && grupo[0].efectivo !== undefined) ? grupo[0].efectivo : 0;
                    if (titulos !== 0 || efectivo !== 0) {
                        lineaMostrada = true;
                        cabeceraMostrada = true;
                        oTable.fnAddData([
                            (alias != null && alias != undefined) ? alias : '',
                            descAli,
                            isin,
                            descIsin,
                            $.number(titulos, 0, ',', '.'),
                            $.number(efectivo, 2, ',', '.')], false);
                    }
                    var i = 1;
                    $.each(grupo, function (indice, valor) {

                        if (grupo !== null && i < grupo.length) {
                            lineaMostrada = true;
                            isin = (grupo[i].isin !== null && grupo[i].isin !== undefined) ? grupo[i].isin : "";
                            descIsin = (grupo[i].descrisin !== null && grupo[i].descrisin !== undefined) ? grupo[i].descrisin : "";
                            titulos = (grupo[i].titulos !== null && grupo[i].titulos !== undefined) ? grupo[i].titulos : 0;
                            totalTitulos += titulos;
                            efectivo = (grupo[i].efectivo !== null && grupo[i].efectivo !== undefined) ? grupo[i].efectivo : 0;

                            var src = (grupo[i].valido === false) ? srcWarning : srcValid;
                            if (titulos !== 0 || efectivo !== 0) {
                                oTable.fnAddData([
                                    ((!cabeceraMostrada) ? alias : ''),
                                    ((!cabeceraMostrada) ? descAli : ''),
                                    isin,
                                    descIsin,
                                    $.number(titulos, 0, ',', '.'),
                                    $.number(efectivo, 2, ',', '.')
                                ], false);
                            }
                            i++;
                        }
                    });
                    //si hay algun registro se muestra el total
                    if (lineaMostrada) {
                        totalTitulosTag = (totalTitulos === 0) ? "" : "<div class=\"negrita celda-tabla-245\"><span class=\"total-efectivo-label\">" + "Total T&iacute;tulos: </span><span style=\"float:right;\">" + $.number(totalTitulos, 0, ',', '.') + "</span></div>";
                        totalEfectivoTag = (datos[key].totalEfectivos === 0) ? "" : "<div class=\"negrita celda-tabla-245\"><span class=\"total-efectivo-label\">" + "Total Efectivo: </span><span style=\"float:right;\">" + $.number(datos[key].totalEfectivos, 2, ',', '.') + "</span></div>";
                        if (totalTitulos !== 0 || datos[key].totalEfectivos !== 0) {
                            oTable.fnAddData([
                                '',
                                '',
                                '',
                                '',
                                totalTitulosTag,
                                totalEfectivoTag
                            ], false);
                        }
                    }
                }
            });
            oTable.fnDraw();
            return false;
        }
        /////////////////////////////////////////////////
        ////// SE EJECUTA NADA MÁS CARGAR LA PÁGINA ////
        function initialize() {
            $scope.filter.fecha = getfechaHoy();
            cargarFechaActualizacionMovimientoAlias();
        }
        $document.ready(function () {
            initializeDataTable();
            initialize();
            loadIsines();
            loadCuentas();
            loadAlias();
            prepareCollapsion();
            $scope.fechaHoy = getfechaHoy();

        });
        $scope.cleanFilter = function (event) {
            $scope.filter = {"isin": "",
                "alias": "",
                "fecha": "",
                "cdcodigocuentaliq": "",
                "chisin" : false,
                "chalias" : false,
                "chcdcodigocuentaliq" : false
            };
        	$scope.btnInvGeneric('Alias');
    		$scope.btnInvGenerico('Isin');
    		$scope.btnInvGenerico('Cdcodigocuentaliq');            $

        };
        function initializeDataTable() {
            oTable = angular.element("#tblBalanceCliente").dataTable({
                "dom": 'T<"clear">lfrtip',
                "bSort": false,
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "aoColumns": [
                    {"sClass": "align-left"},
                    {"sClass": "align-left"},
                    {"sClass": "align-left"},
                    {"sClass": "align-left"},
                    {"sClass": "align-right"},
                    {"sClass": "align-right"}
                ]
            });
        }

        $scope.obtenerDatos = function (event) {

        	if (!validarFecha("fecha")) {
                alert("La Fecha no tiene el formato correcto");
            } else {

	        	if (comprobarfecha($scope.filter.fecha,$scope.fechaHoy) == 1)
	        	{
	        		alert("No se permiten fechas mayores a hoy");
	        	}
	        	else
	        	{
	            	var isin = (!$scope.filter.chisin && getIsin() !== "" ? "#" : "") + getIsin();
	            	var alias = (!$scope.filter.chalias && getAliasId() !== "" ? "#" : "") + getAliasId();
	            	var cuenta = (!$scope.filter.chcdcodigocuentaliq && $scope.filter.cdcodigocuentaliq.trim() !== "" ? "#" : "")  + $scope.filter.cdcodigocuentaliq.trim();

		            var filters = {"isin": isin,
		                "alias": alias,
		                "fecha": transformaFechaInv($scope.filter.fecha),
		                "cdcodigocuentaliq": cuenta};
		            seguimientoBusqueda();
		            inicializarLoading();
		            ConciliacionClearingService.getBalanceMovimientosCuentaVirtual(consultar, onConsultarErrorHandler, filters);
	        	}
            }
        };

        function onConsultarErrorHandler(data, status, headers, config) {
            growl.addErrorMessage(data);
        }
        //para ver el filtro al minimizar la búsqueda
        function seguimientoBusqueda() {
            $scope.rastroBusqueda = "";

            if ($scope.filter.isin !== "") {
            	if ($scope.filter.chisin)
            		$scope.rastroBusqueda += " isin: " + document.getElementById("isin").value;
            	else
            		$scope.rastroBusqueda += " isin distinto: " + document.getElementById("isin").value;
            }
            if ($scope.filter.alias !== "") {
            	if ($scope.filter.chalias)
            		$scope.rastroBusqueda += " alias: " + document.getElementById("alias").value;
            	else
            		$scope.rastroBusqueda += " alias distinto: " + document.getElementById("alias").value;
            }
            if ($scope.filter.fecha !== "") {
                $scope.rastroBusqueda += " fecha: " + $scope.filter.fecha;
            }
            if ($scope.filter.cdcodigocuentaliq !== "") {
            	if ($scope.filter.chcdcodigocuentaliq)
            		$scope.rastroBusqueda += " cuenta: " + $scope.filter.cdcodigocuentaliq;
            	else
            		$scope.rastroBusqueda += " cuenta distinto: " + $scope.filter.cdcodigocuentaliq;
            }
        }



        function getIsin() {
            //var isinCompleto = $scope.filter.isin.trim();
        	var isinCompleto = document.getElementById("isin").value; //modificado por alex 19Jan para realizar la busqueda correcta en Chrome
            var guion = isinCompleto.indexOf("-");
            if (guion > 0) {
                return isinCompleto.substring(0, guion);
            } else {
                return isinCompleto;
            }
        }
        function cargarCuentas(data, status, header, config) {
            if (data !== null && data.resultados !== null && data.resultados.result_cuentas_liquidacion_clearing !== null) {
                $scope.cuentas = data.resultados.result_cuentas_liquidacion_clearing;
            }
        }
        function cargarIsines(datosIsin) {
            var availableTags = [];
            angular.forEach(datosIsin, function (val, key) {
                var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                availableTags.push(ponerTag);
            });
            angular.element("#isin").autocomplete({
                source: availableTags
            });


        }
        function cargaAlias(data, status, headers, config) {
            var availableTagsAlias = [];
            angular.forEach(data.resultados.result_alias, function (val, key) {
                var ponerTag = val.alias + " - " + val.descripcion;
                availableTagsAlias.push(ponerTag);

            });
            // código de autocompletar
            angular.element("#alias").autocomplete({
                source: availableTagsAlias
            });
            $.unblockUI();
        }

        function getAliasId() {
            var alias = $("#alias").val();
            //var alias = $scope.filter.alias.trim(); //comentado por alex el 19Jan para que efectue busqueda correcta en chrome
            var guion = alias.indexOf("-");
            return (guion > 0) ? alias.substring(0, guion).trim() : alias;
        }

        $scope.btnInvAlias = function(){
        	$scope.btnInvGeneric('Alias');
        }

    	$scope.btnInvIsin = function() {
    		$scope.btnInvGenerico('Isin');
    	}

    	$scope.btnInvCdcodigocuentaliq = function() {
    		$scope.btnInvGenerico('Cdcodigocuentaliq');
    	}

    	$scope.btnInvGenerico = function( nombre ) {

    	  var check = '$scope.filter.ch' + nombre.toLowerCase();

      	  if(eval(check)){
      		  	  var valor2 = check + "= false";
      		  	  eval(valor2 ) ;

        		  document.getElementById('textBtnInv'+nombre).innerHTML = "<>";
        		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "red"});
        		  document.getElementById('textInv'+nombre).innerHTML = "DISTINTO";
        	  }else{
               	  var valor2 = check + "= true";
            	  eval(valor2 ) ;

        		  document.getElementById('textBtnInv'+nombre).innerHTML = "=";
        		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "green"});
        		  document.getElementById('textInv'+nombre).innerHTML = "IGUAL";
        	  }
        }


    }]);
