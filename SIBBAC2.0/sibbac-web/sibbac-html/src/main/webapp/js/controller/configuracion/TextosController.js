sibbac20
    .controller('TextosController',
                [
                 '$scope',
                 '$rootScope',
                 '$document',
                 'growl',
                 '$compile',
                 'ngDialog',
                 'TextosService',
                 'EtiquetasService',
                 '$location',
                 'SecurityService',
                 function ($scope, $rootScope, $document, growl, $compile, ngDialog, TextosService, EtiquetasService,
                           $location, SecurityService) {

                   var SHOW_LOG = false;

                   var oTable = undefined;

                   var contadorI = new Array();
                   $scope.needRefresh = false;
                   $scope.div = {};
                   $scope.div.idiomasTraducir = "";
                   $scope.div.textosTraducir = "";
                   $scope.etiquetas = {};
                   $scope.etiquetas.txtEtiquetas = "";
                   $scope.etiquetas.arrTxtEtiquetas = [];                  
                   $document.ready(function () {
                     loadDataTable();
                     cargarIdiomas();
                     cargarTabla();
                   });
                   function loadDataTable () {
                     oTable = $("#tblTextos").DataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                       },
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "retrieve": true,
                       "createdRow" : function (row, data, index) {
                         $compile(row)($scope);
                       }
                     });
                   }
                   $scope.seleccionado = {
                     id : "",
                     tipo : "",
                     descripcion : ""
                   };
                   
                   $scope.traducir = {};
                   $scope.traducir.txtEtiquetas = "";
                   $scope.traducir.idTextoOculto = "";

                   $scope.auxiliar = {};
                   $scope.auxiliar.idiomaActual = "";
                   $scope.listado = null;

                   $scope.idiomas = {};

                   $scope.editar = function () {
                     var seleccionado = $scope.seleccionado;
                     $scope.seleccionado.id = $scope.seleccionadoAux.id;
                     TextosService.modificarTexto(onSuccessSaveTexto, onErrorRequest, seleccionado);
                   };

                   function onSuccessSaveTexto (data) {
                     $scope.needRefresh = true;
                     ngDialog.closeAll();

                   }

                   function cargarTabla () {
                     inicializarLoading();
                     TextosService.getListaTextos(onSuccessCargarTable, onErrorRequest, {});
                   }
                   function onSuccessCargarTable (json) {
                     if (json === undefined || json === null || json.resultados === undefined) {
                       datos = {};
                     } else if (json.error !== undefined && json.error !== null && json.error !== "") {
                       growl.addErrorMessage(json.error);
                     } else {
                       var status = json.resultados.status;
                       if (status === "OK") {
                         $scope.listado = json.resultados.listado;
                         populateDataTableTextos();
                       }
                     }
                     $.unblockUI();
                   }
                   function populateDataTableTextos () {
                     oTable.clear();
                     for ( var k in $scope.listado) {
                       var tipo = "";
                       var item = $scope.listado[k];
                       if (item.tipo == "C") {
                         tipo = "Concepto";
                       }
                       else if (item.tipo == "K") {
                         tipo = "Recordatorio(Asunto)";
                       }
                       else if (item.tipo == "L") {
                         tipo = "Recordatorio(Cuerpo)";
                       }
                       else if (item.tipo == "B") {
                         tipo = "Cobro(Asunto)";
                       }
                       else if (item.tipo == "D") {
                         tipo = "Cobro(Cuerpo)";
                       }
                       else if (item.tipo == "I") {
                         tipo = "Factura(Asunto)";
                       }
                       else if (item.tipo == "J") {
                         tipo = "Factura(Cuerpo)";
                       }
                       else if (item.tipo == "G") {
                         tipo = "Anulaci&oacute;n(Asunto)";
                       }
                       else if (item.tipo == "H") {
                         tipo = "Anulaci&oacute;n(Cuerpo)";
                       }
                       else if (item.tipo == "E") {
                         tipo = "Duplicado(Asunto)";
                       }
                       else if (item.tipo == "F") {
                         tipo = "Duplicado(Cuerpo)";
                       }
                       else if (item.tipo == "M") {
                         tipo = "Nº Factura";
                       }
                       else if (item.tipo == "N") {
                         tipo = "Concepto de factura";
                       }
                       else if (item.tipo == "O") {
                         tipo = "Importe de factura";
                       }
                       else if (item.tipo == "P") {
                         tipo = "Importe base de factura";
                       }
                       else if (item.tipo == "Q") {
                         tipo = "Impuestos ";
                       }
                       else if (item.tipo == "R") {
                         tipo = "Total";
                       }
                       else if (item.tipo == "S") {
                         tipo = "Cuenta bancaria";
                       }
                       else if (item.tipo == "T") {
                         tipo = "IBAN";
                       }
                       else if (item.tipo == "U") {
                         tipo = "Código BIC";
                       }

                       // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                       var enlace = "";
                       var traducir = "";
                       if (SecurityService.inicializated) {
                         if (SecurityService.isPermisoAccion($rootScope.SecurityActions.TRADUCIR, $location.path())) {
                           traducir = "<a class=\"cursor\" ng-click= \"cargoInputsTraducir('" + item.id
                                      + "'); \"><img src='img/trad.gif' title='Traducir'/></a>&nbsp;";
                         }
                         if (SecurityService.isPermisoAccion($rootScope.SecurityActions.MODIFICAR, $location.path())) {
                             enlace = "<a class=\"cursor\" ng-click= \"cargarTexto('" + item.id + "'); \">" +
                             		"<img src='img/editp.png' title='Editar'/></a>";
                           }                         
                       }

                       oTable.row.add([ '<span>' + item.descripcion + '</span>', traducir + enlace ]);
                     }
                     oTable.draw();
                   }
                   function onErrorRequest (data) {
                     $.unblockUI();
                     growl.addErrorMessage("Ocurrió un error durante la petición de datos. " + data);
                   }
                   

                   function openTraducirForm () {
                	   $scope.openTraducirTextosForm();
                   };

                   function openModificarTextoForm () {
                     $scope.dialogTitle = 'Modificar texto';

                     ngDialog.open({
                       template : 'template/configuracion/textos/modificarTexto.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-150',
                       //controller : 'TextosController', //dont need to pass controller otherwise the scope is different and wont be able to load parameter through the scope
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           $scope.needRefresh = false;
                           cargarTabla();
                         }
                       }
                     });
                   };

                   $scope.itemsIdioma = [];
                   $scope.itemIdioma = {};
                   function loadResultadosTextos () {
                     var resultados = $scope.resultadosTextos;
                     angular.element('#idiomasTraducir').empty();
                     angular.element('#textosTraducir').empty();
                     var fila = "";
                     var capa = "";
                     var visible = "1";
                     var k = 0;
                     $scope.itemsIdioma = [];
                     $scope.itemIdioma = {};
                     var cont = 0;
                     contadorI = [];
                     for (k in resultados) {

                       var item = resultados[k];
                       $scope.itemsIdioma[cont] = item;
                       cont++;
                       var eldisplay = "block";
                       if (visible == "1") {

                         visible = "0";
                         $scope.auxiliar.idiomaActual = item.id;
                         $scope.itemIdioma = item;
                       } else {
                         eldisplay = "none";
                       }

                       contadorI.push(item.id);

                     }

                     angular.element("#idiomasTraducir").html($compile(fila)($scope));
                     angular.element("#textosTraducir").html($compile(capa)($scope));
                     growl.addInfoMessage("Cargado los idiomas con exito.")
                   }

                   $scope.openTraducirTextosForm = function () {
                     ngDialog.open({
                       template : 'template/configuracion/textos/traducir-textos-template.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-480',
                       scope : $scope,
                       preCloseCallback : function () {
                       }
                     });
                   }
                  
                   $scope.cargarTexto = function (id) {
                	   
                	   var selected = getItemById(id);
                       $scope.seleccionado.id = id;
                       $scope.seleccionado.tipo = selected.tipo;
                       $scope.seleccionado.descripcion = selected.descripcion;
                       $scope.seleccionadoAux = {
                      		 id : "",
                      		 tipo : "",
                      		 descripcion : ""
                       };
                       $scope.seleccionadoAux.id = id;
                       $scope.seleccionadoAux.tipo = selected.tipo;
                       $scope.seleccionadoAux.descripcion = selected.descripcion;
                       
                       openModificarTextoForm();
                     }                   

                   function getItemById(idInput){
                	   for ( var k in $scope.listado) {
                		   var item = $scope.listado[k];
                           if(idInput == item.id){
                        	   return item;
                           }
                	   }
                   }                   
                   var idAux = "";
                   $scope.cargoInputsTraducir = function (id) {
                     openTraducirForm();
                     cargarEtiquetas();
                     angular.element('.exito').empty();
                     idAux = id;
                     var seleccionado = {
                       idTexto : id
                     };
                     loadResultadosTextos();
                     TextosService.findTextoIdiomaByIdTexto(onFindTextoIdiomaSuccess, onErrorRequest, seleccionado);
                   }

                   $scope.textoIdiomas = [];
                   function onFindTextoIdiomaSuccess (json) {

                     if (json !== null && json !== undefined && json.resultados.resultado !== undefined
                         && json.resultados.resultado !== null) {
                       var resultados = json.resultados.resultado;
                       growl.addSuccessMessage(resultados.length + " textos encontrados.");
                       var item = null;
                       $scope.textoIdiomas = [];
                       for (var z = 0; z < contadorI.length; z++) {
                         var cap = contadorI[z];
                         angular.element("#textoIdioma").val('');
                         // angular.element("#idtextoIdioma" + cap).val('');
                       }
                       var contador = 0;
                       angular.forEach(resultados, function (val, key) {
                         // para cada dato, lo pongo en su id correspondiente
                         if ($scope.auxiliar.idiomaActual === val.idIdioma) {
                           angular.element("#textoIdioma").val(val.descripcion);
                         }
                         $scope.textoIdiomas[contador] = val;
                         contador++;
                       });
                       $scope.traducir.idTextoOculto = idAux;
                     } else {
                       growl.addInfoMessage("No se encontró ningún texto.")
                     }
                   }

                   function updateTextoIdiomas (id, idtexto, descripcion, textoIdioma) {

                     angular.element("#modificarTextos").attr('disabled', "disabled");

                     ididioma = id;
                     // Escapamos caracteres, en caso de haberlos.
                     descripcion = encodeURIComponent(descripcion);
                     // llamada al servicio de SIBBACServiceTextosIdiomas - updateTextosIdiomas
                     if (descripcion !== "") {
                       var seleccionado = {
                         id : textoIdioma,
                         descripcion : descripcion,
                         idTexto : idtexto,
                         idIdiomas : ididioma
                       };

                       TextosService.updateTextosIdiomas(onSuccessUpdateTextoIdioma, onErrorRequest, seleccionado);
                     } else {
                       growl.addInfoMessage("No se actualizan los textos vacios.")
                     }

                   }

                   function cerrarTraducirForm () {
                     ngDialog.closeAll();
                   };

                   function onSuccessUpdateTextoIdioma (json) {
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         cerrarTraducirForm();
                         growl.addInfoMessage("Dato cambiado con éxito.");
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error actualizando el idioma ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   };

                   $scope.guardarTextos = function () {
                     angular.element('#guardarTextos').attr('disabled', "disabled");
                     var idtexto = $scope.traducir.idTextoOculto;
                     var bEncontrado = false;
                     angular.forEach($scope.textoIdiomas, function (val, key) {
                       if ($scope.auxiliar.idiomaActual == val.idIdioma) {
                         bEncontrado = true;
                         val.descripcion = angular.element("#textoIdioma").val();
                       }
                     });
                     if (bEncontrado !== true) {
                       $scope.textoIdiomas.push({
                         idIdioma : $scope.auxiliar.idiomaActual,
                         descripcion : angular.element("#textoIdioma").val(),
                         idTextoIdioma : ""
                       });
                     }
                     for (var z = 0; z < contadorI.length; z++) {
                       var cap = contadorI[z];
                       var val = $scope.textoIdiomas[z];
                       if (val !== undefined) {

                         if ($scope.auxiliar.idiomaActual === val.idIdioma) {
                           val.descripcion = angular.element("#textoIdioma").val();
                         }
                         updateTextoIdiomas(cap, idtexto, val.descripcion, val.idTextoIdioma);
                       }

                     }
                   };
                   $scope.resultadosTextos = {};

                   function onSuccessGetListaIdiomas (json, status, headers, config) {
                     if (SHOW_LOG) {
                       console.log("cargando idiomas...");
                     }
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null && json.error !== "") {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.resultadosTextos = json.resultados;

                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error cargando idiomas ...");
                         growl.addErrorMessage(json.error);
                       }
                     }

                   };

                   function cargarIdiomas () {
                     TextosService.getListaIdiomas(onSuccessGetListaIdiomas, onErrorRequest);
                   };

                   $scope.mostrarIdioma = function (item) {
                     var bEncontrado = false;
                     angular.forEach($scope.textoIdiomas, function (val, key) {
                       if ($scope.auxiliar.idiomaActual == val.idIdioma) {
                         bEncontrado = true;
                         val.descripcion = angular.element("#textoIdioma").val();
                       }
                     });

                     if (bEncontrado !== true) {
                       $scope.textoIdiomas.push({
                         idIdioma : $scope.auxiliar.idiomaActual,
                         descripcion : angular.element("#textoIdioma").val()
                       });
                     }
                     $scope.auxiliar.idiomaActual = item.id;
                     bEncontrado = false;
                     angular.forEach($scope.textoIdiomas, function (val, key) {
                       if ($scope.auxiliar.idiomaActual == val.idIdioma) {
                         bEncontrado = true;
                         angular.element("#textoIdioma").val(val.descripcion);
                       }
                     });
                     if (bEncontrado !== true) {
                       angular.element("#textoIdioma").val("");
                     }

                     angular.element("#spanTitIdioma").text(item.descripcion);
                   }

                   function listaEtiquetasSuccess (json, status, headers, config) {

                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         var resultados = json.resultados;
                         var contador = 0;
                         $scope.etiquetas.txtEtiquetas = "0";

                         angular.forEach(resultados, function (val, key) {
                           var seleccionadoetiquetaAux = {};
                           seleccionadoetiquetaAux = {
                             id : val.id,
                             etiqueta : val.etiqueta
                           };
                           contador++;
                           $scope.etiquetas.arrTxtEtiquetas[contador] = seleccionadoetiquetaAux;
                           // para cada dato, lo pongo en su id correspondiente
                           angular.element('#txtEtiquetas').append(
                                                                   "<option value='" + val.id + "'>" + val.etiqueta
                                                                       + "</option>");
                         });

                         console.log(contador + " etiquetas encontradas.");
                         growl.addSuccessMessage(contador + " etiquetas encontradas.");
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error cargando etiquetas ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }

                   // cargar etiquetas
                   function cargarEtiquetas () {
                     angular.element('#txtEtiquetas').empty();
                     $scope.etiquetas.arrTxtEtiquetas = [];
                     angular.element('#txtEtiquetas').append("<option value='0'>Seleccione una etiqueta</option>");
                     EtiquetasService.getListaEtiquetas(listaEtiquetasSuccess, onErrorRequest);
                   }

                   function getTextoSeleccionado (idEtiqueta) {
                     var textoEtiqueta = "";
                     var cont = 0;
                     angular.forEach($scope.etiquetas.arrTxtEtiquetas, function (val, key) {
                       cont++;
                       if (idEtiqueta == val.id) {
                         textoEtiqueta = val.etiqueta;
                         return false;
                       }
                     });
                     return textoEtiqueta;
                   };

                   // añadir etiqueta al textarea
                   $scope.annadirEtiqueta = function () {

                     var etiqueta = $scope.etiquetas.txtEtiquetas;
                     if (etiqueta === "0") {
                       alert("Seleccione una etiqueta");
                       return false;
                     }
                     var textoSeleccionadoEtiqueta = getTextoSeleccionado(etiqueta);
                     var textoActual = angular.element("#textoIdioma").val();
                     var textoFinal = textoActual + " " + textoSeleccionadoEtiqueta;
                     angular.element("#textoIdioma").val(textoFinal);
                   }

                 } ]);
