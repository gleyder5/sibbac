'use strict';
sibbac20.controller('GruposImpositivosController',
                    [
                     '$scope',
                     '$compile',
                     '$http',
                     '$rootScope',
                     'IsinService',
                     '$location',
                     'SecurityService',
                     function ($scope, $compile, $http, $rootScope, IsinService, $location, SecurityService) {

                       var oTable = undefined;

                       $(document).ready(function () {
                         oTable = $("#tablaGruposImpositivos").dataTable({
                           "dom" : 'T<"clear">lfrtip',
                           "tableTools" : {
                             "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                           },
                           "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                             $compile(nRow)($scope);
                           },
                           "language" : {
                             "url" : "i18n/Spanish.json"
                           },
                           "scrollY" : "480px",
                           columnDefs : [ {
                             type : 'date-eu',
                             targets : [ 4 ]
                           } ]
                         });
                         cargarTabla();
                         initialize();
                         prepareCollapsion();
                       });

                       /* BUSCADOR */
                       $('.btn.buscador').click(function () {
                         // event.preventDefault();
                         $('.resultados').show();
                         collapseSearchForm();
                         return false;
                       });

                       // funcion para cargar la tabla
                       function cargarTabla () {
                         var data = new DataBean();
                         data.setService('SIBBACServiceGruposImpositivos');
                         data.setAction('getGruposImpositivos');
                         console.log(data);
                         var request = requestSIBBAC(data);
                         // var request =
                         // requestSIBBAC('SIBBACServiceGruposImpositivos','getGruposImpositivos','','','');
                         request.success(function (json) {
                           var resultados = json.resultados;
                           var item = null;
                           var contador = 0;
                           var lacategoria = null;
                           var locancelado = null;
                           var estilo = null;
                           oTable.fnClearTable();
                           for ( var k in resultados) {
                             item = resultados[k];
                             if (contador % 2 === 0) {
                               estilo = "even";
                             } else {
                               estilo = "odd";
                             }
                             if (item.categoria === "y" || item.categoria === "s" | item.categoria === "Y"
                                 || item.categoria === "S") {
                               lacategoria = "Repercutido";
                             }
                             if (item.categoria === "n" || item.categoria === "N") {
                               lacategoria = "Soportado";
                             }
                             if (item.cancelado === "y" || item.cancelado === "s" | item.cancelado === "Y"
                                 || item.cancelado === "S") {
                               locancelado = "S&iacute;";
                             }
                             if (item.cancelado === "n" || item.cancelado === "N") {
                               locancelado = "No";
                             }

                             // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                             var enlace = "";
                             if (SecurityService.inicializated) {

                               if (SecurityService.isPermisoAccion($rootScope.SecurityActions.MODIFICAR, $location
                                   .path())) {
                                 enlace = "<a class=\"btn\"  ng-click = \" cargoInputs('" + item.id + "','"
                                          + item.porcentaje + "','" + transformaFechaGuion(item.validoDesde)
                                          + "'); \">" + "<img src='img/editp.png' title='Editar'/></a>";
                               }

                             }

                             oTable.fnAddData([ item.descripcion, item.porcentaje, lacategoria, locancelado,
                                               transformaFechaGuion(item.validoDesde), enlace ], false);
                             contador++;
                           }
                           oTable.fnDraw();
                           // "Footer" de la tabla "padre".
                           // $('.tablaPrueba').append("</table>");
                           // $("#tablaGruposImpositivos").tablesorter({sortList:[[0,0],[2,1]], widgets: ['zebra']});
                           // dragtable.makeDraggable($('#tablaGruposImpositivos'));
                           // quito el loading
                           // document.getElementById('loading').style.display = 'none';
                           // document.getElementById('centrarLoad').style.display = 'none';
                         });
                       }

                       // Evento para dar de alta la regla
                       $scope.altaGrupoImpositivo = function () {
                         var codigo = document.getElementById('textCodigo').value;
                         var descripcion = document.getElementById('textDesc').value;
                         var porcentaje = document.getElementById('textPorcen').value;
                         var categoria = document.getElementById('textCategoria').value;
                         var cancelado = document.getElementById('textCancelado').value;
                         if (porcentaje > 100) {
                           alert("El porcentaje no puede ser superior al 100%");
                           return false;
                         }
                         var validoDesde = document.getElementById('fechaValido').value;
                         console.log("fin de datos");
                         if (codigo == "" || descripcion == "" || porcentaje == "" || categoria == ""
                             || cancelado == "" || validoDesde == "") {
                           alert("Alguno de los campos es vacío. Debe rellenarlo para poder dar de alta");
                           return false;
                         } else {
                           console.log("entro en dar alta");
                           altaGruposImpositivos(codigo, descripcion, porcentaje, categoria, cancelado, validoDesde);
                         }
                       };

                       // Funcion para el alta de la regla
                       function altaGruposImpositivos (codigo, descripcion, porcentaje, categoria, cancelado,
                                                       validoDesde) {
                         if (cancelado == 'REPERCUTIDA') {
                           cancelado = "r";
                         } else {
                           cancelado = "s";
                         }
                         if (categoria == "on") {
                           categoria = "y";
                         } else {
                           categoria = "n";
                         }
                         var filtro = "{\"id\" : \"" + codigo + "\"," + "\"descripcion\" : \"" + descripcion + "\","
                                      + "\"porcentaje\" : \"" + porcentaje + "\"," + "\"categoria\" : \"" + categoria
                                      + "\"," + "\"cancelado\" : \"" + cancelado + "\"," + "\"validoDesde\" : \""
                                      + transformaFechaInv(validoDesde) + "\" }";
                         var jsonData = "{" + "\"service\" : \"SIBBACServiceGruposImpositivos\", "
                                        + "\"action\"  : \"createGrupoImpositivo\", " + "\"filters\"  : " + filtro
                                        + "}";
                         console.log(jsonData);
                         $.ajax({
                           type : "POST",
                           dataType : "json",
                           url : "/sibbac20back/rest/service",
                           data : jsonData,
                           beforeSend : function (x) {
                             if (x && x.overrideMimeType) {
                               x.overrideMimeType("application/json");
                             }
                             // CORS Related
                             x.setRequestHeader("Accept", "application/json");
                             x.setRequestHeader("Content-Type", "application/json");
                             x.setRequestHeader("X-Requested-With", "HandMade");
                             x.setRequestHeader("Access-Control-Allow-Origin", "*");
                             x.setRequestHeader('Access-Control-Allow-Headers',
                                                'Origin, X-Requested-With, Content-Type, Accept')
                           },
                           async : true,
                           success : function (json) {
                             cargarTabla();
                             document.getElementById("cargarTabla").reset();
                           },
                           error : function (c) {
                             alert("ERROR: " + c);
                           }
                         });
                       }

                       $scope.editarGrupo = function () {
                         var codigo = document.getElementById('textIdGrEdit').value;
                         var FechaEdit = document.getElementById('textFechaEdit').value;
                         var porcentaje = document.getElementById('textPorcenEdit').value;

                         if (!validarFecha('textFechaEdit')) {
                           alert("La fecha introducida no tiene el formato correcto: dd/mm/yyyy");
                           return false;
                         } else {
                           var filtro = "{\"id\" : \"" + codigo + "\"," + "\"porcentaje\" : \"" + porcentaje + "\","
                                        + "\"validoDesde\" : \"" + transformaFechaInv(FechaEdit) + "\"}";
                           var jsonData = "{" + "\"service\" : \"SIBBACServiceGruposImpositivos\", "
                                          + "\"action\"  : \"modificarGruposImpositivos\", " + "\"filters\"  : "
                                          + filtro + "}";
                           $.ajax({
                             type : "POST",
                             dataType : "json",
                             url : "/sibbac20back/rest/service",
                             data : jsonData,
                             beforeSend : function (x) {
                               if (x && x.overrideMimeType) {
                                 x.overrideMimeType("application/json");
                               }
                               // CORS Related
                               x.setRequestHeader("Accept", "application/json");
                               x.setRequestHeader("Content-Type", "application/json");
                               x.setRequestHeader("X-Requested-With", "HandMade");
                               x.setRequestHeader("Access-Control-Allow-Origin", "*");
                               x.setRequestHeader('Access-Control-Allow-Headers',
                                                  'Origin, X-Requested-With, Content-Type, Accept')
                             },
                             async : true,
                             success : function (json) {
                               if (json.resultados == null) {
                                 alert(json.error);
                               } else {
                                 cargarTabla();
                                 document.getElementById('formulariosModificarGrupo').style.display = 'none';
                                 document.getElementById('fade').style.display = 'none';

                               }
                             },
                             error : function (c) {
                               console.log("ERROR: " + c);
                             }
                           });
                         }
                       }

                       $scope.cargoInputs = function (codigo, porcentaje, fecha) {

                         document.getElementById('textIdGrEdit').value = codigo;
                         document.getElementById('textPorcenEdit').value = porcentaje;
                         document.getElementById('textFechaEdit').value = fecha;

                         document.getElementById('formulariosModificarGrupo').style.display = 'block';
                         document.getElementById('fade').style.display = 'block';

                       }

                       $scope.refrescarGruposImpositivos = function () {
                         cargarTabla();
                       }
                       function initialize () {
                         var fechas = [ "fechaValido", "textFechaEdit" ];
                         loadpicker(fechas);
                         // $("#tablaGruposImpositivos").tablesorter({sortList:[[0,0],[2,1]], widgets: ['zebra']});
                         // dragtable.makeDraggable($('#tablaGruposImpositivos'));
                       }

                     } ]);
