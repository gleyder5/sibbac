(function(angular, sibbac20, gc, undefined) {
	"use strict";
	
	var ImputacionS3Controller = function(service, CuentaCompensacionService, $location) {
		this.service = service;
		this.location = $location;
		var fatDialogOpt = { width: 480 }, self = this;
				
		this.config = service.getConfig();
		
		this.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen : false,
			options : fatDialogOpt,
			modal : true,
			buttons : {
				"Cerrar" : function() {
					self.resultDialog.dialog("close");
					if (self.result.ok) {
						self.reload();
					}
				}
			}
		});
		
		this.listCuentasCompensacion = [];
		CuentaCompensacionService.getCuentasDeCompensacion(function(data){
			self.listCuentasCompensacion = data.resultados.result_cuentas_compensacion;
			console.log("Cargadas cuentas: " + self.listCuentasCompensacion);
		}, function(msg){
			self.result.ok = false;
			self.result.errorMessages.push(msg);
			console.log("error cargando cuentas compensacion: " + msg);
			self.resultDialog.dialog("open");
		}, {});
		
	};
	
	ImputacionS3Controller.prototype.constructor = ImputacionS3Controller;
	
	ImputacionS3Controller.prototype.reload = function (){
		console.log("inicio reload");
		inicializarLoading();
		this.config = this.service.getConfig();
		$.unblockUI();
		console.log("fin reload");
	}
	
	ImputacionS3Controller.prototype.update = function (){
		console.log("inicio update");
		var self = this;
		inicializarLoading();
		
		this.service.updateConfig(this.config, function(result) {
			console.log("update con Exito");
			$.unblockUI();
			self.result = result;
			self.resultDialog.dialog("open");
		}, function(result){
			console.log("fail");
			self.result = result;
			self.resultDialog.dialog("open");
		});
		console.log("fin update");
		
	}
	
	ImputacionS3Controller.prototype.imputacionPti = function (){
		console.log("inicio imputacionPti");
		var self = this;
		inicializarLoading();
		
		this.service.imputacionPti(function(result) {
			console.log("imputacionPti con Exito");
			$.unblockUI();
			self.result = result;
			self.resultDialog.dialog("open");
		}, function(result){
			console.log("fail");
			self.result = result;
			self.reload();
			self.resultDialog.dialog("open");
			
		});
		console.log("fin imputacionPti");
	}
	
	ImputacionS3Controller.prototype.imputacionEuroCcp = function (){
		console.log("inicio imputacionEuroCcp");
		var self = this;
		inicializarLoading();
		
		this.service.imputacionEuroCcp(function(result) {
			console.log("imputacionEuroCcp con Exito");
			$.unblockUI();
			self.result = result;
			self.resultDialog.dialog("open");
		}, function(result){
			console.log("fail");
			self.result = result;
			self.reload();
			self.resultDialog.dialog("open");
			
		});
		console.log("fin imputacionEuroCcp");
	}
	
	ImputacionS3Controller.prototype.imputacionPtiMab = function (){
		console.log("inicio imputacionPtiMab");
		var self = this;
		inicializarLoading();
		
		this.service.imputacionPtiMab(function(result) {
			console.log("imputacionPtiMab con Exito");
			$.unblockUI();
			self.result = result;
			self.resultDialog.dialog("open");
		}, function(result){
			console.log("fail");
			self.result = result;
			self.reload();
			self.resultDialog.dialog("open");
			
		});
		console.log("fin imputacionPtiMab");
	}
	
	sibbac20.controller("ImputacionS3Controller", ["ImputacionS3Service", "CuentaCompensacionService", "$location", ImputacionS3Controller ]);
})(angular, sibbac20, GENERIC_CONTROLLERS);