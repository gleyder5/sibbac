'use strict';
sibbac20.controller('AliasController',
                    [
                     '$scope',
                     '$rootScope',
                     '$compile',
                     'growl',
                     'AliasService',
                     'TextosService',
                     '$document',
                     'ngDialog',
                     '$location',
                     'SecurityService',

                     function ($scope, $rootScope, $compile, growl, AliasService, TextosService, $document, ngDialog,
                               $location, SecurityService) {

                       var idOcultos = [];
                       var availableTags = [];
                       var oTable = undefined;
                       var SHOW_LOG = false;

                       var NL = "\n";
                       var BR = "<br/>";

                       $scope.needRefresh = false;

                       // filtros
                       $scope.filtro = {};
                       // filtro de modificacion
                       $scope.filtro.mod = {};
                       $scope.filtro.mod.aliasO = "";
                       $scope.filtro.mod.txtAliasF = "";
                       $scope.filtro.mod.txtIdioma = "";

                       $scope.limpiarFiltro = function () {
                         $scope.filtro.mod.aliasO = "";
                         $scope.filtro.mod.txtAliasF = "";
                         $scope.filtro.mod.txtIdioma = "";
                       };

                       $.blockUI({
                         message : '<h1><img src="img/loading.gif" /> Cargando datos...</h1>',
                         css : {
                           border : 'none',
                           padding : '15px',
                           backgroundColor : '#000',
                           '-webkit-border-radius' : '10px',
                           '-moz-border-radius' : '10px',
                           opacity : .5,
                           color : '#fff'
                         }
                       });

                       function initializaTable () {
                         oTable = $("#tblAlias").dataTable({
                           "dom" : 'T<"clear">lfrtip',
                           "tableTools" : {
                             "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                           },
                           "language" : {
                             "url" : "i18n/Spanish.json"
                           },
                           /* Disable initial sort */
                           "order" : [],
                           "scrollY" : "480px",
                           "scrollCollapse" : true,
                           "columns" : [ {
                             "width" : "auto"
                           }, {
                             "width" : "auto"
                           }, {
                             "width" : "10%"
                           }, {
                             "width" : "10%"
                           } ],
                           /* "bSort" : false, Deshabilita todos los sort */
                           "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                             $compile(nRow)($scope);
                           }
                         });
                       }
                       ;

                       function showError (data, status, headers, config) {
                         $.unblockUI();
                         growl.addErrorMessage("Error: " + data);
                       }
                       ;

                       function openEditContactoForm () {
                         ngDialog.open({
                           template : 'template/alias/editAlias.html',
                           className : 'ngdialog-theme-plain custom-width custom-height-320',
                           scope : $scope,
                           preCloseCallback : function () {
                             if ($scope.needRefresh) {
                               $scope.needRefresh = false;
                               cargarTabla();
                             }
                             $scope.limpiarFiltro();
                           }
                         });
                       }
                       ;

                       function successCargarAliasF (json, status, headers, config) {
                         if (SHOW_LOG)
                           console.log("cargar alias en la ventana modal ...");

                         var datos = {};
                         if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                           if (json.error !== null) {
                             growl.addErrorMessage(json.error);
                           } else {
                             var resultados = json.resultados.listaAlias;
                             var html = '<label for="txtAliasF" class="desresumen">Alias Facturaci&oacute;n : </label>'
                                        + BR + NL;
                             html += "<select id=\"txtAliasF\" name=\"txtAliasF\" ng-model=\"filtro.mod.txtAliasF\" >"
                                     + NL;
                             for ( var k in resultados) {
                               var item = resultados[k];
                               var alias = item.nombre;
                               html += "<option value='" + item.id + "'>" + alias + "</option>" + BR + NL;
                             }
                             html += "</select>";
                             angular.element("#divAlias").html($compile(html)($scope));
                           }
                         } else {
                           if (json !== undefined && json.error !== null) {
                             if (SHOW_LOG)
                               console.log("error cargando alias en la ventana modal ...");
                             growl.addErrorMessage(json.error);
                           }
                         }
                       }
                       ;

                       function cargarAliasF (id) {
                         var filtro = {
                           idAlias : id
                         };
                         AliasService.getListaAliasFacturacionAlias(successCargarAliasF, showError, filtro);
                       }
                       ;

                       function successgetListaIdiomas (json, status, headers, config) {
                         if (SHOW_LOG)
                           console.log("cargar idiomas en la ventana modal ...");
                         var datos = {};
                         if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                           if (json.error !== null) {
                             growl.addErrorMessage(json.error);
                           } else {
                             var resultados = json.resultados;
                             var html = '<label for="txtIdioma" class="desresumen">Idioma : </label>' + BR + NL;
                             html += "<select id=\"txtIdioma\" name=\"txtIdioma\" ng-model=\"filtro.mod.txtIdioma\" >"
                                     + NL;
                             for ( var k in resultados) {
                               var item = resultados[k];
                               html += "<option value='" + item.id + "'>" + item.descripcion + "</option>" + BR + NL;
                             }
                             html += "</select>";
                             angular.element("#divIdiomas").html($compile(html)($scope));
                           }
                         } else {
                           if (json !== undefined && json.error !== null) {
                             if (SHOW_LOG)
                               console.log("error cargando alias en la ventana modal ...");
                             growl.addErrorMessage(json.error);
                           }
                         }
                       }
                       ;

                       function cargarIdiomas () {
                         TextosService.getListaIdiomas(successgetListaIdiomas, showError);
                       }

                       $scope.cargoInputs = function (id, ida, idi, alias, aliasf) {
                         openEditContactoForm();
                         cargarAliasF(id);
                         cargarIdiomas();

                         $scope.dialogTitle = "Editar";
                         $scope.nombreAlias = alias;

                         $scope.filtro.mod.aliasO = id;
                         $scope.filtro.mod.txtAliasF = ida;
                         $scope.filtro.mod.txtIdioma = idi;

                       };

                       function successListaAlias (json, status, headers, config) {
                         if (SHOW_LOG)
                           console.log("listar alias ...");
                         var now = Date.now();
                         var datos = {};
                         if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                           if (json.error !== null) {
                             growl.addErrorMessage(json.error);
                           } else {
                             var resultados = json.resultados.listaAlias;
                             oTable.fnClearTable();

                             var k = 0;

                             for (k in resultados) {
                               var item = resultados[k];
                               var alias = item.nombre;
                               var aliasF = item.nombreAliasFacturacion;
                               var aliasMod = alias.replace(/\'/g, "\\'");
                               var aliasFMod = aliasF.replace(/\'/g, "\\'");

                               // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                               var enlace = "";
                               if (SecurityService.inicializated) {
                                 if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR, $location
                                     .path())) {
                                   enlace = "<a class=\"btn\" ng-click=\"cargoInputs('" + item.id + "','"
                                            + item.idAliasFacturacion + "','" + item.idIdioma + "','" + aliasMod
                                            + "','" + aliasFMod
                                            + "'); \"><img src='img/editp.png' title='Editar'/></a>";
                                 }
                               }

                               oTable.fnAddData([ alias, aliasF, item.descipcionIdioma, enlace ], false);
                             }
                             oTable.fnDraw();
                             console.log("Se ha tardado en pintar la tabla :" + ((Date.now() - now) / 1000));

                           }
                         } else {
                           if (json !== undefined && json.error !== null) {
                             if (SHOW_LOG)
                               console.log("error listando alias ...");
                             growl.addErrorMessage(json.error);
                           }
                         }
                         $.unblockUI();
                       }
                       ;

                       function cargarTabla () {
                         // $.blockUI();
                         inicializarLoading();
                         AliasService.getListaAlias(successListaAlias, showError);
                       }
                       ;

                       $(document).ready(function () {
                         if (oTable === undefined) {
                           initializaTable();
                         }
                         cargarTabla();
                       });

                       function successModificarAlias (json, status, headers, config) {
                         if (SHOW_LOG)
                           console.log("modificar alias ...");
                         var datos = {};
                         if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                           if (json.error !== null) {
                             growl.addErrorMessage(json.error);
                           } else {
                             $scope.needRefresh = true;
                             ngDialog.closeAll();
                           }
                         } else {
                           if (json !== undefined && json.error !== null) {
                             if (SHOW_LOG)
                               console.log("error modificando alias ...");
                             growl.addErrorMessage(json.error);
                           }
                         }
                       }
                       ;

                       $scope.modificarAlias = function () {

                         var idA = $scope.filtro.mod.aliasO;
                         var idF = $scope.filtro.mod.txtAliasF;
                         var idI = $scope.filtro.mod.txtIdioma;

                         var filtro = {
                           idAlias : idA,
                           idAliasFacturacion : idF,
                           idIdioma : idI
                         };

                         AliasService.modificarAlias(successModificarAlias, showError, filtro);
                       };

                     } ]);
