'use strict';
sibbac20.controller('FacturaEmitidaController', ['$scope', 'AliasService', 'EstadoService', 'FacturaService', 'growl', '$compile', '$document', function ($scope, AliasService, EstadoService, FacturaService, growl, $compile, $document) {
        'use strict';

        var idOcultos = [];
        var availableTags = [];
        var idAlias = "";
        var fechaDesde = "";
        var fechaHasta = "";
        var oTable = undefined;

        $scope.numeroFactura = "";
        $scope.idEstado = "";

        $scope.historico = {};
        $scope.historico.numeroFactura = '';

        $scope.filtro = {};



        function showError(data, status, headers, config) {
            $.unblockUI();
            growl.addErrorMessage("Error: " + data);
        }
        function loadAlias() {
            AliasService.getAliasFactura(cargarAlias, showError);
        }
        function loadEstados() {
            EstadoService.getEstadosFactura(cargarEstados, showError);
        }

        function loadDataTable() {
            oTable = angular.element("#tblFacturasEmitidas").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "columns": [
                    {"width": "17%"},
                    {"width": "17%"},
                    {"width": "8%", type: "date-eu"},
                    {"width": "10%", type: "date-eu"},
                    {"width": "10%"},
                    {width: "10%", sClass: "monedaR", type: "formatted-num"},
                    {"width": "10%", sClass: "monedaR", type: "formatted-num"},
                    {"width": "8%", sClass: "monedaR", type: "formatted-num"},
                    {"width": "8%", type: "date-eu"},
                    {"width": "8%", type: "date-eu"}
                ],
                "scrollY": "480px",
                "scrollCollapse": true,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    $compile(nRow)($scope);
                }
            });
        }

        function cargarTablaFacturas()
        {
        	inicializarLoading();
        	FacturaService.getListaFacturas(cargarTabla, showError, $scope.filtro);
        }

        $scope.searchFacturas = function (event) {
        	if (event !== undefined && event !== null)
        	{
        		event.preventDefault();
        	}
            if (validarCampoFecha('fechaDesde', false) && validarCampoFecha('fechaHasta', false)) {
                var fechaDesde = angular.element('#fechaDesde').val();
                var fechaHasta = angular.element('#fechaHasta').val();
                var fechas = ['fechaDesde', 'fechaHasta'];
                loadpicker(fechas);

                var numeroFactura =  angular.element("#numeroFactura").val();
                var idAlias = getIdAlias();
                var idEstado =  angular.element("#idEstado").val();
                collapseSearchForm();

                $scope.filtro = {};
                if (numeroFactura !== "") {
                	$scope.filtro = {numeroFactura: numeroFactura};
                } else {
                	$scope.filtro = {idAlias: idAlias, fechaDesde: transformaFechaInv(fechaDesde),
                        fechaHasta: transformaFechaInv(fechaHasta), idEstado: idEstado};
                }
                cargarTablaFacturas();
            } else {
                alert("Los campos de fecha introducidos no son correctos. Por favor revíselos.");
            }
        };

        $scope.limpiarFiltro = function() {
            //ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fechaHasta" ).datepicker( "option", "minDate", "");
            $( "#fechaDesde" ).datepicker( "option", "maxDate", "");

            //

            angular.element('input[type=text]').val('');
            angular.element('select').val('');
        };

        $scope.ocultarHistorialFactura = function() {
       	 angular.element("#formulariosHistorialFactura").hide();
       	 angular.element("#fade").hide();
       };

        $scope.ocultarDatosFactura = function() {
        	 angular.element("#formulariosDatosFactura").hide();
        	 angular.element("#fade").hide();
        };

        $document.ready(function () {
            loadDataTable();
            prepareCollapsion();

										            /* BUSCADOR */
            ajustarAnchoDivTable();
            var fechas = ['fechaHasta', 'fechaDesde'];
            //loadpicker(fechas);
            //verificarFechas([['fechaDesde', 'fechaHasta']]);
            loadAlias();
            loadEstados();
            $("#ui-datepicker-div").remove();
			$('input#fechaDesde').datepicker({
			    onClose: function( selectedDate ) {
			      $( "#fechaHasta" ).datepicker( "option", "minDate", selectedDate );
			    } // onClose
			  });

			  $('input#fechaHasta').datepicker({
			    onClose: function( selectedDate ) {
			      $( "#fechaDesde" ).datepicker( "option", "maxDate", selectedDate );
			    } // onClose
			  });
        });



        function getIdAlias() {
            var idAlias = "";
            var textAlias = angular.element("#textAlias").val();
            if ((textAlias !== null) && (textAlias !== '')) {
                idAlias = idOcultos[availableTags.indexOf(textAlias)];
                if (idAlias === undefined) {
                    idAlias = "";
                }
            }
            return idAlias;
        }

        function cargarAlias(datos) {
            var item = null;
            for (var k in datos) {
                item = datos[k];
                idOcultos.push(item.id);
                availableTags.push(item.nombre.trim());
            }
            // código de autocompletar

            angular.element("#textAlias").empty();
            angular.element("#textAlias").autocomplete({
                source: availableTags
            });
            growl.addInfoMessage("Alias cargados correctamente.")
        }
        $scope.mostrarFormularioDatos = function (numeroFactura, idFactura) {
        	angular.element('#formulariosDatosFactura').show();
        	angular.element('#fade').show();
            var filtro = {facturaId: idFactura};
            angular.element('#df').append(numeroFactura);
            FacturaService.getOrdenesFactura(cargoInputsFactura, showError, filtro);
        };

        $scope.mostrarFormularioHistorico = function (numeroFactura, idFactura) {
        	$scope.historico.numeroFactura = '';
        	angular.element('#formulariosHistorialFactura').show();
        	angular.element('#fade').show();
            var filtro = {idFactura: idFactura};
            growl.addInfoMessage("Cargando Histórico....");
            angular.element('#hf').append(numeroFactura);
            $scope.historico.numeroFactura = numeroFactura;
            FacturaService.getTransicionesDeFactura(cargarHistorico, showError, filtro);
        };
        function createActionsCell(numeroFactura, idFactura) {
            var actionCell = numeroFactura + '<div class="lupa">';
            actionCell += '<a ng-click="mostrarFormularioDatos(' + numeroFactura + ', ' + idFactura + ')">';
            actionCell += '<img src="img/lupa.gif"></a>&nbsp;';
            actionCell += '<a ng-click ="mostrarFormularioHistorico(' + numeroFactura + ', ' + idFactura + ');">';
            actionCell += '<img src="img/hist.gif"></a>&nbsp;';
            actionCell += '<a ng-click ="duplicarFactura(' + idFactura + ',' + numeroFactura + ');" >';
            actionCell += '<img src="img/duplic.gif"></a>';
            actionCell += '</div>';
            return actionCell;
        }
        function cargarTabla(datos) {

            oTable.fnClearTable();
            var item = null;
            for (var k in datos) {
                item = datos[k];
                oTable.fnAddData([
                    createActionsCell(item.numeroFactura, item.idFactura),
                    "<span>" + item.cdAlias.trim() + " - " + item.nombreAlias.trim() + "</span>",
                    "<span>" + transformaFecha(item.fechaCreacion) + "</span>",
                    "<span>" + sumoFecha(item.fechaCreacion) + "</span>",
                    "<span>" + item.descEstado + "</span>",
                    "<span>" + $.number(item.baseImponible, 2, ',', '.') + "</span>",
                    "<span>" + $.number(item.importeImpuestos, 2, ',', '.') + "</span>",
                    "<span>" + $.number(item.baseImponible, 2, ',', '.') + "</span>",
                    "<span>" + transformaFecha(item.fechaInicio) + "</span>",
                    "<span>" + transformaFecha(item.fechaFin) + "</span>"]);
            }
            $.unblockUI();

        }

        // listado estados
        function cargarEstados(datos) {
            var item = null;

            for (var k in datos) {
                item = datos[k];
                // Rellenamos el combo
                angular.element('#idEstado').append("<option value='" + item.idestado + "'>" + item.nombre + "</option>");
            }
            growl.addInfoMessage("Estados cargados correctamente.");
        }

        // listado estados
        function cargarHistorico(datos) {
        	angular.element('#hf').empty();
        	var numeroFactura = $scope.historico.numeroFactura;
        	$scope.historico.numeroFactura = '';
        	angular.element('#tablaHistorial').empty();
            var item = null;
            angular.element('#tablaHistorial').append("<tr>" + "<th>N&uacute;mero Factura</th>" + "<th>Estado</th>" + "<th>Fecha</th>" + "</tr>");
            for (var k in datos) {
                item = datos[k];
                // Rellenamos la tabla
                angular.element('#tablaHistorial').append(
                        "<tr>" + "<td>" + numeroFactura + "</td>" + "<td>" + item.estado + "</td>" + "<td>"
                        + transformaFechaHora(item.fecha) + "</td>" + "</tr>");
            }

        }

        // listado lupa
        function cargoInputsFactura(datos) {
        	angular.element('#df').empty();
        	angular.element('#tablaDatos').empty();
            //
            var item = null;
            angular.element('#tablaDatos').append(
                    "<tr>" + "<th>Orden</th>" + "<th>Booking</th>" + "<th>Num</th>" + "<th>Ref. Banco</th>" + "<th>L&iacute;quido</th>"
                    + "<th>Neto</th>" + "<th>Bruto</th>" + "<th>Comisiones</th>" + "<th>T&iacute;tulo</th>" + "</tr>");
            for (var k in datos) {
                item = datos[k];
                // Rellenamos la tabla
                angular.element('#tablaDatos').append(
                        "<tr>" + "<td class='taleft'>" + item.nOrden + "</td>" + "<td class='taleft'>" + item.nBooking + "</td>"
                        + "<td class='taleft'>" + item.nUcnfclt + "</td>" + "<td class='tacenter'>" + item.cdrefban + "</td>"
                        + "<td class='taright'>" + $.number(item.imnetliq, 2, ',', '.') + "</td>" + "<td class='taright'>" + $.number(item.imtotnet, 2, ',', '.') + "</td>"
                        + "<td class='taright'>" + $.number(item.imtotbru, 2, ',', '.') + "</td>" + "<td class='taright'>" + $.number(item.imcomisn, 2, ',', '.') + "</td>"
                        + "<td class='taleft'>" + item.cdordtit + "</td>" + "</tr>");
            }

        }

        // exportar
        function SendAsExport(format) {
            var c = this.document.forms['facturasemitidas'];
            var f = this.document.forms["export"];
            console.log("Forms: " + f.name + "/" + c.name);
            f.action = "/sibbac20back/rest/export." + format;

            var json = {
                "service": "SIBBACServiceFactura",
                "action": "getListadoFacturas",
                "filters": {
                    "idAlias": getIdAlias(),
                    "fechaDesde": transformaFechaInv(angular.element('#fechaDesde').val()),
                    "fechaHasta": transformaFechaInv(angular.element('#fechaHasta').val()),
                    "idEstado": angular.element('#idEstado').val()
                }
            };
            if (json != null && json != undefined && json.length == 0) {
                alert("Introduzca datos");
            } else {
                f.webRequest.value = JSON.stringify(json);
                f.submit();
            }
        }

        // emitir duplicado
        $scope.duplicarFactura = function (id, nf) {

        	angular.element("#dialog-confirm").html("¿Desea emitir un duplicado de la factura " + nf + "?");


            // Define the Dialog and its properties.
            angular.element("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                title: "Mensaje de Confirmación",
                height: 150,
                width: 360,
                buttons: {
                    "Sí": function () {
                    	var filtro = {idFactura : id}
                        $(this).dialog('close');
                        FacturaService.enviarDuplicado(cargarTablaFacturas, showError, filtro);
                    },
                    "No": function () {
                        $(this).dialog('close');
                    }
                }
            });

        }

        $scope.cambioAlias = function () {
            $scope.numeroFactura = "";
        };

        $scope.cambioNumeroFactura = function () {
            angular.element('#textAlias').val("");
            angular.element('#fechaDesde').val("");
            angular.element('#fechaHasta').val("");

            var listaEstado = document.getElementById("idEstado");
            listaEstado.selectedIndex = 0;
        };

    }]);
