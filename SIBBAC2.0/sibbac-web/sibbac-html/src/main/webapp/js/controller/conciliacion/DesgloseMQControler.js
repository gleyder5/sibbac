'use strict';
sibbac20.controller('DesgloseMQControler', ['$scope', '$compile', 'growl', 'IsinService', '$document', 'DesgloseMQService', 'ngDialog', function ($scope, $compile, growl, IsinService, $document, DesgloseMQService, ngDialog) {

        var SHOW_LOG = true;

        var availableTags = [];
        var oTable = undefined;
        $scope.chisin = true;
        $scope.isines = [];
        $scope.listaChk = [];
        $scope.listaChkValidado = [];
        $scope.listaChkNumOperacion = [];
        $scope.filtroConsulta = {};
        $scope.filtroValidar = {};
        $scope.dialogTitle = "";
        $scope.needRefresh = false;
        $scope.filtroConsulta = {};
        $scope.altaDesglose = {};
        $scope.editarDesglose = {};
        $scope.listaSentido = [];
        $scope.listaTipoSaldo = [];
        $scope.listaTipoCambio = [];
        $scope.sentido = {};
        $scope.tipoSaldo = {};
        $scope.tipoCambio = {};
        $scope.listaBolsaMercado = {};

        var oDesgloses = [];
        var lMarcar = true;
        var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth() + 1;
        var yyyy = hoy.getFullYear();
        var openDesgloses;
        var paramsCrear = [];
        var ejecutadaCon = false;
        var alPaginas = [];
        var pagina = 0;
        var paginasTotales = 0;
        var ultimaPagina = 0;
        var desgloses = [];

        var fsService = server.service.conciliacion.desgloses;

        hoy = yyyy + "_" + mm + "_" + dd;

        function  processInfo(info) {

            pagina = info.page;
            paginasTotales = info.pages;
            var regInicio = info.start;
            var regFin = info.end;
            return regFin - regInicio;
        }

        var calcDataTableHeight = function (sY) {
            console.log("calcDataTableHeight: " + $('#tblDesglosesMQ').height());
            console.log("sY: " + sY);
            var numeroRegistros = processInfo(oTable.api().page.info());
            var tamanno = 50 * numeroRegistros;
            if ($('#tblDesglosesMQ').height() > 510) {
                return 480;
            } else {
                if ($('#tblDesglosesMQ').height() > tamanno)
                {
                    return $('#tblDesglosesMQ').height() + 30;
                } else
                {
                    if (tamanno > 510)
                    {
                        return 480;
                    } else
                    {
                        return tamanno;
                    }
                }
            }
        };

        function cargarIsines(datosIsin) {
            availableTags = [];
            angular.forEach(datosIsin, function (val, key) {
                var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                availableTags.push(ponerTag);
            });
            angular.element("#cdisin").autocomplete({
                source: availableTags
            });
            $scope.isines = datosIsin;
        }//function cargarIsines(datosIsin) {

        function showError(data, status, headers, config) {
            $.unblockUI();
            console.log("Error al inicializar datos: " + status);
        }

        function loadIsines() {
            IsinService.getIsines(cargarIsines, showError);
        }

        // Inicializa la página. Carga los combos y autocompletar del filtro.
        function initialize() {

            //var fechas = ["fechaDe", "fechaA"];
            //verificarFechas([["fechaDe", "fechaA"]]);
            $("#ui-datepicker-div").remove();
            $('input#fechaDe').datepicker({
                onClose: function( selectedDate ) {
                  $( "#fechaA" ).datepicker( "option", "minDate", selectedDate );
                } // onClose
              });

              $('input#fechaA').datepicker({
                onClose: function( selectedDate ) {
                  $( "#fechaDe" ).datepicker( "option", "maxDate", selectedDate );
                } // onClose
              });
            loadIsines();
            prepareCollapsion();
            var sentido = {id: 1, texto: 'Compra'};
            $scope.listaSentido.push(sentido);
            sentido = {id: 2, texto: 'Venta'};
            $scope.listaSentido.push(sentido);
            var tipoSaldo = {id: 'P', texto: 'Propio'};
            $scope.listaTipoSaldo.push(tipoSaldo);
            tipoSaldo = {id: 'T', texto: 'Terceros'};
            $scope.listaTipoSaldo.push(tipoSaldo);
            var tipoCambio = {id: 1, texto: 'Porcentaje'};
            $scope.listaTipoCambio.push(tipoCambio);
            tipoCambio = {id: 2, texto: 'Efectivo'};
            $scope.listaTipoCambio.push(tipoCambio);
        }

        /**
         * Se crea la tabla de forma normal
         * @returns {undefined}
         */
        function loadDataTableClientSidePagination() {
            oTable = $("#tblDesglosesMQ").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": ["/sibbac20/js/swf/copy_csv_xls_pdf.swf"],
                    "aButtons": ["copy",
                        {"sExtends": "csv",
                            "sFileName": "Listado_de_desgloses_" + hoy + ".csv",
                            "mColumns": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                        },
                        {
                            "sExtends": "xls",
                            "sFileName": "Listado_de_desgloses_" + hoy + ".csv",
//                      	 "sCharSet" : "utf8",
                            "mColumns": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                        },
                        {
                            "sExtends": "pdf",
                            "sPdfOrientation": "landscape",
                            "sTitle": " ",
                            "sPdfSize": "A4",
                            "sPdfMessage": "Listado de Listado_de_desgloses",
                            "sFileName": "Listado_de_desgloses_" + hoy + ".pdf",
                            "mColumns": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                        },
                        "print"
                    ]
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "aoColumns": [
                    {className: 'checkbox', targets: 0, sClass: "centrar", type: "checkbox", bSortable: false, name: "active", width: 50}, //0 Validar
                    {sClass: "centrar", bSortable: false, width: 125},        //1 Acciones
                    {sClass: "centrar", width: 100}, // 2 N. Orden.
                    {sClass: "centrar", width: 100}, // 3 Cod. SV
                    {sClass: "centrar", width: 50}, // 4 F. ejecución
                    {sClass: "centrar", width: 50}, // 5 Tipo operación
                    {sClass: "align-right", "sType": "numeric", width: 75}, // 6 Titulos
                    {sClass: "align-right", "sType": "numeric", width: 75}, // 7 Titulos
                    {sClass: "align-right", "sType": "numeric", width: 105}, // 8 Nominal
                    {sClass: "centrar", width: 100}, // 9 Isin
                    {sClass: "centrar", width: 250}, //10 descripcion
                    {sClass: "centrar", width: 75}, //11 Bolsa de negociación
                    {sClass: "centrar", width: 75}, //12 Tipo saldo
                    {sClass: "centrar", width: 75}, //13 Entidad liquidadora
                    {sClass: "centrar", width: 100}, //14 Tipo cambio
                    {sClass: "centrar", width: 75}, //15 Validado
                    {sClass: "centrar", width: 75}, //16 Procesado

                ],
                "fnCreatedRow": function (nRow, aData, iDataIndex) {

                    $compile(nRow)($scope);
                },
                "order": [],
                "scrollY": "480px",
                "scrollX": "100%",
                "scrollCollapse": true,
//	"rowHeight" : "44px",

                drawCallback: function () {
                    processInfo(this.api().page.info());
                    if (alPaginas.length !== paginasTotales) {
                        alPaginas = new Array(paginasTotales);
                        for (var i = 0; i < alPaginas.length; i++) {
                            alPaginas[i] = true;
                        }
                    }

                    if (ultimaPagina !== pagina) {

                        var valor = alPaginas[pagina];

                        if (valor) {
                            angular.element("#MarcarPag").val("Marcar Página");
                        } else {
                            angular.element("#MarcarPag").val("Desmarcar Página");
                        }

                    }
                    ultimaPagina = pagina;
                }
            });

            angular.element("#fechaDe, #fechaA").datepicker({
                dateFormat: 'dd/mm/yy',
                defaultDate: new Date()
            }); // datepick

        }//function loadDataTableClientSidePagination() {

        function cargarBolsaMercado(json) {
        	$scope.listaBolsaMercado = [];
            if (json.resultados != null && json.resultados !== undefined && json.resultados.bolsaMercadoList !== undefined) {
                var datos = json.resultados.bolsaMercadoList;

                var contador = 0;
                for (var k in datos) {
                    var item = datos[k];
                    var bolsaMercado = {
	                		codigo: item.codigo,
	                		descripcion : item.descripcion
                    };
                    $scope.listaBolsaMercado.push(bolsaMercado);
                }//for (var k in datos) {
            }//for (var k in datos) {
        };//cargarBolsaMercado

        function onSuccessBolsaMercado(data, status, header, config) {
            console.log("onSuccessBolsaMercado: " + data);
            cargarBolsaMercado(data);
        };

        function loadBolsaMercado (){
        	 console.log("listar bolsa Mercado: ");
        	 var filtroBolsaMercado = {};
             DesgloseMQService.getListaBolsaMercado(onSuccessBolsaMercado, onErrorRequest, filtroBolsaMercado);
        }

        $document.ready(function () {

            initialize();
            loadDataTableClientSidePagination();
            loadBolsaMercado();
        });

        $scope.LimpiarFiltros = function () {
        	 //ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fechaA" ).datepicker( "option", "minDate", "");
            $( "#fechaDe" ).datepicker( "option", "maxDate", "");

            //
            angular.element('input[type=text]').val('');
            angular.element('#fechaDe').val('');
            angular.element('#fechaA').val('');
            angular.element('#cdisin').val('');
            lMarcar = true;
            angular.element('#MarcarTod').val('Marcar Todos');
            $scope.chisin = false;
            $scope.btnInvCdIsin();

        };

        function lookBotones(tipo) {
            angular.element('#MarcarTod').css('display', 'none');
            angular.element('#MarcarPag').css('display', 'none');
            angular.element('#ValidarDesgloses').css('display', 'none');
            if (tipo) {
                if ($scope.listaChk.length > 0) {
                    angular.element('#MarcarTod').css('display', 'inline');
                    angular.element('#MarcarPag').css('display', 'inline');
                    angular.element('#ValidarDesgloses').css('display', 'inline');
                }//if($scope.listaChk.length>0){
            }//if(tipo)
        }//function lookBotones(tipo){

        //Botón de ejecutar la consulta.
        $scope.cargarTabla = function () {
            ejecutadaCon = true;

            cargarDatosConsulta();
            return false;
        };

        function getIsin(isinCompleto) {

            var guion = isinCompleto.indexOf("-");
            if (guion < 0) {
                return isinCompleto;
            } else {
                return isinCompleto.substring(0, guion);
            }
        }

        function onErrorRequest(data) {
            growl.addErrorMessage("Ocurrió un error de comunicación con el servidor, por favor inténtelo más tarde.");
            $.unblockUI();
        }

        //carga la tablita de los errores de un registro.
        function tablaDesgloses(Table, nTr, validado) {

            var aData = Table.fnGetData(nTr);
            var aObjDesglose = oDesgloses[nTr];
            var resultado =
                    "<div border-width:0 !important;background-color:#dfdfdf; style='float:left;;margin-left: 80px;' id='div_desglose_" + nTr + "'>" +
                    "<table id='tablaDesglose" + nTr + "' style=margin-top:5px;'>" +
                    "<tr>";
            if (validado < 2)
            {
                resultado += "<th class='taleft' style='background-color: #000 !important;'>Acciones</th>";
            }

            resultado +=
                    "<th class='taleft' style='background-color: #000 !important;'>Fecha</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>N. referencia</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Sentido</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Bolsa negociaci&oacute;n</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Isin</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Descripci&oacute;n</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Titulos</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Precio</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Tipo Saldo</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>CCV</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Titular</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Entidad liquidadora</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Tipo cambio</th>";

            resultado += "</tr>";

            for (var i in aObjDesglose) {
                var aDesglose = aObjDesglose[i];
                resultado += "<tr>";
                var id = "";
                var fecha = "";
                var fechaText = "";
                var numReferencia = "";
                var sentido = "";
                var sentidoText = "";
                var bolsa = "";
                var isin = "";
                var descripcion = "";
                var titulos = "";
                var precio = "";
                var tipoSaldo = "";
                var tipoSaldoText = "";
                var ccv = "";
                var titular = "";
                var entidad = "";
                var tipoCambio = "";
                var tipoCambioText = "";
                var numOrden = "";
                var bolsaDescripcion = "";
                for (var j in aDesglose) {
                    var desglose = aDesglose[j];
                    if (j == 'numOrden')
                    {
                    	numOrden = desglose;
                        continue;
                    } else
                    if (j == 'id')
                    {
                        id = desglose;
                        continue;
                    } else
                    if (j == 'fejecucion')
                    {
                        fecha = desglose;
                        if (desglose !== null && desglose !== undefined) {
                            desglose = desglose.substr(8, 2) + "/" + desglose.substr(5, 2) + "/" + desglose.substr(0, 4);
                        }
                        fechaText = desglose;
                    } else
                    if (j == 'numReferencia')
                    {
                        numReferencia = desglose;
                    } else
                    if (j == 'tipoOperacion')
                    {
                        var tipoOperacion = "Compra";
                        if (desglose == '2')
                        {
                            tipoOperacion = "Venta";
                        }
                        sentido = desglose;
                        sentidoText = tipoOperacion;
                    } else
                    if (j == 'bolsa')
                    {
                        bolsa = desglose;
                    } else
                    if (j == 'isin')
                    {
                        isin = desglose;
                    } else
                    if (j == 'desIsin')
                    {
                        descripcion = desglose;
                    } else
                    if (j == 'titulos')
                    {
                        titulos = desglose;
                    } else
                    if (j == 'precio')
                    {
                        precio = desglose;
                    } else
                    if (j == 'tipoSaldo')
                    {
                        var aux = "Propio";
                        if (desglose === 'T')
                        {
                            aux = "Terceros";
                        }
                        tipoSaldoText = aux;
                        tipoSaldo = desglose;
                    } else
                    if (j == 'ccv')
                    {
                        ccv = desglose;
                    } else
                    if (j == 'titular')
                    {
                        titular = desglose;
                    } else
                    if (j == 'entidad')
                    {
                        entidad = desglose;
                    } else
                    if (j == 'tipoCambio')
                    {
                        var aux = "Porcentaje";
                        if (desglose == '2')
                        {
                            aux = "Efectivo";
                        }
                        tipoCambioText = aux;
                        tipoCambio = desglose;
                    }else
                    if (j == 'bolsaDescripcion')
                    {
                    	bolsaDescripcion = desglose;
                    }
                }
                resultado += "<tr>";
                if (validado < 2)
                {
                    resultado += "<td  width=75>";

                    resultado += "<a class=\"btn\" ng-click = \"cargoInputs('" + id + "','" + fechaText + "','" + numReferencia + "','" + sentido + "','" + bolsa + "','" + isin + "','" + descripcion + "','" + titulos + "','" + precio + "','" + tipoSaldo + "','" + ccv + "','" + titular + "','" + entidad + "','" + tipoCambio + "', '"+numOrden+"' ); \"><img src='img/editp.png' title='Editar'/></a>&nbsp;<a class=\"btn\" ng-click=borrarDesglose('" + id + "','" + numReferencia + "');><img src='img/del.png'  title='Eliminar'/></a>";
                    resultado += "</td>";

                }
                resultado += "<td  width=75>" + fechaText + "</td>";
                resultado += "<td  width=75>" + numReferencia + "</td>";
                resultado += "<td  width=50>" + sentidoText + "</td>";
                resultado += "<td  width=100>" + bolsaDescripcion + "</td>";
                resultado += "<td  width=75>" + isin + "</td>";
                resultado += "<td  width=120>" + descripcion + "</td>";
                resultado += "<td  width=75>" + titulos + "</td>";
                resultado += "<td  width=75>" + precio + "</td>";
                resultado += "<td  width=75>" + tipoSaldoText + "</td>";
                resultado += "<td  width=120>" + ccv + "</td>";
                resultado += "<td  width=75>" + titular + "</td>";
                resultado += "<td  width=75>" + entidad + "</td>";
                resultado += "<td  width=75>" + tipoCambioText + "</td>";

                resultado += "</tr>";
            }
            resultado += "</table></div>";

            return resultado;
        }

        // Activa la sublinea.//
        $scope.desplegar = function (fila, validado) {
            if (oTable.fnIsOpen(fila)) {
                if (openDesgloses[fila]) {
                    oTable.fnClose(fila);
                    openDesgloses[fila] = false;
                }
            } else {
                var aData = oTable.fnGetData(fila);
                openDesgloses[fila] = true;
                var resultadoDesgloses = tablaDesgloses(oTable, fila, validado);
                var a = $compile(resultadoDesgloses)($scope);
                oTable.fnOpen(fila, a);
            }
        };

        function collapseSearchForm()
        {
            $('.collapser_search').parents('.title_section').next().slideToggle();
            $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
            $('.collapser_search').toggleClass('active');
            if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
                $('.collapser_search').text("Mostrar opciones de búsqueda");
            } else {
                $('.collapser_search').text("Ocultar opciones de búsqueda");
            }
        }

        function seguimientoBusqueda() {
            $scope.menBusDesgloseMQ = "";
            if ($scope.filtroConsulta.fechaDesde !== "") {
                $scope.menBusDesgloseMQ += " fecha desde: " + $scope.filtroConsulta.fechaDesde;
            }
            if ($scope.filtroConsulta.fechaHasta !== "") {
                $scope.menBusDesgloseMQ += " fecha hasta: " + $scope.filtroConsulta.fechaHasta;
            }
            if ($scope.filtroConsulta.isin !== "") {
            	if($scope.chisin)
            		$scope.menBusDesgloseMQ += " isin: " + $scope.filtroConsulta.isin;
            	else
            		$scope.menBusDesgloseMQ += " isin distinto : " + $scope.filtroConsulta.isin;
            }
        }

        function populateOld(json) {
            if (json.resultados != null && json.resultados !== undefined && json.resultados.desglosesList !== undefined) {

                var item = null;
                var datos = json.resultados.desglosesList;

                var tbl = $("#tblDesglosesMQ > tbody");
                $(tbl).html("");
                oTable.fnClearTable();

                var contador = 0;
                for (var k in datos) {
                    item = datos[k];

                    var estilo = 'style="display:none;"';
                    if (item.validado == 1)
                    {
                        estilo = 'style="display:inline;"';
                    }//if (data.validado == 1)
                    $scope.listaChkValidado[k] = item.validado;
                    $scope.listaChkNumOperacion[k] = item.numOrden;
                    var idName = "check_" + contador;
                    $scope.listaChk[k] = false;
                    var txtMarcar = '<input ng-model="listaChk[' + k + ']"  ' + estilo + ' type="checkbox" class=' + (contador) + '  id="' + idName + '" name="' + idName + '"bvalue="N" class="editor-active">';

                    var numOrden = "";
                    numOrden = "ng-click=\"desplegar(" + (contador) + "," + item.validado + ")\" ";
                    numOrden = numOrden + " style=\"cursor: pointer; color: #ff0000;\"";
                    numOrden = "<div class='taleft' " + numOrden + " >" + item.numOrden + "</div> "
                    oDesgloses.push(item.listaDesgloses);
                    contador++;

                    var classFejecucion = "error";
                    if (item.bokFechaEjecucion == true)
                    {
                        classFejecucion = "exito";
                    }
                    var fejecucion = item.fejecucion;
                    if (fejecucion !== null && fejecucion !== undefined) {
                        fejecucion = fejecucion.substr(8, 2) + "/" + fejecucion.substr(5, 2) + "/" + fejecucion.substr(0, 4);
                    }
                    var fejecucionText = "<div class='" + classFejecucion + "' id='fejecuion_" + contador + "' >" + fejecucion + "</div> ";

                    fejecucion = item.fejecucion;
                    fejecucion = fejecucion.substr(0, 4) + fejecucion.substr(5, 2) + fejecucion.substr(8, 2);

                    var tipoOperacion = "Compra";
                    var tOperacion = 2;
                    if (item.tipoOperacion == '2')
                    {
                        tipoOperacion = "Venta";
                    } else
                    {
                        tOperacion = 1;
                    }

                    var classTipoOperacion = "error";
                    if (item.bokTipoOperacion == true)
                    {
                        classTipoOperacion = "exito";
                    }
                    tipoOperacion = "<div class='" + classTipoOperacion + "' id='tipoOperacion_" + contador + "' >" + tipoOperacion + "</div> ";

                    var classTitulos = "error";
                    if (item.titulos === item.titulosSV)
                    {
                        classTitulos = "exito";
                    }
                    var titulos = "<div class='" + classTitulos + "' id='titulos_" + contador + "' >" + item.titulos + "</div> ";
                    var nominal = "<div class='taleft' id='nominal_" + contador + "' >" + item.nominal + "</div> ";

                    var classIsin = "error";

                    if (item.bokIsin == true)
                    {
                        classIsin = "exito";
                    }
                    var isin = "<div class='" + classIsin + "' id='isin_" + contador + "' >" + item.isin + "</div> ";

                    var classBolsa = "error";
                    if (item.bokBolsa == true)
                    {
                        classBolsa = "exito";
                    }
                    var bolsa = "<div class='" + classBolsa + "' id='bolsa_" + contador + "' >" + item.bolsaDescripcion + "</div> ";

                    var tipoSaldo = "Propio";
                    if (item.tipoSaldo === 'T')
                    {
                        tipoSaldo = "Terceros";
                    }

                    var classTipoSaldo = "error";
                    if (item.bokTipoSaldo == true)
                    {
                        classTipoSaldo = "exito";
                    }
                    tipoSaldo = "<div class='" + classTipoSaldo + "' id='tipoSaldo_" + contador + "' >" + tipoSaldo + "</div> ";

                    var tipoCambio = "Porcentaje";
                    if (item.tipoCambio == '2')
                    {
                        tipoCambio = "Efectivo";
                    }
                    var classTipoCambio = "error";
                    if (item.bokTipoCambio == true)
                    {
                        classTipoCambio = "exito";
                    }
                    tipoCambio = "<div class='" + classTipoCambio + "' id='tipoCambio_" + contador + "' >" + tipoCambio + "</div> ";

                    var validado = "OK";
                    if (item.validado == '0')
                    {
                        validado = "KO";
                    }
                    validado = "<div class='taleft' id=val_'" + contador + "' >" + validado + "</div> ";

                    var estilo = 'style=\"display:none;\"';
                    var acciones = "";
                    if (item.validado == '0')
                    {
                        estilo = 'style="display:inline;"';
                        acciones = "ng-click=\"cargoDesglose(" + (contador - 1) + ")\" ";
                        acciones = acciones + " style=\"cursor: pointer; color: #ff0000;\"";
                        acciones = "<div class='taleft' " + acciones + " ><img src=\"img/addParam.png\"  title=\"Alta desglose\" /></div> ";
                    }//if (data.validado === 1)
                    desgloses[contador - 1] = {
                        tipoReg: "40",
                        refre: '',
                        titulos: 0,
                        nominej: 0,
                        entidad: item.entidad,
                        empConVal: item.empConVal,
                        numCtoOficina: item.numCtoOficina,
                        numCtrVal: item.numCtrVal,
                        numOrden: item.numOrden,
                        codsv: item.codsv,
                        fejecucion: fejecucion,
                        bolsa: item.bolsa,
                        isin: item.isin,
                        tipoOperacion: tOperacion,
                        tipoCambio: item.tipoCambio,
                        tipoSaldo: item.tipoSaldo,
                        cambioOperacion: item.cambioOperacion,
                        denominacionValor: item.denominacionValor,
                        nif_bic: item.titular,
                        refMod: item.refMod,
                        segmento: item.segmento,
                        ordCom: item.ordCom,
                        bolsaDescripcion: item.bolsaDescripcion
                    }//datos de la operacion comunes a todos los desgloses.




                    var rowIndex = oTable.fnAddData([
                        txtMarcar,      // 0 lista de checks.
                        acciones,       // 1 acciones
                        numOrden,       // 2 numero de orden.
                        item.codsv,     // 3 Cod. SV.
                        fejecucionText, // 4 F ejecución.
                        tipoOperacion,  // 5 Tipo operación
                        titulos,        // 6 titulos
                        item.titulosSV, // 7 titulos
                        nominal,        // 8 nominal
                        isin,           // 9 isin
                        item.desIsin,   //10 descripcion isin
                        bolsa,          //11 bolsa de negociación
                        tipoSaldo,      //12 tipo de saldo
                        item.entidad,   //13 entidad liquidadora
                        tipoCambio,     //14 tipo de cambio
                        validado,       //15 validado
                        item.procesado  //16 procesado

                    ], false);

                    var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
                    $('td', nTr)[1].setAttribute('style', 'cursor: pointer; color: #ff0000;');
                    $('td', nTr)[2].setAttribute('style', 'cursor: pointer; color: #ff0000;');


                }
                oTable.fnDraw();

                lookBotones(true);
                $.unblockUI();
                collapseSearchForm();
                seguimientoBusqueda();
                openDesgloses = new Array(contador, false);

            } else {

                if (ejecutadaCon) {
                    var tipo = 2;
                    var errorTxt = json.error;
                    if (errorTxt == undefined) {
                        errorTxt = "Error indeterminado. Consulte con el servicio técnico.";
                        tipo = 1;
                    }
                    fErrorTxt(json.error, tipo);
                    $.unblockUI();
                    $('.collapser_search').parents('.title_section').next().slideToggle();
                    $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
                    $('.collapser_search').toggleClass('active');
                    if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
                        $('.collapser_search').text("Mostrar opciones de búsqueda");
                    } else {
                        $('.collapser_search').text("Ocultar opciones de búsqueda");
                    }
                }


            }

        }
        ;//populateOld

        function onSuccessSinPaginacion(data, status, header, config) {
            console.log("onSuccessSinPaginacion: " + data);
            populateOld(data);
        }
        ;

        function loadDataTableFromServer() {

            oTable = $("#tblDesglosesMQ").dataTable({
                "dom": 'B<"clear">lrtip', /*Para mostrar los controles requeridos del componente datatable*/
                "buttons": [
                    {
                        text: 'Excel',
                        action: function (e, dt, node, config) {

                            var data = {action: "getDesglosesMQExport", service: "SIBBACServiceConciliacionDesglose", filters: $scope.filtroConsulta};
                            console.log("data: " + data);
                            // Optional static additional parameters
                            // data.customParameter = ...;

                            if (config.fnData) {
                                config.fnData(data);
                            }

                            var iframe = $('<iframe/>', {
                                id: "RemotingIFrame"
                            }).css({
                                border: 'none',
                                width: 0,
                                height: 0
                            }).appendTo('body');

                            var contentWindow = iframe[0].contentWindow;
                            contentWindow.document.open();
                            contentWindow.document.close();
                            var form = contentWindow.document.createElement('form');
                            form.setAttribute('method', "POST");
                            form.setAttribute('action', "/sibbac20back/rest/export.xlsx");

                            var input = contentWindow.document.createElement('input');
                            input.name = 'webRequest';
                            input.value = JSON.stringify(data);
                            form.appendChild(input);
                            contentWindow.document.body.appendChild(form);
                            form.submit();

                        }
                    },
                    {
                        text: 'Pdf',
                        action: function (e, dt, node, config) {

                            var data = {action: "getDesglosesMQExport", service: "SIBBACServiceConciliacionDesglose", filters: $scope.filtroConsulta};
                            console.log("data: " + data);
                            // Optional static additional parameters
                            // data.customParameter = ...;

                            if (config.fnData) {
                                config.fnData(data);
                            }

                            var iframe = $('<iframe/>', {
                                id: "RemotingIFrame"
                            }).css({
                                border: 'none',
                                width: 0,
                                height: 0
                            }).appendTo('body');

                            var contentWindow = iframe[0].contentWindow;
                            contentWindow.document.open();
                            contentWindow.document.close();
                            var form = contentWindow.document.createElement('form');
                            form.setAttribute('method', "POST");
                            form.setAttribute('action', "/sibbac20back/rest/export.pdf");
                            var input = contentWindow.document.createElement('input');
                            input.name = 'webRequest';
                            input.value = JSON.stringify(data);
                            form.appendChild(input);
                            contentWindow.document.body.appendChild(form);
                            form.submit();

                        }
                    }
                ],
                "serverSide": true,
                "processing": true,
                "pageLength": datatable.pageLength,
                "ajax": $.fn.dataTable.pipeline({
                    "url": '/sibbac20back/rest/service/datatable',
                    "pages": datatable.pagesForCache, //Paginas que deja en caché por cada llamada AJAX
                    "method": "POST",
                    "dataType": "json",
                    "data": function (d) {
                        //return $.extend({}, d, {service: 'SIBBACServiceConciliacionDesglose', action: 'getDesglosesPageable', filters: $scope.filtro});
                        d.service = 'SIBBACServiceConciliacionDesglose';
                        d.action = 'getDesglosesPageable';
                        d.filters = $scope.filtroConsulta;
                    }
//             	,
//                 "dataSrc": function (json) {
//                     return populateTitulares(json);
//                 }
                }),
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "columns": [
                    {data: null, className: 'checkbox', targets: 0, sClass: "centrar", type: "checkbox", bSortable: false, name: "active", width: 50,
                        render: function (data, type, row, meta) {
                            var estilo = 'style="display:none;"';
                            if (row.validado == 1)
                            {
                                estilo = 'style="display:inline;"';
                            }//if (data.validado == 1)
                            $scope.listaChkValidado[meta.row + 1] = row.validado;
                            $scope.listaChkNumOperacion[meta.row + 1] = row.numOrden;
                            return '<input type="checkbox" ng-model="listaChk[' + (meta.row + 1) + ']" id="check_' + (meta.row + 1) + '" name="check_' + meta.row + '" ' + estilo + ' class="' + meta.row + '"  class="editor-active" bvalue="N" />';
                        }
                    }, // 0 lista de checks
                    {data: null, sClass: "centrar", bSortable: false, name: "active", width: 125,
                        render: function (data, type, row, meta) {
                            var estilo = 'style=\"display:none;\"';
                            var acciones = "";
                            if (row.validado == '0')
                            {
                                estilo = 'style="display:inline;"';
                                acciones = "ng-click=\"cargoDesglose(" + (contador - 1) + ")\" ";
                                acciones = acciones + " style=\"cursor: pointer; color: #ff0000;\"";
                                acciones = "<div class='taleft' " + acciones + " ><img src=\"img/addParam.png\"  title=\"Alta desglose\" /></div> ";
                            }//if (data.validado === 1)
                            var tOperacion = row.tipoOperacion;
                            if (tOperacion != 2)
                            {
                                tOperacion = 1;
                            }
                            desgloses[meta.row] = {
                                tipoReg: "40",
                                refre: '',
                                titulos: 0,
                                nominej: 0,
                                entidad: row.entidad,
                                empConVal: row.empConVal,
                                numCtoOficina: row.numCtoOficina,
                                numCtrVal: row.numCtrVal,
                                numOrden: row.numOrden,
                                codsv: row.codsv,
                                fejecucion: row.fejecucion,
                                bolsa: row.bolsa,
                                isin: row.isin,
                                tipoOperacion: tipoOperacion,
                                tipoCambio: row.tipoCambio,
                                cambioOperacion: row.cambioOperacion,
                                denominacionValor: row.denominacionValor,
                                nif_bic: row.nif_bic,
                                tipoSaldo: row.tipoSaldo,
                                refMod: row.refMod,
                                segmento: row.segmento,
                                ordCom: row.ordCom,
                                bolsaDescripcion: row.bolsaDescripcion
                            }//datos de la operacion comunes a todos los desgloses.

                            return acciones;
                        }
                    }, // 1 acciones
                    {data: null, sClass: "centrar nodetails", targets: 1, width: 100,
                        render: function (data, type, row, meta) {
                            oDesgloses.push(row.listaDesgloses);
                            var enlace = "";
                            enlace = "ng-click='desplegar(" + meta.row + "," + row.validado + ")' ";
                            enlace = enlace + " style='cursor: pointer; color: #ff0000;'";
                            return "<div class='taleft' " + enlace + " >" + row.numOrden + "</div> ";
                        }
                    }, // 2 numero de orden
                    {data: 'codsv', sClass: "centrar", width: 100}, // 3 Cod. SV.
                    {data: null, sClass: "centrar", width: 50,
                        render: function (data, type, row, meta) {
                            var classFejecucion = "error";
                            if (row.bokFechaEjecucion == true)
                            {
                                classFejecucion = "exito";
                            }
                            var fejecucion = item.fejecucion;
                            if (fejecucion !== null && fejecucion !== undefined) {
                                fejecucion = fejecucion.substr(8, 2) + "/" + fejecucion.substr(5, 2) + "/" + fejecucion.substr(0, 4);
                            }
                            return "<div class='" + classFejecucion + "' id='fejecuion_" + meta.row + 1 + "' >" + fejecucion + "</div> ";
                        }
                    }, // 4 F ejecución.
                    {data: null, sClass: "centrar", width: 50,
                        render: function (data, type, row, meta) {
                            var tipoOperacion = "Compra";
                            if (row.tipoOperacion == '2')
                            {
                                tipoOperacion = "Venta";
                            }
                            var classTipoOperacion = "error";
                            if (row.bokTipoOperacion == true)
                            {
                                classTipoOperacion = "exito";
                            }
                            return "<div class='" + classTipoOperacion + "' id='tipoOperacion_" + meta.row + 1 + "' >" + tipoOperacion + "</div> ";
                        }
                    }, // 5 Tipo operación
                    {data: null, sClass: "align-right", "sType": "numeric", width: 75,
                        render: function (data, type, row, meta) {

                            var classTitulos = "error";
                            if (row.titulos === row.titulosSV)
                            {
                                classTitulos = "exito";
                            }
                            return "<div class='" + classTitulos + "' id='titulos_" + meta.row + 1 + "' >" + row.titulos + "</div> ";
                        }
                    }, // 6 Titulos.
                    {data: 'titulosSV', sClass: "align-right", "sType": "numeric", width: 75}, // 7 Titulos.
                    {data: null, sClass: "align-right", "sType": "numeric", width: 105,
                        render: function (data, type, row, meta) {
                            return "<div class='align-right' id='nominal_" + meta.row + 1 + "' >" + row.nominal + "</div> ";
                        }
                    }, // 8 Nominal.
                    {data: null, sClass: "centrar", width: 100,
                        render: function (data, type, row, meta) {

                            var classIsin = "error";
                            if (row.bokIsin == true)
                            {
                                classIsin = "exito";
                            }
                            return "<div class='" + classIsin + "' id='isin_" + meta.row + 1 + "' >" + row.isin + "</div> ";
                        }
                    }, // 9 Isin.
                    {data: 'desIsin', sClass: "centrar", width: 250}, // 8 descripción isin.
                    {data: null, sClass: "centrar", width: 75,
                        render: function (data, type, row, meta) {

                            var classBolsa = "error";
                            if (row.bokBolsa == true)
                            {
                                classBolsa = "exito";
                            }
                            return "<div class='" + classBolsa + "' id='bolsa_" + meta.row + 1 + "' >" + row.bolsaDescripcion + "</div> ";
                        }
                    }, //10 Bolsa de negociación.
                    {data: null, sClass: "centrar", width: 50,
                        render: function (data, type, row, meta) {
                            var tipoSaldo = "Propio";
                            if (row.tipoSaldo === 'T')
                            {
                                tipoSaldo = "Terceros";
                            }

                            var classTipoSaldo = "error";
                            if (row.bokTipoSaldo == true)
                            {
                                classTipoSaldo = "exito";
                            }
                            return "<div class='" + classTipoSaldo + "' id='tipoSaldo_" + meta.row + 1 + "' >" + tipoSaldo + "</div> ";
                        }
                    }, //11 Tipo saldo
                    {data: 'entidad', sClass: "centrar", width: 75}, //12 Entidad liquidadora
                    {data: null, sClass: "centrar", width: 50,
                        render: function (data, type, row, meta) {
                            var tipoCambio = "Porcentaje";
                            if (row.tipoCambio == '2')
                            {
                                tipoCambio = "Efectivo";
                            }
                            var classTipoCambio = "error";
                            if (row.bokTipoCambio == true)
                            {
                                classTipoCambio = "exito";
                            }
                            return "<div class='" + classTipoCambio + "' id='tipoCambio_" + meta.row + 1 + "' >" + tipoCambio + "</div> ";
                        }
                    }, //13 Tipo Cambio
                    {data: null, sClass: "centrar", width: 75,
                        render: function (data, type, row, meta) {
                            var validado = "OK";
                            if (row.validado == '0')
                            {
                                validado = "KO";
                            }
                            return "<div class='taleft' id=val_'" + meta.row + 1 + "' >" + validado + "</div> ";
                        }
                    }, //14 Validado
                    {data: 'procesado', sClass: "centrar", width: 75} //15 Procesado
                ],
                "createdRow": function (nRow, aData, iDataIndex) {
                    $compile(nRow)($scope);
                },
                "order": [],
                "scrollY": "480px",
                "scrollX": "100%",
                "scrollCollapse": true,
// 	"rowHeight" : "44px",

                drawCallback: function () {
                    console.log("drawCallback");
                    processInfo(this.api().page.info());
                    if (alPaginas.length !== paginasTotales) {
                        alPaginas = new Array(paginasTotales);
                        for (var i = 0; i < alPaginas.length; i++) {
                            alPaginas[i] = true;
                        }
                    }

                    if (ultimaPagina !== pagina) {

                        var valor = alPaginas[pagina]

                        if (valor) {
                            document.getElementById("MarcarPag").value = "Marcar Página";
                        } else {
                            document.getElementById("MarcarPag").value = "Desmarcar Página";
                        }

                    }
                    ultimaPagina = pagina;

                    var numFilas = this.api().rows().nodes().length;
                    openDesgloses = new Array(numFilas, false);
                    lookBotones(true);
                    $.unblockUI();

                }
            });



            angular.element("#fechaDe, #fechaA").datepicker({
                dateFormat: 'DD/MM/YYYY',
                defaultDate: new Date(),
            }); // datepick
        }
        ;

        function onSuccessCountRequest(data, status, header, config) {
            console.log("Número de registros: " + data.resultados.registros);
            if (data.error !== undefined && data.error !== null && data.error !== "") {
                growl.addErrorMessage(data.error);
            } else {
                oDesgloses = [];
                $scope.listaChk = [];
                $scope.listaChkValidado = [];
                $scope.listaChkNumOperacion = [];
                collapseSearchForm();
                seguimientoBusqueda();
                desgloses = [];
                if (oTable !== undefined && oTable !== null) {
                    oTable.fnDestroy();
                }
                if (data.resultados.registros > (datatable.pageLength * datatable.pagesForCache)) {
                    growl.addWarnMessage("¡Atención!. El número de registros en la base de datos, es mayor que el permitido, por tanto se deshabilitará el campo de búsqueda sobre la tabla.");
                    loadDataTableFromServer();
                } else {
                    loadDataTableClientSidePagination();
                    DesgloseMQService.getDesglosesSinPaginacion(onSuccessSinPaginacion, onErrorRequest, $scope.filtroConsulta);
                }

            }

        }
        ;

        function recargarDatosPantalla()
        {
            inicializarLoading();

            DesgloseMQService.getDesglosesCount(onSuccessCountRequest, onErrorRequest, $scope.filtroConsulta);
        }
        ;

        // Funcion en la que captura el filtro y muestra todos las operaciones que cumplan las condiciones
        function cargarDatosConsulta() {

            console.log("cargarDatosConsulta");

            var fechaDe = transformaFechaInv(angular.element("#fechaDe").val());
            var fechaA = transformaFechaInv($("#fechaA").val());
            var isinCompleto = angular.element('#cdisin').val();
            var cdIsin = ((!$scope.chisin && $('#cdisin').val() !== "" ? "#" : "") ) + getIsin(isinCompleto);

            $scope.filtroConsulta = {
                "fechaDesde": fechaDe,
                "fechaHasta": fechaA,
                "isin": cdIsin
            };

            recargarDatosPantalla();

        }
        ;

        //marca/desmarca los registros de la pagina
        function toggleCheckBoxesPag(b) {
            var inputs = angular.element('input[type=checkbox]');
            var cb = null;
            for (var i = 0; i < inputs.length; i++) {
                cb = inputs[i];
                var clase = $(cb).attr('class');
                var n = clase.indexOf(" ");
                var fila = clase.substr(0, n);
                var validado = $scope.listaChkValidado[fila];
                if (validado == 1)
                {
                    $scope.listaChk[fila] = b;
                }
            }

        }
        ;

        // marca/desmarca todos los registros
        function toggleCheckBoxes(b) {

            for (var i = 0; i < $scope.listaChk.length; i++) {
                var validado = $scope.listaChkValidado[i];
                if (validado == 1)
                {
                    $scope.listaChk[i] = b;
                }
            }
        }
        ;

        // Boton de marcar todos
        $scope.selececionarCheck = function () {
            if (lMarcar) {
                lMarcar = false;
                angular.element("#MarcarTod").val("Desmarcar Todos");
                toggleCheckBoxes(true);
            } else {
                lMarcar = true;
                angular.element("#MarcarTod").val("Marcar Todos");
                toggleCheckBoxes(false);
            }
        };

        //Boton de marcar página
        $scope.selececionarCheckPag = function () {

            var valor = alPaginas[pagina];

            toggleCheckBoxesPag(valor);
            if (valor) {
                angular.element("#MarcarPag").val("Desmarcar Página");
                alPaginas[pagina] = false;
            } else {
                angular.element("#MarcarPag").val("Marcar Página");
                alPaginas[pagina] = true;
            }
        };

        function onSuccessValidado(json, status, headers, config) {
            if (SHOW_LOG)
                console.log("validados las operaciones seleccionadas ...");
            var datos = {};
            if (json !== undefined &&
                    json.resultados !== undefined &&
                    json.resultados !== null) {
                if (json.error !== null) {
                    growl.addErrorMessage(json.error);
                } else {
                    recargarDatosPantalla();
                }
            } else
            {
                if (json !== undefined && json.error !== null) {
                    if (SHOW_LOG)
                        console.log("error al validar las operaciones ...");
                    growl.addErrorMessage(json.error);
                }
            }
        }
        ;

        //validar desgloses
        $scope.validarDesgloses = function () {

            console.log("*******************");
            console.log("validar desgloses");
            console.log("*******************");

            $scope.listaValidar = [];

            var aData = oTable.fnGetData();
            var contador = 0;
            var input;
            var idName;
            var inputSel;
            for (var i = 0, j = aData.length; i < j; i++) {
                if ($scope.listaChk[i] == true) {
                    $scope.listaValidar.push($scope.listaChkNumOperacion[i]);
                }
            }

            if ($scope.listaValidar.length == 0) {
            	growl.addErrorMessage("Debe seleccionar al menos uno de las operaciones.");
                return;
            }
            lookBotones(false);
            var filtroValidar = JSON.stringify($scope.listaValidar);

            var data = "{\"service\" :\"" + fsService + "\", \"action\"  :\"marcarValidado\"";
            data += ", \"list\" :" + filtroValidar;
            data += "}";

            JSON.stringify(data);
            DesgloseMQService.marcarValidado(onSuccessValidado, onErrorRequest, data);

        };//validarDesgloses = function () {

        function openAltaForm(desglose) {
            $scope.dialogTitle = 'Alta Desglose orden ' + desglose.numOrden;
            ngDialog.open({
                template: 'template/conciliacion/desgloses/altaDesglose.html',
                className: 'ngdialog-theme-plain custom-width custom-height-360',
                scope: $scope,
                preCloseCallback: function () {
                    if ($scope.needRefresh) {
                        $scope.needRefresh = false;
                        recargarDatosPantalla();
                    }
                }
            });
        }
        ;

        //alta desgloses
        $scope.cargoDesglose = function (indice) {

            console.log("*******************");
            console.log("cargar desglose");
            console.log("*******************");

            var desglose = desgloses[indice];

            $scope.dialogTitle = 'Alta Parametrización';
            $scope.altaDesglose = desglose;
            openAltaForm(desglose);

        };//$scope.cargoDesglose = function (indice) {

        function successAltaDesglose(json, status, headers, config) {
            if (SHOW_LOG)
                console.log("desglose creado...");
            var datos = {};
            if (json !== undefined &&
                    json.resultados !== undefined &&
                    json.resultados !== null) {
                if (json.error !== null) {
                    growl.addErrorMessage(json.error);
                } else {
                    $scope.needRefresh = true;
                    ngDialog.closeAll();
                }
            } else
            {
                if (json !== undefined && json.error !== null) {
                    if (SHOW_LOG)
                        console.log("Error desglose creado ...");
                    growl.addErrorMessage(json.error);
                }
            }
        }
        ;

        function successEditarDesglose(json, status, headers, config) {
            if (SHOW_LOG)
                console.log("desglose editado...");
            var datos = {};
            if (json !== undefined &&
                    json.resultados !== undefined &&
                    json.resultados !== null) {
                if (json.error !== null) {
                    growl.addErrorMessage(json.error);
                } else {
                    $scope.needRefresh = true;
                    ngDialog.closeAll();
                }
            } else
            {
                if (json !== undefined && json.error !== null) {
                    if (SHOW_LOG)
                        console.log("Error editando desglose ...");
                    growl.addErrorMessage(json.error);
                }
            }
        }
        ;

        function successBorrarDesglose(json, status, headers, config) {
            if (SHOW_LOG)
                console.log("desglose borrado ...");
            var datos = {};
            if (json !== undefined &&
                    json.resultados !== undefined &&
                    json.resultados !== null) {
                if (json.error !== null) {
                    growl.addErrorMessage(json.error);
                } else {
                    recargarDatosPantalla();
                }
            } else
            {
                if (json !== undefined && json.error !== null) {
                    if (SHOW_LOG)
                        console.log("Error borrando desglose ...");
                    growl.addErrorMessage(json.error);
                }
            }
        }
        ;

        function crearDesglose()
        {
            var filtro = {tipoReg: $scope.altaDesglose.tipoReg,
                refre: $scope.altaDesglose.refre,
                titulos: $scope.altaDesglose.titulos,
                nominej: $scope.altaDesglose.nominej,
                entidad: $scope.altaDesglose.entidad,
                empConVal: $scope.altaDesglose.empConVal,
                numCtoOficina: $scope.altaDesglose.numCtoOficina,
                numCtrVal: $scope.altaDesglose.numCtrVal,
                numOrden: $scope.altaDesglose.numOrden,
                codsv: $scope.altaDesglose.codsv,
                fejecucion: $scope.altaDesglose.fejecucion,
                bolsa: $scope.altaDesglose.bolsa,
                isin: $scope.altaDesglose.isin,
                tipoOperacion: $scope.altaDesglose.tipoOperacion,
                cambioOperacion: $scope.altaDesglose.cambioOperacion,
                nif_bic: $scope.altaDesglose.nif_bic,
                tipoSaldo: $scope.altaDesglose.tipoSaldo,
                refMod: $scope.altaDesglose.refMod,
                segmento: $scope.altaDesglose.segmento,
                ordCom: $scope.altaDesglose.ordCom,
                tipoCambio: $scope.altaDesglose.tipoCambio};

            DesgloseMQService.altaDesglose(successAltaDesglose, showError, filtro);
        }
        ;

        $scope.alta_Desglose = function () {

            $scope.altaDesglose.refre = angular.element("#refre").val();
            $scope.altaDesglose.titulos = angular.element("#titulos").val();
            $scope.altaDesglose.nominej = angular.element("#nominej").val();
            $scope.altaDesglose.nif_bic = angular.element("#titular").val();
            $scope.altaDesglose.refMod = angular.element("#refMod").val();
            var efectivoPattern = new RegExp(/^\d{1,13}\b.?\d{0,2}?$/);
            var titulosPattern = new RegExp(/^\d{1,11}\b.?\d{0,0}?$/);

            var titulos = parseFloat($scope.altaDesglose.titulos);
            var efectivo = parseFloat($scope.altaDesglose.nominej);

            if (!titulosPattern.test($scope.altaDesglose.titulos))
            {
                growl.addErrorMessage("los titulos no tienen el formato correcto");
            } else if (!efectivoPattern.test($scope.altaDesglose.nominej))
            {
                growl.addErrorMessage("el efectivo no tiene el formato correcto");
            } else
            {
                $scope.altaDesglose.cambioOperacion = $scope.altaDesglose.nominej / $scope.altaDesglose.titulos;
                crearDesglose();
            }

        };

        function editarDesglose()
        {
            var fechaEjecucion = transformaFechaInv($scope.editarDesglose.fecha);
            var filtro = {
                id: $scope.editarDesglose.id,
                refre: $scope.editarDesglose.numReferencia,
                fejecucion: fechaEjecucion,
                tipoOperacion: $scope.editarDesglose.sentido,
                bolsa: $scope.editarDesglose.bolsa,
                isin: $scope.editarDesglose.isin,
                titulos: $scope.editarDesglose.titulos,
                nominej: $scope.editarDesglose.nominej,
                cambioOperacion: $scope.editarDesglose.precio,
                tipoSaldo: $scope.editarDesglose.tipoSaldo,
                ccv: $scope.editarDesglose.ccv,
                nif_bic: $scope.editarDesglose.titular,
                entidad: $scope.editarDesglose.entidad,
                tipoCambio: $scope.editarDesglose.tipoCambio
            };

            DesgloseMQService.editarDesglose(successEditarDesglose, showError, filtro);
        }
        ;

        $scope.editar_Desglose = function () {

            $scope.editarDesglose.fecha = angular.element("#fecha").val();
            $scope.editarDesglose.bolsa = angular.element("#bolsa").val();
            var isinCompleto = $scope.editarDesglose.isin;
            $scope.editarDesglose.isin = getIsin(isinCompleto);
            $scope.editarDesglose.titulos = angular.element("#titulos").val();
            $scope.editarDesglose.precio = angular.element("#precio").val();
            $scope.editarDesglose.ccv = angular.element("#ccv").val();
            $scope.editarDesglose.titular = angular.element("#titular").val();
            $scope.editarDesglose.entidad = angular.element("#entidad").val();

            var precioPattern = new RegExp(/^\d{1,6}\b.?\d{0,4}?$/);
            var titulosPattern = new RegExp(/^\d{1,11}\b.?\d{0,0}?$/);

            var titulos = parseFloat($scope.editarDesglose.titulos);
            var precio = parseFloat($scope.editarDesglose.precio);
            var efectivo = titulos * precio;

            if (!titulosPattern.test($scope.editarDesglose.titulos))
            {
                growl.addErrorMessage("los titulos no tienen el formato correcto");
            } else if (!precioPattern.test($scope.editarDesglose.precio))
            {
                growl.addErrorMessage("el precio no tiene el formato correcto");
            } else
            {
                $scope.editarDesglose.nominej = efectivo;
                editarDesglose();
            }

        };

        function openEditForm() {

            ngDialog.open({
                template: 'template/conciliacion/desgloses/editarDesglose.html',
                className: 'ngdialog-theme-plain custom-width custom-height-400',
                scope: $scope,
                preCloseCallback: function () {
                    if ($scope.needRefresh) {
                        $scope.needRefresh = false;
                        recargarDatosPantalla();
                    }
                }, controller: ['$scope', '$document', 'growl', function ($scope, $document, growl) {
                        $document.ready(function () {
                            //
                        });
                    }]
            });
        }
        ;

        //editar desgloses
        $scope.cargoInputs = function (id, fecha, numReferencia, sentido, bolsa, isin, descripcion, titulos, precio, tipoSaldo, ccv, titular, entidad, tipoCambio, numOrden) {

            console.log("*******************");
            console.log(" cargoInputs ");
            console.log("*******************");

            if (sentido == '1')
            {
                sentido = 1;
            } else
            {
                sentido = 2;
            }

            if (tipoSaldo == 'P')
            {
                $scope.tipoSaldo = $scope.listaTipoSaldo[0];
            } else
            {
                $scope.tipoSaldo = $scope.listaTipoSaldo[1];
            }

            if (tipoCambio == '1')
            {
                tipoCambio = 1;
            } else
            {
                tipoCambio = 2;
            }

            $scope.dialogTitle = 'Editar desglose de la orden: '+numOrden;
            $scope.editarDesglose = {};
            $scope.editarDesglose = {id: id,
                fecha: fecha,
                numReferencia: numReferencia,
                sentido: sentido,
                bolsa: bolsa,
                isin: isin,
                descripcion: descripcion,
                titulos: titulos,
                precio: precio,
                tipoSaldo: tipoSaldo,
                ccv: ccv,
                titular: titular,
                entidad: entidad,
                tipoCambio: tipoCambio};
            openEditForm();

        }//$scope.cargoInputs = function () {

        $scope.borrarDesglose = function (id, numReferencia)
        {

            var filtro = {id: id};

            angular.element("#dialog-confirm").html("¿Desea borrar realmente el desglose con la referencia: " + numReferencia + "?");

            // Define the Dialog and its properties.
            angular.element("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                title: "Mensaje de Confirmación",
                height: 120,
                width: 540,
                buttons: {
                    "Sí": function () {
                        $(this).dialog('close');
                        DesgloseMQService.borrarDesglose(successBorrarDesglose, showError, filtro);
                    },
                    "No": function () {
                        $(this).dialog('close');
                    }
                }
            });

        };
        $scope.selectedIsin = function (selected) {
            console.log("selectedIsin");
            console.log("selected.originalObject:  "+selected.originalObject);
            if (selected !== undefined && selected.originalObject !== undefined) {
                console.log("selectedIsin: "+selected.originalObject.codigo);
                $scope.editarDesglose.isin = selected.originalObject.codigo;
            }
        };

    	$scope.btnInvCdIsin = function() {

          	  if($scope.chisin){
      		  	  $scope.chisin = false;

        		  document.getElementById('textBtnInvCdisin').innerHTML = "<>";
        		  angular.element('#textInvCdisin').css({"background-color": "transparent", "color": "red"});

        		  document.getElementById('textInvCdisin').innerHTML = "DISTINTO";

          	  }else{
          		  $scope.chisin = true;
            	  document.getElementById('textBtnInvCdisin').innerHTML = "=";
            	  angular.element('#textInvCdisin').css({"background-color": "transparent", "color": "green"});
       			  document.getElementById('textInvCdisin').innerHTML = "IGUAL";

            }
    	}

    }]);
