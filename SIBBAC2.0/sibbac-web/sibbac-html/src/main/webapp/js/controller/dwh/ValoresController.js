(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var ValoresController = function (service, httpParamSerializer, uiGridConstants, $scope, $filter) {
		var ctrl = this;
		// Sub controlador DynamicFiltersController
		GENERIC_CONTROLLERS.DynamicFiltersController.call(ctrl, service, httpParamSerializer, uiGridConstants);
		// Opciones del modal (480px ancho)
		var wWith = $(window).width();
		ctrl.filter = {};
		ctrl.listadoInstrumento = [];
		ctrl.listadoSibe = [];
		ctrl.listadoMonedas = [];
		ctrl.listadoPaises = [];
		ctrl.result = {};
		ctrl.result.errorMessage = [];
		ctrl.formIds = [];
		ctrl.scope = $scope;
		ctrl.saveFlag = true;
		var fatDialogOpt = {
			width: wWith * 0.62,
			top: wWith * 0.2,
			left: wWith * 0.2
		};


		ctrl.getIds = function (elements) {
			for (var i = 0; i < elements.length; i++) {
				ctrl.formIds.push(elements[i].id);
			};
		}

		var formNextElement = $("#formNext");
		var formPrevElement = $("#formPrev");

		ctrl.getIds(formNextElement.find("input"));
		ctrl.getIds(formNextElement.find("select"));
		ctrl.getIds(formPrevElement.find("input"));

		this.dialogs = ["#modalNext", "#modalPrevious"];

		this.dialogs.forEach(dialog => {
			var aux = $('[role=dialog]').find(dialog);
			(aux.length > 0) ? aux.dialog('destroy').remove() : undefined;
		});

		// Llamadas a back para completar los combos
		ctrl.fillCombos();

		// Resgistramos los modales por la id, y le asignamos una función para el boton con el nombre asignado (Guardar, Modificar, etc)
		ctrl.nextModal = ctrl.registryModal("modalPrevious", "Crear", {
			"< Anterior": function () { },
			Aceptar: function () {
				ctrl.saveFlag ? ctrl.save() : ctrl.edit();
			},
			Cancelar: function () {
				$(this).dialog("close");
			},
			"Siguiente >": function () {
				ctrl.controlBtn(true, '< Anterior', 'Siguiente >');
				$(this).dialog("close");
				ctrl.previousModal.dialog("open");
			}
		}, fatDialogOpt);
		ctrl.previousModal = ctrl.registryModal("modalNext", "Crear", {
			"< Anterior": function () {
				ctrl.controlBtn(false, 'Siguiente >', '< Anterior');
				$(this).dialog("close");
				ctrl.nextModal.dialog("open");
			},
			Aceptar: function () {
				ctrl.saveFlag ? ctrl.save() : ctrl.edit();
			},
			Cancelar: function () {
				$(this).dialog("close");
			},
			"Siguiente >": function () { }
		}, fatDialogOpt);

		$('[role=dialog]').has("#modalPrevious").find("button:contains('Cerrar')").remove();
		$('[role=dialog]').has("#modalNext").find("button:contains('Cerrar')").remove();

		ctrl.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					ctrl.resultDialog.dialog("close");
					if (ctrl.result.ok) {
						ctrl.executeQuery();
					}

				}
			}
		});
		// Ejecutamos la función selectQuery y executeQuery cuando obtengamos la query del back
		ctrl.queries.$promise.then(data => {
			ctrl.selectedQuery = data[0];
			ctrl.selectQuery();
			ctrl.executeQuery();
		});

		ctrl.calcularBanco = function () {
			ctrl.params.CDCVABEV = (ctrl.params.CDISINVV && ctrl.params.CDISINVV.length >= 4) ? ctrl.params.CDISINVV.substring(3, 11) : undefined
		}

		ctrl.controlBtn = function (modalnext, activeButton, desactiveButton) {
			var modalDesactive = modalnext ? "#modalNext" : "#modalPrevious";
			var modalActive = !modalnext ? "#modalPrevious" : "#modalNext";
			$('[role=dialog]').has(modalDesactive).find("button:contains(" + desactiveButton + ")").removeClass("mybutton");
			$('[role=dialog]').has(modalDesactive).find("button:contains(" + desactiveButton + ")").addClass("modalDisabledBtn");
			$('[role=dialog]').has(modalDesactive).find("button:contains(" + desactiveButton + ")").prop('disabled', true);
			$('[role=dialog]').has(modalDesactive).find("button:contains(" + desactiveButton + ")").css('cursor', 'not-allowed');
			$('[role=dialog]').has(modalActive).find("button:contains(" + activeButton + ")").removeClass("modalDisabledBtn");
			$('[role=dialog]').has(modalActive).find("button:contains(" + activeButton + ")").addClass("mybutton");
			$('[role=dialog]').has(modalActive).find("button:contains(" + activeButton + ")").prop('disabled', false);
			$('[role=dialog]').has(modalActive).find("button:contains(" + activeButton + ")").css('cursor', 'pointer');
		}

		ctrl.refactorParams = function () {
			var aux = {};
			Object.keys(ctrl.params).forEach(function (key) {
				aux[key.toLowerCase()] = ctrl.params[key];
			});
			aux["fhbajacv"] = ctrl.filterDate(aux["fhbajacv"]);
			aux["fhiniciv"] = ctrl.filterDate(aux["fhiniciv"]);
			return aux;
		}

		// Devuelve las fechas con el siguiente formato[dd/MM/yyyy]
		ctrl.filterDate = function (date) {
			return $filter('date')(date, 'dd/MM/yyyy');
		}

		// Metodo que pinta en un array los valores que hallan fallado en las validaciones
		ctrl.fillErrMsg = function (key, validity) {
			var msgs = ctrl.result.errorMessage;

			if (!validity.valid) {
				validity.valueMissing ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es obligatorio") : '';
				validity.patternMismatch ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es no respeta el formato") : '';
				validity.tooShort ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es demasiado corto") : '';
				validity.tooLong ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es demasiado largo") : '';
			}
		}

		// Abrimos el formulario que muestra los errores del formulario
		ctrl.notValidForm = function () {
			ctrl.result.ok = false;

			ctrl.resultDialog.dialog("option", "title", "Ejecución con errores");
			ctrl.resultDialog.dialog("open");
			ctrl.scope.$applyAsync();
		}

		// Modificador del datepicker pasado por parámetros dejando el año a modificar directamente y dandole rango.
		ctrl.datepickerModified = function (id) {
			$(id).datepicker({
				changeYear: true,
				yearRange: "1900:9999",
				autoSize: true
			});
		}

		// Método que devuelve comprueba los errores que se han dado en las validaciones y en el que se llena una array con los valores erroneos que hallan salidos
		ctrl.isFormValid = function () {
			ctrl.result= {};
			ctrl.result.errorMessage = [];
			ctrl.formIds.forEach(function (key) {
				var element = document.getElementById(key);
				var validity = element.validity;
				if (element.type != "select-one") {
					if (element.readOnly) {
						element.readOnly = false;
						ctrl.fillErrMsg(key, validity)
						element.readOnly = true;
					} else {
						ctrl.fillErrMsg(key, validity)
					}
				} else {
					(element.required && element.value == "") ? ctrl.result.errorMessage.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es obligatorio") : '';
				}
			});
			return ctrl.result.errorMessage == 0;
		};

		ctrl.controlBtn(true, 'Siguiente >', '< Anterior');

		ctrl.datepickerModified('#FHINICIV');
		ctrl.datepickerModified('#FHBAJACV');
	};


	ValoresController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

	ValoresController.prototype.constructor = ValoresController;

	ValoresController.prototype.fillCombos = function () {
		var ctrl = this;
		inicializarLoading();
		ctrl.when(
			function (done) {
				ctrl.service.executeGetValoresGrupo(function (result) {
					result.forEach(function (data) {
						ctrl.listadoSibe.push({ value: data.cdSibe, label: data.cdSibe + " - " + data.descSibe });
					})
					done();
				}, ctrl.postFail.bind(ctrl));
			},
			function (done) {
				ctrl.service.executeGetGruposValores(function (result) {
					result.forEach(function (data) {
						ctrl.listadoInstrumento.push({ value: data.id, label: data.id + " - " + data.nbDescripcion });
					})
					done();
				}, ctrl.postFail.bind(ctrl));
			},
			function (done) {
				ctrl.service.executeGetMonedas(function (result) {
					result.forEach(function (data) {
						ctrl.listadoMonedas.push({ value: data.cdmoneda, label: data.cdmoneda + " - " + data.nbmoneda });
					})
				}, ctrl.postFail.bind(ctrl));
				done();
			},
			function (done) {
				ctrl.service.executeGetPaises(function (result) {
					result.forEach(function (data) {
						ctrl.listadoPaises.push({ value: data.cdisonum.trim(), label: data.cdisonum + " - " + data.nbpais });
					})
				}, ctrl.postFail.bind(ctrl));
				done();
			}
		).then(function () {
			$.unblockUI();
		})
	}

	ValoresController.prototype.when = function () {
		var args = arguments;  // the functions to execute first
		return {
			then: function (done) {
				var counter = 0;
				for (var i = 0; i < args.length; i++) {
					// call each function with a function to call on done
					args[i](function () {
						counter++;
						if (counter === args.length) {  // all functions have notified they're done
							done();
						}
					});
				}
			}
		};
	};

	// Función que se encarga de levantar los modales con una id que identifica su tipo 
	ValoresController.prototype.openModal = function (id) {
		var ctrl = this;
		ctrl.controlBtn(false, 'Siguiente >', '< Anterior');
		ctrl.removeBtnsStyle("modalPrevious");
		ctrl.removeBtnsStyle("modalNext"); 
		switch (id) {
			case "save":
				ctrl.saveFlag = true;
				ctrl.params = {};
				ctrl.params.FHBAJACV = "31/12/9999";
				ctrl.params.NUDELOTE = 0;
				ctrl.params.CDCOTMAV = 'N';
				ctrl.params.CDCOTBAV = 'N';
				ctrl.params.CDCOTBIV = 'N';
				ctrl.params.CDCOTVAV = 'N';
				ctrl.params.CDCOTVRV = 'N';
				ctrl.params.CDCOTEXV = 'N';

				ctrl.nextModal.dialog("option", "title", "Crear");
				ctrl.nextModal.dialog("open");

				break;
			case "edit":
				ctrl.saveFlag = false;
				if (ctrl.selectedRows().length > 0) {
					// Igualamos los valores a los parámetros que recibiremos en el modal
					ctrl.params = ctrl.selectedRows()[0];

					ctrl.params.FHALTAV = ctrl.filterDate(ctrl.params.FHALTAV);
					ctrl.params.FHBAJACV = ctrl.filterDate(ctrl.params.FHBAJACV);
					ctrl.params.FHINICIV = ctrl.filterDate(ctrl.params.FHINICIV);
					ctrl.params.FHMODIV = ctrl.filterDate(ctrl.params.FHMODIV);
					ctrl.params.FHULTMOD = ctrl.filterDate(ctrl.params.FHULTMOD);

					ctrl.nextModal.dialog("option", "title", "Modificar");
					ctrl.nextModal.dialog("open");
				}

				break;
		}
	};

	ValoresController.prototype.save = function () {
		var ctrl = this;
		if (ctrl.isFormValid()) {
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [ctrl.params]
			ctrl.service.executeActionPOST(ctrl.refactorParams(), {}, function (result) {
				$.unblockUI();
				ctrl.result = result;
				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				ctrl.resultDialog.dialog("open");
				if (ctrl.result.ok == true) {
					ctrl.nextModal.dialog("close");
					ctrl.previousModal.dialog("close");
				}
			}, ctrl.postFail.bind(ctrl));
		} else {
			ctrl.notValidForm();
		}
	};

	ValoresController.prototype.edit = function () {
		var ctrl = this;
		if (ctrl.isFormValid()) {
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [ctrl.params]
			ctrl.service.executeActionPUT(ctrl.refactorParams(), {}, function (result) {
				$.unblockUI();
				ctrl.result = result;
				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				ctrl.resultDialog.dialog("open");
				if (ctrl.result.ok == true) {
					ctrl.nextModal.dialog("close");
					ctrl.previousModal.dialog("close");
				}

			}, ctrl.postFail.bind(ctrl));
		} else {
			ctrl.notValidForm();
		}
	};

	ValoresController.prototype.delete = function () {

		if (this.selectedRows().length > 0) {
			var self = this;
			var deleteObj = [];
			this.selectedRows().forEach(element => {
				deleteObj.push(element.IDVALOR);
			});

			angular.element("#dialog-confirm").html("¿Desea eliminar el/los registro/s seleccionado/s?");

			// Define the Dialog and its properties.
			this.confirmDialog = angular.element("#dialog-confirm").dialog({
				resizable: false,
				modal: true,
				title: "Mensaje de Confirmación",
				height: 150,
				width: 360,
				buttons: {
					" Sí ": function () {
						$(this).dialog('close');
						eliminar();
					},
					" No ": function () {
						$(this).dialog('close');
					}
				}
			});

			function eliminar() {
				// Inicia el "Cargando datos..."
				inicializarLoading();
				// Ejecutamos el servicio pasandole por parámetros [deleteObj]
				self.service.executeActionDEL(deleteObj, {}, function (result) {
					$.unblockUI();
					self.result = result;
					// Ejecuta el modal de respuesta al revicio
					self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					self.resultDialog.dialog("open");

				}, self.postFail.bind(self));
			}
		}
		else {
			fErrorTxt("Debe seleccionar una o más filas para poder realizar esta opcción", 1);
		}
	};

	sibbac20.controller("ValoresController", ["ValoresService", "$httpParamSerializer", "uiGridConstants",
		"$scope", "$filter", ValoresController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);