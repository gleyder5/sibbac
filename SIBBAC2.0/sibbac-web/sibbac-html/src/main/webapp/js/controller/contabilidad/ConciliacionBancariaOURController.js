
sibbac20.controller('ConciliacionBancariaOURController',['$scope', '$filter', '$compile', 'ConciliacionBancariaOURService', 'ConciliacionBancariaCombosService',
                                                         'GenerarInformesService', '$document', 'growl', '$rootScope',
                                                         function($scope, $filter, $compile, ConciliacionBancariaOURService, ConciliacionBancariaCombosService, 
                                                        		 GenerarInformesService, $document, growl, $rootScope) {


	$scope.tituloOUR = "Desgloses OUR";
	
	$scope.resetFiltroOUR = function() {
		$scope.mostrarErrorFiltroFecha = false;
		$scope.filtroOUR = {
			auxiliaresOur : [],
			auxiliaresTLMOur : [],
			aliasOur : [],
			isinOur : [],
			sentidoFilterOur : "",
			nBooking : "",
			fhContratacionDesde : "",
			fhContratacionHasta : "",
			fhLiquidacionDesde : "",
			fhLiquidacionHasta : "",
			refPataCliente : "",
			refDesglose : "",
			refCliente : "",
			refPataMercado : "",
			importeDesde : null,
			importeHasta : null
		};
		if ($scope.listSentidos !== undefined) {
			$scope.defaultSelectedSentido = $scope.listSentidos[2].key;
			$scope.filtroOUR.sentidoFilterOur = $scope.listSentidos[2].key;
		}
		$scope.safeApply();
	};
	
	
	
	$scope.resetFormOUR = function(){
		$scope.formOUR = {
			auxiliaresFilterOur: [],
			auxiliarOur : "",
			aliasFilterOur: [],
			aliasOur : "",
			isinFilterOur: [],
			isinOur : ""
		}
	};
	
	 $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col ) {
		  return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
		    return $('input', td).prop('checked') ? '1' : '0';
		  })
		};

	 $scope.listOurConTheir = [];
	var abreOUR = function(event,listaPartidasTLM){
		$scope.activeAlertOUR = false;
		$scope.alertBlink = false;
		//$scope.listOurConTheir = [];
		// if ($scope.oTableOurConTheir !== undefined && $scope.oTableOurConTheir !== null) {
		// 	$scope.oTableOurConTheir.fnDestroy();
		// }


		
		$scope.listaAuxiliaresOur = [];
		
		$scope.setListaAuxiliaresOUR(event, listaPartidasTLM);
		
		$scope.mostrarErrorFiltroFecha = false;
		
		$scope.cargarAlias();
		
		$scope.cargarIsin();
		
		$scope.cargarSentidos();

		$scope.resetFiltroOUR();
		
		$scope.resetFormOUR();
		
		$scope.seleccionadosOUR = [];
		
		$scope.showingFilterOUR = true;
		
		$scope.tituloPersianaOUR = "Ocultar opciones de búsqueda";
	}

	$scope.$on("abreOUR", abreOUR);
	
	$scope.setListaAuxiliaresOUR = function(event, listaPartidasTLM){
		
		if($scope.listaAuxiliaresOur.length>0){
			var existe = false;
			var indice = -1;
			for(var j=0;j<$scope.listaAuxiliaresOur.length;j++){
				for(var i=0;i<listaPartidasTLM.length;i++){
					if (listaPartidasTLM[i].auxiliarBancario === $scope.listaAuxiliaresOur[j]) {
						existe = true;
					}
				}
				if(!existe){
					indice = j;
				}
				existe = false;
			}
			if(indice>-1){
				if($scope.activeAlertOUR){
					fErrorTxt("Ha deseleccionado la última partida con auxiliar " + $scope.listaAuxiliaresOur[indice] + ". La cosulta del area derecha ha quedado desactualizada. Realice la consulta de nuevo.", 2);
					$scope.showingFilterOUR = true;
					angular.element('#buscadorOUR').slideDown();
					$scope.tituloPersianaOUR = "Mostrar opciones de búsqueda";
					angular.element('#generarApunteModal').modal('hide');
					$scope.alertBlink = true;
					$scope.activeAlertOUR = false;
				}
				var index = 0;
				var indice2 = -1;
				angular.forEach($scope.filtroOUR.auxiliaresOur, function (elemento) {
					if ($scope.listaAuxiliaresOur[indice] == elemento.value) {
						indice2 = index;
					}
					index = index + 1;
				});
				$scope.filtroOUR.auxiliaresOur.splice(indice2, 1);
				$scope.listaAuxiliaresOur.splice(indice, 1);
			}
		}
		
		angular.forEach(listaPartidasTLM, function (elemento) {
			if ($scope.listaAuxiliaresOur.indexOf(elemento.auxiliarBancario) == -1) {
				$scope.listaAuxiliaresOur.push(elemento.auxiliarBancario);
				if($scope.activeAlertOUR){
					fErrorTxt("Ha seleccionado una partida con un nuevo auxiliar (" + elemento.auxiliarBancario + "). La cosulta del area derecha ha quedado desactualizada. Realice la consulta de nuevo.", 2);
					$scope.showingFilterOUR = false;
					angular.element('#buscadorOUR').slideUp();
					$scope.tituloPersianaOUR = "Mostrar opciones de búsqueda";
					angular.element('#generarApunteModal').modal('hide');
					$scope.alertBlink = true;
					$scope.activeAlertOUR = false;
				}
			}
		});
		
		
	};
	
	$scope.seguimientoBusquedaOUR = function() {
		$('.mensajeBusquedaOUR').empty();
		var cadenaFiltrosOUR = "";
		if ($scope.filtroOUR.auxiliaresOur !== null && $scope.filtroOUR.auxiliaresOur.length > 0) {
	    	cadenaFiltrosOUR += " Auxiliar	Contable: ";
	    	for (var i = 0; i < $scope.filtroOUR.auxiliaresOur.length; i++) {
	    		cadenaFiltrosOUR += $scope.filtroOUR.auxiliaresOur[i];
	    		if (i < ($scope.filtroOUR.auxiliaresOur.length -1)) {
	    			cadenaFiltrosOUR += ", ";
	    		}
            }
	    }
		if ($scope.filtroOUR.aliasOur !== null && $scope.filtroOUR.aliasOur.length > 0) {
	    	cadenaFiltrosOUR += " Alias: ";
	    	for (var i = 0; i < $scope.filtroOUR.aliasOur.length; i++) {
	    		cadenaFiltrosOUR += $scope.filtroOUR.aliasOur[i].value;
	    		if (i < ($scope.filtroOUR.aliasOur.length -1)) {
	    			cadenaFiltrosOUR += ", ";
	    		}
            }
	    }
		if ($scope.filtroOUR.isinOur !== null && $scope.filtroOUR.isinOur.length > 0) {
	    	cadenaFiltrosOUR += " ISIN: ";
	    	for (var i = 0; i < $scope.filtroOUR.isinOur.length; i++) {
	    		cadenaFiltrosOUR += $scope.filtroOUR.isinOur[i].value;
	    		if (i < ($scope.filtroOUR.isinOur.length -1)) {
	    			cadenaFiltrosOUR += ", ";
	    		}
            }
	    }
	    if ($scope.filtroOUR.sentidoFilterOur !== null && $scope.filtroOUR.sentidoFilterOur !== "") {
	    	cadenaFiltrosOUR += " Sentido: " + $scope.filtroOUR.sentidoFilterOur;
	    }
	    if ($scope.filtroOUR.nBooking !== null && $scope.filtroOUR.nBooking !== "") {
	    	cadenaFiltrosOUR += " Num. Booking: " + $scope.filtroOUR.nBooking;
	    }
	    if ($scope.filtroOUR.fhContratacionDesde !== null && $scope.filtroOUR.fhContratacionDesde !== "") {
	    	cadenaFiltrosOUR += " Fh. Contrataci&oacute;n Desde: " + $scope.filtroOUR.fhContratacionDesde;
	    }
	    if ($scope.filtroOUR.fhContratacionHasta !== null && $scope.filtroOUR.fhContratacionHasta !== "") {
	    	cadenaFiltrosOUR += " Fh. Contrataci&oacute;n Hasta: " + $scope.filtroOUR.fhContratacionHasta;
	    }
	    if ($scope.filtroOUR.fhLiquidacionDesde !== null && $scope.filtroOUR.fhLiquidacionDesde !== "") {
	    	cadenaFiltrosOUR += " Fh. Liquidaci&oacute;n Desde: " + $scope.filtroOUR.fhLiquidacionDesde;
	    }
	    if ($scope.filtroOUR.fhLiquidacionHasta !== null && $scope.filtroOUR.fhLiquidacionHasta !== "") {
	    	cadenaFiltrosOUR += " Fh. Liquidaci&oacute;n Hasta: " + $scope.filtroOUR.fhLiquidacionHasta;
	    }
	    if ($scope.filtroOUR.refCliente !== null && $scope.filtroOUR.refCliente !== "") {
	    	cadenaFiltrosOUR += " Ref. Cliente: " + $scope.filtroOUR.refCliente;
	    }
	    if ($scope.filtroOUR.refDesglose !== null && $scope.filtroOUR.refDesglose !== "") {
	    	cadenaFiltrosOUR += " Ref. Desglose: " + $scope.filtroOUR.refDesglose;
	    }
	    if ($scope.filtroOUR.refPataCliente !== null && $scope.filtroOUR.refPataCliente !== "") {
	    	cadenaFiltrosOUR += " Referencia Pata Cliente: " + $scope.filtroOUR.refPataCliente;
	    }
	    if ($scope.filtroOUR.refPataMercado !== null && $scope.filtroOUR.refPataMercado !== "") {
	    	cadenaFiltrosOUR += " Referencia Pata Mercado: " + $scope.filtroOUR.refPataMercado;
	    }
	    if ($scope.filtroOUR.importeDesde !== null && $scope.filtroOUR.importeDesde !== "") {
	    	cadenaFiltrosOUR += " Importe Desde: " + $scope.filtroOUR.importeDesde;
	    }
	    if ($scope.filtroOUR.importeHasta !== null && $scope.filtroOUR.importeHasta !== "") {
	    	cadenaFiltrosOUR += " Importe Hasta: " + $scope.filtroOUR.importeHasta;
	    }

		$('.mensajeBusquedaOUR').append(cadenaFiltrosOUR);
	}
	
	
	$scope.$on("setListaAuxiliaresOUR", $scope.setListaAuxiliaresOUR);
	
	$scope.generaListFechas = function (){
		
		var listFechas = [];
		var fechaInicio =  "";
		var fechaFin =  "";
		
	    if ($scope.filtroOUR.fhContratacionDesde) {
	    	fechaInicio = $scope.filtroOUR.fhContratacionDesde;
	    	var fechaSplit = fechaInicio.split("/");
	    	fechaInicio = fechaSplit[1] + "/" + fechaSplit[0] + "/" + fechaSplit[2];
	    }
	    if ($scope.filtroOUR.fhContratacionHasta) {
	    	fechaFin = $scope.filtroOUR.fhContratacionHasta;
	    	var fechaSplit = fechaFin.split("/");
	    	fechaFin = fechaSplit[1] + "/" + fechaSplit[0] + "/" + fechaSplit[2];
	    }
	    if ($scope.filtroOUR.fhLiquidacionDesde) {
	    	fechaInicio = $scope.filtroOUR.fhLiquidacionDesde;
	    	var fechaSplit = fechaInicio.split("/");
	    	fechaInicio = fechaSplit[1] + "/" + fechaSplit[0] + "/" + fechaSplit[2];
	    }
	    if ($scope.filtroOUR.fhLiquidacionHasta !== null && $scope.filtroOUR.fhLiquidacionHasta !== "") {
	    	fechaFin = $scope.filtroOUR.fhLiquidacionHasta;
	    	var fechaSplit = fechaFin.split("/");
	    	fechaFin = fechaSplit[1] + "/" + fechaSplit[0] + "/" + fechaSplit[2];
	    }
	    
	    if(!fechaFin){
	    	fechaFin = new Date().getTime();
	    }else{
	    	fechaFin = new Date(fechaFin).getTime();
	    }
	    
	    fechaInicio = new Date(fechaInicio).getTime();
	    
	    listFechas.push($filter('date')(new Date(fechaInicio), "dd/MM/yyyy"));
	    
	    while($filter('date')(new Date(fechaInicio), "dd/MM/yyyy")!=$filter('date')(new Date(fechaFin), "dd/MM/yyyy")){
	    	fechaInicio = fechaInicio + (3600 * 24 * 1000);
	    	listFechas.push($filter('date')(new Date(fechaInicio), "dd/MM/yyyy"));
	    }
	    
	    return listFechas;
		
	};

	$scope.resetDatatableOur= function(){

        // borra el contenido del body de la tabla
        var tbl = $("#datosOURConTheir > tbody");
        $(tbl).html("");


        let dataTableParams = {
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "aButtons": ["print"]
            },
            "aoColumns": [{sClass: "centrar", width: "10px", orderDataType: "dom-checkbox"},
                {width: "150px"}, {width: "150px"}, {width: "150px"}, {
                    width: "150px",
                    sClass: "monedaR",
                    type: "formatted-num"
                },
                {width: "150px"}, {width: "150px"}, {width: "150px"}, {width: "150px"},
                {width: "150px"}, {width: "150px"}, {width: "150px"}],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $compile(nRow)($scope);
            },
            "scrollX": "100%",
            "scrollCollapse": true,
            "language": {
                "url": "i18n/Spanish.json"
            }
        };
        if($scope.listOurConTheir!=undefined && !$scope.listOurConTheir.length){
            if($scope.oTableOurConTheir!=undefined){
                $scope.oTableOurConTheir.fnDestroy();
            }
            $scope.oTableOurConTheir = $("#datosOURConTheir").dataTable(dataTableParams);
        }
        if($scope.oTableOurConTheir==undefined){
            $scope.oTableOurConTheir = $("#datosOURConTheir").dataTable(dataTableParams);
        }
        $scope.oTableOurConTheir.fnClearTable();

        for (var i = 0; i < $scope.listOurConTheir.length; i++) {

            var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listOurConTheir[' + i + '].selected" ng-click="seleccionarElementoOUR('+ i + ');"/>';

            var ourConTheirList = [
                check,
                $scope.listOurConTheir[i].auxContable,
                $scope.listOurConTheir[i].sentido,
                $scope.listOurConTheir[i].codIsinNombIsin,
                $.number($scope.listOurConTheir[i].impOurPteGenerar, 2, ',', '.'),
                $filter('date')($scope.listOurConTheir[i].fhContratacion, "dd/MM/yyyy"),
                $filter('date')($scope.listOurConTheir[i].fhLiquidacion, "dd/MM/yyyy"),
                $scope.listOurConTheir[i].nBooking,
                $scope.listOurConTheir[i].nDesglose,
                $filter('number')($scope.listOurConTheir[i].secDesglose, 2),
                $scope.listOurConTheir[i].refCliente,
                $scope.listOurConTheir[i].refS3];

            $scope.oTableOurConTheir.fnAddData(ourConTheirList, false);

        }
        $scope.oTableOurConTheir.fnDraw();
        $scope.safeApply();


};

	$scope.consultarOurConTheir = function () {
		if($scope.listaAuxiliaresOur.length>0){
			$scope.mostrarErrorFiltroFecha = $scope.validacionFiltroFecha();
			if (!$scope.mostrarErrorFiltroFecha) {
				
				var listaFechas = $scope.generaListFechas();
				
				$scope.showingFilterOUR = false;
				$scope.tituloPersianaOUR = "Mostrar opciones de búsqueda";
				angular.element('#buscadorOUR').slideUp();
				$scope.activeAlertOUR = true;
				$scope.alertBlink = false;
				inicializarLoading();
	
				$scope.filtroOUR.auxiliaresTLMOur = $scope.listaAuxiliaresOur;
	
				ConciliacionBancariaOURService.consultarOurConTheirBis(function (data) {
					
					if (data.resultados.status === 'KO' || data.resultados.timeout) {
						if(data.resultados.timeout){
							$.unblockUI();
							fErrorTxt(data.resultados.timeout,2);
						}else{
							$.unblockUI();
							fErrorTxt("Se produjo un error en la carga del listado de Our con Their.",1);
						}
					} else {
	
						$scope.listOurConTheir = data.resultados["listaOurConTheir"];
	
						$scope.resetDatatableOur();
						$.unblockUI();
						console.log("");
					}
					angular.element('#generarApunteModal').modal({
                        backdrop : 'static'
                   });
                   $(".modal-backdrop").remove();
				},   
				function (error) {
					$.unblockUI();
					fErrorTxt("Se produjo un error en la carga del listado de Our con Their.",1);
				}, $scope.filtroOUR, listaFechas);
			}
		}else{
			fErrorTxt("Debe seleccionar al menos una partida de la pantalla de la izquierda.",2);
		}
		$scope.seguimientoBusquedaOUR();
	};
	
	$scope.cargarAlias = function () {
		GenerarInformesService.cargaAlias(function (data) {
			if (data.resultados.status == 'KO') {
				fErrorTxt("Se produjo un error en la carga de los combos Alias", 1);
			} else {
				$scope.listaAliasOur = data.resultados["listaAlias"];
				$scope.populateAutocomplete("#filtroAliasOur", $scope.listaAliasOur);
			}
		}, function (error) {
			fErrorTxt("Se produjo un error en la carga de los combos Alias", 1);
		});
	}

	$scope.cargarIsin = function () {
		GenerarInformesService.cargaIsin(function (data) {
			if (data.resultados.status == 'KO') {
				fErrorTxt("Se produjo un error en la carga de los combos ISIN", 1);
			} else {
				$scope.listaIsinOur = data.resultados["listaIsin"];
				$scope.populateAutocomplete("#filtroIsinOur", $scope.listaIsinOur);
			}
		}, function (error) {

			fErrorTxt("Se produjo un error en la carga de los combos ISIN", 1);
		});
	}

	$scope.cargarSentidos = function () {
		ConciliacionBancariaCombosService.sentidos(function (data) {
			if (data.resultados.status == 'KO') {
				fErrorTxt("Se produjo un error en la carga del combo de Sentidos", 1);
			} else {
				$scope.listSentidos = data.resultados["listaSentidos"];
				$scope.defaultSelectedSentido = $scope.listSentidos[2].key;
				$scope.filtroOUR.sentidoFilterOur = $scope.listSentidos[2].key;
			}
		}, function (error) {
			fErrorTxt("Se produjo un error en la carga del combo de Sentidos", 1);
		});
	};
	
    $scope.vaciarTabla = function (tabla) {
        switch (tabla) {
          case "auxiliaresOur":
                $scope.filtroOUR.auxiliaresOur = [];
                break;	
          case "aliasOur":
              $scope.filtroOUR.aliasOur = [];
              break;
          case "isinOur":
              $scope.filtroOUR.isinOur = [];
              break;
          default:
            break;
        }
     };
     
     $scope.eliminarElementoTabla = function (tabla) {
          switch (tabla) {		
            case "auxiliaresOur":
                  for (var i = 0; i < $scope.filtroOUR.auxiliaresOur.length; i++) {
                    if ( $scope.filtroOUR.auxiliaresOur[i] ===  $scope.formOUR.auxiliaresFilterOur) {
                 	   $scope.filtroOUR.auxiliaresOur.splice(i, 1);
                    }
                  }
                  break;	
            case "aliasOur":
                for (var i = 0; i < $scope.filtroOUR.aliasOur.length; i++) {
                  if ( $scope.filtroOUR.aliasOur[i].value ===  $scope.formOUR.aliasFilterOur.value) {
               	   $scope.filtroOUR.aliasOur.splice(i, 1);
                  }
                }
                break;	
            case "isinOur":
                for (var i = 0; i < $scope.filtroOUR.isinOur.length; i++) {
                  if ( $scope.filtroOUR.isinOur[i].value ===  $scope.formOUR.isinFilterOur.value) {
               	   $scope.filtroOUR.isinOur.splice(i, 1);
                  }
                }
                break;	
            default:
              break;
          }
     };
     
		$scope.populateAutocomplete = function (input, availableTags) {
            $(input).autocomplete({
               minLength : 0,
               source : availableTags,
               focus : function (event, ui) {
                 return false;
               },
               select : function (event, ui) {

                 var option = {
                   key : ui.item.key,
                   value : ui.item.value
                 };

                 switch (input) {
                   case "#filtroAliasOur":
                       var existe = false;
                       angular.forEach($scope.filtroOUR.aliasOur, function (campo) {
                         if (campo.value === option.value) {
                           existe = true;
                         }
                       });
                       $scope.formOUR.aliasOur = "";
                       if (!existe) {
                         $scope.filtroOUR.aliasOur.push(option);
                       }
                       break;
                   case "#filtroIsinOur":
                       var existe = false;
                       angular.forEach($scope.filtroOUR.isinOur, function (campo) {
                         if (campo.value === option.value) {
                           existe = true;
                         }
                       });
                       $scope.formOUR.isinOur = "";
                       if (!existe) {
                         $scope.filtroOUR.isinOur.push(option);
                       }
                       break;
                   default:
                     break;
                 }

                 $scope.safeApply();

                 return false;
               }
            });
		};
		
        $scope.procesarAuxiliar = function () {
            var existe = false;
            angular.forEach($scope.filtroOUR.auxiliaresOur, function (campo) {
              if (campo === $scope.filtroOUR.auxiliarOur) {
                existe = true;
              }
            });
            if (!existe) {
              $scope.filtroOUR.auxiliaresOur.push($scope.filtroOUR.auxiliarOur);
            }
            $scope.filtroOUR.auxiliarOur = "";
          };
          
 		 /***************************************************
  		 * ** ACCIONES TABLA ***
  		 **************************************************/

  		var seleccionarTodosOUR = function() {
  			$scope.seleccionadosBorrarOUR = [];
  			for (var i = 0; i < $scope.listOurConTheir.length; i++) {
  				$scope.listOurConTheir[i].selected = true;
  			}
  			$scope.seleccionadosOUR = angular.copy($scope.listOurConTheir);
  		};
  		
  		$scope.$on("seleccionarTodosOUR", seleccionarTodosOUR);

  		var deseleccionarTodosOUR = function() {
  			for (var i = 0; i < $scope.listOurConTheir.length; i++) {
  				$scope.listOurConTheir[i].selected = false;
  			}
  			$scope.seleccionadosOUR = [];
  		};
  		
  		$scope.$on("deseleccionarTodosOUR", deseleccionarTodosOUR);

  		$scope.seleccionarElementoOUR = function(row) {
  			if ($scope.listOurConTheir[row] != null && $scope.listOurConTheir[row].selected) {
  				$scope.listOurConTheir[row].selected = false;
  				var tam = $scope.seleccionadosOUR.length;
  				for (var i = 0; i < tam; i++) {
  					if ($scope.seleccionadosOUR[i].nuOrden === $scope.listOurConTheir[row].nuOrden && 
  							$scope.seleccionadosOUR[i].nBooking === $scope.listOurConTheir[row].nBooking &&
  								$scope.seleccionadosOUR[i].nDesglose === $scope.listOurConTheir[row].nDesglose &&
  									$scope.seleccionadosOUR[i].secDesglose === $scope.listOurConTheir[row].secDesglose &&
  										$scope.seleccionadosOUR[i].impOurPteGenerar === $scope.listOurConTheir[row].impOurPteGenerar) {
  						$scope.seleccionadosOUR.splice(i, 1);
  					}
  				}
  			} else {
  				$scope.listOurConTheir[row].selected = true;
  				$scope.seleccionadosOUR.push($scope.listOurConTheir[row]);
  			}
  		};
  		
  		/***************************************************
  		 * ** ACCION GENERAR APUNTE ***
  		 **************************************************/
  		
  		var generarApunteOUR = function(event, listSeleccionadosIzq, concepto, apunteTotal){
  			if(listSeleccionadosIzq.length>0 && $scope.seleccionadosOUR.length>0){
  				 angular.element('#generarApunteModal').modal('hide');
  				 angular.element("#dialog-confirm").html("¿Esta seguro de que desea realizar el apunte contable?");
  				 angular.element("#dialog-confirm").dialog({
  					 resizable : false,
  					 modal : true,
  					 title : "Mensaje de Confirmación",
  					 height : 150,
  					 width : 360,
  					 buttons : {
  						 " Sí " : function () {
  				  				inicializarLoading();
  				  				ConciliacionBancariaOURService.generarApuntes(function (data) {
  				  					if (data.resultados.status === 'KO') {
  				  						fErrorTxt("Error en la peticion de datos al generar apuntes.", 1);
  				  					}else if (data.resultados.status === 'KOSemaforo') {
  				  						fErrorTxt(data.error, 1);
  				  					}else{
  				  						fErrorTxt("Los apuntes se generaron correctamente.", 3);
  				  						$scope.resetFiltroOUR();
  				  						
  				  						$scope.resetFormOUR();
  				  						
  				  						$scope.seleccionadosOUR = [];

  				  						$scope.$emit("resetPantalla");
                                        $scope.listOurConTheir.forEach(function(itm){
                                            if(itm.selected){
                                                $scope.listOurConTheir.splice($scope.listOurConTheir.indexOf(itm),1);
                                            }
                                        });

                                        $scope.resetDatatableOur();

                                    }

  				  					$.unblockUI();
  				  				}, function (error) {
  				  					fErrorTxt("Se produjo un error al generar los apuntes en el fichero XLS.", 1);
  				  					$.unblockUI();
  				  				}, {listaPartidasTLMIzq: listSeleccionadosIzq, listaDesglosesOUR: $scope.seleccionadosOUR, concepto: concepto, apunte: apunteTotal, auditUser : $rootScope.userName}); 
  							 $(this).dialog('close');
  						 }," No " : function () {
  							 angular.element('#generarApunteModal').modal({backdrop : 'static'});
  							 $(".modal-backdrop").remove();
  							 $(this).dialog('close');
  						 }
  					 }
  		         });
  				 $('.ui-dialog-titlebar-close').remove();

  			}else{
  				fErrorTxt("Debe seleccionar al menos una partida TLM en las dos areas de la pantalla.", 2);
  			}
  		}
  		
  		$scope.$on("generarApunteOUR", generarApunteOUR);
  		
  		/***************************************************
  		 * ** ACCION SELECCIONAR REGISTROS TABLA ***
  		 **************************************************/
  		$scope.$watch('seleccionadosOUR', function (newVal, oldVal) {
  			if (newVal !== oldVal && newVal) {

  				var importeDebe = 0;
  				var importeHaber = 0;

  				angular.forEach($scope.seleccionadosOUR, function (partida) {
  					if(partida.codigoPlantilla.indexOf("LQNET")>-1){
  						if (partida.sentido === "ENTREGA"){
  							importeHaber = importeHaber + partida.impOurPteGenerar;
  						} else if (partida.sentido === "RECEPCION") {
  							importeDebe = importeDebe + partida.impOurPteGenerar;
  						}
  					} else if (partida.codigoPlantilla.indexOf("LQEFE")>-1) {
  						if (partida.sentido === "COMPRA") {
  							importeDebe = importeDebe + partida.impOurPteGenerar;
  						} else if (partida.sentido === "VENTA") {
  							importeHaber = importeHaber + partida.impOurPteGenerar;
  						}
  					} else {
  						importeHaber = importeHaber + partida.impOurPteGenerar;
  					}
  				});
			 	var importePyG = importeDebe - importeHaber;
			 	
			 	$scope.$emit("calculaImporte", importeDebe, importeHaber, importePyG);
				 	
             }
           }, true);
  		
		 /** Validacion de campos del filtro de Fecha. */
		 $scope.validacionFiltroFecha = function () {
			 if (($scope.filtroOUR.fhContratacionDesde == '' && $scope.filtroOUR.fhContratacionHasta != '') || 
				 ($scope.filtroOUR.fhLiquidacionDesde == '' && $scope.filtroOUR.fhLiquidacionHasta != '') || 
				 ($scope.filtroOUR.fhContratacionDesde != '' && $scope.filtroOUR.fhLiquidacionDesde != '') ||
				 ($scope.filtroOUR.fhContratacionDesde == '' && $scope.filtroOUR.fhContratacionHasta == '' && $scope.filtroOUR.fhLiquidacionDesde == '' && $scope.filtroOUR.fhLiquidacionHasta == '') ||
				 ($scope.filtroOUR.sentidoFilterOur == '')) {
				 return true;
			 } else {
				 return false;
			 }
		 };
		 
		 $scope.mostrarOcultarFiltrosOUR = function () {
			 if ($scope.showingFilterOUR == true) {
				 $scope.showingFilterOUR = false;
				 angular.element('#buscadorOUR').slideUp();
				 $scope.tituloPersianaOUR = "Mostrar opciones de búsqueda";
			 } else {
				 $scope.showingFilterOUR = true;
				 angular.element('#buscadorOUR').slideDown();
				 $scope.tituloPersianaOUR = "Ocultar opciones de búsqueda";
			 }
		 };
		 
		

} ]);	
