(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var MantenimientoTituController = function (service, httpParamSerializer, uiGridConstants, $scope, $filter) {
		var ctrl = this;
		// Sub controlador DynamicFiltersController
		GENERIC_CONTROLLERS.DynamicFiltersController.call(ctrl, service, httpParamSerializer, uiGridConstants);
		// Opciones del modal (480px ancho)
		var wWith = $(window).width();
		ctrl.filter = {};
		ctrl.idMifidMemo = {};
		ctrl.listadIdMifid = [];
		ctrl.listadoTipoUsuario = [];
		ctrl.listadoPaises = [];
		ctrl.result = {};
		ctrl.result.errorMessage = [];
		ctrl.formIds = [];
		ctrl.scope = $scope;
		ctrl.saveFlag = true;
		var fatDialogOpt = {
			width: wWith * 0.8,
			top: wWith * 0.2,
			left: wWith * 0.2
		};

		
		this["combo-TIPO_OP_"] = [ {
			label : "COMPRA",
			value : "C"
		}, {
			label : "VENTA",
			value : "V"
		} ];

		ctrl.getIds = function (elements) {
			for (var i = 0; i < elements.length; i++) {
				ctrl.formIds.push(elements[i].id);
			};
		}

		// ctrl.scope.$watch(ctrl.grid.data, function (newValue) {
		// 	Object.keys(newValue).forEach( function (data) {
		// 		var aux = data.replace("_", data);

		// 		ctrl.grid.data[aux] = ctrl.grid.data[data];
		// 		ctrl.grid.data[data] = undefined
		// 	})
		// });

		var modalElement = $("#modalMantenimiento");

		ctrl.getIds(modalElement.find("input"));
		ctrl.getIds(modalElement.find("select"));

		this.dialogs = ["#modalMantenimiento"];

		this.dialogs.forEach(dialog => {
			var aux = $('[role=dialog]').find(dialog);
			(aux.length > 0) ? aux.dialog('destroy').remove() : undefined;
		});

		// Llamadas a back para completar los combos
		ctrl.fillCombos();

		// Resgistramos los modales por la id, y le asignamos una función para el boton con el nombre asignado (Guardar, Modificar, etc)
		ctrl.maintenanceModal = ctrl.registryModal("modalMantenimiento", "Crear", {
			Aceptar: function () {
				ctrl.validateIdMifid();
				ctrl.saveFlag ? ctrl.save() : ctrl.edit();
			},
			Cancelar: function () {
				ctrl.executeQuery();
				$(this).dialog("close");
			}
		}, fatDialogOpt);

		$('[role=dialog]').has("#modalMantenimiento").find("button:contains('Cerrar')").remove();

		ctrl.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					ctrl.resultDialog.dialog("close");
					if (ctrl.result.ok) {
						ctrl.executeQuery();
					}

				}
			}
		});
		// Ejecutamos la función selectQuery y executeQuery cuando obtengamos la query del back
		ctrl.queries.$promise.then(data => {
			ctrl.selectedQuery = data[0];
			ctrl.selectQuery();
			ctrl.executeQuery();
		});

		ctrl.refactorParams = function () {
			var aux = {};
			Object.keys(ctrl.params).forEach(function (key) {
				aux[key.toLowerCase()] = ctrl.params[key];
			});
			var cuentaproducto = aux["cuentaproducto"];
			var cuentaLength = (cuentaproducto != undefined) ? cuentaproducto.length - 3 : undefined; 
			aux["nbapellido2"] = (aux["nbapellido2"] != undefined) ? aux["nbapellido2"] : ""; 
			aux["cuenta"] = (cuentaproducto != undefined && cuentaproducto.length > 3) ? cuentaproducto.substring(0, cuentaLength) : undefined; 
			aux["producto"] = (cuentaproducto != undefined && cuentaproducto.length > 3)  ? cuentaproducto.substring(cuentaLength) : undefined; 
			aux["fhinicio"] = (aux["fhinicio"]) ? ctrl.filterDate(aux["fhinicio"]) : undefined;
			aux["fhfinal"] = (aux["fhfinal"]) ? ctrl.filterDate(aux["fhfinal"]) : undefined;
			aux["fhaudit"] = (aux["fhaudit"]) ? ctrl.filterDate(aux["fhaudit"]) : undefined;
			aux["indtrader"] = (aux["indtrader"] == "S") ? 1 : 0;
			aux["indsale"] = (aux["indsale"] == "S") ? 1 : 0;
			aux["indtradsale"] = (aux["indtradsale"] == "S") ? 1 : 0;
			aux["indaccount"] = (aux["indaccount"] == "S") ? 1 : 0;
			return aux;
		}

		// Devuelve las fechas con el siguiente formato[dd/MM/yyyy]
		ctrl.filterDate = function (date) {
			return $filter('date')(date, 'dd/MM/yyyy');
		}

		// Metodo que pinta en un array los valores que hallan fallado en las validaciones
		ctrl.fillErrMsg = function (key, validity) {
			var msgs = ctrl.result.errorMessage;

			if (!validity.valid) {
				validity.valueMissing ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es obligatorio") : '';
				validity.patternMismatch ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] no respeta el formato") : '';
				validity.tooShort ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es demasiado corto") : '';
				validity.tooLong ? msgs.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es demasiado largo") : '';
			}
		}

		// Abrimos el formulario que muestra los errores del formulario
		ctrl.notValidForm = function () {
			ctrl.result.ok = false;

			ctrl.resultDialog.dialog("option", "title", "Ejecución con errores");
			ctrl.resultDialog.dialog("open");
			ctrl.scope.$applyAsync();
		}

		// Modificador del datepicker pasado por parámetros dejando el año a modificar directamente y dandole rango.
		ctrl.datepickerModified = function (id) {
			$(id).datepicker({
				changeYear: true,
				yearRange: "1900:9999",
				autoSize: true
			});
		}

		// Método que devuelve comprueba los errores que se han dado en las validaciones y en el que se llena una array con los valores erroneos que hallan salidos
		ctrl.isFormValid = function () {
			ctrl.result = {};
			ctrl.result.errorMessage = [];
			ctrl.formIds.forEach(function (key) {
				var element = document.getElementById(key);
				var validity = element.validity;
				if (element.type != "select-one") {
					if (element.readOnly) {
						element.readOnly = false;
						ctrl.fillErrMsg(key, validity)
						element.readOnly = true;
					} else {
						ctrl.fillErrMsg(key, validity)
					}
				} else {
					(element.required && element.value == "") ? ctrl.result.errorMessage.push("- El campo [" + $("label[for='" + key + "']")[0].innerText + "] es obligatorio") : '';
				}
			});
			if (!ctrl.IDMIFID) {
				ctrl.result.errorMessage.push("- El campo [ID MIFID] no respeta el formato a seguir")
			}
			return ctrl.result.errorMessage == 0;
		};

		ctrl.validateIdMifid = function () {
			ctrl.IDMIFID = true;

			switch (ctrl.params.TIPOIDMIFID) {
				case "N":
					// ctrl.service.executeActionIsValidCountry(ctrl.params.CDNACIONALIDAD, function (result) {
					// 	if (result.length > 0) {
					// 		if (ctrl.params.IDMIFID.substring(0, 2) == "ES") {
					// 			// Comprobar Nif como en gestion cliente de sibbac
					// 		} else {
					// 			ctrl.IDMIFID = false;
					// 		}
					// 	} else {
					// 		ctrl.IDMIFID = false;
					// 	}
					// }, ctrl.postFail.bind(ctrl));
					break;
			}
		}

		ctrl.isCONCAT = function (memoFlag) {
			if (ctrl.params.TIPOIDMIFID == "C") {
				ctrl.idMifidMemo = (memoFlag) ? ctrl.params.IDMIFID : ctrl.idMifidMemo;
				var nacionalidad = $("#CDNACIONALIDAD")[0];
				var aux = ctrl.calculateIdMIfid();

				if (!(nacionalidad.required && nacionalidad.value == "") && aux != "") {
					// ctrl.service.executeActionIsValidCountry(nacionalidad.value, function (result) {
					// 	ctrl.params.IDMIFID = result[0] + aux;
					// });
				} else {
					fErrorTxt("Es necesario que Nombre, Primer apellido, Pais nacionalidad y Fecha nacimiento sean introducidos correctamente", 1);
					ctrl.params.TIPOIDMIFID = "";
				}
			} else {
				ctrl.params.IDMIFID = (ctrl.idMifidMemo != "") ? ctrl.idMifidMemo : ctrl.params.IDMIFID;
			}
		}

		ctrl.calculateIdMIfid = function () {
			var fNac = $("#FECHANACIMIENTO")[0];
			fNac.readOnly = false;
			var name = ($("#NBNOMBRE")[0].value + "#####").slice(0, 5);
			var subname = ($("#NBAPELLIDO1")[0].value + "#####").slice(0, 5);
			var aux = "";
			var day = fNac.value.substring(0, 2);
			var month = fNac.value.substring(3, 5);
			var year = fNac.value.substring(6, 10);
			if (fNac.validity.valid && $("#NBNOMBRE")[0].validity.valid && $("#NBAPELLIDO1")[0].validity.valid) {
				aux = year + month + day + name + subname;
			}
			return aux;
		}

		// ctrl.datepickerModified('#id');
	};


	MantenimientoTituController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

	MantenimientoTituController.prototype.constructor = MantenimientoTituController;

	MantenimientoTituController.prototype.fillCombos = function () {
		var ctrl = this;
		inicializarLoading();
		ctrl.when(
			function (done) {
				// ctrl.service.executeActionGetPaises(function (result) {
				// 	result.forEach(function (data) {
				// 		ctrl.listadoPaises.push({ value: data.cdisonum.trim(), label: data.nbpais });
				// 	})
				// 	done();
				// }, ctrl.postFail.bind(ctrl));
			},
			function (done) {
				ctrl.listadIdMifid.push({ value: 'N', label: 'National ID' });
				ctrl.listadIdMifid.push({ value: 'P', label: 'Passport' });
				ctrl.listadIdMifid.push({ value: 'C', label: 'CONCAT' });
				done();
			},
			function (done) {
				ctrl.listadoTipoUsuario.push({ value: 'T', label: 'Trader' });
				ctrl.listadoTipoUsuario.push({ value: 'S', label: 'Sale' });
				done();
			}
		).then(function () {
			$.unblockUI();
		})
	}

	MantenimientoTituController.prototype.when = function () {
		var args = arguments;  // the functions to execute first
		return {
			then: function (done) {
				var counter = 0;
				for (var i = 0; i < args.length; i++) {
					// call each function with a function to call on done
					args[i](function () {
						counter++;
						if (counter === args.length) {  // all functions have notified they're done
							done();
						}
					});
				}
			}
		};
	};

	// Función que se encarga de levantar los modales con una id que identifica su tipo 
	MantenimientoTituController.prototype.openModal = function (id) {
		var ctrl = this;
		ctrl.idMifidMemo = "";
		switch (id) {
			case "save":
				ctrl.saveFlag = true;
				ctrl.params = {};

				ctrl.maintenanceModal.dialog("option", "title", "Crear");
				ctrl.maintenanceModal.dialog("open");

				break;
			case "edit":
				ctrl.saveFlag = false;
				if (ctrl.selectedRows().length > 0) {
					// Igualamos los valores a los parámetros que recibiremos en el modal
					ctrl.params = angular.copy(ctrl.selectedRows()[0]);
					ctrl.params.FECHANACIMIENTO = ctrl.filterDate(ctrl.params.FECHANACIMIENTO);

					ctrl.maintenanceModal.dialog("option", "title", "Modificar");
					ctrl.maintenanceModal.dialog("open");
				}

				break;
		}
	};

	MantenimientoTituController.prototype.save = function () {
		var ctrl = this;
		if (ctrl.isFormValid() && this.params.IDMIFID != undefined && this.params.IDMIFID.trim() != "") {
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [ctrl.params]
			ctrl.service.executeActionPOST(ctrl.refactorParams(), {}, function (result) {
				$.unblockUI();
				ctrl.result = result;
				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				ctrl.resultDialog.dialog("open");
				if (ctrl.result.ok == true) {
					ctrl.maintenanceModal.dialog("close");
				}
			}, ctrl.postFail.bind(ctrl));
		} else if(document.getElementById("IDMIFID").validity.valid && (this.params.IDMIFID == undefined || this.params.IDMIFID.trim() == "")) {
			ctrl.result.errorMessage.push("- El campo [" + $("label[for='IDMIFID']")[0].innerText + "] es obligatorio");
			ctrl.notValidForm();
		} else {
			ctrl.notValidForm();
		}
	};

	MantenimientoTituController.prototype.edit = function () {
		var ctrl = this;
		if (ctrl.isFormValid()) {
			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [ctrl.params]
			ctrl.service.executeActionPUT(ctrl.refactorParams(), {}, function (result) {
				$.unblockUI();
				ctrl.result = result;
				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				ctrl.resultDialog.dialog("open");
				if (ctrl.result.ok == true) {
					ctrl.maintenanceModal.dialog("close");
				}

			}, ctrl.postFail.bind(ctrl));
		} else {
			ctrl.notValidForm();
		}
	};

	MantenimientoTituController.prototype.delete = function () {

		if (this.selectedRows().length > 0) {
			var self = this;
			var deleteObj = [];
			this.selectedRows().forEach(element => {
				deleteObj.push(element.CDEMPLEADO);
			});

			angular.element("#dialog-confirm").html("¿Desea eliminar el/los registro/s seleccionado/s?");

			// Define the Dialog and its properties.
			this.confirmDialog = angular.element("#dialog-confirm").dialog({
				resizable: false,
				modal: true,
				title: "Mensaje de Confirmación",
				height: 150,
				width: 360,
				buttons: {
					" Sí ": function () {
						$(this).dialog('close');
						eliminar();
					},
					" No ": function () {
						$(this).dialog('close');
					}
				}
			});

			function eliminar() {
				// Inicia el "Cargando datos..."
				inicializarLoading();
				// Ejecutamos el servicio pasandole por parámetros [deleteObj]
				self.service.executeActionDEL(deleteObj, {}, function (result) {
					$.unblockUI();
					self.result = result;
					// Ejecuta el modal de respuesta al revicio
					self.resultDialog.dialog("option", "title", self.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					self.resultDialog.dialog("open");

				}, self.postFail.bind(self));
			}
		}
		else {
			fErrorTxt("Debe seleccionar una o más filas para poder realizar esta opcción", 1);
		}
	};

	sibbac20.controller("MantenimientoTituController", ["MantenimientoTituService", "$httpParamSerializer", "uiGridConstants",
		"$scope", "$filter", MantenimientoTituController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);