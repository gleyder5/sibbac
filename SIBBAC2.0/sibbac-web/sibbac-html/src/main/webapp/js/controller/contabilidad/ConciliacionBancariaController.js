sibbac20.controller(
				'ConciliacionBancariaController',
				['$scope','$compile','ConciliacionBancariaService','ConciliacionBancariaCombosService','$document','growl','$rootScope','commonHelper', 'CONSTANTES',
						function($scope, $compile, ConciliacionBancariaService, ConciliacionBancariaCombosService, $document, growl, $rootScope, commonHelper,Constantes) {

				$scope.safeApply = function(fn) {
					var phase = this.$root.$$phase;
					if (phase === '$apply' || phase === '$digest') {
						if (fn && (typeof (fn) === 'function')) {
							fn();
						}
					} else {
						this.$apply(fn);
					}
				};

				var hoy = new Date();
				var dd = hoy.getDate();
				var mm = hoy.getMonth() + 1;
				var yyyy = hoy.getFullYear();
				hoy = yyyy + "_" + mm + "_" + dd;
				
				$scope.resetImportesIzq = function(){
					$scope.importeDebeIzq = 0;
					$scope.importeHaberIzq = 0;
					$scope.importePyGIzq = 0;
				};
				$scope.resetImportesIzq();
				
				$scope.resetImportesDecha = function(){
					$scope.importeDebeDcha = 0;
					$scope.importeHaberDcha = 0;
					$scope.importePyGDcha = 0;
				};
				$scope.resetImportesDecha();
				
				$scope.resetImportesTotal = function(){
					$scope.apunteTotal = {
							importeDebeTotal : 0,
							importeHaberTotal: 0,
							importePyGTotal: 0
					};
				};
				$scope.resetImportesTotal();
				
				$scope.importeDebeTotal2 = $.number($scope.importeDebeTotal, 2, ',', '.');
				$scope.importeHaberTotal2 = $.number($scope.importeHaberTotal, 2, ',', '.');
				$scope.importePyGTotal2 = $.number($scope.importePyGTotal, 2, ',', '.');
				

				/***************************************************
				 * ** INICIALIZACION DE DATOS
				 **************************************************/
					

				$scope.showFormularioCargaFicheroTLM = false;

				$scope.informacionTLM = {
					ultimoArchivo : "",
					fechaBooking : "",
					informacionExistente : false
				}

				$scope.operadoresFiltro = [ "=", "≠", "~", "≥", "≤" ];
				
				$scope.resetForm = function(){
					$scope.form = {
						tiposDeMovimientosFilter: "",
						tipoMovimiento : "",
						auxiliaresFilter: [],
						auxiliar : "",
						conceptosMovimientosFilter: "",
						conceptoMovimiento : "",
						comentariosFilter: "",
						comentario : "",
						numerosDocumentosFilter: "",
						numeroDocumento : "",
						antiguedadesFilter : []
					}
				};

				$scope.resetFiltro = function() {
					$scope.mostrarErrorFiltroFecha = false;
					$scope.filtro = {
						tiposMovimientos : [],
						auxiliares : [],
						conceptosMovimientos : [],
						numerosDocumentos : [],
						comentarios : [],
						bookingDateDesde : '',
						bookingDateHasta : '',
						valueDateDesde : '',
						valueDateHasta : '',
						importeDesde : null,
						importeHasta : null,
						antiguedades : [],
						gin : ''
					}
					
					$scope.resetForm();
					
					$scope.sentidoFiltro = [];
					$scope.sentidoFiltro['numeroDocumento'] = "=";
					$scope.sentidoFiltro['comentario'] = "=";
					$scope.sentidoFiltro['conceptoMovimiento'] = "=";
					
					$scope.resultados = [];

					$scope.seleccionados = [];
					$scope.seleccionadosBorrar = [];
					$scope.modalErroresFicheroTLM = {};
					
					
					$scope.safeApply();
				};

				$scope.resetFiltro();
				
				$scope.ficheroTLM = null;
				$scope.ficheroTLMCopy = null;
				
				var enmascararOperadores = function(operador) {
					var mascara = null;
					switch (operador) {
					case '=':
						mascara = 'IGUAL';
						break;
					case '≠':
						mascara = 'DISTINTO';
						break;
					case '~':
						mascara = 'CONTIENE';
						break;
					case '≥':
						mascara = 'COMIENZA';
						break;
					case '≤':
						mascara = 'FINALIZA';
						break;
					default:
						break;
					}
					return mascara;
				}
				
				 $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col ) {
					  return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
					    return $('input', td).prop('checked') ? '1' : '0';
					  })
					};
							
				/***************************************************
				 * ** DEFINICION TABLA **
				 **************************************************/
				$scope.createOTable = function() {
					$scope.oTable = $("#tblDatos").dataTable({"dom" : 'T<"clear">lfrtip', "tableTools" : {"aButtons" : [ "print" ]},
								"aoColumns" : [ {sClass : "centrar", width : "30px", orderDataType: "dom-checkbox"}, 
								                {width : "70px"}, {width : "50px"},{width : "200px"},{width : "70px"},{width : "100px", sClass : "monedaR",	type : "formatted-num"},
								                {width : "200px"},{width : "10px"},{width : "10px", sType : "date-eu"},{width : "10px", sType : "date-eu"},{width : "140px"}],

								"fnCreatedRow" : function(nRow,aData, iDataIndex) {
									$compile(nRow)($scope);
								},
								"scrollX" : "100%",
								"scrollCollapse" : true,
								"language" : {
									"url" : "i18n/Spanish.json"
								}
							});
				};
							
							
				/***************************************************
				 * ** CARGA DE DATOS INICIALES
				 **************************************************/

				$scope.getInformacionTLM = function(accion){
					if($rootScope.isAuthenticated()){
						ConciliacionBancariaService.getInformacionTLM(function(data) {
	
							$scope.accion = accion;
							
							$scope.informacionTLM = data.resultados.informacionTLM;
							if ($scope.informacionTLM && !$scope.informacionTLM.informacionExistente) {
								$scope.actualizarInformacionTLM();
							} else {
								angular.element('#archivoTLM').modal({backdrop : 'static'});
							}	
						},
						function(error) {
							$.unblockUI();
							fErrorTxt("Se produjo un error en la carga de la pantalla.",1);
						});
					}
				};
				
				$("#selecFichero").change(function(event) {
					event.preventDefault();
					$scope.ficheroTLM = event.target.files[0];
					$scope.cargarFichero(false);
				});
				
				$scope.getInformacionTLM(true); 
				
				$scope.continuarInformacionActualTLM = function(){
					$scope.verAreaIzquierda = true;
					$scope.split = Split(['#left-panel','#right-panel' ],{sizes : [50,50 ]});
					if ($scope.oTable !== undefined && $scope.oTable !== null) {
						$scope.oTable.fnDestroy();
					}
					$scope.createOTable();
					$scope.catalogoNumeroDocumento();
					$scope.catalogoComentarios();
					$scope.catalogoConceptoMovimiento();
					$scope.catalogoAuxiliares();
					$scope.catalogoTiposMovimiento();
					$scope.cargarAntiguedades();
					$scope.showingFilter = true;
					
				};
							
			   /***************************************************
			    *			FUNCIONES COMBOS 					   * 
			    **************************************************/
				
				$scope.listaTiposMovimiento = [];
				$scope.listaAuxiliares = [];
				$scope.listaConceptoMov = [];
				$scope.listaNrosDocumento = [];
				$scope.listaComentarios = [];
				
				$scope.populateAutocomplete = function (input, availableTags) {
                    $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {

                         var option = {
                           key : ui.item.key,
                           value : ui.item.value
                         };

                         switch (input) {
                           case "#filtroConceptoMovimiento":
	                             var existe = false;
	                             angular.forEach($scope.filtro.conceptosMovimientos, function (campo) {
	                               if (campo.value === option.value) {
	                                 existe = true;
	                               }
	                             });
	                             $scope.form.conceptoMovimiento = "";
	                             if (!existe) {
	                               option = {key: option.key, value: option.value, oper: enmascararOperadores($scope.sentidoFiltro['conceptoMovimiento'])};
	                               $scope.filtro.conceptosMovimientos.push(option);
	                             }
	                             break;
                           case "#filtroNumeroDocumento":
	                             var existe = false;
	                             angular.forEach($scope.filtro.numerosDocumentos, function (campo) {
	                               if (campo.value === option.value) {
	                                 existe = true;
	                               }
	                             });
	                             $scope.form.numeroDocumento = "";
	                             if (!existe) {
	                               option = {key: option.key, value: option.value, oper: enmascararOperadores($scope.sentidoFiltro['numeroDocumento'])};
	                               $scope.filtro.numerosDocumentos.push(option);
	                             }
	                             break;
                           case "#filtroComentario":
	                             var existe = false;
	                             angular.forEach($scope.filtro.comentarios, function (campo) {
	                               if (campo.value === option.value) {
	                                 existe = true;
	                               }
	                             });
	                             $scope.form.comentario = "";
	                             if (!existe) {
	                               option = {key: option.key, value: option.value, oper: enmascararOperadores($scope.sentidoFiltro['comentario'])};
	                               $scope.filtro.comentarios.push(option);
	                             }
	                             break;
	                             
                           case "#filtroTipoMovimiento":
	                             var existe = false;
	                             angular.forEach($scope.filtro.tiposMovimientos, function (campo) {
	                               if (campo.value === option.value) {
	                                 existe = true;
	                               }
	                             });
	                             $scope.form.tipoMovimiento = "";
	                             if (!existe) {
	                               $scope.filtro.tiposMovimientos.push(option);
	                             }
	                             break;
                           case "#filtroAuxiliares":
	                             var existe = false;
	                             angular.forEach($scope.filtro.auxiliares, function (campo) {
	                               if (campo.value === option.value) {
	                                 existe = true;
	                               }
	                             });
	                             $scope.form.auxiliar = "";
	                             if (!existe) {
	                               $scope.filtro.auxiliares.push(option);
	                             }
	                             break;
                           default:
                             break;
                         }

                         $scope.safeApply();

                         return false;
                       }
                    });
				};
				
				
				$scope.anadirALista= function(input){

                     switch (input) {
                       case "filtroConceptoMovimiento":
                             var existe = false;
                             var option = {key : $scope.form.conceptoMovimiento, value : $scope.form.conceptoMovimiento, oper: enmascararOperadores($scope.sentidoFiltro['conceptoMovimiento'])};
                             angular.forEach($scope.filtro.conceptosMovimientos, function (campo) {
                               if (campo.value === option.value) {
                                 existe = true;
                               }
                             });
                             $scope.form.conceptoMovimiento = "";
                             if (!existe) {
                               $scope.filtro.conceptosMovimientos.push(option);
                             }
                             break;
                       case "filtroNumeroDocumento":
                             var existe = false;
                             var option = {key : $scope.form.numeroDocumento, value : $scope.form.numeroDocumento, oper: enmascararOperadores($scope.sentidoFiltro['numeroDocumento'])};
                             angular.forEach($scope.filtro.numerosDocumentos, function (campo) {
                               if (campo.value === option.value) {
                                 existe = true;
                               }
                             });
                             $scope.form.numeroDocumento = "";
                             if (!existe) {
                               $scope.filtro.numerosDocumentos.push(option);
                             }
                             break;
                       case "filtroComentario":
                             var existe = false;
                             var option = {key : $scope.form.comentario, value : $scope.form.comentario, oper: enmascararOperadores($scope.sentidoFiltro['comentario'])};
                             angular.forEach($scope.filtro.comentarios, function (campo) {
                               if (campo.value === option.value) {
                                 existe = true;
                               }
                             });
                             $scope.form.comentario = "";
                             if (!existe) {
                               $scope.filtro.comentarios.push(option);
                             }
                             break;
                       default:
                           break;
                       };
				};
				
				$scope.catalogoTiposMovimiento = function(){
					ConciliacionBancariaCombosService.consultarTiposDeMovimiento(function(data) {
						if (data.resultados.status == 'KO') {
							fErrorTxt("Se produjo un error en la carga del combo de tipos de movimiento", 1);
						} else {
							$scope.listaTiposMovimiento = data.resultados.listaTiposMovimiento;
							$scope.populateAutocomplete("#filtroTipoMovimiento", $scope.listaTiposMovimiento);
						}
					},
					function(error) {
						fErrorTxt("Se produjo un error en la carga del combo de tipos de movimiento",1);
					});
				};

				
				$scope.catalogoAuxiliares = function(){
					ConciliacionBancariaCombosService.consultarAuxiliares(function(data) {
						if (data.resultados.status == 'KO') {
							fErrorTxt("Se produjo un error en la carga del combo de auxiliares", 1);
						} else {
							$scope.listaAuxiliares = data.resultados.listaAuxiliaresBancario;
							// $scope.populateAutocomplete("#filtroAuxiliares", $scope.listaAuxiliares);
						}
					},
					function(error) {
						fErrorTxt("Se produjo un error en la carga del combo de auxiliares",1);
					});
				};

				$scope.catalogoConceptoMovimiento = function(){
					ConciliacionBancariaCombosService.consultarConceptoMovimiento(function(data) {
						if (data.resultados.status == 'KO') {
							fErrorTxt("Se produjo un error en la carga del combo concepto movimiento", 1);
						} else {
							$scope.listaConceptoMov = data.resultados.listaConceptoMov;
							$scope.populateAutocomplete("#filtroConceptoMovimiento", $scope.listaConceptoMov);
						}
					},
					function(error) {
						fErrorTxt("Se produjo un error en la carga del combo concepto movimiento",1);
					});
				};


				$scope.catalogoNumeroDocumento = function(){
					ConciliacionBancariaCombosService.consultarNumerosDocumento(function(data) {
						if (data.resultados.status == 'KO') {
							fErrorTxt("Se produjo un error en la carga del combo numero documento", 1);
						} else {
							$scope.listaNrosDocumento = data.resultados.listaNrosDocumento;
							$scope.populateAutocomplete("#filtroNumeroDocumento", $scope.listaNrosDocumento);
						}
					},
					function(error) {
						fErrorTxt("Se produjo un error en la carga del combo numero documento",1);
					});
				};


				$scope.catalogoComentarios = function(){
					ConciliacionBancariaCombosService.consultarComentarios(function(data) {
						if (data.resultados.status == 'KO') {
							fErrorTxt("Se produjo un error en la carga del combo comentarios", 1);
						} else {
							$scope.listaComentarios = data.resultados.listaComentarios;
							$scope.populateAutocomplete("#filtroComentario", $scope.listaComentarios);
						}
					},
					function(error) {
						fErrorTxt("Se produjo un error en la carga del combo comentarios",1);
					});
				};

				$scope.cargarAntiguedades = function () {
					ConciliacionBancariaCombosService.antiguedades(function (data) {
						if (data.resultados.status == 'KO') {
							fErrorTxt("Se produjo un error en la carga del combo de Antiguedades", 1);
						} else {
							$scope.listAntiguedades = data.resultados["listaAntiguedades"];
						}
					}, function (error) {
						fErrorTxt("Se produjo un error en la carga del combo de Antiguedades", 1);
					});
				};
							
                $scope.vaciarTabla = function (tabla) {
                   switch (tabla) {
                     case "conceptosMovimientos":
                       $scope.filtro.conceptosMovimientos = [];
                       break;
                     case "numerosDocumentos":
                    	 $scope.filtro.numerosDocumentos = [];
                       break;
                     case "comentarios":
                       $scope.filtro.comentarios = [];
                       break;
                     case "tiposMovimientos":
                           $scope.filtro.tiposMovimientos = [];
                           break;
                     case "auxiliares":
                           $scope.filtro.auxiliares = [];
                           break;
                     case "antiguedades":
                         $scope.filtro.antiguedades = [];
                         break;	
                     default:
                       break;
                   }
                };
			                
                $scope.eliminarElementoTabla = function (tabla) {
                     switch (tabla) {
                       case "conceptosMovimientos":
                         for (var i = 0; i < $scope.filtro.conceptosMovimientos.length; i++) {
                           if ($scope.filtro.conceptosMovimientos[i].value === $scope.form.conceptosMovimientosFilter) {
                        	   $scope.filtro.conceptosMovimientos.splice(i, 1);
                           }
                         }
                         break;
                       case "numerosDocumentos":
                         for (var i = 0; i < $scope.filtro.numerosDocumentos.length; i++) {
                           if ($scope.filtro.numerosDocumentos[i].value === $scope.form.numerosDocumentosFilter) {
                        	   $scope.filtro.numerosDocumentos.splice(i, 1);
                           }
                         }
                         break;
                       case "tiposMovimientos":
                         for (var i = 0; i < $scope.filtro.tiposMovimientos.length; i++) {
                           if ( $scope.filtro.tiposMovimientos[i].value ===  $scope.form.tiposDeMovimientosFilter) {
                        	   $scope.filtro.tiposMovimientos.splice(i, 1);
                           }
                         }
                         break;
                       case "comentarios":
	                         for (var i = 0; i < $scope.filtro.comentarios.length; i++) {
	                           if ( $scope.filtro.comentarios[i].value ===  $scope.form.comentariosFilter) {
	                        	   $scope.filtro.comentarios.splice(i, 1);
	                           }
	                         }
	                         break;		
                       case "auxiliares":
	                         for (var i = 0; i < $scope.filtro.auxiliares.length; i++) {
	                           if ( $scope.filtro.auxiliares[i].value ===  $scope.form.auxiliaresFilter) {
	                        	   $scope.filtro.auxiliares.splice(i, 1);
	                           }
	                         }
	                         break;	
                       case "antiguedades":
                           for (var i = 0; i < $scope.filtro.antiguedades.length; i++) {
                             if ( $scope.filtro.antiguedades[i] ===  $scope.form.antiguedadesFilter) {
                          	   $scope.filtro.antiguedades.splice(i, 1);
                             }
                           }
                           break;
                       default:
                         break;
                     }
                };
							

				/***************************************************
				 * ** FUNCIONES GENERALES
				 **************************************************/
            	$scope.seguimientoBusqueda = function() {
            	    $('.mensajeBusqueda').empty();
            	    var cadenaFiltros = "";
            	    if ($scope.filtro.tiposMovimientos !== null && $scope.filtro.tiposMovimientos.length > 0) {
            	    	cadenaFiltros += " Tipo de movimiento: ";
            	    	for (var i = 0; i < $scope.filtro.tiposMovimientos.length; i++) {
            	    		cadenaFiltros += $scope.filtro.tiposMovimientos[i].value;
            	    		if (i < ($scope.filtro.tiposMovimientos.length -1)) {
            	    			cadenaFiltros += ", ";
            	    		}
                        }
            	    }
            	    if ($scope.filtro.auxiliares !== null && $scope.filtro.auxiliares.length > 0) {
            	    	cadenaFiltros += " Auxiliar bancario: ";
            	    	for (var i = 0; i < $scope.filtro.auxiliares.length; i++) {
            	    		cadenaFiltros += $scope.filtro.auxiliares[i].value;
            	    		if (i < ($scope.filtro.auxiliares.length -1)) {
            	    			cadenaFiltros += ", ";
            	    		}
                        }
            	    }
            	    if ($scope.filtro.conceptosMovimientos !== null && $scope.filtro.conceptosMovimientos.length > 0) {
            	    	cadenaFiltros += " Concepto Movimiento: ";
            	    	for (var i = 0; i < $scope.filtro.conceptosMovimientos.length; i++) {
            	    		cadenaFiltros += $scope.filtro.conceptosMovimientos[i].value;
            	    		if (i < ($scope.filtro.conceptosMovimientos.length -1)) {
            	    			cadenaFiltros += ", ";
            	    		}
                        }
            	    }
            	    if ($scope.filtro.comentarios !== null && $scope.filtro.comentarios.length > 0) {
            	    	cadenaFiltros += " Comentario: ";
            	    	for (var i = 0; i < $scope.filtro.comentarios.length; i++) {
            	    		cadenaFiltros += $scope.filtro.comentarios[i].value;
            	    		if (i < ($scope.filtro.comentarios.length -1)) {
            	    			cadenaFiltros += ", ";
            	    		}
                        }
            	    }
            	    if ($scope.filtro.numerosDocumentos !== null && $scope.filtro.numerosDocumentos.length > 0) {
            	    	cadenaFiltros += " Nro. de Documento: ";
            	    	for (var i = 0; i < $scope.filtro.numerosDocumentos.length; i++) {
            	    		cadenaFiltros += $scope.filtro.numerosDocumentos[i].value;
            	    		if (i < ($scope.filtro.numerosDocumentos.length -1)) {
            	    			cadenaFiltros += ", ";
            	    		}
                        }
            	    }
            	    if ($scope.filtro.antiguedades !== null && $scope.filtro.antiguedades.length > 0) {
            	    	cadenaFiltros += " Antiguedad: ";
            	    	for (var i = 0; i < $scope.filtro.antiguedades.length; i++) {
            	    		cadenaFiltros += $scope.filtro.antiguedades[i];
            	    		if (i < ($scope.filtro.antiguedades.length -1)) {
            	    			cadenaFiltros += ", ";
            	    		}
                        }
            	    }
            	    if ($scope.filtro.bookingDateDesde !== null && $scope.filtro.bookingDateDesde !== "") {
            	    	cadenaFiltros += " Booking Date Desde: " + $scope.filtro.bookingDateDesde;
            	    }
            	    if ($scope.filtro.bookingDateHasta !== null && $scope.filtro.bookingDateHasta !== "") {
            	    	cadenaFiltros += " Booking Date Hasta: " + $scope.filtro.bookingDateHasta;
            	    }
            	    if ($scope.filtro.valueDateDesde !== null && $scope.filtro.valueDateDesde !== "") {
            	    	cadenaFiltros += " Value Date Desde: " + $scope.filtro.valueDateDesde;
            	    }
            	    if ($scope.filtro.valueDateHasta !== null && $scope.filtro.valueDateHasta !== "") {
            	    	cadenaFiltros += " Value Date Hasta: " + $scope.filtro.valueDateHasta;
            	    }
            	    if ($scope.filtro.importeDesde !== null && $scope.filtro.importeDesde !== "") {
            	    	cadenaFiltros += " Importe Desde: " + $scope.filtro.importeDesde;
            	    }
            	    if ($scope.filtro.importeHasta !== null && $scope.filtro.importeHasta !== "") {
            	    	cadenaFiltros += " Importe Hasta: " + $scope.filtro.importeHasta;
            	    }
            	    if ($scope.filtro.gin !== null && $scope.filtro.gin !== "") {
            	    	cadenaFiltros += " GIN: " + $scope.filtro.gin;
            	    }

            	    $('.mensajeBusqueda').append(cadenaFiltros);
            	};

				 $scope.busquedaConciliacion = function(){
//					 $scope.mostrarErrorFiltroFecha = $scope.validacionFiltroFecha();
					 if (!$scope.mostrarErrorFiltroFecha) {
						 $scope.showingFilter = false;
					 	inicializarLoading();
						ConciliacionBancariaService.busqueda(function(data) {
							
							if (data == null || data === "undefined") {
								fErrorTxt("Se produjo un error en la carga del listado.",1);
								return false;
							}else{ 
								if (typeof data === 'string') {
							
									fErrorTxt("Se produjo un error en la carga del listado.",1);
									return false;
								}else{
									$scope.resultados = data.resultados.resultados;

									// se inicializan las variables de clientes
									// seleccionados
									$scope.seleccionados = [];
									$scope.seleccionadosBorrar = [];

									// borra el contenido del body de la tabla
									var tbl = $("#tblDatos > tbody");
									$(tbl).html("");
									$scope.oTable.fnClearTable();

									if ($scope.resultados.length == 0) {

										$scope.isResultados = false;

									} else {

										$scope.isResultados = true;

										for (var i = 0; i < $scope.resultados.length; i++) {

											var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="resultados['+ 
											i + '].selected" ng-click="seleccionarElemento(' + i + ');" ng-disabled="(isSeleccionado(resultados['+ 
											i + ']) && rightView.TLMView ) || (tipoDeMovimientoACCOUR && tipoDeMovimientoACCOUR!=resultados['+ 
											i + '].tipoDeMovimiento.trim() && rightView.OURView) || (tipoDeMovimientoACCOUR && tipoDeMovimientoACCOUR!=resultados['+ 
											i + '].tipoDeMovimiento.trim() && rightView.ACCView)"/>'; 

											var fila = [
													check,
													$scope.resultados[i].gin,
													$scope.resultados[i].tipoDeMovimiento.trim(),
													$scope.resultados[i].auxiliarBancario,
													$scope.resultados[i].conceptoMovimiento,
													$.number($scope.resultados[i].importe, 2, ',', '.'),
													$scope.resultados[i].numeroDocumento,
													$scope.resultados[i].comentario,
													moment($scope.resultados[i].bookingDate).format('DD/MM/YYYY'),
													moment($scope.resultados[i].valueDate).format('DD/MM/YYYY'),
													$scope.resultados[i].antiguedad];

											$scope.oTable.fnAddData(fila, false);

										}

									}
									$scope.oTable.fnDraw();
							
								}
							}
							$.unblockUI();
						},
						function(error) {
							fErrorTxt("Se produjo un error en la carga del listado de TLM.",1);
							$.unblockUI();
						}, $scope.filtro);
						$scope.seguimientoBusqueda();
				 }
				 };
				 
				 
				 
				 $scope.seleccionadosTMLDcha = [];
				 
				 $scope.isSeleccionado = function(elemento){
					 var existe = false;
					 if(elemento) {
                         angular.forEach($scope.seleccionadosTMLDcha, function (campo) {
                             if (campo.id === elemento.id) {
                                 existe = true;
                             }
                         });
                     }
					 return existe;
				 }
				 
				 var seleccionaTLMDcha = function(event, seleccionadosDcha){
					 $scope.seleccionadosTMLDcha = angular.copy(seleccionadosDcha);
				 };
				 
				 $scope.$on("seleccionaTLMDcha",seleccionaTLMDcha);
				 
				 
							
				 $scope.rotarOperadorFiltroConceptoMovimiento = function() {
					var i = $scope.operadoresFiltro.indexOf($scope.sentidoFiltro['conceptoMovimiento']);
					if (i == $scope.operadoresFiltro.length - 1) {
						$scope.sentidoFiltro['conceptoMovimiento'] = $scope.operadoresFiltro[0];
					} else {
						$scope.sentidoFiltro['conceptoMovimiento'] = $scope.operadoresFiltro[i + 1];
					}
				 }

				 $scope.rotarOperadorFiltroNumeroDocumento = function() {
					var i = $scope.operadoresFiltro.indexOf($scope.sentidoFiltro['numeroDocumento']);
					if (i == $scope.operadoresFiltro.length - 1) {
						$scope.sentidoFiltro['numeroDocumento'] = $scope.operadoresFiltro[0];
					} else {
						$scope.sentidoFiltro['numeroDocumento'] = $scope.operadoresFiltro[i + 1];
					}
				 };

				 $scope.rotarOperadorFiltroComentario = function() {
					var i = $scope.operadoresFiltro.indexOf($scope.sentidoFiltro['comentario']);
					if (i == $scope.operadoresFiltro.length - 1) {
						$scope.sentidoFiltro['comentario'] = $scope.operadoresFiltro[0];
					} else {
						$scope.sentidoFiltro['comentario'] = $scope.operadoresFiltro[i + 1];
					}
				 };

				 $scope.actualizarInformacionTLM = function() {
					angular.element('#archivoTLM').modal({
						backdrop : 'static'
					});
					$scope.showFormularioCargaFicheroTLM = true;
				 };


				 $scope.mostrarModalCarga = function() {
					 $scope.actualizarInformacionTLM();
				 };

				 $scope.cargarFichero = function(saltarValidacionBookingDate) {

					console.log('Entra en enviarFichero....');
					var file;
					if($scope.ficheroTLM){
						file = $scope.ficheroTLM;
						$scope.ficheroTLMCopy = angular.copy($scope.ficheroTLM);
					}else{
						file = $scope.ficheroTLMCopy;
					}
					var file = $scope.ficheroTLM;
					console.log('file ..: ' + file);

					if (file) {
						var reader = new FileReader();
						var btoaTxt;
						var nombre = file.name;
						var ruta = file.path;
						reader.onload = function() {
							var binary = "";
							var bytes = new Uint8Array(
									reader.result);
							var length = bytes.byteLength;

							for (var i = 0; i < length; i++) {
								binary += String.fromCharCode(bytes[i]);
							}
							btoaTxt = btoa(binary);
							importarFicheroTLM(btoaTxt, nombre, saltarValidacionBookingDate);
						};
						reader.readAsArrayBuffer(file);
					}
					angular.element('#archivoTLM').modal('hide');
					if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
						fErrorTxt('The File APIs are not fully supported in this browser.', 1);
					}
					$('#selecFichero').val('');
					inicializarLoading();

				};

				var oTableErroresFicheroTLM = null;
							
							
				/**
				 * Inicializacion de tabla para mostrar errores del
				 * fichero TLM.
				 */
				var inicializarTablaErrsFicheroTLM = function() {
					if (oTableErroresFicheroTLM == null) {
						oTableErroresFicheroTLM = $("#datosErroresFicheroTLM").dataTable({
							"dom" : '<"clear">lrtip',
							"aoColumns" : [ {width : "50px"}, {width : "50px"}, {width : "635px"} ],
							"fnCreatedRow" : function(nRow, aData,iDataIndex) {$compile(nRow)($scope);
							},
							"scrollY" : "100%",
							"language" : {
								"url" : "i18n/Spanish.json"
							}
						});
					}
				}
							
				/**
				*	Borrado de tabla Ver Errores del fichero TLM. 
				*/
				$scope.borrarTablaVerErroresTLM = function () {
					var tbl = $("#datosErroresFicheroTLM > tbody");
				    $(tbl).html("");
				    oTableErroresFicheroTLM.fnClearTable();
				};
							
				/**
				 * Se levanta modal de errores del fichero TLM
				 * 
				 * @param data
				 */
				$scope.mostrarErroresExcelTLM = function(data) {
					$scope.pintarTablaVerErroresTLM(data);
					angular.element('#erroresExcelTLM').modal({
						backdrop : 'static'
					});
				}
				
				$scope.confirmarCarga = function() {
					 angular.element("#dialog-confirm").html("El fichero contiene un fecha Booking Date igual a la ultima fecha Booking date de la base de datos. ¿Desea continuar?");
					 angular.element("#dialog-confirm").dialog({
						 resizable : false,
						 modal : true,
						 title : "Mensaje de Confirmación",
						 height : 160,
						 width : 360,
						 buttons : {
							 " Sí " : function () {
								 $scope.cargarFichero(true);
								 $(this).dialog('close');
							 }," No " : function () {
								 	$(this).dialog('close');
								 	$scope.mostrarModalCarga();
								 }
						 }
	                 });
					 $('.ui-dialog-titlebar-close').remove();
				};

				function importarFicheroTLM(Text, nombre, saltarValidacionBookingDate) {
					var filtro = {
						file : Text,
						filename : nombre,
						userName : $rootScope.userName,
						saltarValidacion : saltarValidacionBookingDate
					};
					ConciliacionBancariaService.cargaFichero(function(data) {
						if (!commonHelper.isUndefinedOrNull(data.resultadosError)
								&& data.resultadosError.erroresExcelTLM != null
								&& data.resultadosError.erroresExcelTLM.length) {
							var erroresArray = data.resultadosError.erroresExcelTLM;
							// Se muestran errores
							// del fichero TLM
							if(erroresArray.length > 0) {
								var error = erroresArray[0].descripcionError;
								if (error == 'bookingIgual') {
									$scope.confirmarCarga();
								} else {
									$scope.mostrarErroresExcelTLM(erroresArray);
									$scope.verAreaIzquierda = false;
								}
							}
						} else {
							if (!commonHelper.isUndefinedOrNull(data.resultados) && data.resultados.status == 'KO') {
								fErrorTxt("Se produjo un error en la carga del fichero.", 1);
								$scope.informacionTLM.informacionExistente = false;
								$scope.verAreaIzquierda = false;
							} else {
								fErrorTxt('Se ha finalizado la carga del fichero', 3);
								// Se exhibe panel de Busqueda TLM
								if(!$scope.informacionTLM){
                                    $scope.informacionTLM = {};
                                }
                                $scope.informacionTLM.informacionExistente = true;
								// Si la carga es satisfactoria se actualiza la informacion.
								$scope.continuarInformacionActualTLM();
							}
						}
						$.unblockUI();
					},
					function(error) {
						fErrorTxt("Se produjo un error en la carga del fichero.", 1);
                        if(!$scope.informacionTLM){
                           $scope.informacionTLM = {};
                        }
                        $scope.informacionTLM.informacionExistente = false;
						$scope.verAreaIzquierda = false;
						$.unblockUI();
					}, filtro);
				};
							
							
				/**
				 * Se pinta el datatable para mostrar errores fichero TLM.
				 * 
				 * @param data
				 */
					// borra el contenido del body de la tabla
				$scope.pintarTablaVerErroresTLM = function(data) {
					if (oTableErroresFicheroTLM != null) {
						$scope.borrarTablaVerErroresTLM();
					}

					var listErrores = [];

					// Se pasa resultado del servicio al scope
					$scope.modalErroresFicheroTLM.listaErroresFicheroTLM = data;

					// Se inicializa tabla
					inicializarTablaErrsFicheroTLM();

					// Se popula tabla
					if ($scope.modalErroresFicheroTLM.listaErroresFicheroTLM.length > 0) {
						for (var i = 0; i < $scope.modalErroresFicheroTLM.listaErroresFicheroTLM.length; i++) {
							listErrores[i] = [
									$scope.modalErroresFicheroTLM.listaErroresFicheroTLM[i].nroColumna,
									$scope.modalErroresFicheroTLM.listaErroresFicheroTLM[i].nroFila,
									$scope.modalErroresFicheroTLM.listaErroresFicheroTLM[i].descripcionError ];
						}
						// Se pintan datos de la tabla
						oTableErroresFicheroTLM.fnAddData(
								listErrores, false);
						oTableErroresFicheroTLM.fnDraw();
					}
				}



				/***************************************************
				 * ** ACCIONES TABLA ***
				 **************************************************/

				$scope.seleccionarTodos = function() {
					// Se inicializa los elementos que ya tuviera la
					// lista de elementos a borrar.
					$scope.seleccionadosBorrar = [];
					$scope.seleccionados = [];
					if(!$scope.rightView.ACCView){
						if(!$scope.rightView.TLMView){
							for (var i = 0; i < $scope.resultados.length; i++) {
									$scope.resultados[i].selected = true;
									$scope.seleccionadosBorrar.push($scope.resultados[i].idInforme);
							}
							$scope.seleccionados = angular.copy($scope.resultados);
						}else{
							for (var i = 0; i < $scope.resultados.length; i++) {
								if(!$scope.isSeleccionado($scope.resultados[i])){
									$scope.resultados[i].selected = true;
									$scope.seleccionadosBorrar.push($scope.resultados[i].idInforme);
									$scope.seleccionados.push($scope.resultados[i]);
								}
							}
						}
					}else{
						for (var i = 0; i < $scope.resultados.length; i++) {
							if(!$scope.tipoDeMovimientoACCOUR || $scope.resultados[i].tipoDeMovimiento.trim() == $scope.tipoDeMovimientoACCOUR){
								$scope.resultados[i].selected = true;
								$scope.seleccionadosBorrar.push($scope.resultados[i].idInforme);
								$scope.seleccionados.push($scope.resultados[i]);
							}
						}
					}
				};

				$scope.deseleccionarTodos = function() {
					for (var i = 0; i < $scope.resultados.length; i++) {
						$scope.resultados[i].selected = false;
					}
					$scope.seleccionados = [];
					$scope.seleccionadosBorrar = [];
					$scope.tipoDeMovimientoACCOUR = null;
				};

				$scope.seleccionarElemento = function(row) {
					if ($scope.resultados[row] != null && $scope.resultados[row].selected) {
						$scope.resultados[row].selected = false;
						for (var i = 0; i < $scope.seleccionados.length; i++) {
							if ($scope.seleccionados[i].id === $scope.resultados[row].id) {
								$scope.seleccionados.splice(i, 1);
								$scope.seleccionadosBorrar.splice(i, 1);
							}
						}
						if(!$scope.seleccionados.length){
							$scope.tipoDeMovimientoACCOUR = null;
						}
					} else {
						$scope.tipoDeMovimientoACCOUR = $scope.resultados[row].tipoDeMovimiento.trim();
						$scope.resultados[row].selected = true;
						$scope.seleccionados.push($scope.resultados[row]);
						$scope.seleccionadosBorrar.push($scope.resultados[row].id);
					}
					
					if($scope.rightView.OURView){
						$scope.$broadcast("setListaAuxiliaresOUR",$scope.seleccionados);
					}
					$scope.safeApply();
				};

						
				/***************************************************
				 * * ACCIONES BOTONERA COMÚN **
				 **************************************************/

				$scope.rightView = {
					OURView : false,
					TLMView : false,
					ACCView : false
				};
				
				var calcularImporteTotal = function(){
                	$scope.apunteTotal.importeDebeTotal = $scope.importeDebeIzq + $scope.importeDebeDcha;
				 	$scope.apunteTotal.importeHaberTotal = ($scope.importeHaberIzq + $scope.importeHaberDcha);
                    if($scope.apunteTotal.importeHaberTotal<0){
                        $scope.apunteTotal.importeHaberTotal *=-1;
                    }
				 	$scope.apunteTotal.importePyGTotal =  - ($scope.apunteTotal.importeDebeTotal - $scope.apunteTotal.importeHaberTotal);
				 	$scope.importeDebeTotal2 = $.number($scope.apunteTotal.importeDebeTotal, 2, ',', '.');
					$scope.importeHaberTotal2 = $.number($scope.apunteTotal.importeHaberTotal, 2, ',', '.');
					$scope.importePyGTotal2 = $.number($scope.apunteTotal.importePyGTotal, 2, ',', '.');
                };

				$scope.showView = function(vista) {

					switch (vista) {

					case "TLM":	if($scope.seleccionados.length>0){
									$scope.rightView = {
										OURView : false,
										TLMView : true,
										ACCView : false
									};
									
									$scope.resetImportesIzq();
		    						
		    					 	angular.forEach($scope.seleccionados, function (partida) {
		    					 		if(partida.tipoDeMovimiento.trim() == Constantes.THEIR_CASH_CREDIT ||
		    					 				partida.tipoDeMovimiento.trim() == Constantes.OUR_CASH_DEBIT){
		    					 			$scope.importeDebeIzq = $scope.importeDebeIzq + partida.importe;
		    					 		}
		    					 		if(partida.tipoDeMovimiento.trim() == Constantes.OUR_CASH_CREDIT ||
		    					 				partida.tipoDeMovimiento.trim() == Constantes.THEIR_CASH_DEBIT){
		    					 			$scope.importeHaberIzq = $scope.importeHaberIzq + partida.importe;
		    					 		}
		    	                    });
		    					 	$scope.importePyGIzq = $scope.importeDebeIzq - $scope.importeHaberIzq;

		    					 	calcularImporteTotal();
		    					 	
									angular.element('#generarApunteModal').modal({
				                         backdrop : 'static'
			                        });
			                        $(".modal-backdrop").remove();
				                    $("#generarApunteModal").draggable();
				                    $("#generarApunteModal").removeClass("modalGenerarApunteGrande");
				                    $("#generarApunteModal").addClass("modalGenerarApunte"); 
				                    $scope.activeConcept = false;
				                    
									$scope.$broadcast("abreTLM");
								}else{
									fErrorTxt("Debe seleccionar al menos una partida pendiente del area izquierda de la pantalla.", 2);
								}
								break;
								
					case "ACC": angular.element('#generarApunteModal').modal('hide');
								if($scope.seleccionados.length>0){
									var mismoTipoMov = false;
									var tipoMov = $scope.seleccionados[0].tipoDeMovimiento.trim();
									var importe = 0;
								 	angular.forEach($scope.seleccionados, function (partida) {
				                        if (tipoMov != partida.tipoDeMovimiento.trim() && !mismoTipoMov) {
				                        	mismoTipoMov = true;
				                        }
				                        importe = importe + partida.importe;
				                     });
				                    if(!mismoTipoMov){
				                    	$scope.seleccionadosTMLDcha = [];
				                    	$scope.tipoDeMovimientoACCOUR = tipoMov;
										$scope.rightView = {
											OURView : false,
											TLMView : false,
											ACCView : true
										};
										
										$scope.$broadcast("abreACC", importe, $scope.seleccionados);
				                    }else{
				                    	fErrorTxt("El tipo de movimiento en todas partidas seleccionadas del area izquierda no es el mismo.", 2);
				                    }
								}else{
									fErrorTxt("Debe seleccionar al menos una partida pendiente del area izquierda de la pantalla.", 2);
								}
			                    
								break;
						
					case "OUR":	if($scope.seleccionados.length>0){
									var mismoTipoMov = false;
									var tipoMov = $scope.seleccionados[0].tipoDeMovimiento.trim();
								 	angular.forEach($scope.seleccionados, function (partida) {
				                        if (tipoMov != partida.tipoDeMovimiento.trim() && !mismoTipoMov) {
				                        	mismoTipoMov = true;
				                        }
				                     });
								 	
				                    if(!mismoTipoMov){
				                    	$scope.seleccionadosTMLDcha = [];
				                    	$scope.tipoDeMovimientoACCOUR = tipoMov;
										$scope.rightView = {
											OURView : true,
											TLMView : false,
											ACCView : false
										};
										$scope.resetImportesIzq();
										angular.forEach($scope.seleccionados, function (partida) {
			    					 		if(partida.tipoDeMovimiento.trim() == Constantes.THEIR_CASH_CREDIT  ||
			    					 				partida.tipoDeMovimiento.trim() == Constantes.OUR_CASH_DEBIT){
			    					 			$scope.importeDebeIzq = $scope.importeDebeIzq + partida.importe;
			    					 		}
			    					 		if(partida.tipoDeMovimiento.trim() == Constantes.OUR_CASH_CREDIT ||
			    					 				partida.tipoDeMovimiento.trim() == Constantes.THEIR_CASH_DEBIT){
			    					 			$scope.importeHaberIzq = $scope.importeHaberIzq + partida.importe;
			    					 			
			    					 		}
										});
										
									 	$scope.importePyGIzq = $scope.importeDebeIzq - $scope.importeHaberIzq ;
			    					 	calcularImporteTotal();
			    					 	
										angular.element('#generarApunteModal').modal({
					                         backdrop : 'static'
				                        });
				                        $(".modal-backdrop").remove();
					                    $("#generarApunteModal").draggable();
					                    $("#generarApunteModal").removeClass("modalGenerarApunteGrande");
					                    $("#generarApunteModal").addClass("modalGenerarApunte");
					                    $scope.activeConcept = false;
					                    
				                    
										$scope.$broadcast("abreOUR", $scope.seleccionados);
				                    }else{
				                    	fErrorTxt("El tipo de movimiento en todas partidas seleccionadas del area izquierda no es el mismo.", 2);
				                    }
								}else{
									fErrorTxt("Debe seleccionar al menos una partida pendiente del area izquierda de la pantalla.", 2);
								}
								break;
						
					default:
						break;
					}
				};
							

				/***************************************************
				 * * ACCIONES BOTONERA ACC **
				 **************************************************/
				
                $scope.$watch('seleccionados', function (newVal, oldVal) {
                    if (newVal !== oldVal && newVal) {
                    	
                    	if($scope.rightView.ACCView){
	                    	var importe = 0;
	    					for(var i=0;i<$scope.seleccionados.length;i++){
	    						importe = importe + $scope.seleccionados[i].importe;
	    					}
	                    	$scope.$broadcast("calculaImporteACC", importe);
                    	}
                    	if($scope.rightView.OURView || $scope.rightView.TLMView){
                    		
                    		$scope.resetImportesIzq();
                    		
    					 	angular.forEach($scope.seleccionados, function (partida) {
                                if(partida.tipoDeMovimiento.trim() == Constantes.THEIR_CASH_CREDIT  ||
                                    partida.tipoDeMovimiento.trim() == Constantes.OUR_CASH_DEBIT){
    					 			$scope.importeDebeIzq = $scope.importeDebeIzq + partida.importe;
    					 		}
                                if(partida.tipoDeMovimiento.trim() == Constantes.OUR_CASH_CREDIT ||
                                    partida.tipoDeMovimiento.trim() == Constantes.THEIR_CASH_DEBIT){
    					 			$scope.importeHaberIzq = $scope.importeHaberIzq + partida.importe;
    					 		}
    	                    });
    					 	$scope.importePyGIzq = $scope.importeDebeIzq - $scope.importeHaberIzq ;
    					 
    					 	calcularImporteTotal();
                    	}
					 	
                    }
                  }, true);
                
                var calculaImporte = function(event, importeDebe, importeHaber, importePyG){
                	$scope.importeDebeDcha = importeDebe;
				 	$scope.importeHaberDcha = importeHaber;
				 	$scope.importePyGDcha = importePyG;
				 	calcularImporteTotal();
                };
                
                $scope.$on("calculaImporte", calculaImporte);
                

				$scope.vaciarTablaApuntesACC = function() {
					var importe = 0;
					for(var i=0;i<$scope.seleccionados.length;i++){
						importe = importe + $scope.seleccionados[i].importe;
					}
					$scope.$broadcast("abreACC", importe, "");
				};

				$scope.eliminarApunteACC = function() {
					$scope.$broadcast("eliminarApunteACC");
				};
				
				$scope.generarApunteACC = function() {
					$scope.$broadcast("generarApunteACC", $scope.seleccionados);
				};
							
				/***************************************************
				 * * ACCIONES BOTONERA TLM **
				 **************************************************/
				
				$scope.seleccionaTodosTLM = function() {
					$scope.$broadcast("seleccionarTodosTLM");
				};

				$scope.deseleccionaTodosTLM = function() {
					$scope.$broadcast("deseleccionarTodosTLM");
				};
				
				$scope.generarApunteTLM = function() {
					if($scope.activeConcept && $scope.conceptoTLMInput){
						 $scope.activeConcept = false;
						 if($scope.conceptoTLMInput != $scope.conceptoTLM.value){
							$scope.conceptoTLM = {key: "", value: $scope.conceptoTLMInput};
						 }
						 $scope.$broadcast("generarApunteTLM", $scope.seleccionados, $scope.conceptoTLM);
						 $(this).dialog('close');
					}else{
						$("#generarApunteModal").removeClass("modalGenerarApunte");
						$("#generarApunteModal").addClass("modalGenerarApunteGrande");
						$scope.consultarConceptos();
						$scope.conceptoTLM = "";
						$scope.conceptoTLMInput = "";
						$scope.activeConcept = true;
					}
				};
				
				/**
				 *	Accion de clic sobre el boton cancelar
				 *	de la ventana flotante de Generar Apunte. 
				 */
				$scope.cancelarGenerarApunte = function(){
					$scope.rightView.OURView = false;
					$scope.rightView.TLMView = false;
			    };
			    
			    $scope.asignarValueInputTLM = function(){
			    	$scope.conceptoTLMInput = $scope.conceptoTLM.value;
			    };
			    
			    $scope.resetConceptoTLM = function(){
			    	$scope.conceptoTLM = "";
			    };
				
			    $scope.consultarConceptos = function () {
				   ConciliacionBancariaCombosService.conceptos(function (data) {
					   $scope.listaConceptos = data.resultados["listaConceptos"];
				   }, function (error) {
					   fErrorTxt("Se produjo un error en la carga del combo de Conceptos.", 1);
				   });
			    };
				
				/***************************************************
				 * * ACCIONES BOTONERA OUR **
				 **************************************************/
				$scope.activeConcept = false;
				$scope.seleccionaTodosOUR = function() {
					$scope.$broadcast("seleccionarTodosOUR");
				};

				$scope.deseleccionaTodosOUR = function() {
					$scope.$broadcast("deseleccionarTodosOUR");
				};		
				
				$scope.generarApunteOUR = function() {
					if($scope.activeConcept && $scope.conceptoOURInput){
						$scope.activeConcept = false;
						if($scope.conceptoOURInput != $scope.conceptoOUR.value){
							$scope.conceptoOUR = {key: "",value: $scope.conceptoOURInput};
						}
						$scope.$broadcast("generarApunteOUR", $scope.seleccionados, $scope.conceptoOUR, $scope.apunteTotal);
					}else{
						$("#generarApunteModal").removeClass("modalGenerarApunte");
						$("#generarApunteModal").addClass("modalGenerarApunteGrande");
						$scope.consultarConceptos();
						$scope.conceptoOUR = "";
						$scope.conceptoOURInput = "";
						$scope.activeConcept = true;
					}
				};
				
			    $scope.asignarValueInputOUR = function(){
			    	$scope.conceptoOURInput = $scope.conceptoOUR.value;
			    }
			    
			    $scope.resetConceptoOUR = function(){
			    	$scope.conceptoOUR = "";
			    };
				
				 /** Validacion de campos del filtro de Fecha. */
				 $scope.validacionFiltroFecha = function () {
					 if (($scope.filtro.bookingDateDesde == '' && $scope.filtro.bookingDateHasta != '') || 
						 ($scope.filtro.valueDateDesde == '' && $scope.filtro.valueDateHasta != '') || 
						 ($scope.filtro.bookingDateDesde != '' && $scope.filtro.valueDateDesde != '') ||
						 ($scope.filtro.bookingDateDesde == '' && $scope.filtro.bookingDateHasta == '' && $scope.filtro.valueDateDesde == '' && $scope.filtro.valueDateHasta == '')) {
						 return true;
					 } else {
						 return false;
					 }
				 };

				 $scope.procesarTipoMovimiento = function () {
					 var existe = false;
					 angular.forEach($scope.filtro.tiposMovimientos, function (campo) {
						 if (campo.key === $scope.filtro.tipoMovimiento.key) {
							 existe = true;
						 }
					 });
					 if (!existe) {
						 $scope.filtro.tiposMovimientos.push($scope.filtro.tipoMovimiento);
					 }
					 $scope.filtro.tipoMovimiento = "";
				 };
				 
				 $scope.procesarAuxiliarBancario = function () {
					 var existe = false;
					 angular.forEach($scope.filtro.auxiliares, function (campo) {
						 if (campo === $scope.filtro.auxiliar) {
							 existe = true;
						 }
					 });
					 if (!existe) {
						 $scope.filtro.auxiliares.push($scope.filtro.auxiliar);
					 }
					 $scope.filtro.auxiliar = "";
				 };
				 
				 $scope.procesarAntiguedad = function () {
					 var existe = false;
					 angular.forEach($scope.filtro.antiguedades, function (campo) {
						 if (campo === $scope.filtro.antiguedad.key) {
							 existe = true;
						 }
					 });
					 if (!existe) {
						 $scope.filtro.antiguedades.push($scope.filtro.antiguedad.key);
					 }
					 $scope.filtro.antiguedad = "";
				 };
				 
				 $scope.recargarPagina = function () {
					 $scope.reloadPage = function(){window.location.reload();}
				 };		
				 
				 $scope.resetPantalla = function () {
					 $scope.rightView = {
						OURView : false,
						TLMView : false,
						ACCView : false
					 };
					 $scope.busquedaConciliacion();
					 
				 };
				 
				 $scope.$on("resetPantalla", $scope.resetPantalla);
				 
} ]);
