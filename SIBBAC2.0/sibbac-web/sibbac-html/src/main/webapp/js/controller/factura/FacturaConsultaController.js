sibbac20.controller('FacturaConsultaController', ['$scope', '$compile', 'AliasService', 'EstadoService', 'FacturaSugeridaService', 'growl', function ($scope, $compile, AliasService, EstadoService, FacturaSugeridaService, growl) {
        var idOcultos = [];
        var availableTags = [];
        var oTable = undefined;

        var ESTADO_SUGERIDA = 501;

        loadAlias = function () {
            AliasService.getAliasFactura(cargarAlias, showError);
        };
        loadEstadosFacturaSugerida = function () {
            EstadoService.getEstadosFacturaSugerida(cargarEstados, showError);
        };


        showError = function (data, status, headers, config) {
            growl.addErrorMessage("Error: " + data);
        };

        loadDataTable = function () {
            oTable = $("#tablaFacturasSugeridas").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    $compile(nRow)($scope);
                }
            });
        };

        $scope.submitForm = function (event) {


			if (event === null)
			{
				event.preventDefault();
			}
            var listaAlias = angular.element('#textAlias').val();
            posicionAlias = availableTags.indexOf(listaAlias);
            var listaEstados = angular.element('#textEstado').val();
            posicionAlias = availableTags.indexOf(listaAlias);
            if ((posicionAlias === "-1") && (listaAlias !== "")) {
                growl.addErrorMessage('El alias introducido no existe: ' + data);
            }
            else
            {
                collapseSearchForm();
                inicializarLoading();
                var alias = "";
                if (listaAlias === null || listaAlias === undefined || listaAlias === "") {
                    alias = "";
                } else {
                    posicionAlias = availableTags.indexOf(listaAlias);
                    alias = idOcultos[posicionAlias];
                }

                // aquí asignamos el estado
                var estado = angular.element("#textEstado").val();
                var filtro = {};
                if (alias !== "") {
                    filtro.idAlias = alias;
                }
                if (estado !== "") {
                    filtro.idEstado = estado;
                }
                FacturaSugeridaService.getListaFacturasSugeridas(cargarTabla, showError, filtro);
            }

        };

        $scope.limpiarFiltro = function () {
            angular.element('input[type=text]').val('');
            angular.element('select').val('');
        };

        $scope.deleteConfirm = function (id) {
            angular.element("#dialog-confirm").html("¿Eliminar Factura?");

            // Define the Dialog and its properties.
            angular.element("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                title: "Mensaje de Confirmación",
                height: 120,
                width: 240,
                buttons: {
                    "Sí": function () {
                        $(this).dialog('close');
                        deleteFacturaSugerida(id);
                    },
                    "No": function () {
                        $(this).dialog('close');
                    }
                }
            });
        };
        $(document).ready(function () {
            loadDataTable();
            loadAlias();
            loadEstadosFacturaSugerida();
            prepareCollapsion();
            ajustarAnchoDivTable();
        });

        // funcion para cargar la tabla
        function cargarTabla(datos) {

            inicializarLoading();
            var cont, eCont, dir, eDir,
                    cif, eCif, comentarios;

            var item = null;
            var wastebasket = null;
            oTable.fnClearTable();
            for (var k in datos) {
                item = datos[ k ];
                wastebasket = "";
                // miro los valores de info alias, y si están a 1 los pinto
                // verdes, a 0 rojos
                cont = item.nContacto === 1 ? "s" : "no";
                eCont = item.nContacto === "" ? "" : "no ";
                dir = item.nDireccion === 1 ? "s" : "n";
                eDir = item.nDireccion === 1 ? "" : "no ";
                cif = item.nCIF === 1 ? "s" : "n";
                eCif = item.nCIF === 1 ? "" : "no ";
                comentarios = item.comentarios === null ? "" : item.comentarios;
                wastebasket = (item.idEstado === ESTADO_SUGERIDA) ? "&nbsp;<img src=\"img/del.png\" style=\"cursor:pointer;\" title=\"Eliminar Factura Sugerida\" width=\"12px\" ng-click=\"deleteConfirm(" + item.idFacturaSugerida + ");\">" : "";
                oTable.fnAddData([
                    "<span ><img src='img/" + cont + "cont.gif' title='Contacto " + eCont + "asignado'>&nbsp;"
                            + "<img src='img/" + dir + "direc.gif' title='Direcci&oacute;n " + eDir + "asignada'>&nbsp;"
                            + "<img src='img/" + cif + "cif.gif' title='CIF " + eCif + "asignado'></span>",
                    "<span >" + item.cdAlias.trim() + " - " + item.nombreAlias + "</span>",
                    "<span >" + transformaFecha(item.fechaCreacion) + "</span>",
                    "<span class=\"monedaR\">" + $.number(item.importe, 2, ',', '.') + "</span>",
                    "<span >" + transformaFecha(item.fechaInicio) + "</span>",
                    "<span >" + transformaFecha(item.fechaFin) + "</span>",
                    "<span >" + item.descEstado + wastebasket + "</span>",
                    "<span >" + comentarios + "</span>"
                ]);
            }
            $.unblockUI();
            growl.addInfoMessage("Tabla cargada correctamente.");
        }
        function deleteSuccessHandler(data, status, headers, config) {
            angular.element('.buscador').hide();
            $scope.submitForm();
            $.unblockUI();
            growl.addSuccessMessage("Factura eliminada correctamente. " + status);
        }
        function deleteFacturaSugerida(id) {
            var filter = {idFacturaSugerida: id};
            inicializarLoading();
            FacturaSugeridaService.remove(deleteSuccessHandler, showError, filter);
        }

        function cargarAlias(datos) {
            var item = null;
            for (var k in datos) {
                item = datos[ k ];
                idOcultos.push(item.id);
                availableTags.push(item.nombre.trim());
            }
            // código de autocompletar
            angular.element("#textAlias").autocomplete({
                source: availableTags
            });
            growl.addInfoMessage("Alias cargados.");
        }
        function cargarEstados(datos) {
            var item = null;
            for (var k in datos) {
                item = datos[ k ];
                // Rellenamos el combo
                $('#textEstado').append(
                        "<option value='" + item.idestado + "'>" + item.nombre + "</option>"
                        );
            }
            growl.addInfoMessage("Estados cargados.");
        }


    }]);

