'use strict';
sibbac20
    .controller(
                'DireccionFiscalController',
                [
                 '$scope',
                 '$http',
                 '$rootScope',
                 '$timeout',
                 'IsinService',
                 'AliasService',
                 'growl',
                 function ($scope, $http, $rootScope, $timeout,IsinService, AliasService,growl) {

                   var idOcultos = [];
                   var availableTags = [];
                   $scope.showBotonesDirFiscal = false;
                   function onErrorRequest (data) {
                         $.unblockUI();
                         growl.addErrorMessage("Ocurrió un error durante la petición de datos.");
                     }
                   $(document).ready(function () {

                     /* Eva:Función para cambiar texto */
                     jQuery.fn.extend({
                       toggleText : function (a, b) {
                         var that = this;
                         if (that.text() != a && that.text() != b) {
                           that.text(a);
                         } else if (that.text() == a) {
                           that.text(b);
                         } else if (that.text() == b) {
                           that.text(a);
                         }
                         return this;
                       }
                     });
                     /* FIN Función para cambiar texto */

                     $('a[href="#release-history"]').toggle(function () {
                       $('#release-wrapper').animate({
                         marginTop : '0px'
                       }, 600, 'linear');
                     }, function () {
                       $('#release-wrapper').animate({
                         marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
                       }, 600, 'linear');
                     });

                     $('#download a').mousedown(function () {
                       _gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
                     });

                     $('.collapser').click(function () {
                       $(this).parents('.title_section').next().slideToggle();
                       // $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                       $(this).toggleClass('active');
                       $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

                       return false;
                     });
                     $('.collapser_search').click(function () {
                       $(this).parents('.title_section').next().slideToggle();
                       $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                       $(this).toggleClass('active');
                       // $(this).toggleText( "Mostrar Alta de Grupo Identificador", "Ocultar
                       // Alta de Grupo Identificador" );
                       if ($(this).text() == "Ocultar Alta de Grupo Identificador") {
                         $(this).text("Mostrar Alta de Grupo Identificador");
                       } else {
                         $(this).text("Ocultar Alta de Grupo Identificador");
                       }
                       return false;
                     });

                     /*
                       * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA
                       */
                     if ($('.contenedorTabla').length >= 1) {
                       var anchoBotonera;
                       $('.contenedorTabla').each(function (i) {
                         anchoBotonera = $(this).find('table').outerWidth();
                         $(this).find('.botonera').css('width', anchoBotonera + 'px');
                         $(this).find('.resumen').css('width', anchoBotonera + 'px');
                         $('.resultados').hide();
                       });
                     }

                     // cargarAlias();
                     inicializarLoading();
                     AliasService.getAlias().success(cargarAlias).error(onErrorRequest);

                   });





/*
                     $scope.modificarDireccion = function() {
                         let data = {};
                         data.calle =$scope.direccion.calle;
                         data.numero =$scope.direccion.numero;
                         data.bloque =$scope.direccion.bloque;
                         data.poblacion =$scope.direccion.poblacion;
                         data.codPostal =$scope.direccion.codigoPostal;
                         data.provincia =$scope.direccion.provincia;
                         data.pais =$scope.direccion.pais;
                         data.codigo = document.getElementById('textIdDirFiscal').value;
                         let requestObject =  {
                             service : "SIBBACServiceAliasDirecciones",
                             action : "modificacionAliasDirecciones",
                             filters: data,
                         };
                         $.ajax({
                             type : "POST",
                             dataType : "json",
                             url : "/sibbac20back/rest/service",
                             data : JOSN.stringify(requestObject),
                             beforeSend : function (x) {
                                 if (x && x.overrideMimeType) {
                                     x.overrideMimeType("application/json");
                                 }
                                 // CORS Related
                                 x.setRequestHeader("Accept", "application/json");
                                 x.setRequestHeader("Content-Type", "application/json");
                                 x.setRequestHeader("X-Requested-With", "HandMade");
                                 x.setRequestHeader("Access-Control-Allow-Origin", "*");
                                 x.setRequestHeader('Access-Control-Allow-Headers',
                                     'Origin, X-Requested-With, Content-Type, Accept')
                             },
                             async : true,
                             success : function (json) {
                                 alert(json.resultados.result);
                             },
                             error : function (c) {
                                 alert("ERROR: " + c);
                             }

                         });

                     }
*/
                    $scope.modificarDireccion = function() {
                         let data = {};
                         data.calle =$scope.direccion.calle;
                         data.numero =$scope.direccion.numero;
                         data.bloque =$scope.direccion.bloque;
                         data.poblacion =$scope.direccion.poblacion;
                         data.codigoPostal =$scope.direccion.codigoPostal;
                         data.provincia =$scope.direccion.provincia;
                         data.pais =$scope.direccion.pais;
                         data.codigo = $scope.direccion.id;

                         var filtro = "{\"id\" : \"" + data.codigo + "\"," + "\"calle\" : \"" + data.calle + "\","
                                  + "\"numero\" : \"" + data.numero + "\"," + "\"bloque\" : \"" + data.bloque + "\","
                                  + "\"poblacion\" : \"" + data.poblacion + "\"," + "\"provincia\" : \"" + data.provincia + "\","
                                  + "\"pais\" : \"" + data.pais + "\"," + "\"codigoPostal\" : \"" + data.codigoPostal + "\" }";

                     var jsonData = "{" + "\"service\" : \"SIBBACServiceAliasDirecciones\", "
                                    + "\"action\"  : \"modificacionAliasDirecciones\", " + "\"filters\"  : " + filtro
                                    + "}";

                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : jsonData,
                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {
                           if(json.resultados && json.resultados.result==""){
                               growl.addSuccessMessage("Se ha modificado la dirección correctamente.");
                               $scope.direccion.id =  json.resultados.aliasDireccionId  ;
                               $scope.showResultado = false;
                           }

                       },
                       error : function (c) {
                         alert("ERROR: " + c);
                       }

                     });

                   };

                    $scope.limpiarDatos = function () {
                        $scope.direccion  = {};
                    }
                    $scope.crearDireccion = function  () {
                       let data = {};
                       data.calle   =$scope.direccion.calle;
                       data.numero  =$scope.direccion.numero;
                       data.bloque  =$scope.direccion.bloque;
                       data.poblacion =$scope.direccion.poblacion;
                       data.codigoPostal =$scope.direccion.codigoPostal;
                       data.provincia =$scope.direccion.provincia;
                       data.pais =$scope.direccion.pais;
                       data.codigo = $scope.direccion.id;
                       data.aliasId = $scope.aliasId;

                       var filtro = "{\"id\" : \"" + data.codigo + "\"," + "\"calle\" : \"" + data.calle + "\","
                           + "\"numero\" : \"" + data.numero + "\"," + "\"bloque\" : \"" + data.bloque + "\","
                           + "\"poblacion\" : \"" + data.poblacion + "\"," + "\"provincia\" : \"" + data.provincia + "\","
                           + "\"pais\" : \"" + data.pais + "\"," + "\"codigoPostal\" : \"" + data.codigoPostal + "\"," +"\"aliasId\" : \"" + data.aliasId + "\" }";

                       var jsonData = "{" + "\"service\" : \"SIBBACServiceAliasDirecciones\", "
                                    + "\"action\"  : \"createAliasDirecciones\", " + "\"filters\"  : " + filtro + "}";

                        let requestObject = {
                            service : 'SIBBACServiceAliasDirecciones',
                            action : 'createAliasDirecciones',
                            filters : data

                        };
                       $http({
                            method:'POST',
                            contentType:'application/json',
                            url : "/sibbac20back/rest/service",
                            data :requestObject,
                            responseType:'json'
                        }).success(function (json) {
                            growl.addSuccessMessage(json.resultados.result);
                            $scope.direccion.id = json.resultados.aliasDireccionId;
                            $scope.showBotonesDirFiscal = true;
                            $scope.showBotonModificar   = true;
                            $scope.showBotonCrear       = false;
                            $scope.showBotonLimpiar = true;
                            $scope.showResultado = false;
                        });



                   };

                   /* cargar tabla alias */
                   $('.btn.buscador').click(function () {

                     var listaAlias = document.getElementById("textAlias").value;

                     var posicionAlias = availableTags.indexOf(listaAlias);
                     if (posicionAlias == "-1") {
                       alert("El alias introducido no existe");
                       return false;
                     } else {
                       var valorSeleccionadoAlias = idOcultos[posicionAlias];
                       document.getElementById('idAlias').value = valorSeleccionadoAlias;

                       $scope.cargarTabla(valorSeleccionadoAlias);
                     }
                     return false;
                   });

                   // function cargarAlias

                   function cargarAlias (data) {

                     angular.forEach(data.resultados.result_alias, function (val, key) {
                       var ponerTag = val.alias + " - " + val.descripcion;
                       idOcultos.push(val.alias);
                       availableTags.push(ponerTag);

                     });

                     $("#textAlias").autocomplete({
                       source : availableTags
                     });

                     $.unblockUI();

                   }
                     $scope.direccion  = {} ;

                   // esta es la tabla que carga todos los datos
                  $scope.cargarTabla =  function(alias) {

                      $scope.showResultado = false;
                      $scope.showBotonesDirFiscal = false;
                      $scope.showBotonModificar   = false;
                      $scope.showBotonCrear       = false;
                      $scope.showBotonLimpiar = false;
                      let requestObject = {
                          service : 'SIBBACServiceAliasDirecciones',
                          action : 'getListaAliasDirecciones',
                          filters : {aliasId:document.getElementById('idAlias').value}

                      };
                      $http({
                           method:'POST',
                           contentType:'application/json',
                           url : "/sibbac20back/rest/service",
                           data :requestObject,
                           responseType:'json'
                      }).success(function (json) {
                          $scope.showBotonesDirFiscal = true;
                          $scope.showBotonLimpiar = true ;

                          let key = Object.keys(json.resultados);

                          $scope.aliasId=  key[0];
                          if(json.resultados!=null){
                              let datos = json.resultados[key[0]];
                              $scope.direccion  = datos ;
                              if ($scope.direccion.id==0) {
                                  $scope.showBotonCrear = true;
                              } else {
                                  $scope.showBotonModificar = true;
                              }
                              $timeout(function () {
                                  $scope.$apply();
                              });

                          }
                          $scope.showResultado = true;
                      }).error(function (c) {
                          console.log("ERROR: " + c);
                      });
                   }

                 } ]);
