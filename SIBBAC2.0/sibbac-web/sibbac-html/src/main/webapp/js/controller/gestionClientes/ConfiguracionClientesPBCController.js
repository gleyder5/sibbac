(function (GENERIC_CONTROLLERS, angular, sibbac20, console, $, undefined) {
	"use strict";

	// OnInit
	var ConfiguracionClientesPBCController = function (service, GestionClientesService, httpParamSerializer, uiGridConstants, $scope, $filter) {
		// Sub controlador DynamicFiltersController
		GENERIC_CONTROLLERS.DynamicFiltersController.call(this, service, httpParamSerializer, uiGridConstants);
		// Opciones del modal (750px ancho)
		var fatDialogOpt = { width: 750 };
		var ctrl = this;
		this.scope = $scope;
		this.today = $filter('date')(new Date(), 'yyyy-MM-dd')
		this.listadoEfectivo = [];
		this.maxefectivo = 0;
		this.porcentajeEfectivo = 0;
		this.codigoKGR = "-";
		this.idClienteHistorico = "-";
		this.dataHistorico = [];
		this.gridObserv = [];
		this.cleaner();
		this.createModalFlag = true;
		this.gestionClientesService = GestionClientesService;
		this.dialogs = ["#modalSave", "#modalEdit", "#modalObserv", "#modalNewObserv", "#resultDialog", "#historicoDialog"];


		this.dialogs.forEach(dialog => {
			var aux = $('[role=dialog]').find(dialog);
			(aux.length > 0) ? aux.dialog('destroy').remove() : undefined;
		});

		this.gestionClientesService.cargaClientes(function (data) {
			ctrl.listaClientes = data.resultados.listaClientes;
			ctrl.listaValuesClientes = ctrl.listaClientes.map(function (data) {
				return data.value;
			})
			ctrl.populateAutocomplete("input.autocomplete_client", ctrl.listaClientes, function (option) {
				ctrl.params.id = option.value;
				ctrl.changedValueKGR(option.value);
			});
		}, function (error) {
			fErrorTxt("Se produjo un error en la carga de los clientes.", 1);
		});


		this.queries.$promise.then(data => {
			this.selectedQuery = data[0];
			this.selectQuery();
			this.filterList.$promise.then(data => {
				this.filterList.pop(2);
				this.filtroActivo = true;
			});
		});

		this.scope.safeApply = function (fn) {
			var phase = this.$root.$$phase;
			if (phase === '$apply' || phase === '$digest') {
				if (fn && (typeof (fn) === 'function')) {
					fn();
				}
			} else {
				this.$apply(fn);
			}
		};

		var gridHistoricoColumns = [{ name: "Fecha de Cambio", field: "fechaCambio" },
		{ name: "Cliente", field: "cliente.cliente" },
		{ name: "KGR", field: "cliente.kgr" },
		{ name: "Control Efectivo", field: "cliente.desControlEfectivo" },
		{ name: "Control Análisis", field: "cliente.desControlAnalisis" },
		{ name: "Control Documentación", field: "cliente.desControlDocumentacion" },
		{ name: "Observación", field: "cliente.observacion" }
		];

		var gridObservacionColumns = [
			{ name: "Fecha Alta", field: "fechaAlta" },
			{ name: "Control Análisis", field: "idControlAnalisis" },
			{ name: "Descripción Control de Análisis", field: "desControlAnalisis" },
			{ name: "Observación", field: "desObservacion" }
		];


		this.gridHistorico = this.createGridColumnsNoSelection(
			gridHistoricoColumns, function (gridApi) {
				ctrl.gridApiRefAlcBaja = gridApi;
			});

		this.gridObserv = this.createGridColumnsNoSelection(
			gridObservacionColumns, function (gridApi) {
				ctrl.gridApiRefObserv = gridApi;
			});

		// Resgistramos los modales por la id, y le asignamos una función para
		// el boton con el nombre asignado (Guardar, Modificar, etc)
		//CREAR CLIENTE
		this.createModal = this.registryModal("modalSave", "Crear...", {}, fatDialogOpt);
		this.createModal.dialog("option", "buttons", {
			Aceptar: function (){
				// ctrl.pantallaObservaciones = false;
				ctrl.save(false);
			},
			Cancelar: function () { $(this).dialog("close"); },
			"Siguiente >": function () {
				$(this).dialog("close");
				ctrl.observModal.dialog("open");
			}
		});

		//MODIFICAR CLIENTE
		this.modifyModal = this.registryModal("modalEdit", "Modificar Cliente PBC:", {}, fatDialogOpt);
		this.modifyModal.dialog("option", "buttons", {
			Modificar: function (){
				// ctrl.pantallaObservaciones = false;
				ctrl.edit(false);
			},
			Cancelar: function () { $(this).dialog("close"); },
			"Siguiente >": function () {
				$(this).dialog("close");
				ctrl.observModal.dialog("open");
				ctrl.observaciones(ctrl.selectedRows());
			}
		});
		//LISTADO OBSERVACIONES
		this.observModal = this.registryModal("modalObserv", "Modificar Cliente PBC:", {}, fatDialogOpt);

		//CREAR/EDITAR OBSERVACION
		this.modalNewObserv = this.registryModal("modalNewObserv", "Modificar Cliente PBC:", {}, fatDialogOpt);


		//RESULTADO FINAL
		this.resultDialog = angular.element("#resultDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					ctrl.resultDialog.dialog("close");
					if (ctrl.result.ok) {
						ctrl.executeQuery();
					}

				}
			}
		});

		//DIALOG HISTORICO
		this.historicoDialog = angular.element("#historicoDialog").dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Cerrar": function () {
					ctrl.historicoDialog.dialog("close");
				}
			}
		}, { width: 1250 });

		getListado("efectivo");
		getListado("documentacion");
		getListado("analisis");

		function getListado(tipoLista) {
			// Ejecutamos el servicio
			switch (tipoLista) {
				case "efectivo":
					service.executeActionGETListadoEfectivoCombobox({}, function (result) {
						ctrl.listadoEfectivo = result;
					}, ctrl.postFail.bind(this));
					break;
				case "documentacion":
					service.executeActionGETListadoDocumentacionCombobox({}, function (result) {
						ctrl.listadoDocumentacion = result;
					}, ctrl.postFail.bind(this));
					break;
			}
		};


		ConfiguracionClientesPBCController.prototype.changedValue = function (idEfectivo) {
			this.service.executeActionGETDatosEfectivo(idEfectivo, function (result) {
				ctrl.maxefectivo = result.importeEfectivoMax;
				ctrl.porcentajeEfectivo = result.perEfectivoMax;
			}, this.postFail.bind(this));
		};


		ConfiguracionClientesPBCController.prototype.changedValueKGR = function (idCliente) {
			this.params.id = idCliente;
			this.service.executeActionGETDatosKGR(this.params, {}, function (result) {
				ctrl.codigokgr = result.cdkgr;
			}, this.postFail.bind(this));
		};

		ConfiguracionClientesPBCController.prototype.actualizarFechaRevision = function () {
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPUTfecha({}, function (result) {
				$.unblockUI();
				ctrl.result = result;
				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Fecha revisión actualizada" : "La fecha revisión no ha podido ser actualizada");
				ctrl.resultDialog.dialog("open");

			}, this.postFail.bind(this));
		};

		ConfiguracionClientesPBCController.prototype.historico = function (clienteHistorico) {
			inicializarLoading();
			
			var aux = {}; 
			Object.keys(clienteHistorico[0]).forEach(function (key) {
				aux[key.toLowerCase()] = clienteHistorico[0][key];
			})
			
			
			this.service.executeActionGETListadoHistorico(aux, {}, function (result) {
				console.log("Recuperado listado historico con exito");
				$.unblockUI();
				ctrl.gridHistorico.data = result;
			}, this.postFail.bind(this));

			// Inicia el "Cargando datos..."
			this.idClienteHistorico = clienteHistorico[0].IDCLIENTE.trim();

			this.historicoDialog.dialog("option", "title", "Ver Historico");
			this.historicoDialog.dialog("open");
		};

		ConfiguracionClientesPBCController.prototype.observaciones = function (cliente) {
			inicializarLoading();
			this.params.id = cliente[0].IDCLIENTE.trim();
			this.service.executeActionGETListadoObservaciones(this.params, {}, function (result) {
				console.log("Recuperado listado de observaciones con exito");
				$.unblockUI();
				ctrl.gridObserv.data = result;
			}, this.postFail.bind(this));
		};

		ConfiguracionClientesPBCController.prototype.changedNewObserv = function (id) {
			var ctrl = this;
			this.service.executeActionGETControlAnalisis(id, function (result) {
				console.log("Recuperado control de análisis con exito");
				$.unblockUI();
				if (result.id) {
					ctrl.observParams.ctrlAnalisis = result.id + "-" + result.descripcion;
				} else {
					ctrl.observParams.ctrlAnalisis = undefined;
				}
			}, this.postFail.bind(this));
		};

		ConfiguracionClientesPBCController.prototype.modalNewObservAccept = function () {
			if (this.exists(this.observParams) && this.exists(this.observParams.ctrlAnalisis) && this.exists(this.observParams.observ)) {
				this.observBtnFlag = false;
				return {
					idControlAnalisis: this.observParams.ctrlAnalisis.split("-")[0],
					desControlAnalisis: this.observParams.ctrlAnalisis.split("-")[1],
					idObservacion: this.observParams.observ.split("-")[0],
					desObservacion: this.observParams.observ.split("-")[1],
					fechaAlta: $filter('date')(new Date(), 'yyyy-MM-dd')
				};
			} else {
				return undefined;
			}
		}



		ConfiguracionClientesPBCController.prototype.fillObserv = function (observacion) {
			return {
				idObservacionCliente: observacion.idObservacionCliente,
				idControlAnalisis: observacion.idControlAnalisis,
				desControlAnalisis: observacion.desControlAnalisis,
				idObservacion: observacion.idObservacion,
				desObservacion: observacion.desObservacion,
				fechaAlta: $filter('date')(new Date(), 'yyyy-MM-dd')
			}
		}

		ConfiguracionClientesPBCController.prototype.ctrlExecuteQuery = function () {
			this.filter.ACTIVO = (this.filtroActivo) ? 1 : 0;
			this.executeQuery();
		}

		ConfiguracionClientesPBCController.prototype.populateAutocomplete = function (input, availableTags, callback) {
			var ctrl = this;
			$(input).autocomplete({
				minLength: 0,
				source: availableTags,
				focus: function (event, ui) {
					return false;
				},
				select: function (event, ui) {
					var option = {
						key: ui.item.key,
						value: ui.item.value
					};
					callback(option);
					ctrl.scope.safeApply();
					return false;
				}
			});

		};
	};

	ConfiguracionClientesPBCController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);

	ConfiguracionClientesPBCController.prototype.constructor = ConfiguracionClientesPBCController;

	/**
	 * Retorna un objeto de configuración con los valores utilizados en este
	 * proyecto para mostrar un grid básico. Para las columnas consulta el
	 * método "columns".
	 */
	ConfiguracionClientesPBCController.prototype.createGridColumnsNoSelection = function (
		columns, gridApiEventListener) {
		angular.forEach(columns, this.customizeColumns, this);
		return {
			paginationPageSizes: [5, 10],
			paginationPageSize: 5,
			enableFiltering: false,
			data: [],
			columnDefs: columns,
			gridMenuShowHideColumns: false,
			onRegisterApi: gridApiEventListener
		};
	};



	// Función que se encarga de levantar los modales con una id que identifica
	// su tipo
	ConfiguracionClientesPBCController.prototype.openModal = function (id) {
		switch (id) {
			case "save":
				this.params = {
				};

				this.codigokgr = '';
				this.maxefectivo = '';
				this.porcentajeEfectivo = '';

				this.createModal.dialog("option", "title", "Crear Cliente");
				this.observModal.dialog("option", "title", "Crear Cliente");
				this.modalNewObserv.dialog("option", "title", "Crear Cliente");
				this.createModal.dialog("open");
				this.cleaner();
				this.createModalFlag = true;
				this.configObservModal();


				break;
			case "edit":
				if (this.selectedRows().length > 0) {
					this.value = this.selectedRows()[0];
					// Igualamos los valores a los parámetros que recibiremos en
					// el modal
					this.params = {
						id: this.value.IDCLIENTE.trim(),
						idControlEfectivo: this.value.IDEFE.toString(),
						idControlDocumentacion: this.value.IDDOC.trim(),
						idControlAnalisis: (this.value.IDANA != undefined) ? this.value.IDANA.trim() : null,
						activo: this.value.ACTIVO
					}
					this.changedValue(this.value.IDEFE.toString());
					this.codigokgr = this.value.CODKGR.trim();
					this.modifyModal.dialog("option", "title", "Modificar Cliente PBC: " + this.selectedRows()[0].IDCLIENTE);
					this.observModal.dialog("option", "title", "Modificar Cliente PBC: " + this.selectedRows()[0].IDCLIENTE);
					this.modalNewObserv.dialog("option", "title", "Modificar Cliente PBC: " + this.selectedRows()[0].IDCLIENTE);
					this.modifyModal.dialog("open");
					this.cleaner();
					this.createModalFlag = false;
					this.configObservModal();
				}

				break;
		}
	}


	ConfiguracionClientesPBCController.prototype.configNewObservModal = function (createFlag) {
		var ctrl = this;
		this.modalNewObserv.dialog("option", "buttons", {
			Aceptar: function () {
				var aux = ctrl.modalNewObservAccept();
				if (aux) {
					ctrl.newObservObj = (createFlag) ? aux : undefined;
					ctrl.modObservObj = (!createFlag) ? aux : undefined;
					ctrl.createIdControlAnalisis = (aux) ? aux.idControlAnalisis + "-" + aux.desControlAnalisis : undefined;

					$(this).dialog("close");
					ctrl.observModal.dialog("open");
					ctrl.scope.$applyAsync();
				} else {
					fErrorTxt("Faltan campos por completar", 1);
				}
			},
			Cancelar: function () {
				$(this).dialog("close");
				ctrl.observModal.dialog("open");
			}
		});
	}

	ConfiguracionClientesPBCController.prototype.configObservModal = function () {
		var ctrl = this;
		this.observModal.dialog("option", "buttons", {
			"< Anterior": function () {
				$(this).dialog("close");
				var prevModal = (ctrl.createModalFlag) ? "#modalSave" : "#modalEdit";
				angular.element(prevModal).dialog("open");
				ctrl.params.idControlAnalisis = (ctrl.createIdControlAnalisis) ? ctrl.createIdControlAnalisis : ctrl.params.idControlAnalisis;
				ctrl.scope.$applyAsync();
			},
			Aceptar: function() {
				// ctrl.pantallaObservaciones = true;
				(ctrl.createModalFlag) ? ctrl.save(true) : ctrl.edit(true);
			},
			Cancelar: function () { $(this).dialog("close"); }
		});
	}


	// Función encargada de ejecutar el servicio guardar
	ConfiguracionClientesPBCController.prototype.save = function (pantallaObservaciones) {
		// El form tiene que ser válido
		if (this.exists(this.params.id) && this.exists(this.params.idControlDocumentacion) && this.exists(this.params.idControlEfectivo) && this.listaValuesClientes.includes(this.params.id)) {
			var ctrl = this;
			this.params.idObservacion = (this.newObservObj) ? this.newObservObj.idObservacion : undefined;
			this.params.idControlAnalisis = (this.createIdControlAnalisis) ? this.createIdControlAnalisis.split("-")[0] : undefined;

			// Inicia el "Cargando datos..."
			inicializarLoading();
			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPOST(this.params, {}, function (result) {
				$.unblockUI();
				ctrl.result = result;
				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				ctrl.resultDialog.dialog("open");

				(ctrl.result.ok) ? (pantallaObservaciones) ? ctrl.observModal.dialog("close") : ctrl.createModal.dialog("close") : '';

			}, this.postFail.bind(this));
		} else if (!this.exists(this.params.id) || !this.exists(this.params.idControlDocumentacion) || !this.exists(this.params.idControlEfectivo)) {
			fErrorTxt("Faltan parámetros por completar", 1);
		} else if (!this.listaValuesClientes.includes(this.params.id)) {
			fErrorTxt("El cliente introducido no existe", 1);
		}
	};

	ConfiguracionClientesPBCController.prototype.edit = function (pantallaObservaciones) {
		// El form tiene que ser válido
		if (this.scope.modalForm.$valid) {
			var ctrl = this;

			this.modifyModal.dialog("close");
			// Inicia el "Cargando datos..."
			inicializarLoading();

			var id = null;
			if (this.newObservObj) {
				this.params.idObservacion = this.newObservObj.idObservacion;
			} else if (this.modObservObj) {
				this.params.idObservacion = this.modObservObj.idObservacion;
				id = this.selectedObserv.idObservacionCliente;
			} else if (this.delObservObj) {
				this.params.idObservacion = "";
				id = this.delObservObj.idObservacionCliente;
			}

			// Ejecutamos el servicio pasandole por parámetros [this.params]
			this.service.executeActionPUT({ idObservacion: id, cliente: this.params }, {}, function (result) {
				$.unblockUI();
				ctrl.result = result;
				// Ejecuta el modal de respuesta al revicio
				ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
				ctrl.resultDialog.dialog("open");

				(ctrl.result.ok) ? (pantallaObservaciones) ? ctrl.observModal.dialog("close") : ctrl.modifyModal.dialog("close") : '';
			}, this.postFail.bind(this));
		} else {
			fErrorTxt("El formato de los datos introducidos no son correctos", 1);
		}
	};

	ConfiguracionClientesPBCController.prototype.delete = function () {
		// El form tiene que ser válido
		if (this.selectedRows().length > 0) {
			var ctrl = this;
			var deleteObj = [];
			this.selectedRows().forEach(element => {
				var id = element.IDCLIENTE;
				deleteObj.push(id);
			});

			angular.element("#dialog-confirm").html("¿Desea eliminar el/los registro/s seleccionado/s?");

			// Define the Dialog and its properties.
			this.confirmDialog = angular.element("#dialog-confirm").dialog({
				resizable: false,
				modal: true,
				title: "Mensaje de Confirmación",
				height: 150,
				width: 360,
				buttons: {
					" Sí ": function () {
						$(this).dialog('close');
						eliminar();
					},
					" No ": function () {
						$(this).dialog('close');
					}
				}
			});

			function eliminar() {
				// Inicia el "Cargando datos..."
				inicializarLoading();
				// Ejecutamos el servicio pasandole por parámetros
				// [this.params]
				ctrl.service.executeActionDEL(deleteObj, {}, function (result) {
					$.unblockUI();
					ctrl.result = result;
					// Ejecuta el modal de respuesta al revicio
					ctrl.resultDialog.dialog("option", "title", ctrl.result.ok ? "Ejecución exitosa" : "Ejecución con errores");
					if (!ctrl.result.ok) {
						fErrorTxt("Ha ocurrido un error. Consulte con el administrador.", 1);
					}
					ctrl.resultDialog.dialog("open");

				}, ctrl.postFail.bind(ctrl));
			}
		} else {
			fErrorTxt("Debe seleccionar una o más filas para poder realizar esta opcción", 1);
		}
	};

	ConfiguracionClientesPBCController.prototype.observSave = function () {
		var ctrl = this;

		//Vaciamos los parámetros del formulario
		this.observParams = {};

		//Cambiamos el título del dialog, cerramos el dialog que estabamos visualizando y 
		// a continuación se abre el dialog de crear observación
		this.modalNewObserv.dialog("option", "title", (this.createModalFlag) ? "Crear Cliente" : "Modificar Cliente PBC: " + this.selectedRows()[0].IDCLIENTE);
		this.observModal.dialog("close");
		this.modalNewObserv.dialog("open");

		this.configNewObservModal(true);

		// Inicia el "Cargando datos..."
		inicializarLoading();
		// Ejecutamos el servicio que nos devuelve las observaciones del combo
		this.service.executeActionGETComboObservaciones(function (result) {
			console.log("Recuperado combo de observaciones con exito");
			$.unblockUI();
			ctrl.listadoNewObserv = result;
		}, this.postFail.bind(this));
	};

	//Función para duplicar el registro indicado
	ConfiguracionClientesPBCController.prototype.observDuplicate = function (observacion) {
		// En el caso cuya fecha de alta sea igual al de la fecha de la acción realizada no se 
		// duplica el registro
		if (this.today != observacion) {
			this.newObservObj = this.fillObserv(observacion);
			this.observBtnFlag = false;
		}
	};

	ConfiguracionClientesPBCController.prototype.observEdit = function (observacion) {
		var ctrl = this;

		//Cambiamos el título del dialog, cerramos el dialog que estabamos visualizando y 
		// a continuación se abre el dialog de crear observación
		this.modalNewObserv.dialog("option", "title", "Modificar Cliente PBC: " + this.selectedRows()[0].IDCLIENTE);
		this.observModal.dialog("close");
		this.modalNewObserv.dialog("open");
		this.selectedObserv = observacion;

		this.configNewObservModal(false);

		// Inicia el "Cargando datos..."
		inicializarLoading();
		// Ejecutamos el servicio que nos devuelve las observaciones del combo
		this.service.executeActionGETComboObservaciones(function (result) {
			console.log("Recuperado combo de observaciones con exito");
			$.unblockUI();
			ctrl.listadoNewObserv = result;
			ctrl.observParams = {
				observ: observacion.idObservacion + "-" + observacion.desObservacion
			}

			// Ejecutamos el servicio que nos devuelve el control de analisis
			ctrl.changedNewObserv(observacion.idObservacion);
		}, this.postFail.bind(this));
	};

	ConfiguracionClientesPBCController.prototype.observDelete = function (observacion) {
		if (this.today != observacion) {
			this.delObservObj = observacion;
			this.observBtnFlag = false;
		}
	};

	ConfiguracionClientesPBCController.prototype.cleaner = function () {
		this.createIdControlAnalisis = undefined;
		this.newObservObj = undefined;
		this.modObservObj = undefined;
		this.delObservObj = undefined;
		this.observBtnFlag = true;
		this.gridObserv.data = [];
	}

	ConfiguracionClientesPBCController.prototype.exists = function (value) {
		return value != '' && value != undefined;
	}

	sibbac20.controller("ConfiguracionClientesPBCController", ["ConfiguracionClientesPBCService", "GestionClientesService", "$httpParamSerializer", "uiGridConstants",
		"$scope", "$filter", ConfiguracionClientesPBCController]);

})(GENERIC_CONTROLLERS, angular, sibbac20, console, $);