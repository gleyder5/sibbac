var GENERIC_CONTROLLERS = GENERIC_CONTROLLERS || {};
(function(angular, sibbac20, gc, undefined){
  "use strict";
  
  gc.ClearingConsultaController = function(isinService, cuentaLiquidacionService, conciliacionClearingService, growl,
      $filter) {
    this.isinService = isinService;
    this.cuentaLiquidacionService = cuentaLiquidacionService;
    this.conciliacionClearingService = conciliacionClearingService;
    this.growl = growl;
    this.$filter = $filter;
    this.oTableT = undefined;
    this.cuentas = [];
    this.rastroBusqueda = "";
    this.ultFecha = "";
    inicializarLoading();
    this.initialize();
  };

  gc.ClearingConsultaController.prototype.initialize = function() {
    this.loadDataTableT();
    cargarFechaActualizacionMovimientoAlias();
    this.loadIsines();
    this.loadCuentas();
    this.initFilter();
    prepareCollapsion();
    loadpicker(['fliquidacionDe']);
  };

  gc.ClearingConsultaController.prototype.initFilter = function() {
    this.filter = {
      "fliquidacionDe" : this.$filter('date')(new Date(), 'dd/MM/yyyy'), "idCuentaLiq" : "", "isin" : "",
      "cuadrados" : "todos"
    };
  };

  /** ISINES **/
  gc.ClearingConsultaController.prototype.loadIsines = function() {
    this.isinService.getIsines().success(this.cargarIsines.bind(this)).error(this.showError.bind(this));
  };

  /** CUENTAS **/
  gc.ClearingConsultaController.prototype.loadCuentas = function() {
    this.cuentaLiquidacionService.getCuentasLiquidacion().success(this.cargarCuentas.bind(this)).error(
        this.showError.bind(this));
    $.unblockUI();
  };

  /** Muestra error **/
  gc.ClearingConsultaController.prototype.showError = function(data, status, headers, config) {
    $.unblockUI();
    this.growl.addErrorMessage("ERROR REQUEST!!!");
    this.desformatearFechasFiltro();
  };

  gc.ClearingConsultaController.prototype.cargarCuentas = function(data, status, header, config) {
    if (data !== null && data.resultados !== null && data.resultados.cuentasLiquidacion !== null) {
      this.cuentas = data.resultados.cuentasLiquidacion;
    }
  };

  gc.ClearingConsultaController.prototype.cargarIsines = function(data, status, headers, config) {
    if (data != null && data.resultados != null && data.resultados.result_isin != null) {
      var datosTsin = data.resultados.result_isin;
      var availableTags = [];
      var ponerTag = "";
      angular.element.each(datosTsin, function(key, val) {
        ponerTag = datosTsin[key].codigo + " - " + datosTsin[key].descripcion;
        availableTags.push(ponerTag);
      });
      angular.element("#isin").autocomplete({
        source : availableTags
      });
    }
  };

  /** Devuelve la fecha de los filtros al formato legible */
  gc.ClearingConsultaController.prototype.formatearFechasFiltro = function() {
    this.filter.fliquidacionDe = transformaFecha(this.filter.fliquidacionDe);
  };

  gc.ClearingConsultaController.prototype.desformatearFechasFiltro = function() {
    this.filter.fliquidacionDe = transformaFechaInv(this.filter.fliquidacionDe);
  }

  gc.ClearingConsultaController.prototype.populateDataTables = function(datosT, fecha) {
    this.ultFecha = fecha;
    this.populateDataTableT(datosT);
    this.formatearFechasFiltro();
    $.unblockUI();
  };

  gc.ClearingConsultaController.prototype.cleanFilter = function(event) {
    this.initFilter();
    this.filter.cuadrados = 'todos';
    angular.element("#isin").val('');
  };

  gc.ClearingConsultaController.prototype.submitForm = function(event) {
    this.obtenerDatos();
    collapseSearchForm();
    this.seguimientoBusqueda();
  };

  gc.ClearingConsultaController.prototype.obtenerDatos = function() {
    this.filter.isin = this.getIsin();
    this.ultFecha = "";
    this.consultar();
  };

  /** para ver el filtro al minimizar la búsqueda */
  gc.ClearingConsultaController.prototype.seguimientoBusqueda = function() {
    var rastroBusqueda = "";

    if (this.filter.fliquidacionDe !== "") {
      rastroBusqueda += " Fecha Liquidación: " + this.filter.fliquidacionDe;
    }
    if (this.filter.isin !== "") {
      rastroBusqueda += " isin: " + this.filter.isin;
    }
    if (this.filter.idCuentaLiq !== "") {
      rastroBusqueda += " Cuenta: " + $('#selectCuenta option:selected').text();
    }
    rastroBusqueda += " Cuadrados: " + this.filter.cuadrados;
    this.rastroBusqueda = rastroBusqueda;
  };

  gc.ClearingConsultaController.prototype.getIsin = function() {
    var isinCompleto = angular.element('#isin').val();
    var guion = isinCompleto.indexOf("-");
    if (guion < 0) {
      return isinCompleto;
    }
    return isinCompleto.substring(0, guion);
  };
  
})(angular, sibbac20, GENERIC_CONTROLLERS);
