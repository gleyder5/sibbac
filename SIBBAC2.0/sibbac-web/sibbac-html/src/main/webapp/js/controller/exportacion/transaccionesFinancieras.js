'use strict';
sibbac20
    .controller(
                'ControltransaccionesFinancieras',
                [
                 '$scope',
                 '$compile',
                 'growl',
                 'JobsService',
                 '$document',
                 function ($scope, $compile, growl, JobsService, $document) {

                   var oTable = undefined;
                   var tipoInforme;
                   var fechaDesde;
                   var mercado;
                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();

                   hoy = yyyy + "_" + mm + "_" + dd;

                   /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
                   if ($('.contenedorTabla').length >= 1) {
                     var anchoBotonera;
                     $('.contenedorTabla').each(function (i) {
                       anchoBotonera = $(this).find('table').outerWidth();
                       $(this).find('.botonera').css('width', anchoBotonera + 'px');
                       $(this).find('.resumen').css('width', anchoBotonera + 'px');
                       $('.resultados').hide();
                     });
                   }

                   function initialize () {
                     // var fechas= ["fcontratacionDe","fcontratacionA"];
                     // loadpicker(fechas);
                     // verificarFechas([["fcontratacionDe","fcontratacionA"]]);
                     $("#ui-datepicker-div").remove();
                     $('input#fcontratacionDe').datepicker({
                       onClose : function (selectedDate) {
                         $("#fcontratacionA").datepicker("option", "minDate", selectedDate);
                       } // onClose
                     });

                     $('input#fcontratacionA').datepicker({
                       onClose : function (selectedDate) {
                         $("#fcontratacionDe").datepicker("option", "maxDate", selectedDate);
                       } // onClose
                     });
                   }

                   $(document).ready(function () {

                     initialize();

                     oTable = $("#tablaConsultaTransaccionesFinancieras").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : [ "/sibbac20/js/swf/copy_csv_xls_pdf.swf" ],
                         "aButtons" : [ "copy", {
                           "sExtends" : "csv",
                           "sFileName" : "Listado_de_transacciones_financieras_" + hoy + ".csv"
                         }, {
                           "sExtends" : "xls",
                           "sFileName" : "Listado_de_transacciones_financieras_" + hoy + ".csv"
                         // "sCharSet" : "utf8",

                         }, {
                           "sExtends" : "pdf",
                           "sPdfOrientation" : "landscape",
                           "sTitle" : " ",
                           "sPdfSize" : "A4",
                           "sPdfMessage" : "Listado de transacciones_financieras",
                           "sFileName" : "Listado_de_transacciones_financieras_" + hoy + ".pdf"
                         }, "print" ]
                       },

                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },

                       "aoColumns" : [

                       {
                         sClass : "centrar",
                         width : 300
                       }, // Reference
                       {
                         sClass : "centrar",
                         width : 75
                       }, // Trade Date
                       {
                         sClass : "centrar",
                         width : 75
                       }, // Settlem... date
                       {
                         sClass : "centrar",
                         width : 50
                       }, // Market
                       {
                         sClass : "centrar",
                         width : 75
                       }, // Subaccount
                       {
                         sClass : "centrar",
                         width : 250
                       }, // Description
                       {
                         sClass : "centrar",
                         width : 50
                       }, // Isin
                       {
                         sClass : "centrar",
                         width : 250
                       }, // Description
                       {
                         sClass : "align-right",
                         "sType" : "numeric",
                         width : 75
                       }, // Quantity
                       {
                         sClass : "align-right",
                         "sType" : "numeric",
                         width : 75
                       }, // Clien Gross
                       {
                         sClass : "align-right",
                         "sType" : "numeric",
                         width : 75
                       }, // Net Client
                       {
                         sClass : "align-right",
                         "sType" : "numeric",
                         width : 100
                       } // Market Charge.
                       ],
                       "fnCreatedRow" : function (nRow, aData, iDataIndex) {

                         $compile(nRow)($scope);
                       },
                       "order" : [],
                       "scrollY" : "480px",
                       "scrollX" : "100%",
                       "scrollCollapse" : true,

                     });

                     angular.element("#fcontratacionDe, #fcontratacionA").datepicker({
                       dateFormat : 'DD/MM/YYYY',
                       defaultDate : new Date(),

                     }); // datepick

                     // Cierra o abre el formulario del filtro.
                     $('.collapser_search').click(function (event) {
                       event.preventDefault();
                       $(this).parents('.title_section').next().slideToggle();
                       $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                       $(this).toggleClass('active');
                       if ($(this).text().indexOf('Ocultar') !== -1) {
                         $(this).text("Mostrar opciones de búsqueda");
                       } else {
                         $(this).text("Ocultar opciones de búsqueda");
                       }
                       return false;
                     });

                   });

                   // Botón de ejecutar la consulta.
                   $scope.Consultar = function () {

                     var errorTxt = validadFecha();

                     if (errorTxt !== null) {
                       fErrorTxt(errorTxt, 2);
                       return false;
                     }

                     if ($('#tipoInforme').val() !== "" || $('#tipoInforme').val() !== undefined) {
                       tipoInforme = $('#tipoInforme').val();
                     }
                     if ($('#fechaDesde').val() !== "" || $('#fechaDesde').val() !== undefined) {
                       fechaDesde = $('#fechaDesde').val();
                     }
                     if ($('#mercado').val() !== "" || $('#mercado').val() !== undefined) {
                       mercado = $('#mercado').val();
                     }

                     cargarDatosConsultaTransaccionesFinancieras();
                     // event.preventDefault();
                     $('.collapser_search').parents('.title_section').next().slideToggle();
                     $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
                     $('.collapser_search').toggleClass('acts');
                     if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
                       $('.collapser_search').text("Mostrar opciones de búsqueda");
                     } else {
                       $('.collapser_search').text("Ocultar opciones de búsqueda");
                     }
                     return false;

                   };

                   // Botón que limpia las condiciones del filtro.
                   $scope.LimpiarFiltros = function () {
                     $('input[type=text]').val('');
                     // ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
                     $("#fcontratacionA").datepicker("option", "minDate", "");
                     $("#fcontratacionDe").datepicker("option", "maxDate", "");
                     //
                   }

                   function cargarDatosConsultaTransaccionesFinancieras () {

                     console.log("cargarDatosConsultaTransaccionesFinancieras");

                     $("#tablaConsultaTransaccionesFinancieras > tbody").empty();

                     // var fecha = transformaFechaInv( $("#fechaDesde").val() );

                     var fcontratacionDe = transformaFechaInv($("#fcontratacionDe").val());
                     var fcontratacionA = transformaFechaInv($("#fcontratacionA").val());

                     var filtro = "{\"tipoInforme\" : \"" + tipoInforme + "\"," + "\"fcontratacionDe\" : \""
                                  + fcontratacionDe + "\"," + "\"fcontratacionA\" : \"" + fcontratacionA + "\","
                                  + "\"mercado\" : \"" + mercado + "\" }";

                     var tituTxt = "Informes de Transacciones Financieras";
                     if (tipoInforme == "1") {
                       tituTxt = "Informes Generico de Transacciones Financieras";
                     } else if (tipoInforme == "2") {
                       tituTxt = "Informes de operaciones exentas sin Open de Transacciones Financieras";
                     } else if (tipoInforme == "3") {
                       tituTxt = "Informes de operaciones solo Open de Transacciones Financieras";
                     }

                     document.getElementById("tituloprincipal").innerHTML = tituTxt;

                     var data = new DataBean();
                     data.setService('SIBBACServiceTransaccionesFinancieras');
                     data.setAction('getInformeList');
                     data.setFilters(filtro);
                     var request = requestSIBBAC(data);
                     request.success(function (json) {
                       console.log(json);
                       console.log(json.resultados);

                       if (json.resultados !== null && json.resultados !== undefined
                           && json.resultados.resultados_transacciones !== undefined) {

                         var item = null;
                         var datos = json.resultados.resultados_transacciones;

                         var tbl = $("#tablaConsultaTransaccionesFinancieras > tbody");
                         $(tbl).html("");
                         oTable.fnClearTable();

                         for ( var k in datos) {

                           item = datos[k];
                           var rowIndex = oTable.fnAddData([

                           item.nbooking, transformaFechaGuion(item.fetrade), transformaFechaGuion(item.fevalor),
                                                            item.mercado, item.cdalias, item.nombrealias, item.cdisin,
                                                            item.nbvalors, $.number(item.nutitcli, 2, ',', '.'),
                                                            $.number(item.eutotbru, 2, ',', '.'),
                                                            $.number(item.eutotnet, 2, ',', '.'),
                                                            $.number(item.imgatdvs, 2, ',', '.')

                           ]);

                           var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;

                         }
                         oTable.fnDraw();
                       } else {
                         var tipo = 2;
                         var errorTxt = json.error;
                         if (errorTxt == undefined) {
                           errorTxt = "Error indeterminado. Consulte con el servicio técnico.";
                           tipo = 1;
                         }
                         fErrorTxt(json.error, tipo);
                         $('.collapser_search').parents('.title_section').next().slideToggle();
                         $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
                         $('.collapser_search').toggleClass('active');
                         if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
                           $('.collapser_search').text("Mostrar opciones de búsqueda");
                         } else {
                           $('.collapser_search').text("Ocultar opciones de búsqueda");
                         }
                       }
                     });
                   }
                   function validadFecha () {

                     var error = null;
                     var fContratacionDe = $("#fcontratacionDe").val();
                     var fContratacionA = $("#fcontratacionA").val();

                     if (fContratacionDe.length == 0) {
                       error = "La Fecha de contratación desde no puede ir vacia."
                       return error;
                     }

                     if (!(fContratacionDe.length == 0 || fContratacionA.length == 0)) {
                       if (!(validDifFechas(fContratacionDe, fContratacionA))) {
                         error = "La diferencia entre la fechas de contratación desde y hasta no puede ser superior a un año."
                         return error;
                       }
                     }

                     if (!(fContratacionDe.length == 0) && fContratacionA.length == 0) {
                       var fechaDeHoy = new Date();
                       var fechaHoyTxt = fechaDeHoy.getDate() + "/" + (fechaDeHoy.getMonth() + 1) + "/"
                                         + fechaDeHoy.getFullYear()
                       if (!(validDifFechas(fContratacionDe, fechaHoyTxt))) {
                         error = "Si no se introduce \"fecha de contración hasta\" la \"fecha de contratación desde\" no puede ser de hace mas de un año."
                                 + " Si desea esta  \"fecha de contratación desde\" esta obligado a introducir una \"fecha de contratación hasta\" inferior a un año de diferencia.";
                         return error;
                       }
                     }

                     return error;
                   }

                   // Valida la diferencia de fechas.
                   function validDifFechas (fecha1, fecha2) {

                     var date1 = fecha1.split('/');
                     var date2 = fecha2.split('/')

                     var dateAnterior = [];
                     dateAnterior[0] = date1[1];
                     dateAnterior[1] = date1[0];
                     dateAnterior[2] = date1[2];
                     var datePosterior = [];
                     datePosterior[0] = date2[1];
                     datePosterior[1] = date2[0];
                     datePosterior[2] = date2[2];

                     var date11 = new Date(dateAnterior);
                     var date22 = new Date(datePosterior);
                     var timeDiff = Math.abs(date22.getTime() - date11.getTime());
                     var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                     if (diffDays > 366) {
                       return false;
                     }
                     return true;

                   }

                 } ]);
