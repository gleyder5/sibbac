'use strict';
sibbac20.controller('CuentaContableController',['$scope','$document','growl','ContaInformesService','IsinService','$compile', '$rootScope', 'SecurityService', '$location',
function($scope, $document, growl, ContaInformesService, IsinService, $compile, $rootScope, SecurityService, $location) {

    var idCuenta = "";
    var idAuxiliar = "";
    var oTable = undefined;
    String.prototype.format = function() {
        var args = arguments;

        return this.replace(/\{(\d+)\}/g, function() {
            return args[arguments[1]];
        });
    };

    var txtMarcar = "<input type='radio' name='rCuenta' class='editor-active' ng-click='activaMod()' value='{0}'>";

    $(function () {
    	$("input#numero").prop('required', true);
    	$("input#nombre").prop('required', true);
    	$.ajax({
    		type: "POST",
    		dataType: "json",
    		url:  "/sibbac20back/rest/service",
    		data: "{\"service\":\"SIBBACServiceCuentaContable\",\"action\":\"init\"}",
    		beforeSend: function( x ) {
    			if(x && x.overrideMimeType) {
    				x.overrideMimeType("application/json");
    			}
    			// CORS Related
    			x.setRequestHeader("Accept", "application/json");
    			x.setRequestHeader("Content-Type", "application/json");
    			x.setRequestHeader("X-Requested-With", "HandMade");
    			x.setRequestHeader("Access-Control-Allow-Origin", "*");
    			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    		},
    		async: true,
    		success: function(json) {
    			var filtro = json.resultados;
    			var select = $("select#operativa");
    			filtro.operativa.forEach(function(entry) {
    				select.append("<option value='" + entry[0] +"'>" + entry[1] +"</option>");
    			});
    			var select = $("select#tipo");
    			filtro.tipo.forEach(function(entry) {
    				select.append("<option value='" + entry[0] +"'>" + entry[1] +"</option>");
    			});
    			var select = $("select#withAux");
    			filtro.withAuxiliar.forEach(function(entry) {
    				select.append("<option value='" + entry[0] +"'>" + entry[1] +"</option>");
    			});
    			var select = $("select#operativaN");
    			filtro.operativa.forEach(function(entry) {
    				select.append("<option value='" + entry[0] +"'>" + entry[1] +"</option>");
    			});
    			var select = $("select#ctaLiqN");
    			select.append("<option value=''></option>");
    			filtro.lCtaLiq.forEach(function(entry) {
    				select.append("<option value='" + entry.id +"'>" + entry.cdCodigo +"</option>");
    			});
    			var select = $("select#tipoN");
    			filtro.tipo.forEach(function(entry) {
    				select.append("<option value='" + entry[0] +"'>" + entry[1] +"</option>");
    			});
    			var select = $("select#withAuxN");
    			filtro.withAuxiliar.forEach(function(entry) {
    				select.append("<option value='" + entry[0] +"'>" + entry[1] +"</option>");
    			});
    			var select = $("select#auxiliarN");
    			select.append("<option value=''></option>");
    			filtro.lAuxiliar.forEach(function(entry) {
    				select.append("<option value='" + entry.id +"'>" + entry.nombre +"</option>");
    			});
    		},
    		error: function(c) {
    			mensajeError.log( "ERROR: " + c );
    		}
    	});

    	$( "input#cuenta" ).autocomplete({
    		source: function( request, response ) {
    			var data = "{\"service\":\"SIBBACServiceCuentaContable\",\"action\":\"getCuentas\",\"filters\":{\"text\":\""
    				+ request.term + "\"}}";
    			$.ajax({
    				type: "POST",
    				dataType: "json",
    				url:  "/sibbac20back/rest/service",
    				data: data,
    				beforeSend: function( x ) {
    					if(x && x.overrideMimeType) {
    						x.overrideMimeType("application/json");
    					}
    					// CORS Related
    					x.setRequestHeader("Accept", "application/json");
    					x.setRequestHeader("Content-Type", "application/json");
    					x.setRequestHeader("X-Requested-With", "HandMade");
    					x.setRequestHeader("Access-Control-Allow-Origin", "*");
    					x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    				},
    				async: true,
    				success: function( json ) {
    					var datos = [];
    					if (json!==undefined && json.resultados!==undefined) {
    						json.resultados.cuentas.forEach(function(entry) {
    							datos.push({
    								value: entry.id,
    								label: entry.cuenta + "-" + entry.nombre
    							});
    						});
    					}
    					response(datos);
    				},
    				error: function(c) {
    					mensajeError.log( "ERROR: " + c );
    				}
    			});
    		},
    		minLength: 2,
    		focus: function() {
    			// prevent value inserted on focus
    			return false;
    		},
    		select: function( event, ui ) {
    			idCuenta = ui.item.value;
    			this.value = ui.item.label;
    			return false;
    		}
    	});

    	oTable = $("table#tblCuentas").dataTable({
    		"dom": 'T<"clear">lfrtip',
    		"tableTools": {
    			"sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
    		},
    		"language": {
    			"url": "i18n/Spanish.json"
    		},
    		"scrollY": "480px",
    		"scrollCollapse": true,
    		"scrollX": "100%",
    		"aoColumns": [
    	              	  {sClass: "centrar", targets: 0, bSortable: false, name: "active"},
    		              {sClass: "centrar"},
    		              {sClass: "centrar"},
    		              {sClass: "centrar"},
    		              {sClass: "centrar"},
    		              {sClass: "centrar"},
    		              {sClass: "centrar"}
    		              ]

    	});

    	$('.collapser_search').click(function () {
    		$(this).parents('.title_section').next().slideToggle();
    		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
    		$(this).toggleClass('active');
    		$(this).toggleText('Mostrar opciones de búsqueda', 'Ocultar opciones de búsqueda');
    		return false;
    	});
    });

    $("input#buscar").click(function () {
    	buscar();
    });

    function buscar() {
    	$('.collapser_search').parents('.title_section').next().slideToggle();
    	$('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
    	$('.collapser_search').toggleClass('active');
    	$("input#Baja").hide();
    	$("input#Modificar").hide();
    	if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
    		$('.collapser_search').text("Mostrar opciones de búsqueda");
    	} else {
    		$('.collapser_search').text("Ocultar opciones de búsqueda");
    	}
    	var params = {
    			"service": "SIBBACServiceCuentaContable",
    			"action": "filtra",
    			"filters": {
    				"cuenta":$("input#cuenta").val(),
    				"operativa":$("select#operativa").val(),
    				"tipo":$("select#tipo").val(),
    				"withAuxiliar":$("select#withAux").val()
    			}};
    	$.ajax({
    		type: 'POST',
    		dataType: "json",
    		contentType: "application/json; charset=utf-8",
    		url: "/sibbac20back/rest/service",
    		data: JSON.stringify(params),
    		beforeSend: function (x) {
    			if (x && x.overrideMimeType) {
    				x.overrideMimeType("application/json");
    			}
    			// CORS Related
    			x.setRequestHeader("Accept", "application/json");
    			x.setRequestHeader("Content-Type", "application/json");
    			x.setRequestHeader("X-Requested-With", "HandMade");
    			x.setRequestHeader("Access-Control-Allow-Origin", "*");
    			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    		},
    		success: function (json) {
    			var cuentas = undefined;
    			var cuenta;
    			var row;
    			if (json === undefined || json.resultados === undefined) {
    				cuentas = {};
    			} else {
    				cuentas = json.resultados.cuentas;
    			}
    			oTable.fnClearTable();
    			$.each(cuentas, function (key, val) {
    				$compile(txtMarcar)($scope)
    				cuenta = cuentas[key];
    				row = oTable.fnAddData([
    				                        txtMarcar.format(cuenta.id),
    				                        cuenta.cuenta,
    				                        cuenta.nombre,
    				                        cuenta.ctaLiq,
    				                        cuenta.operativa,
    				                        cuenta.tipo,
    				                        cuenta.withAuxiliar? "Si": "No"
    				                        	], false);
    			});
    			oTable.fnDraw();
    			$.unblockUI();
    		},
    		error: function(c) {
    			$.unblockUI();
    			mensajeError( "ERROR al ejecutar SIBBACServiceCierreContable#update: " + c );
    		}
    	});
    	return false;
    }

    $("input#limpiar").click(function(event) {
    	$("form#filtro input[type='text']").val('');
    	$("form#filtro select > option:selected").removeAttr("selected");
    });

    $scope.activaMod = function() {
    	$("input#Baja").show();
    	$("input#Modificar").show();
    }

    $("input#guardar").click(function(event) {
    	var cuenta = $("input#numero").val();
    	if (cuenta=="") {
    		mensajeError("La cuenta no puede quedar vacía");
    		return false;
    	}
    	var nombre = $("input#nombre").val();
    	if (nombre=="") {
    		mensajeError("El nombre no puede quedar vacío");
    		return false;
    	}
    	var withAuxiliar = $("select#withAuxN").val();
    	if (withAuxiliar=="") {
    		mensajeError("Tienes que poner si tiene o no auxiliar (\"Con Aux:\")");
    		return false;
    	}
    	var operativa = $("select#operativaN").val();
    	if (operativa=="") {
    		mensajeError("La operativa tiene que ser \"N=Nacional\" o \"I=Internacional\"");
    		return false;
    	}
    	var tipo = $("select#tipoN").val();
    	if (tipo=="") {
    		mensajeError("El tipo tiene que ser \"A=Activo\", \"P=Pasivo\" o \"X=PyG\"");
    		return false;
    	}
    	var ctaLiq = $("select#ctaLiqN").val();
    	if (ctaLiq=="") {
    		mensajeError("La cuenta de liquidación no puede quedar vacía");
    		return false;
    	}
    	var params = {
    			"service": "SIBBACServiceCuentaContable",
    			"action": "add",
    			"filters": {
    				"id":$("input#id").val(),
    				"operativa":operativa,
    				"tipo":tipo,
    				"cuenta":cuenta,
    				"nombre":nombre,
    				"withAuxiliar":withAuxiliar,
    				"ctaLiq":ctaLiq
    			}};
    	$.ajax({
    		type: 'POST',
    		dataType: "json",
    		contentType: "application/json; charset=utf-8",
    		url: "/sibbac20back/rest/service",
    		data: JSON.stringify(params),
    		beforeSend: function (x) {
    			if (x && x.overrideMimeType) {
    				x.overrideMimeType("application/json");
    			}
    			// CORS Related
    			x.setRequestHeader("Accept", "application/json");
    			x.setRequestHeader("Content-Type", "application/json");
    			x.setRequestHeader("X-Requested-With", "HandMade");
    			x.setRequestHeader("Access-Control-Allow-Origin", "*");
    			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    		},
    		success: function (json) {
    			var resultados = json.resultados;
    			var error;
    			if (resultados!==null && (error = resultados.error)!==undefined) {
    				$.unblockUI();
    				mensajeError("ERROR al dar de alta una cuenta contable<br/>" + error);
    			}
    			$scope.cerrar();
    			buscar();
    		},
    		error: function(json) {
    			$.unblockUI();
    			mensajeError("ERROR al ejecutar SIBBACServiceCierreContable#update\n" + json.resultados.error);
    		}
    	});
    	return false;
    });

    $scope.cerrar = function() {
    	$('div#div_formulario').css('display', 'none');
    	$("div#div_formulario input[type='text']").val('');
    	$("div#div_formulario select > option:selected").removeAttr("selected");
    }

    $("input#Baja").click(function(event) {
    	var id = $("input[name='rCuenta']:checked").val();
    	var params = {
    			"service": "SIBBACServiceCuentaContable",
    			"action": "delete",
    			"filters": {
    				"id":$("input[name='rCuenta']:checked").val()
    			}};
    	$.ajax({
    		type: 'POST',
    		dataType: "json",
    		contentType: "application/json; charset=utf-8",
    		url: "/sibbac20back/rest/service",
    		data: JSON.stringify(params),
    		beforeSend: function (x) {
    			if (x && x.overrideMimeType) {
    				x.overrideMimeType("application/json");
    			}
    			// CORS Related
    			x.setRequestHeader("Accept", "application/json");
    			x.setRequestHeader("Content-Type", "application/json");
    			x.setRequestHeader("X-Requested-With", "HandMade");
    			x.setRequestHeader("Access-Control-Allow-Origin", "*");
    			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    		},
    		success: function (json) {
    			var resultados = json.resultados;
    			var error;
    			if (resultados!==null && (error = resultados.error)!==undefined) {
    				$.unblockUI();
    				mensajeError("ERROR al eliminar la cuenta contable<br/>" + error);
    			} else {
    				buscar();
    			}
    		},
    		error: function(json) {
    			$.unblockUI();
    			mensajeError("ERROR al ejecutar SIBBACServiceCierreContable#update\n" + json.resultados.error);
    		}
    	});
    	return false;
    });

    $("input#Alta").click(function(event) {
    	$("p#titleTitu").html("Alta Cuenta Contable");
    	cargaCuenta();
    });

    $("input#Modificar").click(function() {
    	$("p#titleTitu").html("Modificar Cuenta Contable");
    	var salida = cargaCuenta();
    	salida.form.find("#id").val(salida.id);
    });

    function cargaCuenta(td) {
    	var form = $("div#div_formulario");
    	var td = $("input[name='rCuenta']:checked");
    	if (td!=null) {
    		var tds = td.parent().parent().find("td");
    		form.find("#numero").val($(tds[1]).html());
    		form.find("#nombre").val($(tds[2]).html());
    		form.find("#ctaLiqN option").each(function(index, entry) {
    			if ($(tds[3]).html()==entry.text) {
    				$(entry).prop("selected", true);
    			}
    		});
    		form.find("#operativaN").val($(tds[4]).html());
    		form.find("#tipoN").val($(tds[5]).html());
    		form.find("#withAuxN").val("Si"==$(tds[6]).html()? 1: 0);
    		$(form).css('display', 'block');
    		return {"form":form,"id":td.val()};
    	}
    }

} ]);
