(function(sibbac20, angular, gc, undefined) {
  "use strict";

  var ClearingEfectivoController = function(isinService, cuentaLiquidacionService, conciliacionClearingService, growl,
      $filter) {
    gc.ClearingConsultaController.call(this, isinService, cuentaLiquidacionService, conciliacionClearingService, growl,
      $filter)
  };
  
  ClearingEfectivoController.prototype = Object.create(gc.ClearingConsultaController.prototype);

  ClearingEfectivoController.prototype.constructor = ClearingEfectivoController;

  ClearingEfectivoController.prototype.consultar = function() {
    inicializarLoading();
    this.desformatearFechasFiltro();
    this.conciliacionClearingService.getConciliacionEfectivoER(this.filter, this.populateDataTables.bind(this),
        this.showError.bind(this));
  };

  ClearingEfectivoController.prototype.populateDataTableT = function(datosT) {
    var difClassI = '', rownum = 0;

    this.oTableT.fnClearTable();
    if (datosT == null) {
      return;
    }
    angular.forEach(datosT, function(val, key) {
      var difClassI, iguales, sistema, mostrarFila = true;

      iguales = val.efectivoSV === val.efectivoCustodio && val.efectivoSV === val.efectivoCamara;
      difClassI = (!iguales) ? 'colorFondo' : 'centrado';
      sistema = (val.sistemaLiq) ? val.sistemaLiq : " ";
      var diferencia = '-';
      var diferenciaCamara = '-';
      var efectivoSV = val.efectivoSV;
      var efectivoCustodio = val.efectivoCustodio;
      var efectivoCamara = val.efectivoCamara;
      var settlementdate = val.settlementDate !== null ? EpochToHumanSinHora(val.settlementDate) : '';
      if (efectivoSV !== null) {
        if (val.efectivoCustodio !== null) {
          diferencia = val.efectivoSV - val.efectivoCustodio;
        }
        if (val.titulosCamara !== null) {
          diferenciaCamara = val.efectivoSV - efectivoCamara;
        }
      }
      else {
        efectivoSV = '-';
        diferencia = efectivoCustodio * -1;
        diferenciaCamara = efectivoCamara * -1;
      }
      if (efectivoCustodio === null) {
        efectivoCustodio = '-';
        diferencia = efectivoSV;
      }
      if (efectivoCamara === null) {
        efectivoCamara = '-';
        diferenciaCamara = efectivoSV;
      }
      this.oTableT.fnAddData([
        "<span>" + val.isin + "</span>", "<span>" + val.descripcion === null ? "" : val.descripcion + "</span>",
        "<span>" + $.number(efectivoSV, 0, ',', '.') + "</span>",
        "<span>" + $.number(efectivoCustodio, 0, ',', '.') + "</span>",
        '<span>' + $.number(val.efectivoCamara, 0, ',', '.') + '</span>',
        "<span>" + $.number(diferencia, 0, ',', '.') + "</span>",
        '<span>' + $.number(diferenciaCamara, 0, ',', '.') + '</span>', "<span>" + settlementdate + "</span>",
        "<span>" + val.codigoCuentaLiquidacion + "</span>"//,
        //"<span>" + val.mercado + "</span>"
      ], false);
      angular.element(this.oTableT.fnGetNodes(rownum++)).addClass(difClassI);
    }, this);
    this.oTableT.fnDraw();
  };

  ClearingEfectivoController.prototype.loadDataTableT = function() {
    this.oTableT = angular.element("#tblClearingEfectivoTotal").dataTable({
      "dom" : 'T<"clear">lfrtip', "bSort" : true, "tableTools" : {
        "sSwfPath" : "js/swf/copy_csv_xls_pdf.swf"
      }, "language" : {
        "url" : "i18n/Spanish.json"
      }, "scrollY" : "480px", "scrollCollapse" : true, "aoColumns" : [{
        "width" : "auto", "sClass" : "centrado"
      },// ISIN
      {
        "width" : "auto", "sClass" : "align-left"
      },// DESCRIPCION ISIN
      {
        "width" : "auto", "sClass" : "align-right", "type" : 'formatted-num'
      },// EFECTIVO
      // SV
      {
        "width" : "auto", "sClass" : "align-right", "type" : 'formatted-num'
      },// EFECTIVO
      // CUSTODIO
      {
        "sClass" : "align-right"
      },// EFECTIVO CAMARA
      {
        "width" : "auto", "sClass" : "monedaR", "type" : 'formatted-num'
      }, // DIFERENCIA
      // S3
      {
        "sClass" : "align-right"
      },// DIFERENCIA CAMARA
      {
        "width" : "auto", "sClass" : "centrado"
      },// FECHA LIQUIDACION
      {
        "width" : "auto", "sClass" : "centrado"
      }// ,//CUENTA LIQUIDACION
      // {"width": "auto","sClass": "centrado"}//MERCADO
      ]
    });
  }

  sibbac20.controller('ClearingEfectivoController', [
    'IsinService', 'CuentaLiquidacionService', 'ConciliacionClearingService', 'growl', '$filter',
    ClearingEfectivoController]);

})(sibbac20, angular, GENERIC_CONTROLLERS);
