'use strict';
sibbac20
    .controller(
                'GestionPerfilesController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'GestionPerfilesService',
                 '$compile',
                 '$location',
                 'SecurityService',
                 function ($scope, $document, growl, GestionPerfilesService, $compile, $location, SecurityService) {

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   /***************************************************************************************************
                     * ** INICIALIZACION DE DATOS ***
                     **************************************************************************************************/
                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   $scope.followSearch = "";// contiene las migas de los filtros de búsqueda

                   $scope.rolFiltro = null;

                   // Variables y listas general
                   $scope.mostrarBuscador = true;
                   $scope.filtro = {};
                   $scope.listUsuarios = []; // Listado de todos los usuarios
                   $scope.listPerfiles = []; // Listado de todos los perfiles
                   $scope.listAcciones = []; // Listado de todas las acciones para pintar la cabecera del grid de
                   // asignacion de permiso al perfil
                   $scope.listPaginasAcciones = []; // Estructura generica con todas las paginas y sus acciones,
                   // utilizada al crear y modificar los perfiles
                   $scope.listadoPerfiles = []; // Listado de los perfiles que se cargan al consultar

                   $scope.listPaginasAccionesSeleccionadas = []; // Lista con los identificadores de las acciones sobre
                   // paginas seleccionas y que va a tener acceso el perfil

                   $scope.paginaAccion = {};

                   $scope.resetButtonFilters = function () {
                     $scope.btnsFilter = {
                       textPerfil : "IGUAL",
                       btnPerfil : "=",
                       colorPerfil : {
                         'color' : 'green'
                       }
                     };
                   };

                   // Actualiza el filtro con los datos que necesita el servicio
                   $scope.setFiltro = function () {
                     $scope.filtro.idPerfil = $scope.perfilFiltro.key;
                     $scope.filtro.datosPerfil = $scope.perfil;
                     if ($scope.filtro.datosPerfil != null) {
                       $scope.filtro.datosPerfil.listaPaginasAccion = $scope.listPaginasAccionesSeleccionadas;
                     }
                     $scope.filtro.listPaginasAccionesSeleccionadas = $scope.listPaginasAccionesSeleccionadas;
                     $scope.filtro.listaPerfilesBorrar = $scope.perfilesSeleccionadosBorrar;
                   };

                   // Reset del filtro
                   $scope.resetFiltro = function () {
                     $scope.filtro = {
                       notPerfil : false,
                       idPerfil : "0",
                       datosPerfil : null,
                       listaPerfilesBorrar : [],
                       listPaginasAccionesSeleccionadas : []
                     };

                     $scope.resetPaginaAccion = function () {
                       $scope.paginaAccion = {
                         idActionPage : '',
                         bAplicaPerfil : false
                       }
                     }

                     $scope.perfilFiltro = {
                       key : "0",
                       value : ""
                     };
                     $scope.resetButtonFilters();
                   };

                   $scope.resetFiltro();

                   // Reset del perfil
                   $scope.resetPerfil = function () {
                     $scope.perfil = {
                       idProfile : null,
                       name : "",
                       description : "",
                       listaUsuarios : []
                     // Se inicializa con toda la estructura sin asignarle ningun permiso.
                     }

                     $scope.activeAlert = false;
                     $scope.mensajeAlert = "";

                   }

                   $scope.resetButtonFilters();

                   $scope.modal = {
                     titulo : "",
                     showCrear : false,
                     showModificar : false
                   };

                   /***************************************************************************************************
                     * ** DEFINICION TABLA ***
                     **************************************************************************************************/
                   // Listado de perfiles
                   $scope.oTable = $("#datosPerfiles").dataTable({
                     "dom" : 'T<"clear">lfrtip',
                     "tableTools" : {
                       "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                       "aButtons" : [ "copy", {
                         "sExtends" : "csv",
                         "sFileName" : "Listado_Permisos_" + hoy + ".csv",
                         "mColumns" : [ 2, 3 ]
                       }, {
                         "sExtends" : "xls",
                         "sFileName" : "Listado_Permisos_" + hoy + ".xls",
                         "mColumns" : [ 2, 3 ]
                       }, {
                         "sExtends" : "pdf",
                         "sPdfOrientation" : "landscape",
                         "sTitle" : " ",
                         "sPdfSize" : "A3",
                         "sPdfMessage" : "Listado Permisos",
                         "sFileName" : "Listado_Permisos_" + hoy + ".pdf",
                         "mColumns" : [ 2, 3 ]
                       }, "print" ]
                     },
                     "aoColumns" : [ {
                       sClass : "centrar",
                       bSortable : false,
                       width : "7%"
                     }, {
                       sClass : "centrar",
                       width : "40%"
                     }, {
                       sClass : "centrar",
                       width : "53%"
                     } ],
                     "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                       $compile(nRow)($scope);
                     },

                     "scrollY" : "480px",
                     "scrollX" : "100%",
                     "scrollCollapse" : true,
                     "language" : {
                       "url" : "i18n/Spanish.json"
                     }
                   });

                   $scope.borrarTabla = function () {
                     // borra el contenido del body de la tabla
                     var tbl = $("#datosPerfiles > tbody");
                     $(tbl).html("");
                     $scope.oTable.fnClearTable();
                   }

                   // Borrar el DataTable con los check de permisos y acciones anteriores
                   $scope.borrarPermisosAccionesAnteriores = function () {
                     if ($scope.oTablePerfilesAccesos != null) {
                       $scope.oTablePerfilesAccesos.fnClearTable();
                       var table = $('#datosPaginasAccesosProfile').DataTable();
                       table.destroy();
                     }
                     $("#datosPaginasAccesosProfile").remove();
                   }

                   $scope.seleccionarElemento = function (row) {
                     if ($scope.listadoPerfiles[row].selected) {
                       $scope.listadoPerfiles[row].selected = false;
                       for (var i = 0; i < $scope.perfilesSeleccionados.length; i++) {
                         if ($scope.perfilesSeleccionados[i].idProfile === $scope.listadoPerfiles[row].idProfile) {
                           $scope.perfilesSeleccionados.splice(i, 1);
                           $scope.perfilesSeleccionadosBorrar.splice(i, 1);
                         }
                       }
                     } else {
                       $scope.listadoPerfiles[row].selected = true;
                       $scope.perfilesSeleccionados.push($scope.listadoPerfiles[row]);
                       $scope.perfilesSeleccionadosBorrar.push($scope.listadoPerfiles[row].idProfile);

                     }
                   };

                   $scope.seleccionarElementoPaginaPerfil = function (fila, columna, idActionPage) {

                     if ($scope.listadoAccionesPaginasPerfil[fila][columna].bAplicaPerfil) {
                       $scope.listadoAccionesPaginasPerfil[fila][columna].bAplicaPerfil = false;
                       for (var i = 0; i < $scope.listPaginasAccionesSeleccionadas.length; i++) {
                         if ($scope.listPaginasAccionesSeleccionadas[i] === idActionPage) {
                           $scope.listPaginasAccionesSeleccionadas.splice(i, 1);
                         }
                       }

                     } else { // Lo ha seleccionado y hay que incluirlo
                       $scope.listadoAccionesPaginasPerfil[fila][columna].bAplicaPerfil = true;
                       $scope.listPaginasAccionesSeleccionadas.push(idActionPage);
                     }
                   };

                   $scope.seleccionarFilaPaginaPerfil = function (fila, idPage) {

                     var listaAccionesPagina = $scope.listadoAccionesPaginasPerfil[fila];
                     var bSeleccionar = true; // Indica si ha marcado la pagina o la ha desmarcado.
                     if ($scope.listadoPaginasSeleccionadas[fila]) {
                       var bSeleccionar = false;
                     }

                     $scope.listadoPaginasSeleccionadas[fila] = bSeleccionar;
                     // Se recorren todas las acciones de la pagina realizando las siguientes acciones:
                     // Si ha marcado seleccionar pagina, se seleccionan todas las acciones que no estuvieran
                     // seleccionadas y viceversa
                     for (var i = 0; i < listaAccionesPagina.length; i++) {
                       if ($scope.listadoAccionesPaginasPerfil[fila][i].idActionPage != 0) {

                         // Solo se cambia el estado de la accion aquellas que tuvieran un estado distinto al check de
                         // la pagina
                         if ($scope.listadoAccionesPaginasPerfil[fila][i].bAplicaPerfil != bSeleccionar) {
                           $scope
                               .seleccionarElementoPaginaPerfil(
                                                                fila,
                                                                i,
                                                                $scope.listadoAccionesPaginasPerfil[fila][i].idActionPage);

                         }
                       }
                     }

                   };

                   // Asigna a ese perfil todas las acciones
                   $scope.seleccionarTodasAcciones = function () {
                     // Se inicializa las acciiones seleccioandas
                     $scope.listPaginasAccionesSeleccionadas = [];
                     // Se recorre la matriz y se añaden todas las acciones a la lista de selecionados y se marcan
                     // todos los check.
                     for (var i = 0; i < $scope.listadoAccionesPaginasPerfil.length; i++) {
                       var listaAccionesPagina = $scope.listadoAccionesPaginasPerfil[i];
                       for (var j = 0; j < listaAccionesPagina.length; j++) {

                         if (listaAccionesPagina[j].idActionPage != 0) {
                           $scope.listadoAccionesPaginasPerfil[i][j].bAplicaPerfil = true;
                           $scope.listPaginasAccionesSeleccionadas.push(listaAccionesPagina[j].idActionPage);
                         }
                       }
                     }
                     // Se recorren todos los check de pagina y se marcan
                     for (var i = 0; i < $scope.listadoPaginasSeleccionadas.length; i++) {
                       $scope.listadoPaginasSeleccionadas[i] = true;
                     }
                   }

                   // Quita a ese perfil todas las acciones
                   $scope.deSeleccionarTodasAcciones = function () {
                     // Se inicializa las acciiones seleccioandas
                     $scope.listPaginasAccionesSeleccionadas = [];
                     // Se recorre la matriz y se desmarcan todos los check.
                     for (var i = 0; i < $scope.listadoAccionesPaginasPerfil.length; i++) {
                       var listaAccionesPagina = $scope.listadoAccionesPaginasPerfil[i];
                       for (var j = 0; j < listaAccionesPagina.length; j++) {
                         $scope.listadoAccionesPaginasPerfil[i][j].bAplicaPerfil = false;
                       }
                     }
                     // Se recorren todos los check de pagina y se desmarcan
                     for (var i = 0; i < $scope.listadoPaginasSeleccionadas.length; i++) {
                       $scope.listadoPaginasSeleccionadas[i] = false;
                     }
                   }

                   // Funcion general para pintar datatable con las paginas y los permisos
                   $scope.cargarPaginasPerfiles = function (data) {

                     // borro los datos anteriores
                     $scope.borrarPermisosAccionesAnteriores();

                     var contenidoColumnas = [];
                     var contenidoFilas = [];

                     var listaCheckPagina = [];

                     $scope.listadoAccionesPaginasPerfil = [];
                     $scope.listadoPaginasSeleccionadas = []; // Listado que almacena que paginas tiene seleccionadas el

                     // Columna nombre funcionalidad
                     var cabeceracheck = {
                       sTitle : " ",
                       bSortable : false
                     }
                     contenidoColumnas.push(cabeceracheck);

                     // Columna nombre funcionalidad
                     var cabeceraPagina = {
                       sTitle : "Funcionalidad"
                     }
                     contenidoColumnas.push(cabeceraPagina);

                     $.each($scope.listAcciones, function (index_i, registro) {
                       var ao = {
                         sTitle : registro.value,
                         sClass : "centrar",
                         bSortable : false
                       };
                       contenidoColumnas.push(ao);
                     });

                     // Formo las filas.
                     $
                         .each(
                               data,
                               function (index_i, registro) {
                                 var contenidoFila = [];
                                 var checkPagina = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listadoPaginasSeleccionadas['
                                                   + index_i
                                                   + ']" ng-click="seleccionarFilaPaginaPerfil('
                                                   + index_i
                                                   + ',' + registro.idPage + ');"/>';

                                 $scope.listadoPaginasSeleccionadas.push(false);
                                 // Check
                                 contenidoFila.push(checkPagina);
                                 // Descripcion pagina (contiene ruta)
                                 contenidoFila.push(registro.description);
                                 var listaAccionesPaginaProfile = registro.listaAccionesPaginaProfile;

                                 $
                                     .each(
                                           listaAccionesPaginaProfile,
                                           function (index_j, contenido) {
                                             $scope.resetPaginaAccion();

                                             $scope.paginaAccion.idActionPage = contenido.idActionPage;

                                             if ($scope.listPaginasAccionesSeleccionadas
                                                 .indexOf(contenido.idActionPage) != -1) {
                                               $scope.paginaAccion.bAplicaPerfil = true;
                                             }

                                             listaCheckPagina.push($scope.paginaAccion);

                                             if (contenido.idActionPage != 0) {
                                               var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listadoAccionesPaginasPerfil['
                                                           + index_i
                                                           + ']['
                                                           + index_j
                                                           + '].bAplicaPerfil " ng-click="seleccionarElementoPaginaPerfil('
                                                           + index_i
                                                           + ','
                                                           + index_j
                                                           + ','
                                                           + contenido.idActionPage
                                                           + ');"/>';

                                               contenidoFila.push(check);
                                             } else {
                                               contenidoFila.push(' ');
                                             }

                                           });

                                 contenidoFilas.push(contenidoFila);
                                 $scope.listadoAccionesPaginasPerfil.push(listaCheckPagina);
                                 listaCheckPagina = [];
                               });

                     $('#contenedordatos')
                         .append(
                                 '<table id="datosPaginasAccesosProfile" class="fullTable ancha" cellspacing="0"></table>');
                     // Se pintan las columnas en la tabla
                     $scope.oTablePerfilesAccesos = $("#datosPaginasAccesosProfile").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                         "aButtons" : []
                       },
                       "aoColumns" : contenidoColumnas,
                       "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                         $compile(nRow)($scope);
                       },
                       "scrollY" : "330px",
                       "scrollX" : "100%",
                       "scrollCollapse" : true,
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       }
                     });

                     // Se pinta el contenido en la tabla
                     $scope.oTablePerfilesAccesos.fnAddData(contenidoFilas, false);
                     // Se desbloquea la página
                     $.unblockUI();

                   }

                   /***************************************************************************************************
                     * ** FUNCIONES GENERALES ***
                     **************************************************************************************************/

                   // Control para ocultar o presentar el panel de busqueda
                   $scope.mensajeBusqueda = "Ocultar opciones de búsqueda";
                   $scope.mostrarOcultarFiltros = function () {
                     angular.element("#buscador").slideToggle();
                     if ($scope.mostrarBuscador) {

                       if ($scope.perfilFiltro.value == null) {
                         $scope.followSearch += "  Perfil: TODOS";
                       } else if ($scope.perfilFiltro.value.length > 0) {
                         $scope.followSearch += "  Perfil" + $scope.btnsFilter.btnPerfil + " "
                                                + $scope.perfilFiltro.value;
                       } else {
                         $scope.followSearch += "  Perfil: TODOS";
                       }

                       $scope.mostrarBuscador = false;
                       $scope.mensajeBusqueda = "Mostrar opciones de búsqueda";
                     } else {
                       $scope.followSearch = "";
                       $scope.mostrarBuscador = true;
                       $scope.mensajeBusqueda = "Ocultar opciones de búsqueda";
                     }
                   }

                   $scope.controlError = function (txterror) {
                     if (txterror.length == 0) {
                       txterror = "Error genérico, por favor contacte con Soporte IT.";
                     }
                     fErrorTxt(txterror, 1);
                     $scope.mensajeError = txterror;
                     $scope.mostrarErrores = true;
                   }

                   var pupulateAutocomplete = function (input, availableTags) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {

                         var option = {
                           key : ui.item.key,
                           value : ui.item.value
                         };

                         switch (input) {
                           case "#filtro_rol":
                             $scope.filtro.idPerfil = option.key;
                             $scope.rolFiltro = option.value;
                             break;
                           default:
                             break;
                         }

                         $scope.safeApply();

                         return false;
                       }
                     });
                   };

                   $scope.invertirValores = function (option) {

                     switch (option) {
                       case "perfil":
                         if (!$scope.filtro.notPerfil) {
                           $scope.filtro.notPerfil = true;
                           $scope.btnsFilter.textPerfil = "DISTINTO";
                           $scope.btnsFilter.btnPerfil = "<>";
                           $scope.btnsFilter.colorPerfil = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notPerfil = false;
                           $scope.btnsFilter.textPerfil = "IGUAL";
                           $scope.btnsFilter.btnPerfil = "=";
                           $scope.btnsFilter.colorPerfil = {
                             'color' : 'green'
                           };
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   /***************************************************************************************************
                     * ** CARGA DE DATOS INICIAL ***
                     **************************************************************************************************/

                   // Carga combo Perfiles
                   GestionPerfilesService.cargaPerfiles(function (data) {
                     $scope.listPerfiles = data.resultados.listaPerfiles;
                     // pupulateAutocomplete("#filtro_perfil", $scope.listadoPerfiles);
                   }, function (error) {
                     fErrorTxt("Se produjo un error en la carga de los perfiles.", 1);
                   });

                   // Carga combo Usuarios
                   GestionPerfilesService.cargaUsuarios(function (data) {
                     $scope.listUsuarios = data.resultados.listaUsuarios;
                     // pupulateAutocomplete("#filtro_usuario", $scope.listUsuarios);
                   }, function (error) {
                     fErrorTxt("Se produjo un error en la carga de los usuarios.", 1);
                   });

                   // Carga Lista de accioines para la cabecera de la tabla de asignacion de permisos
                   GestionPerfilesService.cargaAcciones(function (data) {
                     $scope.listAcciones = data.resultados.listaAcciones;
                   }, function (error) {
                     fErrorTxt("Se produjo un error en la carga de las acciones .", 1);
                   });

                   // Carga Lista de paginas con funcionalidad presentes en el sistema y las acciones que se pueden
                   // hacer en cada una de ellas.
                   // Se muestra la capa cargando
                   inicializarLoading();
                   GestionPerfilesService.cargaPaginasAcciones(function (data) {
                     $scope.listPaginasAcciones = data.resultados.listaPaginasAcciones;
                     $.unblockUI();
                   }, function (error) {
                     fErrorTxt("Se produjo un error en la carga de las paginas y sus acciones .", 1);
                     $.unblockUI();
                   });

                   /***************************************************************************************************
                     * ** ACCIONES ***
                     **************************************************************************************************/

                   $scope.pintarTablaPerfiles = function (data) {

                     // se inicializan las variables de perfiles seleccionados
                     $scope.perfilesSeleccionadosBorrar = [];
                     $scope.perfilesSeleccionados = [];

                     // borra el contenido del body de la tabla
                     $scope.borrarTabla();

                     $scope.listadoPerfiles = data.resultados["listaPerfilesFiltroDto"];

                     for (var i = 0; i < $scope.listadoPerfiles.length; i++) {

                       var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listadoPerfiles['
                                   + i + '].selected" ng-click="seleccionarElemento(' + i + ');"/>';
                       var perfilList = [ check, $scope.listadoPerfiles[i].name, $scope.listadoPerfiles[i].description ];

                       $scope.oTable.fnAddData(perfilList, false);

                     }

                     $scope.oTable.fnDraw();
                     $.unblockUI();

                   }

                   // Consulta de los datos y carga del grid con el filtro establecido.
                   $scope.getPerfilesFiltro = function () {

                     // Se inicializa el filtro.
                     $scope.setFiltro();

                     // Se oculta el panel de búsqueda y se muestra el detalle
                     $scope.mostrarOcultarFiltros();
                     // Se muestra la capa cargando
                     inicializarLoading();

                     // Se obtienen los datos.
                     GestionPerfilesService.cargarPerfilesFiltro(function (data) {
                       $scope.pintarTablaPerfiles(data);
                     }, function (error) {
                       $.unblockUI();
                       fErrorTxt("Se produjo un error en la carga del listado de Perfiles.", 1);
                     }, $scope.filtro);

                   };

                   $scope.seleccionarTodos = function () {
                     // Se inicializa el de los perfiles a borrar.
                     $scope.perfilesSeleccionadosBorrar = [];
                     for (var i = 0; i < $scope.listadoPerfiles.length; i++) {
                       $scope.listadoPerfiles[i].selected = true;

                       $scope.perfilesSeleccionadosBorrar.push($scope.listadoPerfiles[i].idProfile);
                     }
                     $scope.perfilesSeleccionados = angular.copy($scope.listadoPerfiles);
                   };

                   $scope.deseleccionarTodos = function () {
                     for (var i = 0; i < $scope.listadoPerfiles.length; i++) {
                       $scope.listadoPerfiles[i].selected = false;
                     }
                     $scope.perfilesSeleccionados = [];
                     $scope.perfilesSeleccionadosBorrar = [];
                   };

                   /***************************************************************************************************
                     * ** ACCIONES CRUD / MODALES
                     **************************************************************************************************/

                   // Abrir modal creacion perfiles
                   $scope.abrirCrearPerfil = function () {
                     $scope.modal.showCrear = true;
                     $scope.modal.showModificar = false;
                     $scope.modal.titulo = "Crear Perfil";
                     $scope.resetPerfil();
                     $scope.listPaginasAccionesSeleccionadas = []; // Se inicializa la lista de acciones que puede
                     // hacer un perfil
                     $scope.cargarPaginasPerfiles($scope.listPaginasAcciones);
                     angular.element('#formularios').modal({
                       backdrop : 'static'
                     });
                     $(".modal-backdrop").remove();
                   };

                   // Abrir modal modificar usuarios
                   $scope.abrirModificarPerfil = function () {

                     if ($scope.perfilesSeleccionados.length > 1 || $scope.perfilesSeleccionados.length == 0) {
                       if ($scope.perfilesSeleccionados.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de perfiles.", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de perfiles.", 2)
                       }
                     } else {
                       // se copian los datos del usuario seleccionado en la variable usuario
                       $scope.perfil = angular.copy($scope.perfilesSeleccionados[0]);
                       $scope.listPaginasAccionesSeleccionadas = $scope.perfil.listaPaginasAccion;
                       $scope.modal.showCrear = false;
                       $scope.modal.showModificar = true;
                       $scope.modal.titulo = "Modificar Perfil";
                       $scope.cargarPaginasPerfiles($scope.listPaginasAcciones);
                       angular.element('#formularios').modal({
                         backdrop : 'static'
                       });
                       $(".modal-backdrop").remove();

                     }

                   };

                   // TODO: Meter el mismo nombre para que no le repita
                   $scope.procesarUsuario = function () {
                     var existe = false;
                     angular.forEach($scope.perfil.listaUsuarios, function (campo) {
                       if (campo.value === $scope.form.usuario.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.perfil.listaUsuarios.push($scope.form.usuario);
                     }
                     $scope.form.usuario = "";
                   };

                   $scope.eliminarElementoTabla = function (tabla) {
                     switch (tabla) {
                       case "usuario":
                         for (var i = 0; i < $scope.perfil.listaUsuarios.length; i++) {
                           if ($scope.perfil.listaUsuarios[i].value === $scope.perfil.usuario.value) {
                             $scope.perfil.listaUsuarios.splice(i, 1);
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.vaciarTabla = function (tabla) {
                     switch (tabla) {
                       case "usuario":
                         $scope.perfil.listaUsuarios = [];
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.validacionFormulario = function () {
                     if ($scope.perfil.name.length == 0) {
                       return true;
                     }
                   }

                   $scope.crearPerfil = function () {

                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       inicializarLoading();
                       // Se actualiza los datos del filtro.
                       $scope.setFiltro();
                       GestionPerfilesService.crearPerfil(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           $scope.pintarTablaPerfiles(data);
                           angular.element('#formularios').modal("hide");
                           fErrorTxt('Perfil ' + $scope.perfil.name + ' creado correctamente', 3);
                         }
                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al crear el perfil.", 1);
                       }, $scope.filtro);
                     }
                   };

                   $scope.modificarPerfil = function () {

                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       inicializarLoading();
                       // Se actualiza los datos del filtro.
                       $scope.setFiltro();
                       GestionPerfilesService.modificarPerfil(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           $scope.pintarTablaPerfiles(data);
                           angular.element('#formularios').modal("hide");
                           fErrorTxt('Perfil ' + $scope.perfil.name + ' modificado correctamente', 3);
                         }
                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al modificar el perfil.", 1);
                       }, $scope.filtro);
                     }
                   };

                   $scope.borrarPerfiles = function () {
                     if ($scope.perfilesSeleccionadosBorrar.length > 0) {
                       if ($scope.perfilesSeleccionadosBorrar.length == 1) {
                         angular.element("#dialog-confirm").html("¿Desea eliminar el perfil seleccionado?");
                       } else {
                         angular.element("#dialog-confirm").html(
                                                                 "¿Desea eliminar los "
                                                                     + $scope.perfilesSeleccionadosBorrar.length
                                                                     + " perfiles seleccionados?");
                       }

                       // Define the Dialog and its properties.
                       angular.element("#dialog-confirm").dialog({
                         resizable : false,
                         modal : true,
                         title : "Mensaje de Confirmación",
                         height : 150,
                         width : 360,
                         buttons : {
                           " Sí " : function () {
                             inicializarLoading();
                             // Se actualiza los datos del filtro.
                             $scope.setFiltro();

                             GestionPerfilesService.eliminarPerfiles(function (data) {
                               $scope.pintarTablaPerfiles(data);
                             }, function (error) {
                               $.unblockUI();
                               fErrorTxt("Ocurrió un error al eliminar los perfiles.", 1);
                             }, $scope.filtro);

                             $(this).dialog('close');
                           },
                           " No " : function () {
                             $(this).dialog('close');
                           }
                         }
                       });
                       $('.ui-dialog-titlebar-close').remove();
                     } else {
                       fErrorTxt("Debe seleccionar al menos un elemento de la tabla de perfiles.", 2)
                     }
                   };

                 } ]);
