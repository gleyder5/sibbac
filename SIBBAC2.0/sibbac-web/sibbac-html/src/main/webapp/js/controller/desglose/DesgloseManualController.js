(function(angular, sibbac20, gc, undefined){
  "use strict";
  
  var DesgloseManualController = function($routeParams, service, httpParamSerializer) {
    gc.FilteredGridController.call(this, service, httpParamSerializer);
    this.alloId = {nuorden: $routeParams.nuorden,
      nbooking: $routeParams.nbooking, nucnfclt: $routeParams.nucnfclt
    };
    service.consultaDesglose(this.alloId, this.receiveAlo.bind(this), this.queryFail.bind(this));
    
    
    
  };
  
  DesgloseManualController.prototype = Object.create(gc.FilteredGridController.prototype);
  
  DesgloseManualController.prototype.constructor = DesgloseManualController;
  
  DesgloseManualController.prototype.createGrid = function() {
    var grid = gc.FilteredGridController.prototype.createGrid.call(this);
    grid.multiSelect = false;
    return grid;
  };
  
  DesgloseManualController.prototype.receiveAlo = function(alo) {
    this.alo = alo;
    this.service.executeQuery("alcs", this.alloId, this.queryReceiveData.bind(this), this.queryFail.bind(this));
  };
  
  sibbac20.controller("DesgloseManualController",["$routeParams", "BusquedaAlosDesgloseManualService", 
    "$httpParamSerializer", DesgloseManualController]);
    
})(angular, sibbac20, GENERIC_CONTROLLERS);