'use strict';
sibbac20
    .controller(
                'ReportesCNMVController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'ReportesCNMVService',
                 'IsinService',
                 '$compile',
                 function ($scope, $document, growl, ReportesCNMVService, IsinService, $compile) {
                   var horaMaxima = "235959";

                   var calcDataTableHeight = function (sY) {
                     console.log("calcDataTableHeight: " + angular.element('#tablaReporteCNMV').height());
                     console.log("sY: " + sY);
                     if (angular.element('#tablaReporteCNMV').height() > 500) {
                       return 480;
                     } else {
                       return $('#tablaReporteCNMV').height() + 20;
                     }
                   };

                   function ajustarAltoTabla () {
                     console.log("ajustando inicio");
                     var oSettings = oTable.fnSettings();
                     console.log("ajustando 1");
                     var scrill = calcDataTableHeight(oTable.fnSettings().oScroll.sY);
                     console.log("cuanto: " + scrill);
                     oSettings.oScroll.sY = scrill;
                     console.log("tamanio antes: " + $('.dataTables_scrollBody').height());
                     $('.dataTables_scrollBody').height(scrill);
                     console.log("tamanio despues: " + $('.dataTables_scrollBody').height());
                     console.log("ajustando 2");

                     console.log("ajustando fin");
                   }

                   var oTable = undefined;
                   $scope.numero_fila = 0;
                   $scope.fechaliq = [];
                   var openTitulares = [];
                   $scope.estEnvSeek = ""
                   $scope.filtro = {
                     fliquidacionDe : "",
                     fliquidacionA : "",
                     fejecucionDe : "",
                     fejecucionA : "",
                     estoper : "A",
                     estenv : "",
                     isin : "",
                     chestenv : true,
                     chisin : true
                   };
                   $scope.modiTitu = true;
                   $scope.followSearch = "";// contiene las migas de los filtros de búsqueda
                   $scope.nombreExportacionFicheros = "ReporteCNMV";
                   $scope.listaChk = [];
                   $scope.titularModi = [];
                   $scope.listaChkCrear = [];
                   $scope.listaChkCrearNbook = [];
                   $scope.lenChkCrear = 0;
                   $scope.listaEstadosEnv = {};
                   $scope.tipoModificacionSimple = true;
                   $scope.nTitulosDesglose = 0;
                   $scope.numTitulosActualesDesglose = 0;
                   $scope.nTitulosDisponibles = 0;
                   var params = [];

                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   var lMarcar = true;

                   var alPaginas = [];
                   var pagina = 0;
                   var paginasTotales = 0;
                   var ultimaPagina = 0;

                   $scope.listaDesglosesSelect = [];
                   $scope.listaDesglosesCrear = [];
                   var iNumDesgloseCreados = 0;

                   var iNumDesgloseActual = 0;

                   $document.ready(function () {
                     initialize();
                     loadDataTable();
                     prepareCollapsion();

                     angular.element('#limpiar').click(function (event) {
                       // ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
                       $("#fejecucionA").datepicker("option", "minDate", "");
                       $("#fejecucionDe").datepicker("option", "maxDate", "");
                       $("#fliquidacionA").datepicker("option", "minDate", "");
                       $("#fliquidacionDe").datepicker("option", "maxDate", "");
                       //
                       event.preventDefault();
                       angular.element('input[type=text]').val('');
                       angular.element("#fejecucionDe").val('');
                       angular.element("#fejecucionA").val('');
                       angular.element("#fliquidacionDe").val('');
                       angular.element("#fliquidacionA").val('');
                       angular.element("#estenv").val('');
                       angular.element("#estoper").val('');

                       $scope.filtro = {
                         fliquidacionDe : "",
                         fliquidacionA : "",
                         fejecucionDe : "",
                         fejecucionA : "",
                         estoper : "A",
                         estenv : "",
                         isin : "",
                         chestenv : false,
                         chisin : false
                       };
                       $scope.btnInvGenerico('Estenv');
                       $scope.btnInvGenerico('Isin');

                     });

                     $scope.Consultar = function () {

                       cargarDatosReporteCNMV();
                       seguimientoBusqueda();
                       collapseSearchForm();
                       angular.element('#MarcarTod').css('display', 'inline');
                       angular.element('#MarcarPag').css('display', 'inline');
                     }
                     // Activa la sublinea de los Titulares.

                     // var fechas = [ 'fejecucionA','fejecucionDe','fliquidacionA','fliquidacionDe' ];
                     // loadpicker(fechas);
                     $("#ui-datepicker-div").remove();
                     $('input#fejecucionDe').datepicker({
                       onClose : function (selectedDate) {
                         $("#fejecucionA").datepicker("option", "minDate", selectedDate);
                       } // onClose
                     });

                     $('input#fejecucionA').datepicker({
                       onClose : function (selectedDate) {
                         $("#fejecucionDe").datepicker("option", "maxDate", selectedDate);
                       } // onClose
                     });
                     //
                     $('input#fliquidacionDe').datepicker({
                       onClose : function (selectedDate) {
                         $("#fliquidacionA").datepicker("option", "minDate", selectedDate);
                       } // onClose
                     });

                     $('input#fliquidacionA').datepicker({
                       onClose : function (selectedDate) {
                         $("#fliquidacionDe").datepicker("option", "maxDate", selectedDate);
                       } // onClose
                     });
                   }); // $document.ready

                   /** Inicialización de la página. */
                   function initialize () {
                     inicializarLoading();
                     IsinService.getIsines(onSuccessRequestIsines, onErrorRequest);
                     verificarFechas([ [ "fejecucionDe", "fejecucionA" ], [ "fliquidacionDe", "fliquidacionA" ] ]);
                     cargaComboEnvios();
                     cargaComboEnviosMod();
                     caragaCombotipoDocumento()
                     getHoraMaxima();
                     $.unblockUI();
                   } // initialize

                   function onErrorRequest (data) {
                     $.unblockUI();
                     fErrorTxt("Ocurrió un error durante la petición de datos.", 1);
                   } // onErrorRequest

                   /** Carga el autocompletable de isines. */
                   function onSuccessRequestIsines (datosIsin) {
                     var availableTags = [];
                     angular.forEach(datosIsin, function (val, key) {
                       var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                       availableTags.push(ponerTag);
                     });
                     angular.element("#isin").autocomplete({
                       source : availableTags
                     });
                   } // onSuccessRequestIsines

                   function loadDataTable () {
                     oTable = angular.element("#tablaReporteCNMV").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : [ "/sibbac20/js/swf/copy_csv_xls_pdf.swf" ],
                         "aButtons" : [ "copy", {
                           "sExtends" : "csv",
                           "sFileName" : "Listado_de_Reporte_CNMV_" + hoy + ".csv",
                         }, {
                           "sExtends" : "xls",
                           "sFileName" : "Listado_de_Reporte_CNMV_" + hoy + ".csv",
                         }, {
                           "sExtends" : "pdf",
                           "sPdfOrientation" : "landscape",
                           "sTitle" : " ",
                           "sPdfSize" : "A4",
                           "sPdfMessage" : "Listado de Reporte CNMV.",
                           "sFileName" : "Listado_de_Reporte_CNMV_" + hoy + ".pdf",
                         }, "print" ]
                       },

                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "aoColumns" : [ {
                         className : 'checkbox',
                         targets : 0,
                         sClass : "centrar",
                         type : "checkbox",
                         bSortable : false,
                         name : "active",
                         width : 50
                       }, {
                         className : 'details-tit',
                         sClass : "centrar",
                         targets : 1,
                         bSortable : false,
                         width : 50
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar",
                         "type" : "date-eu"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar",
                         "type" : "date-eu"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar"
                       }, {
                         sClass : "centrar",
                         visible : false
                       }, {
                         sClass : "centrar",
                         visible : false
                       }, {
                         sClass : "centrar",
                         visible : false
                       }, {
                         sClass : "centrar",
                         visible : false
                       }, {
                         sClass : "centrar",
                         visible : false
                       }, {
                         sClass : "centrar"
                       } ],
                       "order" : [ [ 1, "asc" ] ],
                       "scrollX" : "auto",
                       "scrollCollapse" : true,
                       "bProcessing" : true,
                       "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                         $compile(nRow)($scope);
                       },
                       drawCallback : function () {
                         ajustarAltoTabla();
                         processInfo(this.api().page.info());
                         if (alPaginas.length !== paginasTotales) {
                           alPaginas = new Array(paginasTotales);
                           for (var i = 0; i < alPaginas.length; i++) {
                             alPaginas[i] = true;
                           }
                         }

                         if (ultimaPagina !== pagina) {
                           var valor = alPaginas[pagina]

                           if (valor) {
                             angular.element("#MarcarPag").value = "Marcar Página";
                           } else {
                             if (lMarcar) {
                               angular.element("#MarcarPag").value = "Desmarcar Página";
                             }
                           }

                         }
                         ultimaPagina = pagina;
                       }
                     }); // oTable
                   }

                   /**
                     * Prepara los datos para la petición de datos al servidor.
                     */
                   function cargarDatosReporteCNMV () {
                     inicializarLoading();
                     console.log("cargarDatosReporteCNMV");
                     oTable.fnClearTable();

                     var fejecucionDeFormato = $scope.filtro.fejecucionDe;
                     if (fejecucionDeFormato != null && fejecucionDeFormato != undefined && fejecucionDeFormato !== '') {
                       var fcontratacionDeVector = fejecucionDeFormato.split("/");
                       fejecucionDeFormato = fcontratacionDeVector[2] + fcontratacionDeVector[1]
                                             + fcontratacionDeVector[0];
                     }
                     var fejecucionAFormato = $scope.filtro.fejecucionA;
                     if (fejecucionAFormato != null && fejecucionAFormato != undefined && fejecucionAFormato !== '') {
                       var fcontratacionAVector = fejecucionAFormato.split("/");
                       fejecucionAFormato = fcontratacionAVector[2] + fcontratacionAVector[1] + fcontratacionAVector[0];
                     }

                     var feliquidacionDeFormato = $scope.filtro.fliquidacionDe;
                     if (feliquidacionDeFormato != null && feliquidacionDeFormato != undefined
                         && feliquidacionDeFormato !== '') {
                       var fliquidacionDeVectr = feliquidacionDeFormato.split("/");
                       feliquidacionDeFormato = fliquidacionDeVectr[2] + fliquidacionDeVectr[1]
                                                + fliquidacionDeVectr[0];
                     }
                     var feliquidacionAFormato = $scope.filtro.fliquidacionA;
                     if (feliquidacionAFormato != null && feliquidacionAFormato != undefined
                         && feliquidacionAFormato !== '') {
                       var fliquidacionAVectr = feliquidacionAFormato.split("/");
                       feliquidacionAFormato = fliquidacionAVectr[2] + fliquidacionAVectr[1] + fliquidacionAVectr[0];
                     }
                     var estenv = (!$scope.filtro.chestenv && $scope.filtro.estenv !== "" ? "#" : "")
                                  + $scope.filtro.estenv;
                     var isin = (!$scope.filtro.chisin && $scope.filtro.isin !== "" ? "#" : "") + getIsin();

                     var filtro = {
                       fejecucionDe : fejecucionDeFormato,
                       fejecucionA : fejecucionAFormato,
                       fliquidacionDe : feliquidacionDeFormato,
                       fliquidacionA : feliquidacionAFormato,
                       estoper : $scope.filtro.estoper,
                       estenv : estenv,
                       isin : isin
                     }

                     ReportesCNMVService.listaOperacionesH47(onSuccessListaReporteCNMV, onErrorRequest, filtro);
                   }
                   function onSuccessListaReporteCNMV (json) {
                     if (json !== null && json !== undefined) {
                       if (json.error !== null && json.error === "") {
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== null && json.resultados !== undefined
                                  && json.resultados.listadoOperacionesH47 !== undefined) {
                         $('#ModificarOperH47').css('display', 'inline');
                         var item = null;
                         var datos = json.resultados.listadoOperacionesH47;
                         oTable.fnClearTable();
                         var contador = 0;
                         angular
                             .forEach(
                                      datos,
                                      function (item, key) {
                                        contador++;
                                        $scope.listaChk[key] = false;
                                        var idName = "check_" + contador;
                                        if (item.estoper === 'B' || item.estenv == 'ENVIADA'
                                            || item.estenv === 'ACEPTADA CNMV' || item.estenv === 'ACEPTADA BME') {
                                          var mensaje = "";
                                          if (item.estenv === 'ENVIADA') {
                                            mensaje = 'No se puede seleccionar porque su estado de envio es ENVIADA';
                                          } else if (item.estenv === 'ACEPTADA CNMV') {
                                            mensaje = 'No se puede seleccionar porque su estado de envio es ACEPTADA CNMV';
                                          } else if (item.estenv === 'ACEPTADA BME') {
                                            mensaje = 'No se puede seleccionar porque su estado de envio es ACEPTADA BME';
                                          } else if (item.estoper === 'B') {
                                            mensaje = 'No se puede seleccionar porque su estado de operacion es BAJA';
                                          }
                                          var txtMarcar = '<label title="'
                                                          + mensaje
                                                          + '" class="editor-active" style="color: #ff0000;text-align: center;width:50px;"><img src="/sibbac20/images/Warning.ico"/></label>';
                                        } else {
                                          console.log(item.estenv);
                                          // var txtMarcar = "<a id='editarOpe_"+key+"'
                                          // ng-click='ModificarOperH47("+key+")' style='cursor: pointer;'>Editar</a>";
                                          var txtMarcar = '<input ng-model="listaChk['
                                                          + key
                                                          + ']" type="checkbox" class='
                                                          + (contador - 1)
                                                          + '  id="'
                                                          + idName
                                                          + '" name="'
                                                          + idName
                                                          + '"bvalue="N" class="editor-active" ng-click="checkListaChk()">';
                                        }
                                        // var detalle = "<img src='/sibbac20/images/add_verde.png'
                                        // ng-click='muestraDetalles("+ key+ ")' id='imgDetalle_"+ key + "' >";
                                        var titular = "<img src='/sibbac20/images/add_verde.png' ng-click='muestraTitulares("
                                                      + key + ")' id='imgTitulares_" + key + "' >";
                                        var horaNego = item.hNegociacion;
                                        var horaNegoFormato = "";
                                        if (horaNego !== null && horaNego !== undefined) {
                                          var horaNegoFormato = horaNego.substr(0, 2) + ":" + horaNego.substr(2, 2)
                                                                + ":" + horaNego.substr(4, 2);
                                        }
                                        var fechaNego = item.fhNegociacion;
                                        var fechaNegoFormato = "";
                                        if (fechaNego !== null && fechaNego !== undefined) {
                                          var fechaNegoFormato = fechaNego.substr(6, 2) + "/" + fechaNego.substr(4, 2)
                                                                 + "/" + fechaNego.substr(0, 4);
                                        }
                                        var fechaValor = item.fhValor;
                                        var fechaValorFormato = "";
                                        if (fechaValor !== null && fechaValor !== undefined) {
                                          var fechaValorFormato = fechaValor.substr(6, 2) + "/"
                                                                  + fechaValor.substr(4, 2) + "/"
                                                                  + fechaValor.substr(0, 4);
                                        }
                                        var sentido = '';
                                        if (item.sentido !== null) {
                                          if (item.sentido === 'C' || item.sentido === 'B') {
                                            sentido = 'COMPRA';
                                          } else {
                                            sentido = 'VENTA';
                                          }
                                        }

                                        var estOper = '';
                                        if (item.estoper !== null) {
                                          if (item.estoper === 'A') {
                                            estOper = 'ALTA';
                                          } else {
                                            estOper = 'BAJA';
                                          }
                                        }
                                        var rowIndex = oTable.fnAddData([
                                                                         txtMarcar,
                                                                         // detalle,
                                                                         titular,
                                                                         sentido,
                                                                         fechaNegoFormato,
                                                                         horaNegoFormato,
                                                                         item.capacidadNegociacion,
                                                                         fechaValorFormato,
                                                                         item.isin,
                                                                         item.titulos,
                                                                         item.titularPrincipal,
                                                                         item.nbooking + "/" + item.nucnfclt + "/"
                                                                             + item.nucnfliq, estOper, item.estenv,
                                                                         item.id, item.cantidadTotal,
                                                                         item.efectivoTotal,
                                                                         item.identificadorTransaccion,
                                                                         item.precioUnitario, item.mensajeError ],
                                                                        false);
                                        var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
                                        $('td', nTr)[1].setAttribute('style', 'cursor: pointer; color: #ff0000;');
                                      })
                         oTable.fnDraw();
                         ajustarAltoTabla();

                         openTitulares = new Array(contador, false);
                       }
                     }
                     $.unblockUI();
                   } // onSuccessLis

                   /** Crea la subtabla de datos. */
                   function processInfo (info) {

                     pagina = info.page;
                     paginasTotales = info.pages;
                     var regInicio = info.start;
                     var regFin = info.end;
                     return regFin - regInicio;

                   }

                   /** Devuelve el isin seleccionado en el desplegable. */
                   function getIsin () {

                     var isinCompleto = $("#isin").val();
                     // var isinCompleto = $scope.filtro.isin.trim();
                     var guion = isinCompleto.indexOf("-");
                     var isingood = isinCompleto.substring(0, guion);
                     if (guion > 0) {
                       // return isinCompleto.substring(0, guion);
                       return isingood;
                     } else {
                       return isinCompleto;
                     }
                   } // getIsin

                   /** Muesta la leyenda con los filtros aplicados a la búsqueda. */
                   function seguimientoBusqueda () {
                     $scope.followSearch = "";

                     if ($scope.filtro.estoper !== "") {
                       $scope.followSearch += " Estado de Operación: ";
                       $scope.followSearch += $scope.filtro.estoper === "A" ? 'ALTA' : 'BAJA';
                     }

                     if ($scope.filtro.estenv !== "") {
                       if ($scope.filtro.chestenv)
                         $scope.followSearch += " Estado de Envio: " + getEstadoSeleccEnv($scope.filtro.estenv).nombre;
                       else
                         $scope.followSearch += " Estado de Envio distinto: "
                                                + getEstadoSeleccEnv($scope.filtro.estenv).nombre;
                     }

                     if ($scope.filtro.isin !== "") {
                       if ($scope.filtro.chisin)
                         $scope.followSearch += " Isin: " + document.getElementById("isin").value;
                       else
                         $scope.followSearch += " Isin distinto: " + document.getElementById("isin").value;
                     }
                     if ($scope.filtro.fejecucionDe !== "") {
                       $scope.followSearch += " F. Ejecucion Desde: " + $scope.filtro.fejecucionDe;
                     }
                     if ($scope.filtro.fejecucionA !== "") {
                       $scope.followSearch += " F. Ejecucion Hasta: " + $('#fejecucionA').val();
                     }
                     if ($scope.filtro.fliquidacionDe !== "") {
                       $scope.followSearch += " F. liquidacion Desde: " + $scope.filtro.fliquidacionDe;
                     }
                     if ($scope.filtro.fliquidacionA !== "") {
                       $scope.followSearch += " F. liquidacion Hasta: " + $scope.filtro.fliquidacionA;
                     }

                   } // seguimientoBusqueda

                   /** Devuelve el estado seleccionado en el desplegable. */
                   function getEstadoSeleccEnv (idEstado) {
                     var estado = {};
                     angular.forEach($scope.listaEstadosEnv, function (val, key) {
                       if (idEstado == val.idestado) {
                         estado = val;
                         return false;
                       }
                     });
                     return estado;
                   } //

                   function cargaComboEnvios () {
                     var filtro = {};
                     ReportesCNMVService.cargaComboEnvios(onSuccessCargaEnvios, onErrorRequest, filtro);
                   }

                   function onSuccessCargaEnvios (json) {
                     var datos = {};
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultados !== null
                                  && json.resultados.listadoEstados !== undefined) {
                         datos = json.resultados.listadoEstados;
                         $scope.listaEstadosEnv = json.resultados.listadoEstados;
                         angular.forEach(datos, function (item, key) {
                           item = datos[key];
                           $('#estenv').append(
                                               "<option value='" + item.idestado + "'>(" + item.idestado + ") "
                                                   + item.nombre + "</option>");
                         })
                       }
                     }
                   }

                   $scope.muestraTitulares = function (fila) {

                     var aData = oTable.fnGetData(fila);
                     var elemento = angular.element('img[id=imgTitulares_' + fila + ']');

                     if (oTable.fnIsOpen(fila)) {
                       if (openTitulares[fila]) {
                         $(elemento[0]).attr('src', '/sibbac20/images/add_verde.png');
                         $(this).removeClass('open tit');
                         oTable.fnClose(fila);
                         openTitulares[fila] = false;
                       }
                     } else {
                       $(elemento[0]).attr('src', '/sibbac20/images/supr_verde.png');
                       openTitulares[fila] = true;
                       $(this).addClass('open tit');
                       oTable.fnOpen(fila, TablaTitulares(oTable, fila), 'details-tit');
                       ajustarAltoTabla();
                       // document.getElementById('div_tit' + fila).scrollIntoView();

                     }
                   }

                   // carga la tablita de los errores de un registro.
                   function TablaTitulares (Table, nTr) {

                     var aData = Table.fnGetData(nTr);

                     if (aData[11] === 'B' || aData[12] === 'ENVIADA' || aData[12] === 'ACEPTADA') {
                       $scope.modiTitu = false;
                     } else {
                       $scope.modiTitu = true;
                     }

                     $scope.numero_fila = nTr;
                     var resultadoTitulares = "<div border-width:0 !important;background-color:#dfdfdf; style='float:left;;margin-left: 80px; text-align: center;' id='div_tit_"
                                              + nTr
                                              + "' data-ng-controller='ReportesCNMVController'>"
                                              + "<table id='tablaTIT_"
                                              + nTr
                                              + "' style=margin-top:5px;'>"
                                              + "<tr>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>Editar</th>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>Tipo persona</th>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>Tipo documento</th>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>Documento</th>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>Nombre</th>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>T. doc. representante</th>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>Doc. representante</th>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>Nombre representante</th>"
                                              + "<th class='taleft' style='background-color: #000 !important;'>Cantidad valores</th>"
                                              + "</tr>";

                     desplegarTitulares(aData, nTr);

                     return resultadoTitulares;
                   }

                   function desplegarTitulares (aData, fila) {

                     var idOperacion = aData[13];
                     var filters = {
                       "idOperacionH47" : idOperacion
                     };
                     ReportesCNMVService.listaTitulares(onSuccessListaTitulares, onErrorRequest, filters);
                   }
                   ;

                   function onSuccessListaTitulares (json) {
                     console.log(json);
                     console.log(json.resultados);
                     var rowDetalle = "";

                     if (json.resultados !== undefined && json.resultados.listadoTitularesH47 !== undefined) {
                       var datos = json.resultados.listadoTitularesH47;
                       var item = null;
                       var contador = 0;
                       var rowTitu = ""
                       var numero_fila = $scope.numero_fila;
                       var nombreDeTabla = "#tablaTIT_" + numero_fila;

                       angular
                           .forEach(
                                    datos,
                                    function (item, k) {
                                      item = datos[k];
                                      var tsocie = (item.tpsocied !== null && item.tpsocied !== undefined) ? item.tpsocied
                                                                                                          : "";
                                      var tident = (item.tpidenti !== null && item.tpidenti !== undefined) ? item.tpidenti
                                                                                                          : "";
                                      var identi = (item.identificacion !== null && item.identificacion !== undefined) ? item.identificacion
                                                                                                                      : "";
                                      var nombre = (item.nombre !== null && item.nombre !== undefined) ? item.nombre
                                          .trim() : "";
                                      var apell1 = (item.apellido1 !== null && item.apellido1 !== undefined) ? item.apellido1
                                                                                                                .trim()
                                                                                                            : "";
                                      var apell2 = (item.apellido2 !== null && item.apellido2 !== undefined) ? item.apellido2
                                                                                                                .trim()
                                                                                                            : "";

                                      var rtident = (item.repTpidenti !== null && item.repTpidenti !== undefined) ? item.repTpidenti
                                                                                                                 : "";
                                      var ridenti = (item.repIdentificacion !== null && item.repIdentificacion !== undefined) ? item.repIdentificacion
                                                                                                                             : "";
                                      var rnombre = (item.repNombre !== null && item.repNombre !== undefined) ? item.repNombre
                                                                                                                 .trim()
                                                                                                             : "";
                                      var rapell1 = (item.repApellido1 !== null && item.repApellido1 !== undefined) ? item.repApellido1
                                                                                                                       .trim()
                                                                                                                   : "";
                                      var rapell2 = (item.repApellido2 !== null && item.repApellido2 !== undefined) ? item.repApellido2
                                                                                                                       .trim()
                                                                                                                   : "";

                                      var canti = (item.cantidadValores !== null && item.cantidadValores !== undefined) ? item.cantidadValores
                                                                                                                       : "";
                                      var id = (item.id !== null && item.id !== undefined) ? item.id : "";

                                      if ($scope.modiTitu) {
                                        var btnTitu = "<a id='editarTitu_" + id + "' ng-click='modifiTitu(" + id
                                                      + ")' style='cursor: pointer;'>Editar</a>";
                                      } else {
                                        var btnTitu = "";
                                      }

                                      rowTitu += "<tr>" + "<td>" + btnTitu + "</td>" + "<td>" + tsocie + "</td><td>"
                                                 + tident + "</td><td>" + identi + "</td><td>" + nombre + " " + apell1
                                                 + " " + apell2 + "<td>" + rtident + "</td><td>" + ridenti
                                                 + "</td><td>" + rnombre + " " + rapell1 + " " + rapell2 + "</td><td>"
                                                 + canti + "</td></tr>";

                                    })

                       angular.element(nombreDeTabla).append($compile(rowTitu)($scope));

                     }
                     $(nombreDeTabla).append("</table><div>");
                   }

                   $scope.CrearOperH47 = function () {

                     $('#formularioOperH47').attr('class', 'modal modalalnbooking');
                     $('#formularioOperH47').css('display', 'inline');
                     $('#buscarNbooking').css('display', 'inline');
                     $('#nbooking_SE').css('display', 'inline');
                     $('#labnbooking_SE').css('display', 'inline');
                     $('#divtablaCreacion').css('display', 'none');
                     $('#crearNbooking').css('display', 'none');
                     $('#formularioOperH47').css('left', '40%');
                     $('#nbooking_SE').val('');

                     $scope.listaChkCrear = [];
                     $scope.listaChkCrearNbook = [];

                   }
                   $scope.BuscarNbooking = function () {
                     console.log('Entra en bBuscarNbooking');
                     var nbooking = $('#nbooking_SE').val();

                     $scope.buscar(nbooking);

                   }

                   $scope.buscar = function (nbooking) {
                     console.log('Entra en buscar');
                     inicializarLoading();
                     var filtro = {
                       "nbooking" : nbooking
                     };
                     ReportesCNMVService.listaBookingCreacion(onSuccessBookingCreacion, onErrorRequest, filtro);
                   }

                   function onSuccessBookingCreacion (json) {
                     console.log(json);
                     console.log(json.resultados);
                     var rowCrear = "";
                     $.unblockUI();
                     if (json.error !== undefined && json.error !== null && json.error !== "") {
                       $('#formularioOperH47').css('display', 'none');
                       fErrorTxt(json.error, 1);
                     } else if (json.resultados !== undefined && json.resultados.listadoAlcsByBooking !== undefined) {

                       var table = document.getElementById('tablaCreacion');
                       var rowCount = table.rows.length;
                       for (var i = 1; i < rowCount; i++) {
                         var row = table.rows[i];
                         table.deleteRow(i);
                         rowCount--;
                         i--;

                       }

                       $('#tablaCreacion').remove('tr:gt(0)');
                       $('#formularioOperH47').attr('class', 'modal modalalto4');
                       $('#formularioOperH47').css('width', '1300px');
                       $('#formularioOperH47').css('left', '10%');
                       $('#buscarNbooking').css('display', 'none');
                       $('#nbooking_SE').css('display', 'none');
                       $('#labnbooking_SE').css('display', 'none');
                       $('#divtablaCreacion').css('display', 'inline');
                       var datos = json.resultados.listadoAlcsByBooking;
                       var item = null;
                       $scope.listaChkCrea = [];
                       $scope.lenChkCrear = json.resultados.listadoAlcsByBooking.length;
                       angular.forEach(datos, function (item, k) {
                         item = datos[k];
                         $scope.listaChkCrear[k] = false;
                         $scope.listaChkCrearNbook[k] = {
                           "nbooking" : item.nbooking,
                           "nucnfclt" : item.nucnfclt,
                           "nucnfliq" : item.nucnfliq,
                           "id" : item.id
                         }

                         // var fejecucionDeFormato =
                         // item.feejeliq.substr(6,2)+"/"+item.feejeliq.substr(4,2)+"/"+item.feejeliq.substr(0,4);

                         var fejecucionDeFormato = dd + "/" + mm + "/" + yyyy;

                         rowCrear += "<tr><td><input ng-model='sellistaChkcrear(" + k
                                     + ")' type='checkbox' id='chrCre_" + k + "'/>" + "</td><td>" + fejecucionDeFormato
                                     + "</td><td>" + item.sentido + "</td><td>" + item.isin + "</td><td>"
                                     + item.nombreIsin + "</td><td>" + item.titulos + "</td><td>" + item.precioUnitario
                                     + "</td><td>" + item.efectivoTotal + "</td><td>" + item.alias + "</td><td>"
                                     + item.nombreAlias + "</td><td>" + item.titularPrincipal + "</td><td>"
                                     + item.cdEstadoAsig + "</td><td>" + item.nbooking + "</td><td>" + item.nucnfclt
                                     + "</td><td>" + item.nucnfliq + "</td></tr>";

                       })
                       $('#tablaCreacion').append(rowCrear);
                     }
                     // $('#tablaCreacion').append("</table><div>" );
                     $('#crearNbooking').css('display', 'inline');
                   }

                   // cierra los formularios de creacion/modificacion
                   $scope.cerrar = function () {

                     $('#formularioOperH47').css('display', 'none');
                     $('#forModiOperH47').css('display', 'none');
                     $('#forModiEnvioOperH47').css('display', 'none');
                     $('#formularioTituH47').css('display', 'none');
                     $scope.esCreacionDeOperaciones = false;

                   }

                   $scope.CrearNbooking = function () {

                     $scope.listaDesglosesSelect = [];
                     $scope.listaDesglosesCrear = [];
                     iNumDesgloseCreados = 0;
                     iNumDesgloseActual = 0;
                     $scope.esCreacionDeOperaciones = true;

                     var param = [];
                     for (var i = 0, j = $scope.lenChkCrear; i < j; i++) {
                       var elemento = angular.element('input [id=chrCre_' + i + ']');
                       if (document.getElementById('chrCre_' + i).checked == true) {
                         $scope.listaDesglosesSelect.push($scope.listaChkCrearNbook[i]);
                       }
                     }

                     if ($scope.listaDesglosesSelect.length > 0) {
                       procesarDesglose(iNumDesgloseActual);
                     } else {
                       fErrorTxt("No ha seleccionado ningun desglose.", 1);
                     }

                   }

                   function procesarDesglose (iNumDesgloseProcesar) {

                     inicializarLoading();
                     var param = [];
                     param.push($scope.listaDesglosesSelect[iNumDesgloseProcesar]);
                     // Se obtiene el numero de titulos ya registrados de este desglose.
                     $scope.numTitulosActualesDesglose = 0;
                     $scope.nTitulosDesglose = 0;
                     ReportesCNMVService.getNumTitulosActuales(function (data) {
                       $scope.numTitulosActualesDesglose = data.resultados["NumTitululosActuales"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error al obtener los titulos actuales.", 1);
                     }, param);
                     ReportesCNMVService.convertirAlcToH47(onSuccessConversion, onErrorRequest, param);
                     $('#formularioOperH47').css('display', 'none');
                   }

                   function onSuccessConversion (json) {
                     var rowCrear = "";
                     if (json.error !== undefined && json.error !== null && json.error !== "") {
                       fErrorTxt(json.error, 1);
                     } else if (json.resultados !== undefined
                                && json.resultados.operacionesCreadasDesdeAlc !== undefined) {

                       if (json.resultados.operacionesCreadasDesdeAlc.length > 0) {

                         $scope.esCreacionDeOperaciones = true;
                         var operacion = json.resultados.operacionesCreadasDesdeAlc[0];
                         $scope.nTitulosDesglose = operacion.titulos;
                         operacion.hNegociacion = operacion.horaNegociacion;
                         operacion.fhNegociacion = new Date(operacion.fhNegociacion).toString('dd/MM/yyyy');
                         operacion.fhValor = new Date(operacion.fhValor).toString('dd/MM/yyyy');

                         preparaFormularioEdicion(operacion);

                       }
                     }
                   }

                   $scope.saveCrearNbooking = function () {
                     var param = [];
                     for (var i = 0, j = $scope.lenChkCrear; i < j; i++) {
                       var elemento = angular.element('input [id=chrCre_' + i + ']');
                       if (document.getElementById('chrCre_' + i).checked == true) {
                         param.push($scope.listaChkCrearNbook[i]);
                       }
                     }
                     ReportesCNMVService.convertirAlcToH47(onSuccessSaveConversion, onErrorRequest, param);
                     $('#formularioOperH47').css('display', 'none');

                   }

                   function onSuccessSaveConversion (json) {
                     $.unblockUI();
                     console.log(json);
                     console.log(json.resultados);
                     var rowCrear = "";
                     if (json.error !== undefined && json.error !== null && json.error !== "") {
                       fErrorTxt(json.error, 1);
                     } else if (json.resultados !== undefined
                                && json.resultados.operacionesCreadasDesdeAlc !== undefined) {
                       if (json.resultados.operacionesCreadasDesdeAlc.length > 0) {
                         $scope.cerrar();
                         fErrorTxt('Se han convertido ' + json.resultados.operacionesCreadasDesdeAlc.length
                                   + ' operaciones', 3);
                         cargarDatosReporteCNMV();
                         seguimientoBusqueda();
                         collapseSearchForm();
                       }
                     }
                   }

                   $scope.ModificarOperH47 = function (row) {
                     modificarOperaciones(oTable, row);
                   }

                   function modificarOperaciones (oTable, row) {

                     $scope.esCreacionDeOperaciones = false;

                     $scope.listaModi = [];
                     var aData = oTable.fnGetData();
                     var contador = 0;
                     var input;
                     var idName;
                     var inputSel;

                     for (var i = 0, j = aData.length; i < j; i++) {
                       if ($scope.listaChk[i] == true) {
                         $scope.listaModi.push(aData[i]);
                       }
                     }

                     if ($scope.listaModi.length == 0) {
                       $('#forModiOperH47').css('display', 'none');
                       fErrorTxt("Debe seleccionar uno de ellos.", 2)
                       return;
                     }

                     if ($scope.listaModi.length > 1) {

                       $('#forModiEnvioOperH47').css('display', 'inline');
                       $scope.tipoModificaciónSimple = false;
                       params = [];
                       for (var i = 0, j = $scope.listaModi.length; i < j; i++) {
                         var id = $scope.listaModi[i][13];
                         params.push({
                           id : id
                         });
                       }

                       var estEnvS = "";
                       $('#estEnvMas').val(estEnvS);

                     } else {

                       var elemento = $scope.listaModi[0];

                       var datosBooking = elemento[10].split("/");

                       var desglose = {
                         id : elemento[13] !== null ? elemento[13] : 0,
                         nbooking : datosBooking[0],
                         nucnfclt : datosBooking[1],
                         nucnfliq : datosBooking[2]

                       }

                       var param = [];
                       param.push(desglose);
                       // Se obtiene el numero de titulos ya registrados de este desglose.
                       $scope.numTitulosActualesDesglose = 0;
                       $scope.nTitulosDesglose = 0;
                       ReportesCNMVService.getNumTitulosActuales(function (data) {
                         $scope.numTitulosActualesDesglose = data.resultados["NumTitululosActuales"];
                         $scope.nTitulosDesglose = data.resultados["NumTitululosDesglose"];

                         var operacion = {
                           id : elemento[13] !== null ? elemento[13] : 0,
                           estadoEnvio : elemento[12] !== null ? elemento[12].trim() : "",
                           estEnvSeek : elemento[12] !== null ? elemento[12].trim() : "",
                           estEnv : getEstadoEnvio($scope.estEnvSeek),
                           capacidadNegociacion : elemento[5] !== null ? elemento[5].trim() : "",
                           estOperacion : elemento[11] !== null ? elemento[11].trim() : "",
                           idInstrumento : elemento[7] !== null ? elemento[7].trim() : "",
                           sentido : elemento[2] !== null ? elemento[2].trim() : "",
                           titularPrincipal : elemento[9] !== null ? elemento[9].trim() : "",
                           titulos : elemento[8] !== null ? elemento[8] : 0,
                           fhNegociacion : elemento[3] !== null ? elemento[3] : "",
                           fhValor : elemento[6] !== null ? elemento[6] : "",
                           hNegociacion : elemento[4] !== null ? elemento[4].trim() : "",
                           cantidadTotal : elemento[14] !== null ? elemento[14] : 0,
                           efectivoTotal : elemento[15] !== null ? elemento[15] : 0,
                           identificadorTransaccion : elemento[16] !== null ? elemento[16] : 0,
                           precioUnitario : elemento[17] !== null ? elemento[17] : 0,
                           nucnfclt : datosBooking[1],
                           nucnfliq : datosBooking[2]
                         }

                         preparaFormularioEdicion(operacion);

                       }, function (error) {
                         fErrorTxt("Se produjo un error al obtener los titulos actuales.", 1);
                       }, param);

                     }

                   }

                   function preparaFormularioEdicion (operacion) {

                     console.log("*******************");
                     console.log("Modificación datos Operaciones H47");
                     console.log("*******************");
                     console.log(operacion);

                     $('#estOper').prop('disabled', false);
                     $('#estOper').css('background-color', 'rgb(255, 255, 255)');

                     $('#capNeg').prop('disabled', false);

                     document.getElementById("Sentido").disabled = false;
                     $('#Sentido').css('background-color', 'rgb(255, 255, 255)');

                     $('#fEjecucion').prop('disabled', false);
                     $('#fLiquidacion').prop('disabled', false);

                     $('#Isin').prop('disabled', false);
                     $('#Titular').prop('disabled', false);
                     $('#Titulos').prop('disabled', false);

                     $('#fNegociacion').prop('disabled', false);

                     $('#fValor').prop('disabled', false);
                     $('#hNegoci').prop('disabled', false);

                     $('#Cantidad').prop('disabled', false);
                     $('#Efectivo').prop('disabled', false);
                     $('#idTransac').prop('disabled', false);
                     $('#Precio').prop('disabled', false);

                     $('#forModiOperH47').css('display', 'inline');

                     $scope.tipoModificaciónSimple = true;

                     if (operacion.estOperacion === 'ALTA') {
                       operacion.estOperacion = 'A';
                     }
                     if (operacion.estOperacion === 'BAJA') {
                       operacion.estOperacion = 'B';
                     }
                     if (operacion.sentido === 'COMPRA') {
                       operacion.sentido = 'B';
                     }

                     if (operacion.sentido === 'VENTA') {
                       operacion.sentido = 'S';
                     }

                     $('#idOper').val(operacion.id);
                     $('#estEnv').val(operacion.estadoEnvio);
                     $('#capNeg').val(operacion.capacidadNegociacion);
                     $('#estOper').val(operacion.estOperacion);

                     $('#Isin').val(operacion.idInstrumento);
                     $('#Sentido').val(operacion.sentido);
                     $('#Titular').val(operacion.titularPrincipal);
                     $('#Titulos').val(operacion.titulos);

                     $('#fNegociacion').val(operacion.fhNegociacion);
                     $('#fValor').val(operacion.fhValor);
                     $('#hNegoci').val(operacion.hNegociacion);

                     $('#Cantidad').val(operacion.cantidadTotal);
                     $('#Efectivo').val(operacion.efectivoTotal);
                     $('#idTransac').val(operacion.identificadorTransaccion);
                     $('#Precio').val(operacion.precioUnitario);

                     $('#idOper').prop('disabled', true);
                     document.getElementById("Sentido").disabled = true;
                     $('#Sentido').css('background-color', 'rgb(235, 235, 228)');

                     if ((operacion.estadoEnvio === "ACEPTADA" || operacion.estadoEnvio === 'ACEPTADA BME')
                         || !$scope.tipoModificaciónSimple) {
                       if (operacion.estadoEnvio === 'ACEPTADA BME' || !$scope.tipoModificaciónSimple) {
                         $('#estOper').prop('disabled', true);
                         $('#estOper').css('background-color', 'rgb(235, 235, 228)');
                       }
                       $('#capNeg').prop('disabled', true);

                       $('#fEjecucion').prop('disabled', true);
                       $('#fLiquidacion').prop('disabled', true);

                       $('#Isin').prop('disabled', true);

                       $('#Titular').prop('disabled', true);
                       $('#Titulos').prop('disabled', true);

                       $('#fNegociacion').prop('disabled', true);
                       $('#fValor').prop('disabled', true);
                       $('#hNegoci').prop('disabled', true);

                       $('#Cantidad').prop('disabled', true);
                       $('#Efectivo').prop('disabled', true);
                       $('#idTransac').prop('disabled', true);
                       $('#Precio').prop('disabled', true);
                     }

                     // if ($scope.tipoModificaciónSimple) {
                     // $('#capaDesgloseActual').css('display', 'none');
                     // }

                     $('#forModiOperH47').css('height', '365px');
                     $('#estOper').prop('disabled', true);
                     $('#capNeg').prop('disabled', true);

                     $scope.nTitulosDisponibles = $scope.nTitulosDesglose - $scope.numTitulosActualesDesglose;

                     if ($scope.nTitulosDisponibles < 0) {
                       $scope.nTitulosDisponibles = 0;
                     }

                     $('#desDesglose').text(
                                            'Desglose actual ' + operacion.nucnfclt + "/" + operacion.nucnfliq
                                                + '  -  Títulos desglose: ' + $scope.nTitulosDesglose
                                                + ' / Disponibles: ' + $scope.nTitulosDisponibles);

                     $.unblockUI();
                   }

                   $scope.LeerFichH47 = function () {
                     inicializarLoading();
                     var filters = "{ \"group\" : \"" + 'Fallidos' + "\", \"name\" : \"" + 'LecturaH47' + "\" }";
                     performJobCall('FIRE_NOW', filters, null, null, null, null);
                     $.unblockUI();
                     fErrorTxt("Mandada orden de lectura de fichero.", 3);
                     return false;
                   }

                   $scope.GenerarFichH47 = function () {
                     inicializarLoading();
                     var filters = "{ \"group\" : \"" + 'Fallidos' + "\", \"name\" : \"" + 'GeneracionH47' + "\" }";
                     performJobCall('FIRE_NOW', filters, null, null, null, null);
                     $.unblockUI();
                     fErrorTxt("Mandada orden de generación de fichero.", 3);
                     return false;
                   }

                   $scope.modificarCantidadTotal = function () {
                     $('#Cantidad').val($('#Titulos').val());
                   };

                   $scope.modificarTitulos = function () {
                     $('#Titulos').val($('#Cantidad').val());
                   };

                   $('input#fNegociacion').datepicker({
                     onClose : function (selectedDate) {
                       $("#fValor").val(selectedDate);
                     } // onClose
                   });

                   $('input#fValor').datepicker({
                     onClose : function (selectedDate) {
                       $("#fNegociacion").val(selectedDate);
                     } // onClose
                   });

                   $scope.Guardar = function () {

                     var fNego = $('#fNegociacion').val();
                     var fValor = $('#fValor').val();
                     var hNegoci = $('#hNegoci').val();
                     var horas = -1;
                     var minutos = -1;
                     var segundos = -1;

                     if (fNego != null && fNego != undefined && fNego !== '') {
                       var fNegoVector = fNego.split("/");
                       var fNegoFormato = fNegoVector[2] + fNegoVector[1] + fNegoVector[0];
                     }

                     if (fValor != null && fValor != undefined && fValor !== '') {
                       var fValorVector = fValor.split("/");
                       var fValorFormato = fValorVector[2] + fValorVector[1] + fValorVector[0];
                     }

                     if (hNegoci != null && hNegoci != undefined && hNegoci !== '') {
                       var hNegociVector = hNegoci.split(":");
                       horas = parseInt(hNegociVector[0]);
                       minutos = parseInt(hNegociVector[1]);
                       segundos = parseInt(hNegociVector[2]);
                       var hNegociFormato = hNegociVector[0] + hNegociVector[1] + hNegociVector[2];
                     }

                     if ($scope.tipoModificaciónSimple) {

                       var filtro = {
                         "idOperacionH47" : $('#idOper').val(),
                         "estenv" : $('#estEnv').val(),
                         "capacidadNegociacion" : $('#capNeg').val(),
                         "estoper" : $('#estOper').val(),
                         "fejecucion" : "",
                         "fliquidacion" : "",
                         "isin" : $('#Isin').val(),
                         "titularPrincipal" : $('#Titular').val(),
                         "titulos" : $('#Titulos').val(),
                         "cantidadTotal" : $('#Cantidad').val(),
                         "efectivoTotal" : $('#Efectivo').val(),
                         "fenegociacion" : fNegoFormato,
                         "fevalor" : fValorFormato,
                         "hnegociacion" : hNegociFormato,
                         "idTransaccion" : $('#idTransac').val(),
                         "precioUnitario" : $('#Precio').val()
                       }

                       if (fNego == null || fNego == "") {
                         fErrorTxt("Debe especificar una fecha de negociación");
                       } else if (hNegoci == null || hNegoci == "") {
                         fErrorTxt("Debe especificar una hora de negociación");
                       } else if (hNegociVector.length != 3) {
                         fErrorTxt("La hora de negociacion tiene que tener el formato hh:mm:ss");
                       } else if (horas >= 24 || horas < 0 || isNaN(horas)) {
                         fErrorTxt("Las horas de la hora de negociación tiene un valor incorrecto");
                       } else if (minutos >= 60 || minutos < 0 || isNaN(minutos)) {
                         fErrorTxt("Los minutos de la hora de negociación tiene un valor incorrecto");
                       } else if (segundos >= 60 || segundos < 0 || isNaN(segundos)) {
                         fErrorTxt("Los segundos de la hora de negociación tiene un valor incorrecto");
                       } else if (horaMaxima < hNegociFormato) {
                         fErrorTxt("La hora de negociación no puede ser superior a " + horaMaxima.substring(0, 2) + ":"
                                   + horaMaxima.substring(2, 4) + ":" + horaMaxima.substring(4, 6), 1);
                       } else if (filtro.capacidadNegociacion != 'P' && filtro.capacidadNegociacion != 'A') {
                         fErrorTxt("La capacidad de negociación solo puede tomar los valores P o A");
                       } else if (filtro.titulos > $scope.nTitulosDisponibles) {
                         fErrorTxt("El número de títulos no puede ser superior a los disponibles");

                       } else {
                         inicializarLoading();
                         if ($scope.esCreacionDeOperaciones) {

                           $scope.listaChkCrearNbook[i]

                           filtro.isCreation = true;
                           filtro.nbooking = $scope.listaDesglosesSelect[iNumDesgloseActual].nbooking.trim();
                           filtro.nucnfclt = $scope.listaDesglosesSelect[iNumDesgloseActual].nucnfclt.trim();
                           filtro.nucnfliq = $scope.listaDesglosesSelect[iNumDesgloseActual].nucnfliq.trim();

                           var filtros = [];

                           $scope.listaDesglosesCrear[iNumDesgloseCreados] = filtro;
                           iNumDesgloseCreados++;
                           iNumDesgloseActual++;
                           if (iNumDesgloseActual < $scope.listaDesglosesSelect.length) {
                             procesarDesglose(iNumDesgloseActual);
                           } else {
                             if ($scope.listaDesglosesCrear.length > 0) {
                               ReportesCNMVService.convertirAlcToH47(onSuccessSaveConversion, onErrorRequest,
                                                                     $scope.listaDesglosesCrear);
                             } else {
                               $scope.cerrar(); // Se cierra la ventana
                               fErrorTxt("No ha guardado los datos de ninguno de los desgloses seleccionados");
                             }
                             $scope.esCreacionDeOperaciones = false;
                           }

                         } else {
                           ReportesCNMVService.updateOperaciones(onSuccessUpdateOperaciones, onErrorRequest, filtro);
                         }

                       }
                     } else {

                       var filtro = {
                         "estenv" : $('#estEnvMas').val(),
                       }
                       ReportesCNMVService.updateOperacionesMasiva(onSuccessUpdateOperaciones, onErrorRequest, filtro,
                                                                   params);
                     }
                     document.getElementById("MarcarTod").value = "Marcar Todos";
                     document.getElementById("MarcarPag").value = "Marcar Página";
                     // collapseSearchForm();

                   }

                   $scope.Cancelar = function () {

                     if ($scope.esCreacionDeOperaciones) { // Se pueden estar procesando mas registros.
                       iNumDesgloseActual++;
                       if (iNumDesgloseActual < $scope.listaDesglosesSelect.length) { // Se carga el siguiente
                         procesarDesglose(iNumDesgloseActual);
                       } else { // Se procesan los modificados.
                         if ($scope.listaDesglosesCrear.length > 0) {
                           inicializarLoading();
                           ReportesCNMVService.convertirAlcToH47(onSuccessSaveConversion, onErrorRequest,
                                                                 $scope.listaDesglosesCrear);

                         } else {
                           $scope.cerrar(); // Se cierra la ventana
                           fErrorTxt("No ha guardado los datos de ninguno de los desgloses seleccionados");
                         }
                         $scope.esCreacionDeOperaciones = false;
                       }
                     } else {
                       $scope.cerrar(); // Se cierra la ventana
                     }
                   }

                   function cargaComboEnviosMod (tipo) {
                     var filtro = {};

                     ReportesCNMVService.cargaComboEnvios(onSuccessCargaEnviosMod, onErrorRequest, filtro);

                     ReportesCNMVService.cargaComboEnvios(onSuccessCargaEnviosMod2, onErrorRequest, filtro);

                   }

                   function getEstadoEnvio () {
                     var filtro = {};
                     ReportesCNMVService.cargaComboEnvios(onSuccessGetEnvio, onErrorRequest, filtro);
                   }

                   function onSuccessCargaEnviosMod (json) {
                     var datos = {};
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultados !== null
                                  && json.resultados.listadoEstados !== undefined) {
                         datos = json.resultados.listadoEstados;
                         angular.forEach(datos, function (item, key) {
                           item = datos[key];
                           $('#estEnv').append(
                                               "<option value='" + item.idestado + "'>(" + item.idestado + ") "
                                                   + item.nombre + "</option>");
                         })
                       }
                     }
                   }

                   function onSuccessCargaEnviosMod2 (json) {
                     var datos = {};
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultados !== null
                                  && json.resultados.listadoEstados !== undefined) {
                         datos = json.resultados.listadoEstados;
                         angular.forEach(datos, function (item, key) {
                           item = datos[key];
                           $('#estEnvMas').append(
                                                  "<option value='" + item.idestado + "'>(" + item.idestado + ") "
                                                      + item.nombre + "</option>");
                         })
                       }
                     }
                   }

                   function onSuccessGetEnvio (json) {
                     var datosComp = {};
                     var item;
                     var compara = $scope.estEnvSeek;
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultados !== null
                                  && json.resultados.listadoEstados !== undefined) {
                         datosComp = json.resultados.listadoEstados;
                         for (var i = 0; i < datosComp.length; i++) {
                           item = datosComp[i];
                           if (compara === item.nombre) {
                             $('#estEnv').val(item.idestado);
                           }

                         }

                       }
                     }
                   }

                   function onSuccessUpdateOperaciones (json) {
                     $.unblockUI();
                     var datos = {};
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultados !== null
                                  && json.resultados.resultadoUpdateOperacionH47 !== undefined) {
                         $('#forModiOperH47').css('display', 'none');
                         $('#forModiEnvioOperH47').css('display', 'none');
                         datos = json.resultados.resultadoUpdateOperacionH47;
                         // collapseSearchForm();
                         fErrorTxt(datos, 3);
                         inicializarLoading();
                         cargarDatosReporteCNMV();
                         seguimientoBusqueda();

                       }
                     }
                   }

                   $scope.modifiTitu = function (titu) {

                     console.log('Entra en el modifiTitu');
                     $('#formularioTituH47').css('display', 'inline');
                     $scope.titularModi = [];

                     var filtro = {
                       "idTitularH47" : titu
                     };
                     ReportesCNMVService.getTitularById(onSuccessTitular, onErrorRequest, filtro);

                   }

                   function modificarTitulares (titu) {
                     $('#formularioTituH47').css('display', 'inline');
                     $('#idTit').prop('disabled', true);
                     $scope.titularModi = [];

                     var filtro = {
                       "idTitularH47" : titu
                     };
                     ReportesCNMVService.getTitularById(onSuccessTitular, onErrorRequest, filtro);

                   }

                   function onSuccessTitular (json) {
                     var datos = {};
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultados !== null
                                  && json.resultados.listadoTitularesH47 !== undefined) {
                         datos = json.resultados.listadoTitularesH47;
                         angular.forEach(datos, function (item, key) {
                           item = datos[key];
                           $('#idTit').val(item.id);
                           $('#tPerTit').val(item.tpsocied);
                           $('#tDocTit').val(item.tpidenti);
                           $('#docTit').val(item.identificacion);
                           $('#nomTit').val(item.nombre);
                           $('#ap1Tit').val(item.apellido1);
                           $('#ape2Tit').val(item.apellido2);
                           $('#tDocRep').val(item.repTpidenti);
                           $('#docRep').val(item.repIdentificacion);
                           $('#nomRep').val(item.repNombre);
                           $('#ap1Rep').val(item.repApellido1);
                           $('#ap2Rep').val(item.repApellido2);
                           $('#cuenta').val(item.cantidadValores);

                         })
                       }
                     }
                   }

                   function getHoraMaxima () {
                     var filtro = {};
                     ReportesCNMVService.getHoraMaxima(onSuccessGetHoraMaxima, onErrorRequest, filtro);
                   }

                   function onSuccessGetHoraMaxima (json) {
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultados !== null
                                  && json.resultados.horaMaxima !== undefined) {
                         horaMaxima = json.resultados.horaMaxima;
                       }
                     }

                   }

                   function caragaCombotipoDocumento () {

                     var data = new DataBean();

                     data.setService('SIBBACServiceTitulares');
                     data.setAction('getTpidentiList');

                     var request = requestSIBBAC(data);
                     var item;
                     request.success(function (json) {
                       if (json.resultados == null) {
                         fErrorTxt(json.error, 1);
                       } else {

                         var datos = json.resultados.resultados_tpidenti;

                         for ( var k in datos) {

                           item = datos[k];

                           $('#tDocTit').append(
                                                "<option value='" + item.codigo + "'>(" + item.codigo + ") "
                                                    + item.descripcion + "</option>");
                           $('#tDocRep').append(
                                                "<option value='" + item.codigo + "'>(" + item.codigo + ") "
                                                    + item.descripcion + "</option>");

                         }
                       }
                     });
                   }

                   $scope.GuardaTitu = function () {
                     $('#formularioTituH47').css('display', 'none');
                     var filtro = {
                       "idTitularH47" : $('#idTit').val(),
                       "apellido1" : $('#ap1Tit').val(),
                       "apellido2" : $('#ap2Tit').val(),
                       "nombre" : $('#nomTit').val(),
                       "repIdentificacion" : $('#docRep').val(),
                       "identificacion" : $('#docTit').val(),
                       "repApellido1" : $('#ap1Rep').val(),
                       "repApellido2" : $('#ap2Rep').val(),
                       "repNombre" : $('#nomRep').val(),
                       "tpidenti" : $('#tDocTit').val(),
                       "tpsocied" : $('#tPerTit').val(),
                       "repTpidenti" : $('#tDocRep').val(),
                       "cantidadValores" : $('#cuenta').val()
                     }

                     ReportesCNMVService.updateTitulares(onSuccessUpdateTitulares, onErrorRequest, filtro);

                   }

                   function onSuccessUpdateTitulares (json) {
                     var datos = {};
                     if (json !== undefined && json !== null) {
                       if (json.error !== undefined && json.error !== null && json.error !== "") {
                         // growl.addErrorMessage(json.error);
                         fErrorTxt(json.error, 1);
                       } else if (json.resultados !== undefined && json.resultados !== null
                                  && json.resultados.resultadoUpdateOperacionH47 !== undefined) {
                         datos = json.resultados.resultadoUpdateOperacionH47;
                         fErrorTxt(datos, 3);
                       }
                     }
                   }

                   function performJobCall (command, filters, params, lista, successCallBack, _clear) {
                     var clear = (_clear !== undefined) ? _clear : true;

                     // Limpiamos los "targets"..
                     if (clear) {
                       $('#tdRunningJobs').empty();
                       $('#tdCurrentJobs').empty();
                     }

                     // Activamos el "loading"...
                     // document.getElementById( DIV_LOADING ).style.display='block';

                     var data = new DataBean();
                     data.setService('SIBBACServiceJobsManagement');
                     data.setAction(command);
                     if (lista != null) {
                       data.list = lista;
                     }
                     if (filters != null) {
                       data.filters = filters;
                     }
                     if (params != null) {
                       data.params = params;
                     }
                     var SHOW_LOG = false;
                     if (SHOW_LOG)
                       console.log("Llamando al servidor[" + command + "]...");
                     var request = requestSIBBAC(data);
                     request.success(successCallBack);
                     request.error(function (jqXHR, textStatus, errorThrown) {
                       console.log("ERROR: " + jqXHR + ", " + textStatus + ": " + errorThrown);
                       console.log("Headers: " + jqXHR.getAllResponseHeaders());
                       console.log("Response text: " + jqXHR.responseText);
                       hideLoadingLayer(JSON.parse(jqXHR.responseText).error);
                     });
                   }
                   // Boton de marcar todos
                   $scope.selececionarCheck = function () {
                     if (lMarcar) {
                       lMarcar = false;
                       document.getElementById("MarcarTod").value = "Desmarcar Todos";
                       toggleCheckBoxes(true);
                     } else {
                       lMarcar = true;
                       document.getElementById("MarcarTod").value = "Marcar Todos";
                       toggleCheckBoxes(false);
                     }
                     $scope.checkListaChk();

                   };

                   // Boton de marcar página
                   $scope.selececionarCheckPag = function () {

                     var valor = alPaginas[pagina];

                     toggleCheckBoxesPag(valor);
                     if (valor) {
                       document.getElementById("MarcarPag").value = "Desmarcar Página";
                       alPaginas[pagina] = false;
                     } else {
                       document.getElementById("MarcarPag").value = "Marcar Página";
                       alPaginas[pagina] = true;
                     }
                     $scope.checkListaChk();
                   };

                   // marca/desmarca todos los registros
                   function toggleCheckBoxes (b) {

                     for (var i = 0; i < $scope.listaChk.length; i++) {
                       $scope.listaChk[i] = b;
                     }
                   }

                   // marca/desmarca los registros de la pagina
                   function toggleCheckBoxesPag (b) {
                     var inputs = angular.element('input[type=checkbox]');
                     var cb = null;
                     for (var i = 0; i < inputs.length; i++) {
                       cb = inputs[i];
                       var clase = $(cb).attr('class');
                       var n = clase.indexOf(" ");
                       var fila = clase.substr(0, n);
                       $scope.listaChk[fila] = b;
                     }

                   }

                   $scope.checkListaChk = function () {
                     // var inputs = angular.element('input[type=checkbox]');
                     var contChk = 0;
                     for (var i = 0; i < $scope.listaChk.length; i++) {
                       if ($scope.listaChk[i]) {
                         contChk++
                         if (contChk > 1) {
                           break;
                         }
                       }
                     }
                     if (contChk > 1) {
                       document.getElementById("ModificarOperH47").value = "Modificar Estado de envio";
                     } else {
                       document.getElementById("ModificarOperH47").value = "Modificar Operación H47";
                     }

                   }

                   // Cuando en creación de operaciones cambia el valor de Titulos.
                   $("#Titulos").blur(function (event) {
                     var efectivo = $("#Titulos").val() * $("#Precio").val();
                     $("#Efectivo").val(efectivo.toFixed(2));
                   });

                   $("#Cantidad").blur(function (event) {
                     var efectivo = $("#Cantidad").val() * $("#Precio").val();
                     $("#Efectivo").val(efectivo.toFixed(2));
                   });

                   // Cuando en creación de operaciones cambia el valor de precio unitario.
                   $("#Precio").blur(function (event) {
                     var efectivo = $("#Titulos").val() * $("#Precio").val();
                     $("#Efectivo").val(efectivo.toFixed(2));
                   });

                   // Cuando en creación de operaciones cambia el valor de precio unitario.
                   $("#Efectivo").blur(function (event) {
                     var precio = $("#Efectivo").val() / $("#Titulos").val();
                     $("#Precio").val(precio.toFixed(5));
                   });

                   $scope.btnInvEstenv = function () {
                     $scope.btnInvGenerico('Estenv');
                   }

                   $scope.btnInvIsin = function () {
                     $scope.btnInvGenerico('Isin');
                   }

                   $scope.btnInvGenerico = function (nombre) {

                     var check = '$scope.filtro.ch' + nombre.toLowerCase();

                     if (eval(check)) {
                       var valor2 = check + "= false";
                       eval(valor2);

                       document.getElementById('textBtnInv' + nombre).innerHTML = "<>";
                       angular.element('#textInv' + nombre).css({
                         "background-color" : "transparent",
                         "color" : "red"
                       });
                       document.getElementById('textInv' + nombre).innerHTML = "DISTINTO";
                     } else {
                       var valor2 = check + "= true";
                       eval(valor2);

                       document.getElementById('textBtnInv' + nombre).innerHTML = "=";
                       angular.element('#textInv' + nombre).css({
                         "background-color" : "transparent",
                         "color" : "green"
                       });
                       document.getElementById('textInv' + nombre).innerHTML = "IGUAL";
                     }
                   }

                 } ]);
