'use strict';
sibbac20.controller('CrearRectificativaController', ['$scope', '$compile', 'growl', 'CrearRectificativaService', 'FacturaSugeridaService', 'AliasService', '$document', function ($scope, $compile, growl, CrearRectificativaService, FacturaSugeridaService, AliasService, $document) {
        var idOcultos = [];
        var availableTags = [];
        var oTable = undefined;

        $scope.filtro = {"numeroFactura": "",
            "idAlias": "",
            "fechaDesde": "",
            "fechaHasta": ""}

        function loadDataTable() {
            oTable = angular.element(".tablaCrearRectificativa").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "aoColumns": [
                    {"width": "5%"},
                    {"width": "auto"},
                    {"width": "20%"},
                    {"width": "auto", type: "date-eu"},
                    {"width": "auto", sClass: "monedaR"},
                    {"width": "auto", sClass: "monedaR"},
                    {"width": "auto", sClass: "monedaR"},
                    {"width": "auto", sClass: "date-eu"},
                    {"width": "auto", sClass: "date-eu"}
                ],
                "scrollY": "480px",
                "scrollCollapse": true,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    $compile(nRow)($scope);
                }
            });
        }

        $(document).ready(function () {
            loadDataTable();
            //var fechas = ['fechaDesde', 'fechaHasta'];
            //loadpicker(fechas);
            //verificarFechas([['fechaDesde', 'fechaHasta']]);

            prepareCollapsion();

            /*
             * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
             * TABLA
             */
            if ($('.contenedorTabla').length >= 1) {
                var anchoBotonera;
                $('.contenedorTabla').each(function (i) {
                    anchoBotonera = $(this).find('table').outerWidth();
                    $(this).find('.botonera').css('width', anchoBotonera + 'px');
                    $(this).find('.resumen').css('width', anchoBotonera + 'px');
                    // $('.resultados').hide();
                });
            }

            loadAliasJS();
            cargarTabla('', '', '', '');

            //var fechas = ['fechaDesde', 'fechaHasta'];
            //loadpicker(fechas);
            $("#ui-datepicker-div").remove();
			$('input#fechaDesde').datepicker({
			    onClose: function( selectedDate ) {
			      $( "#fechaHasta" ).datepicker( "option", "minDate", selectedDate );
			    } // onClose
			  });

			  $('input#fechaHasta').datepicker({
			    onClose: function( selectedDate ) {
			      $( "#fechaDesde" ).datepicker( "option", "maxDate", selectedDate );
			    } // onClose
			  });
        });

        $scope.loadAlias = function () {
            loadAliasJS();
        }

        function loadAliasJS()
        {
            AliasService.getAliasFactura(cargarAlias, showError);
        }

        $scope.limpiarFiltro = function () {
            //ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fechaHasta" ).datepicker( "option", "minDate", "");
            $( "#fechaDesde" ).datepicker( "option", "maxDate", "");

            //

            angular.element('input[type=text]').val('');
            angular.element('select').val('');
        };

        $scope.buscar = function (event) {
        	$scope.filtro.fechaDesde = $('#fechaDesde').val();
          	$scope.filtro.fechaHasta = $('#fechaHasta').val();

          	$scope.filtro.numeroFactura = document.getElementById("numeroFactura").value;
          	$scope.filtro.idAlias = document.getElementById("textAlias").value;
          	//
            if (validarCampoFecha('fechaDesde', false) && validarCampoFecha('fechaHasta', false)) {
                var numeroFactura = $scope.filtro.numeroFactura;
                var fechaDesde = $scope.filtro.fechaDesde;
                var fechaHasta = $scope.filtro.fechaHasta;
                var idAlias = getIdAlias();
                var fechas = ['fechaDesde', 'fechaHasta'];
                loadpicker(fechas);
                verificarFechas([fechas]);
                cargarTabla($scope.filtro.numeroFactura, $scope.filtro.idAlias, $scope.filtro.fechaDesde, $scope.filtro.fechaHasta);
                angular.element('.collapser_search').parents('.title_section').next().slideToggle();
                angular.element('.collapser_search').toggleClass('active');
                angular.element('.collapser_search').toggleText('Mostrar opciones de búsqueda', 'Ocultar opciones de búsqueda');
            } else {
                alert("Los campos de fecha introducidos no son correctos. Por favor revíselos.");
            }
            return false;
        };

        function getIdAlias() {
            var idAlias = "";
            var textAlias = $scope.filtro.idAlias;
            if ((textAlias !== null) && (textAlias !== '')) {
                idAlias = idOcultos[availableTags.indexOf(textAlias)];
                if (idAlias === undefined) {
                    idAlias = "";
                }
            }
            return idAlias;
        }

        // funcion para cargar la tabla
        function cargarTabla(numeroFactura, idAlias, fechaDesde, fechaHasta) {
            console.log("[valorSeleccionadoAlias==" + idAlias + "] [fechaDesde==" + fechaDesde + "] [fechaHasta==" + fechaHasta + "]");

            inicializarLoading();
            var params = {};
            if (numeroFactura !== "") {
                params.numeroFactura = numeroFactura;
            } else {
                params.idAlias = idAlias;
                params.fechaDesde = transformaFechaInv(fechaDesde);
                params.fechaHasta = transformaFechaInv(fechaHasta);
            }
            CrearRectificativaService.getListadoFacturasEnviadas(successCargarTabla, showError, params);
        }

        function successCargarTabla(json, status, headers, config)
        {
            var datos = {};
            if (json !== undefined &&
                    json.resultados !== undefined &&
                    json.resultados !== null) {
                if (json.error !== null) {
                    growl.addErrorMessage(json.error);
                } else {
                    datos = json.resultados.listadoFacturas;
                    var item = null;
                    var contador = 0;
                    oTable.fnClearTable();
                    for (var k in datos) {
                        item = datos[k];
                        oTable.fnAddData([
                            "<input type='checkbox' class='checkTabla' id='checkTabla' data-ref='idmarcar" + contador + "' name='checkTabla' /><input type='hidden' name='idmarcar"
                                    + contador + "' id='idmarcar" + contador + "' value='" + item.idFactura + "' />",
                            "<span>" + item.numeroFactura + "</span>", "<span >" + item.cdAlias.trim() + " - " + item.nombreAlias + "</span>",
                            "<span>" + transformaFecha(item.fechaCreacion) + "</span>", "<span>" + item.baseImponible + "</span>",
                            "<span>" + item.importeImpuestos + "</span>", "<span>" + item.importeTotal + "</span>",
                            "<span>" + transformaFecha(item.fechaInicio) + "</span>", "<span>" + transformaFecha(item.fechaFin) + "</span>"],false);
                        contador++;
                    }
                    oTable.fnDraw();
                }
            }
            $.unblockUI();
        }

        function showError(data, status, headers, config) {
            $.unblockUI();
            growl.addErrorMessage("Error: " + data);
        }

        function cargarAlias(datos) {
            var item = null;
            for (var k in datos) {
                item = datos[ k ];
                idOcultos.push(item.id);
                availableTags.push(item.nombre.trim());
            }
            // código de autocompletar
            angular.element("#textAlias").autocomplete({
                source: availableTags
            });
            growl.addInfoMessage("Alias cargados.");
        }

        // marcar y desmarcar
        $scope.clickSeleccionarTodos = function () {
            var fila = angular.element('.tablaCrearRectificativa').find('td input.checkTabla:enabled');
            fila.prop('checked', true);
        };

        $scope.clickDesmarcarTodos = function () {
            var fila = angular.element('.tablaCrearRectificativa').find('td input.checkTabla');
            fila.prop('checked', false);
        };

        // Evento para marcar facturas
        $scope.anular = function () {
			console.log("[$scope.anular] entro en el metodo");

            var suma = 0;
            var valores = new Array();
            var los_cboxes = document.getElementsByName('checkTabla');
            var valor = "";
			console.log("[$scope.anular] los_cboxes: " + los_cboxes);
            for (var i = 0, j = los_cboxes.length; i < j; i++) {
				console.log("[$scope.anular] > i: "+ i + ", j: "+ j );
				console.log("[$scope.anular]   los_cboxes["+i+"]: "+los_cboxes[i]);
				console.log("[$scope.anular]   los_cboxes["+i+"].checked: "+los_cboxes[i].checked);
                if (los_cboxes[i].checked === true) {
                    var idRef = los_cboxes[i].getAttribute('data-ref');
                    valor = angular.element('#' + idRef).val();
					console.log("[$scope.anular]   idRef: "+ idRef + ", valor: " + valor);
/*
					var angCampo = angular.element(los_cboxes[i]);
                    var idCampo = angCampo.attr('data');
                    valor = angular.element('#' + idCampo).val();
					console.log("[$scope.anular]   angCampo: "+ angCampo + ", idCampo: "+ idCampo + ", valor: " + valor);
*/
                    valores.push(valor);
                    suma++;
                }
            }

            if (suma === 0) {
                alert('Marque al menos una línea');
                return false;
            } else {
                marcarFacturas(valores);
            }
        };

        function cambioAlias() {
            $scope.filtro.numeroFactura = "";
        }

        $scope.cambioNumeroFactura = function () {
            $scope.filtro.idAlias = "";
            $scope.filtro.fechaDesde = "";
            $scope.filtro.fechaHasta = "";
        }

        //Funcion para marcar facturas
        function marcarFacturas(valores)
        {
			console.log("[marcarFacturas] entro en el metodo");
			console.log("[marcarFacturas] valores: "+ valores);
            var bucleVal = valores.length;
			console.log("[marcarFacturas] bucleVal: "+ bucleVal);
            // console.log("entro en el metodo");
            var params = [];
            for (var v = 0; v < bucleVal; v++) {
				console.log("[marcarFacturas] > v: "+ v + ", valores["+v+"]: "+valores[v]);
                params.push({idFactura: valores[v]});
            }
			console.log("[marcarFacturas] params: "+ params);
            inicializarLoading();
            FacturaSugeridaService.marcarRectificar(onCheckFacturarHandler, showError, params);
        }

        function onCheckFacturarHandler(data, status, headers, config) {
            $.unblockUI();
            growl.addSuccessMessage("Facturas rectificada correctamente.");
            $scope.buscar(null);
        }

    }]);
