(function(sibbac20, angular, gc, undefined) {
  "use strict";

  var ClearingTitulosController = function(isinService, cuentaLiquidacionService, conciliacionClearingService, growl,
      $filter) {
    gc.ClearingConsultaController.call(this, isinService, cuentaLiquidacionService, conciliacionClearingService, growl,
      $filter)
  };
  
  ClearingTitulosController.prototype = Object.create(gc.ClearingConsultaController.prototype);

  ClearingTitulosController.prototype.constructor = ClearingTitulosController;

  ClearingTitulosController.prototype.consultar = function() {
    inicializarLoading();
    this.desformatearFechasFiltro();
    this.conciliacionClearingService.getConciliacionTitulosER(this.filter, this.populateDataTables.bind(this),
        this.showError.bind(this));
  };

  ClearingTitulosController.prototype.populateDataTableT = function(datosT) {
    var difClassI = '', rownum = 0;

    this.oTableT.fnClearTable();
    if (datosT == null) {
      return;
    }
    angular.forEach(datosT, function(val, key) {
      var difClassI, iguales, sistema, mostrarFila = true;

      iguales = val.titulosSV === val.titulosCustodio && val.titulosSV === val.titulosCamara;
      difClassI = (!iguales) ? 'colorFondo' : 'centrado';
      sistema = (val.sistemaLiq) ? val.sistemaLiq : " ";
      var diferencia = '-';
      var diferenciaCamara = '-';
      var titulosSV = val.titulosSV;
      var titulosCustodio = val.titulosCustodio;
      var titulosCamara = val.titulosCamara;
      var settlementdate = val.settlementDate !== null ? EpochToHumanSinHora(val.settlementDate) : '';
      if (titulosSV !== null) {
        if (val.titulosCustodio !== null) {
          diferencia = val.titulosSV - val.titulosCustodio;
        }
        if (val.titulosCamara !== null) {
          diferenciaCamara = val.titulosSV - titulosCamara;
        }
      }
      else {
        titulosSV = '-';
        diferencia = titulosCustodio * -1;
        diferenciaCamara = titulosCamara * -1;
      }
      if (titulosCustodio === null) {
        titulosCustodio = '-';
        diferencia = titulosSV;
      }
      if (titulosCamara === null) {
        titulosCamara = '-';
        diferenciaCamara = titulosSV;
      }
      this.oTableT.fnAddData([
        "<span>" + val.isin + "</span>", "<span>" + val.descripcion === null ? "" : val.descripcion + "</span>",
        "<span>" + $.number(titulosSV, 0, ',', '.') + "</span>",
        "<span>" + $.number(titulosCustodio, 0, ',', '.') + "</span>",
        '<span>' + $.number(val.titulosCamara, 0, ',', '.') + '</span>',
        "<span>" + $.number(diferencia, 0, ',', '.') + "</span>",
        '<span>' + $.number(diferenciaCamara, 0, ',', '.') + '</span>', "<span>" + settlementdate + "</span>",
        "<span>" + val.codigoCuentaLiquidacion + "</span>"//,
        //"<span>" + val.mercado + "</span>"
      ], false);
      angular.element(this.oTableT.fnGetNodes(rownum++)).addClass(difClassI);
    }, this);
    this.oTableT.fnDraw();
  };

  ClearingTitulosController.prototype.loadDataTableT = function() {
    this.oTableT = angular.element("#tblClearingTitulosTotal").dataTable({
      "dom" : 'T<"clear">lfrtip', "bSort" : true, "tableTools" : {
        "sSwfPath" : "js/swf/copy_csv_xls_pdf.swf"
      }, "language" : {
        "url" : "i18n/Spanish.json"
      }, "scrollY" : "480px", "scrollCollapse" : true, "aoColumns" : [{
        "width" : "auto", "sClass" : "centrado"
      },// ISIN
      {
        "width" : "auto", "sClass" : "align-left"
      },// DESCRIPCION ISIN
      {
        "width" : "auto", "sClass" : "align-right", "type" : 'formatted-num'
      },// TITULOS
      // SV
      {
        "width" : "auto", "sClass" : "align-right", "type" : 'formatted-num'
      },// TITULOS
      // CUSTODIO
      {
        "sClass" : "align-right"
      },// TITULOS CAMARA
      {
        "width" : "auto", "sClass" : "monedaR", "type" : 'formatted-num'
      }, // DIFERENCIA
      // S3
      {
        "sClass" : "align-right"
      },// DIFERENCIA CAMARA
      {
        "width" : "auto", "sClass" : "centrado"
      },// FECHA LIQUIDACION
      {
        "width" : "auto", "sClass" : "centrado"
      }// ,//CUENTA LIQUIDACION
      // {"width": "auto","sClass": "centrado"}//MERCADO
      ]
    });
  }


  sibbac20.controller('ClearingTitulosController', [
    'IsinService', 'CuentaLiquidacionService', 'ConciliacionClearingService', 'growl', '$filter',
    ClearingTitulosController]);

})(sibbac20, angular, GENERIC_CONTROLLERS);
