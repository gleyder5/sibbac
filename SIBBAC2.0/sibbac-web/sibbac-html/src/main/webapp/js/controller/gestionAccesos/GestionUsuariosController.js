'use strict';
sibbac20
    .controller(
                'GestionUsuariosController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'GestionUsuariosService',
                 '$compile',
                 'SecurityService',
                 function ($scope, $document, growl, GestionUsuariosService, $compile, SecurityService) {

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   /***************************************************************************************************
                     * ** INICIALIZACION DE DATOS
                     **************************************************************************************************/

                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   // Variables y listas general
                   $scope.mostrarBuscador = true;
                   $scope.filtro = {};
                   $scope.listUsuarios = [];
                   $scope.listPerfiles = [];

                   $scope.listadoUsuarios = [];
                   $scope.usuariosSeleccionados = [];
                   $scope.usuariosSeleccionadosBorrar = [];

                   $scope.activeAlert = false;

                   $scope.modal = {
                     titulo : "",
                     showCrear : false,
                     showModificar : false
                   };

                   $scope.followSearch = "";// contiene las migas de los filtros de búsqueda

                   /***************************************************************************************************
                     * ** DEFINICION TABLA **
                     **************************************************************************************************/

                   $scope.oTable = $("#datosUsuarios").dataTable({
                     "dom" : 'T<"clear">lfrtip',
                     "tableTools" : {
                       "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                       "aButtons" : [ "copy", {
                         "sExtends" : "csv",
                         "sFileName" : "Listado_Usuarios_" + hoy + ".csv",
                         "mColumns" : [ 1, 2, 3, 4, 5 ]
                       }, {
                         "sExtends" : "xls",
                         "sFileName" : "Listado_Usuarios_" + hoy + ".xls",
                         "mColumns" : [ 1, 2, 3, 4, 5 ]
                       }, {
                         "sExtends" : "pdf",
                         "sPdfOrientation" : "landscape",
                         "sTitle" : " ",
                         "sPdfSize" : "A3",
                         "sPdfMessage" : "Listado Usuarios",
                         "sFileName" : "Listado_Usuarios_" + hoy + ".pdf",
                         "mColumns" : [ 1, 2, 3, 4, 5 ]
                       }, "print" ]
                     },
                     "aoColumns" : [ {
                       sClass : "centrar",
                       bSortable : false,
                       width : "7%"
                     }, {
                       sClass : "centrar",
                       width : "18%"
                     }, {
                       sClass : "centrar",
                       width : "25%"
                     }, {
                       sClass : "centrar",
                       width : "25%"
                     }, {
                       sClass : "centrar",
                       width : "25%"
                     } ],
                     "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                       $compile(nRow)($scope);
                     },

                     "scrollY" : "480px",
                     "scrollX" : "100%",
                     "scrollCollapse" : true,
                     "language" : {
                       "url" : "i18n/Spanish.json"
                     }
                   });

                   $scope.borrarTabla = function () {
                     // borra el contenido del body de la tabla
                     var tbl = $("#datosUsuarios > tbody");
                     $(tbl).html("");
                     $scope.oTable.fnClearTable();
                   }

                   $scope.seleccionarElemento = function (row) {
                     if ($scope.listadoUsuarios[row].selected) {
                       $scope.listadoUsuarios[row].selected = false;
                       for (var i = 0; i < $scope.usuariosSeleccionados.length; i++) {
                         if ($scope.usuariosSeleccionados[i].idUser === $scope.listadoUsuarios[row].idUser) {
                           $scope.usuariosSeleccionados.splice(i, 1);
                           $scope.usuariosSeleccionadosBorrar.splice(i, 1);
                         }
                       }
                     } else {
                       $scope.listadoUsuarios[row].selected = true;
                       $scope.usuariosSeleccionados.push($scope.listadoUsuarios[row]);
                       $scope.usuariosSeleccionadosBorrar.push($scope.listadoUsuarios[row].idUser);

                     }
                   };

                   /***************************************************************************************************
                     * ** INICIALIZACION DE DATOS - FUNCIONES
                     **************************************************************************************************/

                   $scope.resetButtonFilters = function () {
                     $scope.btnsFilter = {
                       textUsuario : "IGUAL",
                       btnUsuario : "=",
                       colorUsuario : {
                         'color' : 'green'
                       },
                       textPerfil : "IGUAL",
                       btnPerfil : "=",
                       colorPerfil : {
                         'color' : 'green'
                       }
                     };
                   };

                   // Setea el filtro que se pasará al back
                   $scope.setFiltro = function () {
                     $scope.filtro.perfil = $scope.perfilFiltro.key;
                     $scope.filtro.listaUsuariosBorrar = $scope.usuariosSeleccionadosBorrar;
                     $scope.filtro.usuarioSelect = $scope.usuario;
                   };

                   // Reset del filtro
                   $scope.resetFiltro = function () {
                     $scope.filtro = {
                       notUsuario : false,
                       notPerfil : false,
                       usuario : null,
                       perfil : null,
                       listaUsuariosBorrar : null,
                       usuarioSelect : null
                     };

                     $scope.usuarioFiltro = null;
                     $scope.perfilFiltro = {};

                     $scope.borrarTabla();

                     $scope.resetButtonFilters();

                     $scope.usuariosSeleccionados = [];
                     $scope.usuariosSeleccionadosBorrar = [];
                   };

                   $scope.resetFiltro();

                   /***************************************************************************************************
                     * ** FUNCIONES GENERALES
                     **************************************************************************************************/
                   // Control para ocultar o presentar el panel de busqueda
                   $scope.mensajeBusqueda = "Ocultar opciones de búsqueda";
                   $scope.mostrarOcultarFiltros = function () {
                     angular.element("#buscador").slideToggle();
                     if ($scope.mostrarBuscador) {
                       $scope.followSearch = "";
                       if ($scope.usuarioFiltro == null) {
                         $scope.followSearch += "Usuario: TODOS";
                       } else if ($scope.usuarioFiltro.length == 0) {
                         $scope.followSearch += "Usuario: TODOS";
                       } else {
                         $scope.followSearch += "Usuario" + $scope.btnsFilter.btnUsuario + " " + $scope.usuarioFiltro;
                       }
                       if ($scope.perfilFiltro.value == null) {
                         $scope.followSearch += "  Perfil: TODOS";
                       } else if ($scope.perfilFiltro.value.length > 0) {
                         $scope.followSearch += "  Perfil" + $scope.btnsFilter.btnPerfil + " "
                                                + $scope.perfilFiltro.value;
                       } else {
                         $scope.followSearch += "  Perfil: TODOS";
                       }
                       $scope.mostrarBuscador = false;
                       $scope.mensajeBusqueda = "Mostrar opciones de búsqueda";
                     } else {
                       $scope.followSearch = "";
                       $scope.mostrarBuscador = true;
                       $scope.mensajeBusqueda = "Ocultar opciones de búsqueda";
                     }
                   }

                   $scope.controlError = function (txterror) {
                     if (txterror.length == 0) {
                       txterror = "Error genérico, por favor contacte con Soporte IT.";
                     }
                     fErrorTxt(txterror, 1);
                     $scope.mensajeError = txterror;
                     $scope.mostrarErrores = true;
                   }

                   var pupulateAutocomplete = function (input, availableTags) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {

                         var option = {
                           key : ui.item.key,
                           value : ui.item.value
                         };

                         switch (input) {
                           case "#filtro_usuario":
                             $scope.filtro.usuario = option.key;
                             $scope.usuarioFiltro = option.value;
                             break;
                           default:
                             break;
                         }

                         $scope.safeApply();

                         return false;
                       }
                     });
                   };

                   $scope.invertirValores = function (option) {

                     switch (option) {
                       case "usuario":
                         if (!$scope.filtro.notUsuario) {
                           $scope.filtro.notUsuario = true;
                           $scope.btnsFilter.textUsuario = "DISTINTO";
                           $scope.btnsFilter.btnUsuario = "<>";
                           $scope.btnsFilter.colorUsuario = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notUsuario = false;
                           $scope.btnsFilter.textUsuario = "IGUAL";
                           $scope.btnsFilter.btnUsuario = "=";
                           $scope.btnsFilter.colorUsuario = {
                             'color' : 'green'
                           };
                         }
                         break;
                       case "perfil":
                         if (!$scope.filtro.notPerfil) {
                           $scope.filtro.notPerfil = true;
                           $scope.btnsFilter.textPerfil = "DISTINTO";
                           $scope.btnsFilter.btnPerfil = "<>";
                           $scope.btnsFilter.colorPerfil = {
                             'color' : 'red'
                           };
                         } else {
                           $scope.filtro.notPerfil = false;
                           $scope.btnsFilter.textPerfil = "IGUAL";
                           $scope.btnsFilter.btnPerfil = "=";
                           $scope.btnsFilter.colorPerfil = {
                             'color' : 'green'
                           };
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   /***************************************************************************************************
                     * ** CARGA DE DATOS INICIALES
                     **************************************************************************************************/

                   // Carga combo Usuarios
                   GestionUsuariosService.cargaUsuarios(function (data) {
                     $scope.listUsuarios = data.resultados.listaUsuarios;
                     pupulateAutocomplete("#filtro_usuario", $scope.listUsuarios);
                   }, function (error) {
                     fErrorTxt("Se produjo un error en la carga de los usuarios.", 1);
                   });

                   // Carga combo Perfiles
                   GestionUsuariosService.cargaPerfiles(function (data) {
                     $scope.listPerfiles = data.resultados.listaPerfiles;
                     // pupulateAutocomplete("#filtro_perfil", $scope.listPerfiles);
                   }, function (error) {
                     fErrorTxt("Se produjo un error en la carga de los perfiles.", 1);
                   });

                   /***************************************************************************************************
                     * ** ACCIONES
                     **************************************************************************************************/

                   $scope.pintarTablaUsuarios = function (data) {
                     $scope.listadoUsuarios = data.resultados.listaUsuarios;

                     // se inicializan las variables de usuarios seleccionados
                     $scope.usuariosSeleccionados = [];
                     $scope.usuariosSeleccionadosBorrar = [];

                     // borra el contenido del body de la tabla
                     $scope.borrarTabla();

                     for (var i = 0; i < $scope.listadoUsuarios.length; i++) {

                       var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listadoUsuarios['
                                   + i + '].selected" ng-click="seleccionarElemento(' + i + ');"/>';
                       var perfilesList = "";
                       $.each($scope.listadoUsuarios[i].listaProfiles, function (index_i, contenido) {
                         if (index_i == 0) {
                           perfilesList += contenido.value;
                         } else {
                           perfilesList += ", " + contenido.value;
                         }
                       });

                       var usuariosList = [ check, $scope.listadoUsuarios[i].username, $scope.listadoUsuarios[i].name,
                                           $scope.listadoUsuarios[i].lastname, perfilesList ];

                       $scope.oTable.fnAddData(usuariosList, false);

                     }
                     $scope.oTable.fnDraw();
                     $.unblockUI();
                   }

                   // Carga del listado de plantillas de informe
                   $scope.getUsuarios = function () {
                     $scope.setFiltro();

                     // Se oculta el panel de búsqueda y se muestra el detalle
                     $scope.mostrarOcultarFiltros();
                     // Se muestra la capa cargando
                     inicializarLoading();

                     GestionUsuariosService.buscaUsuarios(function (data) {
                       $scope.pintarTablaUsuarios(data);
                     }, function (error) {
                       $.unblockUI();
                       fErrorTxt("Se produjo un error en la carga del listado de usuarios.", 1);
                     }, $scope.filtro);
                   };

                   $scope.borrarUsuarios = function () {
                     if ($scope.usuariosSeleccionadosBorrar.length > 0) {
                       if ($scope.usuariosSeleccionadosBorrar.length == 1) {
                         angular.element("#dialog-confirm").html("¿Desea eliminar el usuario seleccionado?");
                       } else {
                         angular.element("#dialog-confirm").html(
                                                                 "¿Desea eliminar los "
                                                                     + $scope.usuariosSeleccionadosBorrar.length
                                                                     + " usuarios seleccionados?");
                       }

                       // Define the Dialog and its properties.
                       angular.element("#dialog-confirm").dialog({
                         resizable : false,
                         modal : true,
                         title : "Mensaje de Confirmación",
                         height : 150,
                         width : 360,
                         buttons : {
                           " Sí " : function () {
                             inicializarLoading();
                             $scope.setFiltro();

                             GestionUsuariosService.eliminarUsuarios(function (data) {
                               $scope.pintarTablaUsuarios(data);
                             }, function (error) {
                               $.unblockUI();
                               fErrorTxt("Ocurrió un error al eliminar los usuarios.", 1);
                             }, $scope.filtro);

                             $(this).dialog('close');
                           },
                           " No " : function () {
                             $(this).dialog('close');
                           }
                         }
                       });
                       $('.ui-dialog-titlebar-close').remove();
                     } else {
                       fErrorTxt("Debe seleccionar al menos un elemento de la tabla de usuarios.", 2)
                     }
                   };

                   $scope.resetearPassword = function () {
                     if ($scope.usuariosSeleccionadosBorrar.length > 0) {
                       if ($scope.usuariosSeleccionadosBorrar.length == 1) {
                         angular.element("#dialog-confirm")
                             .html("¿Desea resetear el password a el usuario seleccionado?");
                       } else {
                         angular.element("#dialog-confirm").html(
                                                                 "¿Desea resetear el password a los "
                                                                     + $scope.usuariosSeleccionadosBorrar.length
                                                                     + " usuarios seleccionados?");
                       }

                       // Define the Dialog and its properties.
                       angular.element("#dialog-confirm").dialog({
                         resizable : false,
                         modal : true,
                         title : "Mensaje de Confirmación",
                         height : 150,
                         width : 360,
                         buttons : {
                           " Sí " : function () {
                             inicializarLoading();
                             $scope.setFiltro();

                             GestionUsuariosService.resetearPassword(function (data) {
                               $scope.pintarTablaUsuarios(data);
                             }, function (error) {
                               $.unblockUI();
                               fErrorTxt("Ocurrió un error al resetear el password a los usuarios.", 1);
                             }, $scope.filtro);

                             $(this).dialog('close');
                           },
                           " No " : function () {
                             $(this).dialog('close');
                           }
                         }
                       });
                       $('.ui-dialog-titlebar-close').remove();
                     } else {
                       fErrorTxt("Debe seleccionar al menos un elemento de la tabla de usuarios.", 2)
                     }
                   };

                   /***************************************************************************************************
                     * ** ACCIONES CRUD / MODALES
                     **************************************************************************************************/
                   // Reset objeto usuario
                   $scope.resetUsuario = function () {
                     $scope.usuario = {
                       username : "",
                       name : "",
                       lastname : "",
                       password : "",
                       password2 : "",
                       listaProfiles : [],
                       email : ""
                     };

                     $scope.activeAlert = false;
                     $scope.mensajeAlert = "";
                   };

                   $scope.procesarPerfil = function () {
                     var existe = false;
                     angular.forEach($scope.usuario.listaProfiles, function (campo) {
                       if (campo.value === $scope.form.perfil.value) {
                         existe = true;
                       }
                     });
                     if (!existe) {
                       $scope.usuario.listaProfiles.push($scope.form.perfil);
                     }
                     $scope.form.perfil = "";
                   };

                   $scope.eliminarElementoTabla = function (tabla) {
                     switch (tabla) {
                       case "perfil":
                         for (var i = 0; i < $scope.usuario.listaProfiles.length; i++) {
                           if ($scope.usuario.listaProfiles[i].value === $scope.usuario.perfil.value) {
                             $scope.usuario.listaProfiles.splice(i, 1);
                           }
                         }
                         break;
                       default:
                         break;
                     }
                   };

                   $scope.vaciarTabla = function (tabla) {
                     switch (tabla) {
                       case "perfil":
                         $scope.usuario.listaProfiles = [];
                         break;
                       default:
                         break;
                     }
                   };

                   // Abrir modal creacion usuarios
                   $scope.abrirCrearUsuario = function () {
                     // se inicializa el usuario
                     $scope.resetUsuario();
                     $scope.modal.showCrear = true;
                     $scope.modal.showModificar = false;
                     $scope.modal.titulo = "Crear Usuario";
                     angular.element('#formularios').modal({
                       backdrop : 'static'
                     });
                     $(".modal-backdrop").remove();
                   };

                   // Abrir modal modificar usuarios
                   $scope.abrirModificarUsuario = function () {
                     if ($scope.usuariosSeleccionados.length > 1 || $scope.usuariosSeleccionados.length == 0) {
                       if ($scope.usuariosSeleccionados.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de usuarios.", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de usuarios.", 2)
                       }
                     } else {
                       $scope.mensajeAlert = "";
                       // se copian los datos del usuario seleccionado en la variable usuario
                       $scope.usuario = angular.copy($scope.usuariosSeleccionados[0]);
                       $scope.usuario.password2 = $scope.usuario.password;

                       $scope.modal.showCrear = false;
                       $scope.modal.showModificar = true;
                       $scope.modal.titulo = "Modificar Usuario";
                       angular.element('#formularios').modal({
                         backdrop : 'static'
                       });
                       $(".modal-backdrop").remove();

                     }

                   };

                   $scope.validacionFormulario = function () {
                     if ($scope.usuario.username.length == 0 || $scope.usuario.name.length == 0
                         || $scope.usuario.lastname.length == 0 || $scope.usuario.password.length == 0
                         || $scope.usuario.password2.length == 0) {
                       return true;
                     } else {
                       if ($scope.usuario.password !== $scope.usuario.password2) {
                         return true;
                       } else {
                         return false;
                       }
                     }
                   }

                   $scope.guardarUsuario = function () {

                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       inicializarLoading();
                       $scope.setFiltro();
                       GestionUsuariosService.crearUsuario(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           $scope.pintarTablaUsuarios(data);
                           angular.element('#formularios').modal("hide");
                           fErrorTxt('Usuario ' + $scope.usuario.username + ' creado correctamente', 3);
                         }
                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al crear el usuario.", 1);
                       }, $scope.filtro);
                     }
                   };

                   $scope.modificarUsuario = function () {

                     $scope.activeAlert = $scope.validacionFormulario();
                     if (!$scope.activeAlert) {
                       inicializarLoading();
                       $scope.setFiltro();
                       GestionUsuariosService.modificarUsuario(function (data) {
                         if (data.resultados.status === 'KO') {
                           $.unblockUI();
                           $scope.srcImage = "images/warning.png";
                           $scope.mensajeAlert = data.error;
                         } else {
                           $scope.pintarTablaUsuarios(data);
                           angular.element('#formularios').modal("hide");
                           fErrorTxt('Usuario ' + $scope.usuario.username + ' modificado correctamente', 3);
                         }
                       }, function (error) {
                         $.unblockUI();
                         angular.element('#formularios').modal("hide");
                         fErrorTxt("Ocurrió un error durante la petición de datos al modificar el usuario.", 1);
                       }, $scope.filtro);
                     }
                   };

                   $scope.seleccionarTodos = function () {
                     // Se inicializa la lista de usuarios a borrar.
                     $scope.usuariosSeleccionadosBorrar = [];
                     for (var i = 0; i < $scope.listadoUsuarios.length; i++) {
                       $scope.listadoUsuarios[i].selected = true;
                       $scope.usuariosSeleccionadosBorrar.push($scope.listadoUsuarios[i].idUser);
                     }
                     $scope.usuariosSeleccionados = angular.copy($scope.listadoUsuarios);
                   };

                   $scope.deseleccionarTodos = function () {
                     for (var i = 0; i < $scope.listadoUsuarios.length; i++) {
                       $scope.listadoUsuarios[i].selected = false;
                     }
                     $scope.usuariosSeleccionados = [];
                     $scope.usuariosSeleccionadosBorrar = [];
                   };

                 } ]);
