'use strict';
sibbac20.controller(
		'PlanificacionInformesController',
		[
			'$scope',
			'$filter',
			//Servicios
			'PlanificacionInformesService',
			'$document',
			'growl',
			'$compile',
			'CONSTANTES', 
			'commonHelper',
			function($scope, $filter, PlanificacionInformesService, $document, growl, $compile, CONSTANTES, commonHelper) {
				
				$scope.safeApply = function (fn) {
					var phase = this.$root.$$phase;
					if (phase === '$apply' || phase === '$digest') {
						if (fn && (typeof (fn) === 'function')) {
							fn();
						}
					} else {
						this.$apply(fn);
					}
	             };

                 var hoy = new Date();
                 var dd = hoy.getDate();
                 var mm = hoy.getMonth() + 1;
                 var yyyy = hoy.getFullYear();
                 hoy = yyyy + "_" + mm + "_" + dd;
	             
	             $scope.showingFilter = true;

	             $scope.filtro = {};
	             $scope.validaModal = {};
	             $scope.listInformes = [];
	             $scope.listInformesAlta = [];
	             $scope.listPeriodos = [];
	             $scope.listaFicheros = [];
	             $scope.listTipoDestino = [];
	             $scope.listaDestinos = [];
	             $scope.listFormato = [];
	             $scope.listInformesPlanificacion = [];
	             $scope.planificacionInformesSeleccionadas = [];
	             $scope.planificacionInformesSeleccionadasBorrar = [];
	             $scope.listClasesFiltro = [];
	             $scope.listMercados = [];
	             
                 $scope.modal = {
	                 titulo : "",
	                 showCrear : false,
	                 showModificar : false,
	                 showAlias : false,
	                 showIsin : false
	             };
                 
                 $scope.alta = {};

                 // Validaciones
                 $scope.validaModal.isMailValido = true;
                 $scope.validaModal.isCamposRequeridos = true;
                 $scope.validaModal.isRutaValida = true;
	             
	             var labelFiltro = {
	            		 clase : "Clase",
	            		 mercado : "Mercado",
                		 informe : "Informe", 
                		 periodo : "Periodo Planificado",
                		 fichero : "Nombre Fichero",
                		 tipoDestino: "Tipo Destino",
                		 destino: "Destino",
                		 formato: "Formato"
	                 };
	             
	             $scope.getValorDescriptivo = function (filtro, valor) {
	             	return valor;
	             }
	             
	         	/** Follow search.  */
	             $scope.getFollowSearch = function () {
	             	var texto = "";

	             	for ( var key in $scope.filtro) {
	             		if ($scope.filtro[key] != null && $scope.filtro[key] != "") {

	             			var operador = "";
	             			if ($scope.sentidoFiltro[key] == "IGUAL") {
	             				operador = "=";
	             			} else {
	             				operador = "<>";
	             			}

	             			var valorDescriptivo = $scope.filtro[key];

	             			if (texto == "") {
	             				if(null!=valorDescriptivo.value) {
	             					texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo.value;
	             				} else {
	             					if(null!=valorDescriptivo.nombre){
	             						texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo.nombre;
	             					}else{
	             						texto += labelFiltro[key] + " " + operador + " " + valorDescriptivo;
	             					}
	             				}
	             				
	             			} else {
	             				if(null!=valorDescriptivo.value) {
	             					texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo.value;
	             				} else {
	             					if(null!=valorDescriptivo.nombre){
	             						texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo.nombre;
	             					}else{
	             						texto += ", " + labelFiltro[key] + " " + operador + " " + valorDescriptivo;
	             					}
	             				}
	             			}

	             		}
	             	}
	             	$scope.followSearch = texto;
	             	return texto;
	             }
	             
				 $scope.consultarInformes = function () {
					 PlanificacionInformesService.cargaComboInformes(function (data) {
						 if (data.resultados.status == 'KO') {
							 fErrorTxt("Se produjo un error en la carga del combo de plantillas", 1);
						 } else {
							 $scope.listInformes = data.resultados["listInformes"];
							 $scope.listInformesAlta = data.resultados["listInformes"];
							 pupulateAutocomplete("#filtro_informe", $scope.listInformes);
							 pupulateAutocomplete("#alta_informe", $scope.listInformesAlta);
						 }
					 }, function (error) {
						 fErrorTxt("Se produjo un error en la carga del combo de plantillas", 1);
					 });
				 };
				 
	             $scope.resetFiltro = function () {
	                 $scope.filtro = {
	                	     clase : "",
	                	     mercado : {key: "", value: ""},
	                		 informe : "", 
	                		 periodo : "",
	                		 fichero : "",
	                		 tipoDestino: "",
	                		 destino: "",
	                		 formato: ""
	                 };
	                $scope.consultarInformes();
	                $scope.listMercados = [];
	                $scope.sentidoFiltro = [];
	                $scope.informeFiltro = "";
	                $scope.periodoFiltro = "";
	            	for ( var key in $scope.filtro) {
	            		$scope.sentidoFiltro[key] = "IGUAL";
	            	}
	             };
	             
	             $scope.resetFiltro();
				 
				 $scope.consultarClases = function () {
					 // Carga del combo Clases
					 PlanificacionInformesService.cargarClases(function (data) {
						 $scope.listClasesFiltro = data.resultados["listClases"];
					 }, function (error) {
						 fErrorTxt("Se produjo un error en la carga del combo de clases.", 1);
					 });
				 };
				 
				 $scope.consultarClases();

				 $scope.consultarPeriodos = function () {
					 PlanificacionInformesService.cargaComboPeriodos(function (data) {
						 if (data.resultados.status == 'KO') {
							 fErrorTxt("Se produjo un error en la carga del combo de periodos", 1);
						 } else {
							 $scope.listPeriodos = data.resultados["listPeriodos"];
							 pupulateAutocomplete("#filtro_periodo", $scope.listPeriodos);
							 pupulateAutocomplete("#alta_periodo", $scope.listPeriodos);
						 }
					 }, function (error) {
						 fErrorTxt("Se produjo un error en la carga del combo de periodos", 1);
					 });
				 };
				 $scope.consultarPeriodos();

				 $scope.consultarFicheros = function () {
					 PlanificacionInformesService.cargaComboFicheros(function (data) {
						 if (data.resultados.status == 'KO') {
							 fErrorTxt("Se produjo un error en la carga del combo de ficheros", 1);
						 } else {
							 $scope.listaFicheros = data.resultados["listFicheros"];
							 pupulateAutocomplete("#filtro_fichero", $scope.listaFicheros);
							 pupulateAutocomplete("#alta_fichero", $scope.listaFicheros);
						 }
					 }, function (error) {
						 fErrorTxt("Se produjo un error en la carga del combo de periodos", 1);
					 });
				 };
				 $scope.consultarFicheros();

				 $scope.consultarDestinos = function () {
					 PlanificacionInformesService.cargaComboDestinos(function (data) {
						 if (data.resultados.status == 'KO') {
							 fErrorTxt("Se produjo un error en la carga del combo de destinos", 1);
						 } else {
							 $scope.listDestinos = data.resultados["listDestino"];
							 pupulateAutocomplete("#filtro_destino", $scope.listDestinos);
							 pupulateAutocomplete("#alta_destino", $scope.listDestinos);
						 }
					 }, function (error) {
						 fErrorTxt("Se produjo un error en la carga del combo de periodos", 1);
					 });
				 };
				 $scope.consultarDestinos();

				 $scope.cargarCombosEstaticos = function () {
					 PlanificacionInformesService.cargaCamposEstaticos(function (data) {
						 if (data.resultados.status == 'KO'){
							 fErrorTxt("Se produjo un error en la carga del combo de plantillas", 1);
						 } else {
							 $scope.listTipoDestino = data.resultados["listaTipoDestino"];
							 $scope.listFormato = data.resultados["listaFormatos"];
						 }
					 }, function (error) {
						 fErrorTxt("Se produjo un error en la carga de los combos de tipo destino y formato", 1);
					 });
				 };
				 $scope.cargarCombosEstaticos();
				 
				$scope.asignarValueInputInforme = function(){
					$scope.filtro.informe = $scope.filtro.informeSelect.value;
				};
				
			    $scope.resetInforme = function(){
			    	$scope.filtro.informeSelect = "";
			    };

                var pupulateAutocomplete = function (input, availableTags) {
                    $(input).autocomplete({
                      minLength : 0,
                      source : availableTags,
                      focus : function (event, ui) {
                        return false;
                      },
                      select : function (event, ui) {

                        var option = {
                          key : ui.item.key,
                          value : ui.item.value
                        };

                        switch (input) {
                          case "#filtro_informe":
                            $scope.filtro.informe = option.key;
                            $scope.informeFiltro = option.value;
                            break;
                          case "#filtro_periodo":
                              $scope.filtro.periodo = option.key;
                              $scope.periodoFiltro = option.value;
                              break;
                          case "#filtro_fichero":
                              $scope.filtro.fichero = option.key;
                              $scope.ficheroFiltro = option.value;
                              break;
                          case "#filtro_destino":
                              $scope.filtro.destino = option.key;
                              $scope.destinoFiltro = option.value;
                              break;
                          case "#alta_informe":
                              $scope.alta.informe = option.key;
                              $scope.informeAlta = option.value;
                        	  break;
                          case "#alta_periodo":
                              $scope.alta.periodo = option.key;
                              $scope.periodoAlta = option.value;
                        	  break;
                          case "#alta_fichero":
                              $scope.alta.fichero = option.key;
                              $scope.ficheroAlta = option.value;
                              break;
                          case "#alta_destino":
                              $scope.alta.destino = option.key;
                              $scope.destinoAlta = option.value;
                              break;

                          default:
                            break;
                        }

                        $scope.safeApply();

                        return false;
                      }
                    });
                  };
                  
                  $scope.consultar = function() {
                	  $scope.showingFilter = false;
                	  $scope.getPlanificacionInformes();
                  };

                  $scope.getPlanificacionInformes = function () {
                	  inicializarLoading();
                	  PlanificacionInformesService.cargaPlanificacionInformes(function (data) {
                		  if (data.resultados.status == 'KO'){
 							 fErrorTxt("Se produjo un error en la busqueda de planificaciones de informes", 1);
 						 } else {
 							$scope.listInformesPlanificacion = data.resultados["listInformesPlanificacion"];
                            // borra el contenido del body de la tabla
                            var tbl = $("#datosPlanificacionInformes > tbody");
                            $(tbl).html("");
                            $scope.oTable.fnClearTable();
                            
                            for (var i = 0; i < $scope.listInformesPlanificacion.length; i++) {
                            	var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listInformesPlanificacion['
                                    + i
                                    + '].selected" ng-click="seleccionarElemento('
                                    + i + ');"/>';
                            	
                            	var plantillaList = [
                            	                     check,
                            	                     $scope.listInformesPlanificacion[i].informe,
                            	                     $scope.listInformesPlanificacion[i].periodo,
                            	                     $scope.listInformesPlanificacion[i].nombreFichero,
                            	                     $scope.listInformesPlanificacion[i].tipoDestino,
                            	                     $scope.listInformesPlanificacion[i].destino,
                            	                     $scope.listInformesPlanificacion[i].formato,
                            	                     $scope.listInformesPlanificacion[i].separadorDecimal,
                            	                     $scope.listInformesPlanificacion[i].separadorMiles,
                            	                     $scope.listInformesPlanificacion[i].separadorCampos];
                            	$scope.oTable.fnAddData(plantillaList, false);
                            }
                            $scope.oTable.fnDraw();
                            $.unblockUI();
 						 }
                	  }, function (error) {
 						 fErrorTxt("Se produjo un error en la busqueda de planificaciones de informes", 1);
 					 }, $scope.filtro);
                  };

                  /***************************************************************************************************
                   * ** DEFINICION TABLA ***
                   **************************************************************************************************/

                  $scope.oTable = $("#datosPlanificacionInformes").dataTable({
                      "dom" : 'T<"clear">lfrtip',
                      "tableTools" : {
                        "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                        "aButtons" : [ "copy", {
                          "sExtends" : "csv",
                          "sFileName" : "Listado_Planificacion_Informes_" + hoy + ".csv",
                          "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
                        }, {
                          "sExtends" : "xls",
                          "sFileName" : "Listado_Planificacion_Informes_" + hoy + ".xls",
                          "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
                        }, {
                          "sExtends" : "pdf",
                          "sPdfOrientation" : "landscape",
                          "sTitle" : " ",
                          "sPdfSize" : "A3",
                          "sPdfMessage" : "Listado Final Holders",
                          "sFileName" : "Listado_Planificacion_Informes_" + hoy + ".pdf",
                          "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
                        }, "print" ]
                      },
                      "aoColumns" : [ {
                        sClass : "centrar",
                        bSortable : false,
                        width : "10%"
                      }, {
                        sClass : "centrar",
                        width : "10%"
                      }, {
                        sClass : "centrar",
                        width : "10%"
                      }, {
                        sClass : "centrar",
                        width : "10%"
                      }, {
                        sClass : "centrar",
                        width : "10%"
                      }, {
                        sClass : "centrar",
                        width : "10%"
                      }, {
                    	sClass : "centrar",
	                    width : "10%"
	                  }, {
                        sClass : "centrar",
                        width : "10%"
                      }, {
                        sClass : "centrar",
                        width : "10%"
                      }, {
                    	sClass : "centrar",
                        width : "10%"
                      } ],
                      "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                        $compile(nRow)($scope);
                      },

                      "scrollY" : "480px",
                      "scrollX" : "100%",
                      "scrollCollapse" : true,
                      "language" : {
                        "url" : "i18n/Spanish.json"
                      }
                    });

                  $scope.seleccionarElemento = function (row) {
                      if ($scope.listInformesPlanificacion[row].selected) {
                        $scope.listInformesPlanificacion[row].selected = false;
                        for (var i = 0; i < $scope.planificacionInformesSeleccionadas.length; i++) {
                          if ($scope.planificacionInformesSeleccionadas[i].idPlanificacion === $scope.listInformesPlanificacion[row].idPlanificacion) {
                            $scope.planificacionInformesSeleccionadas.splice(i, 1);
                            $scope.planificacionInformesSeleccionadasBorrar.splice(i, 1);
                          }
                        }
                      } else {
                        $scope.listInformesPlanificacion[row].selected = true;
                        $scope.planificacionInformesSeleccionadas.push($scope.listInformesPlanificacion[row]);
                        $scope.planificacionInformesSeleccionadasBorrar.push($scope.listInformesPlanificacion[row].idPlanificacion);

                      }
                    };
                  
                  /***************************************************************************************************
                   * ** ACCIONES TABLA ***
                   **************************************************************************************************/
                 $scope.seleccionarTodos = function () {
                   // Se inicializa los elementos que ya tuviera la lista de elementos a borrar.
                   $scope.planificacionInformesSeleccionadasBorrar = [];
                   for (var i = 0; i < $scope.listInformesPlanificacion.length; i++) {
                     $scope.listInformesPlanificacion[i].selected = true;
                     $scope.planificacionInformesSeleccionadasBorrar.push($scope.listInformesPlanificacion[i].idPlanificacion);
                   }
                   $scope.planificacionInformesSeleccionadas = angular.copy($scope.listInformesPlanificacion);
                 };

                 $scope.deseleccionarTodos = function () {
                     for (var i = 0; i < $scope.listInformesPlanificacion.length; i++) {
                       $scope.listInformesPlanificacion[i].selected = false;
                     }
                     $scope.planificacionInformesSeleccionadas = [];
                     $scope.planificacionInformesSeleccionadasBorrar = [];
                 };

                 /***************************************************************************************************
                  * ** ACCIONES MODALES ***
                  **************************************************************************************************/

                // Abrir modal creacion planificacion informes
                $scope.abrirCrearPlanificacionInforme = function () {
                  $scope.modal.showCrear = true;
                  $scope.modal.showModificar = false;
                  $scope.modal.titulo = "Crear Planificacion Informe";
                  $scope.resetForms();
                  $scope.informeAlta = "";
                  $scope.periodoAlta = "";
                  $scope.alta.tipoDestino = $scope.listTipoDestino[0].key;
                  $scope.alta.formato = $scope.listFormato[1].key;
                  // Reset de validaciones al abrir modal.
                  $scope.resetValidaModal();
                  
                  angular.element('#formularios').modal({
                    backdrop : 'static'
                  });
                  $(".modal-backdrop").remove();
                };
                $scope.abrirModificarPlanificacionInforme = function () {
                	
                    // Reset de validaciones al abrir modal.
                	$scope.resetValidaModal();
                	
                    if ($scope.planificacionInformesSeleccionadas.length > 1
                            || $scope.planificacionInformesSeleccionadas.length == 0) {
                          if ($scope.planificacionInformesSeleccionadas.length == 0) {
                            fErrorTxt("Debe seleccionar al menos un elemento de la tabla de planificacion de informe", 2)
                          } else {
                            fErrorTxt("Debe seleccionar solo un elemento de la tabla de planificacion de informe.", 2)
                          }
                        } else {
                        	$scope.modal.showCrear = false;
                            $scope.modal.showModificar = true;
                        	$scope.modal.titulo = "Modificar Planificacion Informe";
                        	$scope.resetForms();

                            $scope.alta = angular.copy($scope.planificacionInformesSeleccionadas[0]);

                            PlanificacionInformesService
                                .detallePlanificacionInforme(
                                                      function (data) {

                                                        $scope.resultado = data.resultados["detallePlanificacionInforme"];
                                                        
                                                        $scope.alta = {
                                                        		idPlanificacion : $scope.resultado.idPlanificacion,
                                                        		informe : $scope.resultado.idInforme,
                                                        		periodo : $scope.resultado.idPeriodo,
                                                        		fichero : $scope.resultado.nombreFichero,
                                                        		destino : $scope.resultado.destino,
                                                        		formato : $scope.resultado.formato,
                                                        		tipoDestino : $scope.resultado.tipoDestino,
                                                        		sepDecimal : $scope.resultado.separadorDecimal,
                                                        		sepMiles : $scope.resultado.separadorMiles,
                                                        		sepCampos : $scope.resultado.separadorCampos
                                                        };
                                                        $scope.informeAlta = $scope.resultado.informe;
                                                        $scope.periodoAlta = $scope.resultado.periodo;

                                                    	angular.element('#formularios').modal({
                                                    		backdrop : 'static'
                                                    	});
                                                    	$(".modal-backdrop").remove();

                                                      },
                                                      function (e) {
                                                        $.unblockUI();
                                                        fErrorTxt(
                                                                  "Ocurrió un error durante la petición de datos al recuperar el objeto planificacionInforme.",
                                                                  1);
                                                      }, $scope.alta);
                        }
                };
                
                $scope.resetForms = function () {
                    $scope.alta = {
                    		idPlanificacion : "",
                    		informe : "",
                    		periodo : "",
                    		fichero : "",
                    		destino : "",
                    		formato : "",
                    		tipoDestino : "",
                    		sepDecimal : ".",
                    		sepMiles : ",",
                    		sepCampos : "|"
                    }
                };

                  $scope.resetForms();

                /***************************************************************************************************
                 * ** ACCIONES CRUD / MODALES ***
                 **************************************************************************************************/

               $scope.srcImage = "images/warning.png";
               $scope.crearPlanificacionInforme = function () {
            	   
                 $scope.activeAlert = $scope.validacionFormulario();
                 if (!$scope.activeAlert) {
                	 PlanificacionInformesService.guardarPlanificacionInformes(function (data) {
                     if (data.resultados.status === 'KO') {
                       $.unblockUI();
                       $scope.srcImage = "images/warning.png";
                       $scope.mensajeAlert = data.error;
                     } else {
                    	 $scope.planificacionInformesSeleccionadas = [];
                    	 $scope.getPlanificacionInformes();
                         angular.element('#formularios').modal("hide");
                         $scope.activeAlert = false;
                         if ($scope.modal.showModificar) {
                        	 fErrorTxt('Planificacion de informe modificada correctamente', 3);
                         } else {
                        	 fErrorTxt('Planificacion de informe creada correctamente', 3);
                         }
                         
                     }
                   }, function (e) {
                     $.unblockUI();
                     angular.element('#formularios').modal("hide");
                     fErrorTxt("Ocurrió un error durante la petición de datos al crear planificacion de informe.", 1);
                   }, $scope.alta);
                 }
               };
               
               /**
                *	Validar planificacion de informe. 
                */
               $scope.validarPlanificacionInforme = function () {
            	   $scope.activeAlert = $scope.validacionFormulario();
            	   if (!$scope.activeAlert) {
            		   PlanificacionInformesService.validarPlanificacionInforme(function (data) {
            			   if (!commonHelper.isUndefinedOrNullOrEmpty(data.resultadosError)) {
            				   // Mensaje de validacion (restrictivo)
            				   var msgValidPlanif = data.resultadosError.msgValidPlanif;
            				   if(!commonHelper.isUndefinedOrNullOrEmpty(msgValidPlanif) && msgValidPlanif.length > 0) {
            					   if (!commonHelper.isUndefinedOrNull(msgValidPlanif)) {
            						   $scope.showModalValidPlanif(msgValidPlanif);
            					   } 
            				   }            			   
            				   // Mensaje de confirmacion (no-restrictivo)
            				   var msgConfirmPlanif = data.resultadosError.msgConfirmPlanif;
            				   if (!commonHelper.isUndefinedOrNullOrEmpty(msgConfirmPlanif) && msgConfirmPlanif.length > 0) {
            					   if (!commonHelper.isUndefinedOrNull(msgConfirmPlanif)) {
            						   $scope.showModalConfirmPlanif(msgConfirmPlanif);
            					   }
            				   }
            			   } else {
            				   $scope.crearPlanificacionInforme();
            			   }
            		   },
            		   function (e) {
            			   $.unblockUI();
            			   angular.element('#formularios').modal("hide");
            			   fErrorTxt("Ocurrió un error durante la petición de datos al crear planificacion de informe.", 1);
            		   }, $scope.alta);
            	   }
               };
               
               /**
                *	Esta modal permite mostrar la confirmacion de planificacion.
                *	Caracter NO-RESTRICTIVO
                */
               $scope.showModalValidPlanif = function(mensaje) {
            	   $.unblockUI();
            	   angular.element("#dialog-confirm2").html(mensaje);
            	   angular.element("#dialog-confirm2").dialog({
            		   resizable : true,
            		   modal : true,
            		   title : CONSTANTES.MODAL_TITULO_VALIDACION,
            		   height : 200,
            		   width : 900,
            		   buttons : {
            			   " Cerrar " : function () {
            				   $(this).dialog('close');
            			   }
            		   }
            	   });
            	   $('.ui-dialog-titlebar-close').remove();
               };
               
               /**
                *	Esta modal permite mostrar la validacion de planificacion.
                *	Caracter RESTRICTIVO
                */
               $scope.showModalConfirmPlanif = function(mensaje) {
            	   $.unblockUI();
            	   angular.element("#dialog-confirm3").html(mensaje);
            	   angular.element("#dialog-confirm3").dialog({
            		   resizable : true,
            		   modal : true,
            		   title : CONSTANTES.MODAL_TITULO_CONFIRM,
            		   height : 200,
            		   width : 900,
            		   buttons : {
            			   " Sí " : function () {
            				   $scope.crearPlanificacionInforme();
            				   $(this).dialog('close');
            			   }," No " : function () {
            				   $(this).dialog('close');
            			   }
            		   }
            	   });
            	   $('.ui-dialog-titlebar-close').remove();
               };

               /**
                *	Validacion de la modal de planificacion de informe.
                *	@return boolean 
                */
               $scope.validacionFormulario = function () {

            	   // Reset de validaciones.
            	   $scope.resetValidaModal();

            	   // Valida campos requeridos.
            	   $scope.validaModal.isCamposRequeridos = $scope.alta.informe == "" || $scope.alta.periodo == "" 
            		   || $scope.alta.fichero == ""  || $scope.alta.tipoDestino == ""  
            			   || (($scope.alta.tipoDestino == CONSTANTES.RUTA) && $scope.alta.destino == "")  || $scope.alta.formato == "";

            	   // Valida ruta de sistema de archivos.
            	   if ($scope.alta.tipoDestino == CONSTANTES.RUTA) {
            		   $scope.validaModal.isRutaValida = commonHelper.isRutaValida($scope.alta.destino);
            	   }

            	   // Valida direccion de e-mail.
//            	   if ($scope.alta.tipoDestino == CONSTANTES.EMAIL) {
//            		   $scope.validaModal.isMailValido = commonHelper.isMailValido($scope.alta.destino);
//            	   }

            	   if ($scope.validaModal.isCamposRequeridos || !$scope.validaModal.isMailValido || !$scope.validaModal.isRutaValida) {
            		   return true;
            	   } else {
            		   return false;
            	   }
               }
               
               /**
                *	Reset de validaciones. 
                * 	@param scope
                */
               $scope.resetValidaModal = function () {
                   $scope.activeAlert = false;
                   // Validaciones
                   $scope.validaModal.isMailValido = true;
                   $scope.validaModal.isCamposRequeridos = true;
                   $scope.validaModal.isRutaValida = true;
               };
               
               $scope.cerrarPlanificacion = function () {
            	   angular.element('#formularios').modal("hide");
               };

               $scope.borrarPlanificacionInforme = function () {
            	   if ($scope.planificacionInformesSeleccionadasBorrar.length == 0) {
            		   fErrorTxt("Debe seleccionar al menos un elemento de la tabla de planificacion de informe", 2)
                   } else {
                	   if ($scope.planificacionInformesSeleccionadasBorrar.length == 1) {
                           angular.element("#dialog-confirm").html("¿Desea eliminar la planificacion de informe seleccionada?");
                	   } else {
                           angular.element("#dialog-confirm")
                           .html(
                                 "¿Desea eliminar las " + $scope.planificacionInformesSeleccionadasBorrar.length + " planificaciones de informe seleccionadas?");
                	   }
                       // Define the Dialog and its properties.
                       angular
                           .element("#dialog-confirm")
                           .dialog(
                                   {
                                     resizable : false,
                                     modal : true,
                                     title : "Mensaje de Confirmación",
                                     height : 200,
                                     width : 500,
                                     buttons : {
                                       " Sí " : function () {
                                         PlanificacionInformesService
                                             .eliminarPlanificacion(
                                                                 function (data) {
                                                                	 $scope.getPlanificacionInformes();
                                                                 },
                                                                 function (error) {
                                                                   $.unblockUI();
                                                                   fErrorTxt("Ocurrió un error durante la petición de datos al eliminar planificacion de informes.",
                                                                             1);
                                                                 },
                                                                 {
                                                                   listaPlanificacionesBorrar : $scope.planificacionInformesSeleccionadasBorrar
                                                                 });
                                         $scope.deseleccionarTodos();
                                         $(this).dialog('close');
                                       },
                                       " No " : function () {
                                         $(this).dialog('close');
                                       }
                                     }
                                   });
                       $('.ui-dialog-titlebar-close').remove();

                   }
               };
               
               $scope.cargarPlantillasMercadosFiltro = function (consultarMercados) {
            	   PlanificacionInformesService.cargaPlantillasFiltro(function (data) {
            		   $scope.listInformes = data.resultados["listaPlantillasFiltro"];
            		   if (consultarMercados) {
            			   $scope.listMercados = data.resultados["listaMercados"];
            		   }
            	   }, function (error) {
            		   fErrorTxt("Se produjo un error en la carga del combo de plantillas.", 1);
            	   }, {
            		   "clase" : $scope.filtro.clase,
            		   "mercado" : $scope.filtro.mercado
            	   });
               }; 
}]);
