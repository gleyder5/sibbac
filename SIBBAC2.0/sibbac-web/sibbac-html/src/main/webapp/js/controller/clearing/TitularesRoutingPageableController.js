'use strict';
sibbac20.controller('TitularesRoutingPageableController', ['$scope', '$compile', 'growl', 'JobsService', '$document', 'TitularesRoutingService', function ($scope, $compile, growl, JobsService, $document, TitularesRoutingService) {


        var oTable = undefined;
        var idOcultosIsin = [];
        var availableTagsIsin = [];
        var idOcultosValor = [];
        var availableTagsValor = [];
        var idOcultosNif = [];
        var availableTagsNif = [];
        var idOcultoserrorcode = [];
        var availableTagserrorcode = [];
        var idOcultosdescripcionerror = [];
        var availableTagsdescripcionerror = [];
        var idOcultosIsin = [];
        var availableTagsIsin = [];
        var idOcultosAlias = [];
        var availableTagsAlias = [];
        var lMarcar = true;
        var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth() + 1;
        var yyyy = hoy.getFullYear();
        var openError;
        var openPrecio;
        $scope.listaChk = [];
        $scope.listaErr = [];
        $scope.listaModi = [];
        var paramsCrear = [];
        var ejecutadaCon = false;
        var alPaginas = [];
        var pagina = 0;
        var paginasTotales = 0;
        var ultimaPagina = 0;
//var lAlta = false;
//var lModi = false;


        hoy = yyyy + "_" + mm + "_" + dd;
        var calcDataTableHeight = function (sY) {
            console.log("calcDataTableHeight: " + $('#tablaConsultaTitulares').height());
            console.log("sY: " + sY);
            var numeroRegistros = processInfo(oTable.api().page.info());
            var tamanno = 50 * numeroRegistros;
            if ($('#tablaConsultaTitulares').height() > 510) {
                return 480;
            } else {
                if ($('#tablaConsultaTitulares').height() > tamanno)
                {
                    return $('#tablaConsultaTitulares').height() + 30;
                } else
                {
                    if (tamanno > 510)
                    {
                        return 480;
                    } else
                    {
                        return tamanno;
                    }
                }
            }
        };
        function ajustarAltoTabla() {
            /** HAY QUE DEPURARLO
             console.log("ajustando inicio");
             var oSettings = oTable.settings();
             console.log("ajustando 1");
             var scrill = calcDataTableHeight(oTable.settings().oScroll.sY);
             console.log("cuanto: " + scrill);
             oSettings.oScroll.sY = scrill;
             console.log("tamanio antes: " + $('.dataTables_scrollBody').height());
             $('.dataTables_scrollBody').height(scrill);
             console.log("tamanio despues: " + $('.dataTables_scrollBody').height());
             console.log("ajustando 2");
             */



//      oTable.fnDraw();

//      if ($('#tablaEntregaRecepcion').height() > 480) {
//        $('.dataTables_scrollBody').height( 480 );
//      } else {
//        $('.dataTables_scrollBody').height( $('#tablaEntregaRecepcion').height() + 20 );
//      }
//            console.log("ajustando fin");
        }

        // Inicializa la página. Carga los combos y autocompletar del filtro.
        function initialize() {

            var fechas = ["fcontratacionDe", "fcontratacionA"];
            loadpicker(fechas);
            verificarFechas([["fcontratacionDe", "fcontratacionA"]]);
            cargaAlias()
            cargaIsin()
            cargarComboErrores();
            cargarComboTipoDocumento();
            cargarComboTipoNacionalidad();
            cargarComboTipoSociedad();
            cargarComboTipoPrep();
            cargarComboPaisResidencia();
            cargarComboTipoDomicilio();
            cargarComboNacionalidad();
            prepareCollapsion();

        }

        $document.ready(function () {

            initialize();
            loadDataTableClientSidePagination();
        });



        function loadDataTableFromServer() {

            oTable = $("#tablaConsultaTitulares").dataTable({
                "dom": 'B<"clear">lrtip', /*Para mostrar los controles requeridos del componente datatable*/
                "buttons": [
                    {
                        text: 'Excel',
                        action: function (e, dt, node, config) {

                            var data = {action: "getTitularesExport", service : "SIBBACServiceTitulares" ,filters : $scope.filtro};
                            console.log("data: "+data);
                            // Optional static additional parameters
                            // data.customParameter = ...;

                            if (config.fnData) {
                                config.fnData(data);
                            }

                            var iframe = $('<iframe/>', {
                                id: "RemotingIFrame"
                            }).css({
                                border: 'none',
                                width: 0,
                                height: 0
                            }).appendTo('body');

                            var contentWindow = iframe[0].contentWindow;
                            contentWindow.document.open();
                            contentWindow.document.close();
                            var form = contentWindow.document.createElement('form');
                            form.setAttribute('method', "POST");
                            form.setAttribute('action', "/sibbac20back/rest/export.xlsx");

                            var input = contentWindow.document.createElement('input');
                            input.name = 'webRequest';
                            input.value = JSON.stringify(data);
                            form.appendChild(input);
                            contentWindow.document.body.appendChild(form);
                            form.submit();

                        }
                    },
                    {
                        text: 'Pdf',
                        action: function (e, dt, node, config) {

                            var data = {action: "getTitularesExport", service : "SIBBACServiceTitulares" ,filters : $scope.filtro};
                            console.log("data: "+data);
                            // Optional static additional parameters
                            // data.customParameter = ...;

                            if (config.fnData) {
                                config.fnData(data);
                            }

                            var iframe = $('<iframe/>', {
                                id: "RemotingIFrame"
                            }).css({
                                border: 'none',
                                width: 0,
                                height: 0
                            }).appendTo('body');

                            var contentWindow = iframe[0].contentWindow;
                            contentWindow.document.open();
                            contentWindow.document.close();
                            var form = contentWindow.document.createElement('form');
                            form.setAttribute('method', "POST");
                            form.setAttribute('action', "/sibbac20back/rest/export.pdf");
                            var input = contentWindow.document.createElement('input');
                            input.name = 'webRequest';
                            input.value = JSON.stringify(data);
                            form.appendChild(input);
                            contentWindow.document.body.appendChild(form);
                            form.submit();

                        }
                    }
                ],
                "serverSide": true,
                "processing": true,
                "pageLength": datatable.pageLength,
                "ajax": $.fn.dataTable.pipeline({
                    "url": '/sibbac20back/rest/service/datatable',
                    "pages": datatable.pagesForCache, //Paginas que deja en caché por cada llamada AJAX
                    "method": "POST",
                    "dataType": "json",
                    "data": function (d) {
                        //return $.extend({}, d, {service: 'SIBBACServiceTitulares', action: 'getTitulares', filters: $scope.filtro});
                        d.service = 'SIBBACServiceTitulares';
                        d.action = 'getTitularesPageable';
                        d.filters = $scope.filtro;
                    }
//                	,
//                    "dataSrc": function (json) {
//                        return populateTitulares(json);
//                    }
                }),
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "columns": [
                    {data: null, className: 'checkbox', targets: 0, sClass: "centrar", type: "checkbox", bSortable: false, name: "active", width: 50,
                        render: function (data, type, row, meta) {
                            return '<input type="checkbox" ng-model="listaChk[' + (meta.row + 1) + ']" id="check_' + (meta.row + 1) + '" name="check_' + meta.row + '" class="' + meta.row + '" class="editor-active" bvalue="N" />';
                        }
                    }, //0
                    {data: null, sClass: "centrar nodetails-err", targets: 1, bSortable: false, width: 50,
                        render: function (data, type, row, meta) {
                            if (row.errorList != null && row.errorList.length > 0) {
                                return "<img src='/sibbac20/images/add_verde.png' ng-click='muestraErrores(" + meta.row + ")' id='imgError_" + meta.row + "' />";
                            } else {
                                return "SIN ERROR";
                            }
                        }
                    },
                    {data: null, sClass: "centrar", width: 300,
                        render: function (data, type, row, meta) {
                            return row.nbooking + "/" + row.nucnfclt + "/" + row.nucnfliq;
                        }
                    },
                    {data: 'nureford', sClass: "centrar", width: 100},
                    {data: 'ccv', sClass: "centrar", width: 100},
                    {data: 'cdtpoper', sClass: "centrar", width: 50},
                    {data: 'isin', sClass: "centrar", width: 100},
                    {data: 'nutiteje', sClass: "centrar", width: 100},
                    {data: null, sClass: "centrar nodetails-pre", targets: 8, bSortable: false, width: 50,
                        render: function (data, type, row, meta) {
                            if (row.nureford != null) {
                                return "<img src='/sibbac20/images/add_verde.png' ng-click='muestraPrecio(" + meta.row + ")' id='Precio_" + meta.row + "' />";
                            } else {
                                return "SIN REFERENCIA";
                            }
                        }
                    },
                    {data: 'mercadonacint', sClass: "centrar", width: 50},
                    {data: 'nbclient', sClass: "centrar", width: 250}, //Nombre 10
                    {data: 'nbclient1', sClass: "centrar", width: 250},
                    {data: 'nbclient2', sClass: "centrar", width: 250},
                    {data: 'tipdoc', sClass: "centrar", width: 75},
                    {data: 'nudnicif', sClass: "centrar", width: 100},
                    {data: 'tipopers', sClass: "centrar", width: 75},
                    {data: 'paisres', sClass: "centrar", width: 75},
                    {data: 'tipnactit', sClass: "centrar", width: 75},
                    {data: 'tpdomici', sClass: "centrar", width: 300,
                        render: function (data, type, row, meta) {
                            return ((row.tpdomici !== null) ? row.tpdomici : "") + ((row.nbdomici !== null) ? row.nbdomici : "") + (row.nudomici !== null) ? row.nudomici : "";
                        }
                    },
                    {data: 'errorList', sClass: "centrar", visible: false}, // 19
                    {data: 'poblacion', sClass: "centrar", width: 200}, //20
                    {data: 'provincia', sClass: "centrar", width: 200},
                    {data: 'codpostal', sClass: "centrar", width: 75},
                    {data: 'tiptitu', sClass: "centrar", width: 75},
                    {data: 'particip', sClass: "align-right", "sType": "numeric", width: 75}, // 24
                    {data: 'tipnactit', sClass: "centrar", visible: false}, //25
                    {data: 'nureford', sClass: "centrar", visible: false},
                    {data: 'tpdomici', sClass: "centrar", visible: false},
                    {data: 'nbdomici', sClass: "centrar", visible: false},
                    {data: 'nudomici', sClass: "centrar", visible: false},
                    {data: 'nbooking', sClass: "centrar", visible: false}, // 30
                    {data: 'nucnfclt', sClass: "centrar", visible: false},
                    {data: 'nucnfliq', sClass: "centrar", visible: false},
                    {data: 'id', sClass: "centrar", visible: false},
                    {data: 'nuorden', sClass: "centrar", visible: false},
                    {data: 'nusecuen', sClass: "centrar", visible: false},
                    {data: 'afiError', sClass: "centrar", visible: false}, // 35
                    {data: 'nacionalidad', sClass: "centrar", visible: false}, //36
                    {data: 'fechacontratacion', sClass: "centrar", width: 75},
                    {data: 'alias', sClass: "centrar", width: 75},
                    {data: 'nombrealias', sClass: "centrar", width: 200}
                ],
                "createdRow": function (nRow, aData, iDataIndex) {
                    $compile(nRow)($scope);
                },
                "order": [],
                "scrollY": "480px",
                "scrollX": "100%",
                "scrollCollapse": true,
//		"rowHeight" : "44px",

                drawCallback: function () {
                    console.log("drawCallback");
                    ajustarAltoTabla();
                    processInfo(this.api().page.info());
                    if (alPaginas.length !== paginasTotales) {
                        alPaginas = new Array(paginasTotales);
                        for (var i = 0; i < alPaginas.length; i++) {
                            alPaginas[i] = true;
                        }
                    }

                    if (ultimaPagina !== pagina) {

                        var valor = alPaginas[pagina]

                        if (valor) {
                            document.getElementById("MarcarPag").value = "Marcar Página";
                        } else {
                            document.getElementById("MarcarPag").value = "Desmarcar Página";
                        }

                    }
                    ultimaPagina = pagina;

                    var numFilas = this.api().rows().nodes().length;
                    openError = new Array(numFilas, false);
                    openPrecio = new Array(numFilas, false);
                    $.unblockUI();
                }
            });



            angular.element("#fcontratacionDe, #fcontratacionA").datepicker({
                dateFormat: 'DD/MM/YYYY',
                defaultDate: new Date(),
            }); // datepick

            // Muestra o no el combo del codigo de error.
            $('#descripcionError').change(function (event) {

                if ($("#descripcionError").val() == 0 || $("#descripcionError").val() == 1) {
                    $('#diverrorcode').css('display', 'none');
                    $("#errorcode").val('');
                    $("#errorcode").attr('disabled', true);
                } else {
                    $('#diverrorcode').css('display', 'inline');
                    $("#errorcode").attr('disabled', false);
                }

            });

            $('#selecFichero').change(function (event) {
                event.preventDefault();
                var files = event.target.files;
                cargarFichero(files);
            });
            // Botón X que cierra formulario modificación.
            $('#cerrarTitulares').click(function (event) {
                event.preventDefault();
                cerrar();
            });
            // Botón X que cierra formulario crear.
            $('#cerrarTitularesCrear').click(function (event) {
                event.preventDefault();
                cerrar();
            });
            // Botón X que cierra formulario de importación de ficheros.
            $('#cerrarimportacion').click(function (event) {
                event.preventDefault();
                $('#cargarFichero').css('display', 'none');
                lookBotones(true);
            });
            // Cuando en creación de titulares cambia el valor nº de orden.
            $("#nuorden_SE").blur(function (event) {
                event.preventDefault();
                fSeleccionOrd();
            });
            // Cuando en creación de titulares cambia el valor booking.
            $("#nbooking_SE").blur(function (event) {
                event.preventDefault();
                fSeleccionOrd();
            });
            // Cuando en creación de titulares cambia el valor nucnfclt.
            $("#nucnfclt_SE").blur(function (event) {
                event.preventDefault();
                fSeleccionOrd();
            });
            // Cuando en creación de titulares cambia el valor nucnfliq.
            $("#nucnfliq_SE").blur(function (event) {
                event.preventDefault();
                fSeleccionOrd();
            });
            $('#btnSelecFichero').click(function (event) {
                event.preventDefault();
                $('#cargarFichero').css('display', 'none');
                enviarFichero();
            });
        }
        /**
         * Se crea la tabla de forma normal
         * @returns {undefined}
         */
        function loadDataTableClientSidePagination() {
            oTable = $("#tablaConsultaTitulares").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": ["/sibbac20/js/swf/copy_csv_xls_pdf.swf"],
                    "aButtons": ["copy",
                        {"sExtends": "csv",
                            "sFileName": "Listado_de_control_titulares_" + hoy + ".csv",
                            "mColumns": [2, 3, 4, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25]
                        },
                        {
                            "sExtends": "xls",
                            "sFileName": "Listado_de_control_titulares_" + hoy + ".csv",
//	                      	 "sCharSet" : "utf8",
                            "mColumns": [2, 3, 4, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25]
                        },
                        {
                            "sExtends": "pdf",
                            "sPdfOrientation": "landscape",
                            "sTitle": " ",
                            "sPdfSize": "A4",
                            "sPdfMessage": "Listado de control_titulares_Routing",
                            "sFileName": "Listado_de_control_titulares_" + hoy + ".pdf",
                            "mColumns": [2, 3, 4, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25]
                        },
                        "print"
                    ]
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "aoColumns": [
                    {className: 'checkbox', targets: 0, sClass: "centrar", type: "checkbox", bSortable: false, name: "active", width: 50}, //0
                    {className: 'details-err', sClass: "centrar", targets: 1, bSortable: false, width: 50},
                    {sClass: "centrar", width: 300},
                    {sClass: "centrar", width: 100},
                    {sClass: "centrar", width: 100},
                    {sClass: "centrar", width: 50},
                    {sClass: "centrar", width: 100},
                    {sClass: "centrar", width: 100},
                    {className: 'details-pre', sClass: "centrar", targets: 8, bSortable: false, width: 50},
                    {sClass: "centrar", width: 50},
                    {sClass: "centrar", width: 250}, //Nombre 10
                    {sClass: "centrar", width: 250},
                    {sClass: "centrar", width: 250},
                    {sClass: "centrar", width: 75},
                    {sClass: "centrar", width: 100},
                    {sClass: "centrar", width: 75},
                    {sClass: "centrar", width: 75},
                    {sClass: "centrar", width: 75},
                    {sClass: "centrar", width: 300},
                    {sClass: "centrar", visible: false}, // 19
                    {sClass: "centrar", width: 200}, //20
                    {sClass: "centrar", width: 200},
                    {sClass: "centrar", width: 75},
                    {sClass: "centrar", width: 75},
                    {sClass: "align-right", "sType": "numeric", width: 75}, // 24
                    {sClass: "centrar", visible: false}, //25
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false}, // 30
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false},
                    {sClass: "centrar", visible: false}, // 35
                    {sClass: "centrar", visible: false}, //36
                    {sClass: "centrar", width: 75},
                    {sClass: "centrar", width: 75},
                    {sClass: "centrar", width: 200}
                ],
                "fnCreatedRow": function (nRow, aData, iDataIndex) {

                    $compile(nRow)($scope);
                },
                "order": [],
                "scrollY": "480px",
                "scrollX": "100%",
                "scrollCollapse": true,
//		"rowHeight" : "44px",

                drawCallback: function () {
                    ajustarAltoTabla();
                    processInfo(this.api().page.info());
                    if (alPaginas.length !== paginasTotales) {
                        alPaginas = new Array(paginasTotales);
                        for (var i = 0; i < alPaginas.length; i++) {
                            alPaginas[i] = true;
                        }
                    }

                    if (ultimaPagina !== pagina) {

                        var valor = alPaginas[pagina];

                        if (valor) {
                            document.getElementById("MarcarPag").value = "Marcar Página";
                        } else {
                            document.getElementById("MarcarPag").value = "Desmarcar Página";
                        }

                    }
                    ultimaPagina = pagina;
                }
            });

            angular.element("#fcontratacionDe, #fcontratacionA").datepicker({
                dateFormat: 'dd/mm/yy',
                defaultDate: new Date()
            }); // datepick

            // Muestra o no el combo del codigo de error.
            $('#descripcionError').change(function (event) {

                if ($("#descripcionError").val() == 0 || $("#descripcionError").val() == 1) {
                    $('#diverrorcode').css('display', 'none');
                    $("#errorcode").val('');
                    $("#errorcode").attr('disabled', true);
                } else {
                    $('#diverrorcode').css('display', 'inline');
                    $("#errorcode").attr('disabled', false);
                }

            });


            $('#selecFichero').change(function (event) {
                event.preventDefault();
                var files = event.target.files;
                cargarFichero(files);

            });


            // Botón X que cierra formulario modificación.
            $('#cerrarTitulares').click(function (event) {
                event.preventDefault();
                cerrar();
            });

            // Botón X que cierra formulario crear.
            $('#cerrarTitularesCrear').click(function (event) {
                event.preventDefault();
                cerrar();
            });

            // Botón X que cierra formulario de importación de ficheros.
            $('#cerrarimportacion').click(function (event) {
                event.preventDefault();
                $('#cargarFichero').css('display', 'none');
                lookBotones(true);
            });

            // Cuando en creación de titulares cambia el valor nº de orden.
            $("#nuorden_SE").blur(function (event) {
                event.preventDefault();
                fSeleccionOrd();
            });

            // Cuando en creación de titulares cambia el valor booking.
            $("#nbooking_SE").blur(function (event) {
                event.preventDefault();
                fSeleccionOrd();
            });

            // Cuando en creación de titulares cambia el valor nucnfclt.
            $("#nucnfclt_SE").blur(function (event) {
                event.preventDefault();
                fSeleccionOrd();
            });

            // Cuando en creación de titulares cambia el valor nucnfliq.
            $("#nucnfliq_SE").blur(function (event) {
                event.preventDefault();
                fSeleccionOrd();
            });

            $('#btnSelecFichero').click(function (event) {
                event.preventDefault();
                $('#cargarFichero').css('display', 'none');
                enviarFichero();
            });
        }
//Botón de ejecutar la consulta.
        $scope.Consultar = function () {
            var errorTxt = validadTitulares();
            if (errorTxt !== null) {
                fErrorTxt(errorTxt, 2);
                return false;
            }

            ejecutadaCon = true;
            lookBotones(true);
            cargarDatosConsultaTitulares();
            return false;
        };

// Botón que limpia las condiciones del filtro.
        $scope.LimpiarFiltros = function () {
            $('input[type=text]').val('');
            document.getElementById("mercado").value = '';
            document.getElementById("sentido").value = '';
            document.getElementById("errorcode").value = '';
            document.getElementById("descripcionError").value = '';
            lMarcar = true;
            document.getElementById("MarcarTod").value = "Marcar Todos";
        };

//Botón de chequear errores.
        $scope.CheckErrors = function () {
            CheckErrors();
        };
        $scope.ImportarFichero = function () {
            lookBotones(false);
            $('#cargarFichero').css('display', 'block');
        };

// Boton de marcar todos
        $scope.selececionarCheck = function () {
            if (lMarcar) {
                lMarcar = false;
                document.getElementById("MarcarTod").value = "Desmarcar Todos";
                toggleCheckBoxes(true);
            } else {
                lMarcar = true;
                document.getElementById("MarcarTod").value = "Marcar Todos";
                toggleCheckBoxes(false);
            }
        };
//Boton de marcar página
        $scope.selececionarCheckPag = function () {

            var valor = alPaginas[pagina];

            toggleCheckBoxesPag(valor);
            if (valor) {
                document.getElementById("MarcarPag").value = "Desmarcar Página";
                alPaginas[pagina] = false;
            } else {
                document.getElementById("MarcarPag").value = "Marcar Página";
                alPaginas[pagina] = true;
            }
        };
//Botón de crear titulares.
        $scope.CrearTitulares = function () {
            $("#cargarTitutalaresSelec").css('display', 'block');
            $("#cargarTitutalaresDatos").css('display', 'none');
            $("#formulariotitulares").attr('class', 'modal modalalto9');
            document.getElementById("guardarTitu").value = "Guardar y salir";
            $("#guardarCont").css('display', 'block');
            crearTitulares(oTable);
        };

//Botón de modificar titulares.
        $scope.ModificarTitulares = function () {
            $("#cargarTitutalaresSelec").css('display', 'none');
            $("#cargarTitutalaresDatos").css('display', 'block');
            $("#guardarCont").css('display', 'none');
            $("#formulariotitulares").attr('class', 'modal modal-alto-375');
            document.getElementById("guardarTitu").value = "Guardar";
            modificarTitulares(oTable);
        };


// Activa la sublinea de los errores.
        $scope.muestraErrores = function (fila) {
            var elementoprop = angular.element('img[id=imgError_' + fila + ']');
            var elemento = angular.element('img[id=Precio_' + fila + ']');
            if (oTable.fnIsOpen(fila)) {
                if (openError[fila]) {
                    $(elementoprop[0]).attr('src', '/sibbac20/images/add_verde.png');
                    $(elemento[0]).attr('src', '/sibbac20/images/add_verde.png');
                    $(this).removeClass('open err');
                    oTable.fnClose(fila);
                    openError[fila] = false;
                }
            } else {
                var aData = oTable.fnGetData(fila);
                if (aData[0] !== "SIN ERROR") {
                    $(elementoprop[0]).attr('src', '/sibbac20/images/supr_verde.png');
                    $(elemento[0]).attr('src', '/sibbac20/images/forbidden.png');
                    openError[fila] = true;
                    $(this).addClass('open err');
                    oTable.fnOpen(fila, TablaErrores(oTable, fila), 'details-err');
                    ajustarAltoTabla();
                    document.getElementById('div_err_' + fila).scrollIntoView();
                }
            }
        };

//Activa la sublinea de los Precios.
        $scope.muestraPrecio = function (fila) {
            var elementoprop = angular.element('img[id=Precio_' + fila + ']');
            var elemento = angular.element('img[id=imgError_' + fila + ']');
            if (oTable.fnIsOpen(fila)) {
                if (openPrecio[fila]) {
                    $(elementoprop[0]).attr('src', '/sibbac20/images/add_verde.png');
                    $(elemento[0]).attr('src', '/sibbac20/images/add_verde.png');
                    $(this).removeClass('open pre');
                    oTable.fnClose(fila);
                    openPrecio[fila] = false;
                }
            } else {
                var aData = oTable.fnGetData(fila);
                if (aData[0] !== "SIN REFERENCIA") {
                    $(elementoprop[0]).attr('src', '/sibbac20/images/supr_verde.png');
                    $(elemento[0]).attr('src', '/sibbac20/images/forbidden.png');
                    openPrecio[fila] = true;
                    $(this).addClass('open pre');
                    oTable.fnOpen(fila, TablaPrecio(oTable, fila), 'details-pre');
                    ajustarAltoTabla();
                    document.getElementById('div_pre_' + fila).scrollIntoView();
                }
            }
        };

        $scope.Guardar = function () {
            if ($('#guardarTitu').attr('data') == 'modificar') {
                mandarTitularesModificados();
            } else {
                mandarTitularesCreados();
            }
        };


        $scope.SiguienteTitular = function () {
            mandarTitularesCrearContinuar();
        };

//Devuelve información de la tabla-.
        function  processInfo(info) {

            pagina = info.page;
            paginasTotales = info.pages;
            var regInicio = info.start;
            var regFin = info.end;
            return regFin - regInicio;
        }

        function  infoTabla(info) {
            var regTotales = info.recordsTotal;
            var paginas = info.pages;
            var regPagina = info.length;
            var regInicio = info.start;
            var regFin = info.end;
            return regFin - regInicio;
        }

// marca/desmarca todos los registros
        function toggleCheckBoxes(b) {

            for (var i = 0; i < $scope.listaChk.length; i++) {
                $scope.listaChk[i] = b;
            }
        }

//marca/desmarca los registros de la pagina
        function toggleCheckBoxesPag(b) {
            var inputs = angular.element('input[type=checkbox]');
            var cb = null;
            for (var i = 0; i < inputs.length; i++) {
                cb = inputs[i];
                var clase = $(cb).attr('class');
                var n = clase.indexOf(" ");
                var fila = clase.substr(0, n);
                $scope.listaChk[fila] = b;
            }

        }


// Carga el combo de los códigos de error.
        function cargarComboErrores() {

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('getErrorsList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function (json) {
                if (json.resultados == null) {
                    fErrorTxt(json.error, 1);
                } else {

                    var datos = json.resultados.errorList;
                    for (var k in datos) {

                        item = datos[k];
                        $('#errorcode').append("<option value='" + item.codigo + "'>(" + item.codigo + ") " + item.descripcion + "</option>"

                                );
                    }
                }
            });
        }

//Carga el combo de los tipos de documentos.
        function cargarComboTipoDocumento() {

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('getTpidentiList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function (json) {
                if (json.resultados === null) {
                    fErrorTxt(json.error, 1);
                } else {

                    var datos = json.resultados.resultados_tpidenti;
                    for (var k in datos) {

                        item = datos[k];
                        $('#tpdocu_TI').append("<option value='" + item.codigo + "'>(" + item.codigo + ") " + item.descripcion + "</option>"

                                );
                    }
                }
            });
        }

//Carga el combo de los tipos de nacionalidad
        function cargarComboTipoNacionalidad() {

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('getTpnactitList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function (json) {
                if (json.resultados == null) {
                    fErrorTxt(json.error, 1);
                } else {

                    var datos = json.resultados.resultados_tpnactit;
                    for (var k in datos) {

                        item = datos[k];
                        $('#tpnaci_TI').append("<option value='" + item.codigo + "'>(" + item.codigo + ") " + item.descripcion + "</option>"

                                );
                    }
                }
            });
        }

//Carga el combo de los tipos de sociedad.
        function cargarComboTipoSociedad() {

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('getTpsociedList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function (json) {
                if (json.resultados === null) {
                    fErrorTxt(json.error, 1);
                } else {

                    var datos = json.resultados.resultados_tpsocied;
                    for (var k in datos) {

                        item = datos[k];
                        $('#tpsoci_TI').append("<option value='" + item.codigo + "'>(" + item.codigo + ") " + item.descripcion + "</option>"

                                );
                    }
                }
            });
        }

//Carga el combo de los tipos de prep
        function cargarComboTipoPrep() {

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('getTptiprepList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function (json) {
                if (json.resultados == null) {
                    fErrorTxt(json.error, 1);
                } else {

                    var datos = json.resultados.resultados_tptiprep;
                    for (var k in datos) {

                        item = datos[k];
                        $('#tptipr_TI').append("<option value='" + item.codigo + "'>(" + item.codigo + ") " + item.descripcion + "</option>"

                                );
                    }
                }
            });
        }


//Carga el combo de los paises de residencia.
        function cargarComboPaisResidencia() {

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('getPaisResList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function (json) {
                if (json.resultados == null) {
                    fErrorTxt(json.error, 1);
                } else {

                    var datos = json.resultados.resultados_paisres;
                    for (var k in datos) {

                        item = datos[k];
                        var id = item.id;
                        $('#paires_TI').append("<option value='" + id['cdhppais'] + "'>" + item.cdiso2po + " - " + item.nbhppais + "(" + id['cdhppais'] + ") </option>"

                                );
                    }
                }
            });
        }

//Carga el combo de los tipos de domicilio (tipo de via).
        function cargarComboTipoDomicilio() {

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('getTpDomiciList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function (json) {
                if (json.resultados == null) {
                    fErrorTxt(json.error, 1);
                } else {

                    var datos = json.resultados.resultados_tpdomici;
                    for (var k in datos) {

                        item = datos[k];
                        $('#tpdomi_TI').append("<option value='" + item.cdviapub + "'>(" + item.cdviapub + ") " + item.nbviapub + "</option>"

                                );
                    }
                }
            });
        }

        //Carga el combo de las Nacionalidades.
        function cargarComboNacionalidad() {

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('getNacionalidadList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function (json) {
                if (json.resultados == null) {
                    fErrorTxt(json.error, 1);
                } else {

                    var datos = json.resultados.resultados_nacionalidad;
                    for (var k in datos) {

                        item = datos[k];
                        $('#cdnaci_TI').append("<option value='" + item.cdispais + "'>" + item.cdiso2po + " - " + item.nbispais + "(" + item.cdispais + ") </option>"

                                );
                    }
                }
            });
        }

// Funcion en la que captura el filtro y muestra todos titulares que cumplan dicha condición
        function cargarDatosConsultaTitulares() {

            console.log("cargarDatosConsultaTitulares");

            document.getElementById("MarcarTod").value = "Marcar Todos";
            var fcontratacionDe = transformaFechaInv($("#fcontratacionDe").val());
            var fcontratacionA = transformaFechaInv($("#fcontratacionA").val());
//	var textAlias =  $("#textAlias").val();
            var mercado = $("#mercado").val();
            var sentido = $("#sentido").val();
            var valorSeleccionadoAlias = obtenerIdsSeleccionados('#selectedAlias');
            var valorSeleccionadoIsin = obtenerIdsSeleccionados('#selectedIsin');
            var valorSeleccionadoNif = obtenerIdsSeleccionados('#selectedNif');
            var nombreTitular = obtenerIdsSeleccionados('#selectedNombreTitular');
            var apellido1 = obtenerIdsSeleccionados('#selectedApellido1');
            var apellido2 = obtenerIdsSeleccionados('#selectedApellido2');
            var valorSeleccionadoerrorcode;
            var listaerrorcode = document.getElementById("errorcode").value;
            var posicionerrorcode = availableTagsNif.indexOf(listaerrorcode);
            if (posicionerrorcode == "-1") {
                valorSeleccionadoerrorcode = document.getElementById("errorcode").value;
            } else {
                valorSeleccionadoerrorcode = idOcultosNif[posicionerrorcode];
            }

            var valorSeleccionadodescripcionerror;
            var listadescripcionerror = document.getElementById("descripcionError").value;
            var posiciondescripcionerror = availableTagsdescripcionerror.indexOf(listadescripcionerror);
            if (posiciondescripcionerror == "-1") {
                valorSeleccionadodescripcionerror = document.getElementById("descripcionError").value;
            } else {
                valorSeleccionadodescripcionerror = idOcultosNif[posiciondescripcionerror];
            }

            var nureford = $('#nureford').val();
            var nbooking = $('#nbooking').val();
            $scope.filtro = {
                "fechaDesde": fcontratacionDe,
                "fechaHasta": fcontratacionA,
                "alias": valorSeleccionadoAlias,
                "mercadoNacInt": mercado,
                "sentido": sentido,
                "isin": valorSeleccionadoIsin,
                "nudnicif": valorSeleccionadoNif,
                "nbclient": nombreTitular,
                "nbclient1": apellido1,
                "nbclient2": apellido2,
                "errorcode": valorSeleccionadoerrorcode,
                "hasErrors": valorSeleccionadodescripcionerror,
                "nureford": nureford,
                "nbooking": nbooking
            };
            inicializarLoading();
            TitularesRoutingService.getTitularesCount(onSuccessCountRequest, onErrorCountRequest, $scope.filtro);

        }
        function onSuccessCountRequest(data, status, header, config) {
            console.log("Número de registros: " + data.resultados.registros);
            if (data.error !== undefined && data.error !== null && data.error !== "") {
                growl.addErrorMessage(data.error);
            } else {
                collapseSearchForm();
                if (oTable !== undefined && oTable !== null) {
                    oTable.fnDestroy();
                }
                if (data.resultados.registros > (datatable.pageLength * datatable.pagesForCache)) {
                    growl.addWarnMessage("¡Atención!. El número de registros en la base de datos, es mayor que el permitido, por tanto se deshabilitará el campo de búsqueda sobre la tabla.");
                    loadDataTableFromServer();
                } else {
                    loadDataTableClientSidePagination();
                    var data = new DataBean();
                    data.setService('SIBBACServiceTitulares');
                    data.setAction('getTitulares');
                    data.setFilters($scope.filtro);
                    var request = requestSIBBAC(data);
                    request.success(function (json) {
                        populateTitulares(json);

                    });
                }

            }

        }
        function onErrorCountRequest(data) {
            growl.addErrorMessage("Ocurrió un error de comunicación con el servidor, por favor inténtelo más tarde.");
            $.unblockUI();
        }


        function populateTitulares(json) {
            console.log(json);
            console.log(json.data);
            if (json.data !== null && json.data !== undefined) {
                oTable.fnClear();
                var item = null;
                var datos = json.data;
                var newData = [];
                oTable.clear();
                var contador = 0;
                for (var k in datos) {
                    item = datos[k];
                    contador++;
                    var control_err = 0
                    for (var i in item.errorList) {
                        control_err++;
                        if (control_err > 0)
                            break;
                    }

                    var enlace_err = "";
                    var valor_err = "";
                    var txtPrecio = "";
                    if (item.errorList !== null && control_err > 0) {
                        valor_err = "<img src='/sibbac20/images/add_verde.png' ng-click='muestraErrores(" + k + ")' id='imgError_" + k + "' >";
                    } else {
                        valor_err = "SIN ERROR";
                    }
                    var idName = "check_" + contador;
                    var txtMarcar = '<input ng-model="listaChk[' + k + ']" type="checkbox" class=' + (contador - 1) + '  id="' + idName + '" name="' + idName + '"bvalue="N" class="editor-active">';
                    if (item.nureford !== null) {
                        txtPrecio = "<img src='/sibbac20/images/add_verde.png' ng-click='muestraPrecio(" + k + ")' id='Precio_" + k + "'>";
                    } else {
                        txtPrecio = "SIN REFERENCIA";
                    }


                    var tipodomi = (item.tpdomici !== null) ? item.tpdomici : "";
                    var domicilio = (item.nbdomici !== null) ? item.nbdomici : "";
                    var numedomi = (item.nudomici !== null) ? item.nudomici : "";
                    var fechacontratacion = item.fechacontratacion;
                    var fechaContrFormato = "";
                    if (fechacontratacion !== null && fechacontratacion !== undefined) {
                        var fechaContrFormato = fechacontratacion.substr(8, 2) + "/" + fechacontratacion.substr(5, 2) + "/" + fechacontratacion.substr(0, 4);
                    }

                    $scope.listaChk[k] = false;
                    var rowIndex = oTable.addData([
                        txtMarcar,
                        valor_err,
                        item.nbooking + "/" + item.nucnfclt + "/" + item.nucnfliq,
                        item.nureford,
                        item.ccv,
                        item.cdtpoper,
                        item.isin,
                        item.nutiteje,
                        txtPrecio,
                        item.mercadonacint,
                        item.nbclient,
                        item.nbclient1,
                        item.nbclient2,
                        item.tipdoc,
                        item.nudnicif,
                        item.tipopers,
                        item.paisres,
                        item.tipnactit,
                        tipodomi.trim() + " " + domicilio.trim() + " " + numedomi,
                        item.errorList,
                        item.poblacion,
                        item.provincia,
                        item.codpostal,
                        item.tiptitu,
                        item.particip,
                        item.tipnactit,
                        item.nureford,
                        tipodomi,
                        domicilio,
                        numedomi,
                        item.nbooking,
                        item.nucnfclt,
                        item.nucnfliq,
                        item.id,
                        item.nuorden,
                        item.nusecuen,
                        item.afiError,
                        item.nacionalidad,
                        fechaContrFormato,
                        item.alias,
                        item.nombrealias, ],
                            false);
                    var nTr = oTable.settings().aoData[rowIndex[0]].nTr;
                    if (item.errorList !== null) {
                        $('td', nTr)[1].setAttribute('style', 'cursor: pointer; color: #ff0000;');
                    }
                    $('td', nTr)[8].setAttribute('style', 'cursor: pointer; color: #ff0000;');
                }
                oTable.fnDraw();
                lookBotones(true);
                openError = new Array(contador, false);
                console.log("openPrecio inicializado")
                openPrecio = new Array(contador, false);

                console.log(json.data);

            } else {

                if (ejecutadaCon) {
                    var tipo = 2;
                    var errorTxt = json.error;
                    if (errorTxt == undefined) {
                        errorTxt = "Error indeterminado. Consulte con el servicio técnico.";
                        tipo = 1;
                    }
                    fErrorTxt(json.error, tipo);

                }


            }
            $.unblockUI();
        }
//carga la tablita de los errores de un registro.
        function TablaErrores(Table, nTr) {

            var aData = Table.fnGetData(nTr);
            var aObjError = aData.errorList;
            var resultado =
                    "<div border-width:0 !important;background-color:#dfdfdf; style='float:left;;margin-left: 80px;' id='div_err_" + nTr + "'>" +
                    "<table id='tablaERR" + nTr + "' style=margin-top:5px;'>" +
                    "<tr>" +
                    "<th class='taleft' style='background-color: #000 !important;'>C&oacute;digo</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Descripci&oacute;n</th>" +
                    "</tr>";

            for (var i in aObjError) {
                var aError = aObjError[i];
                resultado += "<tr>";
                for (var j in aError) {
                    if (j == 'codigo')
                        resultado += "<td  width=75>";
                    else
                        resultado += "<td  width=500>";

                    resultado += aError[j] + "</td>";
                }
                resultado += "</tr>";
            }
            resultado += "</table></div>";

            return resultado;
        }


//carga la tablita de los precios de un registro (cabecera).
        function TablaPrecio(Table, nTr) {

            var aData = Table.fnGetData(nTr);
            var resultado =
                    "<div border-width:0 !important;background-color:#dfdfdf; style='float:left;margin-left: 1136px;' id='div_pre_" + nTr + "'>" +
                    "<table id='tablaPRE" + nTr + "' style=margin-top:5px;'>" +
                    "<tr>" +
                    "<th class='taleft' style='background-color: #000 !important;'>T&iacute;tulos</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Precio</th>" +
                    "</tr>";
            desplegarPrecios(aData, nTr);
            return resultado;
        }

//carga la tablita de los precios de un registro (registros).
        function desplegarPrecios(aData, nTr) {

            var nureford = aData['nureford'];
            var filtro = "{\"nureford\" : \"" + nureford.trim() + "\" } ";
            var data = new DataBean();
            var rowPrecios = ""
            var tablaPre = "#tablaPRE" + nTr;
            data.setService('SIBBACServiceTitulares');
            data.setAction('getEjecucionesFromNureford');
            data.setFilters(filtro);
            var request = requestSIBBAC(data);
            request.success(function (json) {
                console.log(json);
                if (json.resultados != null && json.resultados !== undefined) {

                    var item = null;
                    var datos = json.resultados.resultados_ejecuciones;
                    var arPrecios = [];
                    for (var k in datos) {
                        var registro = datos[k];
                        var titulos;
                        var precio;
                        for (var j in registro) {
                            item = registro[j];
                            if (j == "nutiteje") {
                                titulos = item;
                            } else if (j == "imcbmerc") {
                                precio = item;
                            }
                        }
                        arPrecios.push({titulo: titulos, precio: precio})

                    }

                    arPrecios.sort(function (a, b) {
                        if (a.precio > b.precio) {
                            return 1;
                        }
                        if (a.precio < b.precio) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    });
                    for (var i = 0; i < arPrecios.length; i++) {
                        var rwPrecio = arPrecios[i];
                        rowPrecios += "<tr><td>" + rwPrecio.titulo + "</td><td>" + rwPrecio.precio + "</td></tr>";
                    }

                    $(tablaPre).append(rowPrecios);
                }

            });
            $(tablaPre).append("</table><div>");
        }

        /** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
        function valueExistInList(lista, valor) {
            var inList = false;
            $(lista + ' option').each(function (index) {

                if (this.value == valor) {
                    inList = true;
                    return inList;
                }
            });
            return inList;
        }

        function cargaIsin() {
            var availableTagsIsin = [];
            $(function () {
                var data = new DataBean();
                data.setService('SIBBACServiceConciliacion');
                data.setAction('getIsin');
                var request = requestSIBBAC(data);
                request.success(function (json) {
                    var datos = undefined;
                    if (json === undefined || json.resultados === undefined ||
                            json.resultados.result_isin === undefined) {
                        datos = {};
                    } else {
                        datos = json.resultados.result_isin;
                    }
                    var obj;
                    $(datos).each(function (key, val) {
                        obj = {label: val.codigo + " - " + val.descripcion,
                            value: val.codigo};
                        availableTagsIsin.push(obj);
                    });
                    $("#btnDelIsin").click(function (event) {
                        event.preventDefault();
                        $("#selectedIsin").find("option:selected").remove().end();
                    });
                    pupulateIsinAutocomplete("#textIsin", "#idTxIsin", availableTagsIsin, "#selectedIsin");
                });
            });
            // fin
        }

        function pupulateIsinAutocomplete(input, idContainer, availableTagsIsin, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsIsin,
                focus: function (event, ui) {
                    // $( this ).val( ui.item.label );
                    return false;
                },
                select: function (event, ui) {
                    // $(idContainer).val(ui.item.value);
                    if (!valueExistInList(lista, ui.item.value)) {
                        $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                        $(input).val("");
                        $(lista + "> option").attr('selected', 'selected');
                    }
                    return false;
                }
            });
        }


        function cargaAlias() {
            var availableTagsAlias = [];
            $(function () {
                var data = new DataBean();
                data.setService('SIBBACServiceTmct0ali');
                data.setAction('getAlias');
                var request = requestSIBBAC(data);
                request.success(function (json) {
                    var datos = undefined;
                    if (json === undefined || json.resultados === undefined ||
                            json.resultados.result_alias === undefined) {
                        datos = {};
                    } else {
                        datos = json.resultados.result_alias;
                    }
                    var obj;
                    $(datos).each(function (key, val) {
                        obj = {label: val.alias + " - " + val.descripcion,
                            value: val.alias};
                        availableTagsAlias.push(obj);
                    });
                    $("#btnDelAlias").click(function (event) {
                        event.preventDefault();
                        $("#selectedAlias").find("option:selected").remove().end();
                    });
                    pupulateAliasAutocomplete("#textAlias", "#idTxAlias", availableTagsAlias, "#selectedAlias");
                });
            });
            // fin
        }

        function pupulateAliasAutocomplete(input, idContainer, availableTagsAlias, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsAlias,
                focus: function (event, ui) {
                    // $( this ).val( ui.item.label );
                    return false;
                },
                select: function (event, ui) {
                    // $(idContainer).val(ui.item.value);
                    if (!valueExistInList(lista, ui.item.value)) {
                        $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                        $(input).val("");
                        $(lista + "> option").attr('selected', 'selected');
                    }
                    return false;
                }
            });
        }

        $scope.btnAddNif = function () {

            var valor = $('#nif').val();
            $('#selectedNif').append("<option value=\"" + valor + "\" >" + valor + "</option>");
            $('#nif').val("");
        }

        $scope.btnDelNif = function () {
            $("#selectedNif").find("option:selected").remove().end();
        }


        $scope.btnAddNombreTitular = function () {

            var valor = $('#nombreTitular').val();
            $('#selectedNombreTitular').append("<option value=\"" + valor + "\" >" + valor + "</option>");
            $('#nombreTitular').val("");
        }

        $scope.btnDelNombreTitular = function () {
            $("#selectedNombreTitular").find("option:selected").remove().end();
        }

        $scope.btnAddApellido1 = function () {

            var valor = $('#apellido1').val();
            $('#selectedApellido1').append("<option value=\"" + valor + "\" >" + valor + "</option>");
            $('#apellido1').val("");
        }

        $scope.btnDelApellido1 = function () {
            $("#selectedApellido1").find("option:selected").remove().end();
        }

        $scope.btnAddApellido2 = function () {

            var valor = $('#apellido2').val();
            $('#selectedApellido2').append("<option value=\"" + valor + "\" >" + valor + "</option>");
            $('#apellido2').val("");
        }

        $scope.btnDelApellido2 = function () {
            $("#selectedApellido2").find("option:selected").remove().end();
        }

// función que realiza los cambios en la seleeción del alta.
        function fSeleccionOrd() {

            var nuorden = $("#nuorden_SE").val();
            var nbooking = $("#nbooking_SE").val();
            var nucnfclt = $("#nucnfclt_SE").val();
            var nucnfliq = $("#nucnfliq_SE").val();
            var filtro = "{";
            filtro += "\"nuorden\" : \"" + nuorden.trim() + "\",";
            filtro += "\"nbooking\" : \"" + nbooking.trim() + "\",";
            filtro += "\"nucnfclt\" : \"" + nucnfclt.trim() + "\",";
            filtro += "\"nucnfliq\" : \"" + nucnfliq.trim() + "\"";
            filtro += "}";
            if (nuorden.length > 0 || nbooking.length > 0 || nucnfclt.length > 0) {


                var data = new DataBean();
                data.setService('SIBBACServiceTitulares');
                data.setAction('getOrdData');
                data.setFilters(filtro);
                var request = requestSIBBAC(data);
                request.success(function (json) {

                    console.log(json);
                    console.log(json.resultados);
                    if (json.resultados != null && json.resultados !== undefined) {

                        nuorden = json.resultados.nuorden;
                        nbooking = json.resultados.nbooking;
                        nucnfclt = json.resultados.nucnfclt;
                        nucnfliq = json.resultados.nucnfliq;
                        var nuordenCorrect = json.resultados.nuordenCorrect;
                        var nbookingCorrect = json.resultados.nbookingCorrect;
                        var nucnfcltCorrect = json.resultados.nucnfcltCorrect;
                        var nucnfliqCorrect = json.resultados.nucnfliqCorrect;
                        var Correcto = json.resultados.isCorrect;
                        if (Correcto) {

                            $("#nuorden_SE").val(nuorden);
                            $("#nbooking_SE").val(nbooking);
                            $("#nucnfclt_SE").val(nucnfclt);
                            $("#nucnfliq_SE").val(nucnfliq);
                            $("#formulariotitulares").attr('class', 'modal modal-alto-450');
                            $("#cargarTitutalaresDatos").fadeIn();
                        } else {
                            $("#cargarTitutalaresDatos").fadeOut();
                            $("#formulariotitulares").attr('class', 'modal modalalto9');
                            if (nuordenCorrect) {
                                $("#nuorden_SE").val(nuorden);
                            } else {
                                $("#nuorden_SE").val('');
                            }

                            if (nbookingCorrect) {
                                $("#nbooking_SE").val(nbooking);
                            } else {
                                $("#nbooking_SE").val('');
                            }

                            if (nucnfcltCorrect) {
                                $("#nucnfclt_SE").val(nucnfclt);
                            } else {
                                $("#nucnfclt_SE").val('');
                            }

                            if (nucnfliqCorrect) {
                                $("#nucnfliq_SE").val(nucnfliq);
                            } else {
                                $("#nucnfliq_SE").val('');
                            }

                        }

                    } else {
                        fErrorTxt(json.error, 1);
                    }

                });
            }

        }

// Declara los datos del alta
        function crearTitulares(oTable) {
            console.log("*******************");
            console.log("Creación datos titulares routing");
            console.log("*******************");
            lookBotones(false);
            paramsCrear = [];
            document.getElementById("titleTitu").innerHTML = "Creaci&oacute;n Titulares ( " + paramsCrear.length + " Titulares )";
            var nombre = "";
            var apellido1 = "";
            var apellido2 = "";
            var tpdocu = "";
            var nif = "";
            var tptipr = "";
            var tpsoci = "";
            var partic = "";
            var ccv = "";
            var nurefo = "";
            var tpnaci = "";
            var cdnaci = "";
            var paires = "";
            var tpdomi = "";
            var nbdomi = "";
            var nudomi = "";
            var ciudad = "";
            var provin = "";
            var cdpost = "";
            $('#nombre_TI').val(nombre);
            $('#apellido1_TI').val(apellido1);
            $('#apellido2_TI').val(apellido2);
            $('#tpdocu_TI').val(tpdocu);
            $('#nif_TI').val(nif);
            $('#tptipr_TI').val(tptipr);
            $('#tpsoci_TI').val(tpsoci);
            $('#partic_TI').val(partic);
            $('#ccv_TI').val(ccv);
            $('#nurefo_TI').val(nurefo);
            $('#tpnaci_TI').val(tpnaci);
            $('#cdnaci_TI').val(cdnaci);
            $('#paires_TI').val(paires);
            $('#tpdomi_TI').val(tpdomi);
            $('#nbdomi_TI').val(nbdomi);
            $('#nudomi_TI').val(nudomi);
            $('#ciudad_TI').val(ciudad);
            $('#provin_TI').val(provin);
            $('#cdpost_TI').val(cdpost);
            $('#label_nbooking_TI').css('display', 'none');
            $('#nbooking_TI').css('display', 'none');
            $('#nurefo_TI').prop('disabled', false);
            $('#formulariotitulares').css('display', 'block');
            $('fade').css('display', 'block');
            $('div#formulariotitulares input.mybutton').attr('data', 'crear');
        }

//Declara los datos de modificaciones
        function modificarTitulares(oTable) {

            console.log("*******************");
            console.log("Modificación datos titulares routing");
            console.log("*******************");
            $scope.listaModi = [];
            var aData = oTable.getData();
            var contador = 0;
            var input;
            var idName;
            var inputSel;
            for (var i = 0, j = aData.length; i < j; i++) {
                if ($scope.listaChk[i] == true) {
                    $scope.listaModi.push(aData[i]);
                }
            }


            if ($scope.listaModi.length == 0) {
                fErrorTxt("Debe seleccionar al menos uno de ellos.", 2)
                return;
            }
            lookBotones(false);
            document.getElementById("titleTitu").innerHTML = "Modificaci&oacute;n Titulares ( " + $scope.listaModi.length + " Titulares )";
            var nombre = $scope.listaModi[0][10] !== null ? $scope.listaModi[0][10].trim() : "";
            var apellido1 = $scope.listaModi[0][11] !== null ? $scope.listaModi[0][11].trim() : "";
            var apellido2 = $scope.listaModi[0][12] !== null ? $scope.listaModi[0][12].trim() : "";
            var tpdocu = $scope.listaModi[0][13] !== null ? $scope.listaModi[0][13].trim() : "";
            var nif = $scope.listaModi[0][14] !== null ? $scope.listaModi[0][14].trim() : "";
            var tptipr = $scope.listaModi[0][23] !== null ? $scope.listaModi[0][23].trim() : "";
            var tpsoci = $scope.listaModi[0][15] !== null ? $scope.listaModi[0][15].trim() : "";
            var partic = $scope.listaModi[0][24] !== null ? $scope.listaModi[0][24] : 0;
            var ccv = $scope.listaModi[0][4] !== null ? $scope.listaModi[0][4].trim() : "";
            var nurefo = $scope.listaModi[0][26] !== null ? $scope.listaModi[0][26].trim() : "";
            var tpnaci = $scope.listaModi[0][25] !== null ? $scope.listaModi[0][25].trim() : "";
            var cdnaci = $scope.listaModi[0][37] !== null ? $scope.listaModi[0][37].trim() : "";
            var paires = $scope.listaModi[0][16] !== null ? $scope.listaModi[0][16].trim() : "";
            var tpdomi = $scope.listaModi[0][27] !== null ? $scope.listaModi[0][27].trim() : "";
            var nbdomi = $scope.listaModi[0][28] !== null ? $scope.listaModi[0][28].trim() : "";
            var nudomi = $scope.listaModi[0][29] !== null ? $scope.listaModi[0][29].trim() : "";
            var ciudad = $scope.listaModi[0][20] !== null ? $scope.listaModi[0][20].trim() : "";
            var provin = $scope.listaModi[0][21] !== null ? $scope.listaModi[0][21].trim() : "";
            var cdpost = $scope.listaModi[0][22] !== null ? $scope.listaModi[0][22].trim() : "";
            var booking = $scope.listaModi[0][30] !== null ? $scope.listaModi[0][30].trim() : "";
            for (var i = 1, j = $scope.listaModi.length; i < j; i++) {
                nombre = (nombre !== ($scope.listaModi[i][10] !== null ? $scope.listaModi[i][10].trim() : "")) ? "" : nombre;
                apellido1 = (apellido1 !== ($scope.listaModi[i][11] !== null ? $scope.listaModi[i][11].trim() : "")) ? "" : apellido1;
                apellido2 = (apellido2 !== ($scope.listaModi[i][12] !== null ? $scope.listaModi[i][12].trim() : "")) ? "" : apellido2;
                tpdocu = (tpdocu !== ($scope.listaModi[i][13] !== null ? $scope.listaModi[i][13].trim() : "")) ? "" : tpdocu;
                nif = (nif !== ($scope.listaModi[i][14] !== null ? $scope.listaModi[i][14].trim() : "")) ? "" : nif;
                tptipr = (tptipr !== ($scope.listaModi[i][23] !== null ? $scope.listaModi[i][23].trim() : "")) ? "" : tptipr;
                tpsoci = (tpsoci !== ($scope.listaModi[i][15] !== null ? $scope.listaModi[i][15].trim() : "")) ? "" : tpsoci;
                partic = (partic !== $scope.listaModi[i][24]) ? 0 : partic;
                ccv = (ccv !== ($scope.listaModi[i][4] !== null ? $scope.listaModi[i][4].trim() : "")) ? "" : ccv;
                nurefo = (nurefo !== ($scope.listaModi[i][26] !== null ? $scope.listaModi[i][26].trim() : "")) ? "" : nurefo;
                tpnaci = (tpnaci !== ($scope.listaModi[i][25] !== null ? $scope.listaModi[i][25].trim() : "")) ? "" : tpnaci;
                cdnaci = (cdnaci !== ($scope.listaModi[i][37] !== null ? $scope.listaModi[i][37].trim() : "")) ? "" : cdnaci;
                paires = (paires !== ($scope.listaModi[i][16] !== null ? $scope.listaModi[i][16].trim() : "")) ? "" : paires;
                tpdomi = (tpdomi !== ($scope.listaModi[i][27] !== null ? $scope.listaModi[i][27].trim() : "")) ? "" : tpdomi;
                nbdomi = (nbdomi !== ($scope.listaModi[i][28] !== null ? $scope.listaModi[i][28].trim() : "")) ? "" : nbdomi;
                nudomi = (nudomi !== ($scope.listaModi[i][29] !== null ? $scope.listaModi[i][29].trim() : "")) ? "" : nudomi;
                ciudad = (ciudad !== ($scope.listaModi[i][20] !== null ? $scope.listaModi[i][20].trim() : "")) ? "" : ciudad;
                provin = (provin !== ($scope.listaModi[i][21] !== null ? $scope.listaModi[i][21].trim() : "")) ? "" : provin;
                cdpost = (cdpost !== ($scope.listaModi[i][22] !== null ? $scope.listaModi[i][22].trim() : "")) ? "" : cdpost;
                booking = (booking !== ($scope.listaModi[i][30] !== null ? $scope.listaModi[i][30].trim() : "")) ? "" : booking;
            }

            $('#nombre_TI').val(nombre);
            $('#apellido1_TI').val(apellido1);
            $('#apellido2_TI').val(apellido2);
            $('#tpdocu_TI').val(tpdocu);
            $('#nif_TI').val(nif);
            $('#tptipr_TI').val(tptipr);
            $('#tpsoci_TI').val(tpsoci);
            $('#partic_TI').val(partic);
            $('#ccv_TI').val(ccv);
            $('#nurefo_TI').val(nurefo);
            $('#tpnaci_TI').val(tpnaci);
            $('#cdnaci_TI').val(cdnaci);
            $('#paires_TI').val(paires);
            $('#tpdomi_TI').val(tpdomi);
            $('#nbdomi_TI').val(nbdomi);
            $('#nudomi_TI').val(nudomi);
            $('#ciudad_TI').val(ciudad);
            $('#provin_TI').val(provin);
            $('#cdpost_TI').val(cdpost);
            $('#nbooking_TI').val(booking);
            $('#label_nbooking_TI').css('display', 'block');
            $('#nbooking_TI').css('display', 'block');
            $('#nurefo_TI').prop('disabled', true);
            $('#nbooking_TI').prop('disabled', true);
            $('#formulariotitulares').css('display', 'block');
            $('fade').css('display', 'block');
            $('div#formulariotitulares input.mybutton').attr('data', 'modificar');
        }

// prepara titular para cuando se den Continuar en alta.
        function mandarTitularesCrearContinuar() {
            var nombre = $('#nombre_TI').val();
            var apellido1 = $('#apellido1_TI').val();
            var apellido2 = $('#apellido2_TI').val();
            var tpdocu = $('#tpdocu_TI').val();
            var nif = $('#nif_TI').val();
            var tptipr = $('#tptipr_TI').val();
            var tpsoci = $('#tpsoci_TI').val();
            var partic = $('#partic_TI').val();
            var ccv = $('#ccv_TI').val();
            var nurefo = $('#nurefo_TI').val();
            var tpnaci = $('#tpnaci_TI').val();
            var cdnaci = $('#cdnaci_TI').val();
            var paires = $('#paires_TI').val();
            var tpdomi = $('#tpdomi_TI').val();
            var nbdomi = $('#nbdomi_TI').val();
            var nudomi = $('#nudomi_TI').val();
            var ciudad = $('#ciudad_TI').val();
            var provin = $('#provin_TI').val();
            var cdpost = $('#cdpost_TI').val();
            var param;
            var nuorden = $("#nuorden_SE").val();
            var nbooking = $("#nbooking_SE").val();
            var nucnfclt = $("#nucnfclt_SE").val();
            var nucnfliq = $("#nucnfliq_SE").val();
            tpdocu = tpdocu == null ? "" : tpdocu;
            tptipr = tptipr == null ? "" : tptipr;
            tpsoci = tpsoci == null ? "" : tpsoci;
            tpnaci = tpnaci == null ? "" : tpnaci;
            cdnaci = cdnaci == null ? "" : cdnaci;
            paires = paires == null ? "" : paires;
            tpdomi = tpdomi == null ? "" : tpdomi;
            var filtro = "{";
            filtro += "\"nuorden\" : \"" + nuorden.trim() + "\",";
            filtro += "\"nbooking\" : \"" + nbooking.trim() + "\",";
            filtro += "\"nucnfclt\" : \"" + nucnfclt.trim() + "\",";
            filtro += "\"nucnfliq\" : \"" + nucnfliq.trim() + "\"";
            filtro += "}";
            param = {"nbclient": nombre, "nbclient1": apellido1, "nbclient2": apellido2, "nudnicif": nif, "tipnac": tpnaci, "cdnactit": cdnaci, "tipoDocumento": tpdocu, "paisRes": paires,
                "provincia": provin, "ciudad": ciudad, "codpostal": cdpost, "tptiPrep": tptipr, "tpsocied": tpsoci, "particip": partic, "ccv": ccv, "nureford": nurefo, "tpdomici": tpdomi,
                "nbdomici": nbdomi, "nudomici": nudomi}

            var longiparams = paramsCrear.length;
            paramsCrear[longiparams] = param;
            document.getElementById("titleTitu").innerHTML = "Creaci&oacute;n Titulares ( " + paramsCrear.length + " Titulares )";
            $("#cargarTitutalaresDatos").fadeOut();
            $("#cargarTitutalaresDatos").fadeIn();
        }

//graba los datos de alta
        function mandarTitularesCreados() {

            console.log("*******************");
            console.log("Actualización datos de creacion de titulares routing");
            console.log("Titular : "); //+ nombre.trim()+" "+apellido1.trim()+" "+apellido2.trim());
            console.log("*******************");
            var nombre = $('#nombre_TI').val();
            var apellido1 = $('#apellido1_TI').val();
            var apellido2 = $('#apellido2_TI').val();
            var tpdocu = $('#tpdocu_TI').val();
            var nif = $('#nif_TI').val();
            var tptipr = $('#tptipr_TI').val();
            var tpsoci = $('#tpsoci_TI').val();
            var partic = $('#partic_TI').val();
            var ccv = $('#ccv_TI').val();
            var nurefo = $('#nurefo_TI').val();
            var tpnaci = $('#tpnaci_TI').val();
            var cdnaci = $('#cdnaci_TI').val();
            var paires = $('#paires_TI').val();
            var tpdomi = $('#tpdomi_TI').val();
            var nbdomi = $('#nbdomi_TI').val();
            var nudomi = $('#nudomi_TI').val();
            var ciudad = $('#ciudad_TI').val();
            var provin = $('#provin_TI').val();
            var cdpost = $('#cdpost_TI').val();
            var param;
            var nuorden = $("#nuorden_SE").val();
            var nbooking = $("#nbooking_SE").val();
            var nucnfclt = $("#nucnfclt_SE").val();
            var nucnfliq = $("#nucnfliq_SE").val();
            var filtro = "{";
            filtro += "\"nuorden\" : \"" + nuorden.trim() + "\",";
            filtro += "\"nbooking\" : \"" + nbooking.trim() + "\",";
            filtro += "\"nucnfclt\" : \"" + nucnfclt.trim() + "\",";
            filtro += "\"nucnfliq\" : \"" + nucnfliq.trim() + "\"";
            filtro += "}";
            param = {"nbclient": nombre, "nbclient1": apellido1, "nbclient2": apellido2, "nudnicif": nif, "tipnac": tpnaci, "cdnactit": cdnaci, "tipoDocumento": tpdocu, "paisRes": paires,
                "provincia": provin, "ciudad": ciudad, "codpostal": cdpost, "tptiPrep": tptipr, "tpsocied": tpsoci, "particip": partic, "ccv": ccv, "nureford": nurefo, "tpdomici": tpdomi,
                "nbdomici": nbdomi, "nudomici": nudomi}

            var longiparams = paramsCrear.length;
            paramsCrear[longiparams] = param;
            document.getElementById("titleTitu").innerHTML = "Creaci&oacute;n Titulares ( " + paramsCrear.length + " Titulares )";
            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('createTitulares');
            data.setFilters(filtro);
            data.setParams(JSON.stringify(paramsCrear));
            var request = requestSIBBAC(data);
            request.success(function (json) {

                console.log(json);
                console.log(json.resultados);
                if (json.resultados != null && json.resultados !== undefined) {

                    var creaConError = json.resultados.afiErrorCreated;
                    var creaSinError = json.resultados.afiCreated;
                    fErrorTxt("Creados " + creaConError + " titulares con errores. </br> Creados " + creaSinError + " titulares sin errores.", 3);
                    cargarDatosConsultaTitulares();
                } else {
                    fErrorTxt(json.error, 2);
                }

            });
            cerrar()

        }


//Graba los datos de las modificaciones.
        function mandarTitularesModificados() {


            console.log("*******************");
            console.log("Actualización datos titulares routing");
            console.log("Titular : "); //+ nombre.trim()+" "+apellido1.trim()+" "+apellido2.trim());
            console.log("*******************");
            var nombre = $('#nombre_TI').val();
            var apellido1 = $('#apellido1_TI').val();
            var apellido2 = $('#apellido2_TI').val();
            var tpdocu = $('#tpdocu_TI').val();
            var nif = $('#nif_TI').val();
            var tptipr = $('#tptipr_TI').val();
            var tpsoci = $('#tpsoci_TI').val();
            var partic = $('#partic_TI').val();
            var ccv = $('#ccv_TI').val();
            var nurefo = $('#nurefo_TI').val();
            var tpnaci = $('#tpnaci_TI').val();
            var cdnaci = $('#cdnaci_TI').val();
            var paires = $('#paires_TI').val();
            var tpdomi = $('#tpdomi_TI').val();
            var nbdomi = $('#nbdomi_TI').val();
            var nudomi = $('#nudomi_TI').val();
            var ciudad = $('#ciudad_TI').val();
            var provin = $('#provin_TI').val();
            var cdpost = $('#cdpost_TI').val();
            tpdocu = tpdocu == null ? "" : tpdocu;
            tptipr = tptipr == null ? "" : tptipr;
            tpsoci = tpsoci == null ? "" : tpsoci;
            tpnaci = tpnaci == null ? "" : tpnaci;
            cdnaci = cdnaci == null ? "" : cdnaci;
            paires = paires == null ? "" : paires;
            tpdomi = tpdomi == null ? "" : tpdomi;
            var filtro = "{\"nbclient\" : \"" + nombre + "\", \"nbclient1\" : \"" + apellido1
                    + "\", \"nbclient2\" : \"" + apellido2 + "\", \"nudnicif\" : \"" + nif + "\", \"tipnac\" : \"" + tpnaci
                    + "\", \"cdnactit\" : \"" + cdnaci + "\", \"tipoDocumento\" : \"" + tpdocu + "\", \"paisRes\" : \"" + paires
                    + "\", \"provincia\" : \"" + provin + "\", \"ciudad\" : \"" + ciudad + "\", \"codpostal\" : \"" + cdpost
                    + "\", \"tptiPrep\" : \"" + tptipr + "\", \"tpsocied\" : \"" + tpsoci + "\", \"particip\" : \"" + partic
                    + "\", \"ccv\" : \"" + ccv + "\", \"nureford\" : \"" + nurefo + "\", \"tpdomici\" : \"" + tpdomi
                    + "\", \"nbdomici\" : \"" + nbdomi + "\", \"nudomici\" : \"" + nudomi
                    + "\" } ";
            var params = [];
            var param;
            var hasErrors;
            var id;
            var nbooking;
            var nuorden;
            var nucnclt;
            var nucnfliq;
            var nusecuen;
            for (var i = 0, j = $scope.listaModi.length; i < j; i++) {
                hasErrors = "";
                id = "";
                nbooking = "";
                nuorden = "";
                nucnclt = "";
                nucnfliq = "";
                nusecuen = "";
                if ($scope.listaModi[i][36] == true) {
                    hasErrors = $scope.listaModi[i][36];
                    id = $scope.listaModi[i][33];
                    param = {"hasErrors": hasErrors, "id": id};
                } else {
                    hasErrors = $scope.listaModi[i][36];
                    nbooking = hasErrors = $scope.listaModi[i][30];
                    nuorden = $scope.listaModi[i][34];
                    nucnclt = $scope.listaModi[i][31];
                    nucnfliq = $scope.listaModi[i][32];
                    nusecuen = $scope.listaModi[i][35];
                    param = {"hasErrors": hasErrors, "nbooking": nbooking, "nuorden": nuorden, "nucnfclt": nucnclt, "nucnfliq": nucnfliq, "nusecuen": nusecuen};
                }

                params[i] = param;
            }

            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('updateTitularesList');
            data.setFilters(filtro);
            data.setParams(JSON.stringify(params));
            var request = requestSIBBAC(data);
            request.success(function (json) {

                console.log(json);
                console.log(json.resultados);
                if (json.resultados != null && json.resultados !== undefined) {

                    var modiConError = json.resultados.nAfiErrorProcessed;
                    var modiSinError = json.resultados.nAfiProcessed;
                    var dejarErrores = json.resultados.nAfiErrorFixed;
                    var noPasarErrores = json.resultados.nAfiNotProcessed;
                    fErrorTxt("Modificados " + modiConError + " titulares con errores. </br> Modificados " + modiSinError + " titulares sin errores. </br>" + dejarErrores + " titulares dejaron de tener errores.</br>" + noPasarErrores + " titulares no se modificaron porque pasarían a un estado con error", 3);
                    cargarDatosConsultaTitulares();
                    for (var i = 0; i < $scope.listaChk.length; i++) {
                        $scope.listaChk[i] = false;
                    }

                } else {
                    fErrorTxt(json.error, 2);
                }
            });
            cerrar()

        }

// cierra los formularios de creacion/modificacion
        function cerrar() {

            lookBotones(true);
            $('#formulariotitulares').css('display', 'none');
        }


// valida las condiciones del filtro.
        function validadTitulares() {
            var error = null;
            var fContratacionDe = $("#fcontratacionDe").val();
            var fContratacionA = $("#fcontratacionA").val();
            if (($("#descripcionError").val() == 0 || $("#descripcionError").val() == 1) && fContratacionDe.length == 0) {
                error = "Si la descripción del error es \"TODOS\" o \"SIN ERRORES\" las fecha de contratación desde no pueden ir vacia.";
                return error;
            }

            if (!(fContratacionDe.length == 0 || fContratacionA.length == 0)) {
                if (!(validDifFechas(fContratacionDe, fContratacionA))) {
                    error = "La diferencia entre la fechas de inicio y fin no puede ser superior a un año."
                    return error;
                }
            }

            if (!(fContratacionDe.length == 0) && fContratacionA.length == 0) {
                var fechaDeHoy = new Date();
                var fechaHoyTxt = fechaDeHoy.getDate() + "/" + (fechaDeHoy.getMonth() + 1) + "/" + fechaDeHoy.getFullYear()
                if (!(validDifFechas(fContratacionDe, fechaHoyTxt))) {
                    error = "Si no se introduce \"fecha de contración hasta\" la \"fecha de contratación desde\" no puede ser de hace mas de un año." +
                            " Si desea esta  \"fecha de contratación desde\" esta obligado a introducir una \"fecha de contratación hasta\" inferior a un año de diferencia.";
                    return error;
                }
            }
            return error;
        }

// Valida la diferencia de fechas.
        function validDifFechas(fecha1, fecha2) {

            var date1 = fecha1.split('/');
            var date2 = fecha2.split('/')


            var dateAnterior = [];
            dateAnterior[0] = date1[1];
            dateAnterior[1] = date1[0];
            dateAnterior[2] = date1[2];
            var datePosterior = [];
            datePosterior[0] = date2[1];
            datePosterior[1] = date2[0];
            datePosterior[2] = date2[2];
            var date11 = new Date(dateAnterior);
            var date22 = new Date(datePosterior);
            var timeDiff = Math.abs(date22.getTime() - date11.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if (diffDays > 366) {
                return false;
            }
            return true;
        }

//Chequea los errores.
        function CheckErrors() {
            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('checkAfiErrors');
            var request = requestSIBBAC(data);
            request.success(function (json) {

                console.log(json);
                console.log(json.resultados);
                if (json.resultados != null && json.resultados !== undefined) {

                    var fixed = json.resultados.fixedAfis;
                    fErrorTxt("Se han movido " + fixed + " registros a la tabla de sin errores \n", 3);
//		    cargarDatosConsultaTitulares();

                } else {
                    fErrorTxt(json.error, 2);
                }

            });
        }

        function lookBotones(tipo) {
            if (tipo) {
                if ($scope.listaChk.length > 0) {
                    $('#ImportarFichero').css('display', 'inline');
                    $('#CheckearErrors').css('display', 'inline');
                    $('#MarcarTod').css('display', 'inline');
                    $('#MarcarPag').css('display', 'inline');
                    $('#CrearTitulares').css('display', 'inline');
                    $('#ModificarTitulares').css('display', 'inline');
                } else {
                    $('#ImportarFichero').css('display', 'inline');
                    $('#CrearTitulares').css('display', 'inline');
                }
            } else {
                $('#ImportarFichero').css('display', 'none');
                $('#CheckearErrors').css('display', 'none');
                $('#MarcarTod').css('display', 'none');
                $('#MarcarPag').css('display', 'none');
                $('#CrearTitulares').css('display', 'none');
                $('#ModificarTitulares').css('display', 'none');
            }
            return
        }

        function cargarFichero(files) {
            console.log('Entra en enviarFichero....');
            var file = files[0];
            console.log('file ..: ' + file)

            if (files && file) {
                var reader = new FileReader();
                var btoaTxt;
                var nombre = file.name;
                reader.onload = function (readerEvt) {
                    var binaryString = readerEvt.target.result;
                    btoaTxt = btoa(binaryString);
                    enviarFichero(btoaTxt, nombre);
                };
                reader.readAsBinaryString(file);
            }
            $('#cargarFichero').css('display', 'none');
            if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
                fErrorTxt('The File APIs are not fully supported in this browser.', 1);
            }

        }


        function enviarFichero(Text, nombre) {


            var filtro = "{\"filename\" : \"" + nombre + "\",";
            filtro += "\"file\" : \"" + Text + "\"";
            filtro += "}";
            var data = new DataBean();
            data.setService('SIBBACServiceTitulares');
            data.setAction('importTitularesExcel');
            data.setFilters(filtro);
            var request = requestSIBBAC(data);
            request.success(function (json) {

                console.log(json);
                console.log(json.resultados);
                if (json.resultados != null && json.resultados !== undefined) {

                    var resultado = json.resultados.wellProcessed;
                    if (resultado) {
                        fErrorTxt("La importación se ha realizado correctamente", 3);
                    } else {
                        fErrorTxt("La importación se ha realizado NO correctamente", 1);
                    }


                } else {
                    fErrorTxt(json.error, 2);
                }

            });
        }

        function obtenerIdsSeleccionados(lista) {
            var selected = ""
            var i = 0;
            $(lista + ' option').each(function () {
                if (i > 0) {
                    selected += ";";
                }
                selected += $(this).val();
                i++;
            });
            return selected;
        }

    }]);


