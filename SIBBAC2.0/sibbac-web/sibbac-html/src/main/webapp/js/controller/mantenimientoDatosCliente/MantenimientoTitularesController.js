'use strict';
sibbac20
    .controller(
                'MantenimientoTitularesController',
                [
                 '$scope',
                 '$document',
                 'growl',
                 'MantenimientoTitularesService',
                 '$compile',
                 function ($scope, $document, growl, MantenimientoTitularesService, $compile) {
                   var oTable = undefined;
                   var tipoOperacion = "";

                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   $scope.listaModi = [];

                   $scope.datosTitular = {
                     tipopersona : "",
                     tipotitular : "",
                     tipodocumento : "",
                     documento : "",
                     nombre : "",
                     primerapellido : "",
                     segundopellido : "",
                     fechanacimiento : "",
                     tiponacionalidad : "",
                     codnacionalidad : "",
                     nacionalidad : "",
                     tipodireccion : "",
                     domicilio : "",
                     numdomicilio : "",
                     ciudad : "",
                     provincia : "",
                     codpostal : "",
                     codpaisresidencia : "",
                     paisresidencia : "",
                     tipoIdMifid : "",
                     idMifid : "",
                     indClienteSan : "",
                     numPersonaSan : ""
                    	 
                    	 
                   }
                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };
                   $scope.showingFilter = true;

                   $document.ready(function () {
                     inicializaFiltro(true);
                     initialize(); // Se inicializa el
                     // formulario con los
                     // datos esticos
                     loadDataTable();
                   });

                   function inicializaFiltro (estado) {

                     $scope.filtro = {
                       tipopersona : "",
                       tipotitular : "",
                       documento : "",
                       nombre : "",
                       primerapellido : "",
                       segundoapellido : "",
                       tiponacionalidad : "",
                       nacionalidad : "",
                       paisresidencia : "",
                       tipodireccion : "",
                       direccion : "",
                       ciudad : "",
                       provincia : "",
                       codpostal : "",
                       chtipopersona : estado,
                       chtipotitular : estado,
                       chtipodocumento : estado,
                       chdocumento : estado,
                       chnombre : estado,
                       chprimerapellido : estado,
                       chsegundoapellido : estado,
                       chtiponacionalidad : estado,
                       chnacionalidad : estado,
                       chpaisresidencia : estado,
                       chtipodireccion : estado,
                       chdireccion : estado,
                       chciudad : estado,
                       chprovincia : estado,
                       chcodpostal : estado

                     };

                   }

                   function initialize () {
                     // Se inicializan de forma asincrona los datos estaticos y autocompletables de la pantalla
                     MantenimientoTitularesService.Inicializa(onSuccessInicizalizaRequest, onErrorRequest, {});
                     MantenimientoTitularesService.CargaNombres(onSuccessCargaNombresRequest, onErrorRequest, {});
                     MantenimientoTitularesService.CargaApellidos(onSuccessCargaApellidosRequest, onErrorRequest, {});
                     MantenimientoTitularesService.CargaDocumentos(onSuccessCargaDocumentosRequest, onErrorRequest, {});
                     $.unblockUI();
                   }

                   $scope.Consultar = function (event) {
                     $scope.showingFilter = false;
                     cargaTitularesFiltro();
                   }

                   // Boton de limpiar filtros, se utiliza este evento
                   // porque el reset deja los valores en el filtro y
                   // los botones de igual e incluido.
                   $scope.LimpiarFiltros = function (event) {

                     // Inicializamos el filtro
                     inicializaFiltro(false);

                     // Ponemos todos los botones como al principio
                     $scope.btnInvGenerico('TipoPersona', 1);
                     $scope.btnInvGenerico('TipoTitular', 2);
                     $scope.btnInvGenerico('TipoDocumento', 2);
                     $scope.btnInvGenerico('Documento', 1);
                     $scope.btnInvGenerico('Nombre', 1);
                     $scope.btnInvGenerico('PrimerApellido', 1);
                     $scope.btnInvGenerico('SegundoApellido', 1);
                     $scope.btnInvGenerico('TipoNacionalidad', 1);
                     $scope.btnInvGenerico('Nacionalidad', 2);
                     $scope.btnInvGenerico('TipoDireccion', 1);
                     $scope.btnInvGenerico('Direccion', 1);
                     $scope.btnInvGenerico('Ciudad', 1);
                     $scope.btnInvGenerico('Provincia', 1);
                     $scope.btnInvGenerico('CodPostal', 1);
                     $scope.btnInvGenerico('PaisResidencia', 2);

                     // Eliminamos las naciones y paises de
                     // residencia incluidos
                     $("#nacionalidad option[value]").remove();
                     $("#paisResidencia option[value]").remove();

                   }

                   function cargaTitularesFiltro () {
                     // Se muestra la capa cargando
                     inicializarLoading();

                     // Se inicializan todos los filtros teniendo en
                     // cuenta si hay que buscar el valor igual o
                     // distinto, incluido o no incluido.
                     // Tipo de Persona

                     var tipoPersona = $("select#tipopersona").val();
                     if (!$scope.filtro.chtipopersona && tipoPersona !== "") {
                       tipoPersona = "#" + tipoPersona;
                     }

                     // Inicializamos Los tipos de titulares
                     // seleccionados
                     var tipoTitularSeleccionados = getValuesSeleccionadosSelectMultiple('tipotitular');
                     // Asignación si la consulta es de los incluidos
                     // o los excluidos.
                     if (!$scope.filtro.chtipotitular && tipoTitularSeleccionados !== "") {
                       tipoTitularSeleccionados = "#" + tipoTitularSeleccionados;
                     }

                     // Inicializamos Los tipos de documentos
                     // seleccionados
                     var tipoDocumentoSeleccionados = getValuesSeleccionadosSelectMultiple('tipodocumento');
                     // Asignación si la consulta es de los incluidos
                     // o los excluidos.
                     if (!$scope.filtro.chtipodocumento && tipoDocumentoSeleccionados !== "") {
                       tipoDocumentoSeleccionados = "#" + tipoDocumentoSeleccionados;
                     }

                     // documento
                     // var documento = $scope.filtro.documento.toUpperCase();
                     var documento = $("#documento").val().toUpperCase();
                     if (!$scope.filtro.chdocumento && documento !== "") {
                       documento = "#" + documento;
                     }

                     // nombre
                     var nombre = $("#nombre").val().toUpperCase();

                     if (!$scope.filtro.chnombre && nombre !== "") {
                       nombre = "#" + nombre;
                     }

                     // primer apellido
                     var primerapellido = $("#primerapellido").val().toUpperCase();
                     if (!$scope.filtro.chprimerapellido && primerapellido !== "") {
                       primerapellido = "#" + primerapellido;
                     }

                     // segundo apellido
                     var segundoapellido = $("#segundoapellido").val().toUpperCase();
                     if (!$scope.filtro.chsegundoapellido && segundoapellido !== "") {
                       segundoapellido = "#" + segundoapellido;
                     }

                     // Tipo de nacionalidad
                     var tipoNacionalidad = $("select#tiponacionalidad").val();
                     if (!$scope.filtro.chtiponacionalidad && tipoNacionalidad !== "") {
                       tipoNacionalidad = "#" + tipoNacionalidad;
                     }

                     // Inicializamos las nacionalidades
                     // seleccionados
                     var nacionalidadesSeleccionadas = getValuesSeleccionadosSelectMultiple('nacionalidad');
                     // Asignación si la consulta es de los incluidos
                     // o los excluidos.
                     if (!$scope.filtro.chnacionalidad && nacionalidadesSeleccionadas !== "") {
                       nacionalidadesSeleccionadas = "#" + nacionalidadesSeleccionadas;
                     }

                     // FILTROS POR DIRECCION
                     var tipodireccion = $scope.filtro.tipodireccion.toUpperCase();
                     if (!$scope.filtro.chtipodireccion && tipodireccion !== "") {
                       tipodireccion = "#" + tipodireccion;
                     }

                     var direccion = $scope.filtro.direccion.toUpperCase();
                     if (!$scope.filtro.chdireccion && direccion !== "") {
                       direccion = "#" + direccion;
                     }

                     var ciudad = $scope.filtro.ciudad.toUpperCase();
                     if (!$scope.filtro.chciudad && ciudad !== "") {
                       ciudad = "#" + ciudad;
                     }

                     var provincia = $scope.filtro.provincia.toUpperCase();
                     if (!$scope.filtro.chprovincia && provincia !== "") {
                       provincia = "#" + provincia;
                     }

                     var codpostal = $scope.filtro.codpostal;
                     if (!$scope.filtro.chcodpostal && codpostal !== "") {
                       codpostal = "#" + codpostal;
                     }

                     // Inicializamos los paises de residencia
                     // seleccionados seleccionados
                     var paisesResidenciaSeleccionados = getValuesSeleccionadosSelectMultiple('paisResidencia');
                     // Asignación si la consulta es de los incluidos
                     // o los excluidos.
                     if (!$scope.filtro.chpaisresidencia && paisesResidenciaSeleccionados !== "") {
                       paisesResidenciaSeleccionados = "#" + paisesResidenciaSeleccionados;
                     }

                     // formamos el filtro
                     var filtro = {
                       tipopersona : tipoPersona,
                       tipotitular : tipoTitularSeleccionados,
                       tipodocumento : tipoDocumentoSeleccionados,
                       documento : documento,
                       nombre : nombre,
                       primerapellido : primerapellido,
                       segundoapellido : segundoapellido,
                       tiponacionalidad : tipoNacionalidad,
                       nacionalidad : nacionalidadesSeleccionadas,
                       tipodireccion : tipodireccion,
                       direccion : direccion,
                       ciudad : ciudad,
                       provincia : provincia,
                       codpostal : codpostal,
                       paisresidencia : paisesResidenciaSeleccionados

                     };
                     // Realizamos la consulta
                     MantenimientoTitularesService.ConsultaTitularesFiltro(onSuccessConsultaTitularesFiltroRequest,
                                                                           onErrorRequest, filtro);
                   }

                   function loadDataTable () {
                     oTable = $("#datosTitulares").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                         "aButtons" : [ "copy", {
                           "sExtends" : "csv",
                           "sFileName" : "Listado_finalholders_" + hoy + ".csv",
                           "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ]
                         }, {
                           "sExtends" : "xls",
                           "sFileName" : "Listado_finalholders_" + hoy + ".xls",
                           "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ]
                         }, {
                           "sExtends" : "pdf",
                           "sPdfOrientation" : "landscape",
                           "sTitle" : " ",
                           "sPdfSize" : "A3",
                           "sPdfMessage" : "Listado Final Holders",
                           "sFileName" : "Listado_finalholders_" + hoy + ".pdf",
                           "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ]
                         }, "print" ]
                       },

                       "aoColumns" : [ {
                         sClass : "centrar",
                         bSortable : false,
                         width : 20
                       }, // 0
                       {
                         sClass : "centrar",
                         width : 150
                       }, // Documento
                       {
                         sClass : "centrar",
                         width : 50
                       }, // Descripcion Tipo
                       // persona
                       {
                         sClass : "centrar",
                         width : 50
                       }, // Descripcion Tipo
                       // Titular
                       {
                         sClass : "centrar",
                         width : 50
                       }, // Descripcion del Tipo
                       // Documento
                       {
                         sClass : "centrar",
                         width : 200
                       }, // Nombre
                       {
                         sClass : "centrar",
                         width : 250
                       }, // Primer apellido
                       {
                         sClass : "centrar",
                         width : 250
                       }, // Segundo apellido
                       {
                         sClass : "centrar",
                         width : 250
                       }, // fecha de nacimiento
                       {
                         sClass : "centrar",
                         width : 50
                       }, // Descripcion Tipo nacionalidad
                       {
                         sClass : "centrar",
                         width : 250
                       }, // Nacionalidad
                       {
                         sClass : "centrar",
                         width : 75
                       }, // Tipo direccion
                       {
                         sClass : "centrar",
                         width : 150
                       }, // Direccion
                       {
                         sClass : "centrar",
                         width : 150
                       }, // ciudad
                       {
                         sClass : "centrar",
                         width : 150
                       }, // Provincia
                       {
                         sClass : "centrar",
                         width : 50
                       }, // cod postal
                       {
                         sClass : "centrar",
                         width : 250
                       }, // Pais de residencia
                       {
                           sClass : "centrar",
                           width : 25
                       }, // Tipo id mifid
                       {
                           sClass : "centrar",
                           width : 25
                       }, // id Mifid
                       {
                           sClass : "centrar",
                           width : 25
                       }, // ind cliente san
                       {
                           sClass : "centrar",
                           width : 25
                       }, // num cliente san
                       {
                         sClass : "centrar",
                         visible : false
                       }, // Código Nacionalidad
                       {
                         sClass : "centrar",
                         visible : false
                       }, // Código Pais de residencia
                       {
                         sClass : "centrar",
                         visible : false
                       }, // Numero domicilio
                       {
                         sClass : "centrar",
                         visible : false
                       }, // Tipo persona
                       {
                         sClass : "centrar",
                         visible : false
                       }, // Tipo Titular
                       {
                         sClass : "centrar",
                         visible : false
                       }, // Tipo Documento
                       {
                           sClass : "centrar",
                           visible : false
                       }// Tipo nacionalidad
                       ],
                       "scrollY" : "480px",
                       "scrollX" : "100%",
                       "scrollCollapse" : true,
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       }
                     });
                   }

                   /**
                     * Procesa un error durante la petición de dartos al servidor.
                     */
                   function onErrorRequest (data) {
                     $.unblockUI();
                     // growl.addErrorMessage("Ocurrió un error
                     // durante la petición de datos.");
                     fErrorTxt("Ocurrió un error durante la petición de datos.", 1);
                   } // onErrorRequest

                   /** Carga todos los datos iniciales de la pantalla. Documentos */
                   function onSuccessCargaDocumentosRequest (json) {
                     if (json.resultados === null) {
                       // growl.addErrorMessage(json.error);
                       fErrorTxt(json.error, 1);
                     } else {
                       var filtro = json.resultados.datosFiltro;

                       // Funcion que da la funcionalidad autocompletable de la caja de texto de documentos
                       var documentos = filtro.documentos;
                       var datosdocumento = undefined;
                       var availableDocumentos = [];
                       var obj;
                       for ( var k in documentos) {
                         datosdocumento = documentos[k];
                         obj = {
                           label : datosdocumento.trim(),
                           value : datosdocumento.trim()
                         };
                         availableDocumentos.push(obj);
                       }

                       pupulateInputAutocomplete("#documento", availableDocumentos);
                     }

                   }

                   /** Carga todos los datos iniciales de la pantalla. Nombres */
                   function onSuccessCargaNombresRequest (json) {
                     if (json.resultados === null) {
                       // growl.addErrorMessage(json.error);
                       fErrorTxt(json.error, 1);
                     } else {
                       var filtro = json.resultados.datosFiltro;

                       // Funcion que da la funcionalidad autocompletable de la caja de texto de nombre
                       var nombres = filtro.nombres;
                       var datosnombre = undefined;
                       var availableNombres = [];
                       var obj;
                       for ( var k in nombres) {
                         datosnombre = nombres[k];
                         obj = {
                           label : datosnombre.trim(),
                           value : datosnombre.trim()
                         };
                         availableNombres.push(obj);
                       }

                       pupulateInputAutocomplete("#nombre", availableNombres);

                     }

                   }

                   /** Carga todos los datos iniciales de la pantalla Apellidos. */
                   function onSuccessCargaApellidosRequest (json) {
                     if (json.resultados === null) {
                       // growl.addErrorMessage(json.error);
                       fErrorTxt(json.error, 1);
                     } else {
                       var filtro = json.resultados.datosFiltro;

                       // Funcion que da la funcionalidad autocompletable de la caja de texto de apellido1
                       var obj;
                       var apellidos1 = filtro.apellidos1;
                       var datosapellidos1 = undefined;
                       var availableapellidos1 = [];
                       for ( var k in apellidos1) {
                         datosapellidos1 = apellidos1[k];
                         obj = {
                           label : datosapellidos1.trim(),
                           value : datosapellidos1.trim()
                         };
                         availableapellidos1.push(obj);
                       }

                       pupulateInputAutocomplete("#primerapellido", availableapellidos1);

                       // Funcion que da la funcionalidad autocompletable de la caja de texto de apellido1

                       var apellidos2 = filtro.apellidos2;
                       var datosapellidos2 = undefined;
                       var availableapellidos2 = [];
                       for ( var k in apellidos2) {
                         datosapellidos2 = apellidos2[k];
                         obj = {
                           label : datosapellidos2.trim(),
                           value : datosapellidos2.trim()
                         };
                         availableapellidos2.push(obj);
                       }

                       pupulateInputAutocomplete("#segundoapellido", availableapellidos2);

                     }

                   }

                   /** Carga todos los datos iniciales de la pantalla. */
                   function onSuccessInicizalizaRequest (json) {

                     if (json.resultados === null) {
                       // growl.addErrorMessage(json.error);
                       fErrorTxt(json.error, 1);
                     } else {
                       var filtro = json.resultados.datosFiltro;
                       // Se cargan las listas de Tipo Persona del
                       // filtro y del alta y modificacion.
                       var select = $("select#tipopersona");
                       var selectNuevo = $("select#tipopersonaNuevo");
                       $.each(filtro.tipoPersona, function (key, value) {
                         if (key != "") {
                           select.append("<option value='" + key + "'>" + value + "</option>");
                           selectNuevo.append("<option value='" + key + "'>" + value + "</option>");
                         } else
                           select.append("<option value='" + key + "'>" + value + "</option>");

                       });
                       var select = $("select#tipotitular");
                       var selectNuevo = $("select#tipotitularNuevo");
                       $.each(filtro.tipoTitular, function (key, value) {
                         if (key != "") {
                           select.append("<option value='" + key + "'>" + value + "</option>");
                           selectNuevo.append("<option value='" + key + "'>" + value + "</option>");
                         } else
                           select.append("<option value='" + key + "'>" + value + "</option>");
                       });

                       var select = $("select#tipodocumento");
                       var selectNuevo = $("select#tipodocumentoNuevo");
                       $.each(filtro.tipoDocumento, function (key, value) {
                         if (key != "") {
                           select.append("<option value='" + key + "'>" + value + "</option>");
                           selectNuevo.append("<option value='" + key + "'>" + value + "</option>");
                         } else
                           select.append("<option value='" + key + "'>" + value + "</option>");

                       });

                       var select = $("select#tiponacionalidad");
                       var selectNuevo = $("select#tiponacionalidadNuevo");
                       $.each(filtro.tipoNacionalidad, function (key, value) {
                         if (key != "") {
                           select.append("<option value='" + key + "'>" + value + "</option>");
                           selectNuevo.append("<option value='" + key + "'>" + value + "</option>");
                         } else
                           select.append("<option value='" + key + "'>" + value + "</option>");

                       });

                       var select = $("select#tipodireccion");
                       var selectNuevo = $("select#tipodireccionNuevo");
                       $.each(filtro.tipoDomicilio, function (key, value) {
                         if (key != "") {
                           select.append("<option value='" + key + "'>" + value + "</option>");
                           selectNuevo.append("<option value='" + key + "'>" + value + "</option>");
                         } else
                           select.append("<option value='" + key + "'>" + value + "</option>");

                       });

                       // Se tratan las nacionalidades y paises recibidas.
                       // Se carga una array para el filtro y se carga un select para insert y el update
                       var availableNacionalidades = [];
                       var availablePaises = [];
                       var obj;
                       var selectNacionalidades = $("select#nacionalidadNuevo");
                       var selectPaiseResidencia = $("select#paisresidenciaNuevo");

                       var paises = filtro.nacionalidades;
                       var datospais = undefined;
                       for ( var k in paises) {
                         datospais = paises[k];

                         if(datospais.nbpais!=null){
                        	// En las nacionalidades la clave es el
                             // cdisonum que traigo en la key
                             selectNacionalidades.append("<option value='" + datospais.id.cdisonum.trim() + "'>"
                            		 + datospais.nbpais.trim() + "</option>");
                             
                             if(datospais.cdhacienda!=null){
                            	 // En los la clave es el cdhacienda
                            	 selectPaiseResidencia.append("<option value='" + datospais.cdhacienda.trim() + "'>"
                            			 + datospais.nbpais.trim() + "</option>");
                             }
                             // Formo el array de paises y
                             // nacionalidades
                             obj = {
                               label : datospais.id.cdisoalf2.trim() + datospais.id.cdisonum.trim() + "-"
                                       + datospais.nbpais.trim(),
                               value : datospais.id.cdisonum.trim()
                             };
                             availableNacionalidades.push(obj);
                             if(datospais.cdhacienda!=null){
                                 obj = {
                                         label : datospais.id.cdisoalf2.trim() + datospais.id.cdisonum.trim() + "-"
                                                 + datospais.nbpais.trim(),
                                         value : datospais.cdhacienda.trim()
                                       };
                                       availablePaises.push(obj);      
                             }
                         }
                         
                       }

                       $("#btnDelNacionalidad").click(function (event) {
                         event.preventDefault();
                         $("#nacionalidad").find("option:selected").remove().end();
                       });

                       $("#btnDelPaisResidencia").click(function (event) {
                         event.preventDefault();
                         $("#paisResidencia").find("option:selected").remove().end();
                       });

                       // Funcion que da la funcionalidad
                       // autocompletable de la caja de texto y la
                       // lista de las
                       // nacionalidades
                       pupulateListaAutocomplete("#textNacionalidad", "#idTxNacionalidad", availableNacionalidades,
                                                 "#nacionalidad");

                       // Funcion que da la funcionalidad
                       // autocompletable de la caja de texto y la
                       // lista de los Paises
                       // de residenca
                       pupulateListaAutocomplete("#textPaisResidencia", "#idTxPaisResidencia", availablePaises,
                                                 "#paisResidencia");

                       var provincias = filtro.provincias;
                       var datosprovincia = undefined;
                       var availableProvincias = [];
                       for ( var k in provincias) {
                         datosprovincia = provincias[k];
                         obj = {
                           label : datosprovincia.nbprovin.trim(),
                           value : datosprovincia.cdcodigo.trim()
                         };
                         availableProvincias.push(obj);
                       }

                       pupulateInputAutocomplete("#provinciaNuevo", availableProvincias);

                       $scope.fnacVisible = true;
                       $('input#fechaNacimientoNuevo').datepicker("option", "changeMonth", true);
                       $('input#fechaNacimientoNuevo').datepicker("option", "changeYear", true);
                       $('input#fechaNacimientoNuevo').datepicker("option", "yearRange", '-120:+0');
                       $('input#fechaNacimientoNuevo').datepicker("option", "maxDate", '+0m +0w -1d');

                       $("#tipopersonaNuevo").change(function (event) {
                         if ($("#tipopersonaNuevo").val() == 'J') {
                           $("#fechaNacimientoNuevo").val('');
                           $scope.fnacVisible = false;
                         } else {
                           $scope.fnacVisible = true;
                           $('input#fechaNacimientoNuevo').datepicker("option", "changeMonth", true);
                           $('input#fechaNacimientoNuevo').datepicker("option", "changeYear", true);
                           $('input#fechaNacimientoNuevo').datepicker("option", "yearRange", '-120:+0');
                           $('input#fechaNacimientoNuevo').datepicker("option", "maxDate", '+0m +0w -1d');

                         }
                         $scope.safeApply();
                       });

                       // Control de la provincia cuando modifica
                       // el pais de residencia en el alta y la
                       // modificación.
                       $("#paisresidenciaNuevo").change(function (event) {

                         var paisResidenciaActual = $("#paisresidenciaNuevo").val();
                         if (paisResidenciaActual == '011') { // Si
                           // es españa se habilita autocompletar y se inicializan la provicina y el codigo postal
                           $("#provinciaNuevo").val('');
                           $("#idprovinciaNuevo").val('');
                           $("#codpostalNuevo").val('');
                           $("#provinciaNuevo").autocomplete("enable");
                         } else { // Se
                           // desabilita y se deja que inserta un texto fijo. En este caso el codigo postal es 99 + los
                           // 3 digitos del pais.
                           $("#provinciaNuevo").autocomplete("disable");
                           $("#codpostalNuevo").val("99" + paisResidenciaActual);
                         }

                       });

                       // Control del cambio de provincia para que
                       // inicialice el codigo postal con la
                       // provincia
                       $("#provinciaNuevo").autocomplete({
                         select : function (event, ui) {
                           $("#provinciaNuevo").val(ui.item.label);
                           var paisResidenciaActual = $("#paisresidenciaNuevo").val();
                           var provinciaSelecionada = ui.item.value;
                           if ((paisResidenciaActual == '011') || (provinciaSelecionada != "")) { // Si
                             // es
                             // españa
                             // se
                             // habilita
                             // autocompletar
                             $("#codpostalNuevo").val(provinciaSelecionada);
                             $("#codpostalNuevo").focus();
                           }
                           return false;

                         }
                       });

                     }

                   } // onSuccessInicizalizaRequest

                   /**
                     * Carga los datos de los titulares que cumplen el filtro establecido.
                     */
                   function onSuccessConsultaTitularesFiltroRequest (json) {

                     if (json.resultados === null) {
                       // growl.addErrorMessage(json.error);
                       fErrorTxt(json.error, 1);
                     } else {
                       var datos = undefined;

                       if (json === undefined || json === null || json.error === undefined
                           || (json.error !== null && json.error.error !== "")) {
                         if (json === null || json.error === undefined) {
                           growl.addErrorMessage("Ocurrió un error en la petición de datos.");
                         } else {
                           growl.addErrorMessage(json.error);
                         }
                       } else {
                         if (json === undefined || json.resultados === undefined || json.resultados.lista === undefined) {
                           datos = {};
                         } else {
                           datos = json.resultados.lista;
                         }
                         var tbl = $("#datosTitulares > tbody");
                         $(tbl).html("");
                         oTable.fnClearTable();
                         var contador = 0;

                         var item = undefined;
                         for ( var k in datos) {

                           var idName = "check_" + contador;
                           var txtMarcar = '<input style= "width:20px" type="checkbox"  class=' + (contador) + '  id="'
                                           + idName + '" name="' + idName + ' class="editor-active"/>';

                           item = datos[k];
                           contador++;

                           // transformacion de los datos por
                           // sus descripciones.
                           //TODO falta poner este campo para que se visualice en el listado en vez del tipo
                           var desTipoIdMifid;
                           switch (item.tipoIdMifid) {
                             case 'L':
                            	 desTipoIdMifid = 'LEI';
                               break;
                             case 'P':
                            	 desTipoIdMifid = 'PASAPORTE';
                               break;
                             case 'C':
                            	 desTipoIdMifid = 'CONCAT';
                               break;
                             case 'I':
                            	 desTipoIdMifid = 'IDENTIF.NACIONAL';
                               break;
                             default:
                            	 desTipoIdMifid = '';
                               break;
                           }
                           
                           var desTipoPersona;
                           switch (item.tipoPersona) {
                             case 'F':
                               desTipoPersona = 'FÍSICA';
                               break;
                             case 'J':
                               desTipoPersona = 'JURÍDICA';
                               break;
                             default:
                               desTipoPersona = '';
                               break;
                           }

                           var desTipoTitular;
                           switch (item.tipoTitular) {
                             case 'T':
                               desTipoTitular = 'TITULAR';
                               break;
                             case 'R':
                               desTipoTitular = 'REPRESENTANTE';
                               break;
                             case 'U':
                               desTipoTitular = 'USUFRUCTURARIO';
                               break;
                             case 'N':
                               desTipoTitular = 'NUDOPROPIETARIO';
                               break;
                             default:
                               desTipoTitular = '';
                               break;
                           }

                           var desTipoDocumento;
                           switch (item.tipoDocumento) {
                             case 'N':
                               desTipoDocumento = 'NIF';
                               break;
                             case 'C':
                               desTipoDocumento = 'CIF';
                               break;
                             case 'E':
                               desTipoDocumento = 'NIE';
                               break;
                             case 'B':
                               desTipoDocumento = 'BIC';
                               break;
                             case 'O':
                               desTipoDocumento = 'OTROS';
                               break;
                             default:
                               desTipoDocumento = '';
                               break;
                           }

                           var desTipoNacionalidad;
                           switch (item.tipoNacionalidad) {
                             case 'N':
                               desTipoNacionalidad = 'NACIONAL';
                               break;
                             case 'E':
                               desTipoNacionalidad = 'EXTRANJERO';
                               break;
                             default:
                               desTipoNacionalidad = '';
                               break;
                           }
                           var rowIndex = oTable.fnAddData([ txtMarcar, item.documento, desTipoPersona, desTipoTitular,
                                                            desTipoDocumento, item.nombre, item.apellido1,
                                                            item.apellido2, item.fechaNacimiento, desTipoNacionalidad,
                                                            item.nacionalidad, item.tipoDireccion, item.domicilio,
                                                            item.ciudad, item.provincia, item.codigoPostal, item.paisResidencia, 
                                                            item.tipoIdMifid, item.idMifid, item.indClienteSan, item.numPersonaSan,
                                                            item.codNacionalidad, item.codPaisResidencia, item.numDomicilio,
                                                            item.tipoPersona, item.tipoTitular, item.tipoDocumento,
                                                            item.tipoNacionalidad  ], false);

                         }// for
                         oTable.fnDraw();
                       }

                       $.unblockUI();

                     }

                   } // onSuccessConsultaTitularesFiltroRequest

                   // FUNCIONALIDAD BOTONES GESTION DE TITULARES.
                   // Alta
                   $scope.crearTitular = function () {
                	   $scope.fnacVisible = true;
                     // Variable con el tipo de operacion
                     tipoOperacion = "crear";

                     // título de pantalla emergente
                     document.getElementById("titleTitulo").innerHTML = "Crear Final Holder";

                     // En la creacion de los nuevos registros
                     // Se habilita el documento que puede estar
                     // desabilitado
                     $("#documentoNuevo").prop('disabled', false);

                     // Se presenta el boton de vaciar todos los
                     // campos.
                     $("#limpiarDatosTitular").show();
                     // Se seleccionan los valores de la nacion a
                     // España por defecto
                     $("select#nacionalidadNuevo").val("724"); // Codigo
                     // nacionalidad
                     // española
                     $("select#paisresidenciaNuevo").val("011"); // Codigo
                     // pais
                     // españa

                     muestraDialogoDatosTitular();
                   }

                   // Modificar
                   $scope.modificarTitular = function () {
                     // Variable con el tipo de operacion
                     tipoOperacion = "modificar";

                     // título de pantalla emergente
                     document.getElementById("titleTitulo").innerHTML = "Modificar Final Holder";

                     console.log("Modificación de un final holder");

                     // Temporal se obtienen los elementos que se han
                     // checkeado.

                     $scope.listaModi = [];
                     var aData = oTable.fnGetData();
                     var input;
                     var idName;
                     var inputSel;

                     var filasSelecionadas = $("input:checked", oTable.fnGetNodes());

                     if (filasSelecionadas.length > 1) {
                       fErrorTxt("Solo se puede seleccionar un Final Holder para ser modificado.", 1);
                       return false;
                     }

                     if (filasSelecionadas.length == 0) {
                       fErrorTxt("Debe seleccionar al menos uno de ellos.", 2)
                       return;
                     }

                     // Se obtienen la fila seleccionada.
                     $("input:checked", oTable.fnGetNodes()).each(function () {
                       var numFila = this.id.replace('check_', '');
                       $scope.listaModi.push(aData[numFila]);

                     });

                     // Se cargan los datos del titular a modificar
                     cargaDatosTitular($scope.listaModi[0]);

                     // Se desabilita el documento porque es un valor
                     // que no van a poder modificar
                     $("#documentoNuevo").prop('disabled', true)

                     // Se oculta el boton de vaciar todos los
                     // campos.
                     $("#limpiarDatosTitular").hide();

                     muestraDialogoDatosTitular();
                   }

                   // Carga la pantalla modal con los datos del titular
                   function muestraDialogoDatosTitular () {

                     $("#formularios").attr('class', 'modal modalFinalHolder');
                     $("#datosTitular").css('display', 'block');
                     $('#formularios').css('display', 'block');
                     $('fade').css('display', 'block');

                   }

                   // cierra los formularios de creacion/modificacion
                   // reglas
                   $scope.cerrar = function () {
                     tipoOperacion = "";
                     $('#datosTitular').trigger("reset");
                     $('#formularios').css('display', 'none');
                     $('#datosTitular').css('display', 'none');
                     $('#mensajesError').css('display', 'none');
                   }

                   function inicializaDatosTitular () {
                     $scope.datosTitular.documento = $("#documentoNuevo").val();
                     $scope.datosTitular.tipopersona = $("#tipopersonaNuevo").val();
                     $scope.datosTitular.tipotitular = $("#tipotitularNuevo").val();
                     $scope.datosTitular.tipodocumento = $("#tipodocumentoNuevo").val();
                     $scope.datosTitular.fechanacimiento = $("#fechaNacimientoNuevo").val();

                     if($scope.fnacVisible){//O lo que es lo mismo, cuando tratamos con un tipo de persona Física
                    	$scope.datosTitular.nombre = $("#nombreNuevo").val();
                     	$scope.datosTitular.segundoapellido = $("#segundoapellidoNuevo").val();
                     }
                     else{
                    	 $scope.datosTitular.nombre = "";
                    	 $scope.datosTitular.segundoapellido = "";
                     }
                     
                     $scope.datosTitular.primerapellido = $("#primerapellidoNuevo").val();
                     $scope.datosTitular.tiponacionalidad = $("#tiponacionalidadNuevo").val();
                     $scope.datosTitular.codnacionalidad = $("#nacionalidadNuevo").val().trim();
                     $scope.datosTitular.tipodireccion = $("#tipodireccionNuevo").val();
                     $scope.datosTitular.domicilio = $("#domicilioNuevo").val();
                     $scope.datosTitular.numdomicilio = $("#numdomicilioNuevo").val();
                     $scope.datosTitular.ciudad = $("#ciudadNuevo").val();
                     $scope.datosTitular.provincia = $("#provinciaNuevo").val();
                     $scope.datosTitular.codpostal = $("#codpostalNuevo").val();
                     $scope.datosTitular.codpaisresidencia = $("#paisresidenciaNuevo").val().trim();
                     // CAMPOS MIFID
                     $scope.datosTitular.tipoIdMifid = $("#tipoIdMifidNuevo").val();
                     $scope.datosTitular.idMifid = $("#idMifidNuevo").val().trim();
                     $scope.datosTitular.indClienteSan = $("#indClienteSanNuevo").val();
                     $scope.datosTitular.numPersonaSan = $("#numPersonaSanNuevo").val().trim();
                   }

                   function cargaDatosTitular (datosTitularModificar) {
                	                         
                     $("#documentoNuevo").val(datosTitularModificar[1]);
                    
                     $("#nombreNuevo").val(datosTitularModificar[5]);
                     $("#primerapellidoNuevo").val(datosTitularModificar[6]);
                     $("#segundoapellidoNuevo").val(datosTitularModificar[7]);
                     $("#fechaNacimientoNuevo").val(datosTitularModificar[8]);
                     
                     
                     $("#tipodireccionNuevo").val(datosTitularModificar[11]);
                     $("#domicilioNuevo").val(datosTitularModificar[12]);
                     
                     $("#ciudadNuevo").val(datosTitularModificar[13]);
                     $("#provinciaNuevo").val(datosTitularModificar[14]);
                     $("#codpostalNuevo").val(datosTitularModificar[15]);
                     $("#nacionalidadNuevo").val(datosTitularModificar[21]);
                     $("#paisresidenciaNuevo").val(datosTitularModificar[22]);
                     $("#numdomicilioNuevo").val(datosTitularModificar[23]);
                     
                     // CAMPOS MIFID
                     $("#tipoIdMifidNuevo").val(datosTitularModificar[17]);
                     $("#idMifidNuevo").val(datosTitularModificar[18]);
                     $("#indClienteSanNuevo").val(datosTitularModificar[19]);
                     $("#numPersonaSanNuevo").val(datosTitularModificar[20]);
                     
                     
                     $("#tipopersonaNuevo").val(datosTitularModificar[24]);
                     $("#tipotitularNuevo").val(datosTitularModificar[25]);
                     $("#tipodocumentoNuevo").val(datosTitularModificar[26]);
                     $("#tiponacionalidadNuevo").val(datosTitularModificar[27]);
                   }
                   // Guardar los datos de un nuevo titular o modilfica
                   // los datos del titular existente.
                   // Primero realiza la validación de los datos.
                   $scope.guardarDatosTitular = function () {

                     // Se vacia la lista de errores y se oculta
                     $("select#listaErrores").empty();

                     // Se inicializan los datos del titular
                     inicializaDatosTitular();

                     // Insercion de un nuevo registro
                     if (tipoOperacion == "crear") {
                       MantenimientoTitularesService.CrearTitular(onSuccessCreaTitularRequest, onErrorRequest,
                                                                  $scope.datosTitular);
                     }

                     if (tipoOperacion == "modificar") {
                       MantenimientoTitularesService.ModificarTitular(onSuccessModificaTitularRequest, onErrorRequest,
                                                                      $scope.datosTitular);
                     }

                   }

                   // Resutado de la creacion de un nuevo registro
                   function onSuccessCreaTitularRequest (json) {
                     if (json.resultados.status === 'KO') {
                       // growl.addErrorMessage(json.error);
                       $scope.cerrar();
                       fErrorTxt(json.error, 1);
                     } else {

                       // Comprobacion de si la información tiene
                       // errores
                       var errores = json.resultados.listErrores;
                       var valor;

                       if (errores.length > 0) {
                         $('#mensajesError').css('display', 'block');
                         var select = $("select#listaErrores");

                         for (var i = 0, j = errores.length; i < j; i++) {
                           valor = errores[i];
                           select.append("<option>" + valor + "</option>");
                         }
                       } else { // Se realiza el insert o la
                         // modificacion del registor
                         $scope.cerrar();
                         fErrorTxt('Información registrada correctamente', 3);
                       }
                     }
                     $scope.safeApply();

                   }
                   // Resutado de la modificación de un registro
                   function onSuccessModificaTitularRequest (json) {
                     if (json.resultados.status === 'KO') {
                       $scope.cerrar();
                       fErrorTxt(json.error, 1);
                     } else { // Comprobacion de si la información
                       // tiene errores
                       var errores = json.resultados.listErrores;
                       var valor;
                       // Se comprueba si la validación de los
                       // datos a devuelto algun error
                       if (errores.length > 0) {
                         $('#mensajesError').css('display', 'block');
                         var select = $("select#listaErrores");

                         for (var i = 0, j = errores.length; i < j; i++) {
                           valor = errores[i];
                           select.append("<option>" + valor + "</option>");
                         }
                       } else {
                         $scope.cerrar();
                         cargaTitularesFiltro(); // Recarga los
                         // datos del
                         // grid
                         fErrorTxt('Información modificada correctamente', 3);

                       }
                     }
                   }

                   // Botón de eliminar final Holders.
                   $scope.borrarTitular = function () {

                     console.log("Eliminar final holders");

                     $scope.listaBorr = [];
                     var aData = oTable.fnGetData();

                     var prue1 = [];
                     var params = [];

                     var filasSelecionadas = $("input:checked", oTable.fnGetNodes());

                     if (filasSelecionadas.length == 0) {
                       fErrorTxt("Debe seleccionar al menos uno de ellos.", 2)
                       return;
                     }

                     // Se obtienen los check seleccionados.
                     $("input:checked", oTable.fnGetNodes()).each(function () {
                       var numFila = this.id.replace('check_', '');
                       $scope.listaBorr.push(aData[numFila]);

                     });

                     var paramIndi = null

                     for (var i = 0, j = $scope.listaBorr.length; i < j; i++) {
                       paramIndi = {
                         "codFinalHolder" : $scope.listaBorr[i][1]
                       };
                       params.push(paramIndi);
                     }

                     if ($scope.listaBorr.length == 1) {
                       angular.element("#dialog-confirm").html("¿Desea eliminar un final holder?");
                     } else {
                       angular.element("#dialog-confirm").html(
                                                               "¿Desea eliminar " + $scope.listaBorr.length
                                                                   + " final holders?");
                     }

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm")
                         .dialog(
                                 {
                                   resizable : false,
                                   modal : true,
                                   title : "Mensaje de Confirmación",
                                   height : 125,
                                   width : 360,
                                   buttons : {
                                     " Sí " : function () {
                                       if ($scope.listaBorr.length == 1) {
                                         MantenimientoTitularesService
                                             .ComprobarEliminarTitular(onSuccessComprobarEliminarTitular,
                                                                       onErrorRequest, params);
                                         $(this).dialog('close');
                                       } else {
                                         MantenimientoTitularesService.EliminarTitular(onSuccessEliminarTitular,
                                                                                       onErrorRequest, params);
                                         $(this).dialog('close');
                                       }

                                     },
                                     " No " : function () {
                                       $(this).dialog('close');
                                     }
                                   }
                                 });
                     $('.ui-dialog-titlebar-close').remove();

                   }

                   function onSuccessEliminarTitular (json) {

                     console.log(json.resultados);
                     if (json.resultados !== null && json.resultados !== undefined && json.resultados !== undefined) {
                       var datos = json.resultados;
                       fErrorTxt(json.resultados.EliminarTitulares, 3);

                     } else {

                       fErrorTxt(json.error, 2);
                     }

                     $scope.cerrar();
                     cargaTitularesFiltro(); // Recarga los datos del
                     // grid

                   }

                   function onSuccessComprobarEliminarTitular (json) {

                     if (json.resultados !== null && json.resultados !== undefined && json.resultados !== undefined) {
                       var mensaje = json.resultados.ComprobarEliminarTitular;
                       $scope.listaBorr = [];
                       var aData = oTable.fnGetData();

                       var params = [];

                       // Se obtienen los check seleccionados.
                       $("input:checked", oTable.fnGetNodes()).each(function () {
                         var numFila = this.id.replace('check_', '');
                         $scope.listaBorr.push(aData[numFila]);

                       });

                       var paramIndi = null

                       for (var i = 0, j = $scope.listaBorr.length; i < j; i++) {
                         paramIndi = {
                           "codFinalHolder" : $scope.listaBorr[i][1]
                         };
                         params.push(paramIndi);
                       }

                       if (mensaje === 'OK') {

                         MantenimientoTitularesService
                             .EliminarTitular(onSuccessEliminarTitular, onErrorRequest, params);
                         $(this).dialog('close');
                       } else {
                         angular.element("#dialog-confirm").html(mensaje);
                         // Define the Dialog and its properties.
                         angular.element("#dialog-confirm").dialog(
                                                                   {
                                                                     resizable : true,
                                                                     modal : true,
                                                                     title : "Mensaje de Confirmación",
                                                                     height : 125,
                                                                     width : 660,
                                                                     buttons : {
                                                                       " Sí " : function () {

                                                                         MantenimientoTitularesService
                                                                             .EliminarTitular(onSuccessEliminarTitular,
                                                                                              onErrorRequest, params);
                                                                         $(this).dialog('close');
                                                                       },
                                                                       " No " : function () {
                                                                         $(this).dialog('close');
                                                                       }
                                                                     }
                                                                   });
                         $('.ui-dialog-titlebar-close').remove();
                       }
                     } else {

                       fErrorTxt(json.error, 2);
                     }

                     $scope.cerrar();
                     cargaTitularesFiltro(); // Recarga los datos del
                     // grid

                   }

                   /* SECCION DE FUNCIONES Y TRATAMIENTOS GENERICOS */

                   /** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
                   function valueExistInList (lista, valor) {
                     var inList = false;
                     $(lista + ' option').each(function (index) {

                       if (this.value == valor) {
                         inList = true;
                         return inList;
                       }
                     });
                     return inList;
                   }

                   function getValuesSeleccionadosSelectMultiple (select) {
                     var values = "";
                     $("select#" + select).children('option:selected').each(function () {
                       values += $(this).val() + ";";
                     });
                     if (values != "") {
                       values = values.substr(0, values.length - 1);
                     }
                     return values;
                   }

                   function obtenerIdsSeleccionados (lista) {
                     var selected = ""
                     var i = 0;
                     $(lista + ' option').each(function () {
                       if (i > 0) {
                         selected += ";";
                       }
                       selected += $(this).val();
                       i++;
                     });
                     return selected;
                   }

                   function pupulateListaAutocomplete (input, idContainer, availableTags, lista) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {
                         if (!valueExistInList(lista, ui.item.value)) {
                           $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                           $(input).val("");
                           $(lista + "> option").attr('selected', 'selected');
                         }
                         return false;
                       }
                     });
                   }

                   function pupulateInputAutocomplete (input, availableTags) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {
                         $(input).val(ui.item.label);
                         return false;
                       }
                     });
                   }

                   // GESTION DE LOS BOTONES IGUAL E INCLUIDOS
                   $scope.btnInvTipoPersona = function () {
                     $scope.btnInvGenerico('TipoPersona', 1);
                   }

                   $scope.btnInvTipoTitular = function () {
                     $scope.btnInvGenerico('TipoTitular', 2);
                   }

                   $scope.btnInvTipoDocumento = function () {
                     $scope.btnInvGenerico('TipoDocumento', 2);
                   }

                   $scope.btnInvDocumento = function () {
                     $scope.btnInvGenerico('Documento', 1);
                   }

                   $scope.btnInvNombre = function () {
                     $scope.btnInvGenerico('Nombre', 1);
                   }

                   $scope.btnInvPrimerApellido = function () {
                     $scope.btnInvGenerico('PrimerApellido', 1);
                   }

                   $scope.btnInvSegundoApellido = function () {
                     $scope.btnInvGenerico('SegundoApellido', 1);
                   }

                   $scope.btnInvTipoNacionalidad = function () {
                     $scope.btnInvGenerico('TipoNacionalidad', 1);
                   }

                   $scope.btnInvNacionalidad = function () {
                     $scope.btnInvGenerico('Nacionalidad', 2);
                   }

                   $scope.btnInvTipoDireccion = function () {
                     $scope.btnInvGenerico('TipoDireccion', 1);
                   }

                   $scope.btnInvDireccion = function () {
                     $scope.btnInvGenerico('Direccion', 1);
                   }

                   $scope.btnInvCiudad = function () {
                     $scope.btnInvGenerico('Ciudad', 1);
                   }

                   $scope.btnInvProvincia = function () {
                     $scope.btnInvGenerico('Provincia', 1);
                   }

                   $scope.btnInvCodPostal = function () {
                     $scope.btnInvGenerico('CodPostal', 1);
                   }

                   $scope.btnInvPaisResidencia = function () {
                     $scope.btnInvGenerico('PaisResidencia', 2);
                   }

                   // FUNCION GENERICA TRATAMIENTO DEL BOTON IGUAL O
                   // INCLUIDO
                   $scope.btnInvGenerico = function (nombre, tipo) {

                     var check = '$scope.filtro.ch' + nombre.toLowerCase();

                     if (eval(check)) {
                       var valor2 = check + "= false";
                       eval(valor2);

                       document.getElementById('textBtnInv' + nombre).innerHTML = "<>";
                       angular.element('#textInv' + nombre).css({
                         "background-color" : "transparent",
                         "color" : "red"
                       });
                       if (tipo === 1)
                         document.getElementById('textInv' + nombre).innerHTML = "DISTINTO";
                       else
                         document.getElementById('textInv' + nombre).innerHTML = "NO INCLUIDOS";
                     } else {
                       var valor2 = check + "= true";
                       eval(valor2);

                       document.getElementById('textBtnInv' + nombre).innerHTML = "=";
                       angular.element('#textInv' + nombre).css({
                         "background-color" : "transparent",
                         "color" : "green"
                       });
                       if (tipo === 1)
                         document.getElementById('textInv' + nombre).innerHTML = "IGUAL";
                       else
                         document.getElementById('textInv' + nombre).innerHTML = "INCLUIDOS";
                     }
                   }

                   // Boton de marcar todos
                   $scope.selececionarTodosCheck = function () {

                     // Se obtienen los check seleccionados.
                     $("input:not(:checked)", oTable.fnGetNodes()).each(function () {
                       this.checked = true;

                     });

                   };

                   // Boton desmarcar todos
                   $scope.deSelececionarTodosCheck = function () {
                     $("input:checked", oTable.fnGetNodes()).each(function () {
                       this.checked = false;
                     });
                   };

                   function importarficheroExcel (Text, nombre) {
                     angular.element('#cargarFicheroCargaMasiva').modal('hide');
                     var filtro = {
                       file : Text,
                       filename : nombre
                     };
                     MantenimientoTitularesService.importarExcelCargaMasiva(onSuccessImportarRegla, onErrorRequest,
                                                                            filtro);
                   }

                   function onSuccessImportarRegla (json) {
                     console.log(json);
                     $.unblockUI();

                     if (json.resultados.fileErrorsCargaMasiva === undefined && json.resultados.fileErrors === "OK") {

                       fErrorTxt('La carga ha sido realizada con éxito', 3);

                     } else {
                       if (json.resultados.fileErrorsCargaMasiva !== undefined && json.resultados.fileErrors !== "OK") {

                         var ExcelBytes = atob([ json.resultados.fileErrorsCargaMasiva ]);

                         var byteNumbers = new Array(ExcelBytes.length);
                         for (var i = 0; i < ExcelBytes.length; i++) {
                           byteNumbers[i] = ExcelBytes.charCodeAt(i);
                         }

                         var byteArray = new Uint8Array(byteNumbers);

                         var blob = new Blob([ byteArray ], {
                           type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                         });

                         var nav = navigator.userAgent.toLowerCase();

                         if (navigator.msSaveBlob) {
                           navigator.msSaveBlob(blob, "Errores_Carga_Masiva_" + hoy + ".xlsx");
                         } else {
                           var blobUrl = URL.createObjectURL(blob);
                           var link = document.createElement('a');
                           link.href = blobUrl = URL.createObjectURL(blob);
                           link.download = "Errores_Carga_Masiva_" + hoy + ".xlsx";
                           document.body.appendChild(link);
                           link.click();
                           document.body.removeChild(link);
                         }

                         fErrorTxt(
                                   "La carga contenía registros con errores, por favor revise el fichero para poder corregirlos",
                                   2);
                       } else {

                         fErrorTxt("El fichero no corresponde con el formato adecuado para la importacion", 1);
                       }
                     }

                   }

                   $scope.importarFichero = function () {
                     angular.element('#cargarFicheroCargaMasiva').modal();
                     $(".modal-backdrop").remove();
                   }

                   $scope.cargarFichero = function () {

                     console.log('Entra en enviarFichero....');

                     var file = $scope.files[0];
                     console.log('file ..: ' + file);

                     if ($scope.files && file) {
                       var reader = new FileReader();
                       var btoaTxt;
                       var nombre = file.name;
                       var ruta = file.path;
                       reader.onload = function () {
                         var binary = "";
                         var bytes = new Uint8Array(reader.result);
                         var length = bytes.byteLength;

                         for (var i = 0; i < length; i++) {
                           binary += String.fromCharCode(bytes[i]);
                         }

                         btoaTxt = btoa(binary);

                         importarficheroExcel(btoaTxt, nombre);
                       };
                       reader.readAsArrayBuffer(file);
                     }
                     angular.element('#cargarFicheroCargaMasiva').modal('hide');
                     if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
                       fErrorTxt('The File APIs are not fully supported in this browser.', 1);
                     }
                     $('#selecFicheroCargaMasiva').val('');
                     inicializarLoading();
                   }

                   $('#selecFicheroCargaMasiva').change(function (event) {
                     console.log(this.files[0].mozFullPath);
                     event.preventDefault();
                     $scope.files = event.target.files;
                     $scope.cargarFichero();
                   });

                 } ]);
