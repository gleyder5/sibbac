'use strict';
sibbac20
    .controller('ContactosController',
                [
                 '$scope',
                 '$rootScope',
                 '$compile',
                 'growl',
                 'ContactosService',
                 'AliasService',
                 '$document',
                 'ngDialog',
                 '$location',
                 'SecurityService',
                 function ($scope, $rootScope, $compile, growl, ContactosService, AliasService, $document, ngDialog,
                           $location, SecurityService) {

                   var idOcultos = [];
                   var availableTags = [];
                   var oTable = undefined;
                   var SHOW_LOG = false;
                   var indiceTablaCargar = "";
                   var tablaOk = false;

                   $scope.needRefresh = false;

                   // filtros
                   $scope.filtro = {};
                   // filtro de alta
                   $scope.filtro.alta = {};
                   $scope.filtro.alta.textAlias = "";
                   $scope.filtro.alta.textNombre = "";
                   $scope.filtro.alta.textApel1 = "";
                   $scope.filtro.alta.textApel2 = "";
                   $scope.filtro.alta.textTelefono = "";
                   $scope.filtro.alta.textMovil = "";
                   $scope.filtro.alta.textFax = "";
                   $scope.filtro.alta.textEmail = "";
                   $scope.filtro.alta.textCargo = "";
                   $scope.filtro.alta.textGestor = false;

                   // filtro de busqueda
                   $scope.filtro.busqueda = {};
                   $scope.filtro.busqueda.textAliasB = "";

                   // filtro de modificacion
                   $scope.filtro.mod = {};
                   $scope.filtro.mod.textIdAlias = "";
                   $scope.filtro.mod.textIdContacto = "";
                   $scope.filtro.mod.textNombre1 = "";
                   $scope.filtro.mod.textApel11 = "";
                   $scope.filtro.mod.textApel21 = "";
                   $scope.filtro.mod.textTelefono1 = "";
                   $scope.filtro.mod.textFax1 = "";
                   $scope.filtro.mod.textEmail1 = "";
                   $scope.filtro.mod.textCargo1 = "";
                   $scope.filtro.mod.textMovil1 = "";
                   $scope.filtro.mod.chkActiv = false;
                   $scope.filtro.mod.chkDefecto = false;
                   $scope.filtro.mod.textGestor1 = false;

                   $scope.limpiarFiltro = function () {
                     $scope.filtro.alta.textAlias = "";
                     $scope.filtro.alta.textNombre = "";
                     $scope.filtro.alta.textApel1 = "";
                     $scope.filtro.alta.textApel2 = "";
                     $scope.filtro.alta.textTelefono = "";
                     $scope.filtro.alta.textMovil = "";
                     $scope.filtro.alta.textFax = "";
                     $scope.filtro.alta.textEmail = "";
                     $scope.filtro.alta.textCargo = "";
                     $scope.filtro.alta.textGestor = false;
                   };

                   function initializaTable () {
                     oTable = $("#tblContactos").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                       },
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       /* Disable initial sort */
                       "order" : [],
                       /* "bSort" : false, Deshabilita todos los sort */
                       "columns" : [ {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       }, {
                         "width" : "auto"
                       } ],
                       "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                         $compile(nRow)($scope);
                       },
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "filter" : false
                     });
                   }
                   ;

                   function showError (data, status, headers, config) {
                     $.unblockUI();
                     growl.addErrorMessage("Error: " + data);
                   }
                   ;

                   function successCambioStatus (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("cambios de estado ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.filtro.busqueda.textAliasB = indiceTablaCargar;
                         growl.addInfoMessage("Estado cambiado");
                         $scope.buscar();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error cambios de estado ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   $scope.cambioActivoContacto = function (id, activoContacto) {

                     var activo = true;
                     if (activoContacto === 'S' || activoContacto === 's') {
                       activo = 'n';
                     } else {
                       activo = 's';
                     }
                     var filtro = {
                       id : id,
                       activo : activo
                     };

                     angular.element("#dialog-confirm").html("¿Desea cambiar el estado del contacto?");

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm").dialog({
                       resizable : false,
                       modal : true,
                       title : "Mensaje de Confirmación",
                       height : 120,
                       width : 360,
                       buttons : {
                         "Sí" : function () {
                           $(this).dialog('close');
                           ContactosService.cambioStatus(successCambioStatus, showError, filtro);
                         },
                         "No" : function () {
                           $(this).dialog('close');
                         }
                       }
                     });

                   };

                   function openEditContactoForm () {
                     ngDialog.open({
                       template : 'template/contactos/editContacto.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-240',
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           $scope.needRefresh = false;
                           $scope.filtro.busqueda.textAliasB = indiceTablaCargar;
                           $scope.buscar();
                         }
                       }
                     });
                   }
                   ;

                   $scope.cargoInputsContacto = function (idAlias, id, nombr, apellido1, apellido2, telefono1, movil,
                                                          fax, activo, email, posicionCargo, defecto, gestor) {

                     $scope.dialogTitle = "Modificar contacto";

                     $scope.filtro.mod.textIdAlias = idAlias;
                     $scope.filtro.mod.textIdContacto = id;
                     $scope.filtro.mod.textNombre1 = nombr;
                     $scope.filtro.mod.textApel11 = apellido1;
                     $scope.filtro.mod.textApel21 = apellido2;
                     $scope.filtro.mod.textTelefono1 = telefono1;
                     $scope.filtro.mod.textFax1 = fax;
                     $scope.filtro.mod.textEmail1 = email;
                     $scope.filtro.mod.textCargo1 = posicionCargo;
                     $scope.filtro.mod.textMovil1 = movil;
                     $scope.filtro.mod.chkActiv = (activo === 's');
                     $scope.filtro.mod.chkDefecto = (defecto === 'true');
                     $scope.filtro.mod.textGestor1 = (gestor === 'true');

                     openEditContactoForm();
                     angular.element('#chkDefecto').disable = ($scope.filtro.mod.chkDefecto);
                   };

                   function successCargaTabla (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("listar contactos ...");

                     indiceTablaCargar = "";
                     tablaOk = false;
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         datos = json.resultados;
                         oTable.fnClearTable();
                         var cargo = "";
                         var fax = "";
                         // var row;
                         angular.forEach(datos, function (item, key) {
                           indiceTablaCargar = $scope.filtro.busqueda.textAliasB;
                           tablaOk = true;
                           var imagenactivo = "desactivo";
                           var titleactivo = "Activar";
                           if ((item.activo == "S") || (item.activo == "s")) {
                             imagenactivo = "activo";
                             titleactivo = "Desactivar";
                           }

                           var defEnlace1 = "<a class=\"btn\" ng-click=\"cambioActivoContacto('" + item.id + "','"
                                            + item.activo + "')\">";
                           var defEnlace2 = "</a>";

                           if ((item.defecto === true) && ((item.activo === "S") || (item.activo === "s"))) {
                             defEnlace1 = "";
                             defEnlace2 = "";
                           }
                           fax = (item.fax === null) ? "" : item.fax;
                           cargo = (item.posicionCargo === null) ? "" : item.fax;

                           // se controlan las acciones, se muestra el enlace cuando el usuario tiene el
                           // permiso.
                           var enlaceEditar = "";
                           var enlaceBorrar = "";
                           if (SecurityService.inicializated) {

                             if (SecurityService
                                 .isPermisoAccion($rootScope.SecurityActions.MODIFICAR, $location.path())) {
                               enlaceEditar = "<a class=\"btn\" ng-click = \"cargoInputsContacto('" + item.cdAlias
                                              + "','" + item.id + "','" + item.nombre + "','" + item.apellido1 + "','"
                                              + item.apellido2 + "','" + item.telefono1 + "','" + item.movil + "','"
                                              + fax + "','" + item.activo + "','" + item.email + "','" + cargo + "','"
                                              + item.defecto + "','" + item.gestor
                                              + "'); \"><img src='img/editp.png' title='Editar'/></a>";
                             }
                             if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR, $location.path())) {
                               enlaceBorrar = "<a class=\"btn\" ng-click=\"borrarContacto('" + item.id + "','"
                                              + item.nombre + " " + item.apellido1 + " " + item.apellido2
                                              + "');\"><img src='img/del.png'  title='Eliminar'/></a>";
                             }

                           }

                           //
                           oTable.fnAddData([
                                             item.cdAlias,
                                             item.nombre,
                                             item.apellido1,
                                             item.apellido2,
                                             item.telefono1,
                                             item.movil,
                                             fax,
                                             defEnlace1 + "<img src='img/" + imagenactivo + ".png'  title='"
                                                 + titleactivo + "'/>" + defEnlace2, item.email, cargo,
                                             enlaceEditar + "&nbsp;" + enlaceBorrar ], false);
                         });
                         oTable.fnDraw();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error listando contactos ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   function cargartabla (dato) {

                     var valorSeleccionadoAliasB = idOcultos[dato] || 0;
                     var filtro = {
                       cdaliass : valorSeleccionadoAliasB
                     };
                     ContactosService.getListaContactos(successCargaTabla, showError, filtro);
                   }
                   ;

                   $(document).ready(function () {
                     initializaTable();
                     inicializarLoading();
                     AliasService.getAlias().success(cargaAlias).error(showError);
                     var filtro = {
                       cdaliass : 0
                     };
                     ContactosService.getListaContactos(successCargaTabla, showError, filtro);
                   });

                   function ocultarMostrarAlta () {
                     var objDiv = angular.element("#IdMostrarOcultarAlta");
                     objDiv.parents('.title_section').next().slideToggle();
                     objDiv.parents('.title_section').next().next('.button_holder').slideToggle();
                     objDiv.toggleClass('active');
                     objDiv.toggleText('Mostrar Alta de Contactos', 'Ocultar Alta de Contactos');
                     return false;
                   }

                   function successAltaContacto (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("alta contacto ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         ocultarMostrarAlta();
                         $scope.limpiarFiltro();
                         growl.addInfoMessage("contacto dado de alta satisfactoriamente.");
                         if (tablaOk === true) {
                           $scope.filtro.busqueda.textAliasB = indiceTablaCargar;
                           $scope.buscar();
                         }
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("alta contacto ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   function crearContacto (email, valorSeleccionado, nombre, ape1) {
                     var nombre = $scope.filtro.alta.textNombre;
                     var ape1 = $scope.filtro.alta.textApel1;
                     var ape2 = $scope.filtro.alta.textApel2;
                     var telefono = $scope.filtro.alta.textTelefono;
                     var movil = $scope.filtro.alta.textMovil;
                     var fax = $scope.filtro.alta.textFax;
                     var cargo = $scope.filtro.alta.textCargo;
                     var gestor = $scope.filtro.alta.textGestor;

                     var gestor = ($scope.filtro.alta.textGestor === 'true');
                     var activo = "s";
                     var defecto = true;
                     var descripcion = "";
                     var telefono2 = "";
                     var comentarios = "";

                     var filtro = {
                       cdaliass : valorSeleccionado,
                       descripcion : descripcion,
                       telefono1 : telefono,
                       telefono2 : telefono2,
                       movil : movil,
                       fax : fax,
                       email : email,
                       comentarios : comentarios,
                       posicionCargo : cargo,
                       activo : activo,
                       nombre : nombre,
                       apellido1 : ape1,
                       apellido2 : ape2,
                       gestor : gestor,
                       defecto : defecto
                     }
                     console.log("alta contacto");
                     ContactosService.altaContacto(successAltaContacto, showError, filtro);
                   }
                   ;

                   $scope.altaContacto = function () {
                     var email = $scope.filtro.alta.textEmail;
                     var listaEmail = email.split(";");
                     var todosValidos = true;
                     for ( var k in listaEmail) {
                       var item = listaEmail[k].trim();
                       if (!validarEmail(item)) {
                         todosValidos = false;
                       }
                     }
                     var listaAlias = $scope.filtro.alta.textAlias;
                     var posicionAlias = availableTags.indexOf(listaAlias);
                     if (posicionAlias == "-1") {

                       listaAlias = angular.element('#textAlias').val();
                       posicionAlias = availableTags.indexOf(listaAlias);
                       if (posicionAlias == "-1") {
                         alert("El alias introducido no existe");
                         return false;
                       }
                     }
                     var valorSeleccionadoAlias = idOcultos[posicionAlias];
                     var nombre = $scope.filtro.alta.textNombre;
                     var ape1 = $scope.filtro.alta.textApel1;
                     if (nombre == "") {
                       alert("Debe escribir un nombre");
                       return false;
                     }
                     if (ape1 == "") {
                       alert("Debe escribir el primer apellido");
                       return false;
                     }
                     if (todosValidos) {
                       crearContacto(email, valorSeleccionadoAlias, nombre, ape1);
                     }
                     return false;

                   };

                   $scope.desactivarCheck = function () {

                     $scope.filtro.mod.chkDefecto = false;

                   };

                   $scope.buscar = function () {

                     var listaAliasB = $scope.filtro.busqueda.textAliasB;
                     var posicionAliasB = availableTags.indexOf(listaAliasB);
                     if (posicionAliasB == "-1") {

                       listaAliasB = angular.element('#textAliasB').val();
                       posicionAliasB = availableTags.indexOf(listaAliasB);
                       if (posicionAliasB == "-1") {
                         tablaOk = false;
                         alert("El alias introducido no existe");
                         return false;
                       }
                     }
                     cargartabla(posicionAliasB);
                   };

                   function successModificarContacto (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("modificar contacto ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.needRefresh = true;
                         ngDialog.closeAll();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error modificando contacto ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   $scope.modificarContacto = function () {

                     var activo = 's';
                     if ($scope.filtro.mod.chkActiv !== true) {
                       activo = 'n';
                     }

                     var idAlias = $scope.filtro.mod.textIdAlias;
                     var nombre = $scope.filtro.mod.textNombre1;
                     var apellido1 = $scope.filtro.mod.textApel11;
                     var apellido2 = $scope.filtro.mod.textApel21;
                     var telefono1 = $scope.filtro.mod.textTelefono1;
                     var telefonoMovil = $scope.filtro.mod.textMovil1;
                     var fax = $scope.filtro.mod.textFax1;
                     var email = $scope.filtro.mod.textEmail1;
                     var cargo = $scope.filtro.mod.textCargo1;
                     var chkDefecto = $scope.filtro.mod.defecto;
                     var gestor = $scope.filtro.mod.textGestor1;
                     var defecto = $scope.filtro.mod.chkDefecto;
                     var textIdContacto = $scope.filtro.mod.textIdContacto;

                     var descripcion = "";
                     var telefono2 = "";
                     var comentarios = "";

                     var listaEmail = email.split(";");

                     var todosValidos = true;
                     for ( var k in listaEmail) {
                       var item = listaEmail[k].trim();
                       if (!validarEmail(item)) {
                         todosValidos = false;
                       }
                     }

                     if (todosValidos) {
                       var filtro = {
                         id : textIdContacto,
                         descripcion : descripcion,
                         telefono1 : telefono1,
                         telefono2 : telefono2,
                         movil : telefonoMovil,
                         fax : fax,
                         email : email,
                         comentarios : comentarios,
                         posicionCargo : cargo,
                         activo : activo,
                         nombre : nombre,
                         apellido1 : apellido1,
                         apellido2 : apellido2,
                         gestor : gestor,
                         defecto : defecto
                       };

                       ContactosService.modificacionContacto(successModificarContacto, showError, filtro);
                     } else {
                       return false;
                     }
                   };

                   function cargaAlias (data, status, headers, config) {
                     var availableTagsAlias = [];
                     angular.forEach(data.resultados.result_alias, function (val, key) {
                       idOcultos.push(val.alias);
                       var ponerTag = val.alias + " - " + val.descripcion;
                       availableTags.push(ponerTag);
                     });
                     // código de autocompletar
                     angular.element("#textAlias").autocomplete({
                       source : availableTags
                     });
                     angular.element("#textAliasB").autocomplete({
                       source : availableTags
                     });
                     $.unblockUI();
                   }
                   ;

                   function successBajaContacto (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("baja contacto ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.filtro.busqueda.textAliasB = indiceTablaCargar;
                         growl.addInfoMessage("contacto dado de baja satisfactoriamente.");
                         $scope.buscar();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error baja contacto ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   $scope.borrarContacto = function (id, nombre) {
                     var filtro = {
                       id : id
                     };
                     angular.element("#dialog-confirm").html("¿Desea borrar realmente el contacto " + nombre + "?");

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm").dialog({
                       resizable : false,
                       modal : true,
                       title : "Mensaje de Confirmación",
                       height : 120,
                       width : 540,
                       buttons : {
                         "Sí" : function () {
                           $(this).dialog('close');
                           ContactosService.bajaContacto(successBajaContacto, showError, filtro);
                         },
                         "No" : function () {
                           $(this).dialog('close');
                         }
                       }
                     });

                   };

                   $('.collapser').click(function () {
                     $(this).parents('.title_section').next().slideToggle();
                     // $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                     $(this).toggleClass('active');
                     $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

                     return false;
                   });

                   $('.collapser_search').click(function () {
                     ocultarMostrarAlta();
                     return false;
                   });

                 } ]);
