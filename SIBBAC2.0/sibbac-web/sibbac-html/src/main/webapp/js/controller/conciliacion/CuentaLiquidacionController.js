'use strict';
sibbac20
    .controller(
                'CuentaLiquidacionController',
                [
                 '$scope',
                 '$rootScope',
                 '$compile',
                 '$document',
                 'ngDialog',
                 'growl',
                 'CuentaLiquidacionService',
                 'EntidadRegistroService',
                 'blockUI',
                 '$location',
                 'SecurityService',
                 function ($scope, $rootScope, $compile, $document, ngDialog, growl, CuentaLiquidacionService,
                           EntidadRegistroService, blockUI, $location, SecurityService) {
                   //
                   var oTable = undefined;
                   $scope.cuentasList = [];
                   $scope.tiposER = [];
                   $scope.tiposCuenta = [];
                   $scope.tiposCuentaCompensacion = [];
                   $scope.tiposNeteoTitulos = [];
                   $scope.tiposNeteoEfectivo = [];
                   $scope.tiposFichero = [];
                   $scope.sistemasLiquidacion = {};
                   $scope.cuentasMercados = [];
                   $scope.cuentasLiquidacion = {};
                   $scope.entidadesDeRegistro = {};
                   $scope.currentCuentaLiquidacion = new CuentaLiquidacion();
                   // dialog
                   $scope.btnData = "alta";
                   $scope.dialogTitle = "Alta Cuenta Espejo";
                   $scope.selectedMkt = new CuentaMercado();
                   $scope.needRefresh = false;
                   $scope.dialogSuccessAction = false;
                   $scope.dialogErrorAction = false;
                   $scope.successMsgText = "";
                   $scope.errorMsgText = "";
                   $scope.dialogSavingAction = false;
                   //
                   $scope.searchFollow = "";
                   // end
                   // Filtro Búsqueda
                   $scope.filtro = {
                     entidadRegistro : new EntidadRegistro(),
                     tipoFicheroConciliacion : "",
                     idMercado : "",
                     numCuentaER : "",
                     tipoCuentaConciliacion : new TipoCuentaConciliacion(),
                     tipoNeteoTitulo : new TipoNeteoTitulo(),
                     tipoNeteoEfectivo : new TipoNeteoEfectivo(),
                     idsCuentaCompensacion : "",
                     cdCodigo : "",
                     frecuenciaEnvioExtracto : "",
                     cuentaIberclear : ""
                   };
                   // end
                   function onConsultarSuccessHandler (json, status, headers, config) {
                     var datos = undefined;
                     if (json === undefined || json.resultados === undefined
                         || json.resultados.result_cuentas_liquidacion === undefined) {
                       datos = {};
                     } else {
                       datos = json.resultados.result_cuentas_liquidacion;
                       $scope.cuentasList = datos;
                     }
                     var difClass = 'centrado';
                     oTable.fnClearTable();
                     var frecuencia = 0;
                     var cuentasCompensacion = "";
                     var entidadRegistro = "";
                     angular.forEach(datos,
                                     function (val, key) {
                                       entidadRegistro = '<span data="' + val.entidadRegistro.id + '">'
                                                         + val.entidadRegistro.nombre + '</span>';
                                       frecuencia = val.frecuenciaEnvioExtracto;
                                       cuentasCompensacion = "<ul class=\"inner-table-ul\">";
                                       angular.forEach(val.cuentaDeCompensacionList, function (val, key) {
                                         cuentasCompensacion += '<li id="' + val.idCuentaCompensacion + '">'
                                                                + val.cdCodigo + '</li>';
                                       });
                                       cuentasCompensacion += "</ul>";

                                       // se controlan las acciones, se muestra el enlace cuando el usuario tiene el
                                        // permiso.
                                       var modCta = "";
                                       if (SecurityService.inicializated) {
                                         if (SecurityService.isPermisoAccion($rootScope.SecurityActions.MODIFICAR,
                                                                             $location.path())) {
                                           modCta = '<input type="image" data="' + val.id
                                                    + '" src="img/editp.png" ng-click="modificarCuenta(' + val.id
                                                    + ')" />';
                                         }
                                       }

                                       oTable.fnAddData([
                                                         modCta,
                                                         entidadRegistro,
                                                         val.tipoER.nombre,
                                                         addMercados(val.relacionCuentaMercadoDataList, "mercados_"
                                                                                                        + key),
                                                         val.numCuentaER,
                                                         val.tipoCuentaConciliacion.name,
                                                         val.tipoNeteoTitulo.nombre,
                                                         val.tipoNeteoEfectivo.nombre,
                                                         val.cdCodigo,
                                                         cuentasCompensacion,
                                                         frecuencia,
                                                         val.tipoFicheroConciliacion,
                                                         '<span id="' + val.sistemaLiquidacion.id + '">'
                                                             + val.sistemaLiquidacion.codigo + '</span>',
                                                         val.cuentaIberclear ], false);
                                     });
                     oTable.fnDraw();
                     blockUI.stop();
                   }
                   function onRequestErrorHandler (err) {
                     growl.addErrorMessage("Ocurrió un error durante la petición de datos al servidor " + err);
                   }
                   function consultar (params) {
                     CuentaLiquidacionService.getCuentaLiquidacion(onConsultarSuccessHandler, onRequestErrorHandler,
                                                                   params);
                   }
                   function addMercados (datos, id) {
                     // var list = "<select class=\"inner-select\" id=\""+id+"\"
                     // multiple=\"multiple\" >";
                     var list = '<ul class="inner-table-ul" id="' + id + '" >';
                     var valor;
                     var texto;
                     angular.forEach(datos, function (val, key) {
                       valor = val.idMkt;
                       texto = val.codMkt;
                       // list +='<option value="'+valor+'">'+texto+
                       // '</option>';
                       list += '<li value="' + valor + '" >' + texto + '</li>';
                     });
                     list += "</ul>";
                     // list +="</select>";
                     return list;
                   }
                   // obtiene el id de entidad de registro seleccionada
                   function getIdEntidadRegistro () {
                     var er = $("#ER").val();
                     if (er === undefined) {
                       return "";
                     }
                     er = er.toLowerCase();
                     if (er.trim().length > 0 && er.indexOf("tipo") !== -1) {
                       er = er.substring(0, er.indexOf("tipo") - 1);
                     }
                     return er;
                   }
                   function obtenerIdsSeleccionados (lista) {
                     var selected = "";
                     var i = 0;
                     angular.element(lista + ' option').each(function () {
                       if (i > 0) {
                         selected += ", ";
                       }
                       selected += $(this).val();
                       i++;
                     });
                     return selected;
                   }
                   // ///////////////////////////////////////////////
                   // ////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
                   function initialize () {

                     cargaCombos();
                     obtenerDatos();
                     consultar('');
                     // autocomplete multiple
                     CuentaLiquidacionService.getCuentasMercado(onRequestCuentasMercadosSuccess, onRequestErrorHandler,
                                                                {});
                     CuentaLiquidacionService.getCuentaRelacionada(onRequestCuentasRelacionadasSuccess,
                                                                   onRequestErrorHandler, {});
                   }
                   function onRequestCuentasMercadosSuccess (json, status, headers, config) {
                     var availableTags = [];
                     var datos = undefined;
                     if (json === undefined || json.resultados === undefined
                         || json.resultados.result_cuentas_mercados === undefined) {
                       datos = {};
                     } else {
                       datos = json.resultados.result_cuentas_mercados;
                       $scope.cuentasMercados = datos;
                     }
                     var obj;
                     angular.forEach(datos, function (val, key) {
                       obj = {
                         label : val.codigo + " - " + val.descripcion,
                         id : val.id
                       };
                       availableTags.push(obj);
                     });
                     populateMCAComponents("#MCA", "#selectedMCA", "#btnDelMCA", availableTags);
                     populateMCAComponents("#alta_MCA", "#alta_selectedMCA", "#alta_btnDelMCA", availableTags);
                   }
                   /**
                     * Para cargar el autocomplete de los mercados disponibles
                     *
                     * @param {componente}
                     *          input
                     * @param {array}
                     *          lista
                     * @param {button}
                     *          boton
                     * @param {array}
                     *          availableTags
                     * @returns {void}
                     */
                   function populateMCAComponents (input, lista, boton, availableTags) {
                     $(input).autocomplete({
                       source : availableTags,
                       minLengh : 0,
                       maxLength : 10,
                       focus : function (event, ui) {
                         // $("#MCA" ).val(ui.item.label);
                         return false;
                       },
                       select : function (event, ui) {
                         if (!valueExistInList(lista, ui.item.id)) {
                           $(lista).append("<option value=\"" + ui.item.id + "\" >" + ui.item.label + "</option>");
                           $(input).val("");
                           $(lista + "> option").attr('selected', 'selected');
                         }
                         return false;
                       }
                     });

                     // preparar boton para eliminar filas de mercados selecccionados
                     angular.element(boton).on("click", function (event) {
                       event.preventDefault();
                       angular.element(lista).find('option:selected').remove().end();
                       angular.element(lista + "> option").attr('selected', 'selected');
                     });
                   }
                   /** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
                   function valueExistInList (lista, valor) {
                     var inList = false;

                     $(lista + ' option').each(function (index) {

                       if (this.value == valor) {
                         inList = true;
                         return inList;
                       }
                     });

                     return inList;
                   }
                   function onRequestCuentasRelacionadasSuccess (json, status, headers, config) {
                     var availableTags = [];
                     var datos = undefined;
                     if (json === undefined || json.resultados === undefined
                         || json.resultados.result_cuentas_compensacion === undefined) {
                       datos = {};
                     } else {
                       datos = json.resultados.result_cuentas_compensacion;
                     }
                     var obj;
                     $(datos).each(function (key, val) {
                       obj = {
                         label : val.cdCodigo,
                         value : val.idCuentaCompensacion
                       };
                       availableTags.push(obj);
                     });
                     angular.element("#btnDelCC").click(function (event) {
                       event.preventDefault();
                       $("#selectedCC").find("option:selected").remove().end();
                     });
                     pupulateCuentaRelacionadaAutocomplete("#cuentasrelacionadas", "#idCtaComp", availableTags,
                                                           "#selectedCC");
                     // DE MOMENTO NO SE VAN A MODIFICAR LAS CUENTAS DE COMPENSACION
                     // DESDE EL ALTA
                     // pupulateCuentaRelacionadaAutocomplete("#alta_cuentasrelacionadas","#alta_idCtaComp",
                     // availableTags, "#alta_selectedCC");
                   }

                   function pupulateCuentaRelacionadaAutocomplete (input, idContainer, availableTags, lista) {
                     $(input).autocomplete(
                                           {
                                             minLength : 0,
                                             source : availableTags,
                                             focus : function (event, ui) {
                                               // $( this ).val( ui.item.label );
                                               return false;
                                             },
                                             select : function (event, ui) {
                                               // $(idContainer).val(ui.item.value);
                                               if (!valueExistInList(lista, ui.item.value)) {
                                                 angular.element(lista).append(
                                                                               "<option value=\"" + ui.item.value
                                                                                   + "\" >" + ui.item.label
                                                                                   + "</option>");
                                                 angular.element(input).val("");
                                                 angular.element(lista + "> option").attr('selected', 'selected');
                                               }
                                               return false;
                                             }
                                           });

                   }
                   function loadDataTable () {
                     oTable = angular.element("#tblCuentaLiquidacion").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "js/swf/copy_csv_xls_pdf.swf"
                       },
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "aoColumns" : [ {
                         "sClass" : "centrado",
                         "bSortable" : "false"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "align-right",
                         "sType" : "numeric"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "align-center"
                       }, {
                         "sClass" : "align-right"
                       } ],
                       "sort" : [ 1 ],
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                         $compile(nRow)($scope);
                       }
                     });
                   }
                   $document.ready(function () {
                     blockUI.start();
                     loadDataTable();
                     initialize();
                     prepareCollapsion();
                     angular.element('#cargarTabla').submit(function (event) {
                       event.preventDefault();
                       obtenerDatos();
                       collapseSearchForm();
                       seguimientoBusqueda();
                       return false;
                     });

                     $('#limpiar').click(function (event) {
                       event.preventDefault();
                       $('input[type=text]').val('');
                       $('select').val('0');
                     });

                   });

                   function seguimientoBusqueda () {
                     $scope.searchFollow = "";

                     if ($scope.filtro.entidadRegistro.id !== "") {
                       $scope.searchFollow += " Entidad de registro: " + $scope.filtro.entidadRegistro.id;
                     }
                     if ($scope.filtro.idMercado !== "") {
                       $scope.searchFollow += " Mercados asociados: " + $scope.filtro.idMercado;
                     }
                     if ($scope.filtro.numCuentaER !== "") {
                       $scope.searchFollow += " Cuenta: " + $scope.filtro.numCuentaER;
                     }
                     if ($scope.filtro.tipoCuentaConciliacion !== "") {
                       $scope.searchFollow += " Tipo Cuenta: " + $scope.filtro.tipoCuentaConciliacion;
                     }
                     if ($scope.filtro.tipoNeteoEfectivo !== "") {
                       $scope.searchFollow += " Tipo Neteo Efectivo: " + $scope.filtro.tipoNeteoEfectivo;
                     }
                     if ($scope.filtro.tipoNeteoTitulo !== "") {
                       $scope.searchFollow += " Tipo Neteo Títulos: " + $scope.filtro.tipoNeteoTitulo;
                     }
                     if ($scope.filtro.frecuenciaEnvioExtracto !== "") {
                       $scope.searchFollow += " Frecuencia Env&iacute;o Extracto: "
                                              + $scope.filtro.frecuenciaEnvioExtracto;
                     }
                     if ($scope.filtro.tipoFicheroConciliacion !== "") {
                       $scope.searchFollow += " Tipo Fichero: " + $scope.filtro.tipoFicheroConciliacion;
                     }
                     if ($('#cuentas_relacionadas').val() !== "" && $('#cuentas_relacionadas').val() !== undefined) {
                       $scope.searchFollow += " Tipo Fichero: " + $('#cuentas_relacionadas').val();
                     }
                     if ($scope.filtro.cuentaIberclear !== "") {
                       $scope.searchFollow += " Cuenta Iberclear: " + $scope.filtro.cuentaIberclear;
                     }

                   }

                   function obtenerDatos () {
                     if (!blockUI.isBlocking) {
                       blockUI.start();
                     }
                     var params = {
                       "entidadRegistro.id" : $scope.filtro.entidadRegistro.id,
                       "tipoFicheroConciliacion" : $scope.filtro.tipoFicheroConciliacion,
                       "idMercado" : obtenerIdsSeleccionados("#selectedMCA"),
                       "numCuentaER" : $scope.filtro.numCuentaER,
                       "tipoCuentaConciliacion.id" : $scope.filtro.tipoCuentaConciliacion.id,
                       "tipoNeteoTitulo.id" : $scope.filtro.tipoNeteoTitulo.id,
                       "tipoNeteoEfectivo.id" : $scope.filtro.tipoNeteoEfectivo.id,
                       "idsCuentaCompensacion" : obtenerIdsSeleccionados("#selectedCC"),
                       "cdCodigo" : $scope.filtro.cdcamara,
                       "frecuenciaEnvioExtracto" : $scope.filtro.frecuenciaEnvioExtracto,
                       "cuentaIberclear" : $scope.filtro.cuentaIberclear
                     };
                     consultar(params);

                   }

                   // llamada para cargar datos combos
                   function cargaCombos () {
                     // Cargar tipo cuenta
                     CuentaLiquidacionService.getTipoCuenta(onRequestDatosComboSuccess, onRequestErrorHandler, {},
                                                            'tipo_cuenta');
                     CuentaLiquidacionService.getTipoNeteo(onRequestDatosComboSuccess, onRequestErrorHandler, {},
                                                           'tipo_neteo_titulos');
                     CuentaLiquidacionService.getTipoNeteo(onRequestDatosComboSuccess, onRequestErrorHandler, {},
                                                           'tipo_neteo_efectivo');
                     CuentaLiquidacionService.getTipoFichero(onRequestDatosComboSuccess, onRequestErrorHandler, {},
                                                             'tipo_fichero');
                     CuentaLiquidacionService.getSistemasLiquidacion(onRequestDatosComboSuccess, onRequestErrorHandler,
                                                                     {}, 'sistemas_liq');
                     EntidadRegistroService.getAllEntidadRegistro(onRequestEntidadesRegistroSuccess,
                                                                  onRequestErrorHandler, {});

                   }
                   function onRequestEntidadesRegistroSuccess (json) {
                     var datos = undefined;
                     if (json !== undefined && json.resultados !== null && json.resultados !== undefined) {
                       if (json.resultados.result_entidades_de_registro !== null
                           && json.resultados.result_entidades_de_registro !== undefined) {
                         datos = json.resultados.result_entidades_de_registro;
                         $scope.entidadesDeRegistro = datos;
                         var tags = new Array(datos.length);
                         var tag;
                         $(datos).each(function (key, val) {
                           tag = {
                             label : val.nombre + " - " + val.tipoer.nombre,
                             value : val.id,
                             tipoId : val.tipoer.id,
                             tipoName : val.tipoer.nombre
                           };
                           tags[key] = tag;
                         });
                         // si se borra la entidad de registro del campo, que tambien
                         // se borre el id almacenado en el imput hidden
                         $("#ER").change(function () {
                           if ($(this).val() === "") {
                             $scope.filtro.entidadRegistro.id = "";
                           }
                         });
                         loadAutocompleteER("#ER", "#TER", tags);
                       } else {
                         return false;
                       }
                     }
                   }

                   function loadAutocompleteER (idER, idTER, tags) {
                     $(idER).autocomplete({
                       source : tags,
                       focus : function (event, ui) {
                         $(this).val(ui.item.label);
                         return false;
                       },
                       select : function (event, ui) {
                         $scope.filtro.entidadRegistro.id = ui.item.value;
                         $(idTER).val(ui.item.tipoId);
                         return false;
                       }
                     });
                   }
                   // mete en tiposNeteoTitulos todos los tipos excepto el nulo
                   function prepararTiposNeteoTitulos (datos) {
                     $scope.tiposNeteoTitulos = [];
                     angular.forEach(datos, function (val, key) {
                       if (val.name.toLowerCase() != "nula") {
                         $scope.tiposNeteoTitulos.push(val)
                       }
                     });

                   }
                   function onRequestDatosComboSuccess (json, id) {
                     var datos = undefined;
                     if (json !== undefined && json.resultados !== undefined) {
                       if (json.resultados.result_tipo_cuenta !== undefined) {
                         $scope.tiposCuenta = json.resultados.result_tipo_cuenta;
                         return false;
                       }
                       if (json.resultados.result_tipo_Neteo !== undefined) {
                         datos = json.resultados.result_tipo_Neteo;

                         prepararTiposNeteoTitulos(datos);
                         $scope.tiposNeteoEfectivo = datos;
                         return false;
                       }
                       if (json.resultados.result_tipo_fichero !== undefined) {
                         datos = json.resultados.result_tipo_fichero;
                         $scope.tiposFichero = datos;
                       }
                       if (json.resultados.result_tipos_entidad_registro !== undefined) {
                         datos = json.resultados.result_tipos_entidad_registro;
                         $scope.tiposER = datos;
                       }
                       if (json.resultados.result_sistemas_liquidacion !== undefined) {
                         datos = json.resultados.result_sistemas_liquidacion;
                         $scope.sistemasLiquidacion = datos;
                       }
                       if (datos === undefined) {
                         return;
                       } else {
                         // $.each(datos, function (key, val) {
                         var idData;
                         var nombre;

                         for (var i = 0; i < datos.length; i++) {
                           idData = (datos[i].id !== undefined) ? datos[i].id : datos[i];
                           if (id === 'sistemas_liq') {
                             nombre = (datos[i].codigo !== undefined) ? datos[i].codigo
                                                                     : ((datos[i].codigo !== undefined) ? datos[i].codigo
                                                                                                       : datos[i]);
                           } else {
                             nombre = (datos[i].nombre !== undefined) ? datos[i].nombre
                                                                     : ((datos[i].name !== undefined) ? datos[i].name
                                                                                                     : datos[i]);
                           }

                         }
                         // });
                       }
                     }
                   }

                   $scope.modificarCuenta = function (id) {

                     if (id == 0) {
                       $scope.btnData = "alta";
                       $scope.dialogTitle = "Alta Cuenta Liquidacion";
                       $scope.currentCuentaLiquidacion = new CuentaLiquidacion();
                     } else {
                       // cargarInput(id);
                       $scope.currentCuentaLiquidacion = getSelectedCuenta(id);
                       $scope.btnData = "modificar";
                       $scope.dialogTitle = "Modificar Cuenta Liquidacion";

                     }
                     ngDialog.open({
                       template : 'template/configuracion/cuenta-de-liquidacion-template.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-480',
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           obtenerDatos();
                           $scope.needRefresh = false;
                           $scope.dialogErrorAction = false;
                           $scope.dialogSuccessAction = false;
                         }
                       },
                       controller : [ '$scope', '$document', 'growl', function ($scope, $document, growl) {
                         $document.ready(function () {

                         });
                       } ]
                     });

                   };

                   function limpiarFormularioAlta () {
                     $scope.currentCuentaLiquidacion = new CuentaLiquidacion();
                     $scope.selectedMkt.codigo = "";
                   }
                   function getSelectedCuenta (id) {
                     var cuenta = new CuentaLiquidacion();
                     angular.forEach($scope.cuentasList, function (val, key) {
                       if (id == val.id) {
                         cuenta = val;
                         return false;
                       }
                     });
                     return cuenta;
                   }

                   $scope.saveCuentaLiquidacion = function (event) {
                     angular.element("#infoMsgBox").show();

                     var ER = $scope.currentCuentaLiquidacion.entidadRegistro.id;

                     var MCA = obtenerIdsSeleccionados("#alta_selectedMCA");
                     var numero_cuenta = $scope.currentCuentaLiquidacion.numCuentaER;
                     var tipo_cuenta = $scope.currentCuentaLiquidacion.tipoCuentaConciliacion.id;
                     var tipo_neteo_titulos = $scope.currentCuentaLiquidacion.tipoNeteoTitulo.id;
                     var tipo_neteo_efectivo = $scope.currentCuentaLiquidacion.tipoNeteoEfectivo.id;
                     var cuentaIberclear = $scope.currentCuentaLiquidacion.cuentaIberclear;
                     /*
                       * var cuentas_relacionadas = obtenerIdsSeleccionados("#alta_selectedCC");
                       */
                     var cdCodigo = $scope.currentCuentaLiquidacion.cdCodigo;
                     var FEE = $scope.currentCuentaLiquidacion.frecuenciaEnvioExtracto;
                     var tipo_fichero = $scope.currentCuentaLiquidacion.tipoFicheroConciliacion;

                     // do validar
                     var filtro = {
                       "entidadRegistro" : ER,
                       /*-"tipoER" : TER,*/
                       "idMercado" : MCA,
                       "numCuentaER" : numero_cuenta,
                       "esCuentaPropia" : tipo_cuenta,
                       "tipoNeteoTitulo" : tipo_neteo_titulos,
                       "tipoNeteoEfectivo" : tipo_neteo_efectivo,
                       "cdCodigo" : cdCodigo,
                       /* "idCtaCompensacion" : cuentas_relacionadas , */
                       "frecuenciaEnvioExtracto" : FEE,
                       "idCuentaLiquidacion" : $scope.currentCuentaLiquidacion.id,
                       "tipoFicheroConciliacion" : tipo_fichero,
                       "idSistemaLiquidacion" : $scope.currentCuentaLiquidacion.sistemaLiquidacion.id,
                       "cuentaIberclear" : cuentaIberclear
                     };
                     var data = new DataBean();
                     data.setService('SIBBACServiceCuentaLiquidacion');
                     // if ($("#altaparam").attr('data')==='alta'){
                     data.setAction('addCuentaLiquidacion');
                     // }
                     // else if ($("#altaparam").attr('data')==='modificar'){
                     // data.setAction('updateCuentaLiquidacion');
                     // }
                     data.setFilters(JSON.stringify(filtro));
                     var request = requestSIBBAC(data);
                     request.error(function (err) {
                       if (blockUI.isBlocking) {
                         blockUI.stop();
                       }
                       growl.addErrorMessage(err);
                       angular.element("#infoMsgBox").hide();
                     });
                     request.success(function (json) {
                       var msg = "";
                       var error = false;
                       $scope.dialogSavingAction = false;
                       // alert(json.request.filters.diaMes);
                       if (angular.element('div#formularioalta input.mybutton').attr('data') === 'modificar') {
                         msg = "Los datos de la cuenta de liquidación, fueron guardados correctamente.";
                         error = false;
                         if (json.error !== null && json.error !== "") {
                           error = true;
                           msg = 'Ocurrió un error inesperado al modificar los datos err: ' + json.error;
                         }

                       } else {
                         msg = "Cuenta de Liquidación añadida correctamente.";
                         error = false;
                         if (json.error !== null && json.error !== "") {
                           msg = 'Ocurrió un error inesperado al dar de alta la nueva cuenta err: ' + json.error;
                           error = true;
                         } else {
                           limpiarFormularioAlta();
                         }
                       }
                       afterSaveDataCallback(msg, error);

                     });
                   };
                   function afterSaveDataCallback (msg, error) {
                     closeAllMsgBox();
                     if (error) {
                       growl.addErrorMessage(msg);
                       $scope.errorMsgText = msg;
                     } else {
                       growl.addSuccessMessage(msg);
                       $scope.successMsgText = msg;
                     }
                     $scope.needRefresh = true;
                     if (error) {
                       angular.element("#errorMsgBox").show();
                     } else {
                       angular.element("#successMsgBox").show();
                     }
                   }
                   function closeAllMsgBox () {
                     angular.element("#infoMsgBox").hide();
                     angular.element("#errorMsgBox").hide();
                     angular.element("#successMsgBox").hide();
                   }
                   $scope.selectedER = function (selected) {
                     if (selected !== undefined && selected.originalObject !== undefined) {
                       $scope.currentCuentaLiquidacion.entidadRegistro = selected.originalObject;
                     }
                   };
                   $scope.selectedMCA = {
                     id : ""
                   };
                   $scope.removeSelectedMCA = function (event) {
                     event.preventDefault();
                     if ($scope.selectedMCA.id === "" || $scope.selectedMCA.id === undefined) {
                       return false;
                     }
                     var aux = [];
                     angular.forEach($scope.currentCuentaLiquidacion.relacionCuentaMercadoDataList,
                                     function (mkt, key) {
                                       if (mkt.idMkt != $scope.selectedMCA.id) {
                                         aux.push(mkt);
                                       }
                                     });
                     $scope.currentCuentaLiquidacion.relacionCuentaMercadoDataList = [];
                     $scope.currentCuentaLiquidacion.relacionCuentaMercadoDataList = aux;
                     return false;
                   };

                   $scope.selectedCuentaMercado = function (selected) {
                     if (selected === undefined || selected.originalObject === undefined) {
                       return false;
                     }
                     var inArray = false;
                     var toSearch = selected.originalObject.codigo;
                     var ctaMkt = new RelacionCuentaMercado();
                     angular.forEach($scope.currentCuentaLiquidacion.relacionCuentaMercadoDataList,
                                     function (mkt, key) {
                                       if (mkt.codMkt === toSearch) {
                                         inArray = true;
                                         return false;
                                       }
                                     });
                     if (!inArray) {
                       ctaMkt.codMkt = selected.originalObject.codigo;
                       ctaMkt.idMkt = selected.originalObject.id;
                       $scope.currentCuentaLiquidacion.relacionCuentaMercadoDataList.push(ctaMkt);
                       $scope.selectedMkt.codigo = "";
                     }
                   };
                   $scope.selectedER = function (selected) {
                     if (selected !== undefined && selected.originalObject !== undefined) {
                       $scope.currentCuentaLiquidacion.entidadRegistro = selected.originalObject;
                     }
                   };
                   $scope.isSameIdTipoCuenta = function (id) {
                     if ($scope.currentCuentaLiquidacion.tipoCuentaConciliacion === null) {
                       $scope.currentCuentaLiquidacion.tipoCuentaConciliacion = {};
                       $scope.currentCuentaLiquidacion.tipoCuentaConciliacion.id = "";
                     }
                     var res = id === $scope.currentCuentaLiquidacion.tipoCuentaConciliacion.id;
                     // console.log("id: "+id + " cuenta.tipoCuentaConciliacion.id:
                     // "+$scope.cuenta.tipoCuentaConciliacion.id + " res: "+res);
                     return res;
                   };
                   $scope.onERSelectAction = function (selected) {
                     if (selected !== undefined && selected.originalObject !== undefined) {
                       $scope.currentCuentaLiquidacion.entidadRegistro = selected.originalObject;
                     }
                   }
                 } ]);
