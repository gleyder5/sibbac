'use strict';
sibbac20
    .controller(
                'ReportesAutomaticosCNMVController',
                [
                 '$scope',
                 '$document',
                 '$location',
                 'growl',
                 'ReportesCNMVService',
                 'IsinService',
                 '$compile',
                 function ($scope, $document, $location , growl, ReportesCNMVService, IsinService, $compile) {
                	 
                	 $scope.ejecutaMTF = false;
                	 $scope.ejecutaFallidos = false;
                	 
                     ReportesCNMVService.getParametrosEjecucion(function (data) {
                    	 if (data.resultados["crearMTF"] === 'S'){
                    		 $scope.ejecutaMTF = true;
                    	 }
                    	 if (data.resultados["crearFallidos"] === 'S'){
                    		 $scope.ejecutaFallidos = true;
                    	 }
                       }, function (error) {
                         fErrorTxt("Se produjo un error al obtener los parametros de ejecucion.", 1);
                       }, null);                	 
                	 
                		 
                	 
                	 
                	 // Generar datos para las MTF
                     $scope.GenerarH47MTF = function () {
                    	 	if ($scope.ejecutaMTF){
                                inicializarLoading();
                                var filters = "{ \"group\" : \"" + 'Fallidos' + "\", \"name\" : \"" + 'CreacionAutomaticaOperacionesH47Mtfs' + "\" }";
                                performJobCall('FIRE_NOW', filters, null, null, null, null);
                                $.unblockUI();
                                fErrorTxt("Generacion automatica de MTFs iniciada", 3);
                                return false;                    	 		
                    	 		
                    	 		/*ReportesCNMVService.generacionAutomaticaMTF(function (data) {

                    	 		}, function (error) {
                                    fErrorTxt("Se produjo un error en la llamada a la generacion automatica de MTF.", 1);
                                  }, null);
                                */  
                    	 		
                    	 		
                    	 	}else{
                    	 		fErrorTxt("La generacion automatica MTF no esta habilitada", 1);
                    	 	}
                    	 	
                     }
                                     
                     // Generar datos para los Fallisdos nacional
                     $scope.GenerarH47FallidosNacional = function () {
                 	 	if ($scope.ejecutaFallidos){

                            inicializarLoading();
                            var filters = "{ \"group\" : \"" + 'Fallidos' + "\", \"name\" : \"" + 'CreacionAutomaticaOperacionesH47FallisNac' + "\" }";
                            performJobCall('FIRE_NOW', filters, null, null, null, null);
                            $.unblockUI();
                            fErrorTxt("Generacion automatica de Fallidos Nacionales iniciada", 3);
                            return false;                    	 		
                 	 		
                 	 		
//                  	 		ReportesCNMVService.generacionAutomaticaFallidos(function (data) {
//                	 		}, function (error) {
//                                fErrorTxt("Se produjo un error en la llamada a la generacion automatica de Fallidos.", 1);
//                              }, null);
//                	 		
//                	 		fErrorTxt("Generacion automatica de Fallidos Nacionales iniciada", 3);
                	 		
                	 	}else{
                	 		fErrorTxt("La generacion automatica Fallidos Nacionales no esta habilitada", 1);
                	 	}

                     }
                     
                     $scope.ResultadosEjecucionProcesos = function () {
                    	 $location.path('/clearing/ResultadosEjecucionesCNMV');
                     }
                     
                     
                     
                     function performJobCall (command, filters, params, lista, successCallBack, _clear) {
                         var clear = (_clear !== undefined) ? _clear : true;

                         // Limpiamos los "targets"..
                         if (clear) {
                           $('#tdRunningJobs').empty();
                           $('#tdCurrentJobs').empty();
                         }


                         var data = new DataBean();
                         data.setService('SIBBACServiceJobsManagement');
                         data.setAction(command);
                         if (lista != null) {
                           data.list = lista;
                         }
                         if (filters != null) {
                           data.filters = filters;
                         }
                         if (params != null) {
                           data.params = params;
                         }
                         var SHOW_LOG = false;
                         if (SHOW_LOG)
                           console.log("Llamando al servidor[" + command + "]...");
                         var request = requestSIBBAC(data);
                         request.success(successCallBack);
                         request.error(function (jqXHR, textStatus, errorThrown) {
                           console.log("ERROR: " + jqXHR + ", " + textStatus + ": " + errorThrown);
                           console.log("Headers: " + jqXHR.getAllResponseHeaders());
                           console.log("Response text: " + jqXHR.responseText);
                           hideLoadingLayer(JSON.parse(jqXHR.responseText).error);
                         });
                       }
                     
} ]);