'use strict';
sibbac20
    .controller(
                'TipoEntidadRegistroController',
                [
                 '$scope',
                 '$rootScope',
                 '$document',
                 '$compile',
                 'EntidadRegistroService',
                 'growl',
                 'ngDialog',
                 '$location',
                 'SecurityService',
                 function ($scope, $rootScope, $document, $compile, EntidadRegistroService, growl, ngDialog, $location,
                           SecurityService) {

                   $scope.dialogTitle = 'Alta Tipo Entidad Registro';
                   $scope.tipoerList = {};
                   $scope.searchFollow = "";
                   $scope.currentTER = {
                     id : "",
                     nombre : ""
                   };
                   $scope.needRefresh = false;
                   $scope.filter = {
                     nombre : ""
                   };
                   //
                   function onSuccessConsultar (json) {
                     var datos = undefined;
                     if (json === undefined || json.resultados === undefined
                         || json.resultados.result_tipos_entidad_registro === undefined
                         || (json.error !== undefined && json.error !== null)) {
                       growl.addErrorMessage("Ocurrió un error al intentar consultar los tipos de entidad de registro "
                                             + json.error);
                     } else {
                       $scope.tipoerList = json.resultados.result_tipos_entidad_registro;
                       populateData($scope.tipoerList);
                     }
                     $.unblockUI();
                   }
                   function onErrorRequest (data) {
                     growl.addErrorMessage("Ocurrió un error al consultar datos en el servidor. " + data);
                     $.unblockUI();
                   }
                   function populateData (datos) {
                     var difClass = 'centrado';
                     oTable.clear();
                     var frecuencia = 0;
                     var entidadRegistro = "";
                     angular
                         .forEach(
                                  datos,
                                  function (val, key) {
                                    entidadRegistro = '<span data="' + val.id + '">' + val.nombre + '</span>';
                                    frecuencia = val.frecuenciaEnvioExtracto;

                                    // se controlan las acciones, se muestra el enlace cuando el usuario tiene el
                                    // permiso.
                                    var enlaceModificar = "";
                                    var enlaceEliminar = "";
                                    if (SecurityService.inicializated) {
                                      if (SecurityService.isPermisoAccion($rootScope.SecurityActions.MODIFICAR,
                                                                          $location.path())) {
                                        enlaceModificar = '<a class="btn" ng-if="isPermisoAccion(SecurityActions.MODIFICAR)" ng-click="modificarTER('
                                                          + val.id
                                                          + ');"><img src="img/editp.png"  title="editar" /></a>';
                                      }
                                      if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR, $location
                                          .path())) {
                                        enlaceEliminar = '<a class="btn" ng-if="isPermisoAccion(SecurityActions.BORRAR)" ng-click="borrarTipoEntidad('
                                                         + val.id + ');"><img src="img/del.png" title="borrar" /></a>';
                                      }
                                    }

                                    oTable.row.add([ enlaceModificar, entidadRegistro, enlaceEliminar ]);
                                  });
                     oTable.draw();
                   }
                   var oTable = undefined;
                   function consultar (params) {
                     inicializarLoading();
                     EntidadRegistroService.getAllTiposEntidadRegistro(onSuccessConsultar, onErrorRequest, params);
                   }

                   function loadDataTable () {
                     if (oTable !== null && oTable !== undefined) {
                       oTable = undefined;
                     }
                     oTable = angular.element("#tblTipoEntidadRegistro").DataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "js/swf/copy_csv_xls_pdf.swf"
                       },
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       "columns" : [ {
                         "sClass" : "centrado",
                         "bSortable" : "false"
                       }, {
                         "sClass" : "centrado"
                       }, {
                         "sClass" : "centrado",
                         "bSortable" : "false"
                       } ],
                       "sort" : [ 1 ],
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "createdRow" : function (row, data, index) {
                         $compile(row)($scope);
                       }
                     });

                   }
                   $scope.submitForm = function (event) {
                     obtenerDatos();
                     collapseSearchForm();
                     seguimientoBusqueda();
                   };
                   $document.ready(function () {

                     loadDataTable();
                     obtenerDatos();
                     prepareCollapsion();
                     angular.element('#limpiar').click(function (event) {
                       $scope.filter = {
                         nombre : ""
                       };
                     });

                     $scope.abrirAltaTipoEntidadRegistro = function () {
                       $scope.dialogTitle = 'Alta Tipo Entidad Registro';
                       $scope.currentTER = {
                         id : "",
                         nombre : ""
                       };
                       openEditForm();
                     };

                   });
                   function isInList (nombre) {
                     var is = false;
                     angular.forEach($scope.tipoerList, function (val, key) {
                       if (nombre == val.nombre) {
                         is = true;
                         return false;
                       }
                     });
                     return is;
                   }
                   $scope.submitDialogForm = function (event) {
                     if (isInList($scope.currentTER.nombre)) {
                       // alert("El TipoEntidadRegistro con ese nombre ya existe.");
                       // return false;
                     }
                     inicializarLoading();
                     var filtro = {
                       "idTipoEr" : $scope.currentTER.id,
                       "nombre" : $scope.currentTER.nombre
                     };
                     // SI NO TIENE ID ES QUE ES UN NUEVO TIPO
                     if (isNaN(parseInt($scope.currentTER.id))) {
                       EntidadRegistroService.saveTipoEntidadRegistro(onSuccessSaveRequest, onErrorRequest, filtro);
                     } else {
                       EntidadRegistroService.updateTipoEntidadRegistro(onSuccessSaveRequest, onErrorRequest, filtro);
                     }
                     $scope.needRefresh = true;
                   };
                   function seguimientoBusqueda () {
                     $scope.searchFollow = "";
                     if ($scope.filter.nombre !== "") {
                       $scope.searchFollow += " Nombre: " + $scope.filter.nombre;
                     }
                   }

                   function obtenerDatos () {
                     var params = {
                       "nombre" : $scope.filter.nombre
                     };
                     consultar(params);
                     $scope.needRefresh = false;
                   }

                   $scope.modificarTER = function (id) {
                     getTipoerFromList(id);
                     $scope.dialogTitle = 'Modificar Tipo Entidad Registro';
                     openEditForm();
                   };
                   function onSuccessDeleteHandler (json) {
                     $.unblockUI();
                     if (json.resultados === null) {
                       growl.addErrorMessage(json.error);
                     } else {
                       growl.addSuccessMessage("Tipo Entidad de Registro eliminada correctamente.");
                       obtenerDatos();
                     }
                   }

                   $scope.borrarTipoEntidad = function (id) {
                     var filtro = {
                       idTipoEr : id
                     };
                     angular.element("#dialog-confirm").html("¿Eliminar entidad?");

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm")
                         .dialog(
                                 {
                                   resizable : false,
                                   modal : true,
                                   title : "¿Desea borrar realmente el tipo de entidad " + id + "?",
                                   height : 150,
                                   width : 480,
                                   buttons : {
                                     "Sí" : function () {
                                       $(this).dialog('close');
                                       inicializarLoading();
                                       EntidadRegistroService.deleteTipoEntidadRegistro(onSuccessDeleteHandler,
                                                                                        onErrorRequest, filtro);
                                     },
                                     "No" : function () {
                                       $(this).dialog('close');
                                     }
                                   }
                                 });
                   }

                   function getTipoerFromList (id) {
                     angular.forEach($scope.tipoerList, function (val, key) {
                       if (id == val.id) {
                         $scope.currentTER = val;
                         return false;
                       }
                     });
                   }
                   function onSuccessSaveRequest (json) {
                     if (json !== undefined && json.error !== undefined && json.error !== null) {
                       growl.addErrorMessage(json.error);
                     } else {
                       growl.addSuccessMessage("Datos guardados correctamente.");
                       ngDialog.closeAll();
                     }
                     $.unblockUI();
                   }
                   function openEditForm () {
                     ngDialog.open({
                       template : 'template/conciliacion/tipo-entidad-registro/edit.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-480',
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           obtenerDatos();
                           $scope.needRefresh = false;
                           $scope.currentTER = {
                             id : "",
                             nombre : ""
                           };
                         }
                       }
                     });
                   }

                 } ]);
