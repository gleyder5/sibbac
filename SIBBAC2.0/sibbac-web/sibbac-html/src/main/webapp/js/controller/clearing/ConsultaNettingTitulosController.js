sibbac20.controller('ConsultaNettingTitulosController', ['$scope', 'IsinService', 'AliasService', 'ConsultaNettingService', 'EntregaRecepcionService', '$document', 'growl', '$compile',
    function ($scope, IsinService, AliasService, ConsultaNettingService, EntregaRecepcionService, $document, growl, $compile) {
        var oTable = undefined;

        $scope.filtro = {
            fcontratacionDe: "",
            fcontratacionA: "",
            fliquidacionDe: "",
            fliquidacionA: "",
            contabilizado: "",
            liquidado: "",
            alias: "",
            isin: "",
            chalias: true,
            chisin: true
        };

        $scope.followSearch = "";
        $scope.gastos;
        $scope.currentRow = {id: "", index: ""};
        $scope.efectivoTotal = "";
        $scope.corretajeTotal = "";
        $scope.marcados = [];

        function initialize() {
            //verificarFechas([["fcontratacionDe", "fcontratacionA"], ["fliquidacionDe", "fliquidacionA"]]);
        	 $("#ui-datepicker-div").remove();
             $('input#fcontratacionDe').datepicker({
                 onClose: function( selectedDate ) {
                   $( "#fcontratacionA" ).datepicker( "option", "minDate", selectedDate );
                 } // onClose
               });

               $('input#fcontratacionA').datepicker({
                 onClose: function( selectedDate ) {
                   $( "#fcontratacionDe" ).datepicker( "option", "maxDate", selectedDate );
                 } // onClose
               });

               $('input#fliquidacionDe').datepicker({
                   onClose: function( selectedDate ) {
                     $( "#fliquidacionA" ).datepicker( "option", "minDate", selectedDate );
                   } // onClose
                 });

                 $('input#fliquidacionA').datepicker({
                   onClose: function( selectedDate ) {
                     $( "#fliquidacionDe" ).datepicker( "option", "maxDate", selectedDate );
                   } // onClose
                 });
        	loadDataTable();
            loadAlias();
            loadIsines();
            cargarDatosNetting();
        }
        function cargarFechas(){
          	$scope.filtro.fcontratacionDe = $('#fcontratacionDe').val();
          	$scope.filtro.fcontratacionA = $('#fcontratacionA').val();
          	$scope.filtro.fliquidacionDe = $('#fliquidacionDe').val();
          	$scope.filtro.fliquidacionA = $('#fliquidacionA').val();
      }
        function loadDataTable() {
            oTable = $("#tblConsultaTitulos").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    if (aData[0] == "N") {
                        angular.element('td:eq(0)', nRow).html('<input type="checkbox" id="checkTabla" name="checkTabla" class="checkTabla" value="N" >');
                    } else {
                        angular.element('td:eq(0)', nRow).html('<input type="checkbox" id="checkTabla" name="checkTabla" class="checkTabla" value="N" checked="checked" disabled readonly>');
                    }
                    $compile(nRow)($scope);
                },
                "order": [],
                "scrollY": "480px",
                "scrollCollapse": true,
                "scrollX": true

            });
        }

        $document.ready(function () {
            initialize();

            angular.element('#tblConsultaTitulos tbody').on('click', 'td.checkbox', function () {

                var nTr = $(this).parents('tr')[0];
                var aData = oTable.fnGetData(nTr);
                var valor = $('input', nTr)[0].getAttribute('value');
                if (valor == "N") {
                    $('input', nTr)[0].setAttribute('value', aData[1]);
                } else {
                    $('input', nTr)[0].setAttribute('value', 'N');
                }

            });
            $('#tblConsultaTitulos tbody').on('click', 'td.details-alc', function () {

                var nTr = $(this).parents('tr')[0];
                var aData = oTable.fnGetData(nTr);
                if (oTable.fnIsOpen(nTr)) {
                    $(this).removeClass('open alc');
                    oTable.fnClose(nTr);
                } else {

                    $(this).addClass('open alc');
                    oTable.fnOpen(nTr, TablaALC(oTable, nTr), 'details-alc');
                }

            });
            prepareCollapsion();

        });

//        $scope.submitForm = function (event) {
        $scope.Consultar = function (event) {
        	cargarFechas();
            cargarDatosNetting();
            seguimientoBusqueda();
            collapseSearchForm();

        };
        $scope.clearFilters = function (event) {
        	cargarFechas();
        	//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fcontratacionA" ).datepicker( "option", "minDate", "");
            $( "#fcontratacionDe" ).datepicker( "option", "maxDate", "");
            $( "#fliquidacionA" ).datepicker( "option", "minDate", "");
            $( "#fliquidacionDe" ).datepicker( "option", "maxDate", "");
            //
            $scope.filtro = {
                    fcontratacionDe: "",
                    fcontratacionA: "",
                    fliquidacionDe: "",
                    fliquidacionA: "",
                    contabilizado: "",
                    liquidado: "",
                    alias: "",
                    isin: "",
                    chalias: false,
                    chisin: false
            };
    		$scope.btnInvGenerico('Alias');
    		$scope.btnInvGenerico('Isin');
        };
        function cargarDatosNetting() {
            var alias = (!$scope.filtro.chalias && $scope.filtro.alias !== "" ? "#" : "") + getAliasId();
            var isin = (!$scope.filtro.chisin && $scope.filtro.isin !== "" ? "#" : "") + getIsin();

            var filtro = {
                fcontratacionDe: $scope.filtro.fcontratacionDe,
                fcontratacionA: $scope.filtro.fcontratacionA,
                fliquidacionDe: $scope.filtro.fliquidacionDe,
                fliquidacionA: $scope.filtro.fliquidacionA,
                contabilizado: $scope.filtro.contabilizado,
                liquidado: $scope.filtro.liquidado,
                alias: alias,
                isin: isin
            };
            inicializarLoading();
            ConsultaNettingService.listaNettingSinMovimientoTitulos(onSuccessListaNettingSM, onErrorRequest, filtro);
        }
        function onSuccessListaNettingSM(json) {
            console.log(json);
            console.log(json.resultados);
            if (json !== null && json !== undefined) {
                if (json.resultados !== undefined && json.resultados.lista !== undefined) {
                    var item = null;
                    var datos = json.resultados.lista;
                    var efectivoTotal = 0;
                    var corretajeTotal = 0;
                    oTable.fnClearTable();
                    $scope.efectivoTotal ="";
                    $scope.corretajeTotal = "";
                    $scope.marcados = [datos.length];
                    var marcado = false;
                    angular.forEach(datos, function(item, k){
                        marcado = item.liquidado == 'S' ? true : false;
                        $scope.marcados[k]=marcado;
                        var rowIndex = oTable.fnAddData([
                            /*'<input type="checkbox" ng-model="marcados['+k+']" />',*/
                            item.liquidado,
                            item.idnetting,
                            item.liquidado,
                            transformaFechaGuion(item.fechaoperacion),
                            transformaFechaGuion(item.fechacontratacion),
                            item.cdalias,
                            item.camaracompensacion,
                            item.cdisin,
                            item.nbvalors,
                            item.nutiulototales,
                            $.number(item.efectivoneteado, 2, ',', '.'),
                            $.number(item.corretajeneteado, 2, ',', '.'),
                            item.contabilizado,
                            transformaFechaGuion(item.fechacontabilizado)
                        ], false);
                        var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
                        angular.element('td', nTr)[1].setAttribute('style', 'cursor: pointer; color: #ff0000;');
                        angular.element('td', nTr)[5].setAttribute('title', item.descrali);
                        efectivoTotal = efectivoTotal + item.efectivoneteado;
                        corretajeTotal = corretajeTotal + item.corretajeneteado;
                    });
                    oTable.fnDraw();
                    $scope.efectivoTotal = $.number(efectivoTotal, 2, ',', '.');
                    $scope.corretajeTotal = $.number(corretajeTotal, 2, ',', '.');
                } else if (json.error !== null && json.error !== "") {
                    growl.addErrorMessage(json.error);
                }
            }
            $.unblockUI();
        }

        function TablaALC(Table, nTr) {

            var aData = Table.fnGetData(nTr);
            var contador = Table.fnGetPosition(nTr);
            var resultado =
                    "<tr id='tabla_a_desplegar_ALC" + contador + "' style='display:none;'>" +
                    "<td colspan='13' style='border-width:0 !important;background-color:#dfdfdf;'>" +
                    "<table id='tablaParam_ALC" + contador + "' style=width:100%; margin-top:5px;'>" +
                    "<tr>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Estado</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Sentido</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Títulos</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Efectivo</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Corretaje</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Neto</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Canon Contratación</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Canon Compensación</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Canon Liquidación</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>NIF</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Titular</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Booking</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Nucnfclt</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Nucnfliq</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Ref. bancaria</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Ourparticipe</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Theirparticipe</th>" +
                    "</tr></table></td></tr>";
            desplegarDatosALC(aData[1], contador);
            return resultado;
        }

        //funcion para desplegar la tabla de alc
        function desplegarDatosALC(idnetting, numero_fila) {
            $scope.currentRow.id = idnetting;
            $scope.currentRow.index = numero_fila;
            //
            console.log("desplegarDatosALC");
            var filtro = {idnetting: idnetting};
            inicializarLoading();
            EntregaRecepcionService.ListaEntregaRecepcion(onSuccessEntregaRecepcionRequest, onErrorRequest, filtro);
        }
        function onSuccessEntregaRecepcionRequest(json) {
            console.log(json);
            console.log(json.resultados);
            var numero_fila = $scope.currentRow;
            if (json !== null && json !== undefined) {
                if (json.resultados !== undefined && json.resultados.lista !== undefined) {
                    var item = null;
                    var contador = 0;
                    var datos = json.resultados.lista;
                    for (var k in datos) {
                        item = datos[ k ];
                        if (contador % 2 == 0) {
                            estilo = "even";
                        } else {
                            estilo = "odd";
                        }
                        contador++;
                        $("#tablaParam_ALC" + numero_fila).append(
                                "<tr class='" + estilo + "' id='filaRecuperadaALC" + numero_fila + "' style='background-color: #f5ebc5 !important;' >" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.estadoentrec + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.cdtpoper + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nutitliq + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + a2digitos(item.imefeagr) + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + a2digitos(item.imcomisin) + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + a2digitos(item.imnfiliq) + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.canon_contratacion + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.canon_compensacion + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.canon_liquidacion + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.cdniftit + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nbtitliq + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nbooking + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nucnfclt + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nucnfliq + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.cdrefban + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.ourpar + "</td>" +
                                "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.theirpar + "</td>" +
                                "</tr>"
                                );
                        angular.element("#tablaParam_ALC" + numero_fila).append("</table>");
                    }
                } else if (json.error !== null && json.error !== "") {
                    growl.addErrorMessage(json.error);
                }
            }
            desplegar('tabla_a_desplegar_ALC' + numero_fila, 'estadoT' + numero_fila, 'estadoT' + numero_fila);
        }

        //funcion para desplegar las subtablas
        function desplegar(tabla_a_desplegar, e1, estadoTfila) {
            var tablA = document.getElementById(tabla_a_desplegar);
            var estadOt = document.getElementById(e1);
            var fila = document.getElementById(estadoTfila);
            //recorremos las filas y ocultamos todas menos esta

            switch (tablA.style.display) {
                case "none":
                    tablA.style.display = "";
                    if (estadOt != null) {
                        estadOt.innerHTML = "Ocultar";
                    }
                    break;
                default:
                    tablA.style.display = "none";
                    if (estadOt != null) {
                        estadOt.innerHTML = "Mostrar";
                    }
                    break;
            }
        }



        //Evento para marcar gastos
        $scope.marcar = function () {
            var suma = 0;
            var valores = new Array()
            var los_cboxes = document.getElementsByName('checkTabla');
            for (var i = 0, j = los_cboxes.length; i < j; i++) {

                if (los_cboxes[i].value !== 'N') {
                    valores.push(los_cboxes[i].value);
                    suma++;
                }
            }

            if ($scope.gastos == "") {
                alert("Introduzca el gasto asociado");
                return false;
            }

            if (suma == 0) {
                alert('Marque al menos una línea');
                return false;
            } else {
                marcarGastos(valores);
            }

        };

        //Funcion para marcar facturas
        function marcarGastos(valores)
        {
            var bucleVal = valores.length;
            console.log("entro en el metodo");
            var params = {};
            for (var v = 0; v < bucleVal; v++) {
                params.push({ idNetting : valores[v]});
            }

            var filters = {gastos : $scope.gastos};
            console.log(params);
            inicializarLoading();
            ConsultaNettingService.MarcarGastos(onSuccessMarcarGastos, onErrorRequest, filters, params);
        }
        function onSuccessMarcarGastos(json) {
            cargarDatosNetting();
            $.unblockUI();
        }
        function seguimientoBusqueda() {

            $scope.followSearch = "";

            if ($scope.filtro.fcontratacionDe !== "") {
                $scope.followSearch += " F. Contratacion Desde: " + $scope.filtro.fcontratacionDe;
            }
            if ($scope.filtro.fcontratacionA !== "") {
                $scope.followSearch += " F. Contratacion Hasta: " + $scope.filtro.fcontratacionA;
            }
            if ($scope.filtro.fliquidacionDe !== "") {
                $scope.followSearch += " F. liquidacion Desde: " + $scope.filtro.fliquidacionDe
            }
            if ($scope.filtro.fliquidacionA !== "") {
                $scope.followSearch += " F. liquidacion Hasta: " + $scope.filtro.fliquidacionA;
            }

            if ($scope.filtro.alias !== "") {
              	if($scope.filtro.chalias)
              		$scope.followSearch += " Alias: " + document.getElementById("alias").value;
              	else
              		$scope.followSearch += " Alias distinto: " + document.getElementById("alias").value;;
            }
            if ($scope.filtro.isin !== "") {
        	  if($scope.filtro.chisin)
        		  $scope.followSearch += " Isin: " + document.getElementById("isin").value;
        	  else
        		  $scope.followSearch += " Isin distinto: " + document.getElementById("isin").value;
      	  	}
        }
        function loadAlias() {
            inicializarLoading();
            AliasService.getAlias().success(cargaAlias).error(onErrorRequest);
        }
        function cargaAlias(data) {
            var availableTagsAlias = [];
            angular.forEach(data.resultados.result_alias, function (val, key) {
                var ponerTag = val.alias + " - " + val.descripcion;
                availableTagsAlias.push(ponerTag);
            });
            // código de autocompletar
            angular.element("#alias").autocomplete({
                source: availableTagsAlias
            });
            $.unblockUI();
        }
        function getAliasId() {
            //var alias = $scope.filtro.alias.trim();
            var alias = $("#alias").val();
            var guion = alias.indexOf("-");
            return (guion > 0) ? alias.substring(0, guion).trim() : alias;
        }

        /** ISINES **/
        function loadIsines() {
            IsinService.getIsines(cargarIsines, onErrorRequest);
        }
        function cargarIsines(datosIsin) {
            var availableTags = [];
            angular.forEach(datosIsin, function (val, key) {
                var ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                availableTags.push(ponerTag);
            });
            angular.element("#isin").autocomplete({
                source: availableTags
            });
        }
        function getIsin() {
            //var isinCompleto = $scope.filtro.isin.trim();
            var isinCompleto = document.getElementById("isin").value; //modificado por alex 19Jan para realizar la busqueda correcta en Chrome
            var guion = isinCompleto.indexOf("-");
            if (guion > 0) {
                return isinCompleto.substring(0, guion);
            } else {
                return isinCompleto;
            }
        }
        function onErrorRequest(data) {
            $.unblockUI();
            growl.addErrorMessage("Ocurrió un error durante la petición de datos.");
        }

    	$scope.btnInvAlias = function() {
    		$scope.btnInvGenerico('Alias');
    	}

    	$scope.btnInvIsin = function() {
    		$scope.btnInvGenerico('Isin');
    	}

    	$scope.btnInvGenerico = function( nombre ) {

      	  var check = '$scope.filtro.ch' + nombre.toLowerCase();

        	  if(eval(check)){
        		  	  var valor2 = check + "= false";
        		  	  eval(valor2 ) ;

          		  document.getElementById('textBtnInv'+nombre).innerHTML = "<>";
          		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "red"});
          		  document.getElementById('textInv'+nombre).innerHTML = "DISTINTO";
          	  }else{
                 	  var valor2 = check + "= true";
              	  eval(valor2 ) ;

          		  document.getElementById('textBtnInv'+nombre).innerHTML = "=";
          		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "green"});
          		  document.getElementById('textInv'+nombre).innerHTML = "IGUAL";
          	  }
          }

    }]);

