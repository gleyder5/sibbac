sibbac20.controller('ConciliacionBancariaTLMController',['$rootScope', '$scope', '$compile', 'ConciliacionBancariaService', 'ConciliacionBancariaTLMService', 'ConciliacionBancariaCombosService', '$document', 'growl','CONSTANTES',
						function($rootScope, $scope, $compile, ConciliacionBancariaService, ConciliacionBancariaTLMService, ConciliacionBancariaCombosService, $document, growl,Constantes) {

		$scope.tituloTLM = "Partidas TLM";
		$scope.showingFilterTLM = true;
		$scope.tituloPersianaTLM = "Ocultar opciones de búsqueda";
		$scope.safeApply = function(fn) {
			var phase = this.$root.$$phase;
			if (phase === '$apply' || phase === '$digest') {
				if (fn && (typeof (fn) === 'function')) {
					fn();
				}
			} else {
				this.$apply(fn);
			}
		};
		
		 $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col ) {
			  return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
			    return $('input', td).prop('checked') ? '1' : '0';
			  })
			};
		
		var abreTLM = function(event){
		
			if ($scope.oTableTLM !== undefined && $scope.oTableTLM !== null) {
				$scope.oTableTLM.fnDestroy();
			}
			
			$scope.oTableTLM = $("#datosTLM").dataTable({"dom" : 'T<"clear">lfrtip', "tableTools" : {"aButtons" : [ "print" ]},
				"aoColumns" : [ {sClass : "centrar", width : "30px", orderDataType: "dom-checkbox"}, 
				                {width : "70px"}, {width : "50px"},{width : "200px"},{width : "70px"},{width : "100px", render: $.fn.dataTable.render.number( '.', ',', 2)},
				                {width : "200px"},{width : "10px"},{width : "10px", sType : "date-eu"},{width : "10px", sType : "date-eu"},{width : "140px"}],

				"fnCreatedRow" : function(nRow,aData, iDataIndex) {
					$compile(nRow)($scope);
				},
				"scrollX" : "100%",
				"scrollCollapse" : true,
				"language" : {
					"url" : "i18n/Spanish.json"
				}
			});
			if ($scope.oTableTLM !== undefined && $scope.oTableTLM !== null) {
				$scope.oTableTLM.fnClearTable();
			}
			
			$scope.resetFiltroTLM();
			
			$scope.catalogoNumeroDocumentoTLM();
			$scope.catalogoComentariosTLM();
			$scope.catalogoConceptoMovimientoTLM();
			$scope.catalogoAuxiliaresTLM();
			$scope.catalogoTiposMovimientoTLM();
			$scope.cargarAntiguedadesTLM();
			$scope.showingFilterTLM = true;
			$scope.tituloPersianaTLM = "Ocultar opciones de búsqueda";
		};
		
		$scope.$on("abreTLM", abreTLM);
		
		$scope.resultadosTLM = [];
		
		$scope.resetFormTLM = function(){
			$scope.formTLM = {
				tiposDeMovimientosFilter: "",
				tipoMovimiento : "",
				auxiliaresFilter: [],
				auxiliar : "",
				conceptosMovimientosFilter: "",
				conceptoMovimiento : "",
				comentariosFilter: "",
				comentario : "",
				numerosDocumentosFilter: "",
				numeroDocumento : "",
				antiguedadesFilter : []
			}
		};
		
		$scope.resetFiltroTLM = function() {
			$scope.mostrarErrorFiltroFecha = false;
			$scope.filtroTLM = {
				tiposMovimientos : [],
				auxiliares : [],
				conceptosMovimientos : [],
				numerosDocumentos : [],
				comentarios : [],
				bookingDateDesde : '',
				bookingDateHasta : '',
				valueDateDesde : '',
				valueDateHasta : '',
				importeDesde : '',
				importeHasta : '',
				antiguedades : [],
				gin : ''
			}
			
			$scope.sentidoFiltroTLM = [];
			$scope.sentidoFiltroTLM['numeroDocumentoTLM'] = "=";
			$scope.sentidoFiltroTLM['comentarioTLM'] = "=";
			$scope.sentidoFiltroTLM['conceptoMovimientoTLM'] = "=";
			
			
			$scope.resultadosTLM = [];
			$scope.seleccionadosTLM = [];
			$scope.seleccionadosBorrarTLM = [];
			$scope.resetFormTLM();
			
			$scope.safeApply();
		};
		
		$scope.listaTiposMovimientoTLM = [];
		$scope.listaAuxiliaresTLM = [];
		$scope.listaConceptoMovTLM = [];
		$scope.listaNrosDocumentoTLM = [];
		$scope.listaComentariosTLM = [];
		
		$scope.operadoresFiltroTLM = [ "=", "≠", "~", "≥", "≤" ];
		
		var enmascararOperadoresTLM = function(operador) {
			var mascara = null;
			switch (operador) {
			case '=':
				mascara = 'IGUAL';
				break;
			case '≠':
				mascara = 'DISTINTO';
				break;
			case '~':
				mascara = 'CONTIENE';
				break;
			case '≥':
				mascara = 'COMIENZA';
				break;
			case '≤':
				mascara = 'FINALIZA';
				break;
			default:
				break;
			}
			return mascara;
		}
		
		$scope.populateAutocompleteTLM = function (input, availableTags) {
            $(input).autocomplete({
               minLength : 0,
               source : availableTags,
               focus : function (event, ui) {
                 return false;
               },
               select : function (event, ui) {

                 var option = {
                   key : ui.item.key,
                   value : ui.item.value
                 };

                 switch (input) {
                   case "#filtroConceptoMovimientoTLM":
                         var existe = false;
                         angular.forEach($scope.filtroTLM.conceptosMovimientos, function (campo) {
                           if (campo.value === option.value) {
                             existe = true;
                           }
                         });
                         $scope.formTLM.conceptoMovimiento = "";
                         if (!existe) {
                           option = {key: option.key, value: option.value, oper: enmascararOperadoresTLM($scope.sentidoFiltroTLM['conceptoMovimientoTLM'])};
                           $scope.filtroTLM.conceptosMovimientos.push(option);
                         }
                         break;
                   case "#filtroNumeroDocumentoTLM":
                         var existe = false;
                         angular.forEach($scope.filtroTLM.numerosDocumentos, function (campo) {
                           if (campo.value === option.value) {
                             existe = true;
                           }
                         });
                         $scope.formTLM.numeroDocumento = "";
                         if (!existe) {
                           option = {key: option.key, value: option.value, oper: enmascararOperadoresTLM($scope.sentidoFiltroTLM['numeroDocumentoTLM'])};
                           $scope.filtroTLM.numerosDocumentos.push(option);
                         }
                         break;
                   case "#filtroComentarioTLM":
                         var existe = false;
                         angular.forEach($scope.filtroTLM.comentarios, function (campo) {
                           if (campo.value === option.value) {
                             existe = true;
                           }
                         });
                         $scope.formTLM.comentario = "";
                         if (!existe) {
                           option = {key: option.key, value: option.value, oper: enmascararOperadoresTLM($scope.sentidoFiltroTLM['comentarioTLM'])};
                           $scope.filtroTLM.comentarios.push(option);
                         }
                         break;
                         
                   case "#filtroTipoMovimientoTLM":
                         var existe = false;
                         angular.forEach($scope.filtroTLM.tiposMovimientos, function (campo) {
                           if (campo.value === option.value) {
                             existe = true;
                           }
                         });
                         $scope.formTLM.tipoMovimiento = "";
                         if (!existe) {
                           $scope.filtroTLM.tiposMovimientos.push(option);
                         }
                         break;
                   case "#filtroAuxiliaresTLM":
                         var existe = false;
                         angular.forEach($scope.filtroTLM.auxiliares, function (campo) {
                           if (campo.value === option.value) {
                             existe = true;
                           }
                         });
                         $scope.formTLM.auxiliar = "";
                         if (!existe) {
                           $scope.filtroTLM.auxiliares.push(option);
                         }
                         break;
                   default:
                     break;
                 }

                 $scope.safeApply();

                 return false;
               }
            });
		};
		
		$scope.anadirAListaTLM = function(input){
            switch (input) {
              case "filtroConceptoMovimientoTLM":
	            	  var existe = false;
	            	  var option = {key : $scope.formTLM.conceptoMovimiento, value : $scope.formTLM.conceptoMovimiento, oper: enmascararOperadoresTLM($scope.sentidoFiltroTLM['conceptoMovimientoTLM'])};
	                  angular.forEach($scope.filtroTLM.conceptosMovimientos, function (campo) {
	                    if (campo.value === option.value) {
	                      existe = true;
	                    }
	                  });
	                  $scope.formTLM.conceptoMovimiento = "";
	                  if (!existe) {
	                    $scope.filtroTLM.conceptosMovimientos.push(option);
	                  }
                    break;
              case "filtroNumeroDocumentoTLM":
	            	  var existe = false;
	            	  var option = {key : $scope.formTLM.numeroDocumento, value : $scope.formTLM.numeroDocumento, oper: enmascararOperadoresTLM($scope.sentidoFiltroTLM['numeroDocumentoTLM'])};
	                  angular.forEach($scope.filtroTLM.numerosDocumentos, function (campo) {
	                    if (campo.value === option.value) {
	                      existe = true;
	                    }
	                  });
	                  $scope.formTLM.numeroDocumento = "";
	                  if (!existe) {
	                    $scope.filtroTLM.numerosDocumentos.push(option);
	                  }
                    break;
              case "filtroComentarioTLM":
	            	  var existe = false;
	            	  var option = {key : $scope.formTLM.comentario, value : $scope.formTLM.comentario, oper: enmascararOperadoresTLM($scope.sentidoFiltroTLM['comentarioTLM'])};
	                  angular.forEach($scope.filtroTLM.comentarios, function (campo) {
	                    if (campo.value === option.value) {
	                      existe = true;
	                    }
	                  });
	                  $scope.formTLM.comentario = "";
	                  if (!existe) {
	                    $scope.filtroTLM.comentarios.push(option);
	                  }
                    
                    break;
              default:
                  break;
              };
		};
					

		$scope.catalogoTiposMovimientoTLM = function(){
			ConciliacionBancariaCombosService.consultarTiposDeMovimiento(function(data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo tipos de movimiento", 1);
				} else {
					$scope.listaTiposMovimientoTLM = data.resultados.listaTiposMovimiento;
					$scope.populateAutocompleteTLM("#filtroTipoMovimientoTLM", $scope.listaTiposMovimientoTLM);
				}
			},
			function(error) {
				fErrorTxt("Se produjo un error en la carga del combo tipos de movimiento",1);
			});
		};


		$scope.catalogoAuxiliaresTLM = function(){
			ConciliacionBancariaCombosService.consultarAuxiliares(function(data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo de auxiliares", 1);
				} else {
					$scope.listaAuxiliaresTLM = data.resultados.listaAuxiliaresBancario;
					// $scope.populateAutocompleteTLM("#filtroAuxiliaresTLM", $scope.listaAuxiliaresTLM);
				}
			},
			function(error) {
				fErrorTxt("Se produjo un error en la carga del combo de auxiliares",1);
			});
		};

		$scope.catalogoConceptoMovimientoTLM = function(){
			ConciliacionBancariaCombosService.consultarConceptoMovimiento(function(data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo concepto movimiento", 1);
				} else {
					$scope.listaConceptoMovTLM = data.resultados.listaConceptoMov;
					$scope.populateAutocompleteTLM("#filtroConceptoMovimientoTLM", $scope.listaConceptoMovTLM);
				}
			},
			function(error) {
				fErrorTxt("Se produjo un error en la carga del combo concepto movimiento",1);
			});
		};


		$scope.catalogoNumeroDocumentoTLM = function(){
			ConciliacionBancariaCombosService.consultarNumerosDocumento(function(data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo numero documento", 1);
				} else {
					$scope.listaNrosDocumentoTLM = data.resultados.listaNrosDocumento;
					$scope.populateAutocompleteTLM("#filtroNumeroDocumentoTLM", $scope.listaNrosDocumentoTLM);
				}
			},
			function(error) {
				fErrorTxt("Se produjo un error en la carga del combo numero documento",1);
			});
		};


		$scope.catalogoComentariosTLM = function(){
			ConciliacionBancariaCombosService.consultarComentarios(function(data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo comentario", 1);
				} else {
					$scope.listaComentariosTLM = data.resultados.listaComentarios;
					$scope.populateAutocompleteTLM("#filtroComentarioTLM", $scope.listaComentariosTLM);
				}
			},
			function(error) {
				fErrorTxt("Se produjo un error en la carga del combo comentario",1);
			});
		};

		$scope.cargarAntiguedadesTLM = function () {
			ConciliacionBancariaCombosService.antiguedades(function (data) {
				if (data.resultados.status == 'KO') {
					fErrorTxt("Se produjo un error en la carga del combo de Antiguedades", 1);
				} else {
					$scope.listAntiguedades = data.resultados["listaAntiguedades"];
				}
			}, function (error) {
				fErrorTxt("Se produjo un error en la carga del combo de Antiguedades", 1);
			});
		};
					
        $scope.vaciarTablaTLM = function (tabla) {
           switch (tabla) {
             case "conceptosMovimientosTLM":
               $scope.filtroTLM.conceptosMovimientos = [];
               break;
             case "numerosDocumentosTLM":
            	 $scope.filtroTLM.numerosDocumentos = [];
               break;
             case "comentariosTLM":
               $scope.filtroTLM.comentarios = [];
               break;
             case "tiposMovimientosTLM":
                   $scope.filtroTLM.tiposMovimientos = [];
                   break;
             case "auxiliaresTLM":
                   $scope.filtroTLM.auxiliares = [];
                   break;	
             case "antiguedades":
                 $scope.filtroTLM.antiguedades = [];
                 break;
             default:
               break;
           }
        };
	                
        $scope.eliminarElementoTablaTLM = function (tabla) {
             switch (tabla) {
               case "conceptosMovimientosTLM":
                 for (var i = 0; i < $scope.filtroTLM.conceptosMovimientos.length; i++) {
                   if ($scope.filtroTLM.conceptosMovimientos[i].value === $scope.formTLM.conceptosMovimientosFilter) {
                	   $scope.filtroTLM.conceptosMovimientos.splice(i, 1);
                   }
                 }
                 break;
               case "numerosDocumentosTLM":
                 for (var i = 0; i < $scope.filtroTLM.numerosDocumentos.length; i++) {
                   if ($scope.filtroTLM.numerosDocumentos[i].value === $scope.formTLM.numerosDocumentosFilter) {
                	   $scope.filtroTLM.numerosDocumentos.splice(i, 1);
                   }
                 }
                 break;
               case "tiposMovimientosTLM":
                 for (var i = 0; i < $scope.filtroTLM.tiposMovimientos.length; i++) {
                   if ( $scope.filtroTLM.tiposMovimientos[i].value ===  $scope.formTLM.tiposDeMovimientosFilter) {
                	   $scope.filtroTLM.tiposMovimientos.splice(i, 1);
                   }
                 }
                 break;
               case "comentariosTLM":
                     for (var i = 0; i < $scope.filtroTLM.comentarios.length; i++) {
                       if ( $scope.filtroTLM.comentarios[i].value ===  $scope.formTLM.comentariosFilter) {
                    	   $scope.filtroTLM.comentarios.splice(i, 1);
                       }
                     }
                     break;		
               case "auxiliaresTLM":
                     for (var i = 0; i < $scope.filtroTLM.auxiliares.length; i++) {
                       if ( $scope.filtroTLM.auxiliares[i].value ===  $scope.formTLM.auxiliaresFilter) {
                    	   $scope.filtroTLM.auxiliares.splice(i, 1);
                       }
                     }
                     break;	
               case "antiguedadesTLM":
                   for (var i = 0; i < $scope.filtroTLM.antiguedades.length; i++) {
                     if ( $scope.filtroTLM.antiguedades[i] ===  $scope.formTLM.antiguedadesFilter) {
                  	   $scope.filtroTLM.antiguedades.splice(i, 1);
                     }
                   }
                   break;
               default:
                 break;
             }
        };
					

		/***************************************************
		 * ** FUNCIONES GENERALES
		 **************************************************/
    	$scope.seguimientoBusquedaTLM = function() {
    	    $('.mensajeBusquedaTLM').empty();
    	    var cadenaFiltros = "";
    	    if ($scope.filtroTLM.tiposMovimientos !== null && $scope.filtroTLM.tiposMovimientos.length > 0) {
    	    	cadenaFiltros += " Tipo de movimiento: ";
    	    	for (var i = 0; i < $scope.filtroTLM.tiposMovimientos.length; i++) {
    	    		cadenaFiltros += $scope.filtroTLM.tiposMovimientos[i].value;
    	    		if (i < ($scope.filtroTLM.tiposMovimientos.length -1)) {
    	    			cadenaFiltros += ", ";
    	    		}
                }
    	    }
    	    if ($scope.filtroTLM.auxiliares !== null && $scope.filtroTLM.auxiliares.length > 0) {
    	    	cadenaFiltros += " Auxiliar bancario: ";
    	    	for (var i = 0; i < $scope.filtroTLM.auxiliares.length; i++) {
    	    		cadenaFiltros += $scope.filtroTLM.auxiliares[i].value;
    	    		if (i < ($scope.filtroTLM.auxiliares.length -1)) {
    	    			cadenaFiltros += ", ";
    	    		}
                }
    	    }
    	    if ($scope.filtroTLM.conceptosMovimientos !== null && $scope.filtroTLM.conceptosMovimientos.length > 0) {
    	    	cadenaFiltros += " Concepto Movimiento: ";
    	    	for (var i = 0; i < $scope.filtroTLM.conceptosMovimientos.length; i++) {
    	    		cadenaFiltros += $scope.filtroTLM.conceptosMovimientos[i].value;
    	    		if (i < ($scope.filtroTLM.conceptosMovimientos.length -1)) {
    	    			cadenaFiltros += ", ";
    	    		}
                }
    	    }
    	    if ($scope.filtroTLM.comentarios !== null && $scope.filtroTLM.comentarios.length > 0) {
    	    	cadenaFiltros += " Comentario: ";
    	    	for (var i = 0; i < $scope.filtroTLM.comentarios.length; i++) {
    	    		cadenaFiltros += $scope.filtroTLM.comentarios[i].value;
    	    		if (i < ($scope.filtroTLM.comentarios.length -1)) {
    	    			cadenaFiltros += ", ";
    	    		}
                }
    	    }
    	    if ($scope.filtroTLM.numerosDocumentos !== null && $scope.filtroTLM.numerosDocumentos.length > 0) {
    	    	cadenaFiltros += " Nro. de Documento: ";
    	    	for (var i = 0; i < $scope.filtroTLM.numerosDocumentos.length; i++) {
    	    		cadenaFiltros += $scope.filtroTLM.numerosDocumentos[i].value;
    	    		if (i < ($scope.filtroTLM.numerosDocumentos.length -1)) {
    	    			cadenaFiltros += ", ";
    	    		}
                }
    	    }
    	    if ($scope.filtroTLM.antiguedades !== null && $scope.filtroTLM.antiguedades.length > 0) {
    	    	cadenaFiltros += " Antiguedad: ";
    	    	for (var i = 0; i < $scope.filtroTLM.antiguedades.length; i++) {
    	    		cadenaFiltros += $scope.filtroTLM.antiguedades[i];
    	    		if (i < ($scope.filtroTLM.antiguedades.length -1)) {
    	    			cadenaFiltros += ", ";
    	    		}
                }
    	    }
    	    if ($scope.filtroTLM.bookingDateDesde !== null && $scope.filtroTLM.bookingDateDesde !== "") {
    	    	cadenaFiltros += " Booking Date Desde: " + $scope.filtroTLM.bookingDateDesde;
    	    }
    	    if ($scope.filtroTLM.bookingDateHasta !== null && $scope.filtroTLM.bookingDateHasta !== "") {
    	    	cadenaFiltros += " Booking Date Hasta: " + $scope.filtroTLM.bookingDateHasta;
    	    }
    	    if ($scope.filtroTLM.valueDateDesde !== null && $scope.filtroTLM.valueDateDesde !== "") {
    	    	cadenaFiltros += " Value Date Desde: " + $scope.filtroTLM.valueDateDesde;
    	    }
    	    if ($scope.filtroTLM.valueDateHasta !== null && $scope.filtroTLM.valueDateHasta !== "") {
    	    	cadenaFiltros += " Value Date Hasta: " + $scope.filtroTLM.valueDateHasta;
    	    }
    	    if ($scope.filtroTLM.importeDesde !== null && $scope.filtroTLM.importeDesde !== "") {
    	    	cadenaFiltros += " Importe Desde: " + $scope.filtroTLM.importeDesde;
    	    }
    	    if ($scope.filtroTLM.importeHasta !== null && $scope.filtroTLM.importeHasta !== "") {
    	    	cadenaFiltros += " Importe Hasta: " + $scope.filtroTLM.importeHasta;
    	    }
    	    if ($scope.filtroTLM.gin !== null && $scope.filtroTLM.gin !== "") {
    	    	cadenaFiltros += " GIN: " + $scope.filtroTLM.gin;
    	    }

    	    $('.mensajeBusquedaTLM').append(cadenaFiltros);
    	};


		 $scope.busquedaConciliacionTLM = function(){
//			 $scope.mostrarErrorFiltroFecha = $scope.validacionFiltroFecha();
			 $scope.showingFilterTLM = false;
			 $scope.tituloPersianaTLM = "Mostrar opciones de búsqueda";
			 angular.element('#buscadorTLM').slideUp();
			 if (!$scope.mostrarErrorFiltroFecha) {
				 
				inicializarLoading();
				ConciliacionBancariaService.busqueda(function(data) {
					
					if (data == null || data === "undefined") {
						fErrorTxt("Se produjo un error en la carga del listado.",1);
						return false;
					}else{ 
						if (typeof data === 'string') {
					
							fErrorTxt("Se produjo un error en la carga del listado.",1);
							return false;
						}else{
							$scope.resultadosTLM = data.resultados.resultados;

							// se inicializan las variables de clientes
							// seleccionados
							$scope.seleccionadosTLM = [];
							$scope.seleccionadosBorrarTLM = [];

							// borra el contenido del body de la tabla
							var tbl = $("#datosTLM > tbody");
							$(tbl).html("");
							$scope.oTableTLM.fnClearTable();

							if ($scope.resultadosTLM.length == 0) {

								$scope.isResultadosTLM = false;

							} else {

								$scope.isResultadosTLM = true;

								for (var i = 0; i < $scope.resultadosTLM.length; i++) {

									var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="resultadosTLM['+ i 
									+ '].selected" ng-click="seleccionarElementoTLM(' + i + ');" ng-disabled="isSeleccionadoTLM(resultadosTLM['+ i + '])"/>';

									var fila = [
												check,
												$scope.resultadosTLM[i].gin,
												$scope.resultadosTLM[i].tipoDeMovimiento,
												$scope.resultadosTLM[i].auxiliarBancario,
												$scope.resultadosTLM[i].conceptoMovimiento,
												$scope.resultadosTLM[i].importe,
												$scope.resultadosTLM[i].numeroDocumento,
												$scope.resultadosTLM[i].comentario,
												moment($scope.resultadosTLM[i].bookingDate).format('DD/MM/YYYY'),
												moment($scope.resultadosTLM[i].valueDate).format('DD/MM/YYYY'),
												$scope.resultadosTLM[i].antiguedad];

									$scope.oTableTLM.fnAddData(fila, false);

								}

							}
							$scope.oTableTLM.fnDraw();
					
						}
					}
					$.unblockUI();
				},
				function(error) {
					$.unblockUI();
					fErrorTxt("Se produjo un error en la carga del listado de TLM.",1);
				}, $scope.filtroTLM);
				$scope.seguimientoBusquedaTLM();
		 }
		 };
		 
		 
		 $scope.isSeleccionadoTLM = function(elemento){
			 var existe = false;
             if(elemento){
                 angular.forEach($scope.seleccionados, function (campo) {
                     if (campo.id === elemento.id) {
                         existe = true;
                     }
                 });
             }
			 return existe;
//			return $scope.seleccionados.indexOf(elemento)>0;
		 };
		 
					
		 $scope.rotarOperadorFiltroConceptoMovimientoTLM = function() {
			var i = $scope.operadoresFiltroTLM.indexOf($scope.sentidoFiltroTLM['conceptoMovimientoTLM']);
			if (i == $scope.operadoresFiltroTLM.length - 1) {
				$scope.sentidoFiltroTLM['conceptoMovimientoTLM'] = $scope.operadoresFiltroTLM[0];
			} else {
				$scope.sentidoFiltroTLM['conceptoMovimientoTLM'] = $scope.operadoresFiltroTLM[i + 1];
			}
		 }

		 $scope.rotarOperadorFiltroNumeroDocumentoTLM = function() {
			var i = $scope.operadoresFiltroTLM.indexOf($scope.sentidoFiltroTLM['numeroDocumentoTLM']);
			if (i == $scope.operadoresFiltroTLM.length - 1) {
				$scope.sentidoFiltroTLM['numeroDocumentoTLM'] = $scope.operadoresFiltroTLM[0];
			} else {
				$scope.sentidoFiltroTLM['numeroDocumentoTLM'] = $scope.operadoresFiltroTLM[i + 1];
			}
		 };

		 $scope.rotarOperadorFiltroComentarioTLM = function() {
			var i = $scope.operadoresFiltroTLM.indexOf($scope.sentidoFiltroTLM['comentarioTLM']);
			if (i == $scope.operadoresFiltroTLM.length - 1) {
				$scope.sentidoFiltroTLM['comentarioTLM'] = $scope.operadoresFiltroTLM[0];
			} else {
				$scope.sentidoFiltroTLM['comentarioTLM'] = $scope.operadoresFiltroTLM[i + 1];
			}
		 };

		 $scope.actualizarInformacionTLM = function() {
			angular.element('#archivoTLM').modal({
				backdrop : 'static'
			});
			$scope.showFormularioCargaFicheroTLM = true;
		 };
		 
		 /***************************************************
		 * ** ACCIONES TABLA ***
		 **************************************************/

		var seleccionarTodosTLM = function() {
			$scope.seleccionadosBorrarTLM = [];
			for (var i = 0; i < $scope.resultadosTLM.length; i++) {
				if(!$scope.isSeleccionadoTLM($scope.resultadosTLM[i])){
					$scope.resultadosTLM[i].selected = true;
					$scope.seleccionadosBorrarTLM.push($scope.resultadosTLM[i].idInforme);
					$scope.seleccionadosTLM.push($scope.resultadosTLM[i]);
				}
			}
			
			$scope.$emit("seleccionaTLMDcha", $scope.seleccionadosTLM);
		};
		
		$scope.$on("seleccionarTodosTLM", seleccionarTodosTLM);

		var deseleccionarTodosTLM = function() {
			for (var i = 0; i < $scope.resultadosTLM.length; i++) {
				$scope.resultadosTLM[i].selected = false;
			}
			$scope.seleccionadosTLM = [];
			$scope.seleccionadosBorrarTLM = [];
			$scope.$emit("seleccionaTLMDcha", $scope.seleccionadosTLM);
		};
		
		$scope.$on("deseleccionarTodosTLM", deseleccionarTodosTLM);

		$scope.seleccionarElementoTLM = function(row) {
			if ($scope.resultadosTLM[row] != null && $scope.resultadosTLM[row].selected) {
				$scope.resultadosTLM[row].selected = false;
				for (var i = 0; i < $scope.seleccionadosTLM.length; i++) {
					if ($scope.seleccionadosTLM[i].id === $scope.resultadosTLM[row].id) {
						$scope.seleccionadosTLM.splice(i, 1);
					}
				}
				$scope.$emit("seleccionaTLMDcha", $scope.seleccionadosTLM);
			} else {
				$scope.resultadosTLM[row].selected = true;
				$scope.seleccionadosTLM.push($scope.resultadosTLM[row]);
				$scope.seleccionadosBorrarTLM.push($scope.resultadosTLM[row].id);
				$scope.$emit("seleccionaTLMDcha", $scope.seleccionadosTLM);
			}
		};
		
		 $scope.$watch('seleccionadosTLM', function (newVal, oldVal) {
             if (newVal !== oldVal && newVal) {
             	
				var importeDebe = 0;
				var importeHaber = 0;
				
			 	angular.forEach($scope.seleccionadosTLM, function (partida) {
			 		if(partida.tipoDeMovimiento.trim() == Constantes.THEIR_CASH_CREDIT||
			 				partida.tipoDeMovimiento.trim() == Constantes.OUR_CASH_DEBIT){
			 			importeDebe = importeDebe + partida.importe;
			 		}
			 		if(partida.tipoDeMovimiento.trim() == Constantes.OUR_CASH_CREDIT ||
			 				partida.tipoDeMovimiento.trim() ==  Constantes.THEIR_CASH_DEBIT){
			 			importeHaber = importeHaber + partida.importe;
			 			
			 		}
                });
			 	var importePyG = importeDebe - importeHaber;
			 	
			 	$scope.$emit("calculaImporte", importeDebe, importeHaber, importePyG);
				 	
             }
           }, true);
		
		/***************************************************
		 * ** ACCION GENERAR APUNTE ***
		 **************************************************/
		 
		
		var generarApunteTLM = function(event, listSeleccionadosIzq, concepto){
			if(listSeleccionadosIzq.length>0 && $scope.seleccionadosTLM.length>0){
				angular.element('#generarApunteModal').modal('hide');
				angular.element("#dialog-confirm").html("¿Esta seguro de que desea realizar el apunte contable?");
				 angular.element("#dialog-confirm").dialog({
					 resizable : false,
					 modal : true,
					 title : "Mensaje de Confirmación",
					 height : 150,
					 width : 360,
					 buttons : {
						 " Sí " : function () {
								inicializarLoading();
				        		ConciliacionBancariaTLMService.generarApuntes(function (data) {
				        			if (data.resultados.status === 'KO') {
  				  						fErrorTxt("Error en la peticion de datos al generar apuntes.", 1);
  				  					}else if (data.resultados.status === 'KOSemaforo') {
  				  						fErrorTxt(data.error, 1);
  				  					}else{
				        				fErrorTxt("Los apuntes se generaron correctamente.", 3);
				        				$scope.resultadosTLM = [];
				        				$scope.seleccionadosTLM = [];
				        				$scope.seleccionadosBorrarTLM = [];
				        				$scope.resetFormTLM();
				        				$scope.$emit("resetPantalla");
				        			}
				        			$.unblockUI();
				    	    	}, function (error) {
				    	    		fErrorTxt("Se produjo un error al generar los apuntes en el fichero XLS.", 1);
				    	    		$.unblockUI();
				    	    	}, {listaPartidasTLMIzq: listSeleccionadosIzq, listaPartidasTLMDcha: $scope.seleccionadosTLM, concepto: concepto, auditUser : $rootScope.userName});	
							 $(this).dialog('close');
						 }," No " : function () {
							 angular.element('#generarApunteModal').modal({backdrop : 'static'});
							 $(".modal-backdrop").remove();
							 $(this).dialog('close');
						 }
					 }
	             });
				 $('.ui-dialog-titlebar-close').remove();
        	}else{
        		fErrorTxt("Debe seleccionar al menos una partida TLM en las dos areas de la pantalla.", 1);
        	}
		};
		
		$scope.$on("generarApunteTLM", generarApunteTLM);
		
		 /** Validacion de campos del filtro de Fecha. */
		 $scope.validacionFiltroFecha = function () {
			 if (($scope.filtroTLM.bookingDateDesde == '' && $scope.filtroTLM.bookingDateHasta != '') || 
				 ($scope.filtroTLM.valueDateDesde == '' && $scope.filtroTLM.valueDateHasta != '') || 
				 ($scope.filtroTLM.bookingDateDesde != '' && $scope.filtroTLM.valueDateDesde != '') ||
				 ($scope.filtroTLM.bookingDateDesde == '' && $scope.filtroTLM.bookingDateHasta == '' && $scope.filtroTLM.valueDateDesde == '' && $scope.filtroTLM.valueDateHasta == '')) {
				 return true;
			 } else {
				 return false;
			 }
		 };
		 
		 $scope.procesarTipoMovimiento = function () {
			 var existe = false;
			 angular.forEach($scope.filtroTLM.tiposMovimientos, function (campo) {
				 if (campo.key === $scope.filtroTLM.tipoMovimiento.key) {
					 existe = true;
				 }
			 });
			 if (!existe) {
				 $scope.filtroTLM.tiposMovimientos.push($scope.filtroTLM.tipoMovimiento);
			 }
			 $scope.filtroTLM.tipoMovimiento = "";
		 };

		 $scope.procesarAuxiliarBancarioTLM = function () {
			 var existe = false;
			 angular.forEach($scope.filtroTLM.auxiliares, function (campo) {
				 if (campo === $scope.filtroTLM.auxiliar) {
					 existe = true;
				 }
			 });
			 if (!existe) {
				 $scope.filtroTLM.auxiliares.push($scope.filtroTLM.auxiliar);
			 }
			 $scope.filtroTLM.auxiliar = "";
		 };
		 
		 $scope.procesarAntiguedadTLM = function () {
			 var existe = false;
			 angular.forEach($scope.filtroTLM.antiguedades, function (campo) {
				 if (campo === $scope.filtroTLM.antiguedad.key) {
					 existe = true;
				 }
			 });
			 if (!existe) {
				 $scope.filtroTLM.antiguedades.push($scope.filtroTLM.antiguedad.key);
			 }
			 $scope.filtroTLM.antiguedad = "";
		 };
		 
		 $scope.mostrarOcultarFiltrosTLM = function () {
			 if ($scope.showingFilterTLM == true) {
				 $scope.showingFilterTLM = false;
				 angular.element('#buscadorTLM').slideUp();
				 $scope.tituloPersianaTLM = "Mostrar opciones de búsqueda";
			 } else {
				 $scope.showingFilterTLM = true;
				 angular.element('#buscadorTLM').slideDown();
				 $scope.tituloPersianaTLM = "Ocultar opciones de búsqueda";
			 }
		 };
		 
} ]);	
