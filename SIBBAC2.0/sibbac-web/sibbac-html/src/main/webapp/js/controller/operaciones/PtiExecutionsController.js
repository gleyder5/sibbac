/**
 * 
 */
(function(angular, sibbac20, gc, undefined){
  "use strict";
  
    var PtiExecutionsController = function(service, $httpParamSerializer, uiGridConstants) {
      gc.DynamicFiltersController.call(this, service, $httpParamSerializer, uiGridConstants);
      this.selectedQuery = {name: ""};
      this.selectQuery();
      this["combo-CDSENTIDO"] = [{label: ""},
        {label: "Compra", value: "C"},
        {label: "Venta", value: "V"}];
      
      this["combo-CDMIEMBROMKT"] = [{label: ""},
        {label: "Madrid", value: "9838"},
        {label: "Barcelona", value: "9472"},
        {label: "Bilbao", value: "9577"},
        {label: "Valencia", value: "9370"}];
      
      this["auto-CDISIN"] = {minLength : 2, 
        source: service.autocompleter("isin")
      };
    };
    
    PtiExecutionsController.prototype = Object.create(gc.DynamicFiltersController.prototype);
    
    PtiExecutionsController.prototype.constructor = PtiExecutionsController;
    
    PtiExecutionsController.prototype.calculateFollowSearch = function() {
      return GENERIC_CONTROLLERS.FilteredGridController.prototype.calculateFollowSearch.call(this);
    }
  
    sibbac20.controller("PtiExecutionsController",["PtiExecutionsService",   
    "$httpParamSerializer", "uiGridConstants", PtiExecutionsController]);
    
    
})(angular, sibbac20, GENERIC_CONTROLLERS);

