angular.module('sibbac20').controller(['ConciliacionPropiaTituloController', '$scope', 'IsinService', function($scope, IsinService){
	$scope.isines = [];

	/** ISINES **/
    IsinService.getIsines().success(function(data, status, headers, config) {
        console.log("ConciliacionPropiaTituloController isines cargados: "+data);
        if(data != null && data.resultados != null &&
        		data.resultados.result_isin != null){
        	$scope.isines = data.resultados.result_isin;
        }
      }).error(function(data, status, headers, config) {
        console.log("error al cargar isines: "+status);
      });
}]);
