'use strict';
sibbac20.controller('ActualizacionMasivaTitularesController', ['$scope', '$http', '$rootScope','IsinService', function ($scope, $http, $rootScope, IsinService) {

	'use strict';
	    var self = this;

	var nudnicifold = "";
	var nbclient2old = "";
	var provinciaold = "";
	var nbclientold = "";
	var nbclient1old = "";
	var paisResold = "";
	var tpdomiciold = "";
	var nbdomiciold = "";
	var nudomiciold = "";
	var codpostalold = "";
	var tpsociedold = "";
	var tptiPrepold = "";
	var tipoDocumentoold = "";
	var tipnacold = "";
	var participold = "";
	var nurefordold = "";
	var cdnactiold = "";
	var ciudadold = "";
	var ccvold = "";
	var vaciosCampos = false;
	var vaciosSelect = false;

	var saltaFiltro = false;

	var camposArray = ["fechaDesde", "fechaHasta", "provinciaold", "ciudadold", "ccvold",
	                "nbclientold", "nbclient2old", "codpostalold", "nbdomiciold", "nurefordold",
	                "nbclient1old", "nudnicifold", "nudomiciold", "participold"
	                ];

	var selectArray = ["tpsociedold", "tptiPrepold", "tipnacold", "tpdomiciold",
	                   "tipoDocumentoold", "cdnactiold", "paisResold"
	                   ];

	//


	function cargarDatosActualizacionMasivaTitulares(valorSaltaFiltro) {
		  //console.log("traemos: ",valorSaltaFiltro);
		  console.log("cargarDatosActualizacionMasivaTitulares");

		  var filtro = "{\"fechaDesde\" : \""+ transformaFechaInv($("#fechaDesde").val()) + "\"," +
		  "\"fechaHasta\" : \""+ transformaFechaInv($("#fechaHasta").val()) + "\"," +
		  "\"tipoDocumentoold\" : \""+ tipoDocumentoold + "\"," +
		  "\"provinciaold\" : \""+ provinciaold + "\"," +
		  "\"ciudadold\" :\""+ ciudadold + "\"," +
		  "\"ccvold\" : \""+ ccvold + "\"," +
		  "\"nbclientold\" : \""+ nbclientold + "\"," +
		  "\"nbclient2old\" : \""+ nbclient2old + "\"," +
		  "\"tipnacold\" : \""+ tipnacold + "\"," +
		  "\"codpostalold\" : \""+ codpostalold + "\"," +
		  "\"nbdomiciold\" : \""+ nbdomiciold + "\"," +
		  "\"tptiPrepold\" : \""+ tptiPrepold + "\"," +
		  "\"nurefordold\" : \""+ nurefordold + "\"," +
		  "\"nbclient1old\" : \""+ nbclient1old + "\"," +
		  "\"nudnicifold\" : \""+ nudnicifold + "\"," +
		  "\"paisResold\" : \""+ paisResold + "\"," +
		  "\"tpdomiciold\" : \""+ tpdomiciold+ "\"," +
		  "\"nudomiciold\" : \""+ nudomiciold+ "\"," +
		  "\"participold\" : \""+ participold + "\"," +
		  "\"cdnactiold\" : \""+ cdnactiold + "\"," +
		  "\"tpsociedold\" : \""+ tpsociedold + "\"," +




		  "\"tipoDocumento\" : \""+ $("#tipoDocumento").val() + "\"," +
		  "\"provincia\" : \""+ $("#provincia").val() + "\"," +
		  "\"ciudad\" :\""+ $("#ciudad").val() + "\"," +
		  "\"ccv\" : \""+ $("#ccv").val() + "\"," +
		  "\"nbclient\" : \""+ $("#nbclient").val() + "\"," +
		  "\"nbclient2\" : \""+ $("#nbclient2").val() + "\"," +
		  "\"tipnac\" : \""+ $("#tipnac").val() + "\"," +
		  "\"codpostal\" : \""+ $("#codpostal").val() + "\"," +
		  "\"nbdomici\" : \""+ $("#nbdomici").val() + "\"," +
		  "\"tptiPrep\" : \""+ $("#tptiPrep").val() + "\"," +
		  "\"nbclient1\" : \""+ $("#nbclient1").val() + "\"," +
		  "\"nudnicif\" : \""+ $("#nudnicif").val() + "\"," +
		  "\"paisRes\" : \""+ $("#paisRes").val() + "\"," +
		  "\"tpdomici\" : \""+ $("#tpdomici").val() + "\"," +
		  "\"nudomici\" : \""+ $("#nudomici").val() + "\"," +
		  "\"particip\" : \""+ $("#particip").val() + "\"," +
		  "\"cdnacti\" : \""+ $("#cdnacti").val() + "\"," +
		  "\"tpsocied\" : \""+ $("#tpsocied").val() + "\"," +
		  "\"saltaFiltro\" : \""+ valorSaltaFiltro + "\" }";

	//saltaFiltro


			var data = new DataBean();
			data.setService('SIBBACServiceTitulares');
			data.setAction('updateTitularesMassive');
			data.setFilters(filtro);
			var request = requestSIBBAC( data );
			request.success(function(json){


				console.log(json);
		        console.log(json.resultados);
		        if (json.resultados != null && json.resultados !== undefined) {
		        	var modiConError = json.resultados.nAfiErrorProcessed;
		            var modiSinError = json.resultados.nAfiProcessed;
		            var dejarErrores = json.resultados.nAfiErrorFixed;
		        	//desde aqui
		        	if (json.resultados.mensaje != null && json.resultados.mensaje !== undefined && json.resultados.mensaje != ""){
		        		//console.log("Mensaje: ",json.resultados.mensaje);
	            		if (json.resultados.esBloqueante != null && json.resultados.esBloqueante !== undefined){
	            			if(json.resultados.esBloqueante){
	            				//console.log("esBloqueante: ",json.resultados.esBloqueante);
	                			fErrorTxt(json.resultados.mensaje, 2);
	            			}else{
	            				var error = fErrorTxtRespuestaModificar(json.resultados.mensaje);
	            			}
	            		}else{
	            			//console.log("esBloqueante es nulo o indefinido ");
	            			var error = fErrorTxtRespuestaModificar(json.resultados.mensaje);
	            		}
	            	} else {
	           		fErrorTxt("Modificados " + modiConError + " titulares con errores. </br> Modificados " + modiSinError + " titulares sin errores. </br>" + dejarErrores + " titulares dejaron de tener errores.", 3);
	            	}
		        	//alert( "Modificados " + modiConError + " titulares con errores. \n Modificados. " + modiSinError + " titulares sin errores. \n" + dejarErrores + " titulares dejaron de tener errores.");

		        }else{
		        	fErrorTxt(json.error, 2);
		            //alert("Error en la actualización de los datos")
		        }
			 });
	}
		function initialize() {

			  //$('#tipoDocumentoold').val("No filtrar");
			  //$('#nudnicifold').val("No filtrar");
			  //$('#provinciaold').val("No filtrar");
			  //$('#ciudadold').val("No filtrar");
			  //$('#ccvold').val("No filtrar");
			  //$('#nbclientold').val("No filtrar");
			  //$('#nbclient2old').val("No filtrar");
			  //$('#tipnacold').val("No filtrar");
			  //$('#codpostalold').val("No filtrar");
			  //$('#nbdomiciold').val("No filtrar");
			  //$('#nbclient1old').val("No filtrar");
			  //$('#paisResold').val("No filtrar");
			  //$('#tpdomiciold').val("No filtrar");
			  //$('#nudomiciold').val("No filtrar");
			  //$('#nbdomiciold').val("No filtrar");
			  //$('#tpsociedold').val("No filtrar");
			  //$('#tptiPrepold').val("No filtrar");
			  //$('#participold').val("No filtrar");
			  //$('#nurefordold').val("No filtrar");
			  //$('#cdnactiold').val("No filtrar");
				//var fechas= ["fechaDesde","fechaHasta"];
				//loadpicker(fechas);
				//verificarFechas([["fechaDesde","fechaHasta"]]);

			$("#ui-datepicker-div").remove();
	        $('input#fechaDesde').datepicker({
	            onClose: function( selectedDate ) {
	              $( "#fechaHasta" ).datepicker( "option", "minDate", selectedDate );
	            } // onClose
	          });

	          $('input#fechaHasta').datepicker({
	            onClose: function( selectedDate ) {
	              $( "#fechaDesde" ).datepicker( "option", "maxDate", selectedDate );
	            } // onClose
	          });

	            cargarComboNacionalidad();
				cargarComboTipoNacionalidad();
				cargarComboTipoPersona();
				cargarComboTipoPre();
				cargarComboTipoDocumento();
				cargarComboTipoDomicilio();
				cargarComboPaisRes();

				apagarChecks();
			}

		 $(document).ready(function () {
			 //

			 initialize();

		    $('.btn.buscador').click(function (event) {

		    	vaciosCampos = verificarcamposVacios();
		    	vaciosSelect = verificarSelectVacios();
		    	if (vaciosCampos == undefined && vaciosSelect == undefined){
		    		var mensajeCamposvacios = "La consulta debe contener al menos un parametro de busqueda";
		    		fErrorTxt(mensajeCamposvacios, 2);
		    	}else{

			    	fechasNoMayorAnio();
			    	//
					  if($('#tipoDocumentoold').val()=== "No filtrar"){
						  tipoDocumentoold ="";
					  }else if($('#tipoDocumentoold').val()=== "" || $('#tipoDocumentoold').val()===undefined){
						  //tipoDocumentoold ="";
						  tipoDocumentoold ="<null>";
					  }else{
						  tipoDocumentoold =$('#tipoDocumentoold').val();
					  }
					  if($('#nudnicifold').val() != ""){
						  nudnicifold = $('#nudnicifold').val();
					  }
					  if($('#provinciaold').val() != ""){
						  provinciaold = $('#provinciaold').val();
					  }
					  if($('#ciudadold').val() != ""){
						  ciudadold = $('#ciudadold').val();
					  }
					  if($('#ccvold').val() != ""){
						  ccvold = $('#ccvold').val();
					  }
					  if($('#nbclientold').val() != ""){
						  nbclientold = $('#nbclientold').val();
					  }
					  if($('#nbclient2old').val() != ""){
						  nbclient2old = $('#nbclient2old').val();
					  }
					  if($('#codpostalold').val() != ""){
						  codpostalold = $('#codpostalold').val();
					  }
					  if($('#nbdomiciold').val() != ""){
						  nbdomiciold = $('#nbdomiciold').val();
					  }
					  if($('#nbclient1old').val() != ""){
						  nbclient1old = $('#nbclient1old').val();
					  }
					  if($('#nudomiciold').val() != ""){
						  nudomiciold = $('#nudomiciold').val();
					  }
					  if($('#participold').val() != ""){
						  participold = $('#participold').val();
					  }
					  if($('#nurefordold').val() != ""){
						  nurefordold = $('#nurefordold').val();
					  }




					  if($('#tipnacold').val()=== "No filtrar"){
						  tipnacold ="";
					  }else if($('#tipnacold').val()=== "" || $('#tipnacold').val()===undefined){
						  //tipnacold ="";
						  tipnacold ="<null>";
					  }else{
						  tipnacold =$('#tipnacold').val();
					  }


					  if($('#tptiPrepold').val()=== "No filtrar"){
						  tptiPrepold ="";
					  }else if($('#tptiPrepold').val()=== "" || $('#tptiPrepold').val()===undefined){
						  //tptiPrepold ="";
						 tptiPrepold ="<null>";
					  }else{
						  tptiPrepold =$('#tptiPrepold').val();
					  }


					  if($('#tpsociedold').val()=== "No filtrar"){
						  tpsociedold ="";
					  }else if($('#tpsociedold').val()=== "" || $('#tpsociedold').val()===undefined){
						  tpsociedold ="<null>";
						  //tpsociedold ="";
					  }else{
						  tpsociedold =$('#tpsociedold').val();
					  }

					  if($('#paisResold').val()=== "No filtrar"){
						  paisResold ="";
					  }else if($('#paisResold').val()=== "" || $('#paisResold').val()===undefined){
						  paisResold ="<null>";
						  //paisResold ="";
					  }else{
						  paisResold =$('#paisResold').val();
					  }


					  if($('#cdnactiold').val()=== "No filtrar"){
						  cdnactiold ="";
					  }else if($('#cdnactiold').val()=== "" || $('#cdnactiold').val()===undefined){
						  //cdnactiold ="";
						  cdnactiold ="<null>";
					  }else{
						  cdnactiold =$('#cdnactiold').val();
					  }


					  if($('#tpdomiciold').val()=== "No filtrar"){
						  tpdomiciold ="";
					  }else if($('#tpdomiciold').val()=== "" || $('#tpdomiciold').val()===undefined){
						  //tpdomiciold ="";
						  tpdomiciold ="<null>";
					  }else{
						  tpdomiciold =$('#tpdomiciold').val();
					  }
			    	cargarDatosActualizacionMasivaTitulares(false);
			        event.preventDefault();
			        seguimientoBusqueda();
			        return false;

		    	}
		    });




			$('#limpiarValores').click(function(event) {
				//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
	            $( "#fechaHasta" ).datepicker( "option", "minDate", "");
	            $( "#fechaDesde" ).datepicker( "option", "maxDate", "");

	            //
				apagarChecks();
				event.preventDefault();
				$('input[type=text]').val('');
				for (var i = 0; i < camposArray.length ; i++){
					vartemporal = $('#'+camposArray[i]).val();
					vartemporal = "";


				}
				for (var i = 0; i < selectArray.length ; i++){
					vartemporal = $('#'+selectArray[i]).val();
					vartemporal = "";


				}
				//
				/*
				//Codigo optimizado por Alex 02Feb
				document.getElementById("fechaDesde").value = '';
				document.getElementById("fechaHasta").value = '';
				document.getElementById("provinciaold").value = '';
				document.getElementById("ciudadold").value = '';
				document.getElementById("ccvold").value = '';
				document.getElementById("nbclientold").value = '';
				document.getElementById("nbclient2old").value = '';
				document.getElementById("codpostalold").value = '';
				document.getElementById("nbdomiciold").value = '';
				document.getElementById("nurefordold").value = '';
				document.getElementById("nbclient1old").value = '';
				document.getElementById("nudnicifold").value = '';
				document.getElementById("nudomiciold").value = '';
				document.getElementById("participold").value = '';

				document.getElementById("tipoDocumento").value = '';
				document.getElementById("provincia").value = '';
				document.getElementById("ciudad").value = '';
				document.getElementById("ccv").value = '';
				document.getElementById("nbclient").value = '';
				document.getElementById("nbclient2").value = '';
				document.getElementById("tipnac").value = '';
				document.getElementById("codpostal").value = '';
				document.getElementById("nbdomici").value = '';
				document.getElementById("tptiPrep").value = '';
				document.getElementById("nbclient1").value = '';
				document.getElementById("nudnicif").value = '';
				document.getElementById("paisRes").value = '';
				document.getElementById("tpdomici").value = '';
				document.getElementById("nudomici").value = '';
				document.getElementById("particip").value = '';
				document.getElementById("tpsocied").value = '';
				document.getElementById("cdnacti").value = '';
				//desde aqui limpialos los values
				//$('#fechaDesde').val() = "";
				*/
				//
				fechaHasta = "";
				provinciaold = "";
				ciudadold = "";
				ccvold = "";
				nbclientold = "";
				nbclient2old = "";
				codpostalold = "";
				nbdomiciold = "";
				nurefordold = "";
				nbclient1old = "";
				nudnicifold = "";
				nudomiciold = "";
				participold = "";
				tipoDocumento = "";
				provincia = "";
				ciudad = "";
				ccv = "";
				nbclient = "";
				nbclient2 = "";
				tipnac = "";
				codpostal = "";
				nbdomici = "";
				tptiPrep = "";
				nbclient1= "";
				nudnicif = "";
				paisRes = "";
				tpdomici = "";
				nudomici = "";
				particip = "";
				tpsocied = "";
				cdnacti = "";
				//

				cleanOptions(document.getElementById("tpsociedold"));
				cleanOptions(document.getElementById("tptiPrepold"));
				cleanOptions(document.getElementById("tipnacold"));
				cleanOptions(document.getElementById("tpdomiciold"));
				cleanOptions(document.getElementById("tipoDocumentoold"));
				cleanOptions(document.getElementById("cdnactiold"));
				cleanOptions(document.getElementById("paisResold"));
				//removeOptions(document.getElementById("XXX"));

				//apagarChecks();


			});

		 });
		 function cleanOptions(selectbox)
		 {
		     var i;
		     for(i=selectbox.options.length-1;i>=0;i--)
		     {
		         selectbox.selectedIndex=0;
		     }
		 }
		 //using the function:

		 function cargarComboTipoPersona() {

				var data = new DataBean();
				data.setService('SIBBACServiceTitulares');
				data.setAction('getTpsociedList');

				var request = requestSIBBAC(data);
				var item;
				request.success(function(json) {

			        if (json.resultados == null) {
			            if(json.error !== undefined ){
			                alert(json.error);
			            }else{
			                alert("Error indeterminado con la Base de Datos.");
			            }
			        } else {
						var datos = json.resultados.resultados_tpsocied;
						for ( var k in datos) {
							item = datos[k];
							$('#tpsociedold').append("<option value='" + item.codigo + "'>(" +item.codigo+ ") " + item.descripcion + "</option>");
							$('#tpsocied').append("<option value='" + item.codigo + "'>(" +item.codigo+ ") " + item.descripcion + "</option>");
						}

					}
				});

			}



		 function cargarComboTipoNacionalidad() {

				var data = new DataBean();
				data.setService('SIBBACServiceTitulares');
				data.setAction('getTpnactitList');
				var item;
				var request = requestSIBBAC(data);
				request.success(function(json) {

			        if (json.resultados == null) {
			            if(json.error !== undefined ){
			                alert(json.error);
			            }else{
			                alert("Error indeterminado con la Base de Datos.");
			            }
			        } else {
						var datos = json.resultados.resultados_tpnactit;
						for ( var k in datos) {
							item = datos[k];
							$('#tipnacold').append("<option value='" + item.codigo + "'>(" +item.codigo+ ") " + item.descripcion + "</option>");
							$('#tipnac').append("<option value='" + item.codigo + "'>(" +item.codigo+ ") " + item.descripcion + "</option>");
						}

					}
				});

			}


		 function cargarComboNacionalidad() {

			    var data = new DataBean();
			    data.setService('SIBBACServiceTitulares');
			    data.setAction('getNacionalidadList');
			    var request = requestSIBBAC(data);
			    var item;
			    request.success(function(json) {
			        if (json.resultados == null) {
			            if(json.error !== undefined ){
			                alert(json.error);
			            }else{
			                alert("Error indeterminado con la Base de Datos.");
			            }
			        } else {

			            var datos = json.resultados.resultados_nacionalidad;

			            for ( var k in datos) {

			                item = datos[k];

			                $('#cdnactiold').append("<option value='" + item.cdispais + "'>" + item.cdiso2po + " - " + item.nbispais + "(" +item.cdispais+") </option>");
			                $('#cdnacti').append("<option value='" + item.cdispais + "'>" + item.cdiso2po + " - " + item.nbispais + "(" +item.cdispais+") </option>");
			            }
			        }
			    });
			}



		 function cargarComboTipoPre() {

				var data = new DataBean();
				data.setService('SIBBACServiceTitulares');
				data.setAction('getTptiprepList');
				var request = requestSIBBAC(data);
				 var item;
				request.success(function(json) {

			        if (json.resultados == null) {
			            if(json.error !== undefined ){
			                alert(json.error);
			            }else{
			                alert("Error indeterminado con la Base de Datos.");
			            }
			        } else {
						var datos = json.resultados.resultados_tptiprep;
						for ( var k in datos) {
							item = datos[k];
							$('#tptiPrepold').append("<option value='" + item.codigo + "'>(" +item.codigo+ ") " + item.descripcion + "</option>");
							$('#tptiPrep').append("<option value='" + item.codigo + "'>(" +item.codigo+ ") "+ item.descripcion + "</option>");
						}

					}
				});

			}


		 function fechasNoMayorAnio() {


				if ($('#fechaDesde').val() !== "" && $('#fechaDesde').val() !== undefined
					&& $('#fechaHasta').val() !== "" && $('#fechaHasta').val() !== undefined) {

					var fecha1=$('#fechaDesde').val();
					var fecha2=$('#fechaHasta').val();
					var date1 = fecha1.split('/');
					var date2 = fecha2.split('/')


					var dateAnterior=[];
					dateAnterior[0]=date1[1];
					dateAnterior[1]=date1[0];
					dateAnterior[2]=date1[2];
					var datePosterior=[];
					datePosterior[0]=date2[1];
					datePosterior[1]=date2[0];
					datePosterior[2]=date2[2];
					var date11 = new Date(dateAnterior);
					var date22 = new Date(datePosterior);
					var timeDiff = Math.abs(date22.getTime() - date11.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

			 		if(diffDays > 366){

						alert('Entre fecha inicio y fecha hasta no debe superar un año')
						document.getElementById(fechaHasta).focus();

					}
				}
		 }


		 function cargarComboTipoDocumento() {

				var data = new DataBean();
				data.setService('SIBBACServiceTitulares');
				data.setAction('getTpidentiList');
				var request = requestSIBBAC(data);
				var item;
				request.success(function(json) {

			        if (json.resultados == null) {
			            if(json.error !== undefined ){
			                alert(json.error);
			            }else{
			                alert("Error indeterminado con la Base de Datos.");
			            }
			        } else {
						var datos = json.resultados.resultados_tpidenti;
						for ( var k in datos) {
							item = datos[k];
							$('#tipoDocumentoold').append("<option value='" + item.codigo + "'>(" +item.codigo+ ") " + item.descripcion + "</option>");
							$('#tipoDocumento').append("<option value='" + item.codigo + "'>(" +item.codigo+ ") " + item.descripcion + "</option>");
						}
					}
				});

			}

		 function cargarComboTipoDomicilio() {

				var data = new DataBean();
				data.setService('SIBBACServiceTitulares');
				data.setAction('getTpDomiciList');
				var request = requestSIBBAC(data);
				var item;
				request.success(function(json) {

			        if (json.resultados == null) {
			            if(json.error !== undefined ){
			                alert(json.error);
			            }else{
			                alert("Error indeterminado con la Base de Datos.");
			            }
			        } else {
						var datos = json.resultados.resultados_tpdomici;
						for ( var k in datos) {
							item = datos[k];
							$('#tpdomiciold').append("<option value='" + item.cdviapub + "'>" +item.cdviapub+ " - " + item.nbviapub + "</option>");
							$('#tpdomici').append("<option value='" + item.cdviapub + "'>" +item.cdviapub+ " - " + item.nbviapub + "</option>");
						}
					}
				});

			}


		 function cargarComboPaisRes() {

				var data = new DataBean();
				data.setService('SIBBACServiceTitulares');
				data.setAction('getPaisResList');
				var request = requestSIBBAC(data);
				var item;
				request.success(function(json) {

			        if (json.resultados == null) {
			            if(json.error !== undefined ){
			                alert(json.error);
			            }else{
			                alert("Error indeterminado con la Base de Datos.");
			            }
			        } else {
						var datos = json.resultados.resultados_paisres;
						for ( var k in datos) {
							item = datos[k];
							$('#paisResold').append("<option value='" + item.id.cdhppais + "'>" +item.cdiso2po+ " - " + item.nbhppais + " (" +item.id.cdhppais+") </option>");
							$('#paisRes').append("<option value='" + item.id.cdhppais + "'>" +item.cdiso2po+ " - " + item.nbhppais + " (" +item.id.cdhppais+")</option>");
						}
					}
				});

			}




			function seguimientoBusqueda() {


			    $('.mensajeBusqueda').empty();

			    var cadenaFiltros = "";

			    if ($('#fechaDesde').val() !== "" && $('#fechaDesde').val() !== undefined) {
			        cadenaFiltros += " F. Actualizacion Desde:" + $('#fechaDesde').val();
			    }
			    if ($('#fechaHasta').val() !== "" && $('#fechaHasta').val() !== undefined) {
			        cadenaFiltros += " F. Actualizacion Hasta:" + $('#fechaHasta').val();
			    }
			    if ($('#tipoDocumentoold').val() !== "" && $('#tipoDocumentoold').val() !== undefined) {
			        cadenaFiltros += " Tipo Documento:" + tipoDocumentoold;
			    }
			    if ($('#provinciaold').val() !== "" && $('#provinciaold').val() !== undefined) {
			        cadenaFiltros += " Provincia:" + provinciaold;
			    }

			    if ($('#ciudadold').val() !== "" && $('#ciudadold').val() !== undefined) {
			        cadenaFiltros += " Ciudad:" + $('#ciudadold').val();
			    }
			    if ($('#ccvold').val() !== "" && $('#ccvold').val() !== undefined) {
			        cadenaFiltros += " CCV:" + ccvold;
			    }
			    if ($('#nbclientold').val() !== "" && $('#nbclientold').val() !== undefined) {
			        cadenaFiltros += " Nombre Cliente:" + nbclientold;
			    }

			    if ($('#nbclient2old').val() !== "" && $('#nbclient2old').val() !== undefined) {
			        cadenaFiltros += " Segundo Apellido:" + nbclient2old;
			    }

			    if ($('#tipnacold').val() !== "" && $('#tipnacold').val() !== undefined) {
			        cadenaFiltros += " Tipo Nacionalidad:" + tipnacold;
			    }
			    if ($('#codpostalold').val() !== "" && $('#codpostalold').val() !== undefined) {
			        cadenaFiltros += " Codigo Postal:" + codpostalold;
			    }
			    if ($('#nbdomiciold').val() !== "" && $('#nbdomiciold').val() !== undefined) {
			        cadenaFiltros += " Nombre Domicilio:" + nbdomiciold;
			    }
			    if ($('#tptiPrepold').val() !== "" && $('#tptiPrepold').val() !== undefined) {
			        cadenaFiltros += " Tipo Prep:" + tptiPrepold;
			    }

			    if ($('#nurefordold').val() !== "" && $('#nurefordold').val() !== undefined) {
			        cadenaFiltros += " Nro Referencia:" + nurefordold;
			    }

			    if ($('#nbclient1old').val() !== "" && $('#nbclient1old').val() !== undefined) {
			        cadenaFiltros += " Primer Apellido:" + nbclient1old;
			    }

			    if ($('#nudnicifold').val() !== "" && $('#nudnicifold').val() !== undefined) {
			        cadenaFiltros += " Nro Cif :" + nudnicifold;
			    }

			    if ($('#paisResold').val() !== "" && $('#paisResold').val() !== undefined) {
			        cadenaFiltros += " Pais:" + paisResold;
			    }

			    if ($('#tpdomiciold').val() !== "" && $('#tpdomiciold').val() !== undefined) {
			        cadenaFiltros += " Tipo Domicilio:" + tpdomiciold;
			    }

			    if ($('#nudomiciold').val() !== "" && $('#nudomiciold').val() !== undefined) {
			        cadenaFiltros += " Nro Domicilio:" + nudomiciold;
			    }

			    if ($('#participold').val() !== "" && $('#participold').val() !== undefined) {
			        cadenaFiltros += " Participacion:" + participold;
			    }


			    if ($('#tpsociedold').val() !== "" && $('#tpsociedold').val() !== undefined) {
			        cadenaFiltros += " Tipo Sociedad:" + tpsociedold;
			    }

			    if ($('#cdnactiold').val() !== "" && $('#cdnactiold').val() !== undefined) {
			        cadenaFiltros += " Nacionalidad:" + cdnactiold;
			    }

			    $('.mensajeBusqueda').append(cadenaFiltros);


			}
	//enviamos valor null para tomarlo en cuenta o no
	function CheckBox1(valor){
		if (valor == 3){
			if (nurefordold == "" && $('#nurefordold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#nurefordold').attr("placeholder", "Filtrar");
				nurefordold ="<null>";
			}else if(nurefordold == "<null>" && $('#nurefordold').val() == ""){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				nurefordold ="";
				$('#nurefordold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 4){
			//nbclientold = $('#nbclientold').val();
			if (nbclientold == "" && $('#nbclientold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#nbclientold').attr("placeholder", "Filtrar");
				nbclientold ="<null>";
			}else if(nbclientold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				nbclientold ="";
				$('#nbclientold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 5){
			//nbclient1old = $('#nbclient1old').val();
			if (nbclient1old == "" && $('#nbclient1old').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#nbclient1old').attr("placeholder", "Filtrar");
				nbclient1old ="<null>";
			}else if(nbclient1old == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				nbclient1old ="";
				$('#nbclient1old').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 6){
			//nbclient2old = $('#nbclient2old').val();
			if (nbclient2old == "" && $('#nbclient2old').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#nbclient2old').attr("placeholder", "Filtrar");
				nbclient2old ="<null>";
			}else if(nbclient2old == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				nbclient2old ="";
				$('#nbclient2old').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 9){
			//nudnicifold = $('#nudnicifold').val();
			if (nudnicifold == "" && $('#nudnicifold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#nudnicifold').attr("placeholder", "Filtrar");
				nudnicifold ="<null>";
			}else if(nudnicifold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				nudnicifold ="";
				$('#nudnicifold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 11){
			//ccvold = $('#ccvold').val();
			if (ccvold == "" && $('#ccvold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#ccvold').attr("placeholder", "Filtrar");
				ccvold ="<null>";
			}else if(ccvold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				ccvold ="";
				$('#ccvold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 12){
			//participold = $('#participold').val();
			if (participold == "" && $('#participold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#participold').attr("placeholder", "Filtrar");
				participold ="<null>";
			}else if(participold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				participold ="";
				$('#participold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 17){
			//nbdomiciold = $('#nbdomiciold').val();
			if (nbdomiciold == "" && $('#nbdomiciold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#nbdomiciold').attr("placeholder", "Filtrar");
				nbdomiciold ="<null>";
			}else if(nbdomiciold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				nbdomiciold ="";
				$('#nbdomiciold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 18){
			//nudomiciold = $('#nudomiciold').val();
			if (nudomiciold == "" && $('#nudomiciold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#nudomiciold').attr("placeholder", "Filtrar");
				nudomiciold ="<null>";
			}else if(nudomiciold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				nudomiciold ="";
				$('#nudomiciold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 19){
			//ciudadold = $('#ciudadold').val();
			if (ciudadold == "" && $('#ciudadold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#ciudadold').attr("placeholder", "Filtrar");
				ciudadold ="<null>";
			}else if(ciudadold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				ciudadold ="";
				$('#ciudadold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 20){
			//provinciaold = $('#provinciaold').val();
			if (provinciaold == "" && $('#provinciaold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#provinciaold').attr("placeholder", "Filtrar");
				provinciaold ="<null>";
			}else if(provinciaold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				provinciaold ="";
				$('#provinciaold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}else if(valor == 21){
			//codpostalold = $('#codpostalold').val();
			if (codpostalold == "" && $('#codpostalold').val() == "" ){
				$( '#imagen'+valor).fadeTo( "fast", 1 );
				$('#codpostalold').attr("placeholder", "Filtrar");
				codpostalold ="<null>";
			}else if(codpostalold == "<null>"){
				$( '#imagen'+valor).fadeTo( "fast", 0.2 );
				codpostalold ="";
				$('#codpostalold').attr("placeholder", "No Filtrar");
				$('#nurefordold').val()=== "No filtrar";
			}
		}
	}


	function apagarChecks(){
		//console.log("clean Checks");
		for (var i = 1 ; i <= 21 ; i++){
			$( '#imagen'+i).fadeTo( "fast", 0.2 );

		}
	}

	//ALEX 01FEB
	//estas 2 funciones fueron creadas para verificar que ninguno de los campos estan vacios.
	//Se debe buscar al menos con 1 parametro de busqueda
	function verificarcamposVacios(){

		for (var i = 0; i < camposArray.length ; i++){
			if ($('#'+camposArray[i]).val()==""){

			}else{
				return true;
			}
		}
	}
	function verificarSelectVacios(){

		for (var j = 0; j < selectArray.length ; j++){
			if ($('#'+selectArray[j]).val() == "No filtrar"){

			}else{
				return true;
			}
		}
	}


	function fErrorTxtRespuestaModificar(msgError) {

		$('#mensajeModificado').empty();
	    $('.ui-dialog-titlebar-close').remove();

	    var option = $(".mensajeCreado").dialog("option");

	        $("#mensajeModificado").dialog(option, "title", "Advertencia");
	        $('#mensajeModificado').append("<div class='dialogMensajeIncon'> <img src='/sibbac20/images/warning.png'/></div>");

	    var btnAceptar = $('.ui-dialog-buttonpane').find('button:contains("Aceptar")');

	    btnAceptar.width(100);
	    btnAceptar.height(25);
	    $('#mensajeModificado').append("<p class='dialogMensajeText'ALIGN=center>" + msgError + "</p>");
	    $('#mensajeModificado').dialog("open");


	}
	$("#mensajeModificado").dialog({
	    autoOpen: false,
	    modal: false,
	    width: 500,
	    buttons: [
					{
					    text: " Continuar ",
					    icons: {
					        primary: "ui-icon-trash",
					    },
					    class: "ui-button",
					    click: function () {
					        $(this).dialog("close");

					        //modificarTitulares(oTable, true);
					        cargarDatosActualizacionMasivaTitulares(true);
					    }},
						{
						    text: " Cancelar ",
						    icons: {
						        primary: "ui-icon-closethick",
						    },
						    class: "ui-button",
						    click: function () {
						        $(this).dialog("close");
						    }}
				],
	    show: {
	        effect: "blind",
	        duration: 500
	    },
	    hide: {
	        effect: "explode",
	        duration: 500
	    }
	});


}]);
function CheckBox(cual,valor){
	//esta funcion es para cambiar el color de el check de los selects
	var campoTemp=cual.id;

	 if($('#'+campoTemp).val()=== "No filtrar"){
		 $( '#imagen'+valor).fadeTo( "fast", 0.2 );
		 campoTemp ="";

	  }else if($('#'+campoTemp).val() === "" || $('#'+campoTemp).val() === undefined){
		  campoTemp ="<null>";
	  }else{
		  $( '#imagen'+valor).fadeTo( "fast", 1 );
		  campoTemp =$('#'+campoTemp).val();
	  }
}
function CheckBox2(cual,valor){
	//esta funcion es para cambiar el color de el check de los selects
	var campoTemp=cual.id;

	campoTemp=$('#'+campoTemp).val();
	 if(campoTemp === "No filtrar"){
		 $( '#imagen'+valor).fadeTo( "fast", 0.2 );
		 campoTemp ="";
	  }else if(campoTemp === "" || campoTemp === undefined){
		  //console.log("entramos en vacio");
		  campoTemp ="<null>";
		  CheckBox1(valor);

	  }else{
		  $( '#imagen'+valor).fadeTo( "fast", 1 );
	  }

}
