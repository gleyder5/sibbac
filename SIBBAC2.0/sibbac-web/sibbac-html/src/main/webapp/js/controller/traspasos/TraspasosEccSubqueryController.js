(function(GENERIC_CONTROLLERS, sibbac20){
	"strict mode";
	
	var SubqueryController = function(service, httpParamSerializer, uiGridConstants, $scope, $interval) {
		GENERIC_CONTROLLERS.DynamicFiltersController.call(this, service, httpParamSerializer, uiGridConstants);
		this.scope = $scope;
		this.subqueryDialog = this.registryModal("subqueryDialog", "Subquery", {}, {minWidth:620});
		this.scope.$on("openSubquery", this.openSubquery.bind(this));
		this.interval = $interval;
	};
	
	SubqueryController.prototype = Object.create(GENERIC_CONTROLLERS.DynamicFiltersController.prototype);
	
	SubqueryController.prototype.constructor = SubqueryController;
	
	SubqueryController.prototype.openSubquery = function(event, entity, name) {
		this.subqueryDialog.dialog("option", "title", name);
		this.grid.data = [];
		this.selectedQuery = {name: name};
		this.filter = entity;
		this.service.columns(name, this.columnsLoaded.bind(this));
	};
	
	SubqueryController.prototype.columnsLoaded = function(columns) {
		GENERIC_CONTROLLERS.DynamicFiltersController.prototype.columnsLoaded.call(this, columns);
		this.executeQuery();
	};
	
	SubqueryController.prototype.executeQuery = function() {
		GENERIC_CONTROLLERS.DynamicFiltersController.prototype.executeQuery.call(this);
		var self = this;
		this.subqueryDialog.dialog("open");
		this.interval(function() {
			self.gridApiRef.core.handleWindowResize();
		}, 500, 10);
	}

	sibbac20.controller("TraspasosEccSubqueryController", ["TraspasosEccSubqueryService", "$httpParamSerializer", 
		"uiGridConstants", "$scope", "$interval", SubqueryController]);
	
})(GENERIC_CONTROLLERS, sibbac20);