sibbac20.controller('DepositariosValoresController', ['$scope', '$document', 'growl', 'DatosMaestrosService', function ($scope, $document, growl, DatosMaestrosService) {
        var oTable = undefined;
        function consultar() {
            inicializarLoading();
            DatosMaestrosService.getDepositariosValores(onSuccessConsultaRequest, onErrorRequest, {});
        }
        function onErrorRequest(data, status, headers, config) {
            $.unblockUI();
            growl.addErrorMessage("Ocurrió un error durante la petición de datos al servidor de datos." + data);
        }
        function onSuccessConsultaRequest(json) {
            var datos = undefined;
            if (json === null || json === undefined || json.error === undefined || (json.error !== null && json.error.error !== "")) {
                if (json === null || json.error === undefined) {
                    growl.addErrorMessage("Ocurrió un error de conexión con el servidor.");
                } else {
                    growl.addErrorMessage(json.error);
                }
            } else {
                if (json === undefined || json.resultados === undefined || json.resultados.datosMaestros === undefined) {
                    datos = {};
                } else {
                    datos = json.resultados.datosMaestros;
                }
                var difClass = 'centrado';
                oTable.fnClearTable();
                angular.forEach(datos, function (val, key) {
                    var codigo = '';
                    var descripcion = '';
                    var trdType = '';
                    var trdSubType = '';


                    if (datos[key].codigo !== null && datos[key].codigo != undefined) {
                        codigo = datos[key].codigo;
                    }

                    if (datos[key].descripcion !== null && datos[key].descripcion != undefined) {
                        descripcion = datos[key].descripcion;
                    }

                    if (datos[key].trdType !== null && datos[key].trdType != undefined) {
                        trdType = datos[key].trdType;
                    }

                    if (datos[key].trdSubType !== null && datos[key].trdSubType != undefined) {
                        trdSubType = datos[key].trdSubType;
                    }

                    oTable.fnAddData([
                        codigo,
                        descripcion,
                        trdType,
                        trdSubType
                    ]);
                });
            }
            $.unblockUI();
        }
        $document.ready(function () {
            if (oTable === undefined) {
                loadData();
            }
            consultar();
        });
        function loadData() {
            oTable = $("#tblDepositariosValores").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "js/swf/copy_csv_xls_pdf.swf"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "language": {
                    "url": "i18n/Spanish.json"
                }
            });
        }
    }])


