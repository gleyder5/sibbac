'use strict';
sibbac20.controller('MovimientosFallidosController', ['$scope', '$http', '$rootScope','IsinService', function ($scope, $http, $rootScope, IsinService) {
	var idOcultos = [];
	var availableTags = [];
	var idOcultosIsin = [];
	var valorIsin = [];
	var oTable = undefined;

	$(document).ready(function () {
	    oTable = $("#tblMovimientosFallidos").dataTable({
	        "dom": 'T<"clear">lfrtip',
	        "tableTools": {
	            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
	        },
	        "language": {
	            "url": "i18n/Spanish.json"
	        },
	        "scrollY": "480px",
	        "scrollCollapse": true,
	        "columns": [
	            {"width": "auto"},
	            {"width": "auto"},
	            {"width": "auto"},
	            {"width": "auto", sClass: "monedaR", type: "formatted-num"},
	            {"width": "auto"},
	            {"width": "auto"},
	            {"width": "auto"}
	        ]

	    });


	    prepareCollapsion();

	    /*BUSCADOR*/
	    $('.btn.buscador').click(function () {
	        //event.preventDefault();
	        //$('.resultados').show();
	        collapseSearchForm();
	        cargarTabla();
	        return false;
	    });


	    cargarAlias();
	    cargarIsin();
	    cargarTabla();
	});

	function cargarIsin()
	{

	    $.ajax({
	        type: "POST",
	        dataType: "json",
	        url: "/sibbac20back/rest/service",
	        data: "{\"service\" : \"SIBBACServiceIsin\", \"action\"  : \"listIsin\"}",
	        beforeSend: function (x) {
	            if (x && x.overrideMimeType) {
	                x.overrideMimeType("application/json");
	            }
	            // CORS Related
	            x.setRequestHeader("Accept", "application/json");
	            x.setRequestHeader("Content-Type", "application/json");
	            x.setRequestHeader("X-Requested-With", "HandMade");
	            x.setRequestHeader("Access-Control-Allow-Origin", "*");
	            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	        },
	        async: true,
	        success: function (json) {
	            var resultados = json.resultados.listadoIsin;
	            var item = null;
	            for (var k in resultados) {
	                item = resultados[ k ];
	                idOcultosIsin.push(item.id);
	                ponerTag = item.descripcion.trim();
	                valorIsin.push(ponerTag);
	            }
	            //código de autocompletar
	            $("#textIsin").autocomplete({
	                source: valorIsin
	            });
	        },
	        error: function (c) {
	            console.log("ERROR: " + c);
	        }
	    });

	}


	function cargarAlias()
	{
	    $.ajax({
	        type: "POST",
	        dataType: "json",
	        url: "/sibbac20back/rest/service",
	        data: "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAlias\"}",
	        beforeSend: function (x) {
	            if (x && x.overrideMimeType) {
	                x.overrideMimeType("application/json");
	            }
	            // CORS Related
	            x.setRequestHeader("Accept", "application/json");
	            x.setRequestHeader("Content-Type", "application/json");
	            x.setRequestHeader("X-Requested-With", "HandMade");
	            x.setRequestHeader("Access-Control-Allow-Origin", "*");
	            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	        },
	        async: true,
	        success: function (json) {
	            var resultados = json.resultados.listaAlias;
	            var item = null;

	            for (var k in resultados) {
	                item = resultados[ k ];
	                idOcultos.push(item.id);
	                ponerTag = item.nombre;
	                availableTags.push(ponerTag);
	            }
	            //código de autocompletar

	            $("#textAlias").autocomplete({
	                source: availableTags
	            });


	            //fin de código de autocompletar
	        },
	        error: function (c) {
	            console.log("ERROR: " + c);
	        }
	    });
	}
	//funcion para cargar la tabla
	function cargarTabla()
	{
	    console.log("entro en el método");
	    $.ajax({
	        type: "POST",
	        dataType: "json",
	        url: "/sibbac20back/rest/service",
	        data: "{\"service\" : \"SIBBACServiceFallidos\", \"action\"  : \"LISTING\"}",
	        beforeSend: function (x) {
	            if (x && x.overrideMimeType) {
	                x.overrideMimeType("application/json");
	            }
	            // CORS Related
	            x.setRequestHeader("Accept", "application/json");
	            x.setRequestHeader("Content-Type", "application/json");
	            x.setRequestHeader("X-Requested-With", "HandMade");
	            x.setRequestHeader("Access-Control-Allow-Origin", "*");
	            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	        },
	        async: true,
	        success: function (json) {


	            var datos = undefined;
	            if (json === undefined || json.resultados === undefined) {
	                datos = {};
	            } else {
	                datos = json.resultados;
	            }

	            oTable.fnClearTable();

	            for (var k in datos) {
	                item = datos[ k ];
	                oTable.fnAddData([
	                    "<span>" + item.alias + "</span>",
	                    "<span>" + item.isin + "</span>",
	                    "<span>" + item.descripcion + "</span>",
	                    "<span>" + item.saldoAfectado + "</span>",
	                    "<span>" + item.camara + "</span>",
	                    "<span>" + item.segmento + "</span>",
	                    "<span>" + item.sentido + "</span>"
	                ]);
	            }

	        },
	        error: function (c) {
	            console.log("ERROR: " + c);
	        }
	    });

	}

	//id, alias, isin,descripcion, nbholder, saldoAfectado, nbvalor, camara, segmento, sentido, cdtipo, auditDate


	function afectar (id, alias, isin, descripcion, nbholder, saldoAfectado, nbvalor, camara, segmento, sentido, cdtipo, auditDate)
	{

	    //$('.resultados').hide();

	    $('.tablaPrueba2').append(
	            "<tr>" +
	            "<td class='taleft'>" + alias + "</td>" +
	            "<td class='taleft'>" + isin + "</td>" +
	            "<td class='taright'>" + descripcion + "</td>" +
	            "<td class='taleft'>" + nbholder + "</td>" +
	            "<td class='taleft'>" + saldoAfectado + "</td>" +
	            "<td class='taleft'>" + nbvalor + "</td>" +
	            "<td class='taleft'>" + camara + "</td>" +
	            "<td class='taleft'>" + segmento + "</td>" +
	            "<td class='taleft'>" + sentido + "</td>" +
	            "<td class='taleft'>" + cdtipo + "</td>" +
	            "<td class='taleft'>" + auditDate + "</td>" +
	            "</tr>"
	            );

	    //$('.resultados1').show();
	}




}]);