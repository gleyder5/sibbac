'use strict';
sibbac20
    .controller(
                'FinanciacionInteresesController',
                [
                 '$scope',
                 '$http',
                 '$rootScope',
                 'IsinService',
                 '$location',
                 'SecurityService',
                 function ($scope, $http, $rootScope, IsinService, $location, SecurityService) {

                   var popupsInteresAlta;
                   var popupsMinimoAlta;
                   var tablaInteresDemora;
                   var tablaMinimo;
                   var isActiveActive = false;
                   $(document).ready(
                                     function () {
                                       initialize();

                                       $scope.altaInteresDemora = function () {
                                         if (isActiveActive) {
                                           popupsInteresAlta = $("#interesesAlta").bPopup();
                                         }

                                       }

                                       $scope.altaMinimoImporte = function () {
                                         if (isActiveActive) {
                                           popupsMinimoAlta = $("#minimoAlta").bPopup();
                                         }
                                       }

                                       $("#annadirInteres").click(
                                                                  function () {
                                                                    salvarInteres($("#interesesDemoraAltaAlias"),
                                                                                  $("#fechaDesdeInteresesAlta"),
                                                                                  $("#fechaHastaInteresesAlta"),
                                                                                  $("#interesesAltaAliasSeleccionado"),
                                                                                  $("#baseAlias"));
                                                                    popupsInteresAlta.close();
                                                                  });
                                       $("#annadirMinimo")
                                           .click(
                                                  function () {
                                                    salvarImporteMinimo($("#minimoImporteAlta"),
                                                                        $("#minimoAltaAliasSeleccionado"));
                                                    popupsMinimoAlta.close();
                                                  });
                                       $("#borrarMinimo").click(borrarMinimo);
                                       $("#interesesDefectoDemora").keypress(justNumbersD);
                                       $("#minimoDefectoDemora").keypress(justNumbersD);
                                       $("#interesesAltaAlias").keypress(justNumbersD);
                                       $("#minimoImporteAlta").keypress(justNumbersD);
                                       $("#baseDefecto").keypress(justNumbers);
                                       $("#baseAlias").keypress(justNumbers);

                                       $scope.modificarInteresDemora = function () {
                                         var flag = validarInteresesDemora($("#interesesDefectoDemora"),
                                                                           $("#fechaDesdeDefectoDemora"),
                                                                           $("#fechaHastaDefectoDemora"));
                                         if (flag) {
                                           salvarInteres($("#interesesDefectoDemora"), $("#fechaDesdeDefectoDemora"),
                                                         $("#fechaHastaDefectoDemora"), undefined, $("#baseDefecto"));
                                         }
                                       }

                                       $scope.modificarMinimo = function () {
                                         var flag = validarMinimo($("#minimoDefectoDemora"),
                                                                  $("#fechaDesdeDefectoMinimo"),
                                                                  $("#fechaHastaDefectoMinimo"));
                                         if (flag) {
                                           salvarImporteMinimo($("#minimoDefectoDemora"), undefined)
                                         }

                                       }

                                       tablaInteresDemora = $("#tblInteresesDemora").dataTable({
                                         "dom" : 'T<"clear">lfrtip',
                                         "tableTools" : {
                                           "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                                         },
                                         "deferRender" : true,
                                         "language" : {
                                           "url" : "i18n/Spanish.json"
                                         }
                                       });
                                       tablaMinimo = $("#tblMinimo").dataTable({
                                         "dom" : 'T<"clear">lfrtip',
                                         "tableTools" : {
                                           "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                                         },
                                         "deferRender" : true,
                                         "language" : {
                                           "url" : "i18n/Spanish.json"
                                         }
                                       });

                                     });

                   function initialize () {
                     // loadpicker([ "fechaDesdeDefectoDemora", "fechaHastaDefectoDemora",
                     // "fechaDesdeInteresesAlta", "fechaHastaInteresesAlta" ]);
                     // verificarFechas([ [ "fechaDesdeDefectoDemora", "fechaHastaDefectoDemora" ],
                     // [ "fechaDesdeInteresesAlta", "fechaHastaInteresesAlta" ] ]);

                     cargarAlias();
                     cargaInteresPorDefecto();
                     cargaMinimoPorDefecto();
                     cargaTablaInteresesDemora();
                     cargaTablaMinimo();
                     $("#ui-datepicker-div").remove();
                     $('input#fechaDesdeDefectoDemora').datepicker({
                       onClose : function (selectedDate) {
                         $("#fechaHastaDefectoDemora").datepicker("option", "minDate", selectedDate);
                       } // onClose
                     });

                     $('input#fechaHastaDefectoDemora').datepicker({
                       onClose : function (selectedDate) {
                         $("#fechaDesdeDefectoDemora").datepicker("option", "maxDate", selectedDate);
                       } // onClose
                     });
                   }

                   function cargaTablaInteresesDemora () {
                     var request;
                     var data = new DataBean();

                     data.setService('SIBBACServiceFinanciacionIntereses');
                     data.setAction('getAllIntereses');

                     request = requestSIBBAC(data);

                     request.success(function (json) {
                       var intereses = json.resultados.intereses;
                       var botonBorrar;
                       var idBtnDeleteInteres;
                       var item;
                       tablaInteresDemora.fnClearTable();
                       for ( var k in intereses) {
                         item = intereses[k];
                         // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                         botonBorrar = "";
                         if (SecurityService.inicializated) {
                           if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR, $location.path())) {
                             idBtnDeleteInteres = "btnDeleteInteres-" + item.id;
                             botonBorrar += '<input class="mybutton" type="button"';
                             botonBorrar += "id='" + idBtnDeleteInteres + "'";
                             botonBorrar += 'value="Borrar" onclick="borrarIntereses(' + item.id + ')"/>';
                           }
                         }

                         if (item.alias !== "") {
                           tablaInteresDemora.fnAddData([ botonBorrar, item.alias, item.intereses, item.base,
                                                         item.fechaInicio, item.fechaHasta ], false);
                         } else {
                           tablaInteresDemora.fnAddData([ botonBorrar, "TODOS", item.intereses, item.base,
                                                         item.fechaInicio, item.fechaHasta ], false);
                         }
                       }
                       tablaInteresDemora.fnDraw();

                     });
                   }

                   function cargaTablaMinimo () {
                     var request;
                     var data = new DataBean();

                     data.setService('SIBBACServiceFinanciacionIntereses');
                     data.setAction('getAllMinimos');

                     request = requestSIBBAC(data);
                     request.success(function (json) {

                       var minimos = json.resultados.minimos;
                       var item;
                       var botonBorrar;
                       var idBtnDeleteMinimo;

                       tablaMinimo.fnClearTable();
                       for ( var k in minimos) {
                         item = minimos[k];
                         // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                         botonBorrar = "";
                         if (SecurityService.inicializated) {
                           if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR, $location.path())) {
                             idBtnDeleteMinimo = "btnDeleteMinimo-" + item.id;
                             botonBorrar += '<input class="mybutton" type="button"';
                             botonBorrar += "id='" + idBtnDeleteMinimo + "'";
                             botonBorrar += 'value="Borrar" onclick="borrarMinimos(' + item.id + ')" />';
                           }
                         }

                         if (item.alias !== "") {
                           tablaMinimo.fnAddData([ botonBorrar, item.alias, item.minimo ], true);
                         } else {
                           tablaMinimo.fnAddData([ botonBorrar, "TODOS", item.minimo ], true);
                         }
                       }
                       tablaMinimo.fnDraw();

                     });

                   }

                   function cargarAlias () {
                     var request;
                     var data = new DataBean();

                     data.setService('SIBBACServiceAlias');
                     data.setAction('getListaAlias');

                     request = requestSIBBAC(data);

                     request.success(function (json) {

                       var resultados = json.resultados.listaAlias;
                       var availableTags = [];
                       var tag = {};

                       var fnOpen = function (event, ui) {
                         console.log("Se asigna el estilo z-index a 2147483647");
                         $(".ui-autocomplete").css("z-index", "2147483647");
                       };
                       var fnClose = function (event, ui) {
                         console.log("Se asigna el estilo z-index a 9999");
                         $(".ui-autocomplete").css("z-index", "9999");
                       };

                       for ( var k in resultados) {
                         tag = {
                           label : resultados[k].nombre.trim(),
                           value : resultados[k].id
                         };
                         availableTags.push(tag);
                       }

                       $("#interesesAltaAlias").autocomplete({
                         source : availableTags,
                         open : fnOpen,
                         close : fnClose,
                         focus : function (event, ui) {
                           console.log(JSON.stringify(ui));
                           $(this).val(ui.item.label);
                           $("#interesesAltaAliasSeleccionado").val(ui.item.value);
                           return false;
                         },
                         select : function (event, ui) {
                           console.log(JSON.stringify(ui));
                           $(this).val(ui.item.label);
                           $("#interesesAltaAliasSeleccionado").val(ui.item.value);
                           return false;
                         }
                       });

                       $("#minimoAltaAlias").autocomplete({
                         source : availableTags,
                         open : fnOpen,
                         close : fnClose,
                         focus : function (event, ui) {
                           console.log(JSON.stringify(ui));
                           $(this).val(ui.item.label);
                           $("#minimoAltaAliasSeleccionado").val(ui.item.value);
                           return false;
                         },
                         select : function (event, ui) {
                           console.log(JSON.stringify(ui));
                           $(this).val(ui.item.label);
                           $("#minimoAltaAliasSeleccionado").val(ui.item.value);
                           return false;
                         }
                       });

                       isActiveActive = true;

                     });
                   }

                   function cargaInteresPorDefecto () {
                     var request;
                     var data = new DataBean();

                     data.setService('SIBBACServiceFinanciacionIntereses');
                     data.setAction('getInteresPorDefecto');

                     request = requestSIBBAC(data);
                     request.success(function (json) {
                       var resultados = json.resultados;
                       $("#interesesDefectoDemora").val(resultados.intereses);
                       $("#fechaDesdeDefectoDemora").val(transformaFechaGuion(resultados.fechaInicio));
                       $("#fechaHastaDefectoDemora").val(transformaFechaGuion(resultados.fechaHasta));
                       $("#baseDefecto").val(resultados.base);
                     });

                   }

                   function cargaMinimoPorDefecto () {
                     var request;
                     var data = new DataBean();

                     data.setService('SIBBACServiceFinanciacionIntereses');
                     data.setAction('getMinimoPorDefecto');

                     request = requestSIBBAC(data);
                     request.success(function (json) {
                       var minimoImporte = json.resultados.minimoImporte;
                       $("#minimoDefectoDemora").val(minimoImporte);
                       // console.log("tenemos: ",document.getElementById("fechaDesdeDefectoDemora").value);
                       // ALEX 5 feb, de esta manera logramos cargar los limites de fechas
                       $("#fechaHastaDefectoDemora")
                           .datepicker("option", "minDate", document.getElementById("fechaDesdeDefectoDemora").value);
                     });

                   }

                   function salvarImporteMinimo (importeMinimo, alias) {
                     var request;
                     var data = new DataBean();
                     var idAlias;
                     if (alias !== undefined) {
                       idAlias = alias;
                     }
                     var filter = crearFiltro("", $(importeMinimo).val(), "", "", $(idAlias).val());

                     data.setService('SIBBACServiceFinanciacionIntereses');
                     data.setAction('saveMinimo');
                     data.setFilters(filter);

                     request = requestSIBBAC(data);
                     request.success(function (json) {
                       if (json.resultados.status === "OK") {
                         cargaTablaMinimo();
                       } else {
                         alert(json.error);
                       }
                     });

                   }

                   function salvarInteres (interesDemora, fechaInicio, fechaHasta, alias, base) {
                     var request;
                     var data = new DataBean();
                     if (alias === undefined) {
                       alias = "";
                     }
                     var filter = crearFiltro($(interesDemora).val(), "", $(fechaInicio).val(), $(fechaHasta).val(),
                                              $(alias).val(), $(base).val());

                     data.setService('SIBBACServiceFinanciacionIntereses');
                     data.setAction('saveIntereses');
                     data.setFilters(filter);

                     request = requestSIBBAC(data);
                     request.success(function (json) {
                       if (json.resultados.status === "OK") {
                         cargaTablaInteresesDemora();
                       } else {
                         alert(json.error);
                       }
                     });
                   }

                   function borrarIntereses (id) {
                     var request;
                     var data = new DataBean();
                     var filter = "{\"id\" : \"" + id + "\"}";

                     data.setService('SIBBACServiceFinanciacionIntereses');
                     data.setAction('deleteIntereses');
                     data.setFilters(filter);

                     request = requestSIBBAC(data);
                     request.success(function (json) {
                       cargaTablaInteresesDemora();
                     });

                   }

                   function borrarMinimos (id) {

                     var request;
                     var data = new DataBean();
                     var filter = "{\"id\" : \"" + id + "\"}";

                     data.setService('SIBBACServiceFinanciacionIntereses');
                     data.setAction('deleteMinimo');
                     data.setFilters(filter);

                     request = requestSIBBAC(data);
                     request.success(function (json) {
                       cargaTablaMinimo();
                     });

                   }

                   function crearFiltro (intereses, minimoImporte, fechaInicio, fechaHasta, idAlias, base) {
                     var result = "{";
                     if (fechaInicio === undefined) {
                       fechaInicio = "";
                     }
                     if (fechaHasta === undefined) {
                       fechaHasta = "";
                     }

                     if (intereses !== undefined) {
                       result += "\"intereses\" : \"" + intereses + "\",";
                     }
                     if (minimoImporte !== undefined) {
                       result += "\"minimoImporte\" : \"" + minimoImporte + "\",";
                     }
                     result += "\"fechaInicio\" : \"" + fechaInicio + "\",";
                     result += "\"fechaHasta\" : \"" + fechaHasta + "\"";
                     if (idAlias !== undefined) {
                       result += ",\"idAlias\" : \"" + idAlias + "\"";
                     }
                     if (base !== undefined) {
                       result += ",\"base\" : \"" + base + "\"";
                     }
                     result += "}";
                     return result;
                   }

                   function validarAlias (alias) {
                     var flag = true;
                     if ($(alias).val() === "") {
                       alert("El alias es obligatorio");
                     }
                     return flag;
                   }

                   function validarInteresesDemora (interesesDefectoDemora, fechaDesdeDemora, fechaHastaDemora) {
                     var patronIntereseDemora = new RegExp(/^\d{1,2}\b.?\d{0,2}?$/);
                     var flag = true;
                     if (!patronIntereseDemora.test($(interesesDefectoDemora).val())) {
                       alert("El formato del interes no es el correcto. El formato valido es 2 enteros con 2 decimales");
                       flag = false;
                     } else if ($(fechaDesdeDemora).val() === "") {
                       alert("La fecha de inicio debe estar rellenada");
                       flag = false;
                     } else if ($(fechaHastaDemora).val() === "") {
                       alert("La fecha de hasta debe estar rellenada");
                       flag = false;
                     }
                     return flag;
                   }

                   function validarMinimo (campoMinimo, fechaDesdeMinimo, fechaHastaMinimo) {
                     var patronValorMinimo = new RegExp(/^\d{1,15}\b.?\d{0,2}?$/);
                     var flag = true;
                     if (!patronValorMinimo.test($(campoMinimo).val())) {
                       alert("El formato del interes no es el correcto. El formato valido es 15 enteros con 2 decimales");
                       flag = false;
                     }
                     return flag;
                   }

                   function borrarMinimo () {
                     $("#minimoAltaAlias").val("");
                     $("#minimoAltaAliasSeleccionado").val("");
                     $("#minimoImporteAlta").val("");
                   }

                 } ]);
