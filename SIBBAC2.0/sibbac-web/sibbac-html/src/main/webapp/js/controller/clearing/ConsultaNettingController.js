sibbac20
		.controller(
				'ConsultaNettingController',
				[
						'$scope',
						'EntregaRecepcionService',
						'ConsultaNettingService',
						'IsinService',
						'AliasService',
						'$document',
						'growl',
						'$compile',
						function($scope, EntregaRecepcionService,
								ConsultaNettingService, IsinService,
								AliasService, $document, growl, $compile) {
							var oTable = undefined;
							$scope.followSearch = "";
							$scope.filtro = {
								fcontratacionDe : "",
								fcontratacionA : "",
								fliquidacionDe : "",
								fliquidacionA : "",
								liquidado : "",
								alias : "",
								isin : "",
								chalias : true,
								chisin : true
							};

							function initialize() {

								inicializarLoading();
								loadAlias();
								loadIsines();

								$("#ui-datepicker-div").remove();
					            $('input#fcontratacionDe').datepicker({
					                onClose: function( selectedDate ) {
					                  $( "#fcontratacionA" ).datepicker( "option", "minDate", selectedDate );
					                } // onClose
					              });

					              $('input#fcontratacionA').datepicker({
					                onClose: function( selectedDate ) {
					                  $( "#fcontratacionDe" ).datepicker( "option", "maxDate", selectedDate );
					                } // onClose
					              });

					              $('input#fliquidacionDe').datepicker({
					                  onClose: function( selectedDate ) {
					                    $( "#fliquidacionA" ).datepicker( "option", "minDate", selectedDate );
					                  } // onClose
					                });

					                $('input#fliquidacionA').datepicker({
					                  onClose: function( selectedDate ) {
					                    $( "#fliquidacionDe" ).datepicker( "option", "maxDate", selectedDate );
					                  } // onClose
					                });
							}
							function loadDataTable() {
								oTable = $("#tablaNetting")
										.dataTable(
												{
													"dom" : 'T<"clear">lfrtip',
													"tableTools" : {
														"sSwfPath" : "js/swf/copy_csv_xls_pdf.swf"
													},
													"language" : {
														"url" : "i18n/Spanish.json"
													},
													"aoColumns" : [
																	{className : 'checkbox', targets : 0, sClass : "taleft", type : "checkbox", bSortable : false,  name : "active",  width : 50},
																	{className : 'details-tit',	sClass : "centrar",	targets : 1, bSortable : false,	width : 50},
																	{sClass : "centrar"	},
																	{sClass : "centrar" },
																	{sClass : "centrar"	},
																	{sClass : "centrar"	},
																	{sClass : "centrar" },
																	{sClass : "centrar" },
																	{sClass : "centrar"	},
																	{sClass : "centrar"	},
																	{sClass : "centrar"	},
																	{sClass : "centrar"	},
																	{sClass : "centrar"	},
																	{sClass : "centrar"	}
															],
									                "scrollY": "480px",
													"scrollX" : true,
													"scrollCollapse": true,
													"order" : [],
													"fnCreatedRow" : function(
															row, data, index) {
														$compile(row)($scope);
													}
												});
							}
							$document
									.ready(function() {
										initialize();
										loadDataTable();
										angular
												.element('#tablaNetting tbody')
												.on(
														'click',
														'td.details-liq',
														function(e) {

															var nTr = angular
																	.element(
																			e.target)
																	.parents(
																			'tr')[0];
															var aData = oTable
																	.fnGetData(nTr);

															if (oTable
																	.fnIsOpen(nTr)) {
																angular
																		.element(
																				this)
																		.removeClass(
																				'open liq');
																oTable
																		.fnClose(nTr);
															} else {
																if (aData[0] == "DETALLE") {
																	$(this)
																			.addClass(
																					'open liq');
																	oTable
																			.fnOpen(
																					nTr,
																					TablaLiquidaciones(
																							oTable,
																							nTr),
																					'details-liq');
																}
															}

														});

										angular
												.element('#tablaNetting tbody')
												.on(
														'click',
														'td.details-alc',
														function() {

															var nTr = $(this)
																	.parents(
																			'tr')[0];
															var aData = oTable
																	.fnGetData(nTr);

															if (oTable
																	.fnIsOpen(nTr)) {
																$(this)
																		.removeClass(
																				'open alc');
																oTable
																		.fnClose(nTr);
															} else {

																$(this)
																		.addClass(
																				'open alc');
																oTable
																		.fnOpen(
																				nTr,
																				TablaALC(
																						oTable,
																						nTr),
																				'details-alc');
															}

														});
										prepareCollapsion();
									});
/*
							$scope.submitForm = function(event) {
								cargarDatosNetting();
								seguimientoBusqueda();
								collapseSearchForm();
							};
*/
					        $scope.Consultar = function (event) {
								cargarDatosNetting();
								seguimientoBusqueda();
								collapseSearchForm();

					          }
							function cargarDatosNetting() {

						        var alias = (!$scope.filtro.chalias && $scope.filtro.alias !== "" ? "#" : "") + getAliasId();
						        var isin = (!$scope.filtro.chisin && $scope.filtro.isin !== "" ? "#" : "") + getIsin();

								var filtro = {
									fcontratacionDe : $scope.filtro.fcontratacionDe,
									fcontratacionA : $scope.filtro.fcontratacionA,
									fliquidacionDe : $scope.filtro.fliquidacionDe,
									fliquidacionA : $scope.filtro.fliquidacionA,
									liquidado : $scope.filtro.liquidado,
									alias : alias,
									isin : isin
								};
								inicializarLoading();
								ConsultaNettingService.listaNetting(
										onSuccessListaNettingRequest,
										onErrorRequest, filtro);

							}
							function onSuccessListaNettingRequest(json) {
								if (json.resultados !== undefined
										&& json.resultados.lista !== undefined) {

									var item = null;
									var datos = json.resultados.lista;

									oTable.fnClearTable();

									angular
											.forEach(
													datos,
													function(item, k) {
														var enlace_liq = "";
														var valor_liq = "";
														if (item.n_liquidaciones !== 0) {
															valor_liq = "DETALLE";
														} else {
															valor_liq = "PENDIENTE";
														}

														var rowIndex = oTable
																.fnAddData(
																		[
																				valor_liq,
																				item.idnetting,
																				item.liquidado,
																				transformaFechaGuion(item.fechaoperacion),
																				transformaFechaGuion(item.fechacontratacion),
																				item.cdalias,
																				item.camaracompensacion,
																				item.cdisin,
																				item.nbvalors,
																				item.nutiulototales,
																				$
																						.number(
																								item.efectivoneteado,
																								2,
																								',',
																								'.'),
																				$
																						.number(
																								item.corretajeneteado,
																								2,
																								',',
																								'.'),
																				item.contabilizado,
																				transformaFechaGuion(item.fechacontabilizado) ],
																		false);

														var nTr = oTable
																.fnSettings().aoData[rowIndex[0]].nTr;
														if (item.n_liquidaciones !== 0) {
															$('td', nTr)[0]
																	.setAttribute(
																			'style',
																			'cursor: pointer; color: #ff0000;');
															$('td', nTr)[0]
																	.setAttribute(
																			'class',
																			'details-liq');
														}
														$('td', nTr)[1]
																.setAttribute(
																		'style',
																		'cursor: pointer; color: #ff0000;');
														$('td', nTr)[5]
																.setAttribute(
																		'title',
																		item.descrali);
													});
									oTable.fnDraw();
								}
								$.unblockUI();
							}

							function TablaALC(Table, nTr) {

								var aData = Table.fnGetData(nTr);
								var contador = Table.fnGetPosition(nTr);

								var resultado = "<tr id='tabla_a_desplegar_ALC"
										+ contador
										+ "' style='display:none;'>"
										+ "<td colspan='13' style='border-width:0 !important;background-color:#dfdfdf;'>"
										+ "<table id='tablaParam_ALC"
										+ contador
										+ "' style=width:100%; margin-top:5px;'>"
										+ "<tr>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Estado</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Sentido</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Títulos</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Efectivo</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Corretaje</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Neto</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Canon Contratación</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Canon Compensación</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Canon Liquidación</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>NIF</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Titular</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Booking</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Nucnfclt</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Nucnfliq</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Ref. bancaria</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Ourparticipe</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Theirparticipe</th>"
										+ "</tr></table></td></tr>";

								desplegarDatosALC(aData[1], contador);

								return resultado;
							}

							$scope.currentRow = {
								idnetting : "",
								numero_fila : ""
							};

							// funcion para desplegar la tabla de alc
							function desplegarDatosALC(idnetting, numero_fila) {
								$scope.currentRow.idnetting = idnetting;
								$scope.currentRow.numero_fila = numero_fila;
								//
								var filtro = {
									idnetting : idnetting
								};
								inicializarLoading();
								EntregaRecepcionService.ListaEntregaRecepcion(
										onSuccessListaEntregaRecepcionRequest,
										onErrorRequest, filtro);
							}
							function onSuccessListaEntregaRecepcionRequest(json) {
								if (json.resultados !== undefined
										&& json.resultados.lista !== undefined) {
									var item = null;
									var contador = 0;
									var datos = json.resultados.lista;
									var estilo = "";
									angular
											.forEach(
													datos,
													function(item, key) {
														if (contador % 2 == 0) {
															estilo = "even";
														} else {
															estilo = "odd";
														}
														contador++;


														angular
																.element(
																		"#tablaParam_ALC"
																				+ $scope.currentRow.numero_fila)
																.append(
																		"<tr class='"
																				+ estilo
																				+ "' id='filaRecuperadaALC"
																				+ numero_fila
																				+ "' style='background-color: #f5ebc5 !important;' >"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.estadoentrec
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.cdtpoper
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.nutitliq
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ a2digitos(item.imefeagr)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ a2digitos(item.imcomisin)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ a2digitos(item.imnfiliq)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.canon_contratacion
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.canon_compensacion
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.canon_liquidacion
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.cdniftit
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.nbtitliq
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.nbooking
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.nucnfclt
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.nucnfliq
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.cdrefban
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.ourpar
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.theirpar
																				+ "</td>"
																				+ "</tr>");

														angular
																.element(
																		"#tablaParam_ALC"
																				+ $scope.currentRow.numero_fila)
																.append(
																		"</table>");
													});
								}
								desplegar(
										'tabla_a_desplegar_ALC'
												+ $scope.currentRow.numero_fila,
										'estadoT'
												+ $scope.currentRow.numero_fila,
										'estadoT'
												+ $scope.currentRow.numero_fila);
								$.unblockUI();
							}
							$scope.limpiarFiltros = function(event) {
								event.preventDefault();
								//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
								 	$( "#fcontratacionA" ).datepicker( "option", "minDate", "");
						            $( "#fcontratacionDe" ).datepicker( "option", "maxDate", "");
						            $( "#fliquidacionA" ).datepicker( "option", "minDate", "");
						            $( "#fliquidacionDe" ).datepicker( "option", "maxDate", "");
						        //
								$scope.filtro = {
										fcontratacionDe : "",
										fcontratacionA : "",
										fliquidacionDe : "",
										fliquidacionA : "",
										liquidado : "",
										alias : "",
										isin : "",
										chalias : false,
										chisin : false
									};
					    		$scope.btnInvGenerico('Alias');
					    		$scope.btnInvGenerico('Isin');
							};

							function TablaLiquidaciones(Table, nTr) {

								var aData = Table.fnGetData(nTr);
								var contador = Table.fnGetPosition(nTr);

								var resultado = "<tr id='tabla_a_desplegar_LIQ"
										+ contador
										+ "' style='display:none;'>"
										+ "<td style='border-width:0 !important;background-color:#dfdfdf;'>"
										+ "<table id='tablaParamLIQ"
										+ contador
										+ "' style=width:100%; margin-top:5px;'>"
										+ "<tr>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Estado</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Fecha Liquidada</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Fecha Operación</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Fecha Liquidación</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Titulos liquidados</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Neto liquidado</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Efectivo</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Corretaje</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Contabilizado</th>"
										+ "<th class='taleft' style='background-color: #000 !important;'>Fecha contabilizado</th>"
										+ "</tr></table></td></tr>";

								desplegarLiquidaciones(aData[1], contador, nTr);

								return resultado;
							}

							// funcion para desplegar la tabla de liquidaciones
							function desplegarLiquidaciones(idnetting,
									numero_fila) {

								var filtro = {
									idnetting : idnetting
								};
								$scope.currentRow.idnetting = idnetting;
								$scope.currentRow.numero_fila = numero_fila;
								inicializarLoading();
								EntregaRecepcionService.ListaLiquidacionS3(
										onSuccessListaLiquidacionS3Request,
										onErrorRequest, filtro);
							}
							function onSuccessListaLiquidacionS3Request(json) {
								if (json.resultados !== undefined
										&& json.resultados.lista !== undefined) {
									var datos = json.resultados.lista;
									var contador = 0;
									var estilo = null;

									angular
											.forEach(
													datos,
													function(item, key) {
														if (contador % 2 == 0) {
															estilo = "even";
														} else {
															estilo = "odd";
														}
														contador++;

														var enlace = "";
														if (item.cdestados3 == "ERR") {
															enlace = "ng-click=\"desplegarErrores("
																	+ item.idliquidacion
																	+ ", '"
																	+ $scope.currentRow.numero_fila
																	+ contador
																	+ "')\" ";
															enlace = enlace
																	+ " style=\"cursor: pointer; color: #ff0000;\"";
														} else {
															enlace = "";
														}

														angular
																.element(
																		"#tablaParamLIQ"
																				+ $scope.currentRow.numero_fila)
																.append(
																		"<tr class='"
																				+ estilo
																				+ "' id='filaRecuperadaLIQ"
																				+ $scope.currentRow.numero_fila
																				+ "' style='background-color: #f5ebc5 !important;' >"
																				+ "<td class=\"taleft\""
																				+ enlace
																				+ " style='background-color: #f5ebc5 !important;'>"
																				+ item.cdestados3
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ transformaFechaGuion(item.fechaliquidacion)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ transformaFechaGuion(item.fechaoperacion)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ transformaFechaGuion(item.fechavalor)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.nutiuloliquidados
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ a2digitos(item.importenetoliquidado)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ a2digitos(item.importeefectivo)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ a2digitos(item.importecorretaje)
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ item.contabilizado
																				+ "</td>"
																				+ "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
																				+ transformaFechaGuion(item.fechacontabilidad)
																				+ "</td>"
																				+ "</tr>");

														if (item.cdestados3 == "ERR") {
															angular
																	.element(
																			"#tablaParamLIQ"
																					+ $scope.currentRow.numero_fila)
																	.append(
																			"<tr id='tabla_a_desplegar_ERR"
																					+ numero_fila
																					+ contador
																					+ "' style='display:none;'>"
																					+ "<td colspan='10' style='border-width:0 !important;background-color:#dfdfdf;'>"
																					+ "<table id='tablaParamERR"
																					+ $scope.currentRow.numero_fila
																					+ contador
																					+ "' style=width:100%; margin-top:5px;'>"
																					+ "<tr>"
																					+ "<th class='taleft' >Error cliente</th>"
																					+ "<th class='taleft' >Error SV</th>"
																					+ "<th class='taleft' >Descripción</th>"
																					+ "</tr></table></td></tr>");
														}

													});
									angular
											.element(
													"#tablaParamLIQ"
															+ $scope.currentRow.numero_fila)
											.append("</table>");
								}
								desplegar(
										'tabla_a_desplegar_LIQ'
												+ $scope.currentRow.numero_fila,
										'estadoT'
												+ $scope.currentRow.numero_fila,
										'estadoT'
												+ $scope.currentRow.numero_fila);
								$.unblockUI();
							}
							$scope.currentRowLiquidacion = {
								id : "",
								posicion : ""
							};
							// funcion para desplegar la tabla de
							// parametrizacion
							$scope.desplegarErrores = function(idLiquidacion,
									posicion) {
								$scope.currentRowLiquidacion.id = idLiquidacion;
								$scope.currentRowLiquidacion.posicion = posicion;

								console.log("desplegarErrores");
								var existe_tabla = document
										.getElementById('filaRecuperadaERR'
												+ posicion);

								if (existe_tabla != null) {
									desplegar('tabla_a_desplegar_ERR'
											+ posicion, 'estadoT' + posicion,
											'estadoT' + posicion);
									return;
								}
								var filters = {
									idliquidacion : idLiquidacion
								};
								inicializarLoading();
								EntregaRecepcionService.listaErroresS3(
										onSuccessListaErroresS3Request,
										onErrorRequest, filters);
							};
							function onSuccessListaErroresS3Request(json) {
								console.log(json);
								console.log(json.resultados);
								if (json.resultados !== undefined
										&& json.resultados.lista !== undefined) {
									var datos = json.resultados.lista;
									var item = null;
									var estilo = null;
									var posicion = $scope.currentRowLiquidacion.posicion;
									angular
											.forEach(
													datos,
													function(item, key) {

														$(
																"#tablaParamERR"
																		+ posicion)
																.append(
																		"<tr class='odd' id='filaRecuperadaERR"
																				+ posicion
																				+ "' >"
																				+ "<td class='taleft' >"
																				+ item.datoerrorcliente
																				+ "</td>"
																				+ "<td class='taleft' >"
																				+ item.datoerrorsv
																				+ "</td>"
																				+ "<td class='taleft' >"
																				+ item.dserror
																				+ "</td>"
																				+ "</tr>");
													});
									angular
											.element(
													"#tablaParamERR" + posicion)
											.append("</table>");
								}

								desplegar('tabla_a_desplegar_ERR' + posicion,
										'estadoT' + posicion, 'estadoT'
												+ posicion);
								$.unblockUI();
							}

							// funcion para desplegar las subtablas
							function desplegar(tabla_a_desplegar, e1,
									estadoTfila) {
								var tablA = document
										.getElementById(tabla_a_desplegar);
								var estadOt = document.getElementById(e1);
								var fila = angular.element('#' + estadoTfila);
								// se habilitan los eventos click en la tabla
								// que se va a mostrar
								// esto es renderizando las filas que estaban
								// ocultas
								angular.element(tablA).find('tr').each(
										function(key, val) {
											$compile(val)($scope);
										});
								switch (tablA.style.display) {
								case "none":
									tablA.style.display = "";
									if (estadOt != null) {
										estadOt.innerHTML = "Ocultar";
									}
									console.log('foco en tabla '+tabla_a_desplegar);
									document.getElementById(tabla_a_desplegar).scrollIntoView();
									break;

								default:
									tablA.style.display = "none";
									if (estadOt != null) {
										estadOt.innerHTML = "Mostrar";
									}
									break;
								}
							}

							function seguimientoBusqueda() {

								$scope.followSearch = "";
								var cadenaFiltros = "";
								if ($scope.filtro.fcontratacionDe !== "") {
									$scope.followSearch += " F. Contratacion Desde: "
											+ $scope.filtro.fcontratacionDe;
								}
								if ($scope.filtro.fcontratacionA !== "") {
									$scope.followSearch += " F. Contratacion Hasta: "
											+ $scope.filtro.fcontratacionA;
								}
								if ($scope.filtro.fliquidacionDe !== "") {
									$scope.followSearch += " F. liquidacion Desde: "
											+ $scope.filtro.fliquidacionDe;
								}
								if ($scope.filtro.fliquidacionA !== "") {
									$scope.followSearch += " F. liquidacion Hasta: "
											+ $scope.filtro.fliquidacionA;
								}
						        if ($scope.filtro.alias !== "") {
						        	if($scope.filtro.chalias)
						        		$scope.followSearch += " Alias: " + document.getElementById("alias").value;
						        	else
						        		$scope.followSearch += " Alias distinto: " + document.getElementById("alias").value;;
						        }
						        if ($scope.filtro.isin !== "") {
						      	  if($scope.filtro.chisin)
						      		  $scope.followSearch += " Isin: " + document.getElementById("isin").value;
						      	  else
						      		  $scope.followSearch += " Isin distinto: " + document.getElementById("isin").value;
						  	    }
							}
							/** ISINES * */
							function loadIsines() {
								IsinService.getIsines(cargarIsines,
										onErrorRequest);
							}

							/** Alias * */
							function loadAlias() {
								inicializarLoading();
								AliasService.getAlias().success(cargaAlias)
										.error(onErrorRequest);
							}
							function cargarIsines(datosIsin) {
								var availableTags = [];
								angular.forEach(datosIsin, function(val, key) {
									var ponerTag = datosIsin[key].codigo
											+ " - "
											+ datosIsin[key].descripcion;
									availableTags.push(ponerTag);
								});
								angular.element("#isin").autocomplete({
									source : availableTags
								});
							}
							function cargaAlias(data) {
								var availableTagsAlias = [];
								angular.forEach(data.resultados.result_alias,
										function(val, key) {
											var ponerTag = val.alias + " - "
													+ val.descripcion;
											availableTagsAlias.push(ponerTag);
										});
								// código de autocompletar
								angular.element("#alias").autocomplete({
									source : availableTagsAlias
								});
								$.unblockUI();
							}
						    function getAliasId() {
//						      var alias = $scope.filtro.alias.trim();
						      var alias = $("#alias").val();
						      var guion = alias.indexOf("-");
						      return (guion > 0) ? alias.substring(0, guion).trim() : alias;
						    } // getAliasId
							function getIsin() {
								var isinCompleto = document.getElementById("isin").value; //modificado por alex 19Jan para realizar la busqueda correcta en Chrome
								//var isinCompleto = $scope.filtro.isin.trim();
								var guion = isinCompleto.indexOf("-");
								if (guion > 0) {
									return isinCompleto.substring(0, guion);
								} else {
									return isinCompleto;
								}
							}
							function onErrorRequest(data) {
								$.unblockUI();
								growl
										.addErrorMessage("Ocurrió un error durante la petición de datos.");
							}

					    	$scope.btnInvAlias = function() {
					    		$scope.btnInvGenerico('Alias');
					    	}

					    	$scope.btnInvIsin = function() {
					    		$scope.btnInvGenerico('Isin');
					    	}


					    	$scope.btnInvGenerico = function( nombre ) {

					      	  var check = '$scope.filtro.ch' + nombre.toLowerCase();

					        	  if(eval(check)){
					        		  	  var valor2 = check + "= false";
					        		  	  eval(valor2 ) ;

					          		  document.getElementById('textBtnInv'+nombre).innerHTML = "<>";
					          		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "red"});
					          		  document.getElementById('textInv'+nombre).innerHTML = "DISTINTO";
					          	  }else{
					                 	  var valor2 = check + "= true";
					              	  eval(valor2 ) ;

					          		  document.getElementById('textBtnInv'+nombre).innerHTML = "=";
					          		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "green"});
					          		  document.getElementById('textInv'+nombre).innerHTML = "IGUAL";
					          	  }
					          }

						} ]);
