'use strict';
sibbac20
    .controller(
                'BajasManualesController',
                [
                 '$scope',
                 '$filter',
                 '$document',
                 'growl',
                 'BajasManualesService',
                 'CombosFacturasManualesService',
                 '$compile',
                 'SecurityService',
                 '$location',
                 'Logger',
                 '$rootScope',
                
                 function ($scope, $filter, $document, growl, BajasManualesService, CombosFacturasManualesService,
                           $compile, SecurityService, $location, Logger, $rootScope) {

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   var hoy = new Date();
                   var dd = hoy.getDate();
                   var mm = hoy.getMonth() + 1;
                   var yyyy = hoy.getFullYear();
                   hoy = yyyy + "_" + mm + "_" + dd;

                   $scope.showingFilter = true;

                   $scope.listFacturasContabilizadas = [];

                   $scope.facturasContabilizadasSeleccionadas = [];
                   $scope.facturasContabilizadasSeleccionadasBorrar = [];

                   $scope.listAlias = [];
                   $scope.listCodigosImpuestos = [];
                   $scope.listCausasExencion = [];
                   $scope.listClaveRegimen = [];
                   $scope.listTiposFactura = [];
                   $scope.listMoneda = [];
                   $scope.listEntregaBien = [];
                   $scope.listEstados = [];

                   $scope.aliasFiltro = null;
                   $scope.aliasFactura = null;

                   $scope.resetFiltro = function () {
                     $scope.filtro = {
                       nbDocNumero : "",
                       idAlias : "",
                       fechaHasta : "",
                       fechaDesde : "",
                     };
                     $scope.aliasFiltro = "";
                   };

                   $scope.resetFiltro();

                   $scope.cliente = {
                     tpdocumento : null,
                     numdocumento : null,
                     paisResidencia : null,
                     tipoResidencia : null,
                     provincia : null
                   };

                   $scope.today = $filter('date')(new Date(), "dd/MM/yyyy");
                   // Reset objeto plantilla
                   $scope.resetFactura = function () {
                     $scope.factura = {
                       id : null,
                       auditUser : $rootScope.userName,
                       nbDocNumero : null,
                       idAlias : null,
                       idEstado : "",
                       idMoneda : "",
                       idTipoFactura : "",
                       idCausaExencion : "",
                       idClaveRegimen : "",
                       impBaseImponible : null,
                       impImpuesto : null,
                       fhFechaCre : $scope.today,
                       fhFechaFac : null,
                       fhInicio : $scope.today,
                       idCodImpuesto : null,
                       idEntregaBien : "",
                       coefImpCostes : null,
                       baseImponCostes : null,
                       cuotaRepercut : null,
                       nbComentarios : "",
                       imFinSvb : null,
                       imFinDiv : null
                     };
                     $scope.aliasFactura = null;
                     $scope.activeAlert = false;
                   };

                   $scope.resetFactura();

                   $scope.oTable = $("#datosFacturasManualesBajas").dataTable({
                     "dom" : 'T<"clear">lfrtip',
                     "tableTools" : {
                       "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
                       "aButtons" : [ "copy", {
                         "sExtends" : "csv",
                         "sFileName" : "Listado_Facturas_Manuales_Informes_" + hoy + ".csv",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                       }, {
                         "sExtends" : "xls",
                         "sFileName" : "Listado_Facturas_Manuales_Informes_" + hoy + ".xls",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                       }, {
                         "sExtends" : "pdf",
                         "sPdfOrientation" : "landscape",
                         "sTitle" : " ",
                         "sPdfSize" : "A3",
                         "sPdfMessage" : "Listado Final Holders",
                         "sFileName" : "Listado_Facturas_Manuales_Informes_" + hoy + ".pdf",
                         "mColumns" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                       }, "print" ]
                     },
                     "aoColumns" : [ {
                       sClass : "centrar",
                       bSortable : false,
                       width : "7%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     }, {
                       sClass : "centrar",
                       width : "10%"
                     } ],
                     "fnCreatedRow" : function (nRow, aData, iDataIndex) {

                       $compile(nRow)($scope);
                     },

                     "scrollY" : "480px",
                     "scrollX" : "100%",
                     "scrollCollapse" : true,
                     "language" : {
                       "url" : "i18n/Spanish.json"
                     }
                   });

                   $scope.seleccionarElemento = function (row) {
                     if ($scope.listFacturasContabilizadas[row].selected) {
                       $scope.listFacturasContabilizadas[row].selected = false;
                       for (var i = 0; i < $scope.facturasContabilizadasSeleccionadas.length; i++) {
                         if ($scope.facturasContabilizadasSeleccionadas[i].id === $scope.listFacturasContabilizadas[row].id) {
                           $scope.facturasContabilizadasSeleccionadas.splice(i, 1);
                           $scope.facturasContabilizadasSeleccionadasBorrar.splice(i, 1);
                         }
                       }
                     } else {
                       $scope.listFacturasContabilizadas[row].selected = true;
                       $scope.facturasContabilizadasSeleccionadas.push($scope.listFacturasContabilizadas[row]);
                       $scope.facturasContabilizadasSeleccionadasBorrar.push($scope.listFacturasContabilizadas[row].id);

                     }
                   };

                   var pupulateAutocomplete = function (input, availableTags) {
                     $(input).autocomplete({
                       minLength : 0,
                       source : availableTags,
                       focus : function (event, ui) {
                         return false;
                       },
                       select : function (event, ui) {

                         var option = {
                           key : ui.item.key,
                           value : ui.item.value
                         };

                         switch (input) {
                           case "#filtro_alias":
                             $scope.filtro.idAlias = option.key;
                             $scope.aliasFiltro = option.value;
                             break;
                           case "#factura_alias":
                             $scope.factura.idAlias = option.key;
                             $scope.aliasFactura = option.value;
                             break;
                           default:
                             break;
                         }

                         $scope.safeApply();
                         return false;
                       }
                     });
                   };

                   $scope.consultarAlias = function () {
                     CombosFacturasManualesService.consultarAlias(function (data) {
                       $scope.listAlias = data.resultados["listaAlias"];
                       pupulateAutocomplete("#filtro_alias", $scope.listAlias);
                       pupulateAutocomplete("#factura_alias", $scope.listAlias);
                       $scope.safeApply();
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo de Alias.", 1);
                     });
                   };

                   $scope.consultarAlias();

                   $scope.consultarCausasExencion = function () {
                     CombosFacturasManualesService.consultarCausasExencion(function (data) {
                       $scope.listCausasExencion = data.resultados["listaCausasExencion"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Causa Exencion.", 1);
                     });
                   };

                   $scope.consultarCausasExencion();

                   $scope.consultarClaveRegimen = function () {
                     CombosFacturasManualesService.consultarClaveRegimen(function (data) {
                       $scope.listClaveRegimen = data.resultados["listaClaveRegimen"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Clave Regimen.", 1);
                     });
                   };

                   $scope.consultarClaveRegimen();

                   $scope.consultarCodigosImpuestos = function () {
                     CombosFacturasManualesService.consultarCodigosImpuestos(function (data) {
                       $scope.listCodigosImpuestos = data.resultados["listaCodigosImpuestos"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Codigos Impuestos.", 1);
                     });
                   };

                   $scope.consultarCodigosImpuestos();

                   $scope.consultarTiposFactura = function () {
                     CombosFacturasManualesService.consultarTiposFactura(function (data) {
                       $scope.listTiposFactura = data.resultados["listaTiposFacturas"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Tipos Facturas.", 1);
                     });
                   };

                   $scope.consultarTiposFactura();

                   $scope.consultarMonedas = function () {
                     CombosFacturasManualesService.consultarMonedas(function (data) {
                       $scope.listMonedas = data.resultados["listaMonedas"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del Combo Monedas.", 1);
                     });
                   };

                   $scope.consultarMonedas();

                   $scope.consultarEntregaBien = function () {
                     CombosFacturasManualesService.consultarEntregaBien(function (data) {
                       $scope.listEntregaBien = data.resultados["listaEntregaBien"];
                     }, function (error) {
                       fErrorTxt("Se produjo un error en la carga del combo Entrega Bien.", 1);
                     });
                   };

                   $scope.consultarEntregaBien();

                   $scope.consultar = function () {
                     $scope.showingFilter = false;
                     $scope.consultarFacturasManuales();
                   }

                   $scope.consultarFacturasContabilizadas = function () {

                     inicializarLoading();

                     $scope.facturasContabilizadasSeleccionadasBorrar = [];
                     $scope.facturasContabilizadasSeleccionadas = [];

                     BajasManualesService
                         .consultarFacturasContabilizadas(
                                                          function (data) {
                                                            if (data.resultados.status === 'KO') {
                                                              $.unblockUI();
                                                              fErrorTxt(
                                                                        "Se produjo un error en la carga del listado de bajas manuales.",
                                                                        1);
                                                            } else {

                                                              $scope.listFacturasContabilizadas = data.resultados["listaFacturasContabilizadas"];

                                                              // borra el contenido del body de la tabla
                                                              var tbl = $("#datosFacturasManuales > tbody");
                                                              $(tbl).html("");
                                                              $scope.oTable.fnClearTable();

                                                              for (var i = 0; i < $scope.listFacturasContabilizadas.length; i++) {

                                                                var check = '<input style= "width:20px" type="checkbox" class="editor-active" ng-checked="listFacturasContabilizadas['
                                                                            + i
                                                                            + '].selected" ng-click="seleccionarElemento('
                                                                            + i + ');"/>';

                                                                var facturasList = [
                                                                                    check,
                                                                                    $scope.listFacturasContabilizadas[i].nbDocNumero,
                                                                                    $scope.listFacturasContabilizadas[i].empContraparte,
                                                                                    $filter('date')
                                                                                        (
                                                                                         $scope.listFacturasContabilizadas[i].fhFechaCre,
                                                                                         "dd/MM/yyyy"),
                                                                                    $filter('date')
                                                                                        (
                                                                                         $scope.listFacturasContabilizadas[i].fhFechaFac,
                                                                                         "dd/MM/yyyy"),
                                                                                    $scope.listFacturasContabilizadas[i].estado,
                                                                                    $scope.listFacturasContabilizadas[i].impBaseImponible,
                                                                                    $scope.listFacturasContabilizadas[i].impImpuesto,
                                                                                    $scope.listFacturasContabilizadas[i].imFinSvb,
                                                                                    $scope.listFacturasContabilizadas[i].coefImpCostes,
                                                                                    $scope.listFacturasContabilizadas[i].baseImponCostes,
                                                                                    $scope.listFacturasContabilizadas[i].cuotaRepercut ];

                                                                $scope.oTable.fnAddData(facturasList, false);

                                                              }
                                                              $scope.oTable.fnDraw();
                                                              $scope.safeApply();
                                                              $.unblockUI();

                                                            }
                                                          },
                                                          function (error) {
                                                            $.unblockUI();
                                                            fErrorTxt(
                                                                      "Se produjo un error en la carga del listado de factura contabilizadas.",
                                                                      1);
                                                          }, $scope.filtro);
                   };

                   // Abrir modal modificar informes
                   $scope.abrirConsultarFactura = function () {

                     if ($scope.facturasContabilizadasSeleccionadas.length > 1
                         || $scope.facturasContabilizadasSeleccionadas.length == 0) {
                       if ($scope.facturasContabilizadasSeleccionadas.length == 0) {
                         fErrorTxt("Debe seleccionar al menos un elemento de la tabla de facturas contabilizadas", 2)
                       } else {
                         fErrorTxt("Debe seleccionar solo un elemento de la tabla de de facturas contabilizadas.", 2)
                       }
                     } else {

                       $scope.resetFactura();
                       $scope.factura = angular.copy($scope.facturasContabilizadasSeleccionadas[0]);

                       BajasManualesService.detalleBajaManual(function (data) {

                         $scope.factura = data.resultados["detalleFacturaContabilizada"];

                         $scope.factura.fhFechaCre = $filter('date')($scope.factura.fhFechaCre, "dd/MM/yyyy");

                         for (var j = 0; j < $scope.listAlias.length; j++) {
                           if ($scope.listAlias[j].key == $scope.factura.idAlias) {
                             $scope.aliasFactura = $scope.listAlias[j].value;
                           }
                         }

                         $scope.modalFactura = {
                           titulo : "Consular Factura"
                         };

                         angular.element('#formularios').modal({
                           backdrop : 'static'
                         });
                         $(".modal-backdrop").remove();

                       }, function (e) {
                         $.unblockUI();
                         fErrorTxt("Ocurrió un error durante la petición de datos al recuperar el objeto factura.", 1);
                       }, $scope.factura);
                     }
                   };

                   $scope.anularFactura = function () {

                     if ($scope.facturasContabilizadasSeleccionadasBorrar.length > 0) {
                       if ($scope.facturasContabilizadasSeleccionadasBorrar.length == 1) {
                         angular.element("#dialog-confirm").html("¿Desea anular la factura manual seleccionada?");
                       } else {
                         angular.element("#dialog-confirm")
                             .html(
                                   "¿Desea anular las " + $scope.facturasContabilizadasSeleccionadasBorrar.length
                                       + " facturas manuales seleccionadas?");
                       }

                       // Define the Dialog and its properties.
                       angular
                           .element("#dialog-confirm")
                           .dialog(
                                   {
                                     resizable : false,
                                     modal : true,
                                     title : "Mensaje de Confirmación",
                                     height : 150,
                                     width : 360,
                                     buttons : {
                                       " Sí " : function () {
                                         BajasManualesService
                                             .anularBajasManuales(
                                                                  function (data) {

                                                                    if (data.resultados.status === 'KO') {
                                                                      $.unblockUI();
                                                                      fErrorTxt(data.error, 1);
                                                                    } else {
                                                                      $scope.consultarFacturasContabilizadas();
                                                                      fErrorTxt('Facturas anuladas correctamente', 3);
                                                                    }
                                                                  },
                                                                  function (error) {
                                                                    $.unblockUI();
                                                                    fErrorTxt(
                                                                              "Ocurrió un error durante la petición de datos al eliminar las facturas manuales.",
                                                                              1);
                                                                  },
                                                                  {
                                                                    listaFacturasManualesAnular : $scope.facturasContabilizadasSeleccionadasBorrar
                                                                  });

                                         $(this).dialog('close');
                                       },
                                       " No " : function () {
                                         $(this).dialog('close');
                                       }
                                     }
                                   });
                       $('.ui-dialog-titlebar-close').remove();
                     } else {
                       fErrorTxt("Debe seleccionar al menos un elemento de la tabla de facturas Manuales.", 2)
                     }

                   };

                   /***************************************************************************************************
                     * ** ACCIONES TABLA ***
                     **************************************************************************************************/
                   $scope.seleccionarTodos = function () {
                     // Se inicializa los elementos que ya tuviera la lista de elementos a borrar.
                     $scope.facturasContabilizadasSeleccionadasBorrar = [];
                     for (var i = 0; i < $scope.listFacturasContabilizadas.length; i++) {
                       $scope.listFacturasContabilizadas[i].selected = true;
                       $scope.facturasContabilizadasSeleccionadasBorrar.push($scope.listFacturasContabilizadas[i].id);
                     }
                     $scope.facturasContabilizadasSeleccionadas = angular.copy($scope.listFacturasContabilizadas);
                   };

                   $scope.deseleccionarTodos = function () {
                     for (var i = 0; i < $scope.listFacturasContabilizadas.length; i++) {
                       $scope.listFacturasContabilizadas[i].selected = false;
                     }
                     $scope.facturasContabilizadasSeleccionadas = [];
                     $scope.facturasContabilizadasSeleccionadasBorrar = [];
                   };

                 } ]);
