'use strict';
sibbac20.controller('CargaFicheroSwiftController',
    ['$scope', '$document', 'growl', 'CargaFicheroSwiftService',  '$compile',
    function ($scope, $document, growl, CargaFicheroSwiftService,  $compile) {

    	/* DECLARACION */

    	$document.ready(function () {

    	 });


    	$scope.ImportarFicheroSWIFT = function(){



    		// Validaciones del fichero.
    		// Tiene que seleccionar un fichero de tipo DAT y que empieze por BICDUFI
			var file = document.getElementById('selecFicheroSWIFT').files[0];
		    if (file) {


				 // El fichero tiene que empezar por el texto 'BICDUFI'
	    		if( file.name.substring(0,7) !=  "BICDUFI"){
	    			fErrorTxt("El fichero tiene que empezar por el nombre BICDUFI",1);
	    			return;
	    		}

	    		// el tipo de fichero tiene que ser .DAT
	    		var extension = file.name.substring(file.name.length - 4) ;


	    		if( extension  !=  ".DAT"){
	    			fErrorTxt("Solo se tratan ficheros con extension .DAT",1);
	    			return;
	    		}



		        // create reader
		        var reader = new FileReader();
		        var btoaTxt;

		        reader.readAsBinaryString(file);


		        reader.onerror = function(event) {
		        	fErrorTxt("Se ha producido un error al leer el fichero.",1);
	    			 $.unblockUI();
		        };

                // Cuando tenemos cargado en memoria todo el fichero, se lo enviamos al servicio rest
		        reader.onload = function (readerEvt) {

		        	 var datos = readerEvt.target.result;
		        	 var lines = datos.split(/[\r\n]+/g);
		        	 var numLineas = lines.length;
		        	 var filtro = {file : datos, filename : file.name };

		   			 angular.element("#dialog-confirm").html("Va a cargar "+ numLineas +" registros.  Este proceso en funcion del volumen de información a cargar, puede tardar bastante tiempo. ¿Desea continuar?");

		    	    // Define the Dialog and its properties.
		    	    angular.element("#dialog-confirm").dialog({
		    	        resizable: false,
		    	        modal: true,
		    	        title: "Mensaje de Confirmación",
		    	        height: 225,
		    	        width: 360,
		    	        buttons: {
		    	        	 " Sí ": function () {
		    	             	 // Se muestra la capa cargando
		    	 	 	         inicializarLoading();
		    	        		 CargaFicheroSwiftService.importarFichero(onSuccessImportarFichero,onErrorRequest, filtro);
		    	            	$(this).dialog('close');

		    		            },
		    		            " No ": function () {
		    		                $(this).dialog('close');
		    		            }
		    		   }
		    	    });
		    	    $('.ui-dialog-titlebar-close').remove();



		        }

    		}else // No ha seleccionado ningun fichero
    			fErrorTxt("No ha seleccionado ningun fichero.",2);

    	}

        /** Procesa un error durante la petición de dartos al servidor. */
        function onErrorRequest(data) {
          $.unblockUI();
//          growl.addErrorMessage("Ocurrió un error durante la petición de datos.");
          fErrorTxt("Ocurrió un error durante la petición de datos.",1);
        } // onErrorRequest


        function onSuccessImportarFichero(json){
        	$.unblockUI();
        	console.log(json);
        	console.log(json.resultados);

        	if (json.resultados !== null && json.resultados !== undefined && json.resultados.status !== undefined){

        		if (json.resultados.status === 'KO') {
        			fErrorTxt( json.error, 1);
        		}else{
        			fErrorTxt( "Fichero importado correctamente", 1);
        			$("#selecFicheroSWIFT").val('');
        		}
        	}else{

        		fErrorTxt(json.error,3);
        	}
        }



    }]
);
