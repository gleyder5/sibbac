sibbac20.controller('BalanceController', ['$scope', 'IsinService', 'CuentaLiquidacionService', '$document', 'growl', 'ConciliacionBalanceService', 'MovimientoVirtualService',function ($scope, IsinService, CuentaLiquidacionService, $document, growl, ConciliacionBalanceService, MovimientoVirtualService) {
        $scope.isines = [];
        $scope.cuentas = [];
        $scope.fliquidacionDeAux = "";
        $scope.fliquidacionAAux = "";
        $scope.filter = {
            cdisin: '',
            fliquidacionDe: '',
            fliquidacionA: '',
            cuentaLiquidacion: '',
            chcdisin: true,
            chcuentaliquidacion: true
        };
        $scope.followSearch = "";
        IsinService.getIsines();

        $scope.fechaHoy = "";

        /** ISINES **/
        IsinService.getIsines().success(function (data, status, headers, config) {
            console.log("sisines cargados: " + data);
            if (data != null && data.resultados != null &&
                    data.resultados.result_isin != null) {
                $scope.isines = data.resultados.result_isin;
                datosIsin = $scope.isines;
                $.each(datosIsin, function (key, val) {
                    ponerTag = datosIsin[key].codigo + " - " + datosIsin[key].descripcion;
                    availableTags.push(ponerTag);
                });
                $("#cdisin").autocomplete({
                    source: availableTags
                });

            }
        }).error(function (data, status, headers, config) {
            console.log("error al cargar isines: " + status);
        });
        /** CUENTAS **/
        CuentaLiquidacionService.getCuentasLiquidacion().success(function (data, status, header, config) {
            if (data !== null && data.resultados !== null && data.resultados.cuentasLiquidacion !== null) {
                $scope.cuentas = data.resultados.cuentasLiquidacion;
            }
        }).error(function (data, status, headers, config) {
            console.log("error al cargar cuentas: " + status);
        });
        var availableTags = [];
        var oTable = undefined;

        $document.ready(function () {
            if (oTable === undefined) {
                initializeDataTable();
            }
            initializePicker();
            cargarFechaActualizacionMovimientoAlias();
            getFechaPrevia();
            prepareCollapsion();
            $scope.fechaHoy = getfechaHoy();
        });
        function initializeDataTable() {
            oTable = $("#tblBalance").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "aoColumns": [
                    {"width": "auto","sClass": "centrado", "type": 'centrado'},//Fecha Liquidación
                    {"sClass": "align-left"},
                    {"sClass": "align-left"},
                    {"sClass": "align-left"},
                    {"sClass": "monedaR", "type": 'formatted-num'}
                ]
            });
        }
        function initializePicker() {
            loadpicker(['fliquidacionDe', 'fliquidacionA']);
        }

//        $scope.submitForm = function(event) {
        $scope.Consultar = function() {

            if (!validarFecha("fliquidacionDe")) {
            	fErrorTxt("La Fecha no tiene el formato correcto", 1);
            } else {

            	if (comprobarfecha($scope.filter.fliquidacionDe,$scope.fechaHoy) == 1)
            	{
            		fErrorTxt("No se permiten fechas mayores a hoy",1);
            	}
            	else if($scope.filter.cuentaLiquidacion.trim() == ''){
            		fErrorTxt("La cuenta de liquidación es obligatoria",1);
            	}else	{
            		obtenerDatos();
                    collapseSearchForm();
                    seguimientoBusqueda();
            	}
            }
        }

        function obtenerDatos() {

        	var isin = (!$scope.filter.chcdisin && getIsin() !== "" ? "#" : "") + getIsin();
        	var cuenta = (!$scope.filter.chcuentaliquidacion && $scope.filter.cuentaLiquidacion.trim() !== "" ? "#" : "")  + $scope.filter.cuentaLiquidacion.trim();

        	var filters = {
                "isin": isin,
                "fliquidacionDe": transformaFechaInv($scope.filter.fliquidacionDe),
                "fliquidacionA": transformaFechaInv($scope.filter.fliquidacionA),
                "idCuentaLiquidacion": cuenta
            };
            growl.addInfoMessage('Enviando consulta....');
            inicializarLoading();
            ConciliacionBalanceService.getBalance(onSuccessBalanceRequest, onErrorRequestHandler, filters);
        }
        var screenBlock = null;
        function onSuccessBalanceRequest(json, status, headers, config) {
            screenBlock = inicializarLoading();
            if(json.error !== null){
                growl.addErrorMessage(json.error);
                return false;
            }
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
                    json.resultados.result_balance === undefined) {
                datos = {};
            } else {
                datos = json.resultados.result_balance;
            }
            growl.addSuccessMessage(datos.length+" registros obtenidos.");
            oTable.fnClearTable();
            //var row;
            angular.forEach(datos, function (val, key) {
            	var settlementdate = val.settlementdate;
                settlementdate = settlementdate !== null ? transformaFecha(settlementdate) : '';
                oTable.fnAddData([
                    "<span>" + settlementdate + "</span>",
                    "<span>" + val.isin + "</span>",
                    "<span>" + val.descrIsin + "</span>",
                    "<span>" + val.cdCodigo + "</span>",
                    "<span>" + $.number(val.titulos, 0, ',', '.') + "</span>"
                ], false);
            });
            oTable.fnDraw();
            $.unblockUI();

        }
        function onErrorRequestHandler(data, status, headers, config) {
            growl.addErrorMessage(data);
        }
        function seguimientoBusqueda() {
        	$scope.followSearch = "";
            if ($scope.filter.cdisin) {
            	if($scope.filter.chcdisin)
            		$scope.followSearch += " isin: " + document.getElementById("cdisin").value;
            	else
            		$scope.followSearch += " isin distinto: " + document.getElementById("cdisin").value;
            }
            if ($scope.filter.cdcodigocuentaliq !== '') {
            	if($scope.filter.chcuentaliquidacion)
            		$scope.followSearch += " cuenta: " + angular.element('#selectCuenta option:selected').html();
            	else
            		$scope.followSearch += " cuenta distinta: " + angular.element('#selectCuenta option:selected').html();
            }
            if ($scope.filter.fliquidacionDe !== '') {
            	$scope.followSearch += " fecha: " + $scope.filter.fliquidacionDe;
            }
        }
        function getIsin() {
        	var isinCompleto = document.getElementById("cdisin").value; //modificado por alex 19Jan para realizar la busqueda correcta en Chrome
            //var isinCompleto = $scope.filter.cdisin.trim();
            console.log("isinCompleto: " + isinCompleto);
            var guion = isinCompleto.indexOf("-");
            if (guion < 0) {
                return isinCompleto;
            } else {
                return isinCompleto.substring(0, guion).trim();
            }
        }

        function successGetFechaPrevia(datos) {
        	$scope.filter.fliquidacionDe = transformaFecha(datos[0]);
        }

        function getFechaPrevia() {
        	MovimientoVirtualService.getFechaPreviaLiquidacion(successGetFechaPrevia, onErrorRequestHandler);
        }
        //agregado por alex el 19Jan para que limpie de forma correcta los datos
        angular.element('#limpiar').click(function (event) {
            event.preventDefault();
            $('input[type=text]').val('');
            $scope.filter.cdisin='';
            $scope.filter.fliquidacionDe='';
            $scope.filter.fliquidacionA='';
            $scope.filter.cuentaLiquidacion='';
            $scope.filter = {
            		isin: "",
            		fliquidacionDe: "",
            		cuentaLiquidacion: "",
            		fliquidacionA: "",
                    chcdisin: false,
                    chcuentaliquidacion: false
            };
    		$scope.btnInvGenerico('CdIsin');
    		$scope.btnInvGenerico('CuentaLiquidacion')

        });
    	$scope.btnInvCdIsin = function() {
    		$scope.btnInvGenerico('CdIsin');
    	}

    	$scope.btnInvCuentaLiquidacion = function() {
    		$scope.btnInvGenerico('CuentaLiquidacion')
    	}

    	$scope.btnInvGenerico = function( nombre ) {

    	  var check = '$scope.filter.ch' + nombre.toLowerCase();

      	  if(eval(check)){
      		  	  var valor2 = check + "= false";
      		  	  eval(valor2 ) ;

        		  document.getElementById('textBtnInv'+nombre).innerHTML = "<>";
        		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "red"});
        		  document.getElementById('textInv'+nombre).innerHTML = "DISTINTO";
        	  }else{
               	  var valor2 = check + "= true";
            	  eval(valor2 ) ;

        		  document.getElementById('textBtnInv'+nombre).innerHTML = "=";
        		  angular.element('#textInv'+nombre).css({"background-color": "transparent", "color": "green"});
        		  document.getElementById('textInv'+nombre).innerHTML = "IGUAL";
        	  }
        }
    }]);

