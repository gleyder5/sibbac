sibbac20.controller('FacturaRectificativaController', ['$scope', 'growl', 'AliasService', 'FacturaService', function ($scope, growl, AliasService, FacturaService) {
        var idOcultos = [];
        var availableTags = [];
        var oTable = undefined;
        function loadAlias() {
            AliasService.getAliasFactura(cargarAlias, showError);
        }
        function showError(data, status, headers, config) {
            $.unblockUI();
            growl.addWarnMessage(data + " - " + status + " - " + headers);
        }

        $scope.populateTable = function (event) {
        	if (event !== null)
        	{
        		event.preventDefault();
        	}//if (event !== null)
            if (validarCampoFecha('fechaDesde', false) && validarCampoFecha('fechaHasta', false)) {
                var idAlias = getIdAlias();
                var fechaDesde = angular.element('#fechaDesde').val();
                var fechaHasta = angular.element('#fechaHasta').val();
                var fechas = ['fechaDesde', 'fechaHasta'];
                //loadpicker(fechas);
                //verificarFechas([fechas]);
                $("#ui-datepicker-div").remove();
                $('input#fechaDesde').datepicker({
                    onClose: function( selectedDate ) {
                      $( "#fechaHasta" ).datepicker( "option", "minDate", selectedDate );
                    } // onClose
                  });

                  $('input#fechaHasta').datepicker({
                    onClose: function( selectedDate ) {
                      $( "#fechaDesde" ).datepicker( "option", "maxDate", selectedDate );
                    } // onClose
                  });
                collapseSearchForm();
                var filtro = {idAlias: idAlias, fechaDesde: transformaFechaInv(fechaDesde),
                    fechaHasta: transformaFechaInv(fechaHasta)};
                inicializarLoading();
                FacturaService.getFacturasRectificativas(cargarTablaResultados, showError, filtro);

            } else {
                alert("Los campos de fecha introducidos no son correctos. Por favor revíselos.");
            }
        }

        $scope.limpiarFiltro = function() {
        	//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
            $( "#fechaDesde" ).datepicker( "option", "minDate", "");
            $( "#fechaDesde" ).datepicker( "option", "maxDate", "");
            //
            angular.element('input[type=text]').val('');
            angular.element('select').val('');
        };

        function prepareDataTable() {
            oTable = $(".tablaResultados").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollY": "480px",
                "scrollCollapse": true,
                "columns": [
                    {"width": "auto", "sClass": "align-center"},
                    {"width": "auto"},
                    {"width": "auto", type: 'date-eu', "sClass": "align-center"},
                    {"width": "auto", "sClass": "align-right"},
                    {"width": "auto", "sClass": "align-right"},
                    {"width": "auto", "sClass": "align-right"},
                    {"width": "auto", type: 'date-eu', "sClass": "align-center"},
                    {"width": "auto", type: 'date-eu', "sClass": "align-center"}
                ]
            });
        }
        $(document).ready(function () {
            prepareDataTable();
            prepareCollapsion();
            loadAlias();
            /* BUSCADOR */
            ajustarAnchoDivTable();
            cargarAlias();
            //var fechas = ['fechaDesde', 'fechaHasta'];
            //loadpicker(fechas);
            //verificarFechas([fechas]);
            $("#ui-datepicker-div").remove();
			$('input#fechaDesde').datepicker({
			    onClose: function( selectedDate ) {
			      $( "#fechaHasta" ).datepicker( "option", "minDate", selectedDate );
			    } // onClose
			  });

			  $('input#fechaHasta').datepicker({
			    onClose: function( selectedDate ) {
			      $( "#fechaDesde" ).datepicker( "option", "maxDate", selectedDate );
			    } // onClose
			  });
        });

        function getIdAlias() {
            var idAlias = "";
            var textAlias = document.getElementById("textAlias").value;
            if ((textAlias !== null) && (textAlias !== '')) {
                idAlias = idOcultos[availableTags.indexOf(textAlias)];
                if (idAlias === undefined) {
                    idAlias = "";
                }
            }
            return idAlias;
        }

        function cargarAlias(datos) {
            if (datos === undefined) {
                datos = {};
            }
            var item = null;
            for (var k in datos) {
                item = datos[k];
                idOcultos.push(item.id);
                availableTags.push(item.nombre.trim());
            }
            // código de autocompletar
            angular.element("#textAlias").autocomplete({
                source: availableTags
            });
            if (datos.length !== undefined) {
                growl.addInfoMessage(datos.length + " Alias cargados.");
            }
        }

        function cargarTablaResultados(datos)
        {
            var item = null;
            oTable.fnClearTable();
            // var row;
            for (var k in datos) {
                item = datos[k];
                oTable.fnAddData(["<span >" + item.numeroFactura + "</span>",
                    "<span >" + item.cdAlias.trim() + " - " + item.nombreAlias + "</span>",
                    "<span >" + transformaFecha(item.fechaCreacion) + "</span>",
                    "<span class=\"monedaR\">" + $.number(item.baseImponible, 2, ',', '.') + "</span>",
                    "<span class=\"monedaR\">" + $.number(item.importeImpuestos, 2, ',', '.') + "</span>",
                    "<span class=\"monedaR\">" + $.number(item.importeTotal, 2, ',', '.') + "</span>",
                    "<span >" + transformaFecha(item.fechaInicio) + "</span>",
                    "<span >" + transformaFecha(item.fechaFin) + "</span>"]);
            }
            $.unblockUI();
            growl.addSuccessMessage(datos.length + " Facturas encontradas.");
        }
        $scope.cambioAlias = function() {
        	angular.element('#nuFactura').val('');
        };

        $scope.cambioNuFactura = function() {
        	angular.element('#textAlias').val('');
        	angular.element('#fechaDesde').val('');
        	angular.element('#fechaHasta').val('');
        }
    }]);

