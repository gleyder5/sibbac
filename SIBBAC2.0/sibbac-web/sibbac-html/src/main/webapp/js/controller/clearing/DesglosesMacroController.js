'use strict';
sibbac20.controller('DesglosesMacroController',
    ['$scope', '$document', 'growl', 'DesglosesMacroService', 'AliasService', '$compile',
    function ($scope, $document, growl, DesglosesMacroService,  AliasService, $compile) {

        var calcDataTableHeight = function(sY) {
            if ($('#tablaDesglosesMacro').height() > 500) {
              return 480;
            } else {
              return $('#tablaDesglosesMacro').height() + 20;
            }
        };

        function ajustarAltoTabla (){
            console.log("ajustando inicio");
            var oSettings = oTable.fnSettings();
            var scrill = calcDataTableHeight(oTable.fnSettings().oScroll.sY);
            console.log("cuanto: " + scrill);
            oSettings.oScroll.sY = scrill;
            console.log("tamanio antes: " + $('.dataTables_scrollBody').height());
            $('.dataTables_scrollBody').height( scrill );
            console.log("tamanio despues: " + $('.dataTables_scrollBody').height());
            console.log("ajustando fin");
        }

        var oTable = undefined;
        $scope.numero_fila = 0;
        $scope.fechaliq = [];
        $scope.filtro = {
          fcontratacionDe: "",
          fcontratacionA: "",
          alias: "",
          cliente: "",
          chcliente: true,
          chalias: true
        };

        var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth()+1;
        var yyyy = hoy.getFullYear();
        hoy = yyyy + "_" + mm + "_" + dd ;

        var idOcultosAlias = [];
        var availableTagsAlias = [];
        var idOcultosClientes = [];
        var availableTagsClientes = [];

        $document.ready(function () {
            initialize();
            loadDataTable();
 //           prepareCollapsion();

            angular.element('#limpiar').click(function (event) {
                event.preventDefault();
                $( "#fcontratacionA" ).datepicker( "option", "minDate", "");
                $( "#fcontratacionDe" ).datepicker( "option", "maxDate", "");

                angular.element("#alias").val('');
                angular.element("#cliente").val('');

//                $("#selectedCliente").val('');
                $("#selectedCliente").find("option").remove().end();
                $("#selectedAlias").find("option").remove().end();

                $scope.filtro = {
                        fcontratacionDe: "",
                        fcontratacionA: "",
                        alias: "",
                        cliente: "",
                        chcliente: true,
                        chalias: true
                };
            });

            $('.collapser').click(function (event) {
        	    event.preventDefault();
        	    $(this).parents('.title_section').next().slideToggle();
        	    $(this).toggleClass('active');
        	    $(this).toggleText('Clic para mostrar', 'Clic para ocultar');
        	    return false;
           });

           $('.collapser_search').click(function (event) {
        	    event.preventDefault();
        	    $(this).parents('.title_section').next().slideToggle();
        	    $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        	    $(this).toggleClass('active');
        	    if ($(this).text().indexOf('Ocultar') !== -1) {
        	      $(this).text("Mostrar opciones de búsqueda");
        	      $('.mensajeBusqueda').show(750);
        	    } else {
        	      $(this).text("Ocultar opciones de búsqueda");
        	      $('.mensajeBusqueda').hide(1000);
        	    }
        	    return false;
          });

          seguimientoBusqueda();
      })

	  $("#ui-datepicker-div").remove();
	  $('input#fcontratacionDe').datepicker({
	     onClose: function( selectedDate ) {
	       $( "#fcontratacionA" ).datepicker( "option", "minDate", selectedDate );
	     } // onClose
	  });

      $('input#fcontratacionA').datepicker({
        onClose: function( selectedDate ) {
          $( "#fcontratacionDe" ).datepicker( "option", "maxDate", selectedDate );
        } // onClose
      });

        function initialize() {
	        inicializarLoading();
            cargaAlias()
            cargaClientes();

	        $.unblockUI();
        } // initialize

        /** Procesa un error durante la petición de dartos al servidor. */
        function onErrorRequest(data) {
           $.unblockUI();
           fErrorTxt("Ocurrió un error durante la petición de datos.",1);
        } // onErrorRequest

        function loadDataTable() {
           oTable = angular.element("#tablaDesglosesMacro").dataTable({
        		"dom" : 'T<"clear">lfrtip',
        		"tableTools" : {
        		  "sSwfPath" : [ "/sibbac20/js/swf/copy_csv_xls_pdf.swf"],
        	      "aButtons" : [ "copy",
        	                     { "sExtends" : "csv",
        	                       "sFileName": "Listado_de_Desglose_por_Macro_"+hoy+".csv",
        	                       },
        	                       {
        	                         "sExtends" : "xls",
        	                      	 "sFileName": "Listado_de_Desglose_por_Macro_"+hoy+".csv",
//        	                      	 "sCharSet" : "utf8",
        	                       },
        	                       {
        	                         "sExtends" : "pdf",
        	                      	 "sPdfOrientation": "landscape",
        	                      	 "sTitle": " ",
        	                      	 "sPdfSize": "A4",
        	                      	 "sPdfMessage": "Listado_de_Desglose_por_Macro.",
        	                      	 "sFileName": "Listado_de_Desglose_por_Macro_"+hoy+".pdf",
        	                       },
        	                       "print"
        	                     ]
        	    },

        		"language" : {
        			"url" : "i18n/Spanish.json"
        		},
        		"aoColumns": [
        		{sClass: "centrar"},
        		{sClass: "centrar"},
                {sClass: "centrar"},
                {sClass: "centrar"},
                {sClass: "centrar"},
                {sClass: "centrar"},
                {sClass: "monedaR", type: "formatted-num"},
                {sClass: "monedaR", type: "formatted-num"}
              ],
              "order": [[ 1, "asc" ]],
              "scrollX": "auto",
              "scrollCollapse": true,
              "bProcessing": true,
              "fnCreatedRow": function (row, data, index) {
                $compile(row)($scope);
              }, // "fnCreatedRow"
       	      drawCallback : function() {
    			   ajustarAltoTabla();

    	      }
           }); // oTable

        } // loadDataTable

    	// Botón de ejecutar la consulta.
    	$scope.Consultar = function() {

        	$('.collapser_search').click();
    		seguimientoBusqueda();
    	    cargaDesglosesMacros();

    	    return false;
    	}
    	$scope.btnInvCliente = function() {
      	  if($scope.filtro.chcliente){
    		  $scope.filtro.chcliente = false;
    		  document.getElementById('textBtnInvCliente').innerHTML = "<>";
    		  angular.element('#textInvCliente').css({"background-color": "transparent", "color": "red"});
    		  document.getElementById('textInvCliente').innerHTML = "NO INCLUIDOS";
    	  }else{
    		  $scope.filtro.chcliente = true;
    		  document.getElementById('textBtnInvCliente').innerHTML = "=";
    		  angular.element('#textInvCliente').css({"background-color": "transparent", "color": "green"});
    		  document.getElementById('textInvCliente').innerHTML = "INCLUIDOS";
    	  }
    	}

    	$scope.btnInvAlias= function() {
        	  if($scope.filtro.chalias){
      		  $scope.filtro.chalias = false;
      		  document.getElementById('textBtnInvAlias').innerHTML = "<>";
      		  angular.element('#textInvAlias').css({"background-color": "transparent", "color": "red"});
      		  document.getElementById('textInvAlias').innerHTML = "NO INCLUIDOS";
      	  }else{
      		  $scope.filtro.chalias = true;
      		  document.getElementById('textBtnInvAlias').innerHTML = "=";
      		  angular.element('#textInvAlias').css({"background-color": "transparent", "color": "green"});
      		  document.getElementById('textInvAlias').innerHTML = "INCLUIDOS";
      	  }
      	}


        function collapseSearchForm()
        {

            $('.collapser_search').parents('.title_section').next().slideToggle();
            $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
            $('.collapser_search').toggleClass('active');
            if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
            	$('.mensajeBusqueda').show(750);
                $('.collapser_search').text("Mostrar opciones de búsqueda");
            } else {
            	$('.mensajeBusqueda').hide(1000);
                $('.collapser_search').text("Ocultar opciones de búsqueda");
            }
        }

        function seguimientoBusqueda() {
          //cargarFechas();
          $scope.followSearch = "";

          if ($scope.filtro.fcontratacionDe !== "") {
            $scope.followSearch += " F. Contratacion Desde: " + $scope.filtro.fcontratacionDe;
          }
          if ($scope.filtro.fcontratacionA !== "") {
            $scope.followSearch += " F. Contratacion Hasta: " + $('#fcontratacionA').val();
          }
          if (obtenerIdsSeleccionados('#selectedAlias') !== "" && obtenerIdsSeleccionados('#selectedAlias') !== null) {
        	if($scope.filtro.chalias)
        		$scope.followSearch += " Alias: " + obtenerIdsSeleccionados('#selectedAlias');
        	else
        		$scope.followSearch += " Alias no incluya: " + obtenerIdsSeleccionados('#selectedAlias');
          }
          if (obtenerIdsSeleccionados('#selectedCliente') !== "" && obtenerIdsSeleccionados('#selectedCliente') !== null) {
        	  if($scope.filtro.chcliente)
        		  $scope.followSearch += " Cliente: " +obtenerIdsSeleccionados('#selectedCliente');
        	  else
        		  $scope.followSearch += " Cliente no incluya: " +obtenerIdsSeleccionados('#selectedCliente');
          }

        } // seguimientoBusqueda

        function cargaClientes(){

        	var filtro = {};

        	DesglosesMacroService.listaClientes(onSuccessClientes, onErrorRequest, filtro);

        }

        function cargaAlias(){

        	var filtro = {};

        	AliasService.getAlias(onSuccessAlias, onErrorRequest, filtro);
        }

        function cargarAliasCliente(){

            idOcultosAlias = [];
            availableTagsAlias = [];
        	var clienteSel = obtenerIdsSeleccionados('#selectedCliente')
        	$("#selectedAlias").find("option:selected").remove().end();

        	if(clienteSel !== null && clienteSel !== "" && clienteSel !== undefined ){
	        	var filtro = {"cliente" : clienteSel};

	        	AliasService.listaAliasCliente(onSuccessAliasCliente, onErrorRequest, filtro);
        	}else{
            	var filtro = {};

            	AliasService.getAlias(onSuccessAlias, onErrorRequest, filtro);
        	}
        }

    	function onSuccessClientes(json){
    		console.log(json);
    		console.log(json.resultados);

    		if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultado_lista_clientes !== undefined){
    			var datos = json.resultados.resultado_lista_clientes;
                var obj;
                $(datos).each(function (key, val) {
                    obj = {label: val.id + " - " + val.descripcion,value: val.id};
                    availableTagsClientes.push(obj);
                });
                $("#btnDelCliente").click(function (event) {
                    event.preventDefault();
                    $("#selectedCliente").find("option:selected").remove().end();
                    cargarAliasCliente();
                });
                pupulateClientesAutocomplete("#cliente", "#idTxCliente", availableTagsClientes, "#selectedCliente");
    		}else{

    			fErrorTxt(json.error,2);
    		}

    	}

        /** Carga el autocompletable de alias. */
        function onSuccessAlias(datosAlias) {

           console.log(datosAlias);
           console.log(datosAlias.resultados);
           if (datosAlias !== null){
        	   var obj;
	           angular.forEach(datosAlias, function (val, key) {
	        	   obj = {label: val.alias + " - " + val.descripcion,value: val.alias};
	               availableTagsAlias.push(obj);
	           });
	           $("#btnDelAlias").click(function (event) {
	               event.preventDefault();
	               $("#selectedAlias").find("option:selected").remove().end();
	           });
	           pupulateAliasAutocomplete("#alias", "#idTxAlias", availableTagsAlias, "#selectedAlias");
           }else{
   			   fErrorTxt(datosAlias.error,2);
           }
        } // onSuccessRequestAlias

        function onSuccessAliasCliente(datosAlias) {

            console.log(datosAlias);
            console.log(datosAlias.resultados);
            if (datosAlias !== null){
         	   var obj;
         	   var datos = datosAlias.resultados.listaAlias;
         	   for(var i = 0; i<datos.length; i++){
         		  obj = {label: datos[i].nombre,value: obtenerIdSeleccionadoText(datos[i].nombre)};
         		 availableTagsAlias.push(obj);
                  $("#selectedAlias").append("<option value=\"" + obj.value + "\" >" + obj.label + "</option>");
                  $("#selectedAlias").val("");
                  $("#selectedAlias" + "> option").attr('selected', 'selected');
         	   }

 	           $("#btnDelAlias").click(function (event) {
 	               event.preventDefault();
 	               $("#selectedAlias").find("option:selected").remove().end();
 	           });
 	           pupulateAliasAutocomplete("#alias", "#idTxAlias", availableTagsAlias, "#selectedAlias");
            }else{
    			   fErrorTxt(datosAlias.error,2);
            }
         } // onSuccessRequestAlias


        function pupulateClientesAutocomplete(input, idContainer, availableTagsCliente, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsCliente,
                focus: function (event, ui) {
                    // $( this ).val( ui.item.label );
                    return false;
                },
                select: function (event, ui) {
                    // $(idContainer).val(ui.item.value);
                    if (!valueExistInList(lista, ui.item.value)) {
                        $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                        $(input).val("");
                        $(lista + "> option").attr('selected', 'selected');
                        cargarAliasCliente();
                    }
                    return false;
                }
            });
        }

        function pupulateAliasAutocomplete(input, idContainer, availableTagsAlias, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsAlias,
                focus: function (event, ui) {
                    return false;
                },
                select: function (event, ui) {
                    // $(idContainer).val(ui.item.value);
                    if (!valueExistInList(lista, ui.item.value)) {
                        $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                        $(input).val("");
                        $(lista + "> option").attr('selected', 'selected');
                    }
                    return false;
                }
            });
        }

    	function cargaDesglosesMacros(){
//    		prepareCollapsion();
    		inicializarLoading();

	    	var fDesde =  transformaFechaInv($("#fcontratacionDe").val());
	    	var fHasta =  transformaFechaInv($("#fcontratacionA").val());

	    	var valorSeleccionadoAlias = obtenerIdsSeleccionados('#selectedAlias');
	    	var valorSeleccionadoCliente = obtenerIdsSeleccionados('#selectedCliente');
	    	console.log(valorSeleccionadoAlias);

	    	if( !$scope.filtro.chcliente ){
	    		valorSeleccionadoCliente = '#' + valorSeleccionadoCliente;
	    	}

	    	if( !$scope.filtro.chalias ){
	    		valorSeleccionadoAlias = '#' + valorSeleccionadoAlias;
	    	}

	    	var filtro = { "fechaDesde" : fDesde, "fechaHasta" : fHasta, "alias" : valorSeleccionadoAlias, "cliente" :valorSeleccionadoCliente };
			console.log(filtro);
			seguimientoBusqueda();
			$("#tablaDesglosesMacro > tbody").empty();

			DesglosesMacroService.listaDesglosesMacro(onSuccessDesglosesMacro,onErrorRequest, filtro);
        }

    	function onSuccessDesglosesMacro(json){
    		console.log(json);
    		console.log(json.resultados);

    		if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultado_lista_desgloses !== undefined){
    			datos = json.resultados.resultado_lista_desgloses;
    			var datos = json.resultados.resultado_lista_desgloses;
    			var tbl = $("#tablaDesglosesMacro > tbody");
    			$(tbl).html("");

    			console.log('datos' + datos);
    			oTable.fnClearTable();
    			angular.forEach(datos, function (item, key) {
    				var fechaContratacionFormato =  item.fechaContratacion ;
    				if(fechaContratacionFormato!=null && fechaContratacionFormato!=undefined  && fechaContratacionFormato!== ''){
    					var fechaCreaFormatoVector = fechaContratacionFormato.split("-");
    					fechaContratacionFormato = fechaCreaFormatoVector[2]+"/"+fechaCreaFormatoVector[1]+"/"+fechaCreaFormatoVector[0];
    				}

    				var rowIndex = oTable.fnAddData([
    				    item.cliente,
    				    item.alias,
    				    fechaContratacionFormato,
    				    item.referencia,
    				    item.isin,
    				    item.sentido,
    				    item.precio,
    				    item.titulos],
	                false);
	                var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
                })
				oTable.fnDraw();
				ajustarAltoTabla ();

				$.unblockUI();

    		}else{

    			fErrorTxt(json.error,2);
    		}


    	}

        function obtenerIdsSeleccionados(lista) {
            var selected = "";
            var i = 0;
            $(lista + ' option').each(function () {
                if (i > 0) {
                    selected += ";";
                }
                selected += $(this).val();
                i++;
            });
            if( selected === "" )
            	selected = null;
            return selected;
        }

        /** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
        function valueExistInList(lista, valor) {
            var inList = false;

            $(lista + ' option').each(function (index) {

                if (this.value == valor) {
                    inList = true;
                    return inList;
                }
            });

            return inList;
        }
        function obtenerIdSeleccionadoText( texto ){
        	if(texto.indexOf(" - ")!==-1){
        		return texto.substr(0,texto.indexOf(" - "))
        	}
        	return ""
        }
  }]
);
