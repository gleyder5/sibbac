/**
 *
 */

// var NOTIFICACIONES_RELOAD = 10 * 1000;
$(document).ready(function() {
  // $("#divHeader").load("components/header/header.html");
  // $("#divFooter").load("components/footer/footer.html");
  // $("#menuv").load("components/menu/menu.html");
  // $(".notificaciones").load("components/notificaciones/notificaciones.html");
  // $("#core").load("views/CobrosConsulta/CobrosConsulta.html");

  /* XI316253 Duplicada en el utilssibbac.js */
  /* Eva:Funcion para cambiar texto */
  // jQuery.fn.extend({
  // toggleText : function(a, b) {
  // var that = this;
  // if (that.text() != a && that.text() != b) {
  // that.text(a);
  // } else if (that.text() == a) {
  // that.text(b);
  // } else if (that.text() == b) {
  // that.text(a);
  // }
  // return this;
  // }
  // });
  /* FIN Funcion para cambiar texto */

  // setTimeout(afterPagesLoading, 500);
  /*
   * XI316153 Estilos personalizados para el inputbox de filtrado en los
   * dataTables ... Inicio
   */
  var extensions = {
    "sFilter" : "dataTables_filter custom_filter_class",
    "sLength" : "dataTables_length custom_length_class"
  }

  // Used when bJQueryUI is false
  $.extend($.fn.dataTableExt.oStdClasses, extensions);
  // Used when bJQueryUI is true
  $.extend($.fn.dataTableExt.oJUIClasses, extensions);
  /*
   * XI316153 Estilos personalizados para el inputbox de filtrado en los
   * dataTables ... Fin
   */
});

/*******************************************************************************
 *
 * Cookies
 *
 * ------------------------------------------------------
 *
 * Codigo de ejemplo de Arturo. A ser (conenientemente) modificado y
 * (re)utilizado.
 *
 * SEE: http://www.nczonline.net/blog/2009/05/05/http-cookies-explained/
 *
 ******************************************************************************/

function createCookie(name, value, domain, path, days) {

  // Name?
  if (name == null) {
    return false;
  }

  var theCookie = name;

  // Value
  theCookie += "=" + value;

  // Domain
  if (domain != null) {
    theCookie += "; domain=" + domain;
  }

  // Path
  if (path != null) {
    theCookie += "; path=" + path;
  }

  // Days
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    theCookie += "; expires=" + date.toGMTString();
  }

  // Build the cookie
  document.cookie = theCookie;
}

function readCookie(name) {
  var nameEQ = name + "=";
  var allCookies = document.cookie.split(';');
  if (allCookies != null) {
    var cookie = null;
    var value = null;
    for (var i = 0; i < allCookies.length; i++) {
      cookie = allCookies[i];
      while (cookie.charAt(0) == ' ') {
        cookie = cookie.substring(1, cookie.length);
      }
      if (cookie.indexOf(nameEQ) == 0) {
        value = cookie.substring(nameEQ.length, cookie.length);
        break;
      }
    }
    return value;
  } else {
    return null;
  }
}

function deleteCookie(name) {
  createCookie(name, "", null, null, -1);
}

/*
 * Funcion para acceder a los parametros de una URL. SEE:
 * http://www.sitepoint.com/url-parameters-jquery/
 */
$.urlParam = function(name, url) {
  if (typeof (url) === 'undefined')
    url = window.location.href;
  var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(url);
  return results[1] || 0;
};

$(document)
    .hotkeys(
        's',
        '1',
        '8',
        '8',
        'a',
        'c',
        function(event) {
          window
              .open(
                  "elfinder.html",
                  "fsBrowser",
                  "titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=no,resizable=no,width=1080,height=460");
        });
