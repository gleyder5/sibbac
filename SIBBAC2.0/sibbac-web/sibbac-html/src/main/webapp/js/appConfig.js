sibbac20.config(['growlProvider', 'ngDialogProvider', '$datepickerProvider', '$httpProvider', '$locationProvider',
                 function (growlProvider, ngDialogProvider, $datepickerProvider, $httpProvider, $locationProvider) {

		$httpProvider.interceptors.push('httpRequestInterceptor');

		growlProvider.globalTimeToLive(5000);
        ngDialogProvider.setDefaults({
          className : 'ngdialog-theme-default',
          plain : false,
          showClose : true,
          closeByDocument : true,
          closeByEscape : true,
          appendTo : false,
          preCloseCallback : function () {
            console.log('default pre-close callback');
          }
        });
        $datepickerProvider.setDefaults('es');
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

	}]);
