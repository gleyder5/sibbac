sibbac20.factory('AliasSubcuentaService', [
    '$http', '$rootScope', function ($http, $rootScope) {
        var AliasSubcuentaService = {};
        var service = 'SIBBACServiceSubcuentas';
        var serverUrl = $rootScope.restParams.serverUrl;
        AliasSubcuentaService.getListaSubcuentas = function (successHandler, errorHandler, filter) {
            $http({
                method: 'POST',
                url: serverUrl,
                data: {service: service, action: 'getListaSubcuentas', filters: filter}
            }).success(function (data, status, headers, config) {
                if(data.resultados === undefined){
                    data.resultados = {};
                }
                successHandler(data);
            }).error(errorHandler);
        };
        AliasSubcuentaService.modificarSubcuenta = function(successHandler, errorHandler, filter){
            $http({
                url : serverUrl,
                method : 'POST',
                data : {service : service, action : 'modificarSubcuenta', filters : filter}
            }).success(successHandler).error(errorHandler);
        };
        return AliasSubcuentaService;
    }
]);

