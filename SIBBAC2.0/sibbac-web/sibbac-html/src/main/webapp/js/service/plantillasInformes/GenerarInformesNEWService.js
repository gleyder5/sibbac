'use restrict';
sibbac20.factory('GenerarInformesNEWService', [ '$http', function ($http) {
  var GenerarInformesNEWService = {};
  var url = server.url;
  var service = server.service.plantillasInformes.generarPlantillasInformesNEW;

  GenerarInformesNEWService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  GenerarInformesNEWService.getRequestFilters = function (action, filters) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filters
      }
    });
    return request;
  };

  GenerarInformesNEWService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  GenerarInformesNEWService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  //LLamada al servicio que obtiene los datos del combo de plantillas del filtro
  GenerarInformesNEWService.cargaPlantillas = function (successHandler, errorHandler) {
    var request = GenerarInformesNEWService.getRequest('CargaPlantillas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  GenerarInformesNEWService.cargaIsin = function (successHandler, errorHandler) {
    var request = GenerarInformesNEWService.getRequest('CargaIsin');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  GenerarInformesNEWService.cargaAlias = function (successHandler, errorHandler) {
    var request = GenerarInformesNEWService.getRequest('CargaAlias');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que genera el informe
  GenerarInformesNEWService.generarInforme = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesNEWService.getRequestParamsObject('CargaGeneracionInformes', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  // LLamada al servicio que genera el informe en excel
  GenerarInformesNEWService.generarInformeExcel = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesNEWService.getRequestParamsObject('ExportaInformeXLS', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  // LLamada al servicio que genera el informe en excel para terceros
  GenerarInformesNEWService.generarInformeExcelTerceros = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesNEWService.getRequestParamsObject('ExportaInformeTercerosXLS', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  //LLamada al servicio para obtener la query que ejecuta el informe
  GenerarInformesNEWService.obtenerQueryInforme = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesNEWService.getRequestParamsObject('ObtenerQueryInforme', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  //LLamada al servicio para validar si se debe generar el fichero Excel
  GenerarInformesNEWService.validarGeneracionExcel = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesNEWService.getRequestParamsObject('ValidarGeneracionExcel', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  
  //LLamada al servicio que obtiene los datos del combo de clases
  GenerarInformesNEWService.cargarClases = function (successHandler, errorHandler, paramsObject) {
	  var request = GenerarInformesNEWService.getRequestParamsObject('CargaClases', paramsObject);
	  request.success(successHandler).error(errorHandler);
	  return request;
  };

  // LLamada al servicio que obtiene las plantillas a raiz de una clase seleccionada
  GenerarInformesNEWService.cargaPlantillasFiltro = function (successHandler, errorHandler, paramsObject) {
	  var request = GenerarInformesNEWService.getRequestParamsObject('CargarPlantillasFiltro', paramsObject);
	  request.success(successHandler).error(errorHandler);
	  return request;
  };

  return GenerarInformesNEWService;
} ]);