'use restrict';
sibbac20.factory('PlanificacionInformesService', ['$http', function($http) {
	  var PlanificacionInformesService = {};
	  var url = server.url;
	  var service = server.service.plantillasInformes.gestionPlanificacionInformes;

	  PlanificacionInformesService.getRequest = function (action) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	      }
	    });
	    return request;
	  };

	  PlanificacionInformesService.getRequestParamsObject = function (action, paramsObject) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        paramsObject : paramsObject
	      }
	    });
	    return request;
	  };
	  
	  // LLamada al servicio que obtiene los datos del combo estaticos
	  PlanificacionInformesService.cargaCamposEstaticos = function (successHandler, errorHandler) {
	    var request = PlanificacionInformesService.getRequest('CargaSelectsEstaticos');
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

	  // LLamada al servicio que obtiene los datos del combo estaticos
	  PlanificacionInformesService.cargaComboInformes = function (successHandler, errorHandler) {
	    var request = PlanificacionInformesService.getRequest('CargaComboInforme');
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

	  // LLamada al servicio que obtiene los datos del combo estaticos
	  PlanificacionInformesService.cargaComboPeriodos = function (successHandler, errorHandler) {
	    var request = PlanificacionInformesService.getRequest('CargaComboPeriodo');
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

	  // LLamada al servicio que obtiene los datos del combo estaticos
	  PlanificacionInformesService.cargaComboFicheros = function (successHandler, errorHandler) {
	    var request = PlanificacionInformesService.getRequest('CargaAutorellenableFichero');
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

	  // LLamada al servicio que obtiene los datos del combo estaticos
	  PlanificacionInformesService.cargaComboDestinos = function (successHandler, errorHandler) {
	    var request = PlanificacionInformesService.getRequest('CargaAutorellenableDestino');
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

	  // LLamada al servicio que obtiene los de todas las plantillas de informes filtrados.
	  PlanificacionInformesService.cargaPlanificacionInformes = function (successHandler, errorHandler, paramsObject) {
	    var request = PlanificacionInformesService.getRequestParamsObject('CargaPlanificacionInformes', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

	  // LLamada al servicio que guarda una planificacion de informes.
	  PlanificacionInformesService.guardarPlanificacionInformes = function (successHandler, errorHandler, paramsObject) {
	    var request = PlanificacionInformesService.getRequestParamsObject('GuardarPlanificacionInformes', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

	  // LLamada al servicio que elimina una planificacion de informes.
	  PlanificacionInformesService.eliminarPlanificacion = function (successHandler, errorHandler, paramsObject) {
	    var request = PlanificacionInformesService.getRequestParamsObject('EliminarPlanificacionInformes', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
	  
	  // LLamada al servicio que recupera el detalle de una planificación de informe.
	  PlanificacionInformesService.detallePlanificacionInforme = function (successHandler, errorHandler, paramsObject) {
	    var request = PlanificacionInformesService.getRequestParamsObject('DetallePlanificacionInforme', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
	  
	  //LLamada al servicio para validar si se debe planificar con campo destino vacio.
	  PlanificacionInformesService.validarPlanificacionInforme = function (successHandler, errorHandler, paramsObject) {
	    var request = PlanificacionInformesService.getRequestParamsObject('ValidarPlanificacionInforme', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
	  
	  //LLamada al servicio que obtiene los datos del combo de clases
	  PlanificacionInformesService.cargarClases = function (successHandler, errorHandler, paramsObject) {
		  var request = PlanificacionInformesService.getRequestParamsObject('CargaClases', paramsObject);
		  request.success(successHandler).error(errorHandler);
		  return request;
	  };

	  // LLamada al servicio que obtiene las plantillas a raiz de una clase seleccionada
	  PlanificacionInformesService.cargaPlantillasFiltro = function (successHandler, errorHandler, paramsObject) {
		  var request = PlanificacionInformesService.getRequestParamsObject('CargarPlantillasFiltro', paramsObject);
		  request.success(successHandler).error(errorHandler);
		  return request;
	  };

	  return  PlanificacionInformesService;
}]);