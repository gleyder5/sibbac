sibbac20.factory('CuentaCompensacionService', ['$http', '$rootScope', function($http, $rootScope){
        var CuentaCompensacionService = {};
        var service = server.service.configuracion.cuentadecompensacionService;
        var serverUrl = server.url;
        function getRequest(action, filter){
            return $http({
                url : serverUrl,
                method : 'POST',
                data : {service : service, action : action, filters : filter}
            });
        }
        CuentaCompensacionService.getCuentasDeCompensacion = function(successHandler, errorHandler, filter){
            getRequest("getCuentasDeCompensacion", filter).success(successHandler).error(errorHandler);
        };
        CuentaCompensacionService.getTiposCuentaCompensacion = function(successHandler, errorHandler, filter){
            getRequest("getTiposCuentaCompensacion", filter).success(successHandler).error(errorHandler);
        };
        CuentaCompensacionService.getListaCompensadores = function(successHandler, errorHandler, filter){
            getRequest("getListaCompensadores", filter).success(successHandler, errorHandler, filter);
        };
        CuentaCompensacionService.getCuentasMercado = function(successHandler, errorHandler, filter){
            getRequest("getCuentasMercado", filter).success(successHandler, errorHandler, filter);
        };
        CuentaCompensacionService.getCuentaLiquidacion = function(successHandler, errorHandler, filter){
            getRequest("getCuentaLiquidacion", filter).success(successHandler, errorHandler, filter);
        }
        return CuentaCompensacionService;
}]);


