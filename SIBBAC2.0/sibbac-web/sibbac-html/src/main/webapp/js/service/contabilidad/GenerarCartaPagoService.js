sibbac20.factory('GenerarCartaPagoService', [ '$http', '$rootScope', function ($http, $rootScope) {

  var GenerarCartaPagoService = {};
  var serverUrl = $rootScope.restParams.serverUrl;

  GenerarCartaPagoService.initPayLetter = function (successHandler, errorHandler) {
    var request = $http({
      url : serverUrl,
      method : 'POST',
      data : {
        service : server.service.contabilidad.apunteContable,
        action : 'initPayLetter'
      }
    });
    if (successHandler !== undefined && errorHandler !== undefined) {
      request.success(successHandler).error(errorHandler);
    }
    return request;
  }; // GenerarCartaPagoService.initPayLetter

  GenerarCartaPagoService.findPayLetter = function (successHandler, errorHandler, filter) {
    var request = $http({
      url : serverUrl,
      method : 'POST',
      data : {
        service : server.service.contabilidad.apunteContable,
        action : 'findPayLetter',
        filters : filter
      }
    });

    if (successHandler !== undefined && errorHandler !== undefined) {
      request.success(successHandler).error(errorHandler);
    }
    return request;
  }; // GenerarCartaPagoService.findPayLetter

  return GenerarCartaPagoService;
} ]);
