'use restrict';
sibbac20.factory('CargaFicheroSwiftService', ['$http', function ($http) {
	 var CargaFicheroSwiftService = {};
     var url = server.url;
     var service = server.service.mantenimientodatoscliente.swiftService;

     CargaFicheroSwiftService.getRequest = function (action, filter) {
         var request = $http({
             method: 'POST',
             url: url,
             data: {action: action, service: service, filters: filter}
         });
         return request;
     };

     CargaFicheroSwiftService.getRequestParams = function (action, params) {
	        var request = $http({
	            method: 'POST',
	            url: url,
	            data: {action: action, service: service, params: params}
	        });
	        return request;
 	};



 	CargaFicheroSwiftService.importarFichero = function (successHandler, errorHandler, filters) {
            var request = CargaFicheroSwiftService.getRequest('importarFichero', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

    	return CargaFicheroSwiftService;
}]);

