(function(angular, sibbac20, undefined){
  
  "strict mode";
  
  sibbac20.factory("ImputacionS3Service", ["$resource", "$http", function($resource, $http){
	  var baseUrl = "/sibbac20back/rest/imputacions3";
	  var getConfigResource = $resource(baseUrl + "/get/config");
	  var updateConfigResource = $resource(baseUrl + "/update/config");
	  var imputacionS3PtiResource = $resource(baseUrl + "/pti");
	  var imputacionPtiMabResource = $resource(baseUrl + "/pti/mab");
	  var imputacionS3EuroCcpResource = $resource(baseUrl + "/euroccp");
	  
	  
	  return {getConfig: function() {
	            var data = getConfigResource.get();
	            console.log("getConfig = " + data);
	            return data;
	          },
	          updateConfig: function(config, success, fail){
	        	  updateConfigResource.save(config, success, fail);
	          },
	          imputacionPti: function(success, fail){
	        	  imputacionS3PtiResource.save(success, fail);
	          },
	          imputacionEuroCcp: function(success, fail){
	        	  imputacionS3EuroCcpResource.save(success, fail);
	          }
	          ,
	          imputacionPtiMab: function(success, fail){
	        	  imputacionPtiMabResource.save(success, fail);
	          }
	  }
  }]);
})(angular, sibbac20)