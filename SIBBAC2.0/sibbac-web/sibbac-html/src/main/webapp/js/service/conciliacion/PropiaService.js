'use strict';
sibbac20.factory('PropiaService', ['$http', function($http){
        var PropiaService = {};
        var serverUrl = server.url;
        var fService = server.service.conciliacion.propia;

        PropiaService.getAnotacioneseccByTipoCuenta = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'getAnotacioneseccByTipoCuenta', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
       };

        return PropiaService;
}]);
