'use restrict';
sibbac20.factory('GestionPerfilesService', [ '$http', function ($http) {
  var GestionPerfilesService = {};
  var url = server.url;
  var service = server.service.gestionAccesos.gestionPerfiles;

  GestionPerfilesService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  GestionPerfilesService.getRequestFilters = function (action, filters) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filters
      }
    });
    return request;
  };

  GestionPerfilesService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  GestionPerfilesService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que obtiene los permisos de acceso
  GestionPerfilesService.cargaPaginas = function (successHandler, errorHandler) {
    var request = GestionPerfilesService.getRequest('RecuperarPaginas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los perfiles
  GestionPerfilesService.cargaPerfiles = function (successHandler, errorHandler) {
    var request = GestionPerfilesService.getRequest('RecuperarPerfiles');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los usuarios
  GestionPerfilesService.cargaUsuarios = function (successHandler, errorHandler) {
    var request = GestionPerfilesService.getRequest('RecuperarUsuarios');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene la lista de acciones
  GestionPerfilesService.cargaAcciones = function (successHandler, errorHandler) {
    var request = GestionPerfilesService.getRequest('RecuperarAcciones');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene la lista de acciones
  GestionPerfilesService.cargaPaginasAcciones = function (successHandler, errorHandler) {
    var request = GestionPerfilesService.getRequest('RecuperarPaginasAcciones');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los permisos de acceso filtrado por los datos insertados por el usuario
  GestionPerfilesService.cargarPerfilesFiltro = function (successHandler, errorHandler, paramsObject) {
    var request = GestionPerfilesService.getRequestParamsObject('RecuperarPerfilesFiltro', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que modifica los datos del perfil estableciendo los pasados en el filtros
  GestionPerfilesService.modificarPerfil = function (successHandler, errorHandler, paramsObject) {
    var request = GestionPerfilesService.getRequestParamsObject('ModificarPerfil', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que crea un perfil dados los datos pasados en el filtros
  GestionPerfilesService.crearPerfil = function (successHandler, errorHandler, paramsObject) {
    var request = GestionPerfilesService.getRequestParamsObject('crearPerfil', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que elimina los perfiles seleccionados
  GestionPerfilesService.eliminarPerfiles = function (successHandler, errorHandler, paramsObject) {
    var request = GestionPerfilesService.getRequestParamsObject('eliminarPerfiles', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return GestionPerfilesService;
} ]);
