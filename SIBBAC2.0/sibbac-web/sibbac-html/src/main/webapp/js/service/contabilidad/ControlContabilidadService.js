'use restrict';
sibbac20.factory('ControlContabilidadService', [ '$http', function ($http) {
	var ControlContabilidadService = {};
	var url = server.url;
	var service = server.service.contabilidad.controlContabilidad;

	ControlContabilidadService.getRequest = function (action) {
		var request = $http({
			method : 'POST',
			url : url,
			data : {
				action : action,
				service : service,
			}
		});
		return request;
	};

	ControlContabilidadService.getRequestParamsObject = function (action, paramsObject) {
		var request = $http({
			method : 'POST',
			url : url,
			data : {
				action : action,
				service : service,
				paramsObject : paramsObject
			}
		});
		return request;
	};

	// LLamada al servicio que obtiene los datos de los apuntes pendientes.
	ControlContabilidadService.consultarApuntesPndts = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('consultarApuntesPndts',paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};

	// LLamada al servicio que aprobara apuntes
	ControlContabilidadService.aprobarApuntes = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('aprobarApuntes', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};

	// LLamada al servicio que rechazará apuntes
	ControlContabilidadService.rechazarApuntes = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('rechazarApuntes', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};
	
	// LLamada al servicio que modificará el tolerance
	ControlContabilidadService.modificarTolerance = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('modificarTolerance', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};
	
	// LLamada al servicio que recuperará los apuntes auditados
	ControlContabilidadService.consultarAuditoriaApuntes = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('consultarAuditoriaApuntes', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};
	
	// LLamada al servicio que recuperará los tolerances auditados
	ControlContabilidadService.consultarAuditoriaTolerance = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('consultarAuditoriaTolerance', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};
	
	// LLamada al servicio que recuperará las partidas casadas correspondientes a apuntes auditados
	ControlContabilidadService.consultarPartidasCasadas = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('consultarPartidasCasadas', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};
	
	// LLamada al servicio que recuperará los Our correspondientes a apuntes auditados
	ControlContabilidadService.consultarOur = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('consultarOur', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};
	
	// LLamada al servicio que exportará a excel las aprtidas casadas y las our de un apunte auditado
	ControlContabilidadService.exportarXLS = function (successHandler, errorHandler, paramsObject) {
		var request = ControlContabilidadService.getRequestParamsObject('exportarXLS', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};
	

	return ControlContabilidadService;
} ]);