(function(sibbac20){
	"strict mode";
	
	sibbac20.factory("TraspasosEccSubqueryService", ["$resource",function($resource) {
    	var baseUrl, subqueryRes;
    	baseUrl = "/sibbac20back/rest/traspasos/ecc/subquery";
    	subqueryRes = $resource(baseUrl + "/:queryName/:detail", 
    			{queryName: "@queryName", detail:"@detail"});
    	return {
			baseUrl: baseUrl,
			queries: function() { return []; },
			dynamicFilters: function(name) { return []; },
			columns: function(name, success) {
			    var res = subqueryRes.query({queryName: name, detail: "columns"}, function(){
			    	success(res);
			    });
			},
			executeQuery: function(queryName, filter, success, fail) {
				var data;
				filter.queryName = queryName;
				data = subqueryRes.query(filter, function() {
					if(data.length > 0 && data[0].error !== undefined) {
						fail(data[0].error);
						return;
					}
					success(data);
				}, fail);
			}
		};
	}])
})(sibbac20);