sibbac20.factory('ConciliacionBancariaService',['$http',function($http) {
							
	  var ConciliacionBancariaService = {};
	  var url = server.url;
	  var service = server.service.contabilidad.conciliacionBancaria.pantalla;
							
	  ConciliacionBancariaService.getRequest = function (action) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	      }
	    });
	    return request;
	  };
	  
	  ConciliacionBancariaService.getRequestFilters = function (action, filter) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        filters : filter
	      }
	    });
	    return request;
	  };

	  ConciliacionBancariaService.getRequestParamsObject = function (action, paramsObject) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        paramsObject : paramsObject
	      }
	    });
	    return request;
	  };

	  ConciliacionBancariaService.busqueda = function (successHandler, errorHandler, paramsObject) {
	    var request = ConciliacionBancariaService.getRequestParamsObject('BusquedaTLM', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
	  
	  ConciliacionBancariaService.getInformacionTLM = function (successHandler, errorHandler) {
	    var request = ConciliacionBancariaService.getRequest('InformacionTLM');
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
	  
	  ConciliacionBancariaService.cargaFichero = function (successHandler, errorHandler, filter) {
	    var request = ConciliacionBancariaService.getRequestFilters('CargarFicheroTLM', filter);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
							
	  return ConciliacionBancariaService;
							
}]);
