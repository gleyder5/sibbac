'use strict';
sibbac20.factory('TitularesRoutingService', [ '$http', '$resource', function ($http, $resource) {
  var TitularesRoutingService = {};
  var serverUrl = server.url;
  var service = server.service.clearing.titularesRouting;
  var importResource = $resource("/sibbac20back/rest/final-holders/:accion/:detalle", 
		  {accion: "@accion", detalle: "@detalle"});

  function getRequest (action, filtro) {
    var request = $http({
      url : serverUrl,
      method : 'POST',
      data : {
        service : service,
        action : action,
        filters : filtro
      }
    });
    return request;
  }

  TitularesRoutingService.getRequestTodos = function (action, filter, params) {
    var request = $http({
      method : 'POST',
      url : serverUrl,
      data : {
        action : action,
        service : service,
        filters : filter,
        params : params
      }
    });
    return request;
  };
  
  TitularesRoutingService.getTitularesDataTable = function(successHandler, errorHandler, filter) {
	  var url = serverUrl + '/' + service + '/datatable';
	  var request = $http({method: 'GET', url: url, params: filter});
	  request.success(successHandler).error(errorHandler);
  }

  TitularesRoutingService.deleteTitularesList = function (successHandler, errorHandler, filter, params) {
    var request = TitularesRoutingService.getRequestTodos('deleteTitularesList', filter, params);
    if (successHandler !== undefined && errorHandler !== undefined) {
      request.success(successHandler).error(errorHandler);
    }
    return request;
  };

  TitularesRoutingService.deleteTitular = function (successHandler, errorHandler, params) {
    var request = TitularesRoutingService.getRequestTodos('deleteTitular', {}, params);
    if (successHandler !== undefined && errorHandler !== undefined) {
      request.success(successHandler).error(errorHandler);
    }
    return request;
  };

  TitularesRoutingService.createTitularesEspeciales = function (successHandler, errorHandler, filter) {
    var request = getRequest('createTitularesEspeciales', filter);
    if (successHandler !== undefined && errorHandler !== undefined) {
      request.success(successHandler).error(errorHandler);
    }
    return request;
  };
  
  TitularesRoutingService.loadCombo = function(accion, nombreColeccion, receiveData, failCombo, transform) {
	  if(!transform)
		  transform = function(item) {item.descripcion =  item.codigo + " - " + item.descripcion;};
	  var request = getRequest(accion, {});
	  request.success(function(json) {
		if (json.resultados == null) {
			failCombo(json.error, accion);
		} else {
			var item, datos = json.resultados[nombreColeccion];
			for ( var k in datos) {
				item = datos[k];
				transform(item);
			}
			receiveData(datos);
		} 
	  })
	  .error(function() {failCombo("Error de servidor", accion);})
  };
  
  TitularesRoutingService.consultaFinalHolders = function(filter, success, fail) {
	  var items = importResource.query(filter, function(){ success(items); }, fail);
  };
  
  TitularesRoutingService.agregaTitularesImportados = function(importacion, success, fail) {
	  importResource.save({accion : "importacion", detalle: "agrega"}, importacion, success, fail);
  };
  
  TitularesRoutingService.sustituyeTitularesImportados = function(importacion, success, fail) {
	  importResource.save({accion : "importacion", detalle: "sustituye"}, importacion, success, fail);
  };

  return TitularesRoutingService;
} ]);
