sibbac20.factory('CuentasECCService', ['$http', '$rootScope', function ($http, $rootScope) {
        var CuentasECCService = {};
        var url = $rootScope.restParams.serverUrl;
        var service = server.service.conciliacion.cuentasECC;
        CuentasECCService.getMovimientosECC = function (params, successHandler, errorHandler) {
            $http({
                method: 'POST',
                data: {action: 'getMovimientosECC', service: service, filters: params},
                url: url
            }).success(function (data, status, headers, config) {

                if (data !== null && data.error !== null) {
                    errorHandler(data, status, headers, config);
                } else if (data !== null && data.resultados !== null && data.resultados !== null &&
                        data.resultados.data !== null) {
                    var datosT = data.resultados.data;
                    successHandler( datosT);
                }
            }).error(errorHandler);
        };
        CuentasECCService.getCamarasCompensacion = function (successHandler, errorHandler) {
          $http({
              method: 'POST',
              data: {action: 'getCamarasCompensacion', service: service, filters: {}},
              url: url
          }).success(function (data, status, headers, config) {

              if (data !== null && data.error !== null) {
                  errorHandler(data, status, headers, config);
              } else if (data !== null && data.resultados !== null && data.resultados !== null &&
                      data.resultados.data !== null) {
                  var datosT = data.resultados.data;
                  successHandler( datosT);
              }
          }).error(errorHandler);
      };
        return CuentasECCService;
    }]);
