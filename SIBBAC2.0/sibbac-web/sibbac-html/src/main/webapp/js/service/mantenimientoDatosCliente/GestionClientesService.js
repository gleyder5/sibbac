'use restrict';
sibbac20.factory('GestionClientesService', [ '$http', function ($http) {
  var GestionClientesService = {};
  var url = server.url;
  var service = server.service.mantenimientodatoscliente.gestionClientes;


  GestionClientesService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  GestionClientesService.getRequestFilters = function (action, filters) {
	var o = angular.copy(filters);
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : o
      }
    });
    return request;
  };

  GestionClientesService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  GestionClientesService.getRequestParamsObject = function (action, paramsObject) {
	var o = angular.copy(paramsObject);
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : o
      }
    });
    return request;
  };

  // LLamada al servicio que busca los usuarios que coincidan con los filtros
  GestionClientesService.busquedaClientes = function (successHandler, errorHandler, filters) {
    var request = GestionClientesService.getRequestFilters('BusquedaClientes', filters);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que busca los usuarios que coincidan con los filtros
  GestionClientesService.busquedaContactos = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestFilters('BusquedaContactos', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que busca los usuarios que coincidan con los filtros
  GestionClientesService.busquedaMifids = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestFilters('BusquedaMifids', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que borra los usuarios que coincidan con los filtros
  GestionClientesService.eliminarClientes = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestParamsObject('EliminarCliente', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que borra los usuarios que coincidan con los filtros
  GestionClientesService.eliminarContactos = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestParamsObject('EliminarContacto', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que borra los usuarios que coincidan con los filtros
  GestionClientesService.eliminarMIFID = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestParamsObject('EliminarMifid', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que modifica los datos del usuario estableciendo los pasados en el filtros
  GestionClientesService.modificarCliente = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestParamsObject('ModificarCliente', { "datosCliente" : paramsObject });
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que crea un usuario dados los datos pasados en el filtros
  GestionClientesService.crearCliente = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestParamsObject('CrearCliente', { "datosCliente" : paramsObject });
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que crea un usuario dados los datos pasados en el filtros
  GestionClientesService.recuperarCliente = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestParamsObject('RecuperarCliente', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que crea un usuario dados los datos pasados en el filtros
  GestionClientesService.verHistoricoCliente = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestParamsObject('VerHistoricoCliente', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los usuarios
  GestionClientesService.cargaClientes = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestFilters('CargarClientes', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que modifica los datos del usuario estableciendo los pasados en el filtros
  GestionClientesService.modificarContactoCliente = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestParamsObject('ModificarContacto', {"datosContacto":paramsObject});
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que genera el informe en excel
  GestionClientesService.exportarClientesExcel = function (successHandler, errorHandler, paramsObject) {
    var request = GestionClientesService.getRequestFilters('ExportaClientesXLS', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return GestionClientesService;

} ]);
