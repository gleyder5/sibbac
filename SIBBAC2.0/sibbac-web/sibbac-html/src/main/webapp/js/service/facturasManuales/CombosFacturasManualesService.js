'use restrict';
sibbac20.factory('CombosFacturasManualesService', [ '$http', function ($http) {
  var CombosFacturasManualesService = {};
  var url = server.url;
  var service = server.service.facturasmanuales.combosFacturasManuales;

  CombosFacturasManualesService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };


  // LLamada al servicio que obtiene los datos del combo de campos detalle
  CombosFacturasManualesService.consultarCausasExencion = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarCausasExencion');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos del combo de plantillas del filtro
  CombosFacturasManualesService.consultarClaveRegimen = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarClaveRegimen');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  CombosFacturasManualesService.consultarCodigosImpuestos = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarCodigosImpuestos');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  CombosFacturasManualesService.consultarTiposFactura = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarTiposFactura');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CombosFacturasManualesService.consultarMonedas = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarMonedas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CombosFacturasManualesService.consultarEntregaBien = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarEntregaBien');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CombosFacturasManualesService.consultarAlias = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarAlias');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CombosFacturasManualesService.consultarEstados = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarEstados');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CombosFacturasManualesService.consultarSujetas = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarSujetas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CombosFacturasManualesService.consultarExentas = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarExentas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CombosFacturasManualesService.consultarFacturasRectificadas = function (successHandler, errorHandler) {
    var request = CombosFacturasManualesService.getRequest('consultarFacturasRectificadas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return CombosFacturasManualesService;

} ]);
