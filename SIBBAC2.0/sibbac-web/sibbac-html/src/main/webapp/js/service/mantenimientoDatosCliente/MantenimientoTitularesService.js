'use restrict';
sibbac20.factory('MantenimientoTitularesService', [ '$http', function ($http) {
  var MantenimientoTitularesService = {};
  var url = server.url;
  var service = server.service.mantenimientodatoscliente.titularesService;

  MantenimientoTitularesService.getRequest = function (action, filter) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filter
      }
    });
    return request;
  };

  MantenimientoTitularesService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  // LLamada al servicio que obtiene los datos estatios que se cargan en pantalla.
  MantenimientoTitularesService.Inicializa = function (successHandler, errorHandler, filter) {
    var request = MantenimientoTitularesService.getRequest('init', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los distintos nombres que hay en base de datos.
  MantenimientoTitularesService.CargaNombres = function (successHandler, errorHandler, filter) {
    var request = MantenimientoTitularesService.getRequest('CargaNombres', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los distintos apellidos que hay en base de datos.
  MantenimientoTitularesService.CargaApellidos = function (successHandler, errorHandler, filter) {
    var request = MantenimientoTitularesService.getRequest('CargaApellidos', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los distintos Documentos que hay en base de datos.
  MantenimientoTitularesService.CargaDocumentos = function (successHandler, errorHandler, filter) {
    var request = MantenimientoTitularesService.getRequest('CargaDocumentos', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada a la consulta de los datos de los titulares que cumplen el filtro establecido.
  MantenimientoTitularesService.ConsultaTitularesFiltro = function (successHandler, errorHandler, filter) {
    var request = MantenimientoTitularesService.getRequest('ConsultaTitularesFiltro', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // llamada al servicio que valida los datos insertados para un titular
  MantenimientoTitularesService.ValidaTitular = function (successHandler, errorHandler, filter) {
    var request = MantenimientoTitularesService.getRequest('ValidaTitular', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // llamada al servicio que inserta los datos de un nuevo titular
  MantenimientoTitularesService.CrearTitular = function (successHandler, errorHandler, filter) {
    var request = MantenimientoTitularesService.getRequest('CrearTitular', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // llamada al servicio que modifilca los datos de un titular
  MantenimientoTitularesService.ModificarTitular = function (successHandler, errorHandler, filter) {
    var request = MantenimientoTitularesService.getRequest('ModificarTitular', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  MantenimientoTitularesService.EliminarTitular = function (successHandler, errorHandler, params) {
    var request = MantenimientoTitularesService.getRequestParams('EliminarTitulares', params);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  MantenimientoTitularesService.importarExcelCargaMasiva = function (successHandler, errorHandler, filters) {
    var request = MantenimientoTitularesService.getRequest('ImportarExcelCargaMasiva', filters);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  MantenimientoTitularesService.ComprobarEliminarTitular = function (successHandler, errorHandler, params) {
    var request = MantenimientoTitularesService.getRequestParams('ComprobarEliminarTitular', params);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return MantenimientoTitularesService;
} ]);
