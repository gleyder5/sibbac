'use strict';
sibbac20.factory('MovimientoVirtualService', ['$http', function ($http) {
        var MovimientoVirtualService = {};
        var url = server.url;
        var service = server.service.movimientoVirtual;
        MovimientoVirtualService.getMovimientosEspejo = function (params, successHandler, errorHandler) {
            $http({
                url: url,
                data: params,
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null &&
                        data.resultados !== null &&
                        data.resultados !== null &&
                        data.resultados.result_cuentas_virtuales !== null) {
                    datos = data.resultados.result_cuentas_virtuales;
                }
                successHandler(datos);
            }).error(errorHandler);
        };

        MovimientoVirtualService.getFechaPreviaLiquidacion = function (successHandler, errorHandler) {
            $http({
                url: url,
                data: {action: 'getFechaPreviaLiquidacion', service: service},
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null &&
                        data.resultados !== null &&
                        data.resultados !== null &&
                        data.resultados.result_fecha_previa_laboral !== null) {
                    datos = data.resultados.result_fecha_previa_laboral;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        MovimientoVirtualService.getFechaActualizacionMovimientoAlias = function (successHandler, errorHandler) {
            $http({
                url: url,
                data: {action: 'getUpdateLastTime', service: service},
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null &&
                        data.resultados !== null &&
                        data.resultados !== null &&
                        data.resultados.result_last_time_update !== null) {
                    datos = data.resultados.result_last_time_update;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        return MovimientoVirtualService;
    }]);
