'use restrict';
sibbac20.factory('RectificativasManualesService', [ '$http', function ($http) {
  var RectificativasManualesService = {};
  var url = server.url;
  var service = server.service.facturasmanuales.rectificativasManuales;

  RectificativasManualesService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  RectificativasManualesService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que obtiene los de todas las plantillas de informes filtrados.
  RectificativasManualesService.consultarRectificativasManuales = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('consultarRectificativasManuales', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };


  // LLamada al servicio que obtiene los datos del combo de campos detalle
  RectificativasManualesService.crearRectificativaManual = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('crearRectificativaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // Llamada al servicio
  RectificativasManualesService.modificarRectificativaManual = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('modificarRectificativaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que eliminará un registro fisico de factura manual
  RectificativasManualesService.borrarRectificativasManuales = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('borrarRectificativasManuales', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene un detalle de una factura
  RectificativasManualesService.detalleRectificativaManual = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('detalleRectificativaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  RectificativasManualesService.consultarCliente = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('consultarCliente', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  RectificativasManualesService.contabilizar = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('contabilizar', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  RectificativasManualesService.modificarContabilizar = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('modificarContabilizar', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  RectificativasManualesService.crearContabilizar = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('crearContabilizar', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  RectificativasManualesService.anularRectificativasManuales = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('anularRectificativasManuales', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  RectificativasManualesService.consultarProximoNumeroFacturaManual = function (successHandler, errorHandler, paramsObject) {
    var request = RectificativasManualesService.getRequestParamsObject('consultarProximoNumeroFacturaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  RectificativasManualesService.consultarProximoNumeroRectificativa = function (successHandler, errorHandler, paramsObject) {
	var request = RectificativasManualesService.getRequestParamsObject('consultarProximoNumeroRectificativa', paramsObject);
	request.success(successHandler).error(errorHandler);
	return request;
  };
  
  RectificativasManualesService.consultarProximoNumeroRectificativaGrupoIva = function (successHandler, errorHandler, paramsObject) {
	    var request = RectificativasManualesService.getRequestParamsObject('consultarProximoNumeroFacturaRectificativaGrupoIva', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

  // Llamada al servicio que exportará a PDF la factura seleccionada
  RectificativasManualesService.exportarPdf = function (successHandler, errorHandler, paramsObject) {
	var request = RectificativasManualesService.getRequestParamsObject('exportarPdf', paramsObject);
	request.success(successHandler).error(errorHandler);
	return request;
  };


  return RectificativasManualesService;

} ]);
