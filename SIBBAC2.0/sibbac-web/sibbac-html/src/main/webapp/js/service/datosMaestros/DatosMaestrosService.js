sibbac20.factory('DatosMaestrosService', ['$http', function ($http) {
        var DatosMaestrosService = {};
        var url = server.url;
        var service = server.service.datosMaestros;
        function getRequest(action, filter) {
            var request = $http({
                url: url,
                method: 'POST',
                data: {service: service, action: action, filters: filter}
            });
            return request;
        }
        DatosMaestrosService.getCodigosOperacion = function (successHandler, errorHandler, filter) {
            var request = getRequest('getCodigosOperacion', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        DatosMaestrosService.getGruposValores = function (successHandler, errorHandler, filter) {
            var request = getRequest('getCodigosOperacion', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        DatosMaestrosService.getConceptosMovimientoEfectivo = function (successHandler, errorHandler, filter) {
            var request = getRequest('getConceptosMovimientoEfectivo', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        DatosMaestrosService.getDepositariosValores = function (successHandler, errorHandler, filter) {
            var request = getRequest('getDepositariosValores', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        DatosMaestrosService.getModosMatGarantias = function (successHandler, errorHandler, filter) {
            var request = getRequest('getModosMatGarantias', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        DatosMaestrosService.getTiposActivo = function (successHandler, errorHandler, filter) {
            var request = getRequest('getTiposActivo', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        DatosMaestrosService.getTiposGarantiaExigida = function (successHandler, errorHandler, filter) {
            var request = getRequest('getTiposGarantiaExigida', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        DatosMaestrosService.getTiposPersona = function (successHandler, errorHandler, filter) {
            var request = getRequest('getTiposPersona', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        DatosMaestrosService.getTiposProducto = function (successHandler, errorHandler, filter) {
            var request = getRequest('getTiposProducto', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        return DatosMaestrosService;
    }])

