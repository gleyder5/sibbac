'use restrict';
sibbac20.factory('ConsultarRespuestaErroresGCService', [ '$http', function ($http) {
	var ConsultarRespuestaErroresGCService = {};
	var url = server.url;
	var service = server.service.facturas.consultarRespuestaErroresGC;

	ConsultarRespuestaErroresGCService.getRequestFilters = function (action, filters) {
		var o = angular.copy(filters);
		var request = $http({
			method : 'POST',
			url : url,
			data : {
				action : action,
				service : service,
				filters : o
			}
		});
		return request;
	};

	ConsultarRespuestaErroresGCService.getRequest = function (action) {
		var request = $http({
			method : 'POST',
			url : url,
			data : {
				action : action,
				service : service,
			}
		});
		return request;
	};

	ConsultarRespuestaErroresGCService.getRequestParamsObject = function (action, paramsObject) {
		var o = angular.copy(paramsObject);
		var request = $http({
			method : 'POST',
			url : url,
			data : {
				action : action,
				service : service,
				paramsObject : o
			}
		});
		return request;
	};

	// LLamada al servicio que obtiene los datos del combo de estados de fichero del filtro
	ConsultarRespuestaErroresGCService.cargaCatalogos = function (successHandler, errorHandler) {
		var request = ConsultarRespuestaErroresGCService.getRequest('CargaSelects');
		request.success(successHandler).error(errorHandler);
		return request;
	};

	// LLamada al servicio que obtiene los datos de la busqueda de respuesta de errores GC
	ConsultarRespuestaErroresGCService.buscarRespErroresGC = function (successHandler, errorHandler, filters) {
		var request = ConsultarRespuestaErroresGCService.getRequestFilters('BuscarRespuestaErroresGC', filters);
		request.success(successHandler).error(errorHandler);
		return request;
	};

	// LLamada al servicio que recobra los errores producidos en el fichero de respuesta seleccionado
	ConsultarRespuestaErroresGCService.verErroresFichero = function (successHandler, errorHandler, paramsObject) {
		var request = ConsultarRespuestaErroresGCService.getRequestParamsObject('VerErroresDelFichero', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};

	// LLamada al servicio que descarga el fichero
	ConsultarRespuestaErroresGCService.descargarFichero = function (successHandler, errorHandler, paramsObject) {
	    var request = ConsultarRespuestaErroresGCService.getRequestParamsObject('DescargarFichero', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	};

	return ConsultarRespuestaErroresGCService;
} ]);
