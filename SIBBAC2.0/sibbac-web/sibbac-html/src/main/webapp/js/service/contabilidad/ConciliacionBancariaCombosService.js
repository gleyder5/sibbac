sibbac20.factory('ConciliacionBancariaCombosService', [
		'$http',
		function($http) {
			
			var ConciliacionBancariaCombosService = {};
			var service = server.service.contabilidad.conciliacionBancaria.combos;
			var url = server.url;
			
			ConciliacionBancariaCombosService.getRequest = function (action) {
				var request = $http({
					method : 'POST',
					url : url,
					data : {
						action : action,
						service : service
					}
				});
				return request;
			};
			
			ConciliacionBancariaCombosService.getRequestFilters = function (action, filter) {
			    var request = $http({
			      method : 'POST',
			      url : url,
			      data : {
			        action : action,
			        service : service,
			        filters : filter
			      }
			    });
			    return request;
			};

			ConciliacionBancariaCombosService.getRequestParamsObject = function (action, paramsObject) {
				var request = $http({
					method : 'POST',
					url : url,
					data : {
						action : action,
						service : service,
						paramsObject : paramsObject
					}
				});
				return request;
			};
			
			// LLamada al servicio que recuperará los datos del combo códigos de plantilla
			ConciliacionBancariaCombosService.catalogoAntiguedad = function (successHandler, errorHandler) {
				var request = ConciliacionBancariaCombosService.getRequest('antiguedad');
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará los datos del combo códigos de plantilla
			ConciliacionBancariaCombosService.codigosPlantillas = function (successHandler, errorHandler, paramsObject) {
				var request = ConciliacionBancariaCombosService.getRequestParamsObject('consultarCodigosPlantillas', paramsObject);
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará los usuarios
			ConciliacionBancariaCombosService.consultarUsuarios = function (successHandler, errorHandler, paramsObject) {
				var request = ConciliacionBancariaCombosService.getRequestParamsObject('consultarUsuarios', paramsObject);
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará las cuentas contables
			ConciliacionBancariaCombosService.consultarCuentasContables = function (successHandler, errorHandler) {
				var request = ConciliacionBancariaCombosService.getRequestParamsObject('consultarCuentasContables');
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará las plantillas their de contabilizacion
			ConciliacionBancariaCombosService.consultarPlantillasTheir = function (successHandler, errorHandler) {
				var request = ConciliacionBancariaCombosService.getRequestParamsObject('consultarPlantillasTheir');
				request.success(successHandler).error(errorHandler);
				return request;
			};			
			
			// LLamada al servicio que recuperará los datos del combo códigos de tipos de movimiento
			ConciliacionBancariaCombosService.consultarTiposDeMovimiento = function (successHandler, errorHandler) {
				var request = ConciliacionBancariaCombosService.getRequest('consultarTiposDeMovimiento');
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará los datos del combo códigos de tipos de movimiento
			ConciliacionBancariaCombosService.consultarAuxiliares = function (successHandler, errorHandler) {
				var request = ConciliacionBancariaCombosService.getRequest('consultarAuxiliares');
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará los datos del combo códigos de conceptos movimiento
			ConciliacionBancariaCombosService.consultarConceptoMovimiento = function (successHandler, errorHandler) {
				var request = ConciliacionBancariaCombosService.getRequest('consultarConceptoMovimiento');
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará los datos del combo códigos de numeros de documento
			ConciliacionBancariaCombosService.consultarNumerosDocumento = function (successHandler, errorHandler) {
				var request = ConciliacionBancariaCombosService.getRequest('consultarNumerosDocumento');
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará los datos del combo comentarios
			ConciliacionBancariaCombosService.consultarComentarios = function (successHandler, errorHandler) {
				var request = ConciliacionBancariaCombosService.getRequest('consultarComentarios');
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará los conceptos
			ConciliacionBancariaCombosService.conceptos = function (successHandler, errorHandler, paramsObject) {
				var request = ConciliacionBancariaCombosService.getRequestParamsObject('consultarConceptos', paramsObject);
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará los sentidos
			ConciliacionBancariaCombosService.sentidos = function (successHandler, errorHandler, paramsObject) {
				var request = ConciliacionBancariaCombosService.getRequestParamsObject('consultarSentidos', paramsObject);
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			// LLamada al servicio que recuperará las antiguedades
			ConciliacionBancariaCombosService.antiguedades = function (successHandler, errorHandler, paramsObject) {
				var request = ConciliacionBancariaCombosService.getRequestParamsObject('consultarAntiguedades', paramsObject);
				request.success(successHandler).error(errorHandler);
				return request;
			};
			
			return ConciliacionBancariaCombosService;
			
		}
]);
