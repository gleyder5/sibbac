sibbac20.factory('ConciliacionTipoMovimientoService', ['$http', function($http){
        var ConciliacionTipoMovimientoService = {};
        var url = server.url;
        var service = server.service.conciliacion.tipoMovimiento;
        ConciliacionTipoMovimientoService.getTipoMovimiento = function(successHandler, errorHandler, filter){
            var request = $http({
                url : url,
                method : 'POST',
                data : {service : service, action : 'getTipoMovimiento', filters : filter}
            });
            request.success(successHandler).error(errorHandler);
            return request;
        };
        return ConciliacionTipoMovimientoService;
}]);

