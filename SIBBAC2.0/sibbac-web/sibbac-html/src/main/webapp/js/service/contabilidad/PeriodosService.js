'use restrict';
sibbac20.factory('PeriodosService',['$http',function($http) {
							
    var PeriodosService = {};
    var url = server.url;
    var service = server.service.contabilidad.periodos;
							
    PeriodosService.getRequest = function (action) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	      }
	    });
	    return request;
	};
	  
	PeriodosService.getRequestFilters = function (action, filter) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        filters : filter
	      }
	    });
	    return request;
	};

	PeriodosService.getRequestParamsObject = function (action, paramsObject) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        paramsObject : paramsObject
	      }
	    });
	    return request;
	};

		// LLamada al servicio que recuperará los Our correspondientes a apuntes auditados
	PeriodosService.consultarPeriodos = function (successHandler, errorHandler, paramsObject) {
		var request = PeriodosService.getRequestParamsObject('consultarPeriodos', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};
		
		// LLamada al servicio que genera apunte
	PeriodosService.crearPeriodo = function (successHandler, errorHandler) {
	    var request = PeriodosService.getRequest('crearPeriodo');
	    request.success(successHandler).error(errorHandler);
	    return request;
    };
    
    return PeriodosService;
			  
			  
} ]);