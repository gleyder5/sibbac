sibbac20.service('SecurityService', [ '$rootScope', '$http', '$q', 'Logger', '$location', function ($rootScope, $http, $q, Logger,$location) {

  var SecurityService = {};
  var url = "/sibbac20back/rest/login"

  var url = server.url;
  var service = server.service.seguridad.login;
  SecurityService.userActions = null;
  SecurityService.inicializated = false;
  SecurityService.autenticathed = false;
  SecurityService.user = null;
  SecurityService.environment = null;
  SecurityService.cache = {};

  SecurityService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  SecurityService.getRequestFilters = function (action, filters) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filters
      }
    });
    return request;
  };

  SecurityService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  SecurityService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  SecurityService.init = function () {

    var authenticated = SecurityService.getRequestParamsObject('Autenticado', {});
    authenticated.success(function (data) {
      SecurityService.user = data.resultados.user;
      SecurityService.environment = data.resultados.environment;
      SecurityService.authenticated = data.resultados.autenticado;
    });

    var actions = SecurityService.getRequestParamsObject('Permisos', {});
    actions.success(function (data) {
      SecurityService.userActions = data.resultados.permisos;
      if (SecurityService.userActions != null) {
        for (i = 0, len = SecurityService.userActions.length, text = ""; i < len; i++) {
          ap = SecurityService.userActions[i];
          SecurityService.cache[ap.page.url.trim() + "-" + ap.action.name.trim()] = true;
        }
      }
    });

    $q.all([ authenticated, actions ]).then(function () {
      Logger.info("SecurityService: initialized");
      SecurityService.inicializated = true;
    });
    return $q.all([ authenticated, actions ]);

  };

  SecurityService.getUserActions = function (successHandler, errorHandler) {
    var request = SecurityService.getRequestParamsObject('Permisos', {});
    request.success(function (data) {
      SecurityService.userActions = data.resultados.permisos;
      successHandler(data);
    }).error(function () {
      errorHandler(data);
    });
  };

  SecurityService.getMenu = function (successHandler, errorHandler) {
    var request = SecurityService.getRequestParamsObject('Menu', {});
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.actions = function () {
    if (!SecurityService.inicializated) {

      SecurityService.getUserActions(function (data) {
        SecurityService.userActions = data.resultados.permisos;
        SecurityService.inicializated = true;
        return SecurityService.userActions;
      });

    } else {
      return SecurityService.userActions;
    }
  };

  SecurityService.isPermisoPagina = function (pagina) {
    var actions = SecurityService.actions();
    if (actions != null) {
      return SecurityService.isPermisoAccion($rootScope.SecurityActions.CONSULTAR, pagina);
    } else {
      return false;
    }
    return false;
  };

  SecurityService.isPermisoAccion = function (accion, url) {
    var actions = SecurityService.actions();
    var cachedVal = SecurityService.cache[url.trim() + "-" + accion.trim()];
    if (!angular.isUndefined(cachedVal)) {
      // Logger.info("Permiso de accion: " + accion + " para " + url + ": " + cachedVal);
      return cachedVal;
    }
    if (actions != null) {
      for (i = 0, len = actions.length, text = ""; i < len; i++) {
        ap = actions[i];
        if (accion.trim() == ap.action.name.trim() && url.trim() == ap.page.url.trim()) {
          // Logger.info("Permiso de accion permitido: " + accion + " para " + url);
          return true;
        }
      }
      // Logger.warn("Permiso de accion denegado: " + accion + " para " + url);
    } else {
      Logger.error("no hay acciones permitidas para el usuario");
      return false;
    }
    return false;
  };

  SecurityService.isSessionEnabled = function (successHandler, errorHandler) {
    var request = SecurityService.getRequestParamsObject('Autenticado', {});
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.isAuthenticated = function () {
    return SecurityService.authenticated;
  };
  
  SecurityService.redirectToLoginIfNotLoggedIn = function () {
	  if(!$rootScope.isAuthenticated()){
		  console.log("Not logged in. Redirecting user to login screen!");
		  $location.path('/login');
      }
  };

  SecurityService.verificarUsuario = function (successHandler, errorHandler, paramsObject) {
    var request = SecurityService.getRequestParamsObject('PasswordReseteado', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.login = function (successHandler, errorHandler, paramsObject) {
    var request = SecurityService.getRequestParamsObject('Login', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.parametrosComplejidad = function (successHandler, errorHandler, paramsObject) {
    var request = SecurityService.getRequestParamsObject('parametrosComplejidad', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.validaContraseniaActual = function (successHandler, errorHandler, paramsObject) {
    var request = SecurityService.getRequestParamsObject('validaContraseniaActual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.validarHistoricoCrontrasenias = function (successHandler, errorHandler, paramsObject) {
    var request = SecurityService.getRequestParamsObject('validarHistoricoCrontrasenias', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.resetearPassword = function (successHandler, errorHandler, paramsObject) {
    var request = SecurityService.getRequestParamsObject('resetearPassword', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.logout = function (successHandler, errorHandler, paramsObject) {
    var request = SecurityService.getRequestParamsObject('Logout', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  SecurityService.listPassword = function (successHandler, errorHandler, paramsObject) {
    var request = SecurityService.getRequestParamsObject('listPassword', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return SecurityService;

} ]);
