'use restrict';
sibbac20.factory('GenerarInformesService', [ '$http', function ($http) {
  var GenerarInformesService = {};
  var url = server.url;
  var service = server.service.plantillasInformes.generarPlantillasInformes;

  GenerarInformesService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  GenerarInformesService.getRequestFilters = function (action, filters) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filters
      }
    });
    return request;
  };

  GenerarInformesService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  GenerarInformesService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  //LLamada al servicio que obtiene los datos del combo de plantillas del filtro
  GenerarInformesService.cargaPlantillas = function (successHandler, errorHandler) {
    var request = GenerarInformesService.getRequest('CargaPlantillas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  GenerarInformesService.cargaIsin = function (successHandler, errorHandler) {
    var request = GenerarInformesService.getRequest('CargaIsin');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  GenerarInformesService.cargaAlias = function (successHandler, errorHandler) {
    var request = GenerarInformesService.getRequest('CargaAlias');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que genera el informe
  GenerarInformesService.generarInforme = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesService.getRequestParamsObject('CargaGeneracionInformes', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  // LLamada al servicio que genera el informe en excel
  GenerarInformesService.generarInformeExcel = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesService.getRequestParamsObject('ExportaInformeXLS', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  // LLamada al servicio que genera el informe en excel para terceros
  GenerarInformesService.generarInformeExcelTerceros = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesService.getRequestParamsObject('ExportaInformeTercerosXLS', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  //LLamada al servicio para obtener la query que ejecuta el informe
  GenerarInformesService.obtenerQueryInforme = function (successHandler, errorHandler, paramsObject) {
    var request = GenerarInformesService.getRequestParamsObject('ObtenerQueryInforme', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return GenerarInformesService;
} ]);
