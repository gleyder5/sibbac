sibbac20.factory('ConciliacionBancariaTLMService',['$http',function($http) {
							
	  var ConciliacionBancariaTLMService = {};
	  var url = server.url;
	  var service = server.service.contabilidad.conciliacionBancaria.pantallaTLM;
							
	  ConciliacionBancariaTLMService.getRequest = function (action) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	      }
	    });
	    return request;
	  };
	  
	  ConciliacionBancariaTLMService.getRequestFilters = function (action, filter) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        filters : filter
	      }
	    });
	    return request;
	  };

	  ConciliacionBancariaTLMService.getRequestParamsObject = function (action, paramsObject) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        paramsObject : paramsObject
	      }
	    });
	    return request;
	  };

	  ConciliacionBancariaTLMService.generarApuntes = function (successHandler, errorHandler, getRequestParamsObject) {
	    var request = ConciliacionBancariaTLMService.getRequestParamsObject('generarApuntes', getRequestParamsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
							
	  return ConciliacionBancariaTLMService;
							
}]);
