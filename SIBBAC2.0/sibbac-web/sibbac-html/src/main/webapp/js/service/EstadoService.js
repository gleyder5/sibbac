sibbac20.factory('EstadoService', ['$http','$rootScope', function ($http, $rootScope) {
        var EstadoService = {};
        var serverUrl = $rootScope.restParams.serverUrl;
        EstadoService.getEstadosFacturaSugerida = function (successHandler, errorHandler) {
            $http({
                url: serverUrl,
                data: {service: 'SIBBACServiceTmct0estado', action: 'getEstadosFacturaSugerida'},
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null && data.resultados !== null && data.resultados.estados !== null) {
                    datos = data.resultados.estados;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        EstadoService.getEstadosFactura = function(successHandler, errorHandler){

          $http({
                url: serverUrl,
                data: {service: 'SIBBACServiceTmct0estado', action: 'getEstadosFactura'},
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null && data.resultados !== null && data.resultados.estados !== null) {
                    datos = data.resultados.estados;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        return EstadoService;
    }]);

