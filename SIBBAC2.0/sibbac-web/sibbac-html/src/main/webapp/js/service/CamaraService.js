sibbac20.factory('CamaraService', ['$http', function($http){
    var CamaraService = [];

    CamaraService.getCamaras = function(){
        return $http({
            method : 'POST',
            url : '/sibbac20back/rest/service/',
            data : {service : 'SIBBACServiceConciliacion', action : 'getCamara'},
            cache : true
        });
    }

    return CamaraService;
}]);
