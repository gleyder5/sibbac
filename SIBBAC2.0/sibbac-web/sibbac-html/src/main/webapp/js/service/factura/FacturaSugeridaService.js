sibbac20.factory('FacturaSugeridaService', ['$http', '$rootScope', function ($http, $rootScope) {
        var FacturaSugeridaService = {};
        var serverUrl =  $rootScope.restParams.serverUrl;
        var fService = "SIBBACServiceFactura";
        var fsService = "SIBBACServiceFacturaSugerida";
        FacturaSugeridaService.getListaFacturasSugeridas = function (successHandler, errorHandler, filtro) {
            $http({
                url: serverUrl,
                data: {service: fsService, action: "getListadoFacturasSugeridas", filters: filtro},
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null && data.resultados !== null && data.resultados.listadoFacturas !== null) {
                    datos = data.resultados.listadoFacturas;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        FacturaSugeridaService.remove = function(successHandler, errorHandler, filtro){

            $http({
                url: serverUrl,
                method: 'POST',
                data : {service : fsService, action : 'borrarSugeridas', filters : filtro}
            }).success(successHandler).error(errorHandler);
        };
        FacturaSugeridaService.create = function(successHandler, errorHandler, filtro){

            $http({
                url: serverUrl,
                method: 'POST',
                data : {service : fsService, action : 'crearSugeridas', filters : filtro}
            }).success(successHandler).error(errorHandler);
        };

        FacturaSugeridaService.getListForCreate = function (successHandler, errorHandler, filtro) {
            $http({
                url: serverUrl,
                data: {service: fsService, action: "getListadoCrearFacturas", filters: filtro},
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null && data.resultados !== null && data.resultados.listadoFacturas !== null) {
                    datos = data.resultados.listadoFacturas;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        FacturaSugeridaService.marcarParaFacturar = function(successHandler, errorHandler, params){
            $http({
                url: serverUrl,
                data: {service: fsService, action: "marcarFacturar", params: params},
                method: 'POST'
            }).success(successHandler).error(errorHandler);
        };
        FacturaSugeridaService.marcarRectificar = function(successHandler, errorHandler, params){
            $http({
                url: serverUrl,
                data: {service: fService, action: "marcarRectificar", params: params},
                method: 'POST'
            }).success(successHandler).error(errorHandler);
        };
        return FacturaSugeridaService;


    }]);

