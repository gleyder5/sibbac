'use restrict';
sibbac20.factory('DirectivasService', [ '$http', function ($http) {
  var DirectivasService = {};
  var url = server.url;
  var service = server.service.directives.getDirectives;

  DirectivasService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  DirectivasService.getRequestFilters = function (action, filters) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filters
      }
    });
    return request;
  };

  DirectivasService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  DirectivasService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que devuelve el tipo de elemento.
  DirectivasService.devolverTipoElemento = function (successHandler, errorHandler) {
    var request = DirectivasService.getRequest('DevolverElementos');
    request.success(successHandler).error(errorHandler);
    return request;
  };
  
  // LLamada al servicio que devuelve los ids del componente.
  DirectivasService.getIdsElementoDirectiva = function (successHandler, errorHandler, paramsObject) {
    var request = DirectivasService.getRequestParamsObject('DevolverIdsElementos', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  
  // LLamada al servicio que devuelve los elementos por parametros.
  DirectivasService.devolverElementosByParams = function (successHandler, errorHandler, paramsObject) {
    var request = DirectivasService.getRequestParamsObject('DevolverElementosByParams', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  
  //LLamada al servicio que obtiene un valor precargado para el filtro dinamico
  DirectivasService.getValorPrecargado = function (successHandler, errorHandler, paramsObject) {
    var request = DirectivasService.getRequestParamsObject('DevolverValorPrecargado', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  
  return DirectivasService;
} ]);