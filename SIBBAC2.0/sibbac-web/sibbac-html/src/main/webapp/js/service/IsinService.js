angular.module('sibbac20').factory('IsinService', ['$http', function ($http) {

        var IsinService = {};

        IsinService.getIsines = function (successHandler, errorHandler) {
            /** ISINES **/
            var request = $http({
                method: 'POST',
                url: '/sibbac20back/rest/service/',
                data: {service: 'SIBBACServiceConciliacion', action: 'getIsin'},
                cache: true
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(function (data, status, headers, config) {
                    var datosIsin = {};
                    if (data != null && data.resultados != null &&
                            data.resultados.result_isin != null) {
                        var datosIsin = data.resultados.result_isin;

                    }
                    successHandler(datosIsin);

                }).error(errorHandler);
            }
            return request;
        };

        return IsinService;
    }]);
