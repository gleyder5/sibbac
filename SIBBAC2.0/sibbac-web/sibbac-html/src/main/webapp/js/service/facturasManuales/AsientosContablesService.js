'use restrict';
sibbac20.factory('AsientosContablesService', [ '$http', function ($http) {
	var AsientosContablesService = {};
	var url = server.url;
	var service = server.service.facturasmanuales.consultarAsientosContables;

	AsientosContablesService.getRequestFilters = function (action, filters) {
		var o = angular.copy(filters);
		var request = $http({
			method : 'POST',
			url : url,
			data : {
				action : action,
				service : service,
				filters : o
			}
		});
		return request;
	};

	AsientosContablesService.getRequest = function (action) {
		var request = $http({
			method : 'POST',
			url : url,
			data : {
				action : action,
				service : service,
			}
		});
		return request;
	};

	AsientosContablesService.getRequestParamsObject = function (action, paramsObject) {
		var o = angular.copy(paramsObject);
		var request = $http({
			method : 'POST',
			url : url,
			data : {
				action : action,
				service : service,
				paramsObject : o
			}
		});
		return request;
	};

	// LLamada al servicio que obtiene los datos del combo del filtro
	AsientosContablesService.cargaCatalogos = function (successHandler, errorHandler) {
		var request = AsientosContablesService.getRequest('CargaSelects');
		request.success(successHandler).error(errorHandler);
		return request;
	};

	// LLamada al servicio que obtiene los datos de la busqueda de asientos contables
	AsientosContablesService.buscarAsientosContables = function (successHandler, errorHandler, filters) {
		var request = AsientosContablesService.getRequestFilters('BuscarAsientosContables', filters);
		request.success(successHandler).error(errorHandler);
		return request;
	};

	// LLamada al servicio que recobra los datos adicionales del asiento contable seleccionado
	AsientosContablesService.verAsientoContable = function (successHandler, errorHandler, paramsObject) {
		var request = AsientosContablesService.getRequestParamsObject('VerAsientoContable', paramsObject);
		request.success(successHandler).error(errorHandler);
		return request;
	};

	return AsientosContablesService;
} ]);
