sibbac20.factory('EntidadRegistroService', ['$http', function ($http) {
        var EntidadRegistroService = {};
        var url = server.url;
        var service = server.service.entidadRegistro;
        function getGenericRequest(action, filter) {
            var request = $http({
                url: url,
                method: 'POST',
                data: {service: service, action: action, filters: filter}
            });
            return request;
        }
        EntidadRegistroService.getAllEntidadRegistro = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: url,
                method: 'POST',
                data: {service: service, action: 'getAllEntidadRegistro', filters: filter}
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        EntidadRegistroService.getAllTiposEntidadRegistro = function (successHandler, errorHandler, filter) {
            var request = getGenericRequest('getAllTiposEntidadRegistro', filter);
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        EntidadRegistroService.saveEntidadRegistro = function (successHandler, errorHandler, filter) {
            var request = getGenericRequest('saveEntidadRegistro', filter);
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        EntidadRegistroService.updateEntidadRegistro = function (successHandler, errorHandler, filter) {
            var request = getGenericRequest('updateEntidadRegistro', filter);
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        EntidadRegistroService.deleteEntidadRegistro = function (successHandler, errorHandler, filter) {
            var request = getGenericRequest('deleteEntidadRegistro', filter);
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        EntidadRegistroService.saveTipoEntidadRegistro = function (successHandler, errorHandler, filter) {
            var request = getGenericRequest('saveTipoEntidadRegistro', filter);
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        EntidadRegistroService.updateTipoEntidadRegistro = function (successHandler, errorHandler, filter) {
            var request = getGenericRequest('updateTipoEntidadRegistro', filter);
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        EntidadRegistroService.deleteTipoEntidadRegistro = function (successHandler, errorHandler, filter) {
            var request = getGenericRequest('deleteTipoEntidadRegistro', filter);
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        return EntidadRegistroService;
    }]);

