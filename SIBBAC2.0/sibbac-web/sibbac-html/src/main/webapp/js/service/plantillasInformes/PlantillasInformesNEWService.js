'use restrict';
sibbac20.factory('PlantillasInformesNEWService', [ '$http', function ($http) {
  var PlantillasInformesNEWService = {};
  var url = server.url;
  var service = server.service.plantillasInformes.gestionPlantillasInformesNEW;

  PlantillasInformesNEWService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  PlantillasInformesNEWService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que obtiene los de todas las plantillas de informes filtrados.
  PlantillasInformesNEWService.cargaPlantillasInformes = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesNEWService.getRequestParamsObject('CargaPlantillasInformes', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  
  // LLamada al servicio que obtiene los de todas las plantillas de informes filtrados.
  PlantillasInformesNEWService.cargaPlantillaInforme = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesNEWService.getRequestParamsObject('CargaPlantillaInforme', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos del combo de campos detalle
  PlantillasInformesNEWService.cargaCamposDetalle = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesNEWService.getRequestParamsObject('CargaCamposDetalle', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  
  // LLamada al servicio que obtiene los datos del combo de campos detalle
  PlantillasInformesNEWService.cargaCamposDetalleFiltro = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesNEWService.getRequestParamsObject('CargaCamposDetalleFiltro', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos del combo de plantillas del filtro
  PlantillasInformesNEWService.cargaPlantillas = function (successHandler, errorHandler) {
    var request = PlantillasInformesNEWService.getRequest('CargaPlantillas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  PlantillasInformesNEWService.cargaIsin = function (successHandler, errorHandler) {
    var request = PlantillasInformesNEWService.getRequest('CargaIsin');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  PlantillasInformesNEWService.cargaAlias = function (successHandler, errorHandler) {
    var request = PlantillasInformesNEWService.getRequest('CargaAlias');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de combos estaticos
  PlantillasInformesNEWService.cargaSelectsEstaticos = function (successHandler, errorHandler) {
    var request = PlantillasInformesNEWService.getRequest('CargaSelectsEstaticos');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  PlantillasInformesNEWService.crearPlantilla = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesNEWService.getRequestParamsObject('CrearPlantilla', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  PlantillasInformesNEWService.modificarPlantilla = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesNEWService.getRequestParamsObject('ModificarPlantilla', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  PlantillasInformesNEWService.eliminarPlantillas = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesNEWService.getRequestParamsObject('EliminarPlantillas', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  PlantillasInformesNEWService.cargarClases = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesNEWService.getRequestParamsObject('CargaClases', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  PlantillasInformesNEWService.cargarMercados = function (successHandler, errorHandler, paramsObject) {
	  var request = PlantillasInformesNEWService.getRequestParamsObject('CargaMercados', paramsObject);
	  request.success(successHandler).error(errorHandler);
	  return request;
  };

  // LLamada al servicio que obtiene las plantillas a raiz de una clase seleccionada
  PlantillasInformesNEWService.cargaPlantillasFiltro = function (successHandler, errorHandler, paramsObject) {
	  var request = PlantillasInformesNEWService.getRequestParamsObject('CargarPlantillasFiltro', paramsObject);
	  request.success(successHandler).error(errorHandler);
	  return request;
  };

  return PlantillasInformesNEWService;
} ]);
