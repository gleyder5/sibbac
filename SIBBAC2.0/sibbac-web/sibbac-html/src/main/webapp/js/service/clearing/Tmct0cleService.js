sibbac20.factory('Tmct0cleService', ['$http', function($http){
        var Tmct0cleService = {};
        var service = server.service.clearing.tmct0cle;
        var url = server.url;
        Tmct0cleService.getRequest = function(action, filter){
            var request = $http({
                method : 'POST',
                url : url,
                data : {service : service, action:action, filters : filter}
            });
            return request;
        };
        Tmct0cleService.getLista = function(successHandler, errorHandler, filter){
            var request = Tmct0cleService.getRequest('getLista', filter);
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        return Tmct0cleService;
}]);

