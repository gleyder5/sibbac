(function (sibbac20, angular, console) {
    "use strict";
    sibbac20.factory('MantenimientoOpService', ['$resource', "$http", function ($resource, $http) {
        var baseUrl, baseExportUrl, numerationsUrl, numerationsUrlDel, isinUrl, idTitularUrl, cleUrl, queryRes, queryResPost, queryResPut, queryResDel,
            queryResNumeration, queryResNumerationPost, queryResNumerationPut, queryResNumerationDel, queryResIdTitular, queryResCle, queryResQueryCombos,
            exportFetch, queryResGetNominal;
        baseUrl = "/sibbac20back/rest/sicav/mantenimientoOp";
        // baseExportUrl = baseUrl +
		// "/exportEmisionDocumento/:queryName/:detail";
        baseExportUrl = baseUrl + "/exportEmisionDocumento";
        numerationsUrl = baseUrl + "/numeration";
        numerationsUrlDel = baseUrl + "/numeration/delete";
        idTitularUrl = baseUrl + "/idTitular";       
        
        // FIXME:Eliminar
        cleUrl = baseUrl + "/cle";

        // Llamada a back con GET para rellenar input relacionados con efectivo en los modales
        queryResGetNominal = $resource(baseUrl + "/getNominal", {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con POST, permite los parámetros queryName y detail
        queryRes = $resource(baseUrl + "/:queryName/:detail", { queryName: "@queryName", detail: "@detail" }, { execute: { method: "POST", isArray: false } });
        // Llamada a back con POST
        queryResPost = $resource(baseUrl, {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con PUT
        queryResPut = $resource(baseUrl, {}, { execute: { method: "PUT", isArray: false } });
        // Llamada a back con DELETE, permite el parámetro ids
        queryResDel = $resource(baseUrl + '/:ids', { ids: '@ids' }, { execute: { method: "DELETE", isArray: false } });
        // Llamada a back para traerse las numeraciones de la operación que se pide
        queryResNumeration = $resource(numerationsUrl + '/:num', { num: '@num' }, { execute: { method: "GET", isArray: true } });
        // Llamada a back para traerse las numeraciones de la operación que se pide
        queryResNumerationPost = $resource(numerationsUrl, {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back para traerse las numeraciones de la operación que se pide
        queryResNumerationPut = $resource(numerationsUrl, {}, { execute: { method: "PUT", isArray: false } });
        // Llamada a back para 'eliminar' las numeraciones de la operación que se pide
        queryResNumerationDel = $resource(numerationsUrlDel + '/:ids', { ids: '@ids' }, { execute: { method: "DELETE", isArray: false } });
        // Llamada a back para traerse las IDS para autocompletar el campo
		// Identificador del Titular
        
        // FIXME:Eliminar
        queryResIdTitular = $resource(idTitularUrl, { execute: { method: "POST", isArray: false } });
        // Llamada a back para traerse los Ordenantes/Custodios para
		// autocompletar el campo Ordenantes/Custodio del Titular
        
        // FIXME:Eliminar
        queryResCle = $resource(cleUrl, {}, { execute: { method: "GET", isArray: true } });
        // Combo titular
        queryResQueryCombos = $resource(baseUrl + "/:queryName", { queryName: "@queryName" }, { execute: { method: "GET", isArray: false } });
        // Llamada al /POST que exporta los datos de la tabla a una excel
        exportFetch = function(exportUrl, queryName, detail) {
        	 fetch(exportUrl, {
					method: 'POST',
					headers: {'Content-Type':'application/x-www-form-urlencoded'},
				}).then(function(response) {
     	     return response.blob();
     	   }).then(function(blob) {
     		   
     		   if (navigator.msSaveBlob) {
						navigator
								.msSaveBlob(
										blob,
										filename);
					} else {
						var link = document
								.createElement('a');
						link.href = URL
								.createObjectURL(blob);
						var extension ="";
						if (detail == 'excel') {
							extension='.xls';
						} else if (detail == 'csv') {
							extension='.csv';
						}
						link.download = queryName + extension;
						document.body
								.appendChild(link);
						link.click();
						document.body
								.removeChild(link);
					}
     	   });
        
        };
        
        return {
            baseUrl: baseUrl,
            queries: queryRes.query,
            
            // Llamada a /filters
            dynamicFilters: function (name) {
                return queryRes.query({ queryName: name, detail: "filters" });
            },
            
            // Llamada a /columns
            columns: function (name, success) {
                var res = queryRes.query({ queryName: name, detail: "columns" }, function () {
                    success(res);
                });
            },

            // Llamada al /POST que devuelve los datos de la tabla
            executeQuery: function (queryName, filter, success, fail) {
                var data;
                filter.queryName = queryName;
                if ((filter.FECHA_OPE == undefined && filter.FECHA_OPE2 == undefined) == false) {
                    if (filter.FECHA_OPE == undefined) {
                        filter.FECHA_OPE = [];
                        filter.FECHA_OPE.push('01/01/1900');
                    } else {
                        filter.FECHA_OPE = [filter.FECHA_OPE];
                    }

                    if (filter.FECHA_OPE2 != undefined) {
                        filter.FECHA_OPE.push(angular.copy(filter.FECHA_OPE2));
                        filter.FECHA_OPE2 = [];
                    } else {
                        filter.FECHA_OPE.push('31/12/2100');
                    }
                }
                data = queryRes.query(filter, function () {
                    if (data.length > 0 && data[0].error !== undefined) {
                        fail(data[0].error);
                        return;
                    }
                    success(data);
                }, fail);
            },
            
            // Llamada al /POST que devuelve los datos de la tabla
            executeAction: function (queryName, params, items, success, fail) {
                var body = params;
                queryRes.execute({ queryName: queryName }, body, success, fail);                
            },
            
            // Llamada al /POST que hace un save de los datos mandados por
			// parámetro
            executeActionPOST: function (params, direction, success, fail) {
                if (direction == "operation") {
                    queryResPost.execute(params, success, fail);
                } else if (direction == "numeration") {
                    queryResNumerationPost.execute(params, success, fail);
                }
            },
            
            // Llamada al /PUT que hace un edit de los datos mandados por
			// parámetro
            executeActionPUT: function (params, direction, success, fail) {
                if (direction == "operation") {
                    queryResPut.execute(params, success, fail);
                } else if (direction == "numeration") {
                    queryResNumerationPut.execute(params, success, fail);
                }
            },
            
            // Llamada al /DELETE que realiza una eliminación por id de la lista
			// enviada por parámetro
            executeActionDEL: function (params, direction, success, fail) {
                var body = params;
                if (direction == "operation") {
                    queryResDel.execute({ ids: body }, success, fail);
                } else if (direction == "numeration") {
                    queryResNumerationDel.execute({ ids: body }, success, fail);
                }
            },
            
            // Llamada a back para traerse las numeraciones de la operación que
			// se pide
            executeActionNumeration: function (num, success, fail) {
                return queryResNumeration.execute({num: num}, success, fail);
            },
            
            // Llamada a back para traerse las IDS para autocompletar el campo
			// Identificador del Titular
            executeActionQueryCombos: function (queryName, success, fail) {
            	var params;
            	var items;
            	return queryResQueryCombos.query({ queryName: queryName }, success, fail);
            },
            /*
                     
             * Exporta En Excel o Csv
             */
            executeExportEmisionDoc : function(queryName, filter, detail, success, fail) {
            	var exportUrl = baseExportUrl + "/" + queryName + "?" + filter;
            	exportFetch(exportUrl, queryName, detail);
            	
            },
           
            /*
             * Exporta En Excel o Csv
             */
            executeExportExcelCsv : function(queryName, filter, detail, success, fail) {
               var exportUrl = baseUrl + "/" + queryName + "/" + detail + "?" + filter;
               exportFetch(exportUrl, queryName, detail);
		    },
		    // Llamada al /POST que calcula el nominal
		    executeActionGETNominal: function (params, items, success, fail) {
		       var body = params;
		       return queryResGetNominal.execute(body, success, fail);
		    }
        }        
    }]);
})(sibbac20, angular, console);

