sibbac20.factory('JobsService', ['$http', '$rootScope', function ($http, $rootScope) {
        var JobsService = {};
        var serverUrl = server.url;
        JobsService.performJobCall = function (successHandler, errorHandler, data) {
        	var request =$http({
                url: serverUrl,
                data: data,
                method: 'POST'
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
        	return request;
        };
        return JobsService;
    }]);

