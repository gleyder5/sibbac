sibbac20.factory('ConciliacionDiariaService', ['$http', function ($http) {
        var ConciliacionDiariaService = {};
        var url = server.url;
        var service = server.service.conciliacion.diaria;

        ConciliacionDiariaService.getFechaInicial = function () {
            return $http({
                method: 'POST',
                url: url,
                cache: true,
                data: {action: 'getFechaInicial', service: service}
            });
        };
        ConciliacionDiariaService.getOperaciones = function (params, successHandler, errorHandler) {
            $http({
                method: 'POST',
                url: url,
                data: {service: service, action: 'getOperacionesDiaria', filters: params}
            }).success(function (data, status, headers, config) {
                if (data.resultados !== undefined && data.resultados !== null && data.resultados.resultados_diaria !== undefined &&
                        data.resultados.resultados_diaria !== null) {
                    var datos = data.resultados.resultados_diaria;
                    successHandler(datos);
                }else{
                    errorHandler("Ocurrió un error durante la extración de datos de la consulta.");
                }
            }).error(errorHandler);
        };


        ConciliacionDiariaService.getOperacionesDiaria = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: url,
                method: 'POST',
                data: {service: service, action: 'getOperacionesDiaria',
                    filters: filter}
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        return ConciliacionDiariaService;
    }]);

