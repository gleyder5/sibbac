(function (sibbac20, angular, console) {
    "use strict";
    sibbac20.factory('InformesMclaganService', ['$resource', function ($resource) {
        var baseUrl, queryRes;
        baseUrl = "/sibbac20back/rest/mclagan";
        // Llamada a back con POST, permite los parámetros queryName y detail
        queryRes = $resource(baseUrl, {}, { execute: { method: "POST", isArray: false } });
        return {
            baseUrl: baseUrl,
            // Llamada al /POST que devuelve los datos de la tabla
            generate: function (params, success, fail) {
                queryRes.execute(params, success, fail);
            }
        }
    }]);
})(sibbac20, angular, console);

