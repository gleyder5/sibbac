'use restrict';
sibbac20.factory('DesglosesMacroService', ['$http', function ($http) {
        var DesglosesMacroService = {};
        var url = server.url;
        var service = server.service.clearing.desglosesMacro;

        DesglosesMacroService.getRequest = function (action, filter) {
            var request = $http({
                method: 'POST',
                url: url,
                data: {action: action, service: service, filters: filter}
            });
            return request;
        };

        DesglosesMacroService.getRequestParams = function (action, params) {
	        var request = $http({
	            method: 'POST',
	            url: url,
	            data: {action: action, service: service, params: params}
	        });
	        return request;
    	};

    	DesglosesMacroService.listaClientes = function (successHandler, errorHandler, filter) {
            var request = DesglosesMacroService.getRequest('ListaClientes', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        DesglosesMacroService.listaDesglosesMacro = function(successHandler, errorHandler, filter) {
            var request = DesglosesMacroService.getRequest('ListaDesglosesMacro', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        return DesglosesMacroService;
    }]);

