'use strict';
sibbac20.factory('ConciliacionEfectivoService', ['$http', function($http){
        var ConciliacionEfectivoService = {};
        var url = server.url;
        var service = server.service.conciliacion.efectivo;
        ConciliacionEfectivoService.getOperacionesEfectivo = function(successHandler, errorHandler, filter){
            var request = $http({
                url : url,
                method : 'POST',
                data : {service : service, action : 'getOperacionesEfectivo', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        return ConciliacionEfectivoService;
}]);
