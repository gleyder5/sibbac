'use restrict';
sibbac20.factory('CestaOrdenesFidessaService', [ '$http', function ($http) {
  var CestaOrdenesFidessaService = {};
  var url = server.url;
  var service = server.service.clearing.cestaOrdenesFidessa;

  CestaOrdenesFidessaService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  CestaOrdenesFidessaService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que obtiene los ficheros de la ruta.
  CestaOrdenesFidessaService.obtenerFicheros = function (successHandler, errorHandler, paramsObject) {
    var request = CestaOrdenesFidessaService.getRequestParamsObject('obtenerFicheros', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CestaOrdenesFidessaService.obtenerNumeroDesgloses = function (successHandler, errorHandler, paramsObject) {
    var request = CestaOrdenesFidessaService.getRequestParamsObject('obtenerNumeroDesgloses', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CestaOrdenesFidessaService.ejecutarFaseUno = function (successHandler, errorHandler, paramsObject) {
    var request = CestaOrdenesFidessaService.getRequestParamsObject('ejecutarFaseUno', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  CestaOrdenesFidessaService.ejecutarFaseDos = function (successHandler, errorHandler, paramsObject) {
    var request = CestaOrdenesFidessaService.getRequestParamsObject('ejecutarFaseDos', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return CestaOrdenesFidessaService;
} ]);
