sibbac20.factory('HistoricoPartidasCasadasService', [
		'$http',
		function($http) {
			
	var HistoricoPartidasCasadasService = {};
	var url = server.url;
	var service = server.service.contabilidad.historicoPartidasCasadas;
	
	HistoricoPartidasCasadasService.getRequest = function (action) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	      }
	    });
	    return request;
	  };

	  HistoricoPartidasCasadasService.getRequestParamsObject = function (action, paramsObject) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        paramsObject : paramsObject
	      }
	    });
	    return request;
	  };

	  HistoricoPartidasCasadasService.consultarHistorico = function (successHandler, errorHandler, paramsObject) {
	    var request = HistoricoPartidasCasadasService.getRequestParamsObject('consultarHistorico', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };


	  HistoricoPartidasCasadasService.consultarPartidasTLM = function (successHandler, errorHandler, paramsObject) {
	    var request = HistoricoPartidasCasadasService.getRequestParamsObject('consultarPartidasTLM', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
	  
	  
	  HistoricoPartidasCasadasService.consultarDesglosesSIBBAC = function (successHandler, errorHandler, paramsObject) {
	    var request = HistoricoPartidasCasadasService.getRequestParamsObject('consultarDesglosesSIBBAC', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
	  
	  HistoricoPartidasCasadasService.exportarXLS = function (successHandler, errorHandler, paramsObject) {
	    var request = HistoricoPartidasCasadasService.getRequestParamsObject('exportarXLS', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

	  return HistoricoPartidasCasadasService;
		
}]);