'use restrict';
sibbac20.factory('ParametrosSeguridadService', [ '$http', function ($http) {
  var ParametrosSeguridadService = {};
  var url = server.url;
  var service = server.service.gestionAccesos.parametrosSeguridad;

  ParametrosSeguridadService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  ParametrosSeguridadService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  ParametrosSeguridadService.consultarParametros = function (successHandler, errorHandler) {
    var request = ParametrosSeguridadService.getRequest('consultarParametros');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que borra los usuarios que coincidan con los filtros
  ParametrosSeguridadService.guardarParametros = function (successHandler, errorHandler, paramsObject) {
    var request = ParametrosSeguridadService.getRequestParamsObject('guardarParametros', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return ParametrosSeguridadService;
} ]);