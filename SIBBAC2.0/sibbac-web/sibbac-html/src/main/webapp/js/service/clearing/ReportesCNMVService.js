'use restrict';
sibbac20.factory('ReportesCNMVService', [ '$http', function ($http) {
  var ReportesCNMVService = {};
  var url = server.url;
  var service = server.service.clearing.reportesCNMV;

  ReportesCNMVService.getRequest = function (action, filter) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filter
      }
    });
    return request;
  };

  ReportesCNMVService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  ReportesCNMVService.getRequestTodos = function (action, filter, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filter,
        params : params
      }
    });
    return request;
  };

  ReportesCNMVService.listaOperacionesH47 = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('getOperacionesH47', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.listaDetalle = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('getDetallesH47', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.listaTitulares = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('getTitularesH47', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.cargaComboEnvios = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('getEstadosEnvio', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.listaBookingCreacion = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('getAlcsFromNbooking', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.convertirAlcToH47 = function (successHandler, errorHandler, param) {
    var request = ReportesCNMVService.getRequestParams('convertAlcsToH47', param);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.updateOperaciones = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('updateOperacionesH47', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.getTitularById = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('getTitularById', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.getTipoDocumento = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('getTitularById', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.updateTitulares = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('updateTitularesH47', filter);

    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.updateOperacionesMasiva = function (successHandler, errorHandler, filter, params) {
    var request = ReportesCNMVService.getRequestTodos('updateEstadosMasiva', filter, params);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.getHoraMaxima = function (successHandler, errorHandler, filter) {
    var request = ReportesCNMVService.getRequest('getHoraMaxima', filter);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.getNumTitulosActuales = function (successHandler, errorHandler, param) {
    var request = ReportesCNMVService.getRequestParams('getNumTitulosActuales', param);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  ReportesCNMVService.getParametrosEjecucion = function (successHandler, errorHandler, filter) {
	    var request = ReportesCNMVService.getRequest('getParametrosEjecucion', filter);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
  
	  
  ReportesCNMVService.getResultadosEjecuciones = function (successHandler, errorHandler, filter) {
	    var request = ReportesCNMVService.getRequest('getResultadosEjecuciones', filter);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };	  

  ReportesCNMVService.getResultadoGeneracionFicheroOperaciones = function (successHandler, errorHandler, filter) {
	  var request = ReportesCNMVService.getRequest('getResultadoGeneracionFicheroOperaciones', filter);
	  request.success(successHandler).error(errorHandler);
	  return request;
  };

	  
		    
		  	  
  
  return ReportesCNMVService;
} ]);

