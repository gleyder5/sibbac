sibbac20.factory('EtiquetasService', ['$http', '$rootScope', function ($http, $rootScope) {
        var EtiquetasService = {};
        var serverUrl = server.url;
        var fService = server.service.configuracion.etiquetasService;
        EtiquetasService.getListaEtiquetas = function(successHandler, errorHandler){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'getListaEtiquetas'}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        return EtiquetasService;
    }]);
