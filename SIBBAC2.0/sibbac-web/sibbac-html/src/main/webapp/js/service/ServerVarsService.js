sibbac20.factory('ServerVarsService', ['$http', function($http){
        var ServerVarsService = {};
        var url = "/sibbac20back/rest/server-vars"
        ServerVarsService.getServerVars = function(successHandler, errorHandler){
            $http({
                url : url,
                method : 'POST',
                cache : true
            }).success(successHandler).error(errorHandler);
        };
        return ServerVarsService;
}]);

