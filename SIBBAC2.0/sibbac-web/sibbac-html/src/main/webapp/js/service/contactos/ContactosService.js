'use strict';
sibbac20.factory('ContactosService', ['$http', function($http){
        var ContactosService = {};
        var serverUrl = server.url;
        var fService = server.service.contactos.contactosService;

        ContactosService.altaContacto = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'altaContacto', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        ContactosService.getListaContactos = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'getListaContactos', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        ContactosService.cambioStatus = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'cambioStatus', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        ContactosService.modificacionContacto = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'modificacionContacto', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        ContactosService.bajaContacto = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'bajaContacto', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        return ContactosService;
}]);
