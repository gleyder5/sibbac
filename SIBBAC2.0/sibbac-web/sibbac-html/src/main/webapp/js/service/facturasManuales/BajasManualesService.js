'use restrict';
sibbac20.factory('BajasManualesService', [ '$http', function ($http) {
  var BajasManualesService = {};
  var url = server.url;
  var service = server.service.facturasmanuales.bajasManuales;

  BajasManualesService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  BajasManualesService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que obtiene los de todas las plantillas de informes filtrados.
  BajasManualesService.consultarFacturasContabilizadas = function (successHandler, errorHandler, paramsObject) {
    var request = BajasManualesService.getRequestParamsObject('consultarFacturasContabilizadas', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene un detalle de una factura
  BajasManualesService.detalleBajaManual = function (successHandler, errorHandler, paramsObject) {
    var request = BajasManualesService.getRequestParamsObject('detalleBajaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  BajasManualesService.anularBajasManuales = function (successHandler, errorHandler, paramsObject) {
    var request = BajasManualesService.getRequestParamsObject('anularBajasManuales', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return BajasManualesService;

} ]);
