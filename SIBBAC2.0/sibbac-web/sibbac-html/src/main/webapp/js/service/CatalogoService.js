'use restrict';
sibbac20.factory('CatalogoService', [ '$http', function ($http) {
  var CatalogoService = {};
  var url = server.url;
  var service = server.service.catalogo;

  CatalogoService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  CatalogoService.getRequestFilters = function (action, filters) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filters
      }
    });
    return request;
  };

  CatalogoService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  CatalogoService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que busca todos los catalogos juntos
  CatalogoService.todos = function (successHandler, errorHandler, paramsObject) {
    var request = CatalogoService.getRequestParamsObject('', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return CatalogoService;

} ]);
