(function(sibbac20, angular, console) {
    "use strict";
    sibbac20.factory("PtiExecutionsService",["$resource",function($resource){ 
      var baseUrl, queryRes;
      baseUrl = "/sibbac20back/rest/operaciones/ptiexecutions";
      queryRes = $resource(baseUrl + "/:queryName/:detail", 
          {queryName: "@queryName", detail:"@detail"});
      
      return {
          baseUrl: baseUrl,
          queries: function(){return [];},
          dynamicFilters: function(name) {
            return queryRes.query({detail: "filters"});
          },
          columns: function(name, success) {
            var res = queryRes.query({detail: "columns"}, function(){
              success(res);
            });
          },
          executeQuery: function(queryName, filter, success, fail) {
          var data;
          filter.queryName = queryName;
          data = queryRes.query(filter, function() {
            if(data.length > 0 && data[0].error !== undefined) {
              fail(data[0].error);
              return;
            }
            success(data);
          }, fail);
          },
          autocompleter: function(field) {
            return queryRes.query({queryName: "autocompleter", detail: field});
          }
       }
    }]);
})(sibbac20, angular, console);