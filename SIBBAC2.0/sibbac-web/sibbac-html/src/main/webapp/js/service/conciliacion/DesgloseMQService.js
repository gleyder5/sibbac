'use strict';
sibbac20.factory('DesgloseMQService', ['$http', function($http){
	var DesgloseMQService = {};
	var serverUrl = server.url;
	var service = server.service.conciliacion.desgloses;

	function getRequest(action, filtro){
        var request = $http({
            url : serverUrl,
            method : 'POST',
            data : {service : service, action : action, filters : filtro}
        });
        return request;
    }

	function getRequestList(action, data){
        	var request =$http({
                url: serverUrl,
                data: data,
                method: 'POST'
            });
        return request;
    }

	DesgloseMQService.getDesglosesCount = function(successHandler, errorHandler, filter){
        var request = getRequest('getDesglosesCount', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesgloseMQService.getDesglosesSinPaginacion = function(successHandler, errorHandler, filter){
        var request = getRequest('getDesglosesSinPaginacion', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesgloseMQService.getDesglosesHuerfanosCount = function(successHandler, errorHandler, filter){
        var request = getRequest('getDesglosesHuerfanosCount', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesgloseMQService.getDesglosesHuerfanosSinPaginacion = function(successHandler, errorHandler, filter){
        var request = getRequest('getDesglosesHuerfanosSinPaginacion', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesgloseMQService.marcarValidado = function(successHandler, errorHandler, data){
        var request = getRequestList('marcarValidado', data);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesgloseMQService.altaDesglose = function(successHandler, errorHandler, filter){
        var request = getRequest('altaDesglose', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesgloseMQService.editarDesglose = function(successHandler, errorHandler, filter){
        var request = getRequest('editarDesglose', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesgloseMQService.borrarDesglose = function(successHandler, errorHandler, filter){
        var request = getRequest('borrarDesglose', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };


    DesgloseMQService.getListaBolsaMercado = function(successHandler, errorHandler, filter){
        var request = getRequest('getListaBolsaMercado', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    return DesgloseMQService;

}]);//sibbac20.factory('DesgloseMQService', ['$http', function($http){
