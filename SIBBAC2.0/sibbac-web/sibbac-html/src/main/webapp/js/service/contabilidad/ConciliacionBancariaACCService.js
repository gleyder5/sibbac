sibbac20.factory('ConciliacionBancariaACCService',['$http',function($http) {
							
	  var ConciliacionBancariaACCService = {};
	  var service = server.service.contabilidad.conciliacionBancaria.pantallaACC;
	  var url = server.url;
							
	  ConciliacionBancariaACCService.getRequest = function (action) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	      }
	    });
	    return request;
	  };
	  
	  ConciliacionBancariaACCService.getRequestFilters = function (action, filter) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        filters : filter
	      }
	    });
	    return request;
	  };

	  ConciliacionBancariaACCService.getRequestParamsObject = function (action, paramsObject) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        paramsObject : paramsObject
	      }
	    });
	    return request;
	  };

	  ConciliacionBancariaACCService.generarApuntes = function (successHandler, errorHandler, paramsObject) {
	    var request = ConciliacionBancariaACCService.getRequestParamsObject('generarApuntes', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };
							
	  return ConciliacionBancariaACCService;
							
}]);
