sibbac20.factory('FacturaService', ['$http', '$rootScope', function ($http, $rootScope) {
        var FacturaService = {};
        var serverUrl = $rootScope.restParams.serverUrl;
        var frectService = "SIBBACServiceFacturaRectificativa";
        FacturaService.getFacturasRectificativas = function (successHandler, errorHandler, filtro) {
            $http({
                method: 'POST',
                url: serverUrl,
                data: {service: frectService, action: 'getListadoFacturasRectificativas', filters: filtro}
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null && data.resultados !== null && data.resultados.listadoFacturas !== null) {
                    datos = data.resultados.listadoFacturas;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        FacturaService.getListaFacturas = function (successHandler, errorHandler, filtro) {
            $http({
                url: serverUrl,
                data: {service: 'SIBBACServiceFactura', action: 'getListadoFacturas', filters: filtro},
                method: "POST"
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== undefined && data.resultados !== undefined && data.resultados.listadoFacturas !== undefined) {
                    datos = data.resultados.listadoFacturas;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        FacturaService.getTransicionesDeFactura = function (successHandler, errorHandler, filtro) {
            $http({
                url: serverUrl,
                data: {service: 'SIBBACServiceFactura', action: 'getTransicionesFactura', filters: filtro},
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== undefined && data.resultados !== undefined && data.resultados.listadoFacturas !== undefined) {
                    datos = data.resultados.listadoFacturas;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        FacturaService.getOrdenesFactura = function(successHandler, errorHandler, filtro){
          $http({
              url : serverUrl,
              data : {service : "SIBBACServiceOrdenesFactura" ,action : "getListaOrdenesFactura", filters : filtro},
              method : 'POST'
          }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== undefined && data.resultados) {
                    datos = data.resultados;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        FacturaService.enviarDuplicado = function(successHandler, errorHandler, filtro){
            $http({
                url : serverUrl,
                data : {service : "SIBBACServiceFactura" ,action : "enviarDuplicado", filters : filtro},
                method : 'POST'
            }).success(function (data, status, headers, config) {
                  var datos = {};
                  if (data !== undefined && data.resultados) {
                      datos = data.resultados;
                  }
                  successHandler(datos);
              }).error(errorHandler);
          };
        return FacturaService;
    }]);

