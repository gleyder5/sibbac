sibbac20.factory('ConciliacionBancariaOURService',['$http', '$q',function($http, $q) {
							
	  var ConciliacionBancariaOURService = {};
	  var url = server.url;
	  var service = server.service.contabilidad.conciliacionBancaria.pantallaOUR;
							
	  ConciliacionBancariaOURService.getRequest = function (action) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	      }
	    });
	    return request;
	  };
	  
	  ConciliacionBancariaOURService.getRequestFilters = function (action, filter) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        filters : filter
	      }
	    });
	    return request;
	  };

	  ConciliacionBancariaOURService.getRequestParamsObject = function (action, paramsObject) {
	    var request = $http({
	      method : 'POST',
	      url : url,
	      data : {
	        action : action,
	        service : service,
	        paramsObject : paramsObject
	      }
	    });
	    return request;
	  };

		// LLamada al servicio que recuperará los Our correspondientes a apuntes auditados
	  ConciliacionBancariaOURService.consultarOurConTheir = function (successHandler, errorHandler, paramsObject) {
			var request = ConciliacionBancariaOURService.getRequestParamsObject('consultarOurConTheir', paramsObject);
			request.success(successHandler).error(errorHandler);
			return request;
		};
		
		// LLamada al servicio que genera apunte
		ConciliacionBancariaOURService.generarApuntes = function (successHandler, errorHandler, getRequestParamsObject) {
			    var request = ConciliacionBancariaOURService.getRequestParamsObject('generarApuntes', getRequestParamsObject);
			    request.success(successHandler).error(errorHandler);
			    return request;
			  };
			  
			  
		ConciliacionBancariaOURService.consultarOurConTheirBis =  function (successHandler, errorHandler, paramsObject, listaFechas) {
	
			var $d = $q.defer();
			var promesas = [];
	
			for (var i = 0; i < listaFechas.length; i++) {
				
				promesas.push($http({
					method : 'POST',
					url : url,
					data : {
						action : 'consultarOurConTheir',
						service : service,
						paramsObject : paramsObject,
						filters:  {"fecha": listaFechas[i]}
					}
				}));
			}
	
			$q.all(promesas).then(function (promesasRes) {
				dataRes = {resultados : {
						listaOurConTheir: [],
						status: '',
						timeout: ''
					}
				};
				var listData = [];
				for(var j=0;j<promesasRes.length;j++){
					if(promesasRes[j].data.resultados.status === 'KO'){
						dataRes.resultados.status = promesasRes[j].data.resultados.status;
					}else{ 
						if(promesasRes[j].data.resultados.timeout){
							dataRes.resultados.timeout = promesasRes[j].data.resultados.timeout;
						}else{
							if(promesasRes[j].data.resultados.listaOurConTheir) {
                                for (var i = 0; i < promesasRes[j].data.resultados.listaOurConTheir.length; i++) {
                                    dataRes.resultados.listaOurConTheir.push(promesasRes[j].data.resultados.listaOurConTheir[i]);
                                }
                            }
						}
					}
					
				}
				if(dataRes.resultados.timeout){
					dataRes.resultados.listaOurConTheir = [];
					successHandler(dataRes)
				}else{
				    successHandler(dataRes);
				}
			});
	
			return $d.promise;
	  }
		
	  return ConciliacionBancariaOURService;
							
}]);
