'use restrict';
sibbac20.factory('FacturasManualesService', [ '$http', function ($http) {
  var FacturasManualesService = {};
  var url = server.url;
  var service = server.service.facturasmanuales.facturasManuales;

  FacturasManualesService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  FacturasManualesService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que obtiene los de todas las plantillas de informes filtrados.
  FacturasManualesService.consultarFacturasManuales = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('consultarFacturasManuales', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };


  // LLamada al servicio que obtiene los datos del combo de campos detalle
  FacturasManualesService.crearFacturaManual = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('crearFacturaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // Llamada al servicio
  FacturasManualesService.modificarFacturaManual = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('modificarFacturaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que eliminará un registro fisico de factura manual
  FacturasManualesService.borrarFacturasManuales = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('borrarFacturasManuales', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene un detalle de una factura
  FacturasManualesService.detalleFacturaManual = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('detalleFacturaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  FacturasManualesService.consultarCliente = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('consultarCliente', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  FacturasManualesService.contabilizar = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('contabilizar', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  FacturasManualesService.modificarContabilizar = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('modificarContabilizar', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  FacturasManualesService.crearContabilizar = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('crearContabilizar', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  FacturasManualesService.consultarProximoNumeroFacturaManual = function (successHandler, errorHandler, paramsObject) {
    var request = FacturasManualesService.getRequestParamsObject('consultarProximoNumeroFacturaManual', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };
  
  FacturasManualesService.consultarProximoNumeroFacturaManualGrupoIva = function (successHandler, errorHandler, paramsObject) {
	    var request = FacturasManualesService.getRequestParamsObject('consultarProximoNumeroFacturaManualGrupoIva', paramsObject);
	    request.success(successHandler).error(errorHandler);
	    return request;
	  };

  // Llamada al servicio que exportará a PDF la factura seleccionada
  FacturasManualesService.exportarPdf = function (successHandler, errorHandler, paramsObject) {
	var request = FacturasManualesService.getRequestParamsObject('exportarPdf', paramsObject);
	request.success(successHandler).error(errorHandler);
	return request;
  };

  return FacturasManualesService;

} ]);
