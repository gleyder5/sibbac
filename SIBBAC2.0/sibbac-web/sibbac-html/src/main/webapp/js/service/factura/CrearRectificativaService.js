sibbac20.factory('CrearRectificativaService', ['$http', '$rootScope', function ($http, $rootScope) {
        var CrearRectificativaService = {};
        var serverUrl = server.url;
        var fsService = server.service.facturas.crearRectificativa;
        //var fsService = 'SIBBACServiceFactura';
        CrearRectificativaService.getListadoFacturasEnviadas = function (successHandler, errorHandler, filtro) {
        	var request =$http({
                url: serverUrl,
                data: {service: fsService, action: "getListadoFacturasEnviadas", filters: filtro},
                method: 'POST'
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
        	return request;
        };
        return CrearRectificativaService;
    }]);

