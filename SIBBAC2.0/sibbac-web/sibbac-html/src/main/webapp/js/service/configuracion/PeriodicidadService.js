'use strict';
sibbac20.factory('PeriodicidadService', ['$http', function($http){
        var PeriodicidadService = {};
        var serverUrl = server.url;
        var fService = server.service.configuracion.periodicidadService;
        PeriodicidadService.getReglasAndParams = function(successHandler, errorHandler){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'getReglasAndParams'}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.createParametrizacion = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'createParametrizacion', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.updateRegla = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'updateRegla', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.updateParametrizacion = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'updateParametrizacion', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.createRegla = function (successHandler, errorHandler, filter) {
           	var request =$http({
                url: serverUrl,
                data : {service : fService, action : 'createRegla', filters : filter},
                method: 'POST'
           	});
           	if(successHandler !== undefined && errorHandler !== undefined){
           		request.success(successHandler).error(errorHandler);
           	}
           	return request;
        };

        PeriodicidadService.updateActivoRegla = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'updateActivoRegla', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.updateActivoParametrizacion = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'updateActivoParametrizacion', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.deleteRegla = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'deleteRegla', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.getDiasSemana = function(successHandler, errorHandler){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'getDiasSemana'}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.getMeses = function(successHandler, errorHandler){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'getMeses'}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        PeriodicidadService.deleteParametrizacion = function(successHandler, errorHandler, filter){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'deleteParametrizacion', filters : filter}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        return PeriodicidadService;
}]);
