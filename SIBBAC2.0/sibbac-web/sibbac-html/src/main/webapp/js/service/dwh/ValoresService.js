(function (sibbac20, angular, console) {
    "use strict";
    sibbac20.factory('ValoresService', ['$resource', function ($resource) {
        var baseUrl, queryRes, queryResPost, queryResPut, queryResDel, queryResValoresGrupo, queryResGruposValores, queryResMonedas, queryResPaises;
        baseUrl = "/sibbac20back/rest/datosMaestros/valores";
        // Llamada a back con POST, permite los parámetros queryName y detail
        queryRes = $resource(baseUrl + "/:queryName/:detail",
            { queryName: "@queryName", detail: "@detail" }, { execute: { method: "POST", isArray: false } });
        // Llamada a back con POST
        queryResPost = $resource(baseUrl, {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con PUT
        queryResPut = $resource(baseUrl, {}, { execute: { method: "PUT", isArray: false } });
        // Llamada a back con DELETE, permite el parámetro ids
        queryResDel = $resource(baseUrl + '/delete', {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con GET, que devuelve el listado valores grupo
        queryResValoresGrupo = $resource(baseUrl + '/getValoresGrupo', {}, { execute: { method: "GET", isArray: true } });
        // Llamada a back con GET, que devuelve el listado grupos valores
        queryResGruposValores = $resource(baseUrl + '/getGruposValores', {}, { execute: { method: "GET", isArray: true } });
        // Llamada a back con GET, que devuelve el listado de monedas / divisas
        queryResMonedas = $resource(baseUrl + '/getMonedas', {}, { execute: { method: "GET", isArray: true } });
        // Llamada a back con GET, que devuelve el listado de paises
        queryResPaises = $resource(baseUrl + '/getPaises', {}, { execute: { method: "GET", isArray: true } });
        return {
            baseUrl: baseUrl,
            queries: queryRes.query,
            // Llamada a /filters
            dynamicFilters: function (name) {
                return queryRes.query({ queryName: name, detail: "filters" });
            },
            // Llamada a /columns
            columns: function (name, success) {
                var res = queryRes.query({ queryName: name, detail: "columns" }, function () {
                    success(res);
                });
            },

            // Llamada al /POST que devuelve los datos de la tabla
            executeQuery: function (queryName, filter, success, fail) {
                var data;
                filter.queryName = queryName;
                data = queryRes.query(filter, function () {
                    if (data.length > 0 && data[0].error !== undefined) {
                        fail(data[0].error);
                        return;
                    }
                    success(data);
                }, fail);
            }, 
            // Llamada al /POST que devuelve los datos de la tabla
            executeAction: function (queryName, params, items, success, fail) {
                var body = params;
                queryRes.execute({ queryName: queryName }, body, success, fail);
            },
            // Llamada al /POST que hace un save de los datos mandados por parámetro
            executeActionPOST: function (params, items, success, fail) {
                var body = params;
                queryResPost.execute(body, success, fail);
            },
            // Llamada al /PUT que hace un edit de los datos mandados por parámetro
            executeActionPUT: function (params, items, success, fail) {
                var body = params;
                queryResPut.execute(body, success, fail);
            },
            // Llamada al /DELETE que realiza una eliminación por id de la lista enviada por parámetro
            executeActionDEL: function (params, items, success, fail) {
                var body = params;
                queryResDel.execute(body, success, fail);
            },
            // Llamada a back con GET, que devuelve el listado valores grupo
            executeGetValoresGrupo: function (success, fail) {
                queryResValoresGrupo.execute(success, fail);
            },
            // Llamada a back con GET, que devuelve el listado grupos valores
            executeGetGruposValores: function (success, fail) {
                queryResGruposValores.execute(success, fail);
            },
            // Llamada a back con GET, que devuelve el listado de monedas/divisas
            executeGetMonedas: function (success, fail) {
                queryResMonedas.execute(success, fail);
            },
            // Llamada a back con GET, que devuelve el listado de paises
            executeGetPaises: function (success, fail) {
                queryResPaises.execute(success, fail);
            },
            autocompleter: function (field) {
                return queryRes.query({ queryName: "autocompleter", detail: field });
            }
        }
    }]);
})(sibbac20, angular, console);

