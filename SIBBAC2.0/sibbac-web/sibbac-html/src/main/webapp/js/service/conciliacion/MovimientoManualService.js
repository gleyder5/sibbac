'use strict';
sibbac20.factory('MovimientoManualService', ['$http', function($http){
        var MovimientoManualService = {};
        var serverUrl = server.url;
        var fService = server.service.conciliacion.movimientoManualService;
        MovimientoManualService.insertMovimientoManual = function(successHandler, errorHandler, parameters){
        	var request = $http({
                url : serverUrl,
                method : 'POST',
                data : {service : fService, action : 'insertMovimientoManual', params : parameters}
            });
            if(successHandler !== undefined && errorHandler !== undefined){
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        return MovimientoManualService;
}]);
