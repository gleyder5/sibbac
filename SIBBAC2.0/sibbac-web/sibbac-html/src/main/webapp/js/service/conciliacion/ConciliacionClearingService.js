sibbac20.factory('ConciliacionClearingService', ['$http', '$rootScope', function ($http, $rootScope) {
        var ConciliacionClearingService = {};
        var url = $rootScope.restParams.serverUrl;
        var service = server.service.conciliacion.clearing;
        ConciliacionClearingService.getDiferenciasS3 = function (params, successHandler, errorHandler) {
            $http({
                method: 'POST',
                data: {action: 'getDiferenciasS3', service: service, filters: params},
                url: url
            }).success(function (data, status, headers, config) {
                if (data !== null && data.error !== null) {
                    errorHandler(data, status, headers, config);
                } else if (data !== null && data.resultados !== null && data.resultados !== null &&
                        data.resultados.total !== null) {
                    var datosT = data.resultados.total;
                    var fecha = transformaFecha(data.resultados.newestDate) + " " + data.resultados.newestHour;
                    successHandler( datosT, fecha);
                }
            }).error(errorHandler);
        };
        ConciliacionClearingService.getMovimientoUnificadoEfectivo = function (successHandler, errorHandler, filter) {
            $http({
                url: url,
                method: 'POST',
                data: {service: service,
                    action: 'getMovimientoUnificadoEfectivo', filters: filter}
            }).success(successHandler).error(errorHandler);
        };
        ConciliacionClearingService.getBalanceMovimientosCuentaVirtual = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: server.url,
                method: 'POST',
                data: {service: service, action: 'getBalanceMovimientosCuentaVirtual', filters: filter}
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        ConciliacionClearingService.getInitialFliquidacionBetween = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: server.url,
                method: 'POST',
                data: {service: service,
                    action: 'getInitialFliquidacionBetween', filters: filter === undefined ? {} : filter}
            });
            request.success(successHandler).error(errorHandler);
            return request;
        };
        ConciliacionClearingService.getDesgloseCamaraBySentidoCuentasClearing = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: server.url,
                method: 'POST',
                data: {service: service, action: 'getDesgloseCamaraBySentidoCuentasClearing', filters: filter}
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        ConciliacionClearingService.getBalanceMovimientosCuentaVirtualPorIsin = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: url,
                method: 'POST',
                data: {service: service, action: 'getBalanceMovimientosCuentaVirtualPorIsin',
                    filters: filter}
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        ConciliacionClearingService.getConciliacionTitulosER = function (params, successHandler, errorHandler) {
            $http({
                method: 'POST',
                data: {action: 'getConciliacionTitulosER', service: service, filters: params},
                url: url
            }).success(function (data, status, headers, config) {
                if (data !== null && data.error !== null) {
                    errorHandler(data, status, headers, config);
                } else if (data !== null && data.resultados !== null && data.resultados !== null ) {
                    var datosT = data.resultados.data;
                    var fecha = "";
                    successHandler( datosT, fecha);
                }
            }).error(errorHandler);
        };
        ConciliacionClearingService.getConciliacionEfectivoER = function (params, successHandler, errorHandler) {
          $http({
              method: 'POST',
              data: {action: 'getConciliacionEfectivoER', service: service, filters: params},
              url: url
          }).success(function (data, status, headers, config) {
              if (data !== null && data.error !== null) {
                  errorHandler(data, status, headers, config);
              } else if (data !== null && data.resultados !== null && data.resultados !== null ) {
                  var datosT = data.resultados.data;
                  var fecha = "";
                  successHandler( datosT, fecha);
              }
          }).error(errorHandler);
      };
        return ConciliacionClearingService;
    }]);
