'use restrict';
sibbac20.factory('GestionUsuariosService', [ '$http', function ($http) {
  var GestionUsuariosService = {};
  var url = server.url;
  var service = server.service.gestionAccesos.gestionUsuarios;

  GestionUsuariosService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  GestionUsuariosService.getRequestFilters = function (action, filters) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        filters : filters
      }
    });
    return request;
  };

  GestionUsuariosService.getRequestParams = function (action, params) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        params : params
      }
    });
    return request;
  };

  GestionUsuariosService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que busca los usuarios que coincidan con los filtros
  GestionUsuariosService.buscaUsuarios = function (successHandler, errorHandler, paramsObject) {
    var request = GestionUsuariosService.getRequestParamsObject('BuscarUsuarios', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que borra los usuarios que coincidan con los filtros
  GestionUsuariosService.eliminarUsuarios = function (successHandler, errorHandler, paramsObject) {
    var request = GestionUsuariosService.getRequestParamsObject('BorrarUsuarios', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que borra los usuarios que coincidan con los filtros
  GestionUsuariosService.resetearPassword = function (successHandler, errorHandler, paramsObject) {
    var request = GestionUsuariosService.getRequestParamsObject('ResetearPassword', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que modifica los datos del usuario estableciendo los pasados en el filtros
  GestionUsuariosService.modificarUsuario = function (successHandler, errorHandler, paramsObject) {
    var request = GestionUsuariosService.getRequestParamsObject('ModificarUsuario', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que crea un usuario dados los datos pasados en el filtros
  GestionUsuariosService.crearUsuario = function (successHandler, errorHandler, paramsObject) {
    var request = GestionUsuariosService.getRequestParamsObject('CrearUsuario', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los usuarios
  GestionUsuariosService.cargaUsuarios = function (successHandler, errorHandler) {
    var request = GestionUsuariosService.getRequest('RecuperarUsuarios');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los roles
  GestionUsuariosService.cargaPerfiles = function (successHandler, errorHandler) {
    var request = GestionUsuariosService.getRequest('RecuperarPerfiles');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  return GestionUsuariosService;
} ]);
