'use restrict';
sibbac20.factory('ContaInformesService', ['$http', function ($http) {
        var ContaInformesService = {};
        var url = server.url;
        var service = server.service.contabilidad.contaInformes;

        ContaInformesService.getRequest = function (action, filter) {
            var request = $http({
                method: 'POST',
                url: url,
                data: {action: action, service: service, filters: filter}
            });
            return request;
        };

        ContaInformesService.getRequestParams = function (action, params) {
	        var request = $http({
	            method: 'POST',
	            url: url,
	            data: {action: action, service: service, params: params}
	        });
	        return request;
    	};

    	return ContaInformesService;
}]);

