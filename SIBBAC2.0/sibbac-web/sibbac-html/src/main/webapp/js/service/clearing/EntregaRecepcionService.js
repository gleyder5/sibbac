'use restrict';
sibbac20.factory('EntregaRecepcionService', ['$http', function ($http) {
        var EntregaRecepcionService = {};
        var url = server.url;
        var service = server.service.clearing.entregaRecepcion;

        EntregaRecepcionService.getRequest = function (action, filter) {
            var request = $http({
                method: 'POST',
                url: url,
                data: {action: action, service: service, filters: filter}
            });
            return request;
        };

        EntregaRecepcionService.getRequestParams = function (action, params) {
	        var request = $http({
	            method: 'POST',
	            url: url,
	            data: {action: action, service: service, params: params}
	        });
	        return request;
    	};

        EntregaRecepcionService.ListaEntregaRecepcion = function (successHandler, errorHandler, filter) {
            var request = EntregaRecepcionService.getRequest('ListaEntregaRecepcion', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        EntregaRecepcionService.ListaLiquidacionS3 = function (successHandler, errorHandler, filter) {
            var request = EntregaRecepcionService.getRequest('ListaLiquidacionS3', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        EntregaRecepcionService.listaErroresS3 = function (successHandler, errorHandler, filter) {
            var request = EntregaRecepcionService.getRequest('listaErroresS3', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        EntregaRecepcionService.EstadosClearing = function (successHandler, errorHandler, filter) {
            var request = EntregaRecepcionService.getRequest('EstadosClearing', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        EntregaRecepcionService.CancelacionCliente = function (successHandler, errorHandler, params) {
            var request = EntregaRecepcionService.getRequestParams('CancelacionCliente', params);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        EntregaRecepcionService.CancelacionTotal = function (successHandler, errorHandler, params) {
            var request = EntregaRecepcionService.getRequestParams('CancelacionTotal', params);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        EntregaRecepcionService.reenvio = function(successHandler, errorHandler, params) {
        	var request = EntregaRecepcionService.getRequestParams('Reenvio', params);
        	request.success(successHandler).error(errorHandler);
        	return request;
        }
        EntregaRecepcionService.CifSvb = function (successHandler, errorHandler, params) {
            var request = EntregaRecepcionService.getRequestParams('CifSvb', params);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        EntregaRecepcionService.CuentasCompensacion = function (successHandler, errorHandler, params) {
            var request = EntregaRecepcionService.getRequestParams('CuentasCompensacion', params);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        return EntregaRecepcionService;
    }]);

