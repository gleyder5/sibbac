(function(angular, sibbac20, undefined){
  
  "strict mode";

  sibbac20.factory("BusquedaAlosDesgloseManualService", ["$resource", "$http", function($resource, $http){
    var baseUrl, queryRes, lastAlosFilter = null;
    baseUrl = "/sibbac20back/rest/desglose/manual";
    queryRes = $resource(baseUrl + "/:nuorden/:nbooking/:nucnfclt/:queryName/:detail", {
          nuorden: "@nuorden", nbooking: "@nbooking", nucnfclt: "@nucnfclt",
          queryName: "@queryName", detail:"@detail"
        }, {consultaArray: {method: "POST", isArray: true}});
    
    queryHolderAutoCompleter = $resource(baseUrl + "/holderAutocompleter/:holderData/:fechaContratacion", 
    		{holderData: "@holderData", fechaContratacion: "@fechaContratacion"});
    
    queryHolderFinder = $resource(baseUrl + "/holder/:holderData/:fechaContratacion", 
    		{holderData: "@holderData", fechaContratacion: "@fechaContratacion"});
    return {
      queries: function() { return []; },
      dynamicFilters: function(queryName) {
        return queryRes.query({queryName: queryName, detail: "filters"});  
      },
      columns: function(queryName, success) {
        var res = queryRes.query({queryName: queryName, detail: "columns"}, function(){
          success(res);
        });
      },
      executeQuery: function(queryName, filter, success, fail) {
        var data;
        filter.queryName = queryName;
        data = queryRes.query(filter, function() {
          if(data.length > 0 && data[0].error !== undefined) {
            fail(data[0].error);
            return;
          }
          if(queryName === "alos") {
            lastAlosFilter = filter;
          }
          success(data);
        }, fail);
      },
      consultaDesglose: function(aloId, success, fail) {
        var data = queryRes.get(aloId, function() {
          if(data) {
            if(data.error) {
              fail(data.error);
              return
            }
            success(data);
            return
          }
          fail("Sin datos");
        }, fail);
      },
      autocompleter: function(field) {
        return queryRes.query({queryName: "autocompleter", detail: field});
      },
      upload: function(fileName, data, success, fail) {
        $http({url: baseUrl + "/upload/" + fileName,  method: 'PUT', data: data, transformRequest: []}).then(success, fail);
      },
      listAlgoritmoDesglose: function(){
    	  return queryRes.query({queryName: "list", detail: "algoritmoDesglose"});
      },
      listCuentasCompensacion : function (){
    	  return queryRes.query({queryName: "autocompleter", detail: "cuentaCompensacion"}); 
      },
      listMiembrosCompensadores : function (){
    	  return queryRes.query({queryName: "autocompleter", detail: "miembroCompensador"}); 
      },
      listReglasAsignacion : function (){
    	  return queryRes.query({queryName: "autocompleter", detail: "reglasAsignacion"}); 
      },
      listClearersNacional : function (){
    	  return queryRes.query({queryName: "autocompleter", detail: "clearersNacional"}); 
      },
      listAlcsBaja: function(listAlosSeleccionados, success, fail){
    	  return queryRes.consultaArray( { queryName: "alcs", detail : "baja"}, listAlosSeleccionados, success, fail );
      },
      listSolicitudDesgloseAlgoritmo: function(algoritmo, listAlosSeleccionados, success, fail){
    	  return queryRes.consultaArray( { queryName: "listSolicitudDesglose", detail : algoritmo}, listAlosSeleccionados, success, fail );
      },
      holderAutocompleter: function (holderData, fechaContratacion, success, fail){
    	return queryHolderAutoCompleter.query({holderData : holderData, fechaContratacion : fechaContratacion}, success, fail);
      },
      getHolder: function (holder, fechaContratacion, success, fail){
    	  return queryHolderFinder.query({holder : holder, fechaContratacion : fechaContratacion},success, fail);
      },
      desglosar : function (listSolicitudesDesglose, success, fail){
    	  return queryRes.save( { queryName: "desglosar", detail : ""}, listSolicitudesDesglose, success, fail );
      },
      getLastAlosFilter: function() {
        return lastAlosFilter;
      },
      resetLastAlosFilter: function() {
        lastAlosFilter = null;
      }
    };
  }]);

})(angular, sibbac20)