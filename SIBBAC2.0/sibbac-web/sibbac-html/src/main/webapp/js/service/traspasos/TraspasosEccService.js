(function(sibbac20, angular, console) {
    "use strict";
    sibbac20.factory("TraspasosEccService",["$resource",function($resource){ 
    	var baseUrl, queryRes;
    	baseUrl = "/sibbac20back/rest/traspasos/ecc";
    	queryRes = $resource(baseUrl + "/:queryName/:detail", 
    			{queryName: "@queryName", detail:"@detail"}, {execute: {method: "POST", isArray: false}});
    	return {
          baseUrl: baseUrl,
    	  queries: queryRes.query,
          dynamicFilters: function(name) {
        	return queryRes.query({queryName: name, detail: "filters"});
          },
          columns: function(name, success) {
            var res = queryRes.query({queryName: name, detail: "columns"}, function(){
            	success(res);
            });
          },
          executeQuery: function(queryName, filter, success, fail) {
        	var data;
        	filter.queryName = queryName;
        	data = queryRes.query(filter, function() {
        		if(data.length > 0 && data[0].error !== undefined) {
        			fail(data[0].error);
        			return;
        		}
        		success(data);
        	}, fail);
          },
          executeAction: function(queryName, params, items, success, fail) {
        	var body = {params: params, items: items};
        	queryRes.execute({queryName : queryName}, body, success, fail);
          },
          autocompleter: function(field) {
        	  return queryRes.query({queryName: "autocompleter", detail: field});
          }
       }
    }]);
})(sibbac20, angular, console);