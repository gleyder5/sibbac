(function (sibbac20, angular, console) {
    "use strict";
    sibbac20.factory('ConfiguracionClientesPBCService', ['$resource', function ($resource) {
        var baseUrl, queryRes, queryResPost, queryResPut, queryResDel, queryResPutFecha, queryResGetListEfectivo, queryResGetListDocumentacion, queryResGetListAnalisis, queryResGetDatosEfectivo, queryResGetDatosKGR, queryResGetListadoHistorico, queryResGetListadoObservaciones, queryResGetComboObservaciones, queryResGetControlAnalisis;
        baseUrl = "/sibbac20back/rest/gestionClientes/configuracionClientesPBC";
        // Llamada a back con POST, permite los parámetros queryName y detail
        queryRes = $resource(baseUrl + "/:queryName/:detail",
            { queryName: "@queryName", detail: "@detail" }, { execute: { method: "POST", isArray: false } });
        // Llamada a back con POST
        queryResPost = $resource(baseUrl, {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con PUT
        queryResPut = $resource(baseUrl, {}, { execute: { method: "PUT", isArray: false } });
        // Llamada a back con DELETE, permite el parámetro ids
        queryResDel = $resource(baseUrl + '/delete', {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con PUT para actualizar la fecha
        queryResPutFecha = $resource(baseUrl + "/updateFechaRevision", {}, { execute: { method: "PUT", isArray: false } });
        // Llamada a back con GET para rellenar combobox listado Efectivo
        queryResGetListEfectivo = $resource(baseUrl + "/listadoEfectivo", {}, { execute: { method: "GET", isArray: true } });
        // Llamada a back con GET para rellenar combobox listado Documentacion
        queryResGetListDocumentacion = $resource(baseUrl + "/listadoDocumentos", {}, { execute: { method: "GET", isArray: true } });
        // Llamada a back con GET para rellenar combobox listado Analisis
        queryResGetListAnalisis = $resource(baseUrl + "/listadoAnalisis", {}, { execute: { method: "GET", isArray: true } });
        // Llamada a back con GET para rellenar input relacionados con efectivo en los modales
        queryResGetDatosEfectivo = $resource(baseUrl + "/getDatosEfectivo/:id", {id: "@id"}, { execute: { method: "GET", isArray: false } });
        // Llamada a back con GET para rellenar input relacionados con efectivo en los modales
        queryResGetDatosKGR = $resource(baseUrl + "/getDatosKGR", {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con GET para rellenar input relacionados con efectivo en los modales
        queryResGetListadoHistorico = $resource(baseUrl + "/listadoHistorico", {}, { execute: { method: "POST", isArray: true } });
        // Llamada a back con GET para rellenar el grid de observaciones
        queryResGetListadoObservaciones = $resource(baseUrl + "/listadoObservacionesCliente", {}, { execute: { method: "POST", isArray: true } });
        // Llamada a back con GET para rellenar el combo para nuevas observaciones
        queryResGetComboObservaciones = $resource(baseUrl + "/listadoComboObservaciones", {}, { execute: { method: "GET", isArray: true } });
        // Llamada a back con GET para rellenar el control de analisis
        queryResGetControlAnalisis = $resource(baseUrl + "/getControlAnalisis/:id", { id: "@id" }, { execute: { method: "GET", isArray: false } });
        return {
            baseUrl: baseUrl,
            queries: queryRes.query,
            // Llamada a /filters
            dynamicFilters: function (name) {
                return queryRes.query({ queryName: name, detail: "filters" });
            },
            // Llamada a /columns
            columns: function (name, success) {
                var res = queryRes.query({ queryName: name, detail: "columns" }, function () {
                    success(res);
                });
            },
            
            // Llamada al /POST que devuelve los datos de la tabla
            executeQuery: function (queryName, filter, success, fail) {
                var data;
                filter.queryName = queryName;
                data = queryRes.query(filter, function () {
                    if (data.length > 0 && data[0].error !== undefined) {
                        fail(data[0].error);
                        return;
                    }
                    success(data);
                }, fail);
            },
            // Llamada al /POST que devuelve los datos de la tabla
            executeAction: function (queryName, params, items, success, fail) {
                var body = params;
                queryRes.execute({ queryName: queryName }, body, success, fail);
            },
            // Llamada al /POST que hace un save de los datos mandados por parámetro
            executeActionPOST: function (params, items, success, fail) {
                var body = params;
                queryResPost.execute(body, success, fail);
            },
            // Llamada al /PUT que hace un edit de los datos mandados por parámetro
            executeActionPUT: function (params, items, success, fail) {
                var body = params;
                queryResPut.execute(body, success, fail);
            },
            // Llamada al /DELETE que realiza una eliminación por id de la lista enviada por parámetro
            executeActionDEL: function (params, items, success, fail) {
                var body = params;
                queryResDel.execute(body, success, fail);
            },
            // Llamada al /PUT que actualiza las fecha de revision
            executeActionPUTfecha: function (items, success, fail) {
                queryResPutFecha.execute(success, fail);
            },
            // Llamada al /GET que carga el combobox de listado efectivo
            executeActionGETListadoEfectivoCombobox: function (items, success, fail) {
                return queryResGetListEfectivo.execute(success, fail);
            },
            // Llamada al /GET que carga el combobox de listado documentacion
            executeActionGETListadoDocumentacionCombobox: function (items, success, fail) {
                return queryResGetListDocumentacion.execute(success, fail);
            },
            // Llamada al /GET que carga el combobox de listado efectivo
            executeActionGETListadoAnalisisCombobox: function (items, success, fail) {
                return queryResGetListAnalisis.execute(success, fail);
            },
            // Llamada al /GET que carga el combobox de listado efectivo
            executeActionGETDatosEfectivo: function (params, success, fail) {
                return queryResGetDatosEfectivo.execute({ id: params }, success, fail);
            },
            // Llamada al /GET que carga el input de codigo KGR
            executeActionGETDatosKGR: function (params, items, success, fail) {
                var body = params;
                return queryResGetDatosKGR.execute(body, success, fail);
            },
            // Llamada al /GET que carga listado historico de un cliente
            executeActionGETListadoHistorico: function (params, items, success, fail) {
                var body = params;
                return queryResGetListadoHistorico.execute(body, success, fail);
            },
            // Llamada al /GET que carga listado de observaciones de un cliente
            executeActionGETListadoObservaciones: function (params, items, success, fail) {
                var body = params;
                return queryResGetListadoObservaciones.execute(body, success, fail);
            },
            // Llamada al /GET que carga el combo de observaciones 
            executeActionGETComboObservaciones: function (success, fail) {
                return queryResGetComboObservaciones.execute(success, fail);
            },
            // Llamada al /GET que carga el control de analisis 
            executeActionGETControlAnalisis: function (params, success, fail) {
                return queryResGetControlAnalisis.execute({ id: params}, success, fail);
            },
            autocompleter: function (field) {
                return queryRes.query({ queryName: "autocompleter", detail: field });
            }
        }
    }]);
})(sibbac20, angular, console);

