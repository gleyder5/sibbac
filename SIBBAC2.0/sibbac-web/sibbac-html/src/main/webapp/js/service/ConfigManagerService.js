sibbac20.factory('ConfigManagerService', ['$http', function($http){
        var ConfigManagerService = {};
        var url = server.url;
        var service = server.service.url.manager;
        ConfigManagerService.getSibbacUrl = function(successHandler, errorHandler, filter){
            var request = $http({
                url : url,
                method : 'POST',
                data : {service : service, action : 'getSibbacUrl',
                filters : filter}
            });
            request.success(successHandler).error(errorHandler);
            return request;
        };
        return ConfigManagerService;
}]);


