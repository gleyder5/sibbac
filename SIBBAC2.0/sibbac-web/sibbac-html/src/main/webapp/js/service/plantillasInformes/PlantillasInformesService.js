'use restrict';
sibbac20.factory('PlantillasInformesService', [ '$http', function ($http) {
  var PlantillasInformesService = {};
  var url = server.url;
  var service = server.service.plantillasInformes.gestionPlantillasInformes;

  PlantillasInformesService.getRequest = function (action) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
      }
    });
    return request;
  };

  PlantillasInformesService.getRequestParamsObject = function (action, paramsObject) {
    var request = $http({
      method : 'POST',
      url : url,
      data : {
        action : action,
        service : service,
        paramsObject : paramsObject
      }
    });
    return request;
  };

  // LLamada al servicio que obtiene los de todas las plantillas de informes filtrados.
  PlantillasInformesService.cargaPlantillasInformes = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesService.getRequestParamsObject('CargaPlantillasInformes', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos del combo de campos detalle
  PlantillasInformesService.cargaCamposDetalle = function (successHandler, errorHandler) {
    var request = PlantillasInformesService.getRequest('CargaCamposDetalle');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos del combo de plantillas del filtro
  PlantillasInformesService.cargaPlantillas = function (successHandler, errorHandler) {
    var request = PlantillasInformesService.getRequest('CargaPlantillas');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  PlantillasInformesService.cargaIsin = function (successHandler, errorHandler) {
    var request = PlantillasInformesService.getRequest('CargaIsin');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de los combos alias e isin
  PlantillasInformesService.cargaAlias = function (successHandler, errorHandler) {
    var request = PlantillasInformesService.getRequest('CargaAlias');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  // LLamada al servicio que obtiene los datos de combos estaticos
  PlantillasInformesService.cargaSelectsEstaticos = function (successHandler, errorHandler) {
    var request = PlantillasInformesService.getRequest('CargaSelectsEstaticos');
    request.success(successHandler).error(errorHandler);
    return request;
  };

  PlantillasInformesService.crearPlantilla = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesService.getRequestParamsObject('CrearPlantilla', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  PlantillasInformesService.modificarPlantilla = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesService.getRequestParamsObject('ModificarPlantilla', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };

  PlantillasInformesService.eliminarPlantillas = function (successHandler, errorHandler, paramsObject) {
    var request = PlantillasInformesService.getRequestParamsObject('EliminarPlantillas', paramsObject);
    request.success(successHandler).error(errorHandler);
    return request;
  };


  return PlantillasInformesService;
} ]);
