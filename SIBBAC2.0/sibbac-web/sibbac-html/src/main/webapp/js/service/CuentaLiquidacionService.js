sibbac20.factory('CuentaLiquidacionService', ['$http', function($http){
    var CuentaLiquidacionService = {};
        var url = server.url;
        var serviceCL = server.service.conciliacion.cuentaLiquidacion;
        var serviceConCle =server.service.conciliacion.clearing;
	  CuentaLiquidacionService.getCuentasLiquidacion = function() {
		    return $http({
		    	method: 'POST',
		    	url: url,
		    	data : {service : serviceConCle, action : 'getCuentasLiquidacion'},
		    	cache : true
		    });
	  };
          CuentaLiquidacionService.getCuentasLiquidacionClearing = function(){
              return $http({
		    	method: 'POST',
		    	url: url,
		    	data : {service : serviceConCle, action : 'getCuentasLiquidacionClearing'},
		    	cache : true
		    });
          };
          CuentaLiquidacionService.getCuentasLiquidacionClearingNoPropiaNoIndividual = function(){
              return $http({
		    	method: 'POST',
		    	url: url,
		    	data : {service : serviceConCle, action : 'getCuentasLiquidacionClearingNoPropiaNoIndividual'},
		    	cache : true
		    });
          };

          function getCLRequest(action, filter){
              return $http({
                  method: 'POST',
                  url: url,
                  data: {service : serviceCL, action: action, filters: filter}
              });
          }
          CuentaLiquidacionService.getCuentaLiquidacion = function(successHandler, errorHandler, filter){
              var request = getCLRequest('getCuentaLiquidacion', filter);
              if(successHandler !== undefined && errorHandler !== undefined){
                  request.success(successHandler).error(errorHandler);
              }
              return request;
          };
          CuentaLiquidacionService.getCuentasMercado = function(successHandler, errorHandler, filter){
              var request = getCLRequest('getCuentasMercado', filter);
              if(successHandler !== undefined && errorHandler !== undefined){
                  request.success(successHandler).error(errorHandler);
              }
              return request;
          };
          CuentaLiquidacionService.getCuentaRelacionada = function(successHandler, errorHandler, filter){
              var request = getCLRequest('getCuentaRelacionada', filter);
              if(successHandler !== undefined && errorHandler !== undefined){
                  request.success(successHandler).error(errorHandler);
              }
              return request;
          };
          CuentaLiquidacionService.getTipoCuenta = function(successHandler, errorHandler, filter, idComponent){
              var request = getCLRequest('getTipoCuenta', filter);
              if(successHandler !== undefined && errorHandler !== undefined){
                  request.success(function(data){
                      successHandler(data, idComponent);
                  }).error(errorHandler);
              }
              return request;
          };
          CuentaLiquidacionService.getTipoNeteo = function(successHandler, errorHandler, filter, idComponent){
              var request = getCLRequest('getTipoNeteo', filter);
              request.cache = true;
              if(successHandler !== undefined && errorHandler !== undefined){
                  request.success(function(data){
                      successHandler(data, idComponent);
                  }).error(errorHandler);
              }
              return request;
          };
          CuentaLiquidacionService.getTipoFichero = function(successHandler, errorHandler, filter, idComponent){
              var request = getCLRequest('getTipoFichero', filter);
              request.cache = true;
              if(successHandler !== undefined && errorHandler !== undefined){
                  request.success(function(data){
                      successHandler(data, idComponent);
                  }).error(errorHandler);
              }
              return request;
          };
          CuentaLiquidacionService.getSistemasLiquidacion = function(successHandler, errorHandler, filter, idComponent){
              var request = getCLRequest('getSistemasLiquidacion', filter);
              request.cache = true;
              if(successHandler !== undefined && errorHandler !== undefined){
                  request.success(function(data){
                      successHandler(data, idComponent);
                  }).error(errorHandler);
              }
              return request;
          };
          CuentaLiquidacionService.getCuentasLiquidacionFiltrado = function(filter) {
        	  var request = getCLRequest('getCuentaLiquidacion', filter);
              return request;
  	 	  };

  	 	CuentaLiquidacionService.getCuentasLiquidacionFiltrado = function(filter){
            return $http({
		    	method: 'POST',
		    	url: url,
		    	data : {service : serviceConCle, action : 'getCuentasLiquidacionFiltrado', filters: filter},
		    	cache : true
		    });
        };
	  return CuentaLiquidacionService;
}]);

