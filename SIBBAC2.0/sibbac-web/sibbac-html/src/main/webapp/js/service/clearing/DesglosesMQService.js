'use strict';
sibbac20.factory('DesglosesMQService', ['$http', function($http){

	var DesglosesMQService = {};
	var serverUrl = server.url;
	var service = server.service.clearing.desgloses;

	function getRequest(action, filtro){
        var request = $http({
            url : serverUrl,
            method : 'POST',
            data : {service : service, action : action, filters : filtro}
        });
        return request;
    };

		function getRequestList(action, data){
	    	var request =$http({
	            url: serverUrl,
	            data: data,
	            method: 'POST'
	        });
	    return request;
	};

	DesglosesMQService.getListaBolsaMercado = function(successHandler, errorHandler, filter){
        var request = getRequest('getListaBolsaMercado', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesglosesMQService.getDesglosesSinPaginacion = function(successHandler, errorHandler, filter){
        var request = getRequest('getDesglosesSinPaginacion', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesglosesMQService.getDesglosesCount = function(successHandler, errorHandler, filter){
        var request = getRequest('getDesglosesCount', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesglosesMQService.marcarValidado = function(successHandler, errorHandler, data){
        var request = getRequestList('marcarValidado', data);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesglosesMQService.altaDesglose = function(successHandler, errorHandler, filter){
        var request = getRequest('altaDesglose', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesglosesMQService.altaTitular = function(successHandler, errorHandler, filter){
        var request = getRequest('altaTitular', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesglosesMQService.editarDesglose = function(successHandler, errorHandler, filter){
        var request = getRequest('editarDesglose', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesglosesMQService.editarTitular = function(successHandler, errorHandler, filter){
        var request = getRequest('editarTitular', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };

    DesglosesMQService.borrarDesglose = function(successHandler, errorHandler, filter){
        var request = getRequest('borrarDesglose', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };


    DesglosesMQService.borrarTitular = function(successHandler, errorHandler, filter){
        var request = getRequest('borrarTitular', filter);
        if(successHandler !== undefined && errorHandler !== undefined){
            request.success(successHandler).error(errorHandler);
        }
        return request;
    };


    return DesglosesMQService;

}]);//sibbac20.factory('DesglosesMQService', ['$http', function($http){
