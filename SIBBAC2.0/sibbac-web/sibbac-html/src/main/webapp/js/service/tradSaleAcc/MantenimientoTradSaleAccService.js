(function (sibbac20, angular, console) {
    "use strict";
    sibbac20.factory('MantenimientoTradSaleAccService', ['$resource', function ($resource) {
        var baseUrl, queryRes, queryResPost, queryResPut, queryResDel, queryValidIsCountry, queryGetPaises;
        var checkFilters;
        baseUrl = "/sibbac20back/rest/tradSaleAcc/mantenimiento";
        // Llamada a back con POST, permite los parámetros queryName y detail
        queryRes = $resource(baseUrl + "/:queryName/:detail",
            { queryName: "@queryName", detail: "@detail" }, { execute: { method: "POST", isArray: false } });
        // Llamada a back con POST
        queryResPost = $resource(baseUrl, {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con PUT
        queryResPut = $resource(baseUrl, {}, { execute: { method: "PUT", isArray: false } });
        // Llamada a back con DELETE, permite el parámetro ids
        queryResDel = $resource(baseUrl + '/delete', {}, { execute: { method: "POST", isArray: false } });
        // Llamada al /isCountry que realiza una comprobación del pais enviado por parámetro
        queryValidIsCountry = $resource(baseUrl + '/isValidCountry', {}, { execute: { method: "POST", isArray: true } });
        // Llamada al /getPaises que devuelve todos los paises activos
        queryGetPaises = $resource(baseUrl + '/getPaises', {}, { execute: { method: "GET", isArray: true } });
        // Creamos un array con las ids de los valores en la lista del filtro
        checkFilters = ['INDTRADER', 'INDSALE', 'INDTRAD_SALE', 'INDACCOUNT'];
        return {
            baseUrl: baseUrl,
            queries: queryRes.query,
            // Llamada a /filters
            dynamicFilters: function (name) {
                return queryRes.query({ queryName: name, detail: "filters" }, function (filters) {
                    var i = filters.length;

                    while (i--) {
                        if (checkFilters.includes(filters[i].field)) {
                            filters.splice(i, 1);
                        }
                    }
                    return filters;
                });
            },
            // Llamada a /columns
            columns: function (name, success) {
                var res = queryRes.query({ queryName: name, detail: "columns" }, function () {
                    success(res);
                });
            },

            // Llamada al /POST que devuelve los datos de la tabla
            executeQuery: function (queryName, filter, success, fail) {
                var data;
                filter.queryName = queryName;
                if (filter.TIPOFIGURA != undefined) {
                    filter[filter.TIPOFIGURA] = "1";
                    filter.TIPOFIGURA = undefined;
                }
                data = queryRes.query(filter, function () {
                    if (data.length > 0 && data[0].error !== undefined) {
                        fail(data[0].error);
                        return;
                    }
                    Object.keys(data).forEach(function (obj) {
                        Object.keys(data[obj]).forEach(function (keys) {
                            var aux = keys.replace(/_/g, "");
                            var oldValue = data[obj][keys];
                            var newValue = data[obj][aux];

                            newValue = angular.copy(oldValue);
                            oldValue = (aux != keys) ? undefined : oldValue;
                            data[obj][keys] = oldValue;

                            newValue = (checkFilters.includes(keys)) ? ((newValue == 1) ? 'S' : 'N') : newValue;
                            data[obj][aux] = newValue;
                        });
                        if (obj != "$promise" && obj != "$resolved") {
                            data[obj]["CUENTAPRODUCTO"] = data[obj].CUENTA + data[obj].PRODUCTO;
                            data[obj].CUENTA = undefined;
                            data[obj].PRODUCTO = undefined;
                        }
                    });
                    success(data);
                }, fail);
            },
            // Llamada al /POST que devuelve los datos de la tabla
            executeAction: function (queryName, params, items, success, fail) {
                var body = params;
                queryRes.execute({ queryName: queryName }, body, success, fail);
            },
            // Llamada al /POST que hace un save de los datos mandados por parámetro
            executeActionPOST: function (params, items, success, fail) {
                queryResPost.execute(params, success, fail);
            },
            // Llamada al /PUT que hace un edit de los datos mandados por parámetro
            executeActionPUT: function (params, items, success, fail) {
                var body = params;
                queryResPut.execute(body, success, fail);
            },
            // Llamada al /DELETE que realiza una eliminación por id de la lista enviada por parámetro
            executeActionDEL: function (params, items, success, fail) {
                var body = params;
                queryResDel.execute(body, success, fail);
            },
            // Llamada al /getPaises para retornar todos los paises dados de alta
            executeActionGetPaises: function (success, fail) {
                queryGetPaises.execute(success, fail);
            },
            // Llamada al /isCountry que realiza una comprobación del pais enviado por parámetro
            executeActionIsValidCountry: function (params, success, fail) {
                var body = params;
                queryValidIsCountry.execute(body, success, fail);
            },
            autocompleter: function (field) {
                return queryRes.query({ queryName: "autocompleter", detail: field });
            }
        }
    }]);
})(sibbac20, angular, console);

