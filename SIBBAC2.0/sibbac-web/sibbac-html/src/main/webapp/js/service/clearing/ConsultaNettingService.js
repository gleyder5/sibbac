sibbac20.factory('ConsultaNettingService', ['$http', function ($http) {
        var ConsultaNettingService = {};
        var url = server.url;
        var service = server.service.clearing.consultaNetting;
        ConsultaNettingService.getRequest = function (action, filter) {
            var request = $http({
                method: 'POST',
                url: url,
                data: {service: service, action: action, filters: filter}
            });
            return request;
        };

        ConsultaNettingService.listaNetting = function (successHandler, errorHandler, filter) {
            var request = ConsultaNettingService.getRequest('listaNetting', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        ConsultaNettingService.listaNettingSinMovimientoTitulos = function(successHandler, errorHandler, filter){
            var request = ConsultaNettingService.getRequest('listaNettingSinMovimientoTitulos', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        ConsultaNettingService.MarcarGastos = function(successHandler, errorHandler, filter, params){
            var action = 'MarcarGastos';
            var request = $http({
                method: 'POST',
                url: url,
                data: {service: service, action: action, filters: filter, params : params}
            });
            request.success(successHandler).error(errorHandler);
            return request;
        };

        return ConsultaNettingService;

    }]);

