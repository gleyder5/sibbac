(function (sibbac20, angular, console) {
    "use strict";
    sibbac20.factory('ControlEfectivoService', ['$resource', function ($resource) {
        var baseUrl, queryRes, queryResPost, queryResPut, queryResDel;
        baseUrl = "/sibbac20back/rest/datosMaestros/controlEfectivo";
        // Llamada a back con POST, permite los parámetros queryName y detail
        queryRes = $resource(baseUrl + "/:queryName/:detail",
            { queryName: "@queryName", detail: "@detail" }, { execute: { method: "POST", isArray: false } });
        // Llamada a back con POST
        queryResPost = $resource(baseUrl, {}, { execute: { method: "POST", isArray: false } });
        // Llamada a back con PUT
        queryResPut = $resource(baseUrl, {}, { execute: { method: "PUT", isArray: false } });
        // Llamada a back con DELETE, permite el parámetro ids
        queryResDel = $resource(baseUrl + '/:ids', { ids: '@ids' }, { execute: { method: "DELETE", isArray: false } });
        return {
            baseUrl: baseUrl,
            queries: queryRes.query,
            // Llamada a /filters
            dynamicFilters: function (name) {
                return queryRes.query({ queryName: name, detail: "filters" });
            },
            // Llamada a /columns
            columns: function (name, success) {
                var res = queryRes.query({ queryName: name, detail: "columns" }, function () {
                    success(res);
                });
            },
            
            // Llamada al /POST que devuelve los datos de la tabla
            executeQuery: function (queryName, filter, success, fail) {
                var data;
                filter = {};
                filter.queryName = queryName;
                data = queryRes.query(filter, function () {
                    if (data.length > 0 && data[0].error !== undefined) {
                        fail(data[0].error);
                        return;
                    }else{
                    	for(var i=0; data.length; i++){
                    		if(i + 1 <= data.length){		
                    			var str = data[i].PERCENT_TITULOS_MAX;
                    			if(str != null){
                    				str = str.toString();
                        			var res = str.replace(".",",");
                        			data[i].PERCENT_TITULOS_MAX = res;
                    			}                   			
                    			
                    			str = data[i].PERCENT_EFECTIVO_MAX;
                    			if(str != null){
                    				str = str.toString();
                        			res = str.replace(".",",");
                        			data[i].PERCENT_EFECTIVO_MAX = res;
                    			}		
                    			
                    			str = data[i].IMPORTE_EFECTIVO_MAX;
                    			if(str != null){
                    				str = str.toString();
                        			res = str.replace(".",",");
                        			data[i].IMPORTE_EFECTIVO_MAX = res;
                    			}
                    			
                    		}else{
                    			break;
                    		}
                        }
                    }
                    success(data);
                }, fail);
            },
            executeAction: function (queryName, params, items, success, fail) {
                var body = params;
                queryRes.execute({ queryName: queryName }, body, success, fail);
            },
            // Llamada al /POST que hace un save de los datos mandados por parámetro
            executeActionPOST: function (params, items, success, fail) {
                var body = params;
                queryResPost.execute(body, success, fail);
            },
            // Llamada al /PUT que hace un edit de los datos mandados por parámetro
            executeActionPUT: function (params, items, success, fail) {
                var body = params;
                queryResPut.execute(body, success, fail);
            },
            // Llamada al /DELETE que realiza una eliminación por id de la lista enviada por parámetro
            executeActionDEL: function (params, items, success, fail) {
                var body = params;
                queryResDel.execute({ ids: body }, success, fail);
            },
            autocompleter: function (field) {
                return queryRes.query({ queryName: "autocompleter", detail: field });
            }
        }
    }]);
})(sibbac20, angular, console);

