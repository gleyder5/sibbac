sibbac20
		.factory(
				'TextosService',
				[
						'$http',
						function($http) {
							var TextosService = {};
							var url = server.url;
							var service = server.service.configuracion.textosService;
							var idiomaService = server.service.configuracion.idiomaService;
							var serviceTxtIdiomas = server.service.configuracion.textosIdiomaService;
							TextosService.getListaIdiomas = function(successHandler, errorHandler, filter){
					        	var request = $http({
					                url : url,
					                method : 'POST',
					                data : {service : idiomaService, action : 'getListaIdiomas', filters : filter}
					            });
					            if(successHandler !== undefined && errorHandler !== undefined){
					                request.success(successHandler).error(errorHandler);
					            }
					            return request;
					        };
							TextosService.getRequest = function(action, filter) {
								return $http({
									method : 'POST',
									url : url,
									data : {
										action : action,
										service : service,
										filters : filter
									}
								});
							};
							TextosService.getRequestIdiomas = function(action,
									filter) {
								return $http({
									method : 'POST',
									url : url,
									data : {
										action : action,
										service : serviceTxtIdiomas,
										filters : filter
									}
								});
							};
							TextosService.getListaTextos = function(
									successHandler, errorHandler, filter) {
								var request = TextosService.getRequest(
										'getListaTextos', filter);
								if (successHandler !== undefined
										&& errorHandler !== undefined) {
									request.success(successHandler).error(
											errorHandler);
								}
								return request;
							};
							TextosService.modificarTexto = function(
									successHandler, errorHandler, filter) {
								var request = TextosService.getRequest(
										'modificarTexto', filter);
								if (successHandler !== undefined
										&& errorHandler !== undefined) {
									request.success(successHandler).error(
											errorHandler);
								}
								return request;
							};
							TextosService.findTextoIdiomaByIdTexto = function(
									successHandler, errorHandler, filter) {
								var request = TextosService.getRequestIdiomas(
										'findTextoIdiomaByIdTexto', filter);
								if (successHandler !== undefined
										&& errorHandler !== undefined) {
									request.success(successHandler).error(
											errorHandler);
								}
								return request;
							};
							TextosService.updateTextosIdiomas = function(
									successHandler, errorHandler, filter) {
								var request = TextosService.getRequestIdiomas(
										'updateTextosIdiomas', filter);
								if (successHandler !== undefined
										&& errorHandler !== undefined) {
									request.success(successHandler).error(
											errorHandler);
								}
								return request;
							};
							return TextosService;
						} ]);
