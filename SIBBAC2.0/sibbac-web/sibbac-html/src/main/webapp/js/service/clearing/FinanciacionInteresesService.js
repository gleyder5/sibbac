sibbac20.factory('FinanciacionInteresesService', ['$http',
    function ($http) {
        var service = server.service.clearing.financiacionIntereses;
        var url = server.url;
        var FinanciacionInteresesService = {};
        FinanciacionInteresesService.getRequest = function (action, filter) {
            var request = $http({
                url: url,
                mathod: 'POST',
                data: {service: service, action: action, filters: filter}
            });
            return request;
        };

        FinanciacionInteresesService.getListaIntereses = function (successHandler, errorHandler, filter) {
            var request = $http({
                method: 'POST',
                url: url,
                data: {service: service, action: 'getListaIntereses', filters: filter}
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        FinanciacionInteresesService.MarcarBajaIntereses = function (successHandler, errorHandler, params) {
        	var request = $http({
                method: 'POST',
                url: url,
                data: {service: service, action: 'MarcarBajaIntereses', params: params}
            });
        	if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        FinanciacionInteresesService.enviarCalculoIntereses = function (successHandler, errorHandler, filter) {
            var request = FinanciacionInteresesService.getRequest('enviarCalculoIntereses', filter);
            request.success(successHandler).error(errorHandler);
            return request;
        };
        return FinanciacionInteresesService;
    }]);

