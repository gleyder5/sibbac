'use restrict';
sibbac20.factory('GestionReglasService', ['$http', function ($http) {
        var GestionReglasService = {};
        var url = server.url;
        var service = server.service.configuracion.gestionreglasService;

        GestionReglasService.getRequest = function (action, filters) {
            var request = $http({
                method: 'POST',
                url: url,
                data: {action: action, service: service, filters: filters}
            });
            return request;
        };

        GestionReglasService.getRequestParams = function (action, params) {
	        var request = $http({
	            method: 'POST',
	            url: url,
	            data: {action: action, service: service, params: params}
	        });
	        return request;
    	};

    	GestionReglasService.getRequestTodos = function (action, filters, params) {
            var request = $http({
                method: 'POST',
                url: url,
                data: {action: action, service: service, filters: filters, params: params}
            });
            return request;
        };


        GestionReglasService.listaReglas = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getReglasPorFiltros', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.listaParametrosRegla = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getParametrizacionesById', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.altaRegla = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getAltaRegla', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.modificarRegla = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getModificarRegla', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.eliminarRegla = function (successHandler, errorHandler, params) {
            var request = GestionReglasService.getRequestParams('getEliminarRegla', params);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.actualizarActivoRegla = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getActualizarActivo', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.altaParametrizacion = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getAltaParametrizaciones', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };


        GestionReglasService.modificarParametrizacion = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getModificarParametrizacion', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.asignarAlias = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getAsignarAlias', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.asignarSubSta = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getAsignarSubCta', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.exportaParamExcel = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequestParams('getExportarParam', params);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.getAliasList = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getAliasList', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

        GestionReglasService.getSubCtaList = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('getSubCtaList', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };


        GestionReglasService.importarRegla = function (successHandler, errorHandler, filters) {
            var request = GestionReglasService.getRequest('importarRegla', filters);
            request.success(successHandler).error(errorHandler);
            return request;
        };

    	return GestionReglasService;
}]);

