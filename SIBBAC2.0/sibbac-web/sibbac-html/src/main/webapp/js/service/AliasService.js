sibbac20.factory('AliasService', ['$http', '$rootScope', function ($http, $rootScope) {
        var AliasService = {};
        var serverUrl = server.url;
        var fServiceAlias = server.service.alias.aliasService;
        var fServiceTmct0ali = server.service.alias.tmct0aliService;
        var aliasServicios = server.service.alias.servicios;

        AliasService.getAlias = function (successHandler, errorHandler) {
            var request = $http({
                url: serverUrl,
                data: {service: fServiceTmct0ali, action: 'getAlias'},
                method: 'POST',
                cache: true
            });

            if (successHandler !== undefined) {
                request.success(function (data, status, headers, config) {
                    var datos = {};
                    if (data !== null && data.resultados !== null
                            && data.resultados.result_alias !== null) {
                        datos = data.resultados.result_alias;
                    }

                    successHandler(datos);

                }).error(errorHandler);
            }
            return request;
        };
        AliasService.getAliasFactura = function (successHandler, errorHandler) {
            $http({
                url: serverUrl,
                data: {service: fServiceAlias, action: 'getListaAliasFacturacion'},
                method: 'POST',
                cache: true
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null && data.resultados !== undefined
                        && data.resultados !== null
                        && data.resultados.listaAlias !== null
                        && data !== undefined && data.resultados !== undefined
                        && data.resultados.listaAlias !== undefined) {
                    datos = data.resultados.listaAlias;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        AliasService.getListaAliasFacturacionSubcuenta = function (successHandler, errorHandler, filter) {
            $http({
                url: serverUrl,
                data: {service: fServiceAlias, action: 'getListaAliasFacturacionSubcuenta', filters: filter},
                method: 'POST'
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null && data.resultados !== null
                        && data.resultados.listaAlias !== null
                        && data !== undefined && data.resultados !== undefined
                        && data.resultados.listaAlias !== undefined) {
                    datos = data.resultados.listaAlias;
                }
                successHandler(datos);
            }).error(errorHandler);
        };
        AliasService.getListaAlias = function (successHandler, errorHandler) {
            var request = $http({
                url: serverUrl,
                data: {service: fServiceAlias, action: 'getListaAlias'},
                method: 'POST',
                cache: true
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        AliasService.getListaAliasFacturacionAlias = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: serverUrl,
                data: {service: fServiceAlias, action: 'getListaAliasFacturacionAlias', filters: filter},
                method: 'POST'
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        AliasService.modificarAlias = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: serverUrl,
                data: {service: fServiceAlias, action: 'modificarAlias', filters: filter},
                method: 'POST'
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        AliasService.getContactosByService = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: serverUrl,
                data: {service: aliasServicios, action: 'getContactosByService', filters: filter},
                method: 'POST'
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        AliasService.bajaServicioContacto = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: serverUrl,
                data: {service: aliasServicios, action: 'bajaServicioContacto', filters: filter},
                method: 'POST'
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };
        AliasService.getAliasSinFacturacion = function (successHandler, errorHandler) {
            $http({
                url: serverUrl,
                data: {service: fServiceAlias, action: 'getListaAliasSinFacturacion'},
                method: 'POST',
                cache: true
            }).success(function (data, status, headers, config) {
                var datos = {};
                if (data !== null && data.resultados !== null
                        && data.resultados.listaAlias !== null
                        && data !== undefined && data.resultados !== undefined
                        && data.resultados.listaAlias !== undefined) {
                    datos = data.resultados.listaAlias;
                }
                successHandler(datos);
            }).error(errorHandler);
        };

        AliasService.listaAliasCliente = function (successHandler, errorHandler, filter) {
            var request = $http({
                url: serverUrl,
                data: {service: fServiceAlias, action: 'listaAliasCliente', filters: filter},
                method: 'POST',
                cache: true
            });
            if (successHandler !== undefined && errorHandler !== undefined) {
                request.success(successHandler).error(errorHandler);
            }
            return request;
        };

        return AliasService;
    }]);


