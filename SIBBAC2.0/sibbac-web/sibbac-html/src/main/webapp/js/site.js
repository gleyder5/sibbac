$(function () {
    // start the ticker
    //$('#js-news').ticker();

    // hide the release history when the page loads
    //$('#release-wrapper').css('margin-top', '-' + ($('#release-wrapper').height() + 20) + 'px');

    // show/hide the release history on click
    /*$('a[href="#release-history"]').toggle(function () {
     $('#release-wrapper').animate({
     marginTop: '0px'
     }, 600, 'linear');
     }, function () {
     $('#release-wrapper').animate({
     marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
     }, 600, 'linear');
     });

     $('#download a').mousedown(function () {
     _gaq.push(['_trackEvent', 'download-button', 'clicked'])
     });*/
});

//// google analytics code
//var _gaq = _gaq || [];
//_gaq.push(['_setAccount', 'UA-6132309-2']);
//_gaq.push(['_setDomainName', 'www.jquerynewsticker.com']);
//_gaq.push(['_trackPageview']);
//
//(function() {
//  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
//  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
//})();

//Transformar fechas "yyyy-mm-dd hh:mm:ss"
function transformaFechaYHora(fechaEntrada)
{
    var fechaDevuelta = "";
    if (fechaEntrada != null && fechaEntrada != undefined && fechaEntrada != "") {
        var auxFechaEntrada = fechaEntrada;
        while (fechaEntrada.indexOf("-") !== -1) {
            fechaEntrada = fechaEntrada.replace("-", "");
        }
        ano = fechaEntrada.substring(0, 4);
        mes = fechaEntrada.substring(4, 6);
        dia = fechaEntrada.substring(6, 8);

        fechaDevuelta = dia + "/" + mes + "/" + ano;
        console.log("fechaDevuelta: " + fechaDevuelta);

        console.log("auxFechaEntrada: " + auxFechaEntrada);
        console.log("auxFechaEntrada.length: " + auxFechaEntrada.length);
        if (auxFechaEntrada.length === 19) {
            var HH = auxFechaEntrada.substring(11, 13);
            var mm = auxFechaEntrada.substring(14, 16);
            var s = auxFechaEntrada.substring(17, 19);
            fechaDevuelta += " " + HH + ":" + mm + ":" + s;
        }
    }
    return fechaDevuelta;
}


//Transformar fechas
function transformaFecha(fechaEntrada)
{
    var fechaDevuelta = "";
    if (fechaEntrada != null && fechaEntrada != undefined && fechaEntrada != "") {
        var auxFechaEntrada = fechaEntrada;
        while (fechaEntrada.indexOf("-") !== -1) {
            fechaEntrada = fechaEntrada.replace("-", "");
        }
        ano = fechaEntrada.substring(0, 4);
        mes = fechaEntrada.substring(4, 6);
        dia = fechaEntrada.substring(6, 8);

        fechaDevuelta = dia + "/" + mes + "/" + ano;
        if (auxFechaEntrada.length === 15) {
            var HH = auxFechaEntrada.substring(9, 11);
            var mm = auxFechaEntrada.substring(11, 13);
            var s = auxFechaEntrada.substring(13, 15);
            fechaDevuelta += " " + HH + ":" + mm + ":" + s;
        }
    }
    return fechaDevuelta;

}

function transformaFechaHora(fechaEntrada)
{
    var fechaDevuelta = transformaFecha(fechaEntrada);
    if (fechaEntrada != null && fechaEntrada != undefined && fechaEntrada != "") {
        hora = fechaEntrada.substring(9, 11);
        minuto = fechaEntrada.substring(11, 13);
        segundo = fechaEntrada.substring(13, 16);
        fechaDevuelta = fechaDevuelta + " " + hora + ":" + minuto + ":" + segundo;
    }
    return fechaDevuelta;

}

//transformar fecha para formato yyyy-mm-dd/mm/yy
function transformaFechaGuion(fechaEntrada)
{
    var fechaDevuelta = "";
    if (fechaEntrada != null && fechaEntrada != undefined && fechaEntrada != "") {
        ano = fechaEntrada.substring(0, 4);
        mes = fechaEntrada.substring(5, 7);
        dia = fechaEntrada.substring(8, 10);
        fechaDevuelta = dia + "/" + mes + "/" + ano;
    }
    return fechaDevuelta;
}


//transformar fecha para formato dd-mm-yyyy
function transformaFechaInv(fechaEntrada) {
    var fechaDevuelta = "";
    if (fechaEntrada != null && fechaEntrada != undefined && fechaEntrada != "") {
        ano = fechaEntrada.substring(6, 10);
        mes = fechaEntrada.substring(3, 5);
        dia = fechaEntrada.substring(0, 2);
        fechaDevuelta = ano + mes + dia;
    }
    return fechaDevuelta;
}

function transformaFechaGuionInv(fechaEntrada)
{
    var fechaDevuelta = "";
    if (fechaEntrada != null && fechaEntrada != undefined && fechaEntrada != "") {
        ano = fechaEntrada.substring(6, 10);
        mes = fechaEntrada.substring(3, 5);
        dia = fechaEntrada.substring(0, 2);
        fechaDevuelta = ano + "-" + mes + "-" + dia;
    }

    return fechaDevuelta;
}



//poner calendario en español
function cargarCalendarioEsp() {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '',
        nextText: '',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
}

function EpochToHuman(inputtext) {
    if (inputtext == null || inputtext == undefined) {
        return "";
    }
    var epoch = inputtext;
    var outputtext = "";
    var extraInfo = 0;
    var datum = new Date(inputtext);
    outputtext = datum.toLocaleString();
    var formatearfecha = outputtext.split('/');
    var dia = formatearfecha[0];
    var mes = formatearfecha[1];
    (dia.length != 2) ? dia = '0' + dia : dia;
    (mes.length != 2) ? mes = '0' + mes : mes;

    return dia + '/' + mes + '/' + formatearfecha[2];
}

function EpochToHumanSinHora(inputtext) {
    if (inputtext == null || inputtext == undefined) {
        return "";
    }
    var epoch = inputtext;
    var outputtext = "";
    var extraInfo = 0;
    var datum = new Date(inputtext);
    outputtext = datum.toLocaleString();
    var formatearfecha = outputtext.split('/');
    var dia = formatearfecha[0];
    var mes = formatearfecha[1];
    (dia.length != 2) ? dia = '0' + dia : dia;
    (mes.length != 2) ? mes = '0' + mes : mes;
    var anno = formatearfecha[2].split(' ')[0];

    return dia + '/' + mes + '/' + anno;
}

function refrescar(url) {
    $("section").load(url);
}

function sumoFecha(fechaEntrada)
{
    var fechaDevuelta = "";
    if (fechaEntrada != null && fechaEntrada != undefined && fechaEntrada != "") {
        var ano = fechaEntrada.substring(0, 4);
        var mes = '' + fechaEntrada.substring(4, 6);
        var dia = fechaEntrada.substring(6, 8);
        if (mes != 12)
        {
            mes = parseFloat(mes) + 1;
        } else
        {
            mes = "01";
            ano = parseFloat(ano) + 1;
        }
        mes = '' + mes;
        if (mes.length == 1)
        {
            mes = "0" + mes;
        }
        fechaDevuelta = dia + "/" + mes + "/" + ano;
    }
    return fechaDevuelta;
}

function inicializarLoading() {
    return $.blockUI({
        message: '<h1><img style="vertical-align: middle" src="images/loading.gif" /> Cargando datos ...</h1>',
        css: {
        	'z-index': 999999999,
            border: 'none',
            padding: '15px',
            backgroundColor: '#FFF',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#666'
        }
    });
}

function cargarFechaActualizacionMovimientoAlias() {

    var params = new DataBean();
    params.setService('SIBBACServiceMovimientoVirtual');
    params.setAction('getUpdateLastTime');
    var request = requestSIBBAC(params);
    request.success(function (json) {
        var datos = undefined;

        if (json === undefined || json.resultados === undefined || json.resultados === null) {
            datos = {};
        } else {
            datos = json.resultados;
            //ultima fecha actualizacion
            $("#ultFechaMovAlias").html(transformaFecha(datos.result_last_time_update));
        }

    });
}
//Configura la ventana de mensajes.( Dialog )
$("#mensaje").dialog({
    autoOpen: false,
    modal: false,
    width: 500,
    buttons: [
        {
            text: "Aceptar",
            icons: {
                primary: "ui-icon-check",
                class: "ui-button-icon-primary ui-icon ui-icon-check"
            },
            class: "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary",
            click: function () {
                $(this).dialog("close");
            }
        }],
    show: {
        effect: "blind",
        duration: 1000
    },
    hide: {
        effect: "explode",
        duration: 1000
    }
});

//Carga el mensaje adecuando en el dialog
function fErrorTxt(msgError, tipo) {


    $('.ui-dialog-titlebar-close').remove();



    var option = $(".mensaje").dialog("option", "buttons", [
        {
            text: "Aceptar",
            icons: {
                primary: "ui-icon-check",
            },
            class: "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary",
            click: function () {
                $(this).dialog("close");
            }
        }]);



    if (msgError == undefined) {
        tipo = 1
        msgError = "Error indeterminado. Consulte al servicio técnico.";
    }

    $('#mensaje').html('');
    if (tipo == 1) {
        $("#mensaje").dialog(option, "title", "Error");
        $('#mensaje').append("<div class='dialogMensajeIncon'> <img src='/sibbac20/images/error.png'/></div>");
    } else if (tipo == 2) {
        $("#mensaje").dialog(option, "title", "Advertencia");
        $('#mensaje').append("<div class='dialogMensajeIncon'> <img src='/sibbac20/images/warning.png'/></div>");
    } else if (tipo == 3) {
        $("#mensaje").dialog(option, "title", "Confirmación");
        $('#mensaje').append("<div class='dialogMensajeIncon'> <img src='/sibbac20/images/accept.png'/></div>");
    }

    var btnAceptar = $('.ui-dialog-buttonpane').find('button:contains("Aceptar")');


    btnAceptar.width(100);
    btnAceptar.height(25);
    $('#mensaje').append("<p class='dialogMensajeText'ALIGN=center>" + msgError + "</p>");
    $('#mensaje').dialog("open");

}

/**
 * carga los elementos que se van a pasar al autocomplete
 * @param {type} datos
 * @param {type} campoId
 * @param {type} campoNombre
 * @param {type} campoDescripcion
 * @returns {Array}
 */
function cargarElementos(datos, campoId, campoNombre, campoDescripcion) {
    var elementos = [];
    var obj = {};
    angular.forEach(angular.element(datos), function (val, key) {
        obj = {
            label: val[campoId] + " - " + val[campoNombre]
                    + " - " + val[campoDescripcion],
            value: val[campoId]
        };
        elementos.push(obj);
    });
    return elementos;
}

/**
 * 	Funcionalidad Interfaces Emitidas.
 * 	Devuelve true si el fichero seleccionado
 * 	@param ficheroSeleccionado
 * 	@return boolean
 */
function isFicheroRespuestaConErrores(ficheroSeleccionado) {
	return ficheroSeleccionado.estado == 'KO'
		&& (ficheroSeleccionado.nombreFichero.indexOf('RESP') != -1
			|| ficheroSeleccionado.nombreFichero.indexOf('RESPSR') != -1);
}
