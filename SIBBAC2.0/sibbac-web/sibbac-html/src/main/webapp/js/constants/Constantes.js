/**
 *	Aqui se van a cargar las constantes de la app. 
 */


'use strict';
    angular.module('sibbac20').constant('CONSTANTES', (function () {
        // Definiciones de variable.
        let _this = {};
        // Definicion para campo de la tabla de filtros informe.
        _this.tipoFecha = 'FECHA';
        _this.tipoAutorrellenable = 'AUTORRELLENABLE';
        _this.tipoLista = 'LISTA';
        _this.tipoCombo = 'COMBO';
        _this.tipoTexto = 'TEXTO';
        _this.subtipoDesdeHasta = 'DESDE-HASTA';
        /*CONSTANTES CONTABILIDAD*/

        _this.THEIR_CASH_CREDIT = 'Their Cash Credit';
        _this.THEIR_CASH_DEBIT = 'Their Cash Debit';
        _this.OUR_CASH_CREDIT = 'Our Cash Credit';
        _this.OUR_CASH_DEBIT = 'Our Cash Debit';

        // Constantes para tipo destino en planificacion de informes
        _this.ruta = 'RUTA';
        _this.email = 'EMAIL';

        // Constantes para modal.
        _this.modalTituloConfirmacion = 'Mensaje de Confirmación';
        _this.modalTituloValidacion = 'Mensaje de Validación';

        // Usar la variable en sus constantes.
        return {
            SUBTIPO_DESDE_HASTA: _this.subtipoDesdeHasta,
            TIPO_FECHA: _this.tipoFecha,
            TIPO_AUTORRELLENABLE: _this.tipoAutorrellenable,
            TIPO_LISTA: _this.tipoLista,
            TIPO_COMBO: _this.tipoCombo,
            TIPO_TEXTO: _this.tipoTexto,
            RUTA: _this.ruta,
            EMAIL: _this.email,
            MODAL_TITULO_CONFIRM: _this.modalTituloConfirmacion,
            MODAL_TITULO_VALIDACION: _this.modalTituloValidacion,
            THEIR_CASH_CREDIT :_this.THEIR_CASH_CREDIT,
            THEIR_CASH_DEBIT :_this.THEIR_CASH_DEBIT,
            OUR_CASH_CREDIT :_this.OUR_CASH_CREDIT,
            OUR_CASH_DEBIT :_this.OUR_CASH_DEBIT,
        };
    })()

    );
