(function(sibbac20, angular, undefined){
  "use strict";
  
  sibbac20.directive("modalResultDialog",["$timeout", function($timeout){
    return {
      restrict: "E", templateUrl : "components/directives/modalResultDialog.html"
    }
  }]);
  
})(sibbac20, angular);