var GENERIC_CONTROLLERS = GENERIC_CONTROLLERS || {};
(function(angular, $, GENERIC_CONTROLLERS, undefined){
	"use strict";

	/**
	 * Función constructor de un controlador genérico para pantalla con grid y filtros 
	 */
	var FilteredGridController = function(service, httpParamSerializer) {
		this.service = service;
		this.httpParamSerializer = httpParamSerializer;
		this.grid = this.createGrid();
		this.followSearch = "";
	};

	/**
	 * Agrega estilo a las columnas del grid.
	 */
	FilteredGridController.prototype.customizeColumns = function(value) {
		value.enableHiding = false;
		value.width = 145;
		value.headerCellClass = "sb-grid-header";
		switch(value.type) {
		  case "number":
		    value.cellClass = "number-cell";
		    break;
		}
		
	};
	
	/**
	 * Solicita al servicio ejecutar la consulta que alimentará al grid.
	 */
	FilteredGridController.prototype.executeQuery = function() {
		inicializarLoading();
		this.service.executeQuery(this.filter, this.queryReceiveData.bind(this), this.queryFail.bind(this)); 
	};
	
	/**
	 * Recibe del servicio los datos solicitados por el método executeQuery
	 */
	FilteredGridController.prototype.queryReceiveData = function(receivedData) {
		this.grid.data = receivedData;
		this.executedFilter = this.httpParamSerializer(this.filter);
		if(receivedData.length == 0) {
			fErrorTxt("La consulta ejecutada no ha regresado resultados");
		}
		this.followSearch = this.calculateFollowSearch();
		this.showingFilter = false;
		$.unblockUI();
	};
	
	/**
	 * Concatena los nombres de los filtros con los valores actualmente asignados con el
	 * objetivo de ser mostrados en un texto.
	 */
	FilteredGridController.prototype.calculateFollowSearch = function() {
		var fs = "", labels = angular.element("form[name='filters'] label[for]");
		angular.forEach(labels, function(l) {
			var le, inputId, input, val, options;
			le = angular.element(l);
			inputId = le.attr("for"); 
			input = angular.element("#" + inputId);
			if(input && input.length > 0 && input.val() != "") {
				switch(input[0].tagName) {
				case "SELECT":
					options = input.children(":selected");
					if(options.length > 0 && options[0].label != "") {
						val = options[0].label;
					}
					else {
						return;
					}
					break;
				default:
					val = input.val();
				}
				fs += (le.text() + val + ";");
			}
		});
		return fs;
	}
	
	/**
	 * Recibe un error producido por la llamada de executeQuery al servicio
	 */
	FilteredGridController.prototype.queryFail = function(error) {
		fErrorTxt("Se produjo un error en la ejecución de la consulta: " + error, 1);
		$.unblockUI();
	};
	
	/**
	 * Controla los errores ocurridos después de una operacion POST
	 */
	FilteredGridController.prototype.postFail = function(error) {
		var message;
		if(typeof error === "string" ) {
			message = "Se produjo un error al ejecutar la acción: " + error;
		}
		else if(error.length == 1) {
			message = error;
		}
		else {
			message = "Se produjeron los siguientes errores: \n";
			angular.forEach(error, function(value) {
				message += ("- " + value + "\n");
			});
		}
		fErrorTxt(message, 1);
		$.unblockUI();
	}
	
	/**
	 * Función de utilidad para deshabilitar botones cuando no haya renglones seleccionados
	 * en el grid.
	 */
	FilteredGridController.prototype.actionButtonDisabled = function() {
		return this.gridApiRef.selection.getSelectedRows().length === 0;
	};
	
	/**
	 * Función de utilidad que regresa el número de renglones seleccionados en el grid.
	 */
	FilteredGridController.prototype.countSelectedRows = function() {
		return this.gridApiRef.selection.getSelectedRows().length;
	};
	
	/**
	 * Retorna los renglones seleccionados en el grid.
	 */
	FilteredGridController.prototype.selectedRows = function() {
		return this.gridApiRef.selection.getSelectedRows();
	}

	/**
	 * Realiza un filtrado local
	 */
	FilteredGridController.prototype.singleFilterProc = function(renderableRows, filterValue) {
		if(filterValue != undefined && filterValue.trim().length > 0) {
			renderableRows.forEach(function(row){
				var visible = false, n, astr;
				for(n in row.entity) {
					astr = new String(row.entity[n]);
					if(astr.toLocaleLowerCase().includes(filterValue.toLocaleLowerCase())) {
						visible = true;
						break;
					}
				}
				row.visible = visible;
			});
		}
		return renderableRows;
	};

	FilteredGridController.prototype.refreshSingleFilter = function() {
		this.gridApiRef.grid.refresh();
	};

	/**
	 * Retorna un objeto de configuración con los valores utilizados en este proyecto
	 * para mostrar un grid básico. Para las columnas consulta el método "columns".
	 */
	FilteredGridController.prototype.createGrid = function() {
		var ctrl = this, columns;
		columns = this.columns();
		angular.forEach(columns, this.customizeColumns, this);
		return {
			paginationPageSizes: [10, 25, 50],
			paginationPageSize: 25,
			enableFiltering: false,
			data: [], 
			columnDefs: columns,
			gridMenuShowHideColumns: false,
			onRegisterApi: function(gridApi) { 
				ctrl.gridApiRef = gridApi;
				if(gridApi.selection) {
					ctrl.selectAll = gridApi.selection.selectAllRows;
					ctrl.unselectAll = gridApi.selection.clearSelectedRows;
				}
				gridApi.grid.registerRowsProcessor(function(renderableRows){ 
					return ctrl.singleFilterProc(renderableRows, ctrl.singleFilterValue); 
				}, 500);
			},
		};
	};
	
	/**
	 * Regresa la definición básica de las columnas. Para ser implementado en objetos
	 * decendientes.
	 */
	FilteredGridController.prototype.columns = function() {
		return [];
	}
	
	/**
	 * Registra un elemento de la página como diálogo modal
	 * elementId: identificador del elemento
	 * title: título inicial de la diálogo modeal
	 * buttons: objeto cuyos campos son los nombres del botón y como valores funciones
	 * que sirven como controladores del evento click.
	 * options: opcional, valores adicionales que se quieran agregar a los que se ponen
	 * por defecto.
	 */
	FilteredGridController.prototype.registryModal = function(elementId, title, buttons, options) {
		var dialogElement;
		
		if(options === undefined) {
			options = {};
		}
		options.autoOpen = false;
		options.modal = true;
		options.title = title;
		options.buttons = buttons;
		buttons["Cerrar"] = function() {dialogElement.dialog("close");};
		dialogElement = angular.element("#" + elementId);
		dialogElement.dialog(options);
		angular.element(".ui-dialog-buttonset button").addClass("mybutton");
		return dialogElement;
	};

	/**
	 * Elimina el estilo de los botones que se encuentre en el dialog con la id 
	 * que se le pasa por parámetro.
	 */
	FilteredGridController.prototype.removeBtnsStyle = function (idDialog) {
		$("[aria-describedby='"+ idDialog + "']").find("button").each( function (index, element) {
			element.setAttribute("style", "")
		});
	}

	/**
	 * Pone a x px el máximo de altaura en el autocompletar Indicado
	 */
	FilteredGridController.prototype.autoCompleteMaxH = function (idDialog, x) {
		$("[aria-describedby='"+ idDialog + "']").find(".ui-autocomplete").each( function (index, element) {
			element.setAttribute("style", "max-height: " + x +"px;")
		});
	}

	/**
	 * Settear el atributo disable del elemento al estado con los parámetros 
	 * recibidos por parámetros
	 */
	FilteredGridController.prototype.setBtnDisable = function (id, state, value) {
		var buttonElement = $("[aria-describedby='"+ id + "']").find("button:contains('" + value + "')");
		buttonElement.attr("disabled", state);
		(state) ? buttonElement.addClass("ui-state-disabled") : buttonElement.removeClass("ui-state-disabled");
	}
	
	/**
     * Elimina el estilo de los botones que se encuentre en el dialog con la id 
     * que se le pasa por parámetro.
     */
	   FilteredGridController.prototype.removeBtnsStyle = function (idDialog) {
	       $("[aria-describedby='"+ idDialog + "']").find("button").each( function (index, element) {
	           element.setAttribute("style", "")
	       });
	   } 
		
	/**
	 * Retorna una URL para realizar la consulta que regresará los datos mostrados en el
	 * grid en un formato específico (excel, cvs)
	 */
	FilteredGridController.prototype.exportUrl = function(format) {
		var serialFilter, baseUrl, singleObject;
		baseUrl = this.baseUrl();
		if(baseUrl && this.executedFilter !== undefined) {
			serialFilter = this.executedFilter;
			if(this.singleFilterValue) {
				singleObject = {singleFilter: this.singleFilterValue};
				serialFilter += ("&" + this.httpParamSerializer(singleObject));
			}
			return baseUrl + "/" + format + "?" + serialFilter;
		}
		return "";
	};
	
	FilteredGridController.prototype.baseUrl = function() {
		return this.service.baseUrl;
	};
		
	GENERIC_CONTROLLERS.FilteredGridController = FilteredGridController;
	
})(angular, $, GENERIC_CONTROLLERS);