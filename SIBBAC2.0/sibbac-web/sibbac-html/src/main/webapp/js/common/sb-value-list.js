(function(sibbac20, angular, undefined) {
  "use strict";
  var onPasteHandler, linkFunction;

  /**
   * Controlador del evento onPaste asignado al input con nombre 'single'.
   * Recupera del portapapeles los valores a copiar, los separa por espacios
   * en blanco y los introduce como elementos nuevos en el array 'multiple'
   * publicado en el scope.
   * event - Objeto que contiene la informacion del evento
   * scope - Scope de la directiva.
   */
  onPasteHandler = function(event, scope) {
    angular.forEach(event.clipboardData.items, function(i, ndx) {
      if (event.clipboardData.types[ndx] === 'text/plain') {
        i.getAsString(function(v) {
          angular.forEach(v.split(/\s/), function(q) {
            var trimed = angular.uppercase(q.trim());
            if (trimed !== "" && scope.multiple.indexOf(trimed) === -1) {
              scope.multiple.push(trimed);
              scope.$apply();
            }
          });
        });
      }
      event.preventDefault();
    });
  };

  /**
   * Función de enlace de la directiva.
   * Define dos atributos en su scope: single y multiple.
   * scope.single está enlazado al input, el cual tiene una escucha para
   * el evento onPaste, que le permite recibir varios elementos a la vez.
   * scope.multiple está vínculado al ngModel. 
   */
  linkFunction = function(scope, element, attributes, ngModelCtrl) {
    var singleInput;
    scope.single = "";
    scope.sentidoFiltro = "=";
    singleInput = element.find("input")[1];
    singleInput.addEventListener("paste", function(event) {
      onPasteHandler(event, scope);
    });
    if (scope.autocomplete) {
      angular.element(singleInput).autocomplete(scope.autocomplete);
    }
    ngModelCtrl.$render = function() {
      var vv = ngModelCtrl.$viewValue;
      scope.multiple = vv ? vv : [];
    };
    scope.$watch("multiple", function(newValue) {
      ngModelCtrl.$setViewValue(newValue);
    });
    scope.add = function() {
      var c = scope.single.trim();
      if (c !== "" && scope.multiple.indexOf(c) === -1) {
        scope.single = "";
        scope.multiple.push(c);
      }
    };
    scope.remove = function() {
      var pos;
      if (scope.selected) {
        pos = scope.multiple.indexOf(scope.selected);
        if (pos >= 0) {
          scope.selected = null;
          scope.multiple.splice(pos, 1);
        }
      }
    };
    scope.clean = function() {
      scope.selected = null;
      scope.multiple = [];
    };
  };

  sibbac20.directive("sbValueList", function() {
    return {
      scope : {
        label : "@", maxlength : "=", autocomplete : "="
      }, transclude : true, require : "ngModel", restrict : "E",
      templateUrl : "components/directives/sb-value-list.html", link : linkFunction
    }
  });

  sibbac20.directive("sbSentidoFiltro", function() {
    var operadoresFiltro = ["=", "≠", "~", "≥", "≤"];
    return {
      scope : {},
      require : "ngModel",
      restrict : "E",
      template : '<input type="button" class="btn buscador mybutton" '
          + 'style="margin-top: -2px; width: 20px; float: left;" '
          + 'ng-model="sentidoFiltro" value="{{sentidoFiltro}}" ng-click="rotarOperador()">',
      link : function(scope, element, attributes, ngModelCtrl) {
        ngModelCtrl.$render = function() {
          var vv = ngModelCtrl.$viewValue;
          scope.sentidoFiltro = vv ? vv : "=";
        };
        scope.$watch("sentidoFiltro", function(newValue) {
          ngModelCtrl.$setViewValue(newValue);
        });
        scope.rotarOperador = function() {
          console.log("Presionado botón rotarOperador");
        };
      }
    }
  });

})(sibbac20, angular);