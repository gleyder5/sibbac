// Directiva para transformar a mayúsculas
sibbac20.directive('autoUppercase', ['$filter', function ($filter) {
  return {
    require: 'ngModel', link: function (scope, element, attrs, controller) {
      controller.$parsers.push(function (value) {
        var transformedInput = $filter('uppercase')(value);
        if (transformedInput !== value) {
          var el = element[0];
          el.setSelectionRange(el.selectionStart, el.selectionEnd);
          controller.$setViewValue(transformedInput);
          controller.$render();
        }
        return transformedInput;
      });
    }
  };
}]);

//Directiva para permitir solo puntos y números.
sibbac20.directive("comaDotConverter", function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {

      modelCtrl.$parsers.push(function (inputValue) {

        if (typeof (inputValue) == "undefined") return '';
        //var transformedInput = inputValue.replace(/,/g,'.');
        var transformedInput = inputValue.replace(/[^0-9.]/g, '');

        if (transformedInput.split('.').length > 2) {
          transformedInput = transformedInput.substring(0, transformedInput.length - 1);
        }

        if (transformedInput != inputValue) {
          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
        }

        return transformedInput;
      });
    }
  };

});

// Directiva para invocar función al presionar enter sobre un campo
sibbac20.directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
});

sibbac20
  .directive(
    'sentidoFiltro',
    function () {
      return {
        restrict: 'E',
        scope: {
          campo: '='
        },
        template: '<button title="Cambiar el sentido de la busqueda" style="width: 40px; margin-left: 4px; margin-top: 1px;" ng-click="invertirSentidoFiltro()">'
          + '<span class="negrita" >{{getOperadorSentidoFiltro()}}</span>'
          + '</button>'
          + '<span ng-style="getColorSentidoFiltro()"> {{getSentidoFiltro()}}</span>',
        link: function ($scope, element) {

          $scope.getColorSentidoFiltro = function () {
            if ($scope.campo == "IGUAL") {
              return {
                'color': 'green'
              };
            }
            else {
              return {
                'color': 'red'
              };
            }
          };

          $scope.getOperadorSentidoFiltro = function () {
            if ($scope.campo == "IGUAL") {
              return "=";
            }
            else {
              return "<>";
            }
          };

          $scope.getSentidoFiltro = function () {
            return $scope.campo;
          };

          $scope.invertirSentidoFiltro = function () {
            if ($scope.campo == "IGUAL") {
              $scope.campo = "DISTINTO";
            }
            else {
              $scope.campo = "IGUAL";
            }
          };

        }
      }
    });

sibbac20.directive("titleSlideFilter", function () {
  return {
    restrict: "EA",
    transclude: true,
    scope: { showing: "=", onhide: "&" },
    link: function ($scope, $element) {
      var filter = $element.next(), evaluate = function () {
        if ($scope.showing) {
          $scope.mensajeBusqueda = "Ocultar";
          filter.slideDown();
        }
        else {
          $scope.mensajeBusqueda = "Mostrar";
          filter.slideUp();
          $scope.onhide({});
        }
      };
      if ($scope.showing === undefined)
        $scope.showing = true;
      $scope.mostrarOcultarFiltros = function () {
        $scope.showing = !$scope.showing;
      }
      $scope.collapserClass = function () {
        var cls = ["collapser_search", "cursor"];
        if (!$scope.showing)
          cls.push("active");
        return cls;
      };
      $scope.$watch("showing", evaluate);
      evaluate();
    },
    template:
      '<div class="title_section clearfix"><div class="align-left"> ' +
      '<p class="title"><ng-transclude></ng-transclude></p></div>' +
      '<div class="align-right">' +
      '<a ng-class="collapserClass()" ng-click="mostrarOcultarFiltros()">{{mensajeBusqueda}} opciones de b&uacute;squeda</a>' +
      '</div></div>'
  };
});

sibbac20.directive("autoCompleteInput", function () {
  return {
    transclude: true,
    scope: {
      fieldId: "@",
      pattern: "@",
      length: "=",
      index: "=",
      required: "=",
      disabled: "@",
      trim: "@",
      source: "=",
      param: "="
    },
    templateUrl: "components/directives/autoCompleteInput.html",
    link: function (scope, element, attrs) {
      scope.ngModel = "ctrl.params." + scope.fieldId;
      if (scope.pattern = !undefined && scope.pattern != "") {
        $("#" + scope.fieldId).attr({ 'pattern': scope.pattern });
      }
      scope.$watch("disabled", function (value) {
        if (scope.disabled == "true") {
          $("#" + scope.fieldId).attr('disabled','disabled');
        } else {
          $("#" + scope.fieldId).removeAttr('disabled');
        }
      });
      scope.$watch("source", function (value) {
        $("#" + scope.fieldId).autocomplete({
          classes: {
        	  "display": "none"
          },
          minLength: 0,
          source: scope.source,
          focus: function (event, ui) {
            return false;
          },
          change: function (event, ui) {
        	  return true;
          },
          create: function (event,ui) {
        	  return false;
          },
          open: function (event,ui) {
        	  return false;
          },
          select: function (event, ui) {
            var option = {
              key: ui.item.key,
              value: ui.item.value
            };
            scope.param = (scope.trim) ? option.value.trim() : option.value;
            scope.$parent.$apply();
            return false;
          }
        });
      });
    }
  }
});