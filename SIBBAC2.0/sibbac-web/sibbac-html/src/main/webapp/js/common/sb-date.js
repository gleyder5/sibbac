(function(sibbac20, angular, undefined){
  "use strict";
  
  sibbac20.directive("sbDate",["$timeout", function($timeout){
    return {
      restrict: "E", templateUrl : "components/directives/sb-date.html", 
      scope: {ngModel: "=", label: "@", fieldId: "@" }, link: function(scope, element){
        var inputEl = angular.element(element.find("input")[0]);
        $timeout(function() {
          inputEl.datepicker({
            dateFormat : "dd/mm/yy"
          });
        });
      } 
    }
  }]);
  
})(sibbac20, angular);