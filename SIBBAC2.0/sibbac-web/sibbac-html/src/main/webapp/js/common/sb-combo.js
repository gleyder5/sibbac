(function(sibbac20, angular, undefined){
  "use strict";
  
  sibbac20.directive("sbCombo", [function(){
    return {
      restrict: "E", templateUrl: "components/directives/sb-combo.html",
      scope: {ngModel: "=", label: "@", elements: "=", fieldId: "@"}
    }
  }]);
  
})(sibbac20, angular);