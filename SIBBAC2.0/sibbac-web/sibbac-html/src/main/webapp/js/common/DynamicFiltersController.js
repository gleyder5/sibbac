(function(angular, GENERIC_CONTROLLERS, undefined) {
	"use strict";

	/**
	 * Controlador genérico para pantalla con grid y filtros dinámicos
	 */
	var DynamicFiltersController = function(service, httpParamSerializer, uiGridConstants) {
		GENERIC_CONTROLLERS.FilteredGridController.call(this, service, httpParamSerializer);
		this.queries = service.queries();
		this.columnConst = uiGridConstants.dataChange.COLUMN;
	};
	
	DynamicFiltersController.prototype = Object.create(GENERIC_CONTROLLERS.FilteredGridController.prototype);
	
	DynamicFiltersController.prototype.constructor = DynamicFiltersController;

	DynamicFiltersController.prototype.selectQuery = function() {
		this.filterList = this.service.dynamicFilters(this.selectedQuery.name);
		this.filter = {};
		this.executedFilter = undefined;
		this.grid.data = [];
		this.service.columns(this.selectedQuery.name, this.columnsLoaded.bind(this));
	};
	
	DynamicFiltersController.prototype.columnsLoaded = function(columns) {
		var len = this.grid.columnDefs.length;
		this.grid.columnDefs.splice(0, len);
		this.gridApiRef.core.notifyDataChange(this.columnConst);
		angular.forEach(columns, function(c) {
			this.customizeColumns(c);
			this.grid.columnDefs.push(c);
		}, this);
		this.gridApiRef.core.notifyDataChange(this.columnConst);
	};

	DynamicFiltersController.prototype.executeQuery = function() {
		inicializarLoading();
		this.service.executeQuery(this.selectedQuery.name, angular.copy(this.filter), this.queryReceiveData.bind(this), 
				this.queryFail.bind(this)); 
	};
	
	DynamicFiltersController.prototype.baseUrl = function() {
		if(this.selectedQuery) {
			return this.service.baseUrl + "/" + this.selectedQuery.name;
		}
		return null;
	};
	
	
	DynamicFiltersController.prototype.calculateFollowSearch = function() {
		return "Consulta: " + this.selectedQuery.desc + ";" +
			GENERIC_CONTROLLERS.FilteredGridController.prototype.calculateFollowSearch.call(this);
	}
	
	GENERIC_CONTROLLERS.DynamicFiltersController = DynamicFiltersController;
	
})(angular, GENERIC_CONTROLLERS);