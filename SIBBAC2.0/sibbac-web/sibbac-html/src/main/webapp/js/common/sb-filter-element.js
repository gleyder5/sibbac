(function(sibbac20, angular, undefined) {
  "use strict";

  sibbac20.directive("sbFilterElement", [
    "$compile",
    function($compile) {
      var makeNode, makeDateControl, makeInputControl, makeComboControl;
      makeNode = function(scope, html) {
        var template, linkFn;
        template = angular.element(html);
        linkFn = $compile(template);
        return linkFn(scope);
      };
      makeDateControl = function(scope, value) {
        var html, node;
        html = "<sb-date ng-model='target.filter." + value.name + "' field-id='in_" + value.name + "' label='"
            + value.desc + "'></sb-date>";
        node = makeNode(scope, html);
        return node;
      };
      makeInputControl = function(scope, value) {
        var html, node, autoname;
        html = "<sb-value-list ng-model='target.filter." + value.name + "' label='" + value.desc + "' ";
        autoname = "auto-" + value.name;
        if (scope.target[autoname]) {
          html += "autocomplete='target[\"" + autoname + "\"]' "
        }
        html += "></sb-value-list>";
        node = makeNode(scope, html);
        return node;
      };
      makeComboControl = function(scope, value, comboname) {
        var html;
        html = "<sb-combo label='"+ value.desc +"' ng-model='target.filter." + value.name 
          + "' field-id='in_" + value.name
          + "' elements='target[\"" + comboname + "\"]'></select>";
        return makeNode(scope, html);
      };
      return {
        restrict : "E",
        scope : {
          value : "=", target : "="
        },
        link : function(scope, element, attrs) {
          var value, node, comboname;
          value = scope.value;
          switch (value.type) {
            case "date":
              node = makeDateControl(scope, value);
              break;
            default:
              comboname = "combo-" + value.name;
              node = scope.target[comboname] ? 
                  makeComboControl(scope, value, comboname) : 
                  makeInputControl(scope, value);
          }
          element.append(node);
        }
      };
    }]);

})(sibbac20, angular);