/* componentes genericos para sibbac 20*/

/*carga isin,camara o alias*/

(function ($) {
    $.fn.extend({
        loadselectcombo: function (id, fnOnSelect) {
            var contenedor = $(this);
            switch (id) {
                case 'isin':
                    loadisin(contenedor);
                    break;
                case 'camara':
                    loadcamara(contenedor);
                    break;
                case 'alias':
                    loadalias(contenedor, fnOnSelect);
                    break;
            }
        }
    });
})(jQuery)

//Configura la ventana de mensajes.( Dialog )
$( "#mensaje" ).dialog({
    autoOpen: false,
    modal: false,
    width: 500,
    buttons: [
	        {
	          text: "Aceptar",
	          class:"btn mybutton",
	          icons: {
	            primary: "ui-icon-check"
	          },
	          click: function() {
	              $( this ).dialog( "close" );
	          }
	        }],
    show: {
      effect: "blind",
      duration: 1000
    },
    hide: {
      effect: "explode",
      duration: 1000
    }
});

function loadalias(contenedor, fnOnSelect) {
    $(contenedor).append('<label for="textAlias" class="desresumen">Alias:</label> <input type="text" id="textAlias" class="anchoimp">');
    cargarAlias(fnOnSelect);
}

function loadisin(contenedor) {
    $(contenedor).append('<label for="textIsin" class="desresumen">Isin:</label> <input type="text" id="textIsin" class="anchoimp">');
    cargarIsin();
}
function cargarAlias(fnOnSelect)
{

    var data = new DataBean();
    data.setService('SIBBACServiceConsultaNetting');
    data.setAction('TodosAlias');

    var request = requestSIBBAC(data);
    request.success(function (json) {
        if (json.resultados == null)
        {
            alert(json.error);
        }
        else
        {
            var datos = json.resultados.lista;
            var configuration = {};

            for (var k in datos) {
                item = datos[ k ];

                idOcultosAlias.push(item.id);
                ponerTag = item.id + " - " + item.descripcion;
                availableTagsAlias.push(ponerTag);
            }
            if (fnOnSelect === undefined) {
                configuration = {
                    source: availableTagsAlias
                }
            } else {
                configuration = {
                    source: availableTagsAlias,
                    select: fnOnSelect
                }
            }
            $("#textAlias").autocomplete(configuration);
        }
    });

}

function cargarIsin()
{

    var data = new DataBean();
    data.setService('SIBBACServiceConsultaNetting');
    data.setAction('TodosIsin');

    var request = requestSIBBAC(data);
    request.success(function (json) {
        if (json.resultados == null)
        {
            alert(json.error);
        }
        else
        {
            var datos = json.resultados.lista;

            for (var k in datos) {
                item = datos[ k ];

                idOcultosIsin.push(item.id);
                ponerTag = item.id + " - " + item.descripcion;
                availableTagsIsin.push(ponerTag);

            }

            $("#textIsin").autocomplete({
                source: availableTagsIsin
            });

        }
    });

}







