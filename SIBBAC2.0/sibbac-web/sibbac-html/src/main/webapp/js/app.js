var sibbac20 = angular.module('sibbac20', [ 'ngRoute', 'ngAnimate', 'ngResource', 'angular-growl', 'ngDialog', 'angucomplete-alt',
                                           'angular-jquery-datepicker', 'angularMoment', 'blockUI',  
                                           'ui.grid', 'ui.grid.selection', 'ui.grid.i18n', 'ui.grid.pagination','ui.grid.resizeColumns']);

sibbac20.factory('httpRequestInterceptor', function ($q, $location, $rootScope, growl) {
  return {
    request : function (config) {
      return config;
    },
    response : function (config) {
      return config;
    },
    'responseError' : function (rejection) { // no autorizado
      if (rejection.status === 401) {
        growl.addErrorMessage("No tiene permiso de acceso a la acción solicitada");
      }
      return $q.reject(rejection);
    }
  };
});

sibbac20.run(function ($rootScope, amMoment, blockUIConfig, SecurityService, ServerVarsService, Logger, $location, growl, i18nService, $locale) {
      i18nService.setCurrentLang("es");
      $locale.NUMBER_FORMATS.GROUP_SEP = ",";
      $locale.NUMBER_FORMATS.DECIMAL_SEP = ".";

      $rootScope.restParams = {
        serverUrl : "/sibbac20back/rest/service/"
      };
      amMoment.changeLocale('es');
      Logger.info("Angular core arrancado.");
      // angular-block-ui
      blockUIConfig.message = "Por favor, espere ...";
      blockUIConfig.autoBlock = false;
      blockUIConfig.template = '<h1><img style="vertical-align: middle" src="images/loading.gif" /> Cargando datos ...</h1>';

      // MFG 29/09/2016 se obtiene el nodo.
      ServerVarsService.getServerVars(onSuccessServerVarsRequest, onErrorRequest);

      function onSuccessServerVarsRequest (data) {
        if (data.error !== undefined && data.error !== null && data.error !== "") {
          growl.addErrorMessage(data.error);
        } else if (data.resultados !== undefined && data.resultados.server_node !== "") {
          $rootScope.serverNode = data.resultados.server_node;
          $rootScope.version   =data.resultados.version;
          growl.addInfoMessage("nodo: " + $rootScope.serverNode);
        }
      }

      function onErrorRequest (err) {
        growl.addErrorMessage(err);
      }

      $rootScope.doLogout = function () {
        SecurityService.logout(function (data) {
          $rootScope.authenticated = false;
          $rootScope.userName = "";
          $location.path('/login');
        }, function (error) {
          growl.addErrorMessage("No se pudo verificar sesión usuario");
        }, {});
      };

      $rootScope.openModalCambioContrasena = function () {
    	  
    	  $rootScope.$broadcast("abreModalContraseña");
    	  angular.element('#cambio_contrasena_modal').modal({
              backdrop : 'static'
          });
          $(".modal-backdrop").remove();
      };

      Logger.info("Seguridad activada: " + security.active);
      $rootScope.userName = (SecurityService.user == null) ? "" : SecurityService.user.name;
      $rootScope.authenticated = (security.active) ? false : true;
      $rootScope.permisosCargados = (security.active) ? false : true;
      $rootScope.securityActive = security.active;
      Logger.info("Autenticado: " + $rootScope.authenticated);

      $rootScope.isAuthenticated = function () {
        return $rootScope.authenticated;
      }

      $rootScope.SecurityActions = {
        CONSULTAR : "Acceso",
        CREAR : "Crear",
        MODIFICAR : "Modificar",
        BORRAR : "Borrar",
        EXPORTAR_EXCEL : "Exportar Excel",
        IMPORTAR_FICHERO : "Importar Fichero",
        ASIGNAR_ALIAS : "Asignar Alias",
        ASIGNAR_SUBCTA : "Asignar SubCta",
        CANCELACION : "Cancelación",
        CANCELACION_TOTAL : "Cancelación Total",
        CHEQUEAR_ERRORES : "Chequear Errores",
        CREAR_SUSTITUIR : "Crear/Sustituir",
        VALIDAR : "Validar",
        EXPORTAR_TERCEROS : "Exportar Excel Terceros",
        ACTIVAR_DESACTIVAR : "Activar/Desactivar",
        ANADIR_DEFECTO : "Añadir Defecto",
        ANADIR_ALIAS : "Añadir Alias",
        TRADUCIR : "Traducir",
        ALTA_SUBLISTADO : "Alta Sublistado",
        EDITAR_SUBLISTADO : "Editar Sublistado",
        BORRAR_SUBLISTADO : "Borrar Sublistado",
        REENVIAR : "Reenviar"
      }

      if (security.active) {

        SecurityService.init()
            .then(
                  function () {

                    Logger.info("Seguridad inicializada ");
                    Logger.info("Autenticado en servidor: " + SecurityService.isAuthenticated());

                    setInterval(function () {
                      SecurityService.isSessionEnabled(function (data) {
                        // just a ping
                    	  if(data.resultados.autenticado == false){
                    		  $rootScope.doLogout();
                    	  }                    	  
                      }, function (error) {
                        $rootScope.doLogout();
                      });
                    }, 60000);

                    $rootScope.userName = (SecurityService.user == null) ? "" : SecurityService.user.name;
                    $rootScope.environment = (SecurityService.environment == null) ? "" : SecurityService.environment;

                    $rootScope.authenticated = SecurityService.isAuthenticated();
                    $rootScope.permisosCargados = true;
                    
                    if($rootScope.authenticated){
                    	Logger.info("Inicio carga de menú");
                    	SecurityService.getMenu(function (data) {
                    		$rootScope.menu = data.resultados.menu;
                    		for (a = 0, lena = $rootScope.menu.length; a < lena; a++) {
                    			for (b = 0, lenb = $rootScope.menu[a].submenu.length; b < lenb; b++) {
                    				for (c = 0, lenc = $rootScope.menu[a].submenu[b].submenu.length; c < lenc; c++) {
                    					for (d = 0, lend = $rootScope.menu[a].submenu[b].submenu[c].submenu.length; d < lend; d++) {
                    						setPermisoOpcionMenu($rootScope.menu[a].submenu[b].submenu[c].submenu[d]);
                    					}
                    					setPermisoOpcionMenu($rootScope.menu[a].submenu[b].submenu[c]);
                    				}
                    				setPermisoOpcionMenu($rootScope.menu[a].submenu[b]);
                    			}
                    			setPermisoOpcionMenu($rootScope.menu[a]);
                    		}
                    		Logger.info("Fin carga de menú");
                    	}, function (error) {
                    		Logger.error("Error en la carga del menu: " + error);
                    	});
                    }
                    else{
                    	$location.path('/login');
                    }

                    var setPermisoOpcionMenu = function (opcion) {
                      if (opcion.url != null) {
                        // MFG 04/10/2016 Se quita el # en la comprobacion.
                        opcion.active = SecurityService.isPermisoAccion($rootScope.SecurityActions.CONSULTAR,
                                                                        opcion.url.trim().replace('#', ''));
                        // Logger.info(opcion.url.trim() + "-" + opcion.active);
                      } else {
                        var activado = false;
                        for (i = 0, leni = opcion.submenu.length; i < leni; i++) {
                          if (opcion.submenu[i].active) {
                            activado = true;
                          }
                        }
                        opcion.active = activado;
                      }
                    }

                    // intercepción de cambio de ruteo
                    $rootScope.$on("$routeChangeStart", function (event, next, current) {
                      if (security.active) {
                        if (next != null && !angular.isUndefined(next.$$route)) {
                          var path = next.$$route.originalPath, permiso;
                          if (path == "/login")
                            return;
                          if (path == "/logout")
                            return;
                          if (path == "/")
                            return;
                          if (path.startsWith("/desglose/manual")) {
                            //path = "/desglose/manual";
                            return; //TODO: Determinar claramente la navegacion
                          }
                          permiso = SecurityService.isPermisoPagina(path);
                          Logger.info("Permiso para " + path + "es " + permiso);
                          if (!permiso) {
                            event.preventDefault();
                            growl.addErrorMessage("No tiene permiso de acceso para acceder a la página solicitada");
                          }
                        } else {
                          growl.addErrorMessage("No se ha encontrado la página solicitada");
                        }
                      }
                      return;
                    });
                    $rootScope.isPermisoAccion = function (accion) {
                      var pagina = $location.path();
                      return SecurityService.isPermisoAccion(accion, pagina);
                    }

                    Logger.info("Fin carga seguridad");

                    $rootScope.$broadcast(security.event.initialized);

                  });

      }

    });
