sibbac20.factory('Logger', function() {
		return {
			log : function(message) {
				if (environment.debug) {
					if (typeof message == 'object') {
						console.log("[INFO]: " + message.constructor.name);
						console.log(message);
					} else {
						console.log("[INFO]: " + message);
					}
				}
			},
			info : function(message) {
				if (environment.debug) {
					if (typeof message == 'object') {
						console.log("[INFO]:" + message.constructor.name);
						console.log(message);
					} else {
						console.log("[INFO]: " + message);
					}
				}
			},
			warn : function(message) {
				if (environment.debug) {
					if (typeof message == 'object') {
						console.log("[WARN]:" + message.constructor.name);
						console.log(message);
					} else {
						console.log("[WARN]: " + message);
					}
				}
			},
			error : function(message) {
				if (environment.debug) {
					if (typeof message == 'object') {
						console.log("[ERROR]:" + message.constructor.name);
						console.log(message);
					} else {
						console.log("[ERROR]: " + message);
					}
				}
			}
		};
	});