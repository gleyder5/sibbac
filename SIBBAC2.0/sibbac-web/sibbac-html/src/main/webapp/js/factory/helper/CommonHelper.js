/**
 *	Helper Generico como factory. 
 */
sibbac20.factory('commonHelper', function(){

	return {
		/**
		 *	Devuelve true si el valor pasado es null o indefinido.
		 *	@param valor
		 *	@return booleano 
		 */
		isUndefinedOrNull: function(val) {
			return angular.isUndefined(val) || val === null;
		},
		/**
		 *	Devuelve true si el valor pasado como array es null o esta vacio.
		 *	@param valor
		 *	@return booleano 
		 */
		isArrayUndefinedOrNullOrEmpty: function(val) {
			return angular.isUndefined(val) || val === null || ((!angular.isUndefined(val) && !(val === null)) && val.length == 0);
		},
		/**
		 *	Devuelve true si el valor pasado es null o indefinido o vacio.
		 *	@param valor
		 *	@return booleano 
		 */
		isUndefinedOrNullOrEmpty: function(val) {
			return angular.isUndefined(val) || val === null || (!angular.isUndefined(val) && val === '');
		},
		/**
		 *	Convierte una cadena con el formato a 'YYYY-MM-DD'.
		 *	@param valor
		 *	@return fecha formateada 
		 */
		formatDateStr: function (strDate, format) {
			var parts = strDate.split('/');
			var mydate = new Date(parts[2], parts[1] - 1, parts[0]);
			return moment(mydate).format(format);
		},
		/**
		 *	Convierte Date a una cadena con el formato dd/mm/yyyy.
		 *	@param fecha
		 *	@return valor formateado 
		 */
		dateToStrFormat: function (fecha) {
			var dd = (Number(fecha.getDate()) < 10 ? ('0' + fecha.getDate()) : fecha.getDate());
			var mm = (Number(fecha.getMonth()) < 10 ? ('0' + (fecha.getMonth() + 1)) : (fecha.getMonth() + 1));
			var y = fecha.getFullYear();
			return dd + '/' + mm + '/' + y;
		},
		/**
		 * 	Devuelve un objeto Date transformado con momentjs de una expresion de caracteres.
		 * 	@param cadena de caracteres
		 * 	@param patron de representacion por ejemplo: 'DD/MM/YYYY'
		 */
		getDateObjectFromString: function (value, pattern) {
			if (!this.isUndefinedOrNullOrEmpty(value)) {
				return moment(value, pattern);
			}
			return null;
		},
		/**
		 * 	Obtiene el elemento seleccionado de una lista.
		 *	@param listado de elementos
		 *	@return el elemento seleccionado 
		 */
		getSelectedElement: function($elementosLista) {
			var $elemListaSelected = undefined;
			if (!this.isUndefinedOrNull($elementosLista)) {
				for (var elem = 0; elem < $elementosLista.length; elem++) {
					if ($elementosLista[elem].selected) {
						$elemListaSelected = $elementosLista[elem];
					}
				}
			}
			return $elemListaSelected;
		},
		/**
		 * 	Se obtiene indice de elemento seleccionado.
		 * 	@param listado de elementos
		 * 	@param elemento seleccionado
		 * 	@return indice del elemento
		 */
		getIndiceFromElementSelected: function(listadoElemsSelected, elementoSeleccionado) {
			// Encontrado el elemento seleccionado se lo quita de la lista.
			var indice = undefined;
			if (!this.isUndefinedOrNull(listadoElemsSelected)) {
				for (var i = 0; i < listadoElemsSelected.length; i++) {
					if ((elementoSeleccionado != null ? elementoSeleccionado.label.trim() : '') === listadoElemsSelected[i].value.trim()) {
						indice = i;
						console.log(listadoElemsSelected[i]);
					}
				}				
			}
			return indice;
		},
		/**
		 * 	Permite solo enteros en el evento del campo que se aplique.
		 * 	@param $element: elemento a evaluar ligado al evento del elemento HTML
		 */
		allowOnlyIntegers: function ($element) {
			// Solo se van a permitir numeros naturales en los campos tipo Number.
			$element.on('keypress', function(event) {
				if ( !isIntegerOnly() ) {
					event.preventDefault();
				}
				function isIntegerOnly() {
					return /[0-9]/.test(String.fromCharCode(event.which));
				}
			});			
		},
		/**
		 *	Devuelve true si las fechas que se le pasan respetan el rango desde-hasta 
		 *	o alguna de ellas esta vacia.
		 *	@param fechaDesde como string
		 *	@param fechaHasta como string
		 *	@param format: formato a evaluar
		 *	@return boolean 
		 */
		isRangoValidoFechas: function(fechaDesdeStr, fechaHastaStr, format) {
			if (this.isUndefinedOrNullOrEmpty(fechaDesdeStr)) {
				return true; 
			}
			if (this.isUndefinedOrNullOrEmpty(fechaHastaStr)) {
				return true; 
			}
			var fechaDesde = moment(fechaDesdeStr, format);
			var fechaHasta = moment(fechaHastaStr, format);
			return fechaDesde.isBefore(fechaHasta) || fechaDesde.isSame(fechaHasta);
		},
		/**
		 * 	Devuelve indice en base al elemento seleccionado - usado para listas.
		 * 	@param elemento
		 * 	@param listaSeleccionados 
		 * 	@return indice	
		 */
		getIndexBySelectedElem: function (elemento, listaSeleccionados) {
			var indice = null;
			if (!this.isUndefinedOrNull(elemento)) {
				for (var i = 0; i < listaSeleccionados.length; i++) {
					if (listaSeleccionados[i].value == elemento.valorCampo.value) {
						indice = i;
						break;
					}
				}				
			}
			return indice;
		},
		/**
		 *	Se obtiene fecha numerica para campo tipo FECHA.
		 *	@param textoButtonCambioFecha
		 *	@param numberFecha
		 *	@return fechaNumerica
		 */
		getFechaNumerica: function (textoButtonCambioFecha, numberFecha) {
			var fechaNumerica = null;
			if (textoButtonCambioFecha == '+') {
				fechaNumerica = moment().add(Number(numberFecha), 'days');
			} else if (textoButtonCambioFecha == '-') {
				fechaNumerica = moment().add(Number(numberFecha) * -1, 'days');
			} else {
				fechaNumerica = moment();
			}
			return fechaNumerica;
		},
		/**
		 *	Permite operar con distintas fuentes de fecha para campo tipo FECHA
		 * 	Devuelve true si el rango es valido.  
		 */
		isFechaRangoValida: function (fechaCampoDesde, fechaCampoHasta, fechaNumericaDesde, fechaNumericaHasta) {
			var fechaDesde = !this.isUndefinedOrNullOrEmpty(fechaCampoDesde) ? fechaCampoDesde : (!this.isUndefinedOrNullOrEmpty(fechaNumericaDesde) ? fechaNumericaDesde : '');
			var fechaHasta = !this.isUndefinedOrNullOrEmpty(fechaCampoHasta) ? fechaCampoHasta : (!this.isUndefinedOrNullOrEmpty(fechaNumericaHasta) ? fechaNumericaHasta : '');
			if (this.isUndefinedOrNullOrEmpty(fechaDesde) || this.isUndefinedOrNullOrEmpty(fechaHasta)) {
				return true;	
			} else {
//				return (fechaDesde.isBefore(fechaHasta) || fechaDesde.isSame(fechaHasta));		
				return (fechaDesde <= fechaHasta);
			}
		},
		/**
	     *	Permite ajustar la validacion de rango de fechas.
	     *	@param $scope
	     *	@param elemento que se itera
	     *	@param patron de fechas ej. 'DD/MM/YYYY' 
	     */
		ajustarValidacionRangoFechas: function ($scope, elemento, patron) {
		   var fechaDesde = this.getDateObjectFromString(elemento.valorCampo, patron);
		   var fechaHasta = this.getDateObjectFromString(elemento.valorCampoHasta, patron);
		   var fechaNumerica = null;
		   var fechaNumericaHasta = null;
		   if (this.isUndefinedOrNullOrEmpty(fechaDesde)) {
			   if (this.isUndefinedOrNullOrEmpty(elemento.valorNumeroFechaDesde)) {
				   fechaDesde = new Date(1900,1,1); 
			   } else {
				   fechaNumerica = this.getFechaNumerica(elemento.lblBtnSentidoFiltro, elemento.valorNumeroFechaDesde);
			   }
			   
		   }
		   if (this.isUndefinedOrNullOrEmpty(fechaHasta)) {
			   if (this.isUndefinedOrNullOrEmpty(elemento.valorNumeroFechaHasta)) {
				   fechaHasta = new Date(9999,12,31); 
			   } else {
				   fechaNumericaHasta = this.getFechaNumerica(elemento.lblBtnSentidoFiltroHasta, elemento.valorNumeroFechaHasta);
			   }
		   }
		   if (!this.isFechaRangoValida(
				   fechaDesde, fechaHasta, fechaNumerica, fechaNumericaHasta)) {
			   elemento['error_rango_fechas_' + elemento.id] = true;
			   $scope.errorRangoFechas = true;
		   } else {
			   elemento['error_rango_fechas_' + elemento.id] = false;
			   $scope.errorRangoFechas = false;
		   }
	   },
	   /**
	    *	Permite chequear el mail.
	    *	@valor valor a evaluar  
	    * 	@return si el mail es valido o no
	    */
	   isMailValido : function(valor) {
		    var regexp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		    return regexp.test(valor) || this.isUndefinedOrNullOrEmpty(valor); 
	   },
	   /**
	    *	Permite chequear ruta de sistema de archivos valida.
	    *	@valor valor a evaluar  
	    * 	@return si la ruta es valida o no
	    */
	   isRutaValida : function(valor) {
		    var regexp = /^[\/]([A-Za-z0-9_\-\.\/])+[\/]$/;
		    return regexp.test(valor); 
	   }
	}
});
