/**
 *	Helper Generico para los campos dinamicos usado como factory. 
 */
sibbac20.factory('camposDinamicosHelper', ['CONSTANTES', 'commonHelper', 'DirectivasService', function(CONSTANTES, commonHelper, DirectivasService) {
	return {
		/**
		 *	Permite vaciar los datos de filtrado. 
		 */
		vaciarFiltrosComp: function($scope) {
			console.log($scope);
			if (!commonHelper.isUndefinedOrNullOrEmpty($scope.arrComponenteDirectiva)) {
				$scope.arrComponenteDirectiva = [];
				$scope.datosFiltroInformeByIdInforme = [];
			}
		},
		/**
		 *	Se obtiene una cadena de caracteres con datos de filtrado. 
		 */
		getCadenaFiltroCamposDinam: function($scope) {
			var cadenaFiltros = ' ';
			if (!commonHelper.isUndefinedOrNullOrEmpty($scope.filtro.listaFiltrosDinamicos)) {
				angular.forEach($scope.filtro.listaFiltrosDinamicos, function(valueList, indexList) {
					angular.forEach(valueList, function(value, index) {
						if (value.tipo.trim() == CONSTANTES.TIPO_LISTA) {
							if (!commonHelper.isArrayUndefinedOrNullOrEmpty(value.listaSeleccSelectDTO)) {
								cadenaFiltros += ' ' + value.nombre + ": ";
								for (var j = 0; j < value.listaSeleccSelectDTO.length; j++) {
									// Lista
									cadenaFiltros += value.listaSeleccSelectDTO[j].value + ', ';							
								}
							}
						} else if (value.tipo.trim() == CONSTANTES.TIPO_FECHA) {
							// Fecha
							// Se verifica si es desde-hasta.
							if (!commonHelper.isUndefinedOrNull(value.subTipo) && value.subTipo.trim() == CONSTANTES.SUBTIPO_DESDE_HASTA) {
								var fechaDesde = null;
								var fechaHasta = null;
								if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorCampo)) {
									fechaDesde = value.valorCampo;
								} else {
									//var fdMjs = commonHelper.getFechaNumerica(value.lblBtnSentidoFiltro, value['number_' + value.id]);
									var fdMjs = null;
									if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorNumeroFechaDesde)) {
										fdMjs = commonHelper.getFechaNumerica(value.lblBtnSentidoFiltro, value.valorNumeroFechaDesde);
										// Se quita el signo de la expresion valor ya que este se maneja en back con 'value.lblBtnSentidoFiltro'
										fechaDesde = (!commonHelper.isUndefinedOrNullOrEmpty(fdMjs) ? fdMjs.format('DD/MM/YYYY') : '');
									} else {
										if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorCampoHasta) || !commonHelper.isUndefinedOrNullOrEmpty(value.valorNumeroFechaHasta)) {
											fechaDesde = '01/01/1900';
										}
									}
									value.valorCampo = fechaDesde;
									value.valorNumeroFechaDesde = null;
								}
								if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorCampoHasta)) {
									fechaHasta = value.valorCampoHasta;
								} else {
									var fhMjs = null;
									if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorNumeroFechaHasta)) {
										fhMjs = commonHelper.getFechaNumerica(value.lblBtnSentidoFiltroHasta, value.valorNumeroFechaHasta);
										// Se quita el signo de la expresion valor ya que este se maneja en back con 'value.lblBtnSentidoFiltroHasta'
										fechaHasta = (!commonHelper.isUndefinedOrNullOrEmpty(fhMjs) ? fhMjs.format('DD/MM/YYYY') : '');
									} else {
										if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorCampo) || !commonHelper.isUndefinedOrNullOrEmpty(value.valorNumeroFechaDesde)) {
											fechaHasta = '31/12/9999';
										}
									}
									value.valorCampoHasta = fechaHasta;
									value.valorNumeroFechaHasta = null;
								}
								if (!commonHelper.isUndefinedOrNullOrEmpty(fechaDesde)) {
									cadenaFiltros += value.nombre + ' desde: ' + fechaDesde + ', ';
								}
								if (!commonHelper.isUndefinedOrNullOrEmpty(fechaHasta)) {
									cadenaFiltros += value.nombre + ' hasta: ' + fechaHasta + ', ';
								}
							} else {
								// Se verifica si la fecha es por campo de fechas o por campo numerico.
								if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorCampo)) {
									cadenaFiltros += value.nombre + ': ' + value.valorCampo + ', ';							
								} else if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorNumeroFechaDesde)){
									var fechaNumerica = commonHelper.getFechaNumerica(value.lblBtnSentidoFiltro, value['number_' + value.id]);
									if (!commonHelper.isUndefinedOrNullOrEmpty(fechaNumerica)) {
										cadenaFiltros += value.nombre + ': ' + (!commonHelper.isUndefinedOrNullOrEmpty(fechaNumerica) ? fechaNumerica.format('DD/MM/YYYY') : '') + ', ';
									}
								}
							}
						} else {
							if (!commonHelper.isUndefinedOrNullOrEmpty(value.valorCampo)) {
								cadenaFiltros += ', ' + value.nombre  + ": " + value.valorCampo;						
							}
						}
					})
				})
			}
			return cadenaFiltros;
		},
		/**
		 * 	Se obtienen los id's en el componente generico para ser iterados.
		 */ 
		getElementosByParams: function ($scope) {
			 var idPlantilla = $scope.plantilla != null ? $scope.plantilla.key : 0;
			 DirectivasService.devolverElementosByParams(function (data) {
				 $scope.datosFiltroInformeByIdInforme = data.resultados["datosFiltroInformeByIdInforme"];
			 }, function (error) {
				 fErrorTxt("Se produjo un error al cargar ids de elementos.", 1);
			 }, {
		         "idInforme" : Number(idPlantilla)
			 });
		},
		
	   /**
	    * 	Se inicializan datos del scope.
	    * 	@param $scope
	    */
	   inicializarScope: function($scope) {
		   /* Inicializacion de datos para FECHA. */
		   $scope.btnsFecha = {};
		   $scope.btnsFecha.btnCambioFecha = "vacio";
		   $scope.btnsFecha.btnCambioFechaHasta = "vacio";
		   $scope.errorRangoFechas = false;
	   },
	   /**
	    *	Aqui se depositan los eventos de los botones y campos
	    *	que se usan para el calculo de fechas.
	    *	@param $scope
	    *	@param valor: instrucciones del evento
	    *	@param elemento: elemento del campo de fecha
	    */
	   operarFecha: function ($scope, valor, elemento) {
		   switch (valor) {
		   case 'cambiar':
			   // Logica para cambio de fecha en boton.
			   console.log('modif -> ' + elemento.lblBtnSentidoFiltro);
			   if (elemento.lblBtnSentidoFiltro == "+") {
				   elemento.lblBtnSentidoFiltro = "-";
				   elemento.inhabilitar = false;
				   elemento.inhabilitarNumerico = false;
			   } else if (elemento.lblBtnSentidoFiltro == "-") {
				   elemento.lblBtnSentidoFiltro = "+";
				   elemento.inhabilitar = false;
				   elemento.inhabilitarNumerico = false;
			   }
			   
			   // Solo tiene sentido para subtipo DESDE-HASTA
			   if (elemento.subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA) {
				   // Validacion fecha desde-hasta.
				   commonHelper.ajustarValidacionRangoFechas($scope, elemento, 'DD/MM/YYYY');
			   }
			   break;
		   case 'vaciar':
			   // Vaciar el campo de fecha desde
			   elemento.valorCampo = '';
			   // Vaciar el campo de numeros de fecha desde
			   elemento.valorNumeroFechaDesde = '';
			   // Poner el boton de cambio de fecha a "="
			   elemento.lblBtnSentidoFiltro = "+";
			   // Habilitar el campo de fecha desde
			   elemento.inhabilitar = false;
			   // Habilitar el campo numerico desde
			   elemento.inhabilitarNumerico = false;
			   // Deshabilitar errores de rango de fechas
			   $scope.errorRangoFechas = false;
			   elemento['error_rango_fechas_' + elemento.id] = false;
			   break;
		   case 'calendario':
			   // Se vacia campo numerico fecha desde
			   elemento.valorNumeroFechaDesde = '';
			   // Se inhabilita campo numerico de fecha desde
			   elemento.inhabilitarNumerico = true;
			   
			   // Solo tiene sentido para subtipo DESDE-HASTA
			   if (elemento.subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA) {
				   // Se valida el rango de fechas desde-hasta
				   commonHelper.ajustarValidacionRangoFechas($scope, elemento, 'DD/MM/YYYY');
			   }
			   break;
		   case 'numerico':
			   // Se vacia campo de fecha desde
			   elemento.valorCampo = '';
			   // Se habilita campo numerico de fecha desde
			   elemento.inhabilitarNumerico = false;

			   // Si el campo numerico de fechas esta vacio se habilita campo de fechas
			   // Caso contrario se deshabilita dicho campo de fechas
			   if (elemento.valorNumeroFechaDesde === '') {
				   elemento.inhabilitar = false;
			   } else {
				   elemento.inhabilitar = true;
			   }
			   // Solo tiene sentido para subtipo DESDE-HASTA
			   if (elemento.subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA) {
				   // Validar fechas desde-hasta
				   commonHelper.ajustarValidacionRangoFechas($scope, elemento, 'DD/MM/YYYY');
			   }
			   break;
		   case 'cambiarHasta':
			   if (elemento.lblBtnSentidoFiltroHasta == "+") {
				   elemento.lblBtnSentidoFiltroHasta = "-";
			   } else if (elemento.lblBtnSentidoFiltroHasta == "-") {
				   elemento.lblBtnSentidoFiltroHasta = "+";
				   elemento.inhabilitarHasta = false;
				   elemento.inhabilitarNumericoHasta = false;
			   }
			   // Solo tiene sentido para subtipo DESDE-HASTA
			   if (elemento.subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA) {
				   // Validar fechas desde-hasta
				   commonHelper.ajustarValidacionRangoFechas($scope, elemento, 'DD/MM/YYYY');
			   }
			   break;
		   case 'vaciarHasta':
			   // Vaciar el campo de fecha hasta
			   elemento.valorCampoHasta = '';
			   // Vaciar el campo de numeros de fecha hasta
			   elemento.valorNumeroFechaHasta = '';
			   // Poner el boton de cambio de fecha a "="
			   elemento.lblBtnSentidoFiltroHasta = "+";
			   // Habilitar el campo de fecha hasta
			   elemento.inhabilitarHasta = false;
			   // Habilitar el campo numerico hasta
			   elemento.inhabilitarNumericoHasta = false;
			   // Deshabilitar errores de rango de fechas
			   $scope.errorRangoFechas = false;
			   elemento['error_rango_fechas_' + elemento.id] = false;
			   break;
		   case 'calendarioHasta':
			   // Vaciar campo numerico hasta
			   elemento.valorNumeroFechaHasta = '';
			   
			   // Inhabilitar campo numerico hasta
			   elemento.inhabilitarNumericoHasta = true;

			   // Solo tiene sentido para subtipo DESDE-HASTA
			   if (elemento.subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA) {
				   // Validacion fecha desde-hasta.
				   commonHelper.ajustarValidacionRangoFechas($scope, elemento, 'DD/MM/YYYY');
			   }
			   break;
		   case 'numericoHasta':
			   // Se vacia campo fecha hasta
			   elemento.valorCampoHasta = '';
			   // Habilitar el campo numerico hasta
			   elemento.inhabilitarNumericoHasta = false;

			   // Se habilita campo fecha hasta si el campo numerico hasta
			   // esta vacio, caso contrario se deshabilita
			   if (elemento.valorNumeroFechaHasta === '') {
				   elemento.inhabilitarHasta = false;
			   } else {
				   elemento.inhabilitarHasta = true;
			   }
			   
			   // Solo tiene sentido para subtipo DESDE-HASTA
			   if (elemento.subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA) {
				   // Validacion fecha desde-hasta.
				   commonHelper.ajustarValidacionRangoFechas($scope, elemento, 'DD/MM/YYYY');
			   }
			   break;
		   default:
			   break;
		   }
	   },
	   /**
	    *	Aqui se depositan los eventos de los botones y campos
	    *	que se usan para el tipo de busqueda.
	    *	@param $scope
	    *	@param valor: instrucciones del evento
	    *	@param elemento: elemento del campo
	    */
	   operarListaTexto: function ($scope, valor, elemento) {
		   switch (valor) {
		   case 'cambiar':
			   if (elemento.lblTipoBusqueda == "I") {
				   elemento.lblTipoBusqueda = "D";
			   } else if (elemento.lblTipoBusqueda == "D") {
				   elemento.lblTipoBusqueda = "C";
			   } else if (elemento.lblTipoBusqueda == "C") {
				   elemento.lblTipoBusqueda = "NC";
			   } else if (elemento.lblTipoBusqueda == "NC") {
				   elemento.lblTipoBusqueda = "I";
			   }
			   break;
		   }
	   },
	   
	   /**
	    *	Cambio de sentido del autorrellenable.
	    *	@param elemento 
	    */
	   cambiarSentidoAutorr: function (elemento) {
		   if (elemento.lblBtnSentidoFiltro == '=') {
			   elemento.textoAuxiliar = "DISTINTO";
			   elemento.lblBtnSentidoFiltro = "<>";
		   } else {
			   elemento.textoAuxiliar = "IGUAL";
			   elemento.lblBtnSentidoFiltro = "=";
		   }
	   },
	   /**
	    * 	Convierte las fechas numericas en fecha de calendario para el DTO del back.
	    * 	@param arrComponenteDirectiva
	    */
	   getFechasFormateadas: function (arrComponenteDirectiva) {
		   if (!commonHelper.isUndefinedOrNullOrEmpty(arrComponenteDirectiva)) {
			   for (var j = 0; j < arrComponenteDirectiva.length; j++) {
				   if (arrComponenteDirectiva[j].tipo == CONSTANTES.TIPO_FECHA) {
					   // Solo fecha desde
					   var numericoDesde = arrComponenteDirectiva[j]['number_' + arrComponenteDirectiva[j].id];
					   if (commonHelper.isUndefinedOrNullOrEmpty(arrComponenteDirectiva[j].valorCampo) 
							   && !commonHelper.isUndefinedOrNullOrEmpty(numericoDesde)) {
						   arrComponenteDirectiva[j].valorCampo = (commonHelper.getFechaNumerica(
								   arrComponenteDirectiva[j].lblBtnSentidoFiltro, numericoDesde)).format('DD/MM/YYYY');
					   }
					   // Se complementa con fecha hasta en caso de ser necesario
					   if (arrComponenteDirectiva[j].subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA) {
						   var numericoHasta = arrComponenteDirectiva[j]['number_' + arrComponenteDirectiva[j].id + '_hasta'];
						   if (commonHelper.isUndefinedOrNullOrEmpty(arrComponenteDirectiva[j].valorCampoHasta) 
								   && !commonHelper.isUndefinedOrNullOrEmpty(numericoHasta)) {
							   arrComponenteDirectiva[j].valorCampoHasta = (commonHelper.getFechaNumerica(
									   arrComponenteDirectiva[j].lblBtnSentidoFiltroHasta, numericoHasta)).format('DD/MM/YYYY');
						   }
					   }
				   }
			   }
		   }
	   }
	}
}]);