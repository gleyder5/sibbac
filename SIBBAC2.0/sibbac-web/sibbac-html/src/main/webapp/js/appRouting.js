sibbac20.config(function ($routeProvider, $locationProvider) {
  // $locationProvider.html5Mode(true);
  $routeProvider.when('/', {
    templateUrl: 'views/main.html',
    controller: 'MainController'
  })
    /** LOGIN * */
    .when('/login', {
      templateUrl: 'views/security/login.html',
      controller: 'LoginController'
    })
    /** LOGOUT * */
    .when('/logout', {
      templateUrl: 'views/security/login.html',
      controller: 'LoginController'
    })
    /** GESTIÓN DE ACCESOS * */
    .when('/gestionAccesos/gestionUsuarios', {
      templateUrl: 'views/gestionAccesos/gestionUsuarios.html',
      controller: 'GestionUsuariosController'
    }).when('/gestionAccesos/gestionPerfiles', {
      templateUrl: 'views/gestionAccesos/gestionPerfiles.html',
      controller: 'GestionPerfilesController'
    }).when('/parametrosSeguridad', {
      templateUrl: 'views/gestionAccesos/parametrosSeguridad.html',
      controller: 'ParametrosSeguridadController'
    })
    /** PLANTILLAS DE INFORMES* */
    .when('/plantillasInformes/generarInformes', {
      templateUrl: 'views/plantillasInformes/generarInformes.html',
      controller: 'GenerarInformesController'
    }).when('/plantillasInformes/generarInformesNEW', {
      templateUrl: 'views/plantillasInformes/generarInformesNEW.html',
      controller: 'GenerarInformesNEWController'
    }).when('/plantillasInformes/plantillasInformes', {
      templateUrl: 'views/plantillasInformes/plantillasInformes.html',
      controller: 'PlantillasInformesController'
    }).when('/plantillasInformes/plantillasInformesNEW', {
      templateUrl: 'views/plantillasInformes/plantillasInformesNEW.html',
      controller: 'PlantillasInformesNEWController'
    }).when('/plantillasInformes/planificacionInformes', {
      templateUrl: 'views/plantillasInformes/planificacionInformes.html',
      controller: 'PlanificacionInformesController'
    })
    /** MANTENIMIENTO DATOS CLIENTE* */
    .when('/mantenimientosDatosCliente/mntoTitulares', {
      templateUrl: 'views/mantenimientoDatosCliente/mantenimientoTitulares.html',
      controller: 'MantenimientoTitularesController'
    }).when('/mantenimientosDatosCliente/cargaFicheroSwift', {
      templateUrl: 'views/mantenimientoDatosCliente/cargaFicheroSwift.html',
      controller: 'CargaFicheroSwiftController'
    }).when('/mantenimientosDatosCliente/gestionClientes', {
      templateUrl: 'views/mantenimientoDatosCliente/gestionClientes.html',
      controller: 'GestionClientesController'
    })
    /** FACTURAS MANUALES **/
    .when('/facturasManuales/facturasManuales', {
      templateUrl: 'views/facturasManuales/facturasManuales.html',
      controller: 'FacturasManualesController'
    }).when('/facturasManuales/bajasManuales', {
      templateUrl: 'views/facturasManuales/bajasManuales.html',
      controller: 'BajasManualesController'
    }).when('/facturasManuales/rectificativasManuales', {
      templateUrl: 'views/facturasManuales/rectificativasManuales.html',
      controller: 'RectificativasManualesController'
    }).when('/facturasManuales/asientosContables', {
      templateUrl: 'views/facturasManuales/asientosContables.html',
      controller: 'AsientosContablesController'
    })
    /** * CONCILIACION * */
    /** titulos * */
    .when('/conciliacion/diaria', {
      templateUrl: 'views/conciliacion/diaria.html',
      controller: 'DiariaController'
    }).when('/conciliacion/clearingTitulos', {
      templateUrl: 'views/conciliacion/clearingTitulos.html',
      controller: 'ClearingTitulosController',
      controllerAs: 'ctrl'
    }).when('/conciliacion/propiaTitulos', {
      templateUrl: 'views/conciliacion/propiaTitulos.html',
      controller: 'PropiaTitulosController'
    })
    /** Conciliación bancaria **/
    .when('/contabilidad/partidasPendientes/conciliacionBancaria', {
      templateUrl: 'views/contabilidad/conciliacionBancaria.html',
      controller: 'ConciliacionBancariaController'
    }).when('/contabilidad/partidasPendientes/controlContabilidad', {
      templateUrl: 'views/contabilidad/controlContabilidad.html',
      controller: 'ControlContabilidadController'
    }).when('/contabilidad/partidasPendientes/historicoPartidasCasadas', {
      templateUrl: 'views/contabilidad/historicoPartidasCasadas.html',
      controller: 'HistoricoPartidasCasadasController'
    }).when('/contabilidad/calendarioContable', {
      templateUrl: 'views/contabilidad/periodos.html',
      controller: 'PeriodosController'
    })
    /** otros * */
    .when('/conciliacion/movimientosManuales', {
      templateUrl: 'views/conciliacion/movimientosManuales.html',
      controller: 'MovimientoManualController'
    }).when('/conciliacion/cuentasEspejo', {
      templateUrl: 'views/conciliacion/cuentasEspejo.html',
      controller: 'CuentasEspejoController'
    }).when('/conciliacion/cuentasVirtuales', {
      templateUrl: 'views/conciliacion/cuentasVirtuales.html',
      controller: 'CuentaVirtualController'
    }).when('/conciliacion/cuentasECC', {
      templateUrl: 'views/conciliacion/cuentasECC.html',
      controller: 'CuentasECCController'
    })
    /** efectivo * */
    .when('/conciliacion/clearingConsulta', {
      templateUrl: 'views/conciliacion/clearingEfectivo.html',
      controller: 'ClearingEfectivoController',
      controllerAs: "ctrl"
    }).when('/conciliacion/provisionEfectivo', {
      templateUrl: 'views/conciliacion/provisionEfectivo.html',
      controller: 'ProvisionEfectivoClearingController'
    }).when('/conciliacion/provisionEfectivaPropia', {
      templateUrl: 'views/conciliacion/provisionEfectivaPropia.html',
      controller: 'ProvisionEfectivoPropiaController'
    })
    /** balance * */
    .when('/conciliacion/balance', {
      templateUrl: 'views/conciliacion/balance.html',
      controller: 'BalanceController'
    }).when('/conciliacion/clearingBalance', {
      templateUrl: 'views/conciliacion/clearingBalance.html',
      controller: 'ClearingBalanceClienteController'
    }).when('/conciliacion/clearingBalanceValor', {
      templateUrl: 'views/conciliacion/clearingBalanceValor.html',
      controller: 'ClearingBalanceValorController'
    }).when('/conciliacion/desglosesMQ', {
      templateUrl: 'views/conciliacion/desglosesMQ.html',
      controller: 'DesgloseMQControler'
    }).when('/conciliacion/desglosesHuerfanos', {
      templateUrl: 'views/conciliacion/desglosesHuerfanos.html',
      controller: 'DesgloseHerfanosControler'
    })
    /** END CONCILIACION * */
    /** RECEPCIONES * */
    .when('/clearing/EntregaRecepcionCliente', {
      templateUrl: 'views/clearing/EntregaRecepcionCliente.html',
      controller: 'EntregaRecepcionClienteController'
    }).when('/clearing/ConsultaNetting', {
      templateUrl: 'views/clearing/ConsultaNetting.html',
      controller: 'ConsultaNettingController'
    }).when('/clearing/ConsultaNettingTitulosSinMov', {
      templateUrl: 'views/clearing/ConsultaNettingTitulosSinMov.html',
      controller: 'ConsultaNettingTitulosController'
    }).when('/clearing/ConsultaIntereses', {
      templateUrl: 'views/clearing/ConsultaIntereses.html',
      controller: 'ConsultaInteresesController'
    }).when('/clearing/ControlTitularesRouting', {
      templateUrl: 'views/clearing/ControlTitularesRouting.html',
      controller: 'ControlTitularesRoutingController'
    }).when('/clearing/desglosesMQ', {
      templateUrl: 'views/clearing/desglosesPartenon.html',
      controller: 'DesglosesMQControler'
    }).when('/clearing/ActualizacionMasivaTitulares', {
      templateUrl: 'views/clearing/ActualizacionMasivaTitulares.html',
      controller: 'ActualizacionMasivaTitularesController'
    }).when('/clearing/ReportesCNMV', {
      templateUrl: 'views/clearing/ReportesCNMV.html',
      controller: 'ReportesCNMVController'
    }).when('/clearing/ReportesAutomaticosCNMV', {
      templateUrl: 'views/clearing/ReportesAutomaticosCNMV.html',
      controller: 'ReportesAutomaticosCNMVController'
    }).when('/clearing/ReportesOperacionesTrCNMV', {
      templateUrl: 'views/clearing/ReportesOperacionesTrCNMV.html',
      controller: 'ReportesOperacionesTrCNMVController'
    }).when('/clearing/ResultadosEjecucionesCNMV', {
      templateUrl: 'views/clearing/ResultadosEjecucionesCNMV.html',
      controller: 'ResultadosEjecucionesCNMVController'
    }).when('/clearing/DesglosesMacro', {
      templateUrl: 'views/clearing/DesglosesMacro.html',
      controller: 'DesglosesMacroController'
    }).when('/clearing/cestaOrdenesFidessa', {
      templateUrl: 'views/clearing/CestaOrdenesFidessa.html',
      controller: 'CestaOrdenesFidessaController'
    })
    /** END RECEPCION * */
    /** FACTURACION * */
    // facturas
    .when('/facturas/FacturaConsulta', {
      templateUrl: 'views/facturas/FacturaConsulta.html',
      controller: 'FacturaConsultaController'
    }).when('/facturas/FacturasRectificativas', {
      templateUrl: 'views/facturas/FacturasRectificativas.html',
      controller: 'FacturaRectificativaController'
    }).when('/facturas/FacturasEmitidas', {
      templateUrl: 'views/facturas/FacturasEmitidas.html',
      controller: 'FacturaEmitidaController'
    }).when('/facturas/CrearFactura', {
      templateUrl: 'views/facturas/CrearFactura.html',
      controller: 'CrearFacturaController'
    }).when('/facturas/CrearRectificativa', {
      templateUrl: 'views/facturas/CrearRectificativa.html',
      controller: 'CrearRectificativaController'
    }).when('/facturas/ConsultarRespuestaErroresGC', {
      templateUrl: 'views/facturas/ConsultarRespuestaErroresGC.html',
      controller: 'ConsultarRespuestaErroresGCController'
    })
    // cobros
    .when('/facturas/CrearCobro', {
      templateUrl: 'views/facturas/CrearCobro.html',
      controller: 'CrearCobroController'
    }).when('/facturas/CobrosConsultar', {
      templateUrl: 'views/facturas/CobrosConsultar.html',
      controller: 'CobrosConsultaController'
    })
    /** END FACTURACION * */
    /** CONTABILIDAD * */
    .when('/contabilidad/contaInformes', {
      templateUrl: 'views/contabilidad/contaInformes.html',
      controller: 'CommonController'
    }).when('/contabilidad/cierreContable', {
      templateUrl: 'views/contabilidad/cierreContable.html',
      controller: 'CierreContableController'
    }).when('/contabilidad/cuentaContable', {
      templateUrl: 'views/contabilidad/cuentaContable.html',
      controller: 'CuentaContableController'
    })
    // Apunte contable
    .when('/contabilidad/altaApunteContable', {
      templateUrl: 'views/contabilidad/altaApunteContable.html',
      controller: 'AltaApunteContableController'
    }).when('/contabilidad/generarCartaPago', {
      templateUrl: 'views/contabilidad/generarCartaPago.html',
      controller: 'GenerarCartaPagoController'
    }).when('/contabilidad/norma43Descuadres', {
      templateUrl: 'views/contabilidad/norma43Descuadres.html',
      controller: 'Norma43Controller'
    }).when('/contabilidad/contaInformes', {
      templateUrl: 'views/contabilidad/contaInformes.html',
      controller: 'ContaInformesController'
    })
    /** END CONTABILIDAD * */
    /** FALLIDOS Y AJUSTES* */
    .when('/fallidos/operacionFallida', {
      templateUrl: 'views/fallidos/operacionFallida.html',
      controller: 'OperacionesFallidasController'
    }).when('/fallidos/fallidosMovimiento', {
      templateUrl: 'views/fallidos/fallidosMovimiento.html',
      controller: 'MovimientosFallidosController'
    })
    /** END FALLIDOS Y AJUSTES * */
    /** INFORMES * */
    // BME ECC
    .when('/exportacion/exportacion', {
      templateUrl: 'views/exportacion/exportacion.html',
      controller: 'CommonController'
    }).when('/exportacion/transaccionesFinancieras', {
      templateUrl: 'views/exportacion/transaccionesFinancieras.html',
      controller: 'ControltransaccionesFinancieras'
    })
    /** END INFORMES * */
    /** CONFIGURACION * */
    .when('/configuracion/LanzadorProcesos', {
      templateUrl: 'views/configuracion/LanzadorProcesos.html',
      controller: 'CommonController'
    }).when('/configuracion/jobs', {
      templateUrl: 'views/configuracion/jobs.html',
      controller: 'JobsController'
    }).when('/periodicidad/periodicidad', {
      templateUrl: 'views/periodicidad/periodicidad.html',
      controller: 'PeriodicidadController'
    }).when('/configuracion/financiacionIntereses', {
      templateUrl: 'views/configuracion/financiacionIntereses.html',
      controller: 'FinanciacionInteresesController'
    }).when('/configuracion/GestionReglas', {
      templateUrl: 'views/configuracion/GestionReglas.html',
      controller: 'GestionReglasController'
    }).when('/configuracion/parametrizaciones', {
      templateUrl: 'views/configuracion/parametrizaciones.html',
      controller: 'ParametrizacionesController'
    })
    // tablas maestras

    // conciliacion
    .when('/conciliacion/tipoMovimiento', {
      templateUrl: 'views/conciliacion/tipoMovimiento.html',
      controller: 'TipoMovimientoController'
    }).when('/conciliacion/cuentaLiquidacion', {
      templateUrl: 'views/conciliacion/cuentaLiquidacion.html',
      controller: 'CuentaLiquidacionController'
    }).when('/conciliacion/entidadRegistro', {
      templateUrl: 'views/conciliacion/entidadRegistro.html',
      controller: 'EntidadRegistroController'
    }).when('/conciliacion/tipoEntidadRegistro', {
      templateUrl: 'views/conciliacion/tipoEntidadRegistro.html',
      controller: 'TipoEntidadRegistroController'
    })
    // configuracion
    .when('/configuracion/cuentaCompensacion', {
      templateUrl: 'views/configuracion/cuentaCompensacion.html',
      controller: 'CuentaCompensacionController'
    })
    // datosMaestros
    .when('/datosMaestros/codigosOperacion', {
      templateUrl: 'views/datosMaestros/codigosOperacion.html',
      controller: 'CodigosOperacionController'
    }).when('/datosMaestros/gruposValores', {
      templateUrl: 'views/datosMaestros/gruposValores.html',
      controller: 'GruposValoresController'
    }).when('/datosMaestros/conceptosMovEfectivo', {
      templateUrl: 'views/datosMaestros/conceptosMovEfectivo.html',
      controller: 'ConceptosMovEfectivoController'
    }).when('/datosMaestros/depositariosValores', {
      templateUrl: 'views/datosMaestros/depositariosValores.html',
      controller: 'DepositariosValoresController'
    }).when('/datosMaestros/modosMatGarantias', {
      templateUrl: 'views/datosMaestros/modosMatGarantias.html',
      controller: 'ModosMatGarantiasController'
    }).when('/datosMaestros/tiposActivo', {
      templateUrl: 'views/datosMaestros/tiposActivo.html',
      controller: 'TiposActivoController'
    }).when('/datosMaestros/tiposGarantiaExigidas', {
      templateUrl: 'views/datosMaestros/tiposGarantiaExigidas.html',
      controller: 'TiposGarantiaExigidasController'
    }).when('/datosMaestros/tiposPersona', {
      templateUrl: 'views/datosMaestros/tiposPersona.html',
      controller: 'TiposPersonaController'
    }).when('/datosMaestros/tiposProducto', {
      templateUrl: 'views/datosMaestros/tiposProducto.html',
      controller: 'TiposProductoController'
    })
    // alias
    .when('/contactos/contactos', {
      templateUrl: 'views/contactos/contactos.html',
      controller: 'ContactosController'
    })

    .when('/alias/alias', {
      templateUrl: 'views/alias/alias.html',
      controller: 'AliasController'
    }).when('/configuracion/serviciosCliente', {
      templateUrl: 'views/configuracion/serviciosCliente.html',
      controller: 'ServiciosAClienteController'
    })
    // facturas
    .when('/configuracion/aliasSubcuenta', {
      templateUrl: 'views/configuracion/aliasSubcuenta.html',
      controller: 'AliasSubcuentaController'
    }).when('/Recordatorios/Recordatorios', {
      templateUrl: 'views/Recordatorios/Recordatorios.html',
      controller: 'RecordatorioController'
    }).when('/direccionFiscal/direccionFiscal', {
      templateUrl: 'views/direccionFiscal/direccionFiscal.html',
      controller: 'DireccionFiscalController'
    }).when('/configuracion/numeradores', {
      templateUrl: 'views/configuracion/numeradores.html',
      controller: 'NumeradoresController'
    }).when('/configuracion/GruposImpositivos', {
      templateUrl: 'views/configuracion/GruposImpositivos.html',
      controller: 'GruposImpositivosController'
    }).when('/configuracion/identificador', {
      templateUrl: 'views/configuracion/identificador.html',
      controller: 'IdentificadorController'
    }).when('/configuracion/PeriodosContables', {
      templateUrl: 'views/configuracion/PeriodosContables.html',
      controller: 'PeriodosContablesController'
    }).when('/configuracion/Textos', {
      templateUrl: 'views/configuracion/Textos.html',
      controller: 'TextosController'
    })
    // traspasos
    .when('/traspasos/TraspasosEcc', {
      templateUrl: 'views/traspasos/TraspasosEcc.html',
      controller: 'TraspasosEccController',
      controllerAs: 'ctrl'
    })
    // desgloses
    .when('/desglose/manual', {
      templateUrl: 'views/desglose/BusquedaAlosDesgloseManual.html',
      controller: 'BusquedaAlosDesgloseManualController',
      controllerAs: 'ctrl'
    })
    .when('/desglose/manual/:nuorden/:nbooking/:nucnfclt', {
      templateUrl: 'views/desglose/DesgloseManual.html',
      controller: 'BusquedaAlosDesgloseManualController',
      controllerAs: 'ctrl'
    })
    .when('/operaciones/ptiexecutions', {
      templateUrl: 'views/operaciones/PtiExecutions.html',
      controller: 'PtiExecutionsController',
      controllerAs: 'ctrl'
    })
    .when('/imputacionS3', {
      templateUrl: 'views/imputacionS3/ImputacionS3.html',
      controller: 'ImputacionS3Controller',
      controllerAs: 'ctrl'
    })
    /** DMO **/
    .when('/dmo/dmo', {
      templateUrl:  'views/dmo/dmo-view.html',
      controller:  'DmoController'
    })
    .when('/dmo/dmoReport', {
      templateUrl:  'views/dmo/dmo-report-view.html',
      controller:  'DmoReportController'
    })
    // DWH - Mantenimiento de Valores
    .when('/dwh/mantenimientoValores', {
      templateUrl: 'views/dwh/mantenimientoValores.html',
      controller: 'ValoresController',
      controllerAs: 'ctrl'
      // DWH - Trader/Sale y Account Manager
    }).when('/dwh/mantenimientoTradSaleAcc', {
      templateUrl: 'views/dwh/mantenimientoTradSaleAcc.html',
      controller: 'MantenimientoTradSaleAccController',
      controllerAs: 'ctrl'
    })
  .when('/dwh/LanzamientoManual', {
    templateUrl : 'views/dwh/LanzamientoManual.html',
    controller : 'LanzamientoManualController',
    controllerAs : 'ctrl'
  })
  .when('/desglose/manual/:nuorden/:nbooking/:nucnfclt', {
    templateUrl : 'views/desglose/DesgloseManual.html',
    controller : 'BusquedaAlosDesgloseManualController',
    controllerAs : 'ctrl'
  })
  .when('/operaciones/ptiexecutions', {
    templateUrl : 'views/operaciones/PtiExecutions.html',
    controller : 'PtiExecutionsController',
    controllerAs : 'ctrl'
  })
  .when('/imputacionS3', {
    templateUrl : 'views/imputacionS3/ImputacionS3.html',
    controller : 'ImputacionS3Controller',
    controllerAs : 'ctrl'
  })
  /** DMO **/
  .when('/dmo/dmo', {
    templateUrl: 'views/dmo/dmo-view.html',
	controller: 'DmoController'
  })
  .when('/dmo/dmoReport', {
	templateUrl: 'views/dmo/dmo-report-view.html',
	controller: 'DmoReportController'
  })
  /** PBC-DMO **/
  .when('/datosMaestros/controlDoc', {
	templateUrl : 'views/datosMaestros/controlDoc.html',
	controller : 'ControlDocController',
	controllerAs : 'ctrl'
  }).when('/datosMaestros/controlEfectivo', {
	templateUrl : 'views/datosMaestros/controlEfectivo.html',
	controller : 'ControlEfectivoController',
	controllerAs : 'ctrl'
  }).when('/datosMaestros/controlAnalisis', {
	templateUrl : 'views/datosMaestros/controlAnalisis.html',
	controller : 'ControlAnalisisController',
	controllerAs : 'ctrl'
  }).when('/datosMaestros/paises', {
    templateUrl : 'views/datosMaestros/paraisosFiscales.html',
	controller : 'ParaisosFiscalesController',
	controllerAs : 'ctrl'
  }).when('/gestionClientes/configuracionClientesPBC', {
    templateUrl : 'views/gestionClientes/configuracionClientesPBC.html',
    controller : 'ConfiguracionClientesPBCController',
    controllerAs : 'ctrl'
  })  
  /** MacLagan */
  .when('/informesMclagan', {
    templateUrl : 'views/informesMclagan/InformesMclagan.html',
    controller : 'InformesMclaganController',
    controllerAs : 'ctrl'
  })
  /** SICAV */
  .when('/sicav/mantenimientoTitu', {
    templateUrl : 'views/sicav/MantenimientoTitu.html',
    controller : 'MantenimientoTituController',
    controllerAs : 'ctrl'
  })
  .when('/sicav/mantenimientoOp', {
    templateUrl : 'views/sicav/MantenimientoOp.html',
    controller : 'MantenimientoOpController',
    controllerAs : 'ctrl'
  })
  /** END CONFIGURACION * */
  .otherwise({
    redirectTo : '/'
  });
})
