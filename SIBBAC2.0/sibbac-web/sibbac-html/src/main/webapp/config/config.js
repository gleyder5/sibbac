var datatable = {};
// Máximo número de registros que se pueden recibir del servidor para paginar del lado del cliente.
datatable.maxRecordsClientSideAllow = 2500;

// Registros mostrados por pantalla
datatable.pageLength = 50;

// Paginas que se van a cachear
datatable.pagesForCache = 50;

var security = {
  active : true,
  event : {
    initialized : "permisosCargados"
  }
};

var environment = {
  debug : true
};

var server = {};
server.url = '/sibbac20back/rest/service';
server.frontUrl = '/sibbac20/';
server.service = {};

server.service.gestionAccesos = {
  gestionUsuarios : "SIBBACServiceGestionUsuarios",
  gestionPerfiles : "SIBBACServiceGestionPerfiles",
  parametrosSeguridad : "SIBBACServiceParametrosSeguridad"
};

server.service.plantillasInformes = {
  gestionPlantillasInformes : "SIBBACServicePlantillasInformes",
  generarPlantillasInformes : "SIBBACServiceGenerarInformes",
  gestionPlanificacionInformes: "SIBBACServicePlanificacionInformes",
  gestionPlantillasInformesNEW : "SIBBACServicePlantillasInformesNEW",
  generarPlantillasInformesNEW : "SIBBACServiceGenerarInformesNEW"
};

server.service.catalogo = "SIBBACServiceCatalogo";

server.service.seguridad = {
  login : "SIBBACServiceSecurity"
};

server.service.mantenimientodatoscliente = {
  titularesService : "SIBBACServiceTmct0Fis",
  swiftService : "SIBBACServiceTmct0swi",
  gestionClientes : "SIBBACServiceGestionClientes"
};

server.service.facturasmanuales = {
  facturasManuales : "SIBBACServiceFacturasManuales",
  bajasManuales : "SIBBACServiceBajasManuales",
  rectificativasManuales : "SIBBACServiceRectificativasManuales",
  consultarAsientosContables : "SIBBACServiceAsientosContables",
  combosFacturasManuales : "SIBBACServiceCombosFacturasManuales"
};

server.service.conciliacion = {
  balance : "SIBBACServiceConciliacionBalance",
  clearing : "SIBBACServiceConciliacionClearing",
  cuentasECC : "SIBBACServiceConciliacionCuentaECC",
  efectivo : "SIBBACServiceConciliacionEfectivo",
  diaria : "SIBBACServiceConciliacionDiaria",
  tipoMovimiento : "SIBBACServiceTipoMovimiento",
  textsIdiomas : "SIBBACServiceTextosIdiomas",
  propia : "SIBBACServiceConciliacionPropia",
  movimientoManualService : "SIBBACServiceConciliacionMovimientoManual",
  cuentaLiquidacion : "SIBBACServiceCuentaLiquidacion",
  desgloses : "SIBBACServiceConciliacionDesglose"
};

server.service.contabilidad = {
  conciliacionBancaria : {
	  combos : "SIBBACServiceCombosPantallaConciliacion",
	  pantalla : "SIBBACServicePantallaConciliacion",
	  pantallaOUR : "SIBBACServicePantallaConciliacionOUR",
	  pantallaTLM : "SIBBACServicePantallaConciliacionTLM",
	  pantallaACC : "SIBBACServicePantallaConciliacionACC"
  },
  controlContabilidad : "SIBBACServiceControlContabilidad",
  historicoPartidasCasadas : "SIBBACServiceHistoricoPartidasCasadas",
  apunteContable : "SIBBACServiceApunteContable",
  contaInformes : "SIBBACServiceContaInformes",
  periodos : "SIBBACServiceContaPeriodos"	  
};

server.service.facturas = {
  crearRectificativa : 'SIBBACServiceFactura',
  consultarRespuestaErroresGC : "SIBBACServiceConsultarRespuestaErroresGC"
};

server.service.configuracion = {
  jobsService : "SIBBACServiceJobsManagement",
  textosService : 'SIBBACServiceTexto',
  textosIdiomaService : 'SIBBACServiceTextosIdiomas',
  idiomaService : "SIBBACServiceIdioma",
  periodicidadService : "SIBBACServicePeriodicidades",
  etiquetasService : "SIBBACServiceEtiquetas",
  cuentadecompensacionService : "SIBBACServiceCuentasDeCompensacion",
  gestionreglasService : "SIBBACServiceGestionReglas"
};

server.service.datosMaestros = 'SIBBACServiceDatosMaestros';

server.service.entidadRegistro = 'SIBBACServiceEntidadRegistro';

server.service.movimientoVirtual = 'SIBBACServiceMovimientoVirtual';

server.service.url = {
  manager : 'SIBBACUrlManagerService'
};

server.service.clearing = {
  entregaRecepcion : 'SIBBACServiceEntregaRecepcion',
  consultaNetting : 'SIBBACServiceConsultaNetting',
  financiacionIntereses : 'SIBBACServiceFinanciacionIntereses',
  titularesRouting : 'SIBBACServiceTitulares',
  reportesCNMV : 'SIBBACServiceReportesCNMV',
  desglosesMacro : 'SIBBACServiceDesglosesMacro',
  desgloses : "SIBBACServiceDesgloses",
  cestaOrdenesFidessa : "SIBBACServiceCestaOrdenesFidessa"
};

server.service.contactos = {
  contactosService : 'SIBBACServiceContacto'
};

server.service.alias = {
  aliasService : "SIBBACServiceAlias",
  tmct0aliService : "SIBBACServiceTmct0ali",
  servicios : "SIBBACServiceServicios"
};

/** Servicio usado en directivas - componentes genericos. */
server.service.directives = {
  getDirectives : "SIBBACServiceDirectives"
};

