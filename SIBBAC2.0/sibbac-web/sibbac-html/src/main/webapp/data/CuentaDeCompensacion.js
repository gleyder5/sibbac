function CuentaDeCompensacion() {
    return {
        "idCuentaCompensacion": "",
        "cdCodigo": "",
        "codigoS3": "",
        "cdCuentaCompensacion": "",
        "esClearing": "",
        "s3": null,
        "idCompensador": "",
        "idTipoCuenta": "",
        "tipoCuentaConciliacion": {
            "id": "",
            "name": ""
        },
        "entidadRegistro": {
            "id": "",
            "bic": "",
            "nombre": "",
            "tipoer": {
                "id": "",
                "nombre": ""
            }
        },
        "tipoER": {
            "id": "",
            "nombre": ""
        },
        "idMercadoContratacion": "",
        "numCuentaER": "",
        "esCuentaPropia": "",
        "tipoNeteoTitulo": {
            "id": "",
            "nombre": ""
        },
        "tipoNeteoEfectivo": {
            "id": "",
            "nombre": ""
        },
        "frecuenciaEnvioExtracto": "",
        "tipoFicheroConciliacion": "",
        "cuentaLiquidacionId": "",
        "cuentaLiquidacionCodigo": "",
        "subcustodio": "",
        "numCtaSubcustodio": "",
        "relacionCuentaMercadoDataList": [],
        "cuentaMercado" : {
            "id": "",
            "codigo": "",
            "descripcion": ""
        },
        "sistemaLiquidacion": {
            "id": "",
            "codigo": "",
            "descripcion": ""
        }
    };
}

