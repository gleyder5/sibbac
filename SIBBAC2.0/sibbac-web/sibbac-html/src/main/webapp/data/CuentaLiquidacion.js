//Necesita importar:
//EntidadRegistro.js
//TipoER.js
//TipoNeteoTitulo.js
//TipoNeteoEfectivo.js
//TipoCuentaConciliacion.js
//SistemaLiquidacion.js
function CuentaLiquidacion() {
    return {"id": "",
        "entidadRegistro": new EntidadRegistro(),
        "idMercadoContratacion": "",
        "cdCodigo": "",
        "numCuentaER": "",
        "esCuentaPropia": "",
        "tipoER": new TipoER(),
        "tipoNeteoTitulo": new TipoNeteoTitulo(),
        "tipoNeteoEfectivo": new TipoNeteoEfectivo(),
        "frecuenciaEnvioExtracto": "",
        "tipoFicheroConciliacion": "",
        "tipoCuentaConciliacion": new TipoCuentaConciliacion(),
        "relacionCuentaMercadoDataList": [],
        "cuentaDeCompensacionList": [],
        "sistemaLiquidacion": new SistemaLiquidacion(),
        "cuentaIberclear": ""
    };
}

