/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*MENU PRINCIPAL*/

var refreshDataListener = 0;
var refreshJobsListener = 0;


$('.menuPrincipal a').click(function () {
	$(this).toggleClass('activo');
	if ($(this).hasClass('activo')) {
		$(this).siblings().animate({height: "toggle"}, 100);
		$(this).closest('ul').find('li a.activo').not(this).siblings('div').hide();
		$(this).closest('ul').find('li a.activo').not(this).removeClass('activo');
	} else {
		$(this).siblings().animate({height: "toggle"}, 100);
	}
});
var timeout;
function renderPart(){
	clearTimeout(timeout);
	//    render();
}

$(".menuItem").click(function(event){
	event.preventDefault();

	var url = $(this).attr("href");


	if(url === "" || url ==="#"){
		return false;
	}
	if ($(this).attr("target")==="_blank"){
		window.open(url);
		return false;
	}
	var titulo = $(this).attr("title");
	if ( titulo === "" || titulo === undefined ) {
		titulo = "SIBBAC20";
	}
	document.title = titulo;
	$('#tituloHeader').empty();
	$('#tituloHeader').append(titulo);
	//
	$("section").load(url, function(){
		if (typeof refreshDataListener != 'undefined') {
			clearInterval( refreshDataListener );
		}
		timeout = setTimeout("renderPart()", 500);
	});
});


