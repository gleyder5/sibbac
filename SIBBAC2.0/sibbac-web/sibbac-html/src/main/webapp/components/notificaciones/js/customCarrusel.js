(function($){
	//Men� principal:
    function customCarrusel(item) {
        this.item = $(item);
        this.init();
    }
    customCarrusel.prototype = {
        init: function() {
			var _self = this;
			this.setCarouselWidth(this.getItemWidth());
			this.setCarousePosition(this.getItemWidth())
			this.item.find('.prev').click(function(){
				_self.atras(_self.getItemWidth());
				return false;
			});
			this.item.find('.next').click(function(){
				_self.adelante(_self.getItemWidth());
				return false;
			});

        },
		getItemWidth:function(){
			return this.item.find('.notificacion').outerWidth(true);
		},
		setCarouselWidth:function(itemWidth){
			this.item.find('.notificaciones2').width(this.item.find('.notificacion').length*itemWidth);
		},
		setItemsOrder:function(){
			this.item.find('.notificaciones2').prepend(this.item.find('.notificacion:last-child'));
		},
		setCarousePosition:function(itemWidth){
			var posicionInicial = 0;
			this.item.find('.notificaciones2').css('left', -posicionInicial+'px');
		},
		adelante:function(itemWidth){
			var self = this;
			if(!this.item.find('.notificaciones2').hasClass('animando')){
				this.item.find('.notificaciones2').addClass('animando');
				this.item.find('.notificaciones2').animate({
					left: '-='+itemWidth
				}, function(){
					self.item.find('.notificaciones2').append(self.item.find('.notificacion:first-child'));
					var actualLeft = parseInt(self.item.find('.notificaciones2').css('left').split('px')[0]);
					self.item.find('.notificaciones2').css('left', actualLeft+itemWidth+'px');
					self.item.find('.notificaciones2').removeClass('animando');
				});
			}
		},
		atras:function(itemWidth){
			var self = this;
			this.item.find('.notificaciones2').prepend(this.item.find('.notificacion:last-child'));
			var actualLeft = parseInt(self.item.find('.notificaciones2').css('left').split('px')[0]);
			self.item.find('.notificaciones2').css('left', actualLeft-itemWidth+'px');
			if(!this.item.find('.notificaciones2').hasClass('animando')){
				this.item.find('.notificaciones2').addClass('animando');
				this.item.find('.notificaciones2').animate({
					left: '+='+itemWidth
				}, function(){
					self.item.find('.notificaciones2').removeClass('animando');
				});
			}
		}
    }
    $.fn.customCarrusel = function(opt) {
        var args = Array.prototype.slice.call(arguments, 1);
        return this.each(function() {
            var item = $(this), instance = item.data('customCarrusel');
            if(!instance) {
                item.data('customCarrusel', new customCarrusel(this, opt));
            } else {
                if(typeof opt === 'string') {
                    instance[opt].apply(instance, args);
                }
            }
        });
    }
}(jQuery));

$(document).ready(function(){

    $('.carrusel').customCarrusel();

})