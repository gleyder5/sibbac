var idOcultosAlias = [];
var availableTagsAlias = [];
var idOcultosIsin = [];
var availableTagsIsin = [];
var idOcultosContrapartida = [];
var availableTagsContrapartida = [];
var oTable = undefined;

/* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
if ($('.contenedorTabla').length >= 1) {
  var anchoBotonera;
  $('.contenedorTabla').each(function (i) {
    anchoBotonera = $(this).find('table').outerWidth();
    $(this).find('.botonera').css('width', anchoBotonera + 'px');
    $(this).find('.resumen').css('width', anchoBotonera + 'px');
    $('.resultados').hide();
  });
}

function initialize () {

  var fechas = [ "fcontratacionDe", "fcontratacionA", "fliquidacionDe", "fliquidacionA" ];
  loadpicker(fechas);
  verificarFechas([ [ "fcontratacionDe", "fcontratacionA" ], [ "fliquidacionDe", "fliquidacionA" ] ]);

  $('#paraalias').loadselectcombo('alias');
  $('#paraisin').loadselectcombo('isin');
  loadAutocompletar('#contrapartida', 'SIBBACServiceTmct0cle', 'getLista', idOcultosContrapartida,
                    availableTagsContrapartida);
  cargarComboEstados();

}

$(document).ready(function () {

  initialize();

  oTable = $("#tablaEntregaRecepcion").dataTable({
    "dom" : 'T<"clear">lfrtip',
    "tableTools" : {
      "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
    },
    "language" : {
      "url" : "i18n/Spanish.json"
    },
    "aoColumns" : [ {
      sClass : "centrar details-control", /*
                                           * "bSortable": false Deshabilita sort por columna
                                           */
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "monedaR",
      type : "formatted-num"
    }, {
      sClass : "monedaR",
      type : "formatted-num"
    }, {
      sClass : "monedaR",
      type : "formatted-num"
    }, {
      sClass : "monedaR",
      type : "formatted-num"
    }, {
      sClass : "monedaR",
      type : "formatted-num"
    }, {
      sClass : "monedaR",
      type : "formatted-num"
    }, {
      sClass : "monedaR",
      type : "formatted-num"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    }, {
      sClass : "centrar"
    } ],
    "order" : [],
    "scrollY" : "480px",
    "scrollCollapse" : true,
    "scrollX" : "100%"
  });

  $('#tablaEntregaRecepcion tbody').on('click', 'td.details-control', function () {

    var nTr = $(this).parents('tr')[0];
    var aData = oTable.fnGetData(nTr);

    if (oTable.fnIsOpen(nTr)) {
      $(this).removeClass('open');
      oTable.fnClose(nTr);
    } else {
      if (aData[0] != "PDTE. FICHERO" && aData[0] != "ENVIADO FICHERO") {
        $(this).addClass('open');
        oTable.fnOpen(nTr, TablaLiquidaciones(oTable, nTr), 'details');
      }
    }

  });

  /* Eva:Función para cambiar texto */
  jQuery.fn.extend({
    toggleText : function (a, b) {
      var that = this;
      if (that.text() != a && that.text() != b) {
        that.text(a);
      } else if (that.text() == a) {
        that.text(b);
      } else if (that.text() == b) {
        that.text(a);
      }
      return this;
    }
  });
  /* FIN Función para cambiar texto */

  $('a[href="#release-history"]').toggle(function () {
    $('#release-wrapper').animate({
      marginTop : '0px'
    }, 600, 'linear');
  }, function () {
    $('#release-wrapper').animate({
      marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
    }, 600, 'linear');
  });

  $('#download a').mousedown(function () {
    _gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
  });

  prepareCollapsion();

  $('#cargarTabla').submit(function (event) {
    event.preventDefault();

    if (!validarInertavaloNumerico($('#importeNetoDesde'), $('#importeNetoHasta'))) {
      mensajeError("El intervalo de importes no es correcto. Por favor, revíselo");
    }
    if (!validarInertavaloNumerico($('#titulosDesde'), $('#titulosHasta'))) {
      mensajeError("El intervalo de titulos no es correcto. Por favor, revíselo");
    }

    cargarDatosEntregaRecepcion();
    seguimientoBusqueda();
    collapseSearchForm();
  });

  $('#importeNetoDesde').blur(function (event) {
    formatearImporte(this);
  });
  $('#importeNetoHasta').blur(function (event) {
    formatearImporte(this);
  });
  $('#titulosDesde').blur(function (event) {
    formatearTitulos(this);
  });
  $('#titulosHasta').blur(function (event) {
    formatearTitulos(this);
  });

  $('#limpiar').click(function (event) {
    event.preventDefault();
    $('input[type=text]').val('');
    document.getElementById("estado").value = '';
    document.getElementById("sentido").value = '';
  });

});

function render () {

}

function formatearImporte (campoImporte) {
  var importe = $(campoImporte).val();
  if (importe === null || importe === undefined || importe === '') {
    importe = null;
  } else {
    importe = importe.replace('.', '').replace(',', '.');
    if (isNaN(importe)) {
      mensajeError("El importe introducido no es correcto.");
      importe = null;
    } else {
      importe = $.number(importe, 2, ',', '.');
    }
  }
  $(campoImporte).val(importe);
}

function formatearTitulos (campoTitulos) {
  var titulos = $(campoTitulos).val();
  if (titulos === null || titulos === undefined || titulos === '') {
    titulos = null;
  } else {
    titulos = titulos.replace('.', '').replace(',', '.');
    if (isNaN(titulos)) {
      mensajeError("Los títutlos introducidos no son correctos.");
      titulos = null;
    } else {
      titulos = $.number(titulos, 0, '', '.');
    }
  }
  $(campoTitulos).val(titulos);
}

function validarInertavaloNumerico (campoDesde, campoHasta) {
  var numeroDesde = getNumero(campoDesde);
  var numeroHasta = getNumero(campoHasta);
  var validado = false;
  if (numeroDesde === '' || numeroHasta === '') {
    validado = true;
  } else {
    if (numeroDesde <= numeroHasta) {
      validado = true;
    }
  }
  return validado;
}

function getNumero (campo) {
  var numero = $(campo).val();
  if (numero === null || numero === undefined || numero === '') {
    numero = '';
  } else {
    numero = numero.replace('.', '').replace(',', '.');
    if (isNaN(numero)) {
      numero = '';
    } else {
      numero = parseFloat(numero);
    }
  }
  return numero;
}

function cargarDatosEntregaRecepcion () {

  console.log("cargarDatosEntregaRecepcion");

  $("#tablaEntregaRecepcion > tbody").empty();

  var listaAlias = document.getElementById("textAlias").value;
  var posicionAlias = availableTagsAlias.indexOf(listaAlias);
  if (posicionAlias == "-1") {
    valorSeleccionadoAlias = document.getElementById("textAlias").value;
  } else {
    valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
  }

  var listaIsin = document.getElementById("textIsin").value;
  var posicionIsin = availableTagsIsin.indexOf(listaIsin);
  if (posicionIsin == "-1") {
    valorSeleccionadoIsin = document.getElementById("textIsin").value;
  } else {
    valorSeleccionadoIsin = idOcultosIsin[posicionIsin];
  }

  var contrapartida = $("#contrapartida").val();
  var posicionContrapartida = availableTagsContrapartida.indexOf(contrapartida);
  var valorSeleccionadoContrapartida = '';
  if (posicionContrapartida == "-1") {
    valorSeleccionadoContrapartida = contrapartida;
  } else {
    valorSeleccionadoContrapartida = idOcultosContrapartida[posicionContrapartida];
  }
  var importeNetoDesde = getNumero($("#importeNetoDesde"));
  var importeNetoHasta = getNumero($("#importeNetoHasta"));
  var titulosDesde = getNumero($("#titulosDesde"));
  var titulosHasta = getNumero($("#titulosHasta"));

  var filtro = "{\"fcontratacionDe\" : \"" + $("#fcontratacionDe").val() + "\", \"fcontratacionA\" : \""
               + $("#fcontratacionA").val() + "\", \"fliquidacionDe\" : \"" + $("#fliquidacionDe").val()
               + "\", \"fliquidacionA\" : \"" + $("#fliquidacionA").val() + "\", \"estado\" : \"" + $("#estado").val()
               + "\", \"nbooking\" : \"" + $("#booking").val() + "\", \"alias\" : \"" + valorSeleccionadoAlias
               + "\", \"isin\" : \"" + valorSeleccionadoIsin + "\", \"sentido\" : \"" + $("#sentido").val()
               + "\", \"nif\" : \"" + $("#nif").val() + "\", \"nombretitular\" : \"" + $("#nombretitular").val()
               + "\", \"biccle\" : \"" + valorSeleccionadoContrapartida + "\", \"importeNetoDesde\" : \""
               + importeNetoDesde + "\", \"importeNetoHasta\" : \"" + importeNetoHasta + "\", \"titulosDesde\" : \""
               + titulosDesde + "\", \"titulosHasta\" : \"" + titulosHasta + "\" } ";
  var data = new DataBean();
  data.setService('SIBBACServiceEntregaRecepcion');
  data.setAction('ListaEntregaRecepcion');
  data.setFilters(filtro);
  var request = requestSIBBAC(data);
  request.success(function (json) {

    console.log(json);
    console.log(json.resultados);

    if (json.resultados != null && json.resultados !== undefined && json.resultados.lista !== undefined) {

      var item = null;
      var datos = json.resultados.lista;

      var tbl = $("#tablaEntregaRecepcion > tbody");
      $(tbl).html("");
      oTable.fnClearTable();

      for ( var k in datos) {
        item = datos[k];

        var rowIndex = oTable.fnAddData([ item.estadoentrec, item.cdtpoper, item.cdalias, item.cdisin, item.nbvalors,
                                         item.nutitliq, $.number(item.imefeagr, 2, ',', '.'),
                                         $.number(item.imcomisin, 2, ',', '.'), $.number(item.imnfiliq, 2, ',', '.'),
                                         $.number(item.canon_contratacion, 2, ',', '.'),
                                         $.number(item.canon_compensacion, 2, ',', '.'),
                                         $.number(item.canon_liquidacion, 2, ',', '.'), item.cdniftit, item.nbtitliq,
                                         item.cdcustod, item.nbooking, item.nucnfclt, item.nucnfliq, item.cdrefban,
                                         item.ourpar, item.theirpar ], false);

        var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
        if (item.estadoentrec != "PDTE. FICHERO" && item.estadoentrec != "ENVIADO FICHERO") {
          $('td', nTr)[0].setAttribute('style', 'cursor: pointer; color: #ff0000;');
        }

        $('td', nTr)[2].setAttribute('title', item.descrali);

      }
      oTable.fnDraw();

    }

  });

}

function TablaLiquidaciones (Table, nTr) {

  var aData = Table.fnGetData(nTr);
  var contador = Table.fnGetPosition(nTr);

  var resultado = "<tr id='tabla_a_desplegar" + contador + "' style='display:none;'>"
                  + "<td style='border-width:0 !important;background-color:#dfdfdf;'>" + "<table id='tablaParam"
                  + contador + "' style=width:100%; margin-top:5px;'>" + "<tr>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Estado</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Fecha Liquidada</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Fecha Operación</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Fecha Liquidación</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Titulos liquidados</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Neto liquidado</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Efectivo</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Corretaje</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Contabilizado</th>"
                  + "<th class='taleft' style='background-color: #000 !important;'>Fecha contabilizado</th>"
                  + "</tr></table></td></tr>";

  desplegarLiquidaciones(aData[14], aData[15], aData[16], contador);

  return resultado;
}

// funcion para desplegar la tabla de liquidaciones
function desplegarLiquidaciones (nbooking, nucnfclt, nucnfliq, numero_fila) {

  console.log("desplegarLiquidaciones");

  var params = {
    "service" : "SIBBACServiceEntregaRecepcion",
    "action" : "ListaLiquidacionS3",
    "filters" : {
      "nbooking" : nbooking,
      "nucnfclt" : nucnfclt,
      "nucnfliq" : nucnfliq
    }
  };

  $.ajax({

    type : 'POST',
    dataType : "json",
    url : "/sibbac20back/rest/service",
    data : JSON.stringify(params),
    beforeSend : function (x) {
      if (x && x.overrideMimeType) {
        x.overrideMimeType("application/json");
      }
      // CORS Related
      x.setRequestHeader("Accept", "application/json");
      x.setRequestHeader("Content-Type", "application/json");
      x.setRequestHeader("X-Requested-With", "HandMade");
      x.setRequestHeader("Access-Control-Allow-Origin", "*");
      x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    },
    async : true,
    success : function (json) {

      console.log(json);
      console.log(json.resultados);
      if (json.resultados !== undefined && json.resultados.lista !== undefined) {
        var datos = json.resultados.lista;
        var item = null;
        var contador = 0;
        var estilo = null;

        for ( var k in datos) {

          item = datos[k];

          if (contador % 2 == 0) {
            estilo = "even";
          } else {
            estilo = "odd";
          }
          contador++;

          var enlace = "";
          if (item.cdestados3 == "ERR") {
            enlace = "onClick=\"desplegarErrores(" + item.idliquidacion + ", '" + numero_fila + contador + "')\" ";
            enlace = enlace + " style=\"cursor: pointer; color: #ff0000;\"";
          } else {
            enlace = "";
          }

          $("#tablaParam" + numero_fila)
              .append(
                      "<tr class='" + estilo + "' id='filaLiqRecuperada" + numero_fila
                          + "' style='background-color: #f5ebc5 !important;' >" + "<td class=\"taleft\"" + enlace
                          + " style='background-color: #f5ebc5 !important;'>" + item.cdestados3 + "</td>"
                          + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                          + transformaFechaGuion(item.fechaliquidacion) + "</td>"
                          + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                          + transformaFechaGuion(item.fechaoperacion) + "</td>"
                          + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                          + transformaFechaGuion(item.fechavalor) + "</td>"
                          + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                          + item.nutiuloliquidados + "</td>"
                          + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                          + a2digitos(item.importenetoliquidado) + "</td>"
                          + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                          + a2digitos(item.importeefectivo) + "</td>"
                          + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                          + a2digitos(item.importecorretaje) + "</td>"
                          + "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.contabilizado
                          + "</td>" + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                          + transformaFechaGuion(item.fechacontabilidad) + "</td>" + "</tr>");

          if (item.cdestados3 == "ERR") {
            $("#tablaParam" + numero_fila)
                .append(
                        "<tr id='subtabla_a_desplegar" + numero_fila + contador + "' style='display:none;'>"
                            + "<td colspan='10' style='border-width:0 !important;background-color:#dfdfdf;'>"
                            + "<table id='subtablaParam" + numero_fila + contador
                            + "' style=width:100%; margin-top:5px;'>" + "<tr>"
                            + "<th class='taleft' >Error cliente</th>" + "<th class='taleft' >Error SV</th>"
                            + "<th class='taleft' >Descripción</th>" + "</tr></table></td></tr>");
          }

        }

        $("#tablaParam" + numero_fila).append("</table>");

      }

      desplegar('tabla_a_desplegar' + numero_fila, 'estadoT' + numero_fila, 'estadoT' + numero_fila);

    },
    error : function (c) {
      console.log("ERROR: " + c);
    }
  });

}

// funcion para desplegar la tabla de parametrizacion
function desplegarErrores (idLiquidacion, posicion) {

  console.log("desplegarErrores");

  var existe_tabla = document.getElementById('filaErrorRecuperada' + posicion);

  if (existe_tabla != null) {
    desplegar('subtabla_a_desplegar' + posicion, 'estadoT' + posicion, 'estadoT' + posicion);
    return;
  }

  var params = {
    "service" : "SIBBACServiceEntregaRecepcion",
    "action" : "listaErroresS3",
    "filters" : {
      "idliquidacion" : idLiquidacion
    }
  };

  $.ajax({

    type : 'POST',
    dataType : "json",
    url : "/sibbac20back/rest/service",
    data : JSON.stringify(params),
    beforeSend : function (x) {
      if (x && x.overrideMimeType) {
        x.overrideMimeType("application/json");
      }
      // CORS Related
      x.setRequestHeader("Accept", "application/json");
      x.setRequestHeader("Content-Type", "application/json");
      x.setRequestHeader("X-Requested-With", "HandMade");
      x.setRequestHeader("Access-Control-Allow-Origin", "*");
      x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    },
    async : true,
    success : function (json) {

      console.log(json);
      console.log(json.resultados);
      if (json.resultados !== undefined && json.resultados.lista !== undefined) {
        var datos = json.resultados.lista;
        var item = null;
        var contador = 0;
        var estilo = null;

        for ( var k in datos) {

          item = datos[k];

          $("#subtablaParam" + posicion).append(
                                                "<tr class='odd' id='filaErrorRecuperada" + posicion + "' >"
                                                    + "<td class='taleft' >" + item.datoerrorcliente + "</td>"
                                                    + "<td class='taleft' >" + item.datoerrorsv + "</td>"
                                                    + "<td class='taleft' >" + item.dserror + "</td>" + "</tr>");

        }

        $("#subtablaParam" + posicion).append("</table>");

      }

      desplegar('subtabla_a_desplegar' + posicion, 'estadoT' + posicion, 'estadoT' + posicion);

    },
    error : function (c) {
      console.log("ERROR: " + c);
    }
  });

}

function cargarComboEstados () {

  var data = new DataBean();
  data.setService('SIBBACServiceEntregaRecepcion');
  data.setAction('EstadosClearing');

  var request = requestSIBBAC(data);
  request.success(function (json) {
    if (json.resultados == null) {
      alert(json.error);
    } else {
      var datos = json.resultados.lista;

      for ( var k in datos) {
        item = datos[k];
        $('#estado').append("<option value='" + item.id + "'>" + item.descripcion + "</option>"

        );
      }
    }
  });

}

// funcion para desplegar las subtablas
function desplegar (tabla_a_desplegar, e1, estadoTfila) {
  var tablA = document.getElementById(tabla_a_desplegar);
  var estadOt = document.getElementById(e1);
  var fila = document.getElementById(estadoTfila);
  // recorremos las filas y ocultamos todas menos esta

  switch (tablA.style.display) {
    case "none":
      tablA.style.display = "";
      if (estadOt != null) {
        estadOt.innerHTML = "Ocultar";
      }
      break;

    default:
      tablA.style.display = "none";
      if (estadOt != null) {
        estadOt.innerHTML = "Mostrar";
      }
      break;
  }
}

function seguimientoBusqueda () {

  var listaAlias = document.getElementById("textAlias").value;
  posicionAlias = availableTagsAlias.indexOf(listaAlias);
  if (posicionAlias == "-1") {
    valorSeleccionadoAlias = document.getElementById("textAlias").value;
  } else {
    valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
  }

  var listaIsin = document.getElementById("textIsin").value;
  posicionIsin = availableTagsIsin.indexOf(listaIsin);
  if (posicionIsin == "-1") {
    valorSeleccionadoIsin = document.getElementById("textIsin").value;
  } else {
    valorSeleccionadoIsin = idOcultosIsin[posicionIsin];
  }

  var contrapartida = $("#contrapartida").val();
  var posicionContrapartida = availableTagsContrapartida.indexOf(contrapartida);
  var valorSeleccionadoContrapartida = '';
  if (posicionContrapartida == "-1") {
    valorSeleccionadoContrapartida = contrapartida;
  } else {
    valorSeleccionadoContrapartida = idOcultosContrapartida[posicionContrapartida];
  }

  $('.mensajeBusqueda').empty();
  var cadenaFiltros = "";
  if ($('#estado option:selected').text() !== "" && $('#estado').val() !== undefined) {
    cadenaFiltros += " Estado:" + $('#estado option:selected').text();
  }
  if ($('#sentido option:selected').text() !== "" && $('#sentido').val() !== undefined) {
    cadenaFiltros += " Sentido:" + $('#sentido option:selected').text();
  }
  if ($('#textAlias').val() !== "" && $('#textAlias').val() !== undefined) {
    cadenaFiltros += " Alias:" + valorSeleccionadoAlias;
  }
  if ($('#textIsin').val() !== "" && $('#textIsin').val() !== undefined) {
    cadenaFiltros += " Isin:" + valorSeleccionadoIsin;
  }
  if ($('#contrapartida').val() !== "" && $('#contrapartida').val() !== undefined) {
    cadenaFiltros += " Contrapartida:" + valorSeleccionadoContrapartida;
  }
  if ($('#booking').val() !== "" && $('#booking').val() !== undefined) {
    cadenaFiltros += " Booking:" + $('#booking').val();
  }
  if ($('#fcontratacionDe').val() !== "" && $('#fcontratacionDe').val() !== undefined) {
    cadenaFiltros += " F. Contratacion Desde:" + $('#fcontratacionDe').val();
  }
  if ($('#fcontratacionA').val() !== "" && $('#fcontratacionA').val() !== undefined) {
    cadenaFiltros += " F. Contratacion Hasta:" + $('#fcontratacionA').val();
  }
  if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined) {
    cadenaFiltros += " F. liquidacion Desde:" + $('#fliquidacionDe').val();
  }
  if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined) {
    cadenaFiltros += " F. liquidacion Hasta:" + $('#fliquidacionA').val();
  }
  if ($('#importeNetoDesde').val() !== "" && $('#importeNetoDesde').val() !== undefined) {
    cadenaFiltros += " Importe Neto Desde:" + $('#importeNetoDesde').val();
  }
  if ($('#importeNetoHasta').val() !== "" && $('#importeNetoHasta').val() !== undefined) {
    cadenaFiltros += " Importe Neto Hasta:" + $('#importeNetoHasta').val();
  }
  if ($('#titulosDesde').val() !== "" && $('#titulosDesde').val() !== undefined) {
    cadenaFiltros += " Titulos Desde:" + $('#titulosDesde').val();
  }
  if ($('#titulosHasta').val() !== "" && $('#titulosHasta').val() !== undefined) {
    cadenaFiltros += " Titulos Hasta:" + $('#titulosHasta').val();
  }
  if ($('#nif').val() !== "" && $('#nif').val() !== undefined) {
    cadenaFiltros += " Documento:" + $('#nif').val();
  }
  if ($('#nombretitular').val() !== "" && $('#nombretitular').val() !== undefined) {
    cadenaFiltros += " Nombre Titular:" + $('#nombretitular').val();
  }
  $('.mensajeBusqueda').append(cadenaFiltros);

}
