var idOcultosAlias = [];
var availableTagsAlias = [];
var idOcultosIsin = [];
var availableTagsIsin = [];
var oTable = undefined;

/* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
if ($('.contenedorTabla').length >= 1) {
    var anchoBotonera;
    $('.contenedorTabla').each(function (i) {
        anchoBotonera = $(this).find('table').outerWidth();
        $(this).find('.botonera').css('width', anchoBotonera + 'px');
        $(this).find('.resumen').css('width', anchoBotonera + 'px');
        $('.resultados').hide();
    });
}




function initialize() {

	var fechas= ["fcontratacionDe","fcontratacionA","fliquidacionDe","fliquidacionA"];
	loadpicker(fechas);
	verificarFechas([["fcontratacionDe","fcontratacionA"],["fliquidacionDe","fliquidacionA"]]);
	cargarAlias();
	cargarIsin();


}

$(document).ready(function () {


	initialize();

	oTable = $("#tablaNetting").dataTable({
		"dom": 'T<"clear">lfrtip',
         "tableTools": {
             "sSwfPath": "js/swf/copy_csv_xls_pdf.swf"
         },
         "language": {
             "url": "i18n/Spanish.json"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         /*
         columnDefs: [ 	{ 	className: 'details-liq',
             				targets:   0  },
             			{ 	className: 'details-alc',
             				targets:   1  },
             			{ 	className: 'monedaR',
                 			targets:   [9,10,11]},
             			{ 	type: 'formatted-num',
                     		targets:   [9,10,11]},
                     	{ 	type: 'date-eu',
                         	targets:   [3,4,13]}
             		],*/
         "order": []
	});

	$('#tablaNetting tbody').on('click', 'td.details-liq', function () {

		 var nTr = $(this).parents('tr')[0];
		 var aData = oTable.fnGetData(nTr);

	      if (oTable.fnIsOpen(nTr)) {
	    	  $(this).removeClass('open liq');
	          oTable.fnClose(nTr);
	      } else {
	    	  if ( aData[0]=="DETALLE"){
	    		  $(this).addClass('open liq');
	          	oTable.fnOpen(nTr, TablaLiquidaciones(oTable, nTr), 'details-liq');
	    	  }
	      }

	} );

	$('#tablaNetting tbody').on('click', 'td.details-alc', function () {

		 var nTr = $(this).parents('tr')[0];
		 var aData = oTable.fnGetData(nTr);

	      if (oTable.fnIsOpen(nTr)) {
	    	  $(this).removeClass('open alc');
	          oTable.fnClose(nTr);
	      } else {

    		  $(this).addClass('open alc');
          	 oTable.fnOpen(nTr, TablaALC(oTable, nTr), 'details-alc');
	      }

	} );

	 /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({
            marginTop: '0px'
        }, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({
            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
        }, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
    });

    $('.collapser').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

        return false;
    });
    $('.collapser_search').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        if ($(this).text().indexOf('Ocultar') !== -1) {
            $(this).text("Mostrar opciones de búsqueda");
        } else {
            $(this).text("Ocultar opciones de búsqueda");
        }
        return false;
    });
    $('.btn.buscador').click(function (event) {

    	cargarDatosNetting();
        event.preventDefault();
        seguimientoBusqueda();
        $('.collapser_search').parents('.title_section').next().slideToggle();
        $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
        $('.collapser_search').toggleClass('active');
        if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
            $('.collapser_search').text("Mostrar opciones de búsqueda");
        } else {
            $('.collapser_search').text("Ocultar opciones de búsqueda");
        }
        return false;
    });


});

function render() {

}


function cargarDatosNetting() {

	$("#tablaNetting > tbody").empty();

	var listaAlias = document.getElementById("textAlias").value;
	posicionAlias = availableTagsAlias.indexOf(listaAlias);
	if(posicionAlias=="-1"){
		valorSeleccionadoAlias = document.getElementById("textAlias").value;
	}else {
		valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
	}

	var listaIsin = document.getElementById("textIsin").value;
	posicionIsin = availableTagsIsin.indexOf(listaIsin);
	if(posicionIsin=="-1"){
		valorSeleccionadoIsin = document.getElementById("textIsin").value;
	}else {
		valorSeleccionadoIsin = idOcultosIsin[posicionIsin];
	}

	 var filtro = "{\"fcontratacionDe\" : \""+ $("#fcontratacionDe").val() + "\"," +
	  "\"fcontratacionA\" : \""+ $("#fcontratacionA").val() + "\"," +
	  "\"fliquidacionDe\" : \""+ $("#fliquidacionDe").val() + "\"," +
	  "\"fliquidacionA\" : \""+ $("#fliquidacionA").val() + "\"," +
	  "\"liquidado\" : \""+ $("#liquidado").val() + "\"," +
	  "\"alias\" : \""+ valorSeleccionadoAlias + "\"," +
	  "\"isin\" : \""+ valorSeleccionadoIsin + "\" } ";


    var data = new DataBean();
	data.setService('SIBBACServiceConsultaNetting');
	data.setAction('listaNetting');
	data.setFilters(filtro);
	var request = requestSIBBAC( data );
	request.success(function(json){

		console.log(json);
	    console.log(json.resultados);
		if (json.resultados !== undefined && json.resultados.lista !== undefined){

			var item = null;
			var datos = json.resultados.lista;

			var tbl = $("#tablaNetting > tbody");
			 $(tbl).html("");
			oTable.fnClearTable();

			for ( var k in datos ) {
				item = datos[ k ];

				var enlace_liq = "";
				var valor_liq = "";
				if (item.n_liquidaciones!==0){
					valor_liq = "DETALLE";
				}else{
					valor_liq = "PENDIENTE";
				}


				var rowIndex = oTable.fnAddData([
								                  valor_liq,
				                                  item.idnetting,
				                                  item.liquidado,
				                                  transformaFechaGuion(item.fechaoperacion),
				                                  transformaFechaGuion(item.fechacontratacion),
				                                  item.cdalias,
				                                  item.camaracompensacion,
				                                  item.cdisin,
				                                  item.nbvalors,
				                                  item.nutiulototales,
				                                  a2digitos(item.efectivoneteado),
				                                  a2digitos(item.corretajeneteado),
				                                  item.contabilizado,
				                                  transformaFechaGuion(item.fechacontabilizado)
				            					]);

				var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
				if ( item.n_liquidaciones!==0 ){
					$('td', nTr)[0].setAttribute( 'style', 'cursor: pointer; color: #ff0000;' );
				}
				$('td', nTr)[1].setAttribute( 'style', 'cursor: pointer; color: #ff0000;' );
				$('td', nTr)[5].setAttribute( 'title', item.descrali );


			}


		}


	});


}


function TablaALC(Table, nTr) {

    var aData = Table.fnGetData(nTr);
    var contador = Table.fnGetPosition(nTr);

    var resultado =
	    "<tr id='tabla_a_desplegar_ALC"+ contador + "' style='display:none;'>" +
	    "<td colspan='13' style='border-width:0 !important;background-color:#dfdfdf;'>"+
		"<table id='tablaParam_ALC"+ contador +"' style=width:100%; margin-top:5px;'>" +
		"<tr>"+
			"<th class='taleft' style='background-color: #000 !important;'>Estado</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Sentido</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Títulos</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Efectivo</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Corretaje</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Neto</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Canon Contratación</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Canon Compensación</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Canon Liquidación</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>NIF</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Titular</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Booking</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Nucnfclt</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Nucnfliq</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Ref. bancaria</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Ourparticipe</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Theirparticipe</th>" +
		"</tr></table></td></tr>";

    desplegarDatosALC( aData[1], contador);

    return resultado;
}


//funcion para desplegar la tabla de alc
function desplegarDatosALC( idnetting, numero_fila) {


	console.log("desplegarDatosALC");


	var filtro = "{\"idnetting\" : \""+ idnetting + "\" } ";


	var data = new DataBean();
	data.setService('SIBBACServiceEntregaRecepcion');
	data.setAction('ListaEntregaRecepcion');
	data.setFilters(filtro);
	var request = requestSIBBAC( data );
	request.success(function(json){

	    console.log(json);
	    console.log(json.resultados);

		if (json.resultados !== undefined && json.resultados.lista !== undefined){

			var item = null;
			var contador = 0;
			var datos = json.resultados.lista;


			for ( var k in datos ) {
				item = datos[ k ];

				if (contador%2==0) {
					estilo = "even";
				} else {
					estilo = "odd";
				}
				contador++;

				$("#tablaParam_ALC"+numero_fila).append(
						"<tr class='"+ estilo +"' id='filaRecuperadaALC"+ numero_fila +"' style='background-color: #f5ebc5 !important;' >" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.estadoentrec +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.cdtpoper +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.nutitliq +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ a2digitos(item.imefeagr) +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ a2digitos(item.imcomisin) +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ a2digitos(item.imnfiliq) +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.canon_contratacion +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.canon_compensacion +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.canon_liquidacion +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.cdniftit +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.nbtitliq +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.nbooking +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.nucnfclt +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.nucnfliq +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.cdrefban +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.ourpar +"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+ item.theirpar +"</td>" +
						"</tr>"
				);


				$("#tablaParam_ALC"+numero_fila).append("</table>" );


			}


		}

		desplegar('tabla_a_desplegar_ALC'+ numero_fila, 'estadoT'+ numero_fila, 'estadoT' + numero_fila );

	});




}


function TablaLiquidaciones(Table, nTr) {

    var aData = Table.fnGetData(nTr);
    var contador = Table.fnGetPosition(nTr);

    var resultado =
	    "<tr id='tabla_a_desplegar_LIQ"+ contador + "' style='display:none;'>" +
		"<td style='border-width:0 !important;background-color:#dfdfdf;'>"+
		"<table id='tablaParamLIQ"+ contador +"' style=width:100%; margin-top:5px;'>" +
		"<tr>"+
			"<th class='taleft' style='background-color: #000 !important;'>Estado</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Fecha Liquidada</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Fecha Operación</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Fecha Liquidación</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Titulos liquidados</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Neto liquidado</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Efectivo</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Corretaje</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Contabilizado</th>" +
			"<th class='taleft' style='background-color: #000 !important;'>Fecha contabilizado</th>" +
		"</tr></table></td></tr>";

	desplegarLiquidaciones( aData[1], contador, nTr);

    return resultado;
}


//funcion para desplegar la tabla de liquidaciones
function desplegarLiquidaciones( idnetting, numero_fila) {


	console.log("desplegarLiquidaciones");

	var filtro = "{\"idnetting\" : \""+ idnetting + "\" } ";


	var data = new DataBean();
	data.setService('SIBBACServiceEntregaRecepcion');
	data.setAction('ListaLiquidacionS3');
	data.setFilters(filtro);
	var request = requestSIBBAC( data );
	request.success(function(json){

	    console.log(json);
	    console.log(json.resultados);
		if (json.resultados !== undefined && json.resultados.lista !== undefined){
			var datos = json.resultados.lista;
			var item = null;
			var contador = 0;
			var estilo = null;

			for ( var k in datos ) {

				item = datos[ k ];

				if (contador%2==0) {
					estilo = "even";
				} else {
					estilo = "odd";
				}
				contador++;

				var enlace ="";
				if ( item.cdestados3 =="ERR" ) {
					enlace = "onClick=\"desplegarErrores("+item.idliquidacion+ ", '"+  numero_fila + contador +"')\" ";
					enlace = enlace + " style=\"cursor: pointer; color: #ff0000;\"";
				} else {
					enlace = "";
				}

				$("#tablaParamLIQ"+numero_fila).append(
						"<tr class='"+ estilo +"' id='filaRecuperadaLIQ"+ numero_fila +"' style='background-color: #f5ebc5 !important;' >" +
						"<td class=\"taleft\"" + enlace+ " style='background-color: #f5ebc5 !important;'>"+item.cdestados3+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+transformaFechaGuion(item.fechaliquidacion)+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+transformaFechaGuion(item.fechaoperacion)+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+transformaFechaGuion(item.fechavalor)+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+item.nutiuloliquidados+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+a2digitos(item.importenetoliquidado)+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+a2digitos(item.importeefectivo)+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+a2digitos(item.importecorretaje)+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+item.contabilizado+"</td>" +
						"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+transformaFechaGuion(item.fechacontabilidad)+"</td>" +
						"</tr>"
				);

				if ( item.cdestados3 =="ERR" ) {
					$("#tablaParamLIQ"+numero_fila).append(
						 "<tr id='tabla_a_desplegar_ERR"+ numero_fila + contador +"' style='display:none;'>" +
							"<td colspan='10' style='border-width:0 !important;background-color:#dfdfdf;'>"+
							"<table id='tablaParamERR"+  numero_fila + contador +"' style=width:100%; margin-top:5px;'>" +
							"<tr>"+
								"<th class='taleft' >Error cliente</th>" +
								"<th class='taleft' >Error SV</th>" +
								"<th class='taleft' >Descripción</th>" +
						"</tr></table></td></tr>"
					);
				}

			}


			$("#tablaParamLIQ"+numero_fila).append("</table>" );


		}


		desplegar('tabla_a_desplegar_LIQ'+ numero_fila, 'estadoT'+ numero_fila, 'estadoT' + numero_fila );

	});


}

//funcion para desplegar la tabla de parametrizacion
function desplegarErrores( idLiquidacion, posicion) {


	console.log("desplegarErrores");

	var existe_tabla = document.getElementById('filaRecuperadaERR'+ posicion);

	if (existe_tabla!=null){
		desplegar('tabla_a_desplegar_ERR'+ posicion, 'estadoT'+ posicion, 'estadoT' + posicion );
		return;
	}

    var params = {"service": "SIBABCServiceEntregaRecepcion",
        "action": "listaErroresS3",
        "filters": {
        			"idliquidacion": idLiquidacion
        		}
    };


	$.ajax({

		type: 'POST',
	    dataType: "json",
	    url: "/sibbac20back/rest/service",
	    data: JSON.stringify(params),
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {

		    console.log(json);
		    console.log(json.resultados);
			if (json.resultados !== undefined && json.resultados.lista !== undefined){
				var datos = json.resultados.lista;
				var item = null;
				var estilo = null;

				for ( var k in datos ) {

					item = datos[ k ];

					$("#tablaParamERR"+posicion).append(
							"<tr class='odd' id='filaRecuperadaERR"+ posicion + "' >" +
							"<td class='taleft' >"+item.datoerrorcliente+"</td>" +
							"<td class='taleft' >"+item.datoerrorsv+"</td>" +
							"<td class='taleft' >"+item.dserror+"</td>" +
							"</tr>"
					);

				}

				$("#tablaParamERR"+posicion).append("</table>" );


			}

			 desplegar('tabla_a_desplegar_ERR'+ posicion, 'estadoT'+ posicion, 'estadoT' + posicion );

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});

}


//funcion para desplegar las subtablas
function desplegar( tabla_a_desplegar, e1, estadoTfila ) {
	var tablA = document.getElementById(tabla_a_desplegar);
	var estadOt = document.getElementById(e1);
	var fila = document.getElementById(estadoTfila);
	//recorremos las filas y ocultamos todas menos esta

	switch(tablA.style.display) {
					case "none":
					tablA.style.display = "";
					if (estadOt!=null) { estadOt.innerHTML = "Ocultar"; }
					break;

					default:
					tablA.style.display = "none";
					if (estadOt!=null) { estadOt.innerHTML = "Mostrar"; }
					break;
			}
}

function cargarIsin()
{

	var data = new DataBean();
	data.setService('SIBBACServiceConsultaNetting');
	data.setAction('TodosIsin');

	var request = requestSIBBAC( data );
	request.success(function(json) {
			if (json.resultados==null)
			{
				alert(json.error);
			}
			else
			{
				var datos = json.resultados.lista;

				for ( var k in datos ) {
					item = datos[ k ];

					idOcultosIsin.push(item.id);
					ponerTag = item.id+" - "+item.descripcion;
					availableTagsIsin.push(ponerTag);

				}

				$( "#textIsin" ).autocomplete({
		             source: availableTagsIsin
		        });

			}
		});

}

function cargarAlias()
{

	var data = new DataBean();
	data.setService('SIBBACServiceConsultaNetting');
	data.setAction('AliasNetting');

	var request = requestSIBBAC( data );
	request.success(function(json) {
			if (json.resultados==null)
			{
				alert(json.error);
			}
			else
			{
				var datos = json.resultados.lista;

				for ( var k in datos ) {
					item = datos[ k ];
					idOcultosAlias.push(item.id);
					ponerTag = item.id+" - "+item.descripcion;
					availableTagsAlias.push(ponerTag);
				}

				$( "#textAlias" ).autocomplete({
		             source: availableTagsAlias
		        });

			}
		});


}


//exportar
function SendAsExport( format ) {
	var c = this.document.forms['Netting'];
	var f = this.document.forms["export"];

	f.action="/sibbac20back/rest/export." + format;

	var listaAlias = document.getElementById("textAlias").value;
	posicionAlias = availableTagsAlias.indexOf(listaAlias);
	if(posicionAlias=="-1"){
		valorSeleccionadoAlias = document.getElementById("textAlias").value;
	}else {
		valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
	}

	var listaIsin = document.getElementById("textIsin").value;
	posicionIsin = availableTagsIsin.indexOf(listaIsin);
	if(posicionIsin=="-1"){
		valorSeleccionadoIsin = document.getElementById("textIsin").value;
	}else {
		valorSeleccionadoIsin = idOcultosIsin[posicionIsin];
	}


	var json = {
		    "service": "SIBBACServiceConsultaNetting",
		    "action": "listaNetting",
	        "filters" : {"isin":valorSeleccionadoIsin,
	            "alias":valorSeleccionadoIsin,
	            "fcontratacionDe": $("#fcontratacionDe").val(),
	            "fcontratacionA": $("#fcontratacionA").val(),
	            "fliquidacionDe": $("#fliquidacionDe").val(),
	            "fliquidacionA": $("#fliquidacionA").val()}
	};

	if ( json!=null && json!=undefined && json.length==0 ) {
		alert("Introduzca datos");
	} else {
		f.webRequest.value=JSON.stringify(json);
		f.submit();
	}
}

function seguimientoBusqueda(){

	var listaAlias = document.getElementById("textAlias").value;
	posicionAlias = availableTagsAlias.indexOf(listaAlias);
	if(posicionAlias=="-1"){
		valorSeleccionadoAlias = document.getElementById("textAlias").value;
	}else {
		valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
	}

	var listaIsin = document.getElementById("textIsin").value;
	posicionIsin = availableTagsIsin.indexOf(listaIsin);
	if(posicionIsin=="-1"){
		valorSeleccionadoIsin = document.getElementById("textIsin").value;
	}else {
		valorSeleccionadoIsin = idOcultosIsin[posicionIsin];
	}

	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";
	if ($('#fcontratacionDe').val() !== "" && $('#fcontratacionDe').val() !== undefined){
		cadenaFiltros += " F. Contratacion Desde:"+ $('#fcontratacionDe').val();
	}
	if ($('#fcontratacionA').val() !== "" && $('#fcontratacionA').val() !== undefined){
		cadenaFiltros += " F. Contratacion Hasta:"+ $('#fcontratacionA').val();
	}
	if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined){
		cadenaFiltros += " F. liquidacion Desde:"+ $('#fliquidacionDe').val();
	}
	if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined ){
		cadenaFiltros += " F. liquidacion Hasta:"+ $('#fliquidacionA').val();
	}

	if ($('#textAlias').val() !== "" && $('#textAlias').val() !== undefined){
		cadenaFiltros += " Alias:"+ valorSeleccionadoAlias;
	}
	if ($('#textIsin').val() !== "" && $('#textIsin').val() !== undefined){
		cadenaFiltros += " Isin:"+ valorSeleccionadoIsin;
	}

	$('.mensajeBusqueda').append(cadenaFiltros);


}
