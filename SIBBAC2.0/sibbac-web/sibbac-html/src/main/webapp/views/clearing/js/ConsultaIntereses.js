var idOcultosAlias = [];
var availableTagsAlias = [];
var popupsAvisoEnvioCalculoIntereses;

var oTable = undefined;
function initialize() {


    var fechas = ["fechaInicio", "fechaHasta"];

    loadpicker(fechas);
    verificarFechas(["fechaInicio", "fechaHasta"]);
    $('#paraalias').loadselectcombo('alias', fnEnviarCalculoIntereses);
    render();


}

$(document).ready(function () {

    initialize();

    oTable = $("#tablaIntereses").dataTable({
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "i18n/Spanish.json"
        }, /*
         "columnDefs": [{
         "className": "checkbox",
         "targets":   0  },{
         "targets":   1  },
         {"className": "monedaR",
         "targets":   [9,10,11]  },
         { 	type: 'formatted-num',
         targets:   [9,10,11]},
         { 	type: 'date-eu',
         targets:   [3,4,13]}
         ],*/
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
            if (aData[2] == "A") {
                $('td:eq(0)', nRow).html('<input type="checkbox" id="checkTabla" name="checkTabla" class="checkTabla" value="N" >');
            } else {
                $('td:eq(0)', nRow).html('<input type="checkbox" id="checkTabla" name="checkTabla" class="checkTabla" value="N" checked="checked" disabled readonly>');
            }

        },
        "scrollY": "480px",
        "scrollCollapse": true,
        "order": []
    });


    $('#tablaIntereses tbody').on('click', 'td.checkbox', function () {

        var nTr = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nTr);

        var valor = $('input', nTr)[0].getAttribute('value');
        if (valor == "N") {
            $('input', nTr)[0].setAttribute('value', aData[0]);
        } else {
            $('input', nTr)[0].setAttribute('value', 'N');
        }

    });
    prepareCollapsion();
    $('.btn.buscador').click(function (event) {
        cargarDatosIntereses();
        event.preventDefault();
        seguimientoBusqueda();
        collapseSearchForm();
        return false;
    });

    $('#textAlias').blur(function (event) {
        fnEnviarCalculoIntereses(event);
        return false;
    });

    $('#canceloEnvioCalculoIntereses').click(function (event) {
        popupsInteresAlta.close();
    });

    $('#aceptoEnvioCalculoIntereses').click(function (event) {
        var data = new DataBean();
        var filter = "{\"cdalias\" : \"" + $("#enviarCalculoIntereses").attr("idAlias") + "\"}";
        var request;

        data.setService('SIBBACServiceFinanciacionIntereses');
        data.setAction('enviarCalculoIntereses');
        data.setFilters(filter);

        request = requestSIBBAC(data);
        request.success(function (json) {
            popupsInteresAlta.close();
        });
    });

    $('#enviarCalculoIntereses').click(function (event) {
        $("#textoDescriptivoAlias").text($("#enviarCalculoIntereses").attr("descripcionAlias"));
        popupsInteresAlta = $("#avisoEnvioCalculoIntereses").bPopup();
    });

    $("#limpiar").click(function (event) {
        $("#enviarCalculoIntereses").attr("idAlias", "");
        $("#enviarCalculoIntereses").attr("descripcionAlias", "");
        $("#enviarCalculoIntereses").hide();
    });

});

function fnEnviarCalculoIntereses(event) {

    var listaAlias = document.getElementById("textAlias").value;
    posicionAlias = availableTagsAlias.indexOf(listaAlias);

    if (posicionAlias == "-1") {
        valorSeleccionadoAlias = document.getElementById("textAlias").value;
    } else {
        valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
    }
    console.log(valorSeleccionadoAlias);
    if (valorSeleccionadoAlias === undefined || valorSeleccionadoAlias === null || valorSeleccionadoAlias === "") {
        $("#enviarCalculoIntereses").attr("idAlias", "");
        $("#enviarCalculoIntereses").attr("descripcionAlias", "");
        $("#enviarCalculoIntereses").hide();
    } else {
        $("#enviarCalculoIntereses").attr("idAlias", valorSeleccionadoAlias);
        try {
            $("#enviarCalculoIntereses").attr("descripcionAlias", listaAlias.split("-")[1]);
        } catch (err) {
            console.error("La estructura del alias no contiene descripcion");
            console.error(err.message);
        }

        $("#enviarCalculoIntereses").show();
    }
}

function render() {
    var input = "<input id='enviarCalculoIntereses' type='button' class='mybutton' value='Enviar Calculo de Intereses'>"
    $('#paraalias').append(input);
    $("#enviarCalculoIntereses").hide();
}

function cargarDatosIntereses() {

    $("#tablaIntereses > tbody").empty();

    var listaAlias = document.getElementById("textAlias").value;
    posicionAlias = availableTagsAlias.indexOf(listaAlias);
    if (posicionAlias == "-1") {
        valorSeleccionadoAlias = document.getElementById("textAlias").value;
    } else {
        valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
    }

    var filtro = "{\"fechaInicio\" : \"" + $("#fechaInicio").val() + "\"," +
            "\"fechaHasta\" : \"" + $("#fechaHasta").val() + "\"," +
            "\"importeInteresesDesde\" : \"" + $("#importeInteresesDesde").val() + "\"," +
            "\"importeInteresesHasta\" : \"" + $("#importeInteresesHasta").val() + "\"," +
            "\"estado\" : \"" + $("#estado").val() + "\"," +
            "\"cdalias\" : \"" + valorSeleccionadoAlias + "\" } ";


    var data = new DataBean();
    data.setService('SIBBACServiceFinanciacionIntereses');
    data.setAction('getListaIntereses');
    data.setFilters(filtro);
    var request = requestSIBBAC(data);
    request.success(function (json) {

        console.log(json);
        console.log(json.resultados);
        if (json.resultados !== undefined && json.resultados.lista !== undefined) {

            var item = null;
            var datos = json.resultados.lista;
            var efectivoTotal = 0;
            var corretajeTotal = 0;

            var tbl = $("#tablaIntereses > tbody");
            $(tbl).html("");
            oTable.fnClearTable();

            for (var k in datos) {
                item = datos[ k ];

                var rowIndex = oTable.fnAddData([item.idInteres,
                    item.referencia,
                    item.estado,
                    item.sentido,
                    item.cdalias,
                    item.mercado,
                    item.isin,
                    item.titular,
                    item.titulos,
                    transformaFechaGuion(item.fechaContratacion),
                    transformaFechaGuion(item.fechaOperacion),
                    transformaFechaGuion(item.fechaLiquidacion),
                    a2digitos(item.importeliquidado),
                    a2digitos(item.tipoInteres),
                    item.base,
                    a2digitos(item.intereses)
                ]);
            }




        }


    });


}

//Evento para marcar gastos
$('#btnMarcar').click(function () {
    var suma = 0;
    var valores = new Array()
    var los_cboxes = document.getElementsByName('checkTabla');
    for (var i = 0, j = los_cboxes.length; i < j; i++) {

        if (los_cboxes[i].value !== 'N') {
            valores.push(los_cboxes[i].value);
            suma++;
        }
    }

    if (suma == 0) {
        alert('Marque al menos una línea');
        return false;
    } else {
        MarcarEstadoBaja(valores);
    }

})

//Funcion para marcar facturas
function MarcarEstadoBaja(valores)
{
    bucleVal = valores.length;
    console.log("entro en el metodo");

    var params = "[ ";
    for (var v = 0; v < bucleVal; v++) {
        params = params + "{ \"idInteres\" :\"" + valores[v] + "\" },";
    }
    params = params.substring(0, params.length - 1);
    params = params + "\ ]";



    var jsonData = "{" +
            "\"service\" : \"SIBBACServiceFinanciacionIntereses\", " +
            "\"action\"  : \"MarcarBajaIntereses\", " +
            "\"params\"  : " + params + "}";

    console.log(jsonData);
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/sibbac20back/rest/service",
        data: jsonData,
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        async: true,
        success: function (json) {

            cargarDatosIntereses();

            //$("section").load("views/clearing/ConsultaIntereses.html");
        },
        error: function (c) {
            alert("ERROR: " + c);
        }

    });
}

function seguimientoBusqueda() {

    var listaAlias = document.getElementById("textAlias").value;
    posicionAlias = availableTagsAlias.indexOf(listaAlias);
    if (posicionAlias == "-1") {
        valorSeleccionadoAlias = document.getElementById("textAlias").value;
    } else {
        valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
    }


    $('.mensajeBusqueda').empty();
    var cadenaFiltros = "";
    if ($('#fechaInicio').val() !== "" && $('#fechaInicio').val() !== undefined) {
        cadenaFiltros += " F. Contratacion Desde:" + $('#fechaInicio').val();
    }
    if ($('#fechaHasta').val() !== "" && $('#fechaHasta').val() !== undefined) {
        cadenaFiltros += " F. Contratacion Hasta:" + $('#fechaHasta').val();
    }
    if ($('#importeInteresesDesde').val() !== "" && $('#importeInteresesDesde').val() !== undefined) {
        cadenaFiltros += " Importe Intereses Desde:" + $('#importeInteresesDesde').val();
    }
    if ($('#importeInteresesHasta').val() !== "" && $('#importeInteresesHasta').val() !== undefined) {
        cadenaFiltros += " Importe Intereses Hasta:" + $('#importeInteresesHasta').val();
    }

    if ($('#textAlias').val() !== "" && $('#textAlias').val() !== undefined) {
        cadenaFiltros += " Alias:" + valorSeleccionadoAlias;
    }

    if ($('#estado').val() !== "" && $('#estado').val() !== undefined) {
        cadenaFiltros += " Estado:" + $('#estado').val();
    }


    $('.mensajeBusqueda').append(cadenaFiltros);


}
