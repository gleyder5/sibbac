var idOcultosAlias = [];
var availableTagsAlias = [];
var idOcultosIsin = [];
var availableTagsIsin = [];
var oTable = undefined;

function initialize() {


    var fechas = ["fcontratacionDe", "fcontratacionA", "fliquidacionDe", "fliquidacionA"];
    loadpicker(fechas);
    verificarFechas([["fcontratacionDe", "fcontratacionA"], ["fliquidacionDe", "fliquidacionA"]]);
    cargarAlias();
    cargarIsin();
    cargarDatosNetting();

}

$(document).ready(function () {

    initialize();

    oTable = $("#tblConsultaTitulos").dataTable({
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "i18n/Spanish.json"
        },

        /*"columnDefs": [ {"className": "checkbox", targets: 0},
                        {"className": "details-alc",targets: 1},
                        {"className": "monedaR", targets: [9, 10, 11]},
                        {type: 'formatted-num', targets: [9, 10, 11]},
                        {type: 'date-eu', targets: [3, 4, 13]}
        ],*/
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
            if (aData[0] == "N") {
                $('td:eq(0)', nRow).html('<input type="checkbox" id="checkTabla" name="checkTabla" class="checkTabla" value="N" >');
            } else {
                $('td:eq(0)', nRow).html('<input type="checkbox" id="checkTabla" name="checkTabla" class="checkTabla" value="N" checked="checked" disabled readonly>');
            }

        },

        "order": [],
        "scrollY": "480px",
        "scrollCollapse": true,
        "scrollX": "100%"
    });


    $('#tblConsultaTitulos tbody').on('click', 'td.checkbox', function () {

        var nTr = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nTr);

        var valor = $('input', nTr)[0].getAttribute('value');
        if (valor == "N") {
            $('input', nTr)[0].setAttribute('value', aData[1]);
        } else {
            $('input', nTr)[0].setAttribute('value', 'N');
        }

    });


    $('#tblConsultaTitulos tbody').on('click', 'td.details-alc', function () {

        var nTr = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nTr);

        if (oTable.fnIsOpen(nTr)) {
            $(this).removeClass('open alc');
            oTable.fnClose(nTr);
        } else {

            $(this).addClass('open alc');
            oTable.fnOpen(nTr, TablaALC(oTable, nTr), 'details-alc');
        }

    });


    prepareCollapsion();
    $('.btn.buscador').click(function (event) {

        cargarDatosNetting();
        event.preventDefault();
        seguimientoBusqueda();
        collapseSearchForm();
        return false;
    });


});

function cargarDatosNetting() {

    $("#tblConsultaTitulos > tbody").empty();

    var listaAlias = document.getElementById("textAlias").value;
    posicionAlias = availableTagsAlias.indexOf(listaAlias);
    if (posicionAlias == "-1") {
        valorSeleccionadoAlias = document.getElementById("textAlias").value;
    } else {
        valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
    }

    var listaIsin = document.getElementById("textIsin").value;
    posicionIsin = availableTagsIsin.indexOf(listaIsin);
    if (posicionIsin == "-1") {
        valorSeleccionadoIsin = document.getElementById("textIsin").value;
    } else {
        valorSeleccionadoIsin = idOcultosIsin[posicionIsin];
    }
    var contabilizado = $("#contabilizado option:selected").val() === "" ? null : $("#contabilizado option:selected").val();
    var filtro = "{\"fcontratacionDe\" : \"" + $("#fcontratacionDe").val() + "\"," +
            "\"fcontratacionA\" : \"" + $("#fcontratacionA").val() + "\"," +
            "\"fliquidacionDe\" : \"" + $("#fliquidacionDe").val() + "\"," +
            "\"fliquidacionA\" : \"" + $("#fliquidacionA").val() + "\"," +
            "\"contabilizado\" : \"" + $("#contabilizado").val() + "\"," +
            "\"liquidado\" : \"" + $("#liquidado option:selected").val() + "\"," +
            "\"alias\" : \"" + valorSeleccionadoAlias + "\"," +
            "\"isin\" : \"" + valorSeleccionadoIsin + "\" } ";


    var data = new DataBean();
    data.setService('SIBBACServiceConsultaNetting');
    data.setAction('listaNettingSinMovimientoTitulos');
    data.setFilters(filtro);
    var request = requestSIBBAC(data);
    request.success(function (json) {

        console.log(json);
        console.log(json.resultados);
        if (json.resultados !== undefined && json.resultados.lista !== undefined) {

            var item = null;
            var datos = json.resultados.lista;
            var efectivoTotal = 0;
            var corretajeTotal = 0;

            oTable.fnClearTable();
            $('#EfectivoTotal').empty();
            $('#CorretajeTotal').empty();

            for (var k in datos) {
                item = datos[ k ];


                var rowIndex = oTable.fnAddData([item.liquidado,
                    item.idnetting,
                    item.liquidado,
                    transformaFechaGuion(item.fechaoperacion),
                    transformaFechaGuion(item.fechacontratacion),
                    item.cdalias,
                    item.camaracompensacion,
                    item.cdisin,
                    item.nbvalors,
                    item.nutiulototales,
                    a2digitos(item.efectivoneteado),
                    a2digitos(item.corretajeneteado),
                    item.contabilizado,
                    transformaFechaGuion(item.fechacontabilizado)
                ]);

                var nTr = oTable.fnSettings().aoData[rowIndex[0]].nTr;
                $('td', nTr)[1].setAttribute('style', 'cursor: pointer; color: #ff0000;');
                $('td', nTr)[5].setAttribute('title', item.descrali);

                efectivoTotal = efectivoTotal + item.efectivoneteado;
                corretajeTotal = corretajeTotal + item.corretajeneteado;

            }


            $('div#EfectivoTotal').html(a2digitos(efectivoTotal));
            $('div#CorretajeTotal').append(a2digitos(corretajeTotal));


        }


    });


}

function TablaALC(Table, nTr) {

    var aData = Table.fnGetData(nTr);
    var contador = Table.fnGetPosition(nTr);

    var resultado =
            "<tr id='tabla_a_desplegar_ALC" + contador + "' style='display:none;'>" +
            "<td colspan='13' style='border-width:0 !important;background-color:#dfdfdf;'>" +
            "<table id='tablaParam_ALC" + contador + "' style=width:100%; margin-top:5px;'>" +
            "<tr>" +
            "<th class='taleft' style='background-color: #000 !important;'>Estado</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Sentido</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Títulos</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Efectivo</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Corretaje</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Neto</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Canon Contratación</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Canon Compensación</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Canon Liquidación</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>NIF</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Titular</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Booking</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Nucnfclt</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Nucnfliq</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Ref. bancaria</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Ourparticipe</th>" +
            "<th class='taleft' style='background-color: #000 !important;'>Theirparticipe</th>" +
            "</tr></table></td></tr>";

    desplegarDatosALC(aData[1], contador);

    return resultado;
}


//funcion para desplegar la tabla de alc
function desplegarDatosALC(idnetting, numero_fila) {


    console.log("desplegarDatosALC");

    var filtro = "{\"idnetting\" : \"" + idnetting + "\" } ";

    var data = new DataBean();
    data.setService('SIBBACServiceEntregaRecepcion');
    data.setAction('ListaEntregaRecepcion');
    data.setFilters(filtro);
    var request = requestSIBBAC(data);
    request.success(function (json) {

        console.log(json);
        console.log(json.resultados);

        if (json.resultados !== undefined && json.resultados.lista !== undefined) {

            var item = null;
            var contador = 0;
            var datos = json.resultados.lista;


            for (var k in datos) {
                item = datos[ k ];

                if (contador % 2 == 0) {
                    estilo = "even";
                } else {
                    estilo = "odd";
                }
                contador++;

                $("#tablaParam_ALC" + numero_fila).append(
                        "<tr class='" + estilo + "' id='filaRecuperadaALC" + numero_fila + "' style='background-color: #f5ebc5 !important;' >" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.estadoentrec + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.cdtpoper + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nutitliq + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + a2digitos(item.imefeagr) + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + a2digitos(item.imcomisin) + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + a2digitos(item.imnfiliq) + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.canon_contratacion + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.canon_compensacion + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.canon_liquidacion + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.cdniftit + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nbtitliq + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nbooking + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nucnfclt + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.nucnfliq + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.cdrefban + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.ourpar + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + item.theirpar + "</td>" +
                        "</tr>"
                        );


                $("#tablaParam_ALC" + numero_fila).append("</table>");


            }


        }

        desplegar('tabla_a_desplegar_ALC' + numero_fila, 'estadoT' + numero_fila, 'estadoT' + numero_fila);

    });




}


//funcion para desplegar las subtablas
function desplegar(tabla_a_desplegar, e1, estadoTfila) {
    var tablA = document.getElementById(tabla_a_desplegar);
    var estadOt = document.getElementById(e1);
    var fila = document.getElementById(estadoTfila);
    //recorremos las filas y ocultamos todas menos esta

    switch (tablA.style.display) {
        case "none":
            tablA.style.display = "";
            if (estadOt != null) {
                estadOt.innerHTML = "Ocultar";
            }
            break;

        default:
            tablA.style.display = "none";
            if (estadOt != null) {
                estadOt.innerHTML = "Mostrar";
            }
            break;
    }
}



function cargarAlias()
{

    var data = new DataBean();
    data.setService('SIBBACServiceConsultaNetting');
    data.setAction('AliasNetting');

    var request = requestSIBBAC(data);
    request.success(function (json) {
        if (json.resultados == null)
        {
            alert(json.error);
        }
        else
        {
            var datos = json.resultados.lista;

            for (var k in datos) {
                item = datos[ k ];

                idOcultosAlias.push(item.id);
                ponerTag = item.id + " - " + item.descripcion;
                availableTagsAlias.push(ponerTag);

            }


            $("#textAlias").autocomplete({
                source: availableTagsAlias
            });

        }
    });


}

function cargarIsin()
{

    var data = new DataBean();
    data.setService('SIBBACServiceConsultaNetting');
    data.setAction('TodosIsin');

    var request = requestSIBBAC(data);
    request.success(function (json) {
        if (json.resultados == null)
        {
            alert(json.error);
        }
        else
        {
            var datos = json.resultados.lista;

            for (var k in datos) {
                item = datos[ k ];

                idOcultosIsin.push(item.id);
                ponerTag = item.id + " - " + item.descripcion;
                availableTagsIsin.push(ponerTag);

            }
            $("#textIsin").autocomplete({
                source: availableTagsIsin
            });

        }
    });

}

//Evento para marcar gastos
$('#btnMarcar').click(function () {
    var suma = 0;
    var valores = new Array()
    var los_cboxes = document.getElementsByName('checkTabla');
    for (var i = 0, j = los_cboxes.length; i < j; i++) {

        if (los_cboxes[i].value !== 'N') {
            valores.push(los_cboxes[i].value);
            suma++;
        }
    }

    if ($("#gastos").val() == "") {
        alert("Introduzca el gasto asociado");
        return false;
    }

    if (suma == 0) {
        alert('Marque al menos una línea');
        return false;
    } else {
        marcarGastos(valores);
    }

})

//Funcion para marcar facturas
function marcarGastos(valores)
{
    bucleVal = valores.length;
    console.log("entro en el metodo");

    var params = "[ ";
    for (var v = 0; v < bucleVal; v++) {
        params = params + "{ \"idNetting\" :\"" + valores[v] + "\" },";
    }
    params = params.substring(0, params.length - 1);
    params = params + "\ ]";

    var filters = "{ \"gastos\": " + $("#gastos").val() + "}";

    var jsonData = "{" +
            "\"service\" : \"SIBBACServiceConsultaNetting\", " +
            "\"action\"  : \"MarcarGastos\", " +
            "\"params\"  : " + params + ", " +
            "\"filters\"  : " + filters + "}";

    console.log(jsonData);
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/sibbac20back/rest/service",
        data: jsonData,
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        async: true,
        success: function (json) {

            cargarDatosNetting();

            $("section").load("views/clearing/ConsultaNettingTitulosSinMov.html");
        },
        error: function (c) {
            alert("ERROR: " + c);
        }

    });
}

function seguimientoBusqueda() {

    var listaAlias = document.getElementById("textAlias").value;
    posicionAlias = availableTagsAlias.indexOf(listaAlias);
    if (posicionAlias == "-1") {
        valorSeleccionadoAlias = document.getElementById("textAlias").value;
    } else {
        valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
    }

    var listaIsin = document.getElementById("textIsin").value;
    posicionIsin = availableTagsIsin.indexOf(listaIsin);
    if (posicionIsin == "-1") {
        valorSeleccionadoIsin = document.getElementById("textIsin").value;
    } else {
        valorSeleccionadoIsin = idOcultosIsin[posicionIsin];
    }

    $('.mensajeBusqueda').empty();
    var cadenaFiltros = "";
    if ($('#fcontratacionDe').val() !== "" && $('#fcontratacionDe').val() !== undefined) {
        cadenaFiltros += " F. Contratacion Desde:" + $('#fcontratacionDe').val();
    }
    if ($('#fcontratacionA').val() !== "" && $('#fcontratacionA').val() !== undefined) {
        cadenaFiltros += " F. Contratacion Hasta:" + $('#fcontratacionA').val();
    }
    if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined) {
        cadenaFiltros += " F. liquidacion Desde:" + $('#fliquidacionDe').val();
    }
    if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined) {
        cadenaFiltros += " F. liquidacion Hasta:" + $('#fliquidacionA').val();
    }

    if ($('#textAlias').val() !== "" && $('#textAlias').val() !== undefined) {
        cadenaFiltros += " Alias:" + valorSeleccionadoAlias;
    }
    if ($('#textIsin').val() !== "" && $('#textIsin').val() !== undefined) {
        cadenaFiltros += " Isin:" + valorSeleccionadoIsin;
    }

    $('.mensajeBusqueda').append(cadenaFiltros);


}




