$(document).ready(function () {
    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		// $(this).toggleText( "Mostrar Alta de Regla", "Ocultar Alta de Regla" );
		if ( $(this).text()=="Ocultar Alta de Regla" ) {
			$(this).text( "Mostrar Alta de Regla" );
		} else {
			$(this).text( "Ocultar Alta de Regla" );
		}
		return false;
	});

/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}

	cargarArchivos();

});


//funcion para cargar la tabla
function cargarArchivos()
{
	console.log("entro en el método");
	var data = new DataBean();
	data.setService('SIBBACServiceFixml');
	data.setAction('getDistinctTipo');
	var request = requestSIBBAC( data );
	request.success( function(json) {
			var resultados = json.resultados.resultados;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				// Rellenamos el combo
				$('#selectArchivo').append(
						"<option value='" + item +"'>" + item +"</option>"

				);

				}
		});
}
//Click para lanzar último proceso
// $('#buscar').click(function(){
function descargarArchivoFIXML(){
	var tipoDeArchivo = $( "#selectArchivo" ).val();
	var fechaDeArchivo = $( "#textFecha" ).val();
	console.log("Proceso de descarga del archivo FIXML: [tipoDeArchivo=="+tipoDeArchivo+"] [fechaDeArchivo=="+fechaDeArchivo+"]");
	var lista = document.getElementById("textFecha");
	var indiceSeleccionado = lista.selectedIndex;
	var opcionSeleccionada = lista.options[indiceSeleccionado];
	var textoSeleccionado = opcionSeleccionada.text;
	var valorSeleccionado = opcionSeleccionada.value;

	if (valorSeleccionado=="0")
		{
			alert("Seleccione los datos necesarios");
			return false;
		} else {
			lanzarPeticion()
		}
}


//Evento para dar de alta la regla
function cargarFechas(){
	//valor del combo
	var lista = document.getElementById("selectArchivo");
	var indiceSeleccionado = lista.selectedIndex;
	var opcionSeleccionada = lista.options[indiceSeleccionado];
	var textoSeleccionado = opcionSeleccionada.text;
	var valorSeleccionado = opcionSeleccionada.value;

	if (valorSeleccionado=="0") {
		alert("Seleccione un archivo");
		return false;
	} else {
		//console.log("entro en el método");
		filter = "{\"filetype\" : \"" + valorSeleccionado + "\"}";
		var data = new DataBean();
		data.setService('SIBBACServiceFixml');
		data.setAction('getAllByTipo');
		data.setFilters(filter);
		var request = requestSIBBAC( data );
		request.success(function(json) {
			var resultados = json.resultados.resultados;
			var item = null;
			$('#textFecha').empty();
			for ( var k in resultados ) {
				item = resultados[ k ];
				console.log(item.ferecepcion);
				// Rellenamos el combo
				$('#textFecha').append(
					"<option value='" + item.id +"'>" + EpochToHuman(item.ferecepcion) +"</option>"

				);
			}
		});
	}
}



//Evento para dar de alta la regla
function lanzarPeticion(){
	//valor del combo
	var lista = document.getElementById("selectArchivo");
	var indiceSeleccionado = lista.selectedIndex;
	var opcionSeleccionada = lista.options[indiceSeleccionado];
	var textoSeleccionado = opcionSeleccionada.text;
	var valorSeleccionado = opcionSeleccionada.value;

	if (valorSeleccionado=="0") {
		alert("Seleccione un archivo");
		return false;
	} else {
		//console.log("entro en el método");
		filter = "{\"filetype\" : \"" + valorSeleccionado + "\"}";
		var data = new DataBean();
		data.setService('SIBBACServiceFixml');
		data.setAction('getAllByTipo');
		data.setFilters(filter);
		var request = requestSIBBAC( data );
		request.success( function(json) {
			var resultados = json.resultados.resultados;
			var item = null;
			$('#textFecha').empty();
			for ( var k in resultados ) {
				item = resultados[ k ];
				console.log(item.feproceso);
				// Rellenamos el combo
				$('#textFecha').append(
					"<option value='" + EpochToHuman(item.feproceso) +"'>" + EpochToHuman(item.feproceso) +"</option>"

				);
			}
		});
	}
}

