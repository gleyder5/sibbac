
function consultar(action, oTable) {

    var data = new DataBean();
    data.setService('SIBBACServiceDatosMaestros');
    data.setAction(action);
    var request = requestSIBBAC(data);
    request.success(function (json) {
        var datos = undefined;
        if (json === undefined || json.resultados === undefined || json.resultados.datosMaestros === undefined) {
            datos = {};
        } else {
            datos = json.resultados.datosMaestros;
        }
        var tbl = $("#datosMaestros > tbody");
        var difClass = 'centrado';
        oTable.fnClearTable();
        $.each(datos, function (key, val) {
            var codigo = '';
            var descripcion = '';
            var trdType = '';
            var trdSubType = '';


            if (datos[key].codigo !== null && datos[key].codigo != undefined) {
                codigo = datos[key].codigo;
            }

            if (datos[key].descripcion !== null && datos[key].descripcion != undefined) {
                descripcion = datos[key].descripcion;
            }

            if (datos[key].trdType !== null && datos[key].trdType != undefined) {
                trdType = datos[key].trdType;
            }

            if (datos[key].trdSubType !== null && datos[key].trdSubType != undefined) {
                trdSubType = datos[key].trdSubType;
            }

            oTable.fnAddData([
                codigo,
                descripcion,
                trdType,
                trdSubType
            ]);
        });
    });
}

/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize(service, oTable) {
    consultar(service, oTable);
}

function loadData(service) {
    var oTable = $("#datosMaestros").dataTable({
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
        },
        "scrollY": "480px",
        "scrollCollapse": true,
        "language": {
            "url": "i18n/Spanish.json"
        }
    });
    initialize(service, oTable);
}

