$(document).ready(function () {

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		$('editarParamPop').bPopup({
        modalClose: false,
        opacity: 0.6,
        positionStyle: 'fixed' //'fixed' or 'absolute'
	});
		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Alta de Grupo Impositivo','Ocultar Alta de Grupo Impositivo');
		return false;
	});

/*BUSCADOR*/
	$('.btn.buscador').click(function(){
		//event.preventDefault();
			$('.resultados').show();
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');

		return false;
	});
/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}
	$('#altaParam').click(function(){
		crearParametrizacion();
	});
	var fechas = ['fechaValido'];
    loadpicker(fechas);
	cargarTabla();

});


//funcion para cargar la tabla
function cargarTabla()
{
	console.log("entro en el método");
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceGruposImpositivos\", \"action\"  : \"getGruposImpositivos\"}",
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var resultados = json.resultados;
			var item = null;
			var contador = 0;

			for ( var k in resultados ) {
				item = resultados[ k ];
				if (contador%2==0) {
					estilo = "even";
				} else {
					estilo = "odd";
				}
				// Pintamos la tabla "padre". La cabecera...
				$('.tablaPrueba').append(
						"<tr class='"+ estilo +"'>" +
							"<td>"+item.id + "</td> " +
							"<td class='taleft'>"+item.descripcion + "</td>" +
							"<td class='taleft'>"+item.porcentaje + "</td>" +
							"<td class='taleft'>"+item.categoria + "</td>" +
							"<td class='taleft'>"+item.cancelado + "</td>" +
							"<td class='taleft'>"+item.validoDesde + "</td>" +
					 "</tr>"

				);
				contador++;
				}

				// "Footer" de la tabla "padre".
				$('.tablaPrueba').append( "</table>" );

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});

}

//Evento para dar de alta la regla
$('#btnAltaRegla').click(function(){
	var codigo = document.getElementById('textCodigo').value;
	var descripcion =  document.getElementById('textDesc').value;
	var porcentaje = document.getElementById('textPorcen').value;
	var categoria = document.getElementById('textCategoria').value;
	var cancelado = document.getElementById('textCancelado').value;
	alert(porcentaje);
	if(porcentaje>100){
		alert("El porcentaje no puede ser superior al 100%");
		return false;
	}

	var validoDesde = document.getElementById('fechaValido').value;
	console.log("fin de datos");
	if (codigo=="" || descripcion =="" || porcentaje =="" || categoria=="" || cancelado=="" || validoDesde=="")
		{
			alert("Alguno de los campos es vacío. Debe rellenarlo para poder dar de alta");
			return false;
		} else {
			console.log("entro en dar alta");
			altaRegla(codigo, descripcion, porcentaje, categoria, cancelado, validoDesde);
		}
})

//Funcion para el alta de la regla
function altaRegla(codigo, descripcion, porcentaje, categoria, cancelado, validoDesde)
{
	console.log("entro en el metodo");
	var filtro = "{\"id\" : \""+ codigo + "\"," +
			      "\"descripcion\" : \""+ descripcion + "\"," +
			      "\"porcentaje\" : \""+ porcentaje + "\"," +
			      "\"categoria\" : \""+ categoria + "\"," +
			      "\"cancelado\" : \""+ cancelado + "\"," +
			      "\"validoDesde\" : \""+ validoDesde + "\" }";

	var jsonData = "{" +
			"\"service\" : \"SIBBACServiceGruposImpositivos\", " +
			"\"action\"  : \"createGrupoImpositivo\", "+
			"\"filters\"  : "+ filtro + "}";
	console.log(jsonData);
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: jsonData,
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			$("section").load("views/grupos/GruposImpositivos.html");
		},
		error: function(c) {
			alert( "ERROR: " + c );
		}

	});
}





