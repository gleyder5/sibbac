

var oTable = undefined;
var tablasParams = [];
$(document).ready(function () {
	oTable = $("#tblPeriocidad").dataTable({
		 "dom": 'T<"clear">lfrtip',
		 "language": {
			"url": "i18n/Spanish.json"
		},
		"tableTools": {
			"sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
        "scrollY": "480px",
        "scrollCollapse": true,
		"bSort" : false
	});
	initialize();
	cargarTabla();
});
	/*Eva:Función para cambiar texto*/
	jQuery.fn.extend({
		toggleText: function (a, b) {
			var that = this;
			if (that.text() != a && that.text() != b) {
				that.text(a);
			} else if (that.text() == a) {
				that.text(b);
			} else if (that.text() == b) {
				that.text(a);
			}
			return this;
		}
	});
	/*FIN Función para cambiar texto*/

	$('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		// $(this).toggleText( "Mostrar Alta de Regla", "Ocultar Alta de Regla" );
		if ( $(this).text()=="Ocultar Alta de Regla" ) {
			$(this).text( "Mostrar Alta de Regla" );
		} else {
			$(this).text( "Ocultar Alta de Regla" );
		}
		return false;
	});

/*BUSCADOR*/
	$('.btn.buscador').click(function(){
		//event.preventDefault();
			$('.resultados').show();
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');
		return false;
	});
/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}
	$('#altaParam').click(function(){

		var fechaDesde = document.getElementById('fDesdeParam').value;
		var fechaHasta = document.getElementById('fHastaParam').value;
		var margenAnt = document.getElementById('margenAnterior').value;
		var margenPost= document.getElementById('margenPosterior').value;
		var hora= document.getElementById('horaParam').value;
		var minuto= document.getElementById('minParam').value;
		var diaMes = document.getElementById('diaMes').value;

		var listaIdMes = document.getElementById("idMes");
		var indiceSeleccionado = listaIdMes.selectedIndex;
		var opcionSeleccionada = listaIdMes.options[indiceSeleccionado];
		var textoSeleccionadoidMes = opcionSeleccionada.text;
		var valorSeleccionadoidMes = opcionSeleccionada.value;


		if (!validarFecha('fDesdeParam'))
		{
		  alert("La fecha desde no tiene el formato correcto (dd/mm/yyyy)");
		  return false;
		}


		if (fechaHasta!="")
			{
				if (!validarFecha('fHastaParam'))
					{
					  alert("La fecha hasta no tiene el formato correcto (dd/mm/yyyy)");
					  return false;
					}
			}


		//if mes = 0 { compruebo día y si lo hay error } else {
		if ((valorSeleccionadoidMes=="0") && (diaMes!=""))
			{
			alert("Debe seleccionar un mes");
			return false;
			}
		if (!validaMesDia(valorSeleccionadoidMes, diaMes) && (valorSeleccionadoidMes!="0"))
		{
			alert("El mes seleccionado no tiene " + diaMes + " dias");
			 return false;
		}

		//}

		if (hora > 24)
			{
			alert("No existen mas de 24 horas");
			 return false;
			}
		if (minuto >60)
			{
			alert("No existen mas de 60 minutos");
			 return false;
			}
		else
			{
			crearParametrizacion();
			}
		initialize();
	});

	function cambiaDiaSemana(param)
	{

		//cojo el valor del id y si es distinto de cero cambio lo de mes
		  var lista = document.getElementById("diaSemana"+param);
		  var indiceSeleccionado = lista.selectedIndex;
		  var opcionSeleccionada = lista.options[indiceSeleccionado];
		  var textoSeleccionado = opcionSeleccionada.text;
		  var valorSeleccionado = opcionSeleccionada.value;
		  if(valorSeleccionado!="0"){
			  document.getElementById("diaMes"+param).value="";
			  document.getElementById("idMes"+param).selectedIndex="0";


		  }
	}

	function  cambioMes(param)
	{
		 var lista = document.getElementById("idMes"+param);
		  var indiceSeleccionado = lista.selectedIndex;
		  var opcionSeleccionada = lista.options[indiceSeleccionado];
		  var textoSeleccionado = opcionSeleccionada.text;
		  var valorSeleccionado = opcionSeleccionada.value;
		  if(valorSeleccionado!="0"){
			  document.getElementById("diaSemana"+param).selectedIndex="0";
		  }

	}


	function cambioFestivo(param,num)
	{

		if (param ==true)
			{
				$('#cambioExcepcionar'+num).css('display', 'block');
				//document.getElementById("labAnterior").checked='true';
			}
		else
			{
				$('#cambioExcepcionar'+num).css('display', 'none');
				//desmarco laborable anterior y posterior
				document.getElementById("labAnterior"+num).checked="";
				document.getElementById("labPosterior"+num).checked="";
			}
	}

	function cambioLabAnterior(param, num)
	{
		if (param ==true)
		{
			document.getElementById("labPosterior"+num).checked="";
		}
	}

	function cambioLabPosterior(param, num)
	{
		if (param ==true)
		{
			document.getElementById("labAnterior"+num).checked="";
		}
	}




//funcion para cargar la tabla
function cargarTabla()
{
	console.log("entro en el método cargartabla nuevo");
	var data = new DataBean();
	data.setService('SIBBACServicePeriodicidades');
	data.setAction('getReglasAndParams');
	var request = requestSIBBAC( data );
	request.success( function(json) {
			console.log(json);
			console.log(json.resultados);
			if (json.resultados !== undefined){
				var resultados = json.resultados;
				var item = null;
				var contador = 0;
				var nParametrizaciones = 0;
				oTable.fnClearTable();
				var rownum =0;
				for ( var k in resultados ) {

				// En "k" tengo lo de la izquierda, por ejemplo: "Id_Regla_1".
				// En "item" tengo todo lo de la derecha.
				// En "item.parametrizacion.length" tenemos el numero de "hijos".
					item = resultados[ k ];
					nParametrizaciones = item.parametrizacion.length;

					if (item.activo=="true") {
						imagenactivo = "activo";
						titleactivo = "Desactivar";
					} else {
						imagenactivo = "desactivo";
						titleactivo = "Activar";
					}
					if (contador%2==0) {
						estilo = "even";
					} else {
						estilo = "odd";
					}

					var enlace ="";

					if ( nParametrizaciones>0 ) {
						// Tenemos "nParametrizaciones" parametrizaciones.

						enlace = "onClick=\"desplegar(\'tabla_a_desplegar"+contador+"\',\'estadoT"+contador+"\', \'estadoTfila"+contador+"\', "+contador+")\" ";
						enlace = enlace + " style=\"cursor: pointer; color: #ff0000;\"";
					} else {
						enlace = "";
					}

					// Pintamos la tabla "padre". La cabecera...
					oTable.fnAddData([
										"<div class='taleft' " + enlace + " >" +item.idReglaUsuario + "</div> ",
										"<div class='taleft' >" +item.descripcion + "</div> ",
										"<div class='taleft' >" +transformaFecha(item.fechaDesde) + "</div> ",
										"<div class='taleft' >" +transformaFecha(item.fechaHasta) + "</div> ",
										"<div class='tacenter' ><a class=\"btn\" onclick=\"cambioActivoRegla('"+item.id+"','"+ item.activo+"')\"><img src='img/"+ imagenactivo +".png'  title='"+ titleactivo +"'/></a></div> ",
										"<div class='tacenter' ><a class=\"btn\" onclick = \"document.getElementById('formulariosParametrizacion').style.display='block';document.getElementById('fade').style.display='block';cargoIdRegla('"+item.idReglaUsuario+"'); \"><img src='img/addParam.png'  title='AltaParametro' /></a></div> ",
										"<div class='taleft' ><a class=\"btn\" onclick = \"document.getElementById('formulariosModificarRegla').style.display='block';document.getElementById('fade').style.display='block';cargoInputsReglas('"+item.id+"','"+item.idReglaUsuario+"','"+item.descripcion+"','"+transformaFecha(item.fechaDesde)+"','"+transformaFecha(item.fechaHasta)+"','"+item.activo+"'); \"><img src='img/editp.png' title='Editar'/></a>&nbsp;<a class=\"btn\" onclick=\"borrarRegla('"+item.idReglaUsuario+"');\"><img src='img/del.png'  title='Eliminar'/></a></div> "
										]);
					$(oTable.fnGetNodes(rownum)).addClass(estilo);
					/*$('.tablaPrueba').append(
						"<tr class='"+ estilo +"'>" +
							"<td class=\"taleft\"" + enlace+ ">"+item.idReglaUsuario + "</td> " +
							"<td class='taleft'>"+item.descripcion + "</td>" +
							"<td class='taleft'>"+transformaFecha(item.fechaDesde) + "</td>" +
							"<td class='taleft'>"+transformaFecha(item.fechaHasta) + "</td>" +
							"<td class='tacenter'><a href='#' onclick=\"cambioActivoRegla('"+item.id+"','"+ item.activo+"')\"><img src='img/"+ imagenactivo +".png'  title='"+ titleactivo +"'/></a></td>" +
							"<td class='tacenter'><a href='#' onclick = \"document.getElementById('formulariosParametrizacion').style.display='block';document.getElementById('fade').style.display='block';cargoIdRegla('"+item.idReglaUsuario+"'); \"><img src='img/addParam.png'  title='AltaParametro' /></a></td>" +
							"<td class='taleft' ><a href='#' onclick = \"document.getElementById('formulariosModificarRegla').style.display='block';document.getElementById('fade').style.display='block';cargoInputsReglas('"+item.id+"','"+item.idReglaUsuario+"','"+item.descripcion+"','"+transformaFecha(item.fechaDesde)+"','"+transformaFecha(item.fechaHasta)+"','"+item.activo+"'); \"><img src='img/editp.png' title='Editar'/></a>&nbsp;<a href='#' onclick=\"borrarRegla('"+item.idReglaUsuario+"');\"><img src='img/del.png'  title='Eliminar'/></a></td>" +
					 "</tr>"
					);*/

					if ( nParametrizaciones>0 ) {
					// Tenemos "nParametrizaciones" parametrizaciones. Pintamos cabecera.
						var row ='';
						 row += "<tr id='tabla_a_desplegar"+ contador +"' style='display:none;'>" +
								"<td colspan='7' style='border-width:0 !important;background-color:#dfdfdf;'>"+
								"<table id='tablaParam"+ contador +"' style=width:100%; margin-top:5px;'>" +
												"<tr>"+
													"<th class='taleft' style='background-color: #000 !important;'>Nombre</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Fecha desde</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Fecha hasta</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Excepcionar Festivos</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Laborable Anterior</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Laborable Posterior</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Día Semana</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Mes</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Día Mes</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Hora</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Minuto</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Margen Anterior</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Margen Posterior</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Activa</th>" +
													"<th class='taleft' style='background-color: #000 !important;'>Acciones</th>" +
												"</tr>";
						var parametrizacion = null;

					// Pintamos el cuerpo.
						var contParm = 0;
						for ( var p in item.parametrizacion ) {
							parametrizacion = item.parametrizacion[p];
							estiloParam = ( contParm%2==0 ) ? "even" : "odd";


							imagenactivoParam = ( parametrizacion.activoParam=="true" ) ? "activo" : "desactivo";
							titleactivoParam = ( parametrizacion.activoParam=="true" ) ? "Desactivar" : "Activar";
							excepcionar = ( parametrizacion.excepcionarFestivos=="true" ) ? "checked='checked'" : "";
							laborablePosterior = ( parametrizacion.laborablePosterior=="true" ) ? "checked='checked'" : "";
							laborableAnterior = ( parametrizacion.laborableAnterior=="true" ) ? "checked='checked'" : "";


							var diaSemana=parametrizacion.diaSemanaParamName;

							if (diaSemana==("null"))
								{
								diaSemana="";
							}

							var mes =parametrizacion.mesParamName;
							if (mes==("null"))
								{
								mes="";
								}
							var diaMes = parametrizacion.diaMesParam;
							if (diaMes==("null"))
								{
								diaMes="";
								}



								row +=	"<tr class='"+estiloParam+"' id='editDiv'>"+
								"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+parametrizacion.idParam+"</td>" +
								"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+transformaFecha(parametrizacion.fechaDesdeParam)+"</td>" +
								"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+transformaFecha(parametrizacion.fechaHastaParam)+"</td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'><input type='checkbox' class='checkTabla' " + excepcionar +" disabled/></td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'><input type='checkbox' class='checkTabla' " + laborableAnterior +"  disabled/></td>" 	+
											"<td class='taleft' style='background-color: #f5ebc5 !important;'><input type='checkbox' class='checkTabla' " + laborablePosterior +"  disabled/></td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+diaSemana+"</td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+mes+"</td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+diaMes+"</td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+parametrizacion.hora+"</td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+parametrizacion.minuto+"</td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+parametrizacion.margenAnterior+"</td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'>"+parametrizacion.margenPosterior+"</td>" +
											"<td class='taleft' style='background-color: #f5ebc5 !important;'><a class=\"btn\" onclick=\"cambioActivoParametrizacion("+parametrizacion.idParam+","+ parametrizacion.activoParam+")\"><img src='img/"+ imagenactivoParam +".png'  title='"+ titleactivoParam +"'/></a></td>"+
											"<td class='taleft' style='background-color: #f5ebc5 !important;'><a class=\"btn\" onclick = \"document.getElementById('formulariosParametrizacionModificacion').style.display='block';document.getElementById('fade').style.display='block';cargoInputs('"+parametrizacion.idParam+"','"+ item.idReglaUsuario+"','"+transformaFecha(parametrizacion.fechaDesdeParam)+"','"+transformaFecha(parametrizacion.fechaHastaParam)+"','"+parametrizacion.excepcionarFestivos+"','"+parametrizacion.laborableAnterior+"','"+parametrizacion.laborablePosterior+"','"+diaMes+"','"+parametrizacion.mesParam+"','"+parametrizacion.diaSemanaParam+"','"+parametrizacion.hora+"','"+parametrizacion.minuto+"','"+parametrizacion.margenAnterior+"','"+parametrizacion.margenPosterior+"','"+parametrizacion.activoParam+"' ); \"><img src='img/editp.png' title='Editar'/></a>&nbsp;<a class=\"btn\" onclick=borrarParam("+parametrizacion.idParam+");><img src='img/del.png'  title='Eliminar'/></a></td>" +
								"</tr>"
							;
								contParm++;
								}
						}

					// Pintamos el pie.
					row += "</table></td></tr>" ;

					tablasParams.push(row);
						contador++;
						rownum++;
				}

				// "Footer" de la tabla "padre".
				$('.tablaPrueba').append( "</table>" );
			}else{
				$("section").load("views/periodicidad/periodicidad.html");
			}
		});
}


function cargoInputsReglas(id,idRegla, descr, fInicio, fFin, activa)
{
	document.getElementById('textIdReg').value = id;
	document.getElementById('idReglaFormulario').value = idRegla;
	document.getElementById('descReglaFormulario').value = descr;
	document.getElementById('fIniReglaFormulario').value = fInicio;
	document.getElementById('fFinReglaFormulario').value = fFin;
	if (activa=='true')
	{
		document.getElementById('activaReglaFormulario').checked = activa;
	}
	else
		{
		document.getElementById('activaReglaFormulario').checked = "";
		}






}

function borrarRegla(id)
{
	var filtro = "{\"idReglaUsuario\" : \""+ id + "\" }";

	if (confirm("¿Desea borrar realmente la regla " + id + "?") == true) {
		var data = new DataBean();
		data.setService('SIBBACServicePeriodicidades');
		data.setAction('deleteRegla');
		data.setFilters(filtro);
		var request = requestSIBBAC( data );
		request.success( function(json) {
				if (json.resultados==null)
				{
					alert(json.error);
				}
				else
				{
					$("section").load("views/periodicidad/periodicidad.html");
				}

			});
	} else {
	   // alert("You pressed Cancel!");
	}

}

function cambioActivoRegla(id, activoRegla)
{
	var activo=true;
	if (activoRegla=='true')
		{
		activo=false;
		}
	else
		{
		activo=true;
		}
	var filtro = "{\"id\" : \""+ id + "\","+
				  "\"activo\" : \""+ activo + "\" }";

	 if (confirm("¿Desea cambiar el estado de la regla " + id + "?") == true) {
		var data = new DataBean();
			data.setService('SIBBACServicePeriodicidades');
			data.setAction('updateActivoRegla');
			data.setFilters(filtro);
			var request = requestSIBBAC( data );
			request.success( function(json) {
				if (json.resultados==null)
				{
					alert(json.error);
				}
				else
				{
					$("section").load("views/periodicidad/periodicidad.html");
				}

			});
	 }

}



function cambioActivoParametrizacion(id, activoParam)
{
	var activo=true;
	if (activoParam==true)
		{
		activo=false;
		}
	else
		{
		activo=true;
		}
	var filtro = "{\"id\" : \""+ id + "\","+
				  "\"activo\" : \""+ activo + "\" }";
	 if (confirm("¿Desea cambiar el estado de la parametrización " + id + "?") == true) {
		var data = new DataBean();
			data.setService('SIBBACServicePeriodicidades');
			data.setAction('updateActivoParametrizacion');
			data.setFilters(filtro);
			var request = requestSIBBAC( data );
			request.success(function(json) {
				if (json.resultados==null)
				{
					alert(json.error);
				}
				else
				{
					$("section").load("views/periodicidad/periodicidad.html");
				}
			});
	 }

}

//funcion para desplegar la tabla de parametrizacion
function desplegar( tabla_a_desplegar, e1, estadoTfila,row ) {
	$(oTable.fnGetNodes(row)).after(tablasParams[row]);
	var tablA = document.getElementById(tabla_a_desplegar);
	var estadOt = document.getElementById(e1);
	var fila = document.getElementById(estadoTfila);
	console.log($(tablA).css('display'));
	if ($(tablA).css('display')==='none'){
		$(tablA).css('display','');
	}
	else{
		$(tablA).css('display','none')
	}
	tablasParams[row]=tablA;

}





//cargar en los inputs del edit modal los valores correspondientes
function cargoInputs(id,idRegla,fDesde,fHasta,excep, labAnt, labPos,diaMes,mes,diaSemana, hora,min,mAnt,mPos, activo){


	var excepcionar = ( excep=="true" ) ? "checked='checked'" : "";
	var laborablePosterior = ( labPos=="true" ) ? "checked='checked'" : "";
	var laborableAnterior = ( labAnt=="true" ) ? "checked='checked'" : "";

	document.getElementById('textIdRegla1').value = idRegla;//nuevo

	document.getElementById('horaParam1').value = hora;
	document.getElementById('minParam1').value = min;
	// faltan dia y mes
	document.getElementById('diaMes1').value = diaMes;
	document.getElementById('idMes1').value = mes;
	document.getElementById('diaSemana1').value = diaSemana;




	document.getElementById('margenPosterior1').value = mPos;
	document.getElementById('margenAnterior1').value = mAnt;
	document.getElementById('labPosterior1').checked = laborablePosterior;
	document.getElementById('labAnterior1').checked = laborableAnterior;
	document.getElementById('exFestivo1').checked = excepcionar;


	document.getElementById('fDesdeParam1').value = fDesde;
	document.getElementById('fHastaParam1').value = fHasta;
	document.getElementById('textIdParam1').value = id;
	if (activo=='true')
	{
		document.getElementById('activaParam1').checked = activo;
	}
	else
		{
		document.getElementById('activaParam1').checked = "";
		}


	// llamada a ajax


			var data = new DataBean();
			data.setService('SIBBACServicePeriodicidades');
			data.setAction('getDiasSemana');
			var request = requestSIBBAC( data );
			request.success(function(json) {

			if (json.resultados==null)
			{
				alert(json.error);
			}
			else
			{
				var resultados = json.resultados.semanas;
				var item = null;
				var contador = 0;
				var nParametrizaciones = 0;
				$("#diaSemana1").empty();

				$("#diaSemana1").append("<option value='0'>Seleccione un d&iacute;a</option>");
				for ( var k in resultados ) {

					// En "k" tengo lo de la izquierda, por ejemplo: "Id_Regla_1".
					// En "item" tengo todo lo de la derecha.
					// En "item.parametrizacion.length" tenemos el numero de "hijos".
					item = resultados[ k ];
					if (diaSemana == item.id)
						{
						$("#diaSemana1").append("<option value='" + item.id +  "' selected>" + item.nombre +  "</option>");
						}
					else
						{
						$("#diaSemana1").append("<option value='" + item.id +  "'>" + item.nombre +  "</option>");
						}

				}
			}
		});

		var data = new DataBean();
		data.setService('SIBBACServicePeriodicidades');
		data.setAction('getMeses');
		var request = requestSIBBAC( data );
		request.success( function(json) {
			if (json.resultados==null)
			{
				alert(json.error);
			}
			else
			{
				var resultados = json.resultados.meses;
				var item = null;

				$("#idMes1").empty();
				$("#idMes1").append("<option value='0'>Seleccione mes</option>");
				for ( var k in resultados ) {

					item = resultados[ k ];

					if (mes==item.id)
						{
						$("#idMes1").append("<option value='" + item.id +  "' selected>" + item.nombre +  "</option>");
						}
					else
						{
						$("#idMes1").append("<option value='" + item.id +  "'>" + item.nombre +  "</option>");
						}

				}
				//asigno el id del mes en el combo
			//	document.getElementById("idMes1").selected = mes;
			}

		});
}

//cargar en los inputs del edit regla modal los valores correspondientes
function cargoInputsReg(id){
	document.getElementById('textIdReg').value = id;
}

//funcion para cargar el id de la regla
function cargoIdRegla(id){
	document.getElementById('textIdRegla').value = id;
	var data = new DataBean();
			data.setService('SIBBACServicePeriodicidades');
			data.setAction('getDiasSemana');
			var request = requestSIBBAC( data );
			request.success(function(json) {

			if (json.resultados==null)
			{
				alert(json.error);
			}
			else
			{
				var resultados = json.resultados.semanas;
				var item = null;
				var contador = 0;
				var nParametrizaciones = 0;
				$("#diaSemana").empty();

				$("#diaSemana").append("<option value='0'>Seleccione un d&iacute;a</option>");
				for ( var k in resultados ) {

					// En "k" tengo lo de la izquierda, por ejemplo: "Id_Regla_1".
					// En "item" tengo todo lo de la derecha.
					// En "item.parametrizacion.length" tenemos el numero de "hijos".
					item = resultados[ k ];
					if (diaSemana == item.id)
						{
						$("#diaSemana").append("<option value='" + item.id +  "' selected>" + item.nombre +  "</option>");
						}
					else
						{
						$("#diaSemana").append("<option value='" + item.id +  "'>" + item.nombre +  "</option>");
						}

				}
			}



		});



		var data = new DataBean();
			data.setService('SIBBACServicePeriodicidades');
			data.setAction('getMeses');
			var request = requestSIBBAC( data );
			request.success(function(json) {
			if (json.resultados==null)
			{
				alert(json.error);
			}
			else
			{

				var resultados = json.resultados.meses;
				var item = null;

				$("#idMes").empty();
				$("#idMes").append("<option value='0'>Seleccione mes</option>");
				for ( var k in resultados ) {

					item = resultados[ k ];

					if (mes==item.id)
						{
						$("#idMes").append("<option value='" + item.id +  "' selected>" + item.nombre +  "</option>");
						}
					else
						{
						$("#idMes").append("<option value='" + item.id +  "'>" + item.nombre +  "</option>");
						}

				}
				//asigno el id del mes en el combo
			//	document.getElementById("idMes1").selected = mes;
			}

		});
}


//funcion para borrar parametrizaciones
function borrarParam(id)
{
	var filtro = "{\"id\" : \""+ id + "\" }";

	if (confirm("¿Desea borrar realmente la parametrización " + id + "?") == true) {
		var data = new DataBean();
		data.setService('SIBBACServicePeriodicidades');
		data.setAction('deleteParametrizacion');
		data.setFilters(filtro);
		var request = requestSIBBAC( data );
		request.success(function(json) {
				if (json.resultados==null)
				{
					alert(json.error);
				}
				else
				{

					$("section").load("views/periodicidad/periodicidad.html");
				}
			});
	} else {
	   // alert("You pressed Cancel!");
	}

}

//Crear parametrizacion
function crearParametrizacion()
{
	var idRegla = document.getElementById('textIdRegla').value;
	var fechaDesde = document.getElementById('fDesdeParam').value;
	var fechaHasta = document.getElementById('fHastaParam').value;
	var labAnt= document.getElementById('labAnterior').checked;
	var labPost= document.getElementById('labPosterior').checked;
	var exFestivo= document.getElementById('exFestivo').checked;
	var margenAnt = document.getElementById('margenAnterior').value;
	var margenPost= document.getElementById('margenPosterior').value;
	var hora= document.getElementById('horaParam').value;
	var minuto= document.getElementById('minParam').value;
	var activa = document.getElementById('activaParam').checked;
	var diaMes = document.getElementById('diaMes').value;
	var idMes = document.getElementById('idMes').value;
	var diaSemana = document.getElementById('diaSemana').value;


	 var listaIdMes = document.getElementById("idMes");
	 var indiceSeleccionado = listaIdMes.selectedIndex;
	 var opcionSeleccionada = listaIdMes.options[indiceSeleccionado];
	 var textoSeleccionadoidMes = opcionSeleccionada.text;
	 var valorSeleccionadoidMes = opcionSeleccionada.value;

	 if (fechaHasta=="")
		 {
		 fechaHasta="31/12/9999";
		 }

	 if(hora=="")
		 {
		 hora="12";
		 }

	 if (minuto=="")
		 {
		 minuto="0";
		 }

	if (margenAnt =="")
	{
		margenAnt="0";
	}
	if (margenPost=="")
	{
		margenPost ="30";
	}

	 var filtro = "{\"excepcionarFestivos\" : \""+ exFestivo + "\"," +
	  "\"laborableAnterior\" : \""+ labAnt + "\"," +
	  "\"laborablePosterior\" : \""+ labPost + "\"," +
	  "\"hora\" : \""+ hora + "\"," +
	  "\"minuto\" : \""+ minuto + "\"," +
	  "\"margenAnterior\" : \""+ margenAnt + "\"," +
	  "\"margenPosterior\" : \""+ margenPost + "\"," +
	  "\"fechaDesde\" : \""+ transformaFechaInv(fechaDesde) + "\"," +
	  "\"fechaHasta\" : \""+ transformaFechaInv(fechaHasta) + "\"," +
	  "\"activo\" : \""+ activa + "\"," +
	  "\"idReglaUsuario\" : \""+ idRegla + "\" ,";



	if (diaSemana!="0")
		{
		 filtro = filtro +  "\"diaSemana\" : \""+ diaSemana + "\",";
		}
	if (valorSeleccionadoidMes!="0")
		{
			var filtro = filtro + "\"diaMes\" : \""+ diaMes + "\"," +
			"\"idMes\" : \""+ valorSeleccionadoidMes + "\",";
	  }
	  else {
		  if (diaSemana=="0"  && valorSeleccionadoidMes=="0"  && (diaMes=="0" || diaMes==""))
		{
		alert("Debe seleccionar un dia de la semana o un mes y su día");
		return false;
		}
	  }
	filtro = filtro.substring(0,filtro.length-1);
	filtro = filtro +"}";

	var data = new DataBean();
	data.setService('SIBBACServicePeriodicidades');
	data.setAction('createParametrizacion');
	data.setFilters(filtro);
	var request = requestSIBBAC( data );
	request.success(function(json) {
			if (json.resultados==null)
			{
				alert(json.error);
			}
			else
			{
				//alert(json.request.filters.diaMes);
				$("section").load("views/periodicidad/periodicidad.html");
			}
		});
}

//Evento para dar de alta la regla
$('#btnAltaRegla').click(function(){
	var codigo = document.getElementById('textCodigo').value;
	var descripcion =  document.getElementById('textDesc').value;
	var fechaInicio =  document.getElementById('fechaDesde').value;
	var fechaFin = document.getElementById('fechaHasta').value;
	var activada = document.getElementById('chkActiv').checked;

	if (codigo=="" || descripcion =="" || fechaInicio=="")
		{
			alert("Alguno de los campos es vacío. Debe rellenarlo para poder dar de alta");
			return false;
	}
	if (!validarFecha('fechaDesde'))
		{
		alert("La fecha introducida no tiene el formato correcto: dd/mm/yyyy");
		return false;
		}
	else {
			altaRegla(codigo, descripcion, fechaInicio, fechaFin, activada)
		}
})


//Evento para dar de alta la regla
$('#editarRegla').click(function(){
	var codigo = document.getElementById('idReglaFormulario').value;
	var descripcion =  document.getElementById('descReglaFormulario').value;
	var fechaInicio =  document.getElementById('fIniReglaFormulario').value;
	var fechaFin = document.getElementById('fFinReglaFormulario').value;
	var id = document.getElementById('textIdReg').value;
	var activada = document.getElementById('activaReglaFormulario').checked;


	if (!validarFecha('fIniReglaFormulario') || !validarFecha('fFinReglaFormulario'))
	{
		alert("La fecha introducida no tiene el formato correcto: dd/mm/yyyy");
		return false;
	}
	else
	{
		var filtro = "{\"id\" : \""+ id + "\"," +
		"\"idReglaUsuario\" : \""+ codigo + "\"," +
		"\"descripcion\" : \""+ descripcion + "\"," +
		"\"fechaDesde\" : \""+ transformaFechaInv(fechaInicio) + "\"," +
		"\"fechaHasta\" : \""+ transformaFechaInv(fechaFin) + "\"," +
		"\"activo\" : \""+ activada  + "\" }";

		var data = new DataBean();
		data.setService('SIBBACServicePeriodicidades');
		data.setAction('updateRegla');
		data.setFilters(filtro);
		var request = requestSIBBAC( data );
		request.success(function(json) {
			if (json.resultados==null)
			{
				alert(json.error);
			}
			else
			{
				$("section").load("views/periodicidad/periodicidad.html");

			}
		});

	}
});



$('#modificacionParam').click(function(){


	var idRegla = document.getElementById('textIdRegla1').value;
	var id = document.getElementById('textIdParam1').value;
	var hora = document.getElementById('horaParam1').value;
	var min= document.getElementById('minParam1').value;

	var dia = document.getElementById('diaMes1').value;

	var mes = document.getElementById('idMes1').value;

	var activo = document.getElementById('activaParam1').checked;

	var diaSemana = document.getElementById('diaSemana1').value;

	var mPos= document.getElementById('margenPosterior1').value;
	var mAnt=document.getElementById('margenAnterior1').value;
	var laborablePosterior=document.getElementById('labPosterior1').checked;
	var laborableAnterior=document.getElementById('labAnterior1').checked;


	var excepcionar =document.getElementById('exFestivo1').checked;


	var fDesde= transformaFechaInv(document.getElementById('fDesdeParam1').value);
	var fHasta = transformaFechaInv(document.getElementById('fHastaParam1').value);

	 var listaIdMes = document.getElementById("idMes1");
	 var indiceSeleccionado = listaIdMes.selectedIndex;
	 var opcionSeleccionada = listaIdMes.options[indiceSeleccionado];
	 var textoSeleccionadoidMes = opcionSeleccionada.text;
	 var valorSeleccionadoidMes = opcionSeleccionada.value;



	var filtro = "{\"id\" : \""+ id + "\"," +
	  "\"excepcionarFestivos\" : \""+ excepcionar + "\"," +
	  "\"laborableAnterior\" : \""+ laborableAnterior + "\"," +
	  "\"laborablePosterior\" : \""+ laborablePosterior + "\"," +
	  "\"hora\" : \""+ hora + "\"," +
	  "\"minuto\" : \""+ min + "\"," +
	  "\"margenAnterior\" : \""+ mAnt + "\"," +
	  "\"margenPosterior\" : \""+ mPos + "\"," +
	  "\"fechaDesde\" : \""+ fDesde + "\"," +
	  "\"fechaHasta\" : \""+ fHasta + "\"," +
	  "\"activo\" : \""+ activo + "\"," +
	  "\"idReglaUsuario\" : \""+ idRegla  +"\" ,";

	if (diaSemana!="0")
		{
		 filtro = filtro +  "\"diaSemana\" : \""+ diaSemana + "\",";
		}
	if (valorSeleccionadoidMes!="0")
		{
			var filtro = filtro + "\"diaMes\" : \""+ dia + "\"," +
			"\"idMes\" : \""+ valorSeleccionadoidMes + "\",";
	  }
	  else {
		  if (diaSemana=="0"  && valorSeleccionadoidMes=="0"  && (diaMes=="0" || diaMes==""))
		{
		alert("Debe seleccionar un dia de la semana o un mes y su día");
		return false;
		}
	  }
	filtro = filtro.substring(0,filtro.length-1);
	filtro = filtro +"}";
		var jsonData = "{" + "\"service\" : \"SIBBACServicePeriodicidades\", " +
		"\"action\"  : \"updateParametrizacion\", "+
		"\"filters\"  : "+ filtro + "}";
	var data = new DataBean();
	data.setService('SIBBACServicePeriodicidades');
	data.setAction('updateParametrizacion');
	data.setFilters(filtro);
	var request = requestSIBBAC( data );
	request.success(function(json) {
		if (json.resultados==null)
		{
			alert(json.error);
		}
		else
		{
			$("section").load("views/periodicidad/periodicidad.html");
		}
	});
});



//Funcion para el alta de la regla
function altaRegla(codigo, descripcion, fechaInicio, fechaFin, activada)
{
	if (fechaFin=="")
		{
		fechaFin="31/12/9999";
		}
	var filtro = "{\"idReglaUsuario\" : \""+ codigo + "\"," +
				  "\"descripcion\" : \""+ descripcion + "\"," +
				  "\"fechaDesde\" : \""+ transformaFechaInv(fechaInicio) + "\"," +
				  "\"fechaHasta\" : \""+ transformaFechaInv(fechaFin) + "\"," +
				  "\"activo\" : \""+ activada + "\" }";

	var data = new DataBean();
	data.setService('SIBBACServicePeriodicidades');
	data.setAction('createRegla');
	data.setFilters(filtro);
	var request = requestSIBBAC( data );
	request.success(function(json) {
		if (json.resultados==null)
			{
			alert(json.error);
			}
		else
			{
			$("section").load("views/periodicidad/periodicidad.html");
			}
	});
}


function initialize() {

	var fechas= ["fechaDesde","fechaHasta","fechaDesde1","fechaHasta1","fDesdeParam","fHastaParam","fIniReglaFormulario","fIniReglaFormulario","fFinReglaFormulario","fDesdeParam1","fHastaParam1"];
	loadpicker(fechas);
	verificarFechas([["fechaDesde","fechaHasta"],["fechaDesde1","fechaHasta1"],["fDesdeParam","fHastaParam"],["fIniReglaFormulario","fIniReglaFormulario"],["fDesdeParam1","fHastaParam1"]]);
}


function render() {

}







