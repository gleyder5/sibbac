var oTable = undefined;
function loadDataTable() {
    oTable = $("#tablaPrueba").dataTable({
        "dom": 'T<"clear">lfrtip',
        "language": {
            "url": "i18n/Spanish.json"
        },
        "tableTools": {
            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
        },
        "scrollY": "480px",
        "scrollCollapse": true,
        "order": []
    });
}
$(document).ready(function () {

    if(oTable === undefined){
        loadDataTable();
    }

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({
            marginTop: '0px'
        }, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({
            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
        }, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
    });

    $('.collapser').click(function () {
        $(this).parents('.title_section').next().slideToggle();
        //$(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

        return false;
    });
    $('.collapser_search').click(function () {
        $(this).parents('.title_section').next().slideToggle();
        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Mostrar Alta de Facturación', 'Ocultar Alta de Facturación');
        return false;
    });

    /*BUSCADOR*/
    $('.btn.buscador').click(function () {
        //event.preventDefault();
        $('.resultados').show();
        //$('.collapser_search').parents('.title_section').next().slideToggle();
        //$('.collapser_search').toggleClass('active');
        //$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');

        return false;
    });
    /*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            //	$('.resultados').hide();
        });
    }


    $('#btnAltaNumerador').click(function () {
        //alert("metodo");
        //crearNumerador();
    });

    cargarTabla();



});


function cargarIdiomas()
{
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/sibbac20back/rest/service",
        data: "{\"service\" : \"SIBBACServiceIdioma\", \"action\"  : \"getListaIdiomas\"}",
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        async: true,
        success: function (json) {
            var resultados = json.resultados;
            var item = null;
            $('#txtIdioma').empty();
            for (var k in resultados) {
                item = resultados[ k ];
                // Rellenamos el combo de idiomas para modificar
                $('#txtIdioma').append(
                        "<option value='" + item.id + "'>" + item.descripcion + "</option>"

                        );

            }
        },
        error: function (c) {
            console.log("ERROR: " + c);
        }
    });


}

function cargarAliasF(id)
{

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/sibbac20back/rest/service",
        data: "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacionAlias\",\"filters\" : {\"idAlias\" : \"" + id + "\"}}",
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        async: true,
        success: function (json) {
            var resultados = json.resultados.listaAlias;
            var item = null;
            $('#txtAliasF').empty();
            for (var k in resultados) {
                item = resultados[ k ];
                alias = item.nombre;
                // Rellenamos el combo de idiomas para modificar
                $('#txtAliasF').append(
                        "<option value='" + item.id + "'>" + alias + "</option>"

                        );

            }
        },
        error: function (c) {
            console.log("ERROR: " + c);
        }
    });


}


function cargarTabla()
{
    var params = new DataBean();
    var request;

    params.setService('SIBBACServiceAlias');
    params.setAction('getListaAlias');

    request = requestSIBBAC(params);

    request.success(function (json) {
        var resultados = json.resultados.listaAlias;
        var item = null;
        var now = Date.now();

        oTable.fnClearTable();
        for (var k in resultados) {
            // En "k" tengo lo de la izquierda, por ejemplo: "Id_Regla_1".
            // En "item" tengo todo lo de la derecha.
            // En "item.parametrizacion.length" tenemos el numero de "hijos".
            item = resultados[ k ];


            alias = item.nombre;
            aliasF = item.nombreAliasFacturacion;
            oTable.fnAddData([
                alias,
                aliasF,
                item.descipcionIdioma,
                "<a class=\"btn\" onclick = \"document.getElementById('formularioAlias').style.display='block';document.getElementById('fade').style.display='block';cargoInputs('" + item.id + "','" + item.idAliasFacturacion + "','" + item.idIdioma + "','" + alias + "','" + aliasF + "'); \"><img src='img/editp.png' title='Editar'/></a>"
            ], false);


        }
        oTable.fnDraw();
        console.log("Se ha tardado en pintar la tabla :" + ((Date.now() - now) / 1000))
    });

}

function cargoInputs(id, ida, idi, alias, aliasf)
{
    //cargar alias e idiomas
    cargarAliasF(id);
    cargarIdiomas();
    //$('#txtAliasF').
    //$('#txtIdioma').

    //var listaId = document.getElementById("txtAliasF");
    //listaId.selectedIndex = ida;
    $('#nombreAlias').empty();
    $('#nombreAlias').append(alias);
    document.getElementById('aliasO').value = id;
    document.getElementById('txtAliasF').value = ida;
    document.getElementById('txtIdioma').value = idi;
    //document.getElementById('textModifDescripcion').value = descripcion;
}

//cambio datos
$('#btnEditar').click(function () {

    var idA = document.getElementById('aliasO').value;
    var idF = document.getElementById('txtAliasF').value;
    var idI = document.getElementById('txtIdioma').value;

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/sibbac20back/rest/service",
        data: "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"modificarAlias\",\"filters\" : {\"idAlias\" : \"" + idA + "\",\"idAliasFacturacion\" : \"" + idF + "\",\"idIdioma\" : \"" + idI + "\"}}",
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        async: true,
        success: function (json) {
            $("section").load("views/alias/alias.html");
        }
    });




})
