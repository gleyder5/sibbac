var idOcultos = [];
var availableTags = [];
$(document).ready(function() {

	/* Eva:Función para cambiar texto */
	jQuery.fn.extend({
		toggleText : function(a, b) {
			var that = this;
			if (that.text() != a && that.text() != b) {
				that.text(a);
			} else if (that.text() == a) {
				that.text(b);
			} else if (that.text() == b) {
				that.text(a);
			}
			return this;
		}
	});
	/* FIN Función para cambiar texto */

	$('a[href="#release-history"]').toggle(function() {
		$('#release-wrapper').animate({
			marginTop : '0px'
		}, 600, 'linear');
	}, function() {
		$('#release-wrapper').animate({
			marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function() {
		_gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
	});

	$('.collapser').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		// $(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar', 'Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		// $(this).toggleText( "Mostrar Alta de Grupo Identificador", "Ocultar
		// Alta de Grupo Identificador" );
		if ($(this).text() == "Ocultar Alta de Grupo Identificador") {
			$(this).text("Mostrar Alta de Grupo Identificador");
		} else {
			$(this).text("Ocultar Alta de Grupo Identificador");
		}
		return false;
	});

	/*
	 * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
	 * TABLA
	 */
	if ($('.contenedorTabla').length >= 1) {
		var anchoBotonera;
		$('.contenedorTabla').each(function(i) {
			anchoBotonera = $(this).find('table').outerWidth();
			$(this).find('.botonera').css('width', anchoBotonera + 'px');
			$(this).find('.resumen').css('width', anchoBotonera + 'px');
			$('.resultados').hide();
		});
	}

	cargarAlias();

});

// $('#modificarDirFiscal').click(function(){
// alert("entro");
// });

function modificarDireccion() {

	calle = document.getElementById('textCalle').value;
	numero = document.getElementById('textNum').value;
	bloque = document.getElementById('textBloque').value;
	poblacion = document.getElementById('textPoblacion').value;
	codPostal = document.getElementById('textCP').value;
	provincia = document.getElementById('textProvincia').value;
	pais = document.getElementById('textPais').value;

	codigo = document.getElementById('textIdDirFiscal').value;

	var filtro = "{\"id\" : \"" + codigo + "\"," + "\"calle\" : \"" + calle + "\"," + "\"numero\" : \"" + numero + "\","
			+ "\"bloque\" : \"" + bloque + "\"," + "\"poblacion\" : \"" + poblacion + "\"," + "\"provincia\" : \"" + provincia + "\","
			+ "\"pais\" : \"" + pais + "\"," + "\"codigoPostal\" : \"" + codPostal + "\" }";

	var jsonData = "{" + "\"service\" : \"SIBBACServiceAliasDirecciones\", " + "\"action\"  : \"modificacionAliasDirecciones\", "
			+ "\"filters\"  : " + filtro + "}";

	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			alert(json.resultados.result);
		},
		error : function(c) {
			alert("ERROR: " + c);
		}

	});

}

function crearDireccion() {
	calle = document.getElementById('textCalle').value;
	numero = document.getElementById('textNum').value;
	bloque = document.getElementById('textBloque').value;
	poblacion = document.getElementById('textPoblacion').value;
	codPostal = document.getElementById('textCP').value;
	provincia = document.getElementById('textProvincia').value;
	pais = document.getElementById('textPais').value;

	codigo = document.getElementById('idAlias').value;

	var filtro = "{\"aliasId\" : \"" + codigo + "\"," + "\"calle\" : \"" + calle + "\"," + "\"numero\" : \"" + numero + "\","
			+ "\"bloque\" : \"" + bloque + "\"," + "\"poblacion\" : \"" + poblacion + "\"," + "\"provincia\" : \"" + provincia + "\","
			+ "\"pais\" : \"" + pais + "\"," + "\"codigoPostal\" : \"" + codPostal + "\" }";

	var jsonData = "{" + "\"service\" : \"SIBBACServiceAliasDirecciones\", " + "\"action\"  : \"createAliasDirecciones\", "
			+ "\"filters\"  : " + filtro + "}";

	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			$("section").load("views/direccionFiscal/direccionFiscal.html");
		},
		error : function(c) {
			alert("ERROR: " + c);
		}

	});

}

/* cargar tabla alias */
$('.btn.buscador').click(function() {

	var listaAlias = document.getElementById("textAlias").value;

	posicionAlias = availableTags.indexOf(listaAlias);
	if (posicionAlias == "-1") {
		alert("El alias introducido no existe");
		return false;
	} else {
		valorSeleccionadoAlias = idOcultos[posicionAlias];
		document.getElementById('idAlias').value = valorSeleccionadoAlias;

		cargarTabla(valorSeleccionadoAlias);
	}
	return false;

	// document.getElementById('textIdAlias').value = valorSeleccionado;

});

// function cargarAlias

function cargarAlias() {

	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAlias\"}",

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			var resultados = json.resultados.listaAlias;
			var item = null;

			for ( var k in resultados) {
				item = resultados[k];
				idOcultos.push(item.id);
				ponerTag = item.nombre.trim();
				availableTags.push(ponerTag);
			}
			// código de autocompletar

			$("#textAlias").autocomplete({
				source : availableTags
			});
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

// esta es la tabla que carga todos los datos
function cargarTabla(alias) {
	console.log("entro en el método");
	var filtro = "{\"aliasId\" : \"" + alias + "\" }";
	$
			.ajax({
				type : "POST",
				dataType : "json",
				url : "/sibbac20back/rest/service",
				data : "{\"service\" : \"SIBBACServiceAliasDirecciones\", \"action\"  : \"getListaAliasDirecciones\", \"filters\"  : "
						+ filtro + "}",

				beforeSend : function(x) {
					if (x && x.overrideMimeType) {
						x.overrideMimeType("application/json");
					}
					x.setRequestHeader("Accept", "application/json");
					x.setRequestHeader("Content-Type", "application/json");
					x.setRequestHeader("X-Requested-With", "HandMade");
					x.setRequestHeader("Access-Control-Allow-Origin", "*");
					x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
				},
				async : true,
				success : function(json) {
					$('#dFisc').css("display", "block");
					var datos = json.resultados[alias];
					$("#botoneraDirFiscal").empty();
					if (datos.calle != "") {
						$('#botoneraDirFiscal')
								.append(
										'<input class="btn buscador mybutton" type="button"  id="modificarDirFiscal" value="Modificar" onclick="modificarDireccion()" />');
					} else {
						$('#botoneraDirFiscal')
								.append(
										'<input class="btn buscador mybutton" type="button"  id="crearDirFiscal" value="Dar de alta" onclick="crearDireccion()" />');
					}
					$('#botoneraDirFiscal').append('<input class="mybutton" type="reset"  id="limpiar" value="Borrar campos" />');

					document.getElementById('textCalle').value = datos.calle;
					document.getElementById('textNum').value = datos.numero;
					document.getElementById('textBloque').value = datos.bloque;
					document.getElementById('textPoblacion').value = datos.poblacion;
					document.getElementById('textCP').value = datos.codigoPostal;
					document.getElementById('textProvincia').value = datos.provincia;
					document.getElementById('textPais').value = datos.pais;

					document.getElementById('textIdDirFiscal').value = datos.id;

				},
				error : function(c) {
					console.log("ERROR: " + c);
				}
			});
}
