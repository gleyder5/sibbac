
var oTable = undefined;
function consultar() {
	var data = new DataBean();
	data.setService('SIBBACServiceTipoMovimiento');
	data.setAction('getTipoMovimiento');
	var request = requestSIBBAC( data );
	request.success( function (json) {
    var datos = undefined;
    if (json === undefined || json.resultados === undefined || json.resultados.result_tipo_movimiento === undefined) {
    	datos ={};
    } else {
        datos = json.resultados.result_tipo_movimiento;
    }
            var tbl = $("#tblTipoMovimiento > tbody");
            var difClass = 'centrado';
			oTable.fnClearTable();
            $.each(datos, function (key, val) {
            	var codigo = '';
            	var titulo  = '';
            	var efectivo = '';
            	if( datos[key].tipoOperacion !== null && datos[key].tipoOperacion != undefined ){
            		codigo = datos[key].tipoOperacion;
            	}else if( datos[key].codigo !== null && datos[key].codigo != undefined ){
            		codigo = datos[key].codigo;
            	}
            	if( datos[key].afectaTituloContado !== null && datos[key].afectaTituloContado != undefined  ){
            		titulo = datos[key].afectaTituloContado;
            	}else if( datos[key].afectacionTitulos !== null && datos[key].afectacionTitulos != undefined  ){
            		titulo = datos[key].afectacionTitulos;
            	}
            	if( datos[key].afectaSaldoEfectivo !== null && datos[key].afectaSaldoEfectivo != undefined ){
            		efectivo = datos[key].afectaSaldoEfectivo;
            	}else if( datos[key].afectacionSaldo !== null && datos[key].afectacionSaldo != undefined  ){
            		efectivo = datos[key].afectacionSaldo;
            	}
			    oTable.fnAddData([
                                   codigo,
                                   datos[key].descripcion,
                                   titulo,
                                   efectivo
             					]);
            });
    });
}

/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {
	consultar();

}
$(document).ready(function () {
	oTable = $("#tblTipoMovimiento").dataTable({
		"dom": 'T<"clear">lfrtip',
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "language": {
             "url": "i18n/Spanish.json"
         }
	});
    initialize();

    /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }

    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/



});
function render() {
    //
}






