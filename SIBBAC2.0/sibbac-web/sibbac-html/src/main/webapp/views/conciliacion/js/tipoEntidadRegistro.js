//
var oTable = undefined;
function consultar(params) {

    // for(key in params.filters){
    // alert("key: "+key + " value: "+params.filters[key]);
    // }
    var data = new DataBean();
    data.setService('SIBBACServiceEntidadRegistro');
    data.setAction('getAllTiposEntidadRegistro');
    if (params !== '') {
        data.setFilters(params);
    }
    var request = requestSIBBAC(data);
    request
            .success(function (json) {
                var datos = undefined;
                if (json === undefined
                        || json.resultados === undefined
                        || json.resultados.result_tipos_entidad_registro === undefined) {
                    datos = {};
                } else {
                    datos = json.resultados.result_tipos_entidad_registro;
                }
                var tbl = $("#tblCuentaLiquidacion > tbody");
                var difClass = 'centrado';
                oTable.fnClearTable();
                var frecuencia = 0;
                var entidadRegistro = "";
                $
                        .each(
                                datos,
                                function (key, val) {
                                    entidadRegistro = '<span data="'
                                            + datos[key].id + '">'
                                            + datos[key].nombre + '</span>';
                                    frecuencia = val.frecuenciaEnvioExtracto;
                                    oTable
                                            .fnAddData([
                                                "<a class=\"btn\" data=\""
                                                        + datos[key].id
                                                        + "\" onclick=\"modificarCuenta('"
                                                        + datos[key].id
                                                        + "');\"><img src='img/editp.png'  title='editar' /></a>",
                                                entidadRegistro,
                                                "<a class=\"btn\" data=\""
                                                        + datos[key].id
                                                        + "\" onclick=\"borrarTipoEntidad('"
                                                        + datos[key].id
                                                        + "');\"><img src='img/del.png'  title='borrar' /></a>"]);
                                });

            });
}

// obtiene el id de entidad de registro seleccionada
function getIdEntidadRegistro() {
    var er = $("#ER").val();
    if (er === undefined) {
        return "";
    }
    er = er.toLowerCase();
    if (er.trim().length > 0 && er.indexOf("tipo") !== -1) {
        er = er.substring(0, er.indexOf("tipo") - 1);
    }
    return er;
}
function obtenerIdsSeleccionados(lista) {
    var selected = "";
    var i = 0;
    $(lista + ' option').each(function () {
        if (i > 0) {
            selected += ", ";
        }
        selected += $(this).val();
        i++
    });
    return selected;
}
// ///////////////////////////////////////////////
// ////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {

    obtenerDatos();
    consultar('');
}
/** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
function valueExistInList(lista, valor) {
    var inList = false;

    $(lista + ' option').each(function (index) {

        if (this.value == valor) {
            inList = true;
            return inList;
        }
    });

    return inList;
}

$(document).ready(function () {
    oTable = $("#tblCuentaLiquidacion").dataTable({
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "i18n/Spanish.json"
        },
        "aoColumns": [{
                "sClass": "centrado",
                "bSortable": "false"
            }, {
                "sClass": "centrado"
            }, {
                "sClass": "centrado",
                "bSortable": "false"
            }],
        "scrollY": "480px",
        "scrollCollapse": true,
        "sort": [1]
    });
    initialize();

    /*
     * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
     * TABLA
     */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }

    /* Eva:Función para cambiar texto */
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /* FIN Función para cambiar texto */

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({
            marginTop: '0px'
        }, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({
            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
        }, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
    });

    $(function () {
        prepareCollapsion();
    });
    $('#cargarTabla').submit(function (event) {
        event.preventDefault();
        obtenerDatos();
        collapseSearchForm();
        seguimientoBusqueda();
        return false;
    });

    $('#limpiar').click(function (event) {
        event.preventDefault();
        $('input[type=text]').val('');
        $('select').val('0');
    });

    // ALTA DE ENTIDAD REGISTRO
    $('#cargarPopupParam').submit(function (event) {
        event.preventDefault();
        var msgValid = formIsValid();
        if (msgValid !== "") {
            alert(msgValid);
            return false;
        }
        $('#formularioalta').css('display', 'none');
        $('fade').css('display', 'none');
        var id = $('#idTipoEr').val();
        var nombre = $("#alta_nombre").val();

        // do validar
        var filtro = {
            "idTipoEr": id,
            "nombre": nombre,
        };
        var data = new DataBean();
        data.setService('SIBBACServiceEntidadRegistro');
        if ($("#altaparam").attr('data') === 'alta') {
            data.setAction('saveTipoEntidadRegistro');
        } else if ($("#altaparam").attr('data') === 'modificar') {
            data.setAction('updateTipoEntidadRegistro');
        }
        data.setFilters(JSON.stringify(filtro));
        var request = requestSIBBAC(data);
        request.success(function (json) {
            // alert(json.request.filters.diaMes);
            alert("Tipo entidad registro añadido correctamente.");
            $("#idTipoEr").val("");
            $("section").load("views/conciliacion/tipoEntidadRegistro.html");

        });
    });

    $('#altapopup').click(function (event) {
        event.preventDefault();
        $('#formularioalta').css('display', 'block');
        $('fade').css('display', 'block');
        $('div.piePagina input.mybutton').attr('data', 'alta');
        $('p.titleAlta').text('Alta Tipo Entidad Registro');
    });

});
function render() {
    //
}
function formIsValid() {
    var msgError = "";

    return msgError;
}
function seguimientoBusqueda() {
    $('.mensajeBusqueda').empty();
    var cadenaFiltros = "";
    if ($('#id').val() !== "" && $('#id').val() !== undefined
            && $('#id').val() !== "0") {
        cadenaFiltros += " Id: " + $('#id').val();
    }

    if ($('#nombre').val() !== "" && $('#nombre').val() !== undefined) {
        cadenaFiltros += " Nombre: " + $('#nombre').val();
    }
    $('.mensajeBusqueda').append(cadenaFiltros);

}

function obtenerDatos() {
    var params = {
        /* "tipoer.id" : $("#TER").val(), */
        "id": $("#id").val(),
        "nombre": $("#nombre").val()
    };
    $("#tblCuentaLiquidacion > tbody").html("");
    consultar(JSON.stringify(params));
}

function modificarCuenta(id) {
    cargarInput(id);
    $('#formularioalta').css('display', 'block');
    $('fade').css('display', 'block');
    $('div#formularioalta input.mybutton').attr('data', 'modificar');
    $('p.titleAlta').text('Modificar Tipo Entidad Registro');
}

function selectOption(queLista, texto) {
    $(queLista + " option:selected").attr('selected', 'false');
    var external = "";
    var inList = "";
    $(queLista).find("option").each(function (key, val) {
        inList = $(val).text().trim().toLowerCase();
        external = texto.trim().toLowerCase();

        if (inList === external) {
            $(val).attr("selected", "selected");
        }
    });
}
function cargarInput(id) {
    var fila = $('a[data="' + id + '"]').parent().parent().children();
    /** ***ID TIPO ER ***** */
    $("#idTipoEr").val(id);
    /** ******ENTIDAD REGISTRO ********* */
    var erId = $(fila[1]).find("span").attr("data");
    var erName = $(fila[1]).find("span").text();
    $('#alta_nombre').val(erName);
}

function borrarTipoEntidad(id) {
    var filtro = "{\"idTipoEr\" : \"" + id + "\" }";

    if (confirm("¿Desea borrar realmente el tipo entidad " + id + "?") == true) {
        var data = new DataBean();
        data.setService('SIBBACServiceEntidadRegistro');
        data.setAction('deleteTipoEntidadRegistro');
        data.setFilters(filtro);
        var request = requestSIBBAC(data);
        request.success(function (json) {
            if (json.resultados == null) {
                alert(json.error);
            } else {
                $("section")
                        .load("views/conciliacion/tipoEntidadRegistro.html");
            }

        });
    } else {
        // alert("You pressed Cancel!");
    }

}

function cerrar() {
    document.getElementById('formularioalta').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    $('#alta_id').val('');
    $('#alta_nombre').val('');
}
