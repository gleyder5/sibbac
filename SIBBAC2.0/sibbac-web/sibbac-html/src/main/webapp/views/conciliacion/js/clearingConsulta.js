var request = "";
var oTableD = undefined;
var oTableI = undefined;

function cargarFechaActualizacion() {

    var params = new DataBean();
    params.setService('SIBBACServiceConciliacionEfectivo');
    params.setAction('getFechaUltimoFicheroSubido');
    var request = requestSIBBAC(params);
    request.success(function (json) {
        var datos = undefined;

        if (json === undefined || json.resultados === undefined || json.resultados === null) {
            datos = {};
        } else {
            datos = json.resultados;
            //ultima fecha actualizacion
            $("#ultFecha").html(transformaFecha(datos.result_last_input_file_date));
        }

    });
}

function consultar(params) {
    inicializarLoading();
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(params),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datosD = undefined;
            var datosI = undefined;
            if (json === undefined ||
                    json.resultados === undefined ||
                    json.resultados === null ||
                    json.resultados.derecha === undefined ||
                    json.resultados.izquierda === undefined) {
                datosD = {};
                datosI = {};
            } else {
                request = json;
            }
            //tabla izquierda
            var tblIzquierda = $("#tblIzquierda > tbody");
            //$(tblIzquierda).html("");
            oTableI.fnClearTable();
            datosI = json.resultados.izquierda;

            $.each(datosI, function (key, val) {

                if ((datosI[key].efectivo !== null && datosI[key].efectivo !== 0) ||
                        (datosI[key].corretaje !== null && datosI[key].corretaje > 0)) {
                    oTableI.fnAddData([
                        transformaFecha(datosI[key].fliquidacion),
                        datosI[key].cdcodigocuentaliq,
                        datosI[key].sistemaLiq,
                        a2digitos(datosI[key].efectivo),
                        a2digitos(datosI[key].corretaje),
                        a2digitos(datosI[key].efectivo + datosI[key].corretaje)
                    ]);
                }

            });

            //tabla derecha
            var tblDerecha = $("#tblDerecha > tbody");
            //$(tblDerecha).html("");
            oTableD.fnClearTable();
            datosD = json.resultados.derecha;

            $.each(datosD, function (key, val) {
                var movimientos = datosD[key].movimientos;
                for (var i = 0; i < movimientos.length; i++) {
                    var mov = movimientos[i];

                    //fila tabla derecha
                    oTableD.fnAddData([
                        transformaFecha(mov.settlementdate),
                        datosD[key].codigoCuenta,
                        '',
                        a2digitos(mov.importe)
                    ]);
                }

            });
            $.unblockUI();
        },
        error: function (err) {
            $.unblockUI();
        }

    });
}
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {

    var fechas = ['fliquidacionA', 'fliquidacionDe'];
    loadpicker(fechas);
    verificarFechas([['fliquidacionDe', 'fliquidacionA']]);
    $('#fliquidacionDe').val(getfechaHoy());
    cargaCombo();
}

$(document).ready(function () {
    oTableI = $("#tblIzquierda").dataTable({
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "i18n/Spanish.json"
        },
        /* Disable initial sort */
        "order": [],
        /*"bSort" : false, Deshabilita todos los sort*/
        "aoColumns": [
            {"width": "auto", sClass: "centrar", type: 'date-eu'},
            {"width": "auto", sClass: "centrar"},
            {"width": "auto", sClass: "centrar"},
            {"width": "auto", sClass: "monedaR", type: "formatted-num"},
            {"width": "auto", sClass: "monedaR", type: "formatted-num"},
            {"width": "auto", sClass: "monedaR", type: "formatted-num"}
        ],
        "scrollY": "480px",
        "scrollCollapse": true,
        "filter": false
    });
    oTableD = $("#tblDerecha").dataTable({
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "i18n/Spanish.json"
        },
        "aoColumns": [
            {"width": "auto", sClass: "centrar", type: 'date-eu'},
            {"width": "auto", sClass: "centrar"},
            {"width": "auto", sClass: "centrar"},
            {"width": "auto", sClass: "monedaR", type: "formatted-num"}
        ],
        "scrollY": "480px",
        "scrollCollapse": true,
        "filter": false
    });
    initialize();

    /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }

    /*Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({
            marginTop: '0px'
        }, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({
            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
        }, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
    });

    $('.collapser').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

        return false;
    });
    $('.collapser_search').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        if ($(this).text().indexOf('Ocultar') !== -1) {
            $(this).text("Mostrar opciones de búsqueda");
        } else {
            $(this).text("Ocultar opciones de búsqueda");
        }
        return false;
    });
    $('#cargarTabla').submit(function (event) {

        event.preventDefault();
        if (!validar()) {
            return false;
        }
        else {
            $("#tblIzquierda > tbody").empty();
            //$("#tblIzquierda").trigger("update");
            $("#tblDerecha > tbody").empty();
            //$("#tblDerecha").trigger("update");
            if ($('#fliquidacionDe').val() !== undefined && $('#fliquidacionDe').val() !== "" && $("#selectCuenta option:selected").val() != "0") {
                obtenerDatos();
                event.preventDefault();
                $('.collapser_search').parents('.title_section').next().slideToggle();
                $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
                $('.collapser_search').toggleClass('active');
                if ($('.collapser_search').text() == "Ocultar opciones de búsqueda") {
                    $('.collapser_search').text("Mostrar opciones de búsqueda");
                } else {
                    $('.collapser_search').text("Ocultar opciones de búsqueda");
                }
                seguimientoBusqueda();
            } else {
                alert("Se deben informar todos los campos obligatorios.");
            }
            return false;
        }
    });
    $('#limpiar').click(function (event) {
        event.preventDefault();
        $('input[type=text]').val('');
        $('select').val('0');
    });

});


function render() {
    //
}


//para ver el filtro al minimizar la búsqueda
function seguimientoBusqueda() {
    $('.mensajeBusqueda').empty();
    var cadenaFiltros = "";

    if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined) {
        cadenaFiltros += " Fecha Liquidacion Desde: " + $('#fliquidacionDe').val();
    }
    if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined) {
        cadenaFiltros += " Fecha Liquidacion Hasta: " + $('#fliquidacionA').val();
    }
    if ($('#selectCuenta').val() !== 0) {
        cadenaFiltros += " Cuenta: " + $('#selectCuenta option:selected').text();
    }
    $('.mensajeBusqueda').append(cadenaFiltros);
}

function obtenerDatos() {
    var params = {"service": "SIBBACServiceConciliacionEfectivo",
        "action": "getOperacionesEfectivo",
        "filters": {"cdcodigocuentaliq": $("#selectCuenta option:selected").text(),
            "fliquidacionDe": transformaFechaInv($("#fliquidacionDe").val()),
            "fliquidacionA": transformaFechaInv($("#fliquidacionA").val())}
    };
    $("#tblDerecha > tbody").html("");
    $("#tblIzquierda > tbody").html("");
    consultar(params);
//	    	var data = new DataBean();
//	    	data.setService('SIBBACServiceConciliacionEfectivo');
//	    	data.setAction('getOperacionesEfectivo');
//	    	data.setFilters(JSON.stringify(params));
//	    	$("#tblDerecha > tbody").html("");
//	 	    $("#tblIzquierda > tbody").html("");
//	        consultar(data);

}
// llamada para cargar datos combo
function cargaCombo() {
    var llamada = {"service": "SIBBACServiceConciliacionClearing",
        "action": "getCuentasLiquidacion"
    };
    $("#selectCuenta").html("");
    $("#selectCuenta").append("<option value='0'>Seleccione  opcion</option>");
    cargaDatosCombo(llamada);
    cargarFechaActualizacionMovimientoAlias();
    cargarFechaActualizacion();
}



//carga datos combo cuenta
function cargaDatosCombo(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
                    json.resultados.cuentasLiquidacion === undefined) {
                datos = {};
            } else {
                datos = json.resultados.cuentasLiquidacion;
            }

            $.each(datos, function (key, val) {
                $("#selectCuenta").append('<option value=' + datos[key].id + '>' + datos[key].name + '</option>');
            });
        }
    });
}

function validar() {
    var campos = $('input[campo="obligatorio"]');
    if (campos.length === 0) {
        return true;
    }
    else {
        var valido = true;

        for (var i = 0; i < campos.length; i++) {
            if ($(campos[i]).val() === '') {
                $(campos[i]).addClass('error');
                valido = false;
            }
        }
        if (!valido) {
            alert('Complete todos los campos');
            return false;
        }
        else {
            $("input[campo='obligatorio']").removeClass('error');
            return true;
        }

    }
}





