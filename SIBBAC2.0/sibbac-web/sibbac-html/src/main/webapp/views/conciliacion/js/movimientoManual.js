var listaIsin = [];
var listaCamara = [];
var availableTags = [];
var availableTagsCamara = [];
var listaAlias = [];
var availableTagsAlias = [];

function insertado(params) {

    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(params),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos ={};
            var error = false;

            if (json !== undefined &&
        	    json.resultados !== undefined &&
        	    json.resultados !== null) {
                if (json.error !== null) {
       	        	alert(json.error);
           			error = true;
       	        }
       	        else
       	        {
       	        	if (json.resultados.result_movimiento_manual !== undefined && json.resultados.result_movimiento_manual !== null) {
       	        		 datos = json.resultados.result_movimiento_manual ;
       	        	 }
        	    }
            }
            else
    		{
    			if (json !== undefined && json.error !== null) {
    				alert(json.error);
           			error = true;
    		    }
    		}

            if (error !== true)
            {
            	alert(datos);
            }

          }
    });
}
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {
	var fechas = ['fliquidacion', 'fcontratacion'];
	loadpicker(fechas);
	verificarFechas([['fcontratacion', 'fliquidacion']]);
    cargaCombos();
    $("#efectivo").keypress(justNumbersD)
    $("#efectivo").blur(function() {
    	if ($(this).val() !=0){
    		$('.campoocultable[for="'+$(this).attr('id') +'"]').show();
    	}
    	else{
    		$('.campoocultable[for="'+$(this).attr('id') +'"]').hide();
    		$('.campoocultable[for="'+$(this).attr('id') +'"] select').val(0);
    	}
    });
    $("#titulos").keypress(justNumbersD);
    $("#titulos").blur(function() {
    	if ($(this).val() !=0){
    		$('.campoocultable[for="'+$(this).attr('id') +'"]').show();
    	}
    	else{
    		$('.campoocultable[for="'+$(this).attr('id') +'"]').hide();
    		$('.campoocultable[for="'+$(this).attr('id') +'"] select').val(0);
    	}
    });
}


$(document).ready(function () {
    initialize();

    /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }

    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({
            marginTop: '0px'
        }, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({
            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
        }, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
    });

    $('.collapser').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

        return false;
    });
    $('.collapser_search').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        if ( $(this).text()=="Ocultar opciones de búsqueda" ) {
			$(this).text( "Mostrar opciones de búsqueda" );
		} else if ( $(this).text()=="Mostrar opciones de búsqueda" ) {
			$(this).text( "Ocultar opciones de búsqueda" );
		}else{
			$(this).text( "Mostrar opciones de búsqueda" );
		}
        return false;
    });
    $('.btn.buscador').click(function (event) {
    	//compruebo que todos los campos vengan informados
    	var efectivoPattern = new RegExp(/^\d{1,15}\b.?\d{0,2}?$/);
    	var titulosPattern = new RegExp(/^\d{1,12}\b.?\d{0,6}?$/);
    	if((($("#titulos").val()!=null && $("#titulos").val()!="")||($("#efectivo").val()!=null && $("#efectivo").val()!="")) &&
    		 $("#selectSentido").val()!=null && $("#selectSentido").val()!=0 &&
    		 $("#cdisin").val()!=null && $("#cdisin").val()!=""  &&
    		 $("#alias").val()!=null && $("#alias").val()!="" &&
    		 ($("#selectCuenta").val()!=null  && $("#selectCuenta").val()!=0 ||$("#selectCuenta2").val()!=null  && $("#selectCuenta2").val()!=0)&&
    		 $("#fliquidacion").val()!=null && $("#fliquidacion").val()!=""
    			){
    	  if( $("#selectCuenta2").val() != 0 && ( $("#efectivo").val() == null || $("#efectivo").val() == "" )  ){
    		  alert("No se ha introducido ningun valor en efectivo pero se ha seleccionado un valor del combo 'Cuenta Liquidacion Efectivo'");
    	  }else if( $("#selectCuenta2").val() == 0  &&  $("#efectivo").val() != ""  ){
    		  alert("Se ha introducido un valor en efectivo pero no se ha seleccionado un valor del combo 'Cuenta Liquidacion Efectivo'");
    	  }else if( $("#titulos").val()===null && $("#titulos").val()==="" && !efectivoPattern.test( $("#efectivo").val() ) ){
    		  alert("El valor de efectivo tiene que ser numérico y tener un maximo de 15 enteros y 2 decimales");
    	  }else if( $("#efectivo").val()===null && $("#efectivo").val()==="" && !efectivoPattern.test( $("#titulos").val() ) ){
    		  alert("El valor de titulos tiene que ser numérico y tener un maximo de 12 enteros y 6 decimales");
    	  }else{
    		  obtenerDatos();
      		event.preventDefault();
      		$('.collapser_search').parents('.title_section').next().slideToggle();
      		$('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
      		$('.collapser_search').toggleClass('active');
      		if ( $('.collapser_search').text()=="Ocultar opciones de búsqueda" ) {
      			$('.collapser_search').text( "Mostrar opciones de búsqueda" );
      		} else if ( $('.collapser_search').text()=="Mostrar opciones de búsqueda" ) {
      			$('.collapser_search').text( "Ocultar opciones de búsqueda" );
      		}else{
      			$('.collapser_search').text( "Mostrar opciones de búsqueda" );
      		}
    	  }
    	}else{
    		alert("Se deben informar todos los campos.");
    	}

    	 return false;
    });
	$('#limpiar').click(function (event) {
		event.preventDefault();
		$('input[type=text]').val('');
		$('select').val('0');
	});


});
function render() {
    //
}


function obtenerDatos() {
    var params = {"service": "SIBBACServiceConciliacionMovimientoManual",
        "action": "insertMovimientoManual",
        "params": [{"titulos": $("#titulos").val()},{"cdisin": $("#cdisin").val()},
            {"sentido": $("#selectSentido option:selected").val()}, {"cdcamara": $("#cdcamara").val()},
            {"fliquidacion": transformaFechaInv($("#fliquidacion").val())},  {"fcontratacion": transformaFechaInv($("#fcontratacion").val())},
            {"efectivo": $("#efectivo").val()}, {"alias": $("#alias").val()},
          {"idCuentaLiq" : $("#selectCuenta option:selected").val()},{"desCuentaLiq" : $("#selectCuenta option:selected").val()},
          {"idCuentaLiqEfectivo" : $("#selectCuenta2 option:selected").val()},{"desCuentaLiqEfectivo" : $("#selectCuenta2 option:selected").val()}]
    };
    insertado(params);
}

//llamada para cargar datos combos
function cargaCombos(){
	 var llamada = {"service": "SIBBACServiceConciliacionClearing",
		     	   "action": "getCuentasLiquidacion"
	 			   };
	 var llamada2 = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getIsin"
			   };
	 var llamada3 = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getCamara"
			   };

	 //limpio los combos
	 $("#selectCuenta").html("");
	 $("#cdisin").html("");
	 $("#cdcamara").html("");
	 $("#alias").html("");
	 //cargo cuenta liquidacion
	 $("#selectCuenta").append("<option value='0'>Seleccione una opci&oacute;n</option>");
	 $("#selectCuenta2").append("<option value='0'>Seleccione una opci&oacute;n</option>");
	 cargaDatosCombo(llamada);
	 //cargo isin
	 cargaDatosCombo(llamada2);
	 //cargo camara
	 cargaDatosCombo(llamada3);
	 cargarAlias();
	 $('.campoocultable').hide();

}

function cargarAlias()
{

	var params = new DataBean();
	params.setService('SIBBACServiceTmct0ali');
	params.setAction('getAlias');
	var request = requestSIBBAC(params);
	request.success(function(json) {
		var datos = undefined;

		if (json === undefined || json.resultados === undefined
				|| json.resultados.result_alias === undefined) {
			datos = {};
		} else {
			datos = json.resultados.result_alias;
			var item = null;

			for ( var k in datos) {
				item = datos[k];
				listaAlias.push(item.alias);
				ponerTag = item.alias + " - " + item.descripcion;
				availableTagsAlias.push(ponerTag);

			}
			// código de autocompletar

			$("#alias").autocomplete({
				source : availableTagsAlias
			});
		}
		// fin de código de autocompletar
	});
	request.error( function(c) {
			console.log( "ERROR: " + c );
		});
	}

//carga datos combos
function cargaDatosCombo(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datosCL = undefined;
            var datosIsin = undefined;
            var datosCamara = undefined;

            if( json !== undefined || json.resultados !== undefined){
             if(json.resultados.cuentasLiquidacion === undefined ) {
               datosCL={};
             } else {
                datosCL = json.resultados.cuentasLiquidacion;
             }

            if (json.resultados.result_isin === undefined ) {
                datosIsin={};
             } else {
            	  datosIsin = json.resultados.result_isin;
             }

            if (json.resultados.result_camara === undefined ) {
                datosCamara={};
             } else {
            	 datosCamara = json.resultados.result_camara;
             }

            }else{

            	return false;
            }


            $.each(datosCL, function (key, val) {
            	$("#selectCuenta").append('<option value='+datosCL[key].name+'>'+datosCL[key].name+'</option>');
            	$("#selectCuenta2").append('<option value='+datosCL[key].name+'>'+datosCL[key].name+'</option>');
            });

            $.each(datosIsin, function (key, val) {

            		listaIsin.push(datosIsin[key].codigo);
    				ponerTag = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
    				availableTags.push(ponerTag);

    			//código de autocompletar

               $( "#cdisin" ).autocomplete({
                 source: availableTags
               });

            });

            $.each(datosCamara, function (key, val) {


				listaCamara.push(datosCamara[key].cdCodigo);
				ponerTagCamara = datosCamara[key].cdCodigo+" - "+datosCamara[key].nbDescripcion;
				availableTagsCamara.push(ponerTagCamara);

			//código de autocompletar

           $( "#cdcamara" ).autocomplete({
             source: availableTagsCamara
           });

            });
        }
    });
}
