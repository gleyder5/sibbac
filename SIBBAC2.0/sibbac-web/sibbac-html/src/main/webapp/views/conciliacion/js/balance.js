var listaIsin = [];
var availableTags = [];
var oTable = undefined;

$(document).ready(function() {
	oTable = $("#tblClearingTitulos").dataTable({
		"dom": 'T<"clear">lfrtip',
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "language": {
             "url": "i18n/Spanish.json"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "aoColumns" :[
                       {"sClass": "centrado"},
                       {"sClass": "centrado"},
                       {"sClass": "centrado"},
                       {"sClass": "monedaR", "type": 'formatted-num'}
                       ]
	});
	initializePicker();
	cargarFechaActualizacionMovimientoAlias();
	cargaCombo();
	getFechaPrevia();
	$('.collapser_search').click(collapserSearch);
	$('#cargarTabla').submit(btnBuscador);
});

function initializePicker() {
	loadpicker(['fliquidacionDe', 'fliquidacionA']);
}
function cargaCombo(){
	 var selectCuentaCall = {"service": "SIBBACServiceConciliacionClearing",
		     	   "action": "getCuentasLiquidacion"
	 			   };
	 var cdIsinCall = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getIsin"
			   };

	 $("#selectCuenta").html("");
	 $("#selectCuenta").append("<option value=\"\">Seleccione una opci&oacute;n</option>");
	 cargaDatosCombo(selectCuentaCall);

	 $("#cdisin").html("");
	 cargaDatosCombo(cdIsinCall);
}
//carga datos combo cuenta
function cargaDatosCombo(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
                    json.resultados.cuentasLiquidacion === undefined) {
                datos={};
//                return false;
            } else {
                datos = json.resultados.cuentasLiquidacion;
            }

            if (json.resultados.result_isin === undefined ) {
                datosIsin={};
             } else {
            	datosIsin = json.resultados.result_isin;
             }

            $.each(datos, function (key, val) {
            	$("#selectCuenta").append('<option value='+datos[key].id+'>'+datos[key].name+'</option>');
            });

            $.each(datosIsin, function (key, val) {
        		listaIsin.push(datosIsin[key].codigo);
				ponerTag = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
				availableTags.push(ponerTag);
            });
            $( "#cdisin" ).autocomplete({
				source: availableTags
			});
        }
    });
}
function collapserSearch(event){
	event.preventDefault();
	$(this).parents('.title_section').next().slideToggle();
	$(this).parents('.title_section').next().next('.button_holder').slideToggle();
	$(this).toggleClass('active');
	if ( $(this).text()=="Ocultar opciones de búsqueda" ) {
		$(this).text( "Mostrar opciones de búsqueda" );
	} else if ( $(this).text()=="Mostrar opciones de búsqueda" ) {
		$(this).text( "Ocultar opciones de búsqueda" );
	}else{
		$(this).text( "Mostrar opciones de búsqueda" );
	}
	return false;
}
function btnBuscador(event){

	event.preventDefault();
	if(!validarFecha("fliquidacionDe")){
		alert("La Fecha no tiene el formato correcto");
		return false;
	} else{
	$("#tblClearingTitulos > tbody").empty();
	    obtenerDatos();
		$('.collapser_search').parents('.title_section').next().slideToggle();
		$('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
		$('.collapser_search').toggleClass('active');
		if ( $('.collapser_search').text().indexOf('Ocultar') !==-1 ) {
			$('.collapser_search').text( "Mostrar opciones de búsqueda" );
		} else {
			$('.collapser_search').text( "Ocultar opciones de búsqueda" );
		}
		seguimientoBusqueda();
		return false;
	}
}
function obtenerDatos(){
	 var params = {"service": "SIBBACServiceConciliacionBalance",
		     	   "action": "getBalance",
		     	   "filters":{
		     		   "cdisin" : getIsin(),
		                "fliquidacionDe" : transformaFechaInv($("#fliquidacionDe").val()),
		                "fliquidacionA":transformaFechaInv($("#fliquidacionA").val()),
		                "cuentaLiquidacion":$("#selectCuenta").val()
		                }
	 			  };
	    $("#tblClearingTitulos > tbody").html("");
		consultar(params);
}
var screenBlock = null;
function consultar(params) {
	screenBlock = inicializarLoading();
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(params),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
            		json.resultados.result_balance === undefined) {
                datos={};
            } else {
                datos = json.resultados.result_balance;
            }
            var tbl = $("#tblClearingTitulos > tbody");
            $(tbl).html("");
			oTable.fnClearTable();
            //var row;
            $.each(datos, function (key, val) {
				oTable.fnAddData([
                                   "<span>"+val.isin+"</span>",
                                   "<span>"+val.descrIsin+"</span>",
                                   "<span>"+val.cdCodigo+"</span>",
                                   "<span>"+val.titulos+"</span>"
             					],false);
            });
            oTable.fnDraw();
            $.unblockUI();
        },
        error : function(err){
        	console.log("Error: "+err);
        	$unblockUI();
        }

    });
}
function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";
	if ($('#cdisin').val() !== "" && $('#cdisin').val() !== undefined){
		cadenaFiltros += " isin: "+ $('#cdisin').val();
	}
	if ($('#selectCuenta').val() !== "" && $('#selectCuenta').val() !== undefined && $('#selectCuenta').val() !== '0' ){
		cadenaFiltros += " cuenta: "+ $('#selectCuenta option:selected').html();
	}
	if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined ){
		cadenaFiltros += " fecha: "+ $('#fliquidacionDe ').val();
	}
	$('.mensajeBusqueda').append(cadenaFiltros);
}
function getIsin(){
	var isinCompleto = $('#cdisin').val().trim();
	console.log("isinCompleto: "+isinCompleto);
	var guion=isinCompleto.indexOf("-");
	if (guion<0){
		return isinCompleto;
	}else{
		return isinCompleto.substring(0, guion);
	}
}
function getFechaPrevia() {
	var params = new DataBean();
	params.setService('SIBBACServiceMovimientoVirtual');
	params.setAction('getFechaPreviaLiquidacion');
	var request = requestSIBBAC(params);
	request.success(function(json) {
		var datos = undefined;

		if (json === undefined || json.resultados === undefined
				|| json.resultados.result_fecha_previa_laboral === undefined) {
			datos = {};
		} else {
			datos = json.resultados.result_fecha_previa_laboral;
			$("#fliquidacionDe").val(transformaFecha(datos[0]));
		}
	});
}
