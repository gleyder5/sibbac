
var refreshDataListener = 0;
/* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
if ($('.contenedorTabla').length >= 1) {
    var anchoBotonera;
    $('.contenedorTabla').each(function (i) {
        anchoBotonera = $(this).find('table').outerWidth();
        $(this).find('.botonera').css('width', anchoBotonera + 'px');
        $(this).find('.resumen').css('width', anchoBotonera + 'px');
        $('.resultados').hide();
    });
}

function consultar(params) {

    /* parte ajax */

    var request = requestSIBBAC(params);
    request.success(function (json) {
        var datos = undefined;

        if (json === undefined || json.resultados === undefined ||
                json.resultados.result_cuenta_propia === undefined) {
            datos = {};
        } else {
            datos = json.resultados.result_cuenta_propia;
            var tbl = $("#tblCtaPropia > tbody");
            oTable.fnClearTable();
            var difClass = 'centrado';
            $.each(datos, function (key, val) {
                var descIsin = (datos[key].isinDescripcion === null) ? "" : datos[key].isinDescripcion;
                oTable.fnAddData([
					datos[key].cdisin,
					descIsin,
					datos[key].cdCuenta,
					a2digitos(datos[key].nutitulos),
					a2digitos(datos[key].imefectivo)
					]);


                /*var row = "";
                row = "<tr class=\"" + difClass + "\" >" +
                        "<td><div class=\"aleft\">" + datos[key].cdisin + "</div>" +
                        "<td><div class=\"aleft\">" + descIsin + "</div>" +
                        "</td><td>" + datos[key].cdCuenta +
                        "</td><td class=\"monedaR\"><div><span>" + a2digitos(datos[key].nutitulos) +
                        "</div><span></td><td class=\"monedaR\"><div><span>" + a2digitos(datos[key].imefectivo) +
                        "</div><span></td></tr>";
                tbl.append(row);*/
            });

            //$("#tblCtaPropia").tablesorter({sortList: [[0, 0], [2, 1]], widgets: ['zebra']});

        }
    });
}
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize_propiaTitulos() {
    var fechas = ['fcontratacionDe', 'fcontratacionA', 'fliquidacionA', 'fliquidacionDe'];
    loadpicker(fechas);
    obtenerDatos();
}
var oTable = undefined;
$(document).ready(function () {
	oTable = $("#tblCtaPropia").dataTable({
		"pagingType": "full_numbers",
		 "language": {
             "url": "i18n/Spanish.json"
         },
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "aoColumns" : [
                        { sWidth: '30%' },
                        { sWidth: '25%', sClass: "aleft" },
                        { sWidth: '20%', sClass: "monedaR",type:"formatted-num" },
                        { sWidth: '12%', sClass: "monedaR",type:"formatted-num" },
                        { sWidth: '15%', sClass: "monedaR",type:"formatted-num" }
                    ]
	});
    if ($("#tblCtaPropia > tbody").children().size() === 0) {
        console.log('carga initialize');
        initialize_propiaTitulos();
    }
    else {
        console.log('ups no se deberia cargar');
    }
    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() !== a && that.text() !== b) {
                that.text(a);
            } else if (that.text() === a) {
                that.text(b);
            } else if (that.text() === b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({
            marginTop: '0px'
        }, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({
            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
        }, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
    });

    $('.collapser').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

        return false;
    });
    $('.collapser_search').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        if ($(this).text() == "Ocultar opciones de búsqueda") {
            $(this).text("Mostrar opciones de búsqueda");
        } else {
            $(this).text("Ocultar opciones de búsqueda");
        }
        return false;
    });
    $('.btn.buscador').click(function (event) {
        event.preventDefault();
        $("#tblCtaPropia > tbody").empty();
        $("#tblCtaPropia").trigger("update");
        obtenerDatos();

        $('.collapser_search').parents('.title_section').next().slideToggle();
        $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
        $('.collapser_search').toggleClass('active');
        if ($('.collapser_search').text() == "Ocultar opciones de búsqueda") {
            $('.collapser_search').text("Mostrar opciones de búsqueda");
        } else {
            $('.collapser_search').text("Ocultar opciones de búsqueda");
        }
        seguimientoBusqueda();
        return false;
    });

    $('#limpiar').click(function (event) {
        event.preventDefault();
        $('input[type=text]').val('');
        $('select').val('0');
    });
//	$(".getExcel").click(function(event){
//		event.preventDefault();
//		obtenerExcel();
//	});
    if (refreshDataListener === 0) {
        refreshDataListener = setInterval(obtenerDatos, 320000);
    }
});
function render() {
    //
}
//rellena los filtros
function obtenerFiltros() {
    return {"fcontratacionDe": transformaFechaInv($("#fcontratacionDe").val()),
        "fcontratacionA": transformaFechaInv($("#fcontratacionA").val()),
        "fliquidacionDe": transformaFechaInv($("#fliquidacionDe").val()),
        "fliquidacionA": transformaFechaInv($("#fliquidacionA").val()),
        "cdisin": $("#cdisin").val(),
        "cdcamara": $("#cdcamara").val()};
}
function obtenerDatos() {
    $("#tblCtaPropia > tbody > tr").remove();
    var param = obtenerFiltros();

    var params = new DataBean();
    params.setService('SIBBACServiceConciliacionPropia');
    params.setAction('getAnotacioneseccByTipoCuenta');
    params.setFilters(JSON.stringify(param));
    consultar(params);
}
////Obtener excel
//function obtenerExcel(){
//	var param = obtenerFiltros();
//	var params = new DataBean();
//	params.setService('SIBBACServiceConciliacionPropia');
//	params.setAction('getAnotacioneseccByTipoCuenta');
//	params.setURL("/sibbac20back/rest/export.xls");
//	params.setFilters(JSON.stringify(param));
//    consultar(params);
//
//}

//para ver el filtro al minimizar la búsqueda
function seguimientoBusqueda() {
    $('.mensajeBusqueda').empty();
    var cadenaFiltros = "";
    if ($('#fcontratacionDe').val() !== "") {
        cadenaFiltros += " fcontratacionDe: " + $('#fcontratacionDe').val();
    }
    if ($('#fcontratacionA').val() !== "") {
        cadenaFiltros += " fcontratacionA: " + $('#fcontratacionA').val();
    }
    if ($('#fliquidacionDe').val() !== "") {
        cadenaFiltros += " fliquidacionDe: " + $('#fliquidacionDe').val();
    }
    if ($('#fliquidacionA').val() !== "") {
        cadenaFiltros += " fliquidacionA: " + $('#fliquidacionA').val();
    }
    if ($('#cdisin').val() !== "") {
        cadenaFiltros += " isin: " + $('#cdisin').val();
    }
    if ($('#cdcamara').val() !== "") {
        cadenaFiltros += " camara: " + $('#cdcamara').val();
    }
    $('.mensajeBusqueda').append(cadenaFiltros);
}

//exportar
function SendAsExport(format) {
    var c = this.document.forms['propiaTitulos'];
    var f = this.document.forms["export"];
    console.log("Forms: " + f.name + "/" + c.name);
    f.action = "/sibbac20back/rest/export." + format;
    var json = {
        "service": "SIBBACServiceConciliacionPropia",
        "action": "getAnotacioneseccByTipoCuenta",
        "filters": {"fcontratacionDe": transformaFechaInv($("#fcontratacionDe").val()),
            "fcontratacionA": transformaFechaInv($("#fcontratacionA").val()),
            "fliquidacionDe": transformaFechaInv($("#fcontratacionDe").val()),
            "fliquidacionA": transformaFechaInv($("#fcontratacionA").val()),
            "cdisin": $("#cdisin").val(),
            "cdcamara": $("#cdcamara").val()}};
    if (json != null && json != undefined && json.length == 0) {
        alert("Introduzca datos");
    } else {
        f.webRequest.value = JSON.stringify(json);
        f.submit();
    }
}
