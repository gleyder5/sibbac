var listaIsin = [];
var availableTags = [];
var listaAlias = [];
var availableTagsAlias = [];
var oTable = undefined;
function consultar(params) {

	inicializarLoading();
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(params),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
                // x.overrideMimeType("application/json;charset=UTF-8");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos = undefined;
            if (json === undefined ||
            		json.resultados === undefined ||
            		json.resultados === null ||
                    json.resultados.result_balance_clearing === undefined) {
                datos={};
            } else {
                datos = json.resultados.result_balance_clearing;
                var errors = cargarDatos(datos);
            }
            $.unblockUI();
			//$("#tblClearingTitulos").tablesorter({sortList:[[0,0],[2,1]], widgets: ['zebra']});

        },
        error : function(err){
        	$.unblockUI();
        }

    });
}
function cargarDatos(datos){
	var tbl = $("#tblClearingTitulos > tbody");
    $(tbl).html("");
    var srcValid = "img/activo.png";
    var srcWarning = "img/desactivo.png";
	oTable.fnClearTable();
	var alias = "";
	var descAli = "";
	var src = "";
	var isin = "";
	var descIsin = "";
	var titulos = 0;
	var efectivo = 0;
	var lineaMostrada = false;
	var cabeceraMostrada = false;
	var totalTitulosTag = "";
	var totalEfectivoTag = "";
	var totalTitulos = 0;
    $.each(datos, function (key, val) {
        var grupo = datos[key].grupoBalanceClienteList;
        lineaMostrada = false;
        totalTitulos = 0;
        totalEfectivoTag = "";
        totalTitulosTag = "";
        cabeceraMostrada = false;
        if(grupo !== null){
	        alias = (datos[key].alias !== "" && datos[key].alias !== null) ? datos[key].alias : datos[key].descralias;
	        descAli = (datos[key].descralias !== "" && datos[key].descralias !== null) ? datos[key].descralias : datos[key].alias;
	        src = (grupo[0].valido === false) ?srcWarning : srcValid;
	        isin = (grupo[0].isin !== null && grupo[0].isin !== undefined) ? grupo[0].isin : "";
			descIsin = (grupo[0].descrisin !== null && grupo[0].descrisin !== undefined) ? grupo[0].descrisin : "";
	        titulos = (grupo[0].titulos !== null && grupo[0].titulos !== undefined ) ? grupo[0].titulos : 0;
			totalTitulos += titulos;
	        efectivo = (grupo[0].efectivo !== null && grupo[0].efectivo !== undefined) ? grupo[0].efectivo : 0;
			if(titulos !== 0 || efectivo !==0){
				lineaMostrada = true;
				cabeceraMostrada = true;
			oTable.fnAddData([
			        (alias!=null && alias!=undefined) ? alias : '' ,
			        descAli,
			        isin,
			        descIsin,
			        $.number(titulos,0,',','.'),
			        $.number(efectivo, 2,',','.')], false);
			}
	        var i = 1;
	        $.each(grupo, function(indice, valor){

	            if(grupo !== null && i < grupo.length){
	            	lineaMostrada = true;
	            	isin = (grupo[i].isin !== null && grupo[i].isin !== undefined) ? grupo[i].isin : "";
	    			descIsin = (grupo[i].descrisin !== null && grupo[i].descrisin !== undefined) ? grupo[i].descrisin : "";
	    			titulos = (grupo[i].titulos !== null && grupo[i].titulos !== undefined ) ? grupo[i].titulos : 0;
	    			totalTitulos += titulos;
	    			efectivo = (grupo[i].efectivo !== null && grupo[i].efectivo !== undefined) ? grupo[i].efectivo : 0;

	    			var src = (grupo[i].valido === false) ?srcWarning : srcValid;
	    			if(titulos !== 0 || efectivo !==0){
		    			oTable.fnAddData([
									((!cabeceraMostrada) ? alias : ''),
									((!cabeceraMostrada) ? descAli : ''),
									isin,
									descIsin,
									$.number(titulos,0,',','.'),
									$.number(efectivo, 2, ',','.')
		     					], false);
	    			}
	                i++;
            }
        });
	        //si hay algun registro se muestra el total
	        if(lineaMostrada){
	        	totalTitulosTag = (totalTitulos === 0) ? "" : "<div class=\"negrita celda-tabla-245\"><span class=\"total-efectivo-label\">"+"Total T&iacute;tulos: </span><span style=\"float:right;\">" + $.number(totalTitulos,0,',','.') + "</span></div>";
	        	totalEfectivoTag = (datos[key].totalEfectivos === 0) ? "" : "<div class=\"negrita celda-tabla-245\"><span class=\"total-efectivo-label\">"+"Total Efectivo: </span><span style=\"float:right;\">" + $.number(datos[key].totalEfectivos,2,',','.') + "</span></div>";
	        	if(totalTitulos !== 0 || datos[key].totalEfectivos !==0){
		        	oTable.fnAddData([
							  '',
	                          '',
	                          '',
	                          '',
	                          totalTitulosTag,
	                          totalEfectivoTag
	     					], false);
		        	}
	        }
    	}
    });
    oTable.fnDraw();
    return false;
}
/////////////////////////////////////////////////
////// SE EJECUTA NADA MÁS CARGAR LA PÁGINA ////
function initialize() {
	var fechas = ['fecha'];
	loadpicker(fechas);
	$('#fecha').val(getfechaHoy());
	cargaIsin();
	cargaAlias();
	cargarFechaActualizacionMovimientoAlias();
	cargarCombos();
	//obtenerDatos();
}
$(document).ready(function () {
	oTable = $("#tblClearingTitulos").dataTable({
		"dom": 'T<"clear">lfrtip',
		"bSort": false,
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "language": {
             "url": "i18n/Spanish.json"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "aoColumns" :[
          {"sClass": "align-left"},
          {"sClass": "align-left"},
          {"sClass": "align-left"},
          {"sClass": "align-left"},
          {"sClass": "align-right"},
          {"sClass": "align-right"}
          ]
	});
    initialize();

    /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }

    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function (event) {
		event.preventDefault();
		$(this).parents('.title_section').next().slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function(event){
		event.preventDefault();
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		if ( $(this).text()=="Ocultar opciones de búsqueda" ) {
			$(this).text( "Mostrar opciones de búsqueda" );
		} else {
			$(this).text( "Ocultar opciones de búsqueda" );
		}
		return false;
	});
	$('#cargarTabla').submit(function(event){
		event.preventDefault();

		obtenerDatos();
		$('.collapser_search').parents('.title_section').next().slideToggle();
		$('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
		$('.collapser_search').toggleClass('active');
		if ( $('.collapser_search').text().indexOf('Ocultar')!==-1 ) {
			$('.collapser_search').text( "Mostrar opciones de búsqueda" );
		} else {
			$('.collapser_search').text( "Ocultar opciones de búsqueda" );
		}
		seguimientoBusqueda();
		return false;
	});

	$('#limpiar').click(function (event) {
		event.preventDefault();
		$('input[type=text]').val('');
		$('select').val('0');
	});

});
function render() {
    //
}


function obtenerDatos(){

	 var params = {"service": "SIBBACServiceConciliacionClearing",
		     	   "action": "getBalanceMovimientosCuentaVirtual",
		     	   "filters":{"isin" : getIsin(),
		                     "alias" : getAliasId(),
							 "fecha": transformaFechaInv($("#fecha").val()),
							 "cdcodigocuentaliq":$("#selectCuenta").val()}
	 			  };
	    $("#tblClearingTitulos > tbody").html("");
		consultar(params);
}

//llamada para cargar datos isin
function cargaIsin(){

	 var llamada = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getIsin"
			   };
	 //limpio los datos
	 $("#isin").html("");
	 //cargo isin
	 cargaDatosIsin(llamada);
}

function cargarCombos(){
	 var selectCuentaCall = {"service": "SIBBACServiceConciliacionClearing",
		     	   "action": "getCuentasLiquidacionClearing"
	 			   };

	 $("#selectCuenta").html("");
	 //$("#selectCuenta").append("<option value='0'>Todas</option>");
	 cargaDatosCombo(selectCuentaCall);
}

//carga datos combo cuenta
function cargaDatosCombo(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
                    json.resultados.result_cuentas_liquidacion_clearing === undefined) {
                datos={};
//                return false;
            } else {
                datos = json.resultados.result_cuentas_liquidacion_clearing;
            }

            if (json.resultados.result_isin === undefined ) {
                datosIsin={};
             } else {
            	datosIsin = json.resultados.result_isin;
             }
            $("#selectCuenta").empty();
            $("#selectCuenta").append("<option value=\"\">Seleccione una Cuenta</option>" );
            $.each(datos, function (key, val) {
            	$("#selectCuenta").append('<option value='+datos[key].cdCodigo+'>'+datos[key].cdCodigo+'</option>');
            });

            $.each(datosIsin, function (key, val) {
        		listaIsin.push(datosIsin[key].codigo);
				ponerTag = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
				availableTags.push(ponerTag);
            });
			$( "#cdisin" ).autocomplete({
				source: availableTags
			});
        }
    });
}

function cargaDatosIsin(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datosIsin = undefined;

            if( json !== undefined || json.resultados !== undefined){

            if (json.resultados.result_isin === undefined ) {
                datosIsin={};
             } else {
            	  datosIsin = json.resultados.result_isin;
             }


            }else{
            	return false;
            }



            $.each(datosIsin, function (key, val) {

            		listaIsin.push(datosIsin[key].codigo);
    				ponerTag = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
    				availableTags.push(ponerTag);

    			//código de autocompletar

               $( "#isin" ).autocomplete({
                 source: availableTags
               });

            });


        }
    });
}

//para ver el filtro al minimizar la búsqueda
function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";
	if ($('#isin').val() !== "" && $('#isin').val() !== undefined){
		cadenaFiltros += " isin: "+ $('#isin').val();
	}
	if ($('#alias').val() !== "" && $('#alias').val() !== undefined ){
		cadenaFiltros += " alias: "+ $('#alias').val();
	}
	if ($('#fecha').val() !== "" && $('#fecha').val() !== undefined ){
		cadenaFiltros += " fecha: "+ $('#fecha').val();
	}
	if ($('#selectCuenta').val() !== "" && $('#selectCuenta').val() !== undefined && $('#selectCuenta').val() !== '0' ){
		cadenaFiltros += " cuenta: "+ $('#selectCuenta option:selected').html();
	}
	$('.mensajeBusqueda').append(cadenaFiltros);
}



function getIsin(){
	var isinCompleto = $('#isin').val();
	var guion=isinCompleto.indexOf("-");
	if (guion > 0){
		return isinCompleto.substring(0, guion);
	}else{
		return isinCompleto;
	}
}

function cargaAlias() {

	var params = new DataBean();
	params.setService('SIBBACServiceTmct0ali');
	params.setAction('getAlias');
	var request = requestSIBBAC(params);
	request.success(function(json) {
		var datos = undefined;

		if (json === undefined || json.resultados === undefined
				|| json.resultados.result_alias === undefined) {
			datos = {};
		} else {
			datos = json.resultados.result_alias;
			var item = null;

			for ( var k in datos) {
				item = datos[k];
				listaAlias.push(item.alias);
				ponerTag = item.alias + " - " + item.descripcion;
				availableTagsAlias.push(ponerTag);

			}
			// código de autocompletar

			$("#alias").autocomplete({
				source : availableTagsAlias
			});
		}
		// fin de código de autocompletar
	});
	request.error( function(c) {
		console.log( "ERROR: " + c );
	});
}

function getAliasId() {
	var alias = $('#alias').val();
	var guion = alias.indexOf("-");
	return (guion > 0) ? alias.substring(0, guion) : alias;
}





