var oTable = undefined;

function consultar(params) {
	$.blockUI({
		message: '<h1><img src="img/loading.gif" /> Cargando datos...</h1>',
		css: {
	        border: 'none',
	        padding: '15px',
	        backgroundColor: '#000',
	        '-webkit-border-radius': '10px',
	        '-moz-border-radius': '10px',
	        opacity: .5,
	        color: '#fff'
	     }
	});
    /* parte ajax */
			var data = new DataBean();
			data.setService('SIBBACServiceConciliacionClearing');
			data.setAction('getDesgloseCamaraBySentidoCuentasClearing');
			data.setFilters(params);
			var request = requestSIBBAC( data );
			request.success( function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined || json.resultados === null || json.resultados.result_provision_efectivo === undefined) {
            	datos ={};
            } else {
                datos = json.resultados.result_provision_efectivo;
                var errors = cargarDatos(datos);
            }
            $.unblockUI();
        });
}
function cargarDatos(datos){
    var tbl = $("#tblProvision > tbody");
	$(tbl).html("");
    var difClass = 'centrado';
	oTable.fnClearTable();
    $.each(datos, function (key, val) {
		var filas;
		if ($('#porcentaje').val()!==""){
			filas = 3;
		}else{
			filas = 2;
		}
	    for (var i =0;i<filas;i++){

			/*var row = "<tr id=\"isin" + key + "\" class=\"" + difClass + "\" >" +
					"<td>" + datos[key].isin +
					"</td><td>" + datos[key].codCtaDeCompensacion;	*/
			var row = [];

				row.push(datos[key].isin);
				row.push(datos[key].codCtaDeCompensacion);
					switch (i){
						case 0:
								row.push(' Favorable ');
								row.push('0,00');
								//corretajes agrupados por isin
								row.push(a2digitos(datos[key].corretaje));
								row.push(a2digitos(datos[key].corretaje));
							   break;
					    case 1:
					    	//suma de corretajes y efectivos
						       var suma = datos[key].corretaje + datos[key].efectivo;//datos[key].efectivo + datos[key].totalCanones;
								row.push(' Desfavorable ');
								row.push(a2digitos(datos[key].efectivo));
								row.push(a2digitos(datos[key].corretaje));
								//row.push(datos[key].totalCanones);
								row.push(a2digitos(suma));
							   break;
					    case 2:
						    if (($("#cdCodigo").val()==='0' || $("#cdCodigo").val()==="" || $("#cdCodigo").val()===0 ) || $("#cdCodigo").val()=== datos[key].codCtaDeCompensacion ){
							   //var suma = ((datos[key].efectivo * parseFloat($('#porcentaje').val()))/100) + datos[key].totalCanones;
						    	var suma = ((datos[key].efectivo * parseFloat($('#porcentaje').val()))/100) + datos[key].corretaje;
							   row.push(parseFloat($('#porcentaje').val()) + '% Fallidos');
								row.push(a2digitos(((datos[key].efectivo * parseFloat($('#porcentaje').val()))/100)));
								//row.push(datos[key].totalCanones);
								row.push(a2digitos(datos[key].corretaje));
								row.push(a2digitos(suma));
							}
							break;

					}
					//row += "</td></tr>";
					//tbl.append(row);
					oTable.fnAddData(row, false);
		}
    });
    oTable.fnDraw();

}
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {
	//Si se ha llamado initialize desde un setTimeout,
	//se comprueba y se elimina

	var fechas = ['fliquidacionDe','fliquidacionA'];
	verificarFechas([["fliquidacionDe","fliquidacionA"]]);
	loadpicker(fechas);
	cargarFechaActualizacionMovimientoAlias();
	cargaCombo();
	cargarFechasLiquidacion();
	//obtenerDatos();
}

$(document).ready(function () {
	oTable = $("#tblProvision").dataTable({
		"dom": 'T<"clear">lfrtip',
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "language": {
             "url": "i18n/Spanish.json"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "aoColumns" : [
           {sClass : "centrar"},
           {sClass : "centrar"},
           {sClass : "align-left"},
           {sClass : "monedaR",type:"formatted-num"},
           {sClass : "monedaR",type:"formatted-num"},
           {sClass : "monedaR",type:"formatted-num"}
           ]
	});
    initialize();

	/* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
	if ($('.contenedorTabla').length >= 1) {
		var anchoBotonera;
		$('.contenedorTabla').each(function (i) {
			anchoBotonera = $(this).find('table').outerWidth();
			$(this).find('.botonera').css('width', anchoBotonera + 'px');
			$(this).find('.resumen').css('width', anchoBotonera + 'px');
			$('.resultados').hide();
		});
	}
    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({marginTop: '0px'}, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'}, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked']);
    });

    $(function(){
		prepareCollapsion();
	});
    $('#cargarTabla').submit(function (event) {
        event.preventDefault();
    	$("#tblProvision > tbody").empty();
        //$("#tblProvision").trigger("update");
        var isValid = validarRequeridos();
    	if(!isValid){
    		return false;
    	}
        obtenerDatos();
        collapseSearchForm();
		seguimientoBusqueda();
        return false;
    });

	$('#limpiar').click(function (event) {
		event.preventDefault();
		$('input[type=text]').val('');
		$('select').val('0');
	});

});
function validarRequeridos(){
	var isValid = true;
	if($("#fliquidacionDe").val() === ""){
		alert("Por favor cumplimente todos los campos marcados con (*).");
		return false;
	}
	return isValid;
}
function render() {
    //
}

function obtenerDatos() {
    var cuenta = ($("#cdCodigo").val() === "0" || $("#cdCodigo").val() === "" ) ? "" : $("#cdCodigo").val();
    var params = {"fliquidacionA" : transformaFechaInv($("#fliquidacionA").val()),
            "fliquidacionDe": transformaFechaInv($("#fliquidacionDe").val()),
            "codCtaDeCompensacion" : cuenta };
    $("#tblCtaPropia > tbody").html("");
    consultar(JSON.stringify(params));
}


//llamada para cargar datos combo
function cargaCombo(){
	var data = new DataBean();
	data.setService('SIBBACServiceConciliacionClearing');
	data.setAction('getCuentasLiquidacionClearingNoPropiaNoIndividual');
	 $("#cdCodigo").html("");
	 $("#cdCodigo").append("<option value='0'> todas Clearing no propia no individual</option>");
	 cargaDatosCombo(data);
}
//cargar fechas liquidacion between
function cargarFechasLiquidacion(){
    console.log("llamando al servicio: getInitialFliquidacionBetween()");
    var data = new DataBean();
    data.setService('SIBBACServiceConciliacionClearing');
    data.setAction('getInitialFliquidacionBetween');
    var request = requestSIBBAC(data);
    request.success(function(json){
        var datos = undefined;
        if (json === undefined || json.resultados === undefined || json.resultados.result_fliquidacion_between === undefined) {
           //
        } else {
            datos = json.resultados.result_fliquidacion_between;
            $("#fliquidacionDe").val(transformaFecha(datos.fliquidacionDe));
            $("#fliquidacionA").val(transformaFecha(datos.fliquidacionA));
        }
    });
}
//carga datos combo cuenta
function cargaDatosCombo(llamada) {
    console.log ('llamada a getCuentasLiquidacionClearingNoPropiaNoIndividual');
	var requestCombo = requestSIBBAC( llamada );
	requestCombo.success( function (json) {
        var datos = undefined;
        if (json === undefined || json.resultados === undefined || json.resultados.result_cuentas_liquidacion_clearing === undefined) {
           $("#cdCodigo").append('<option value="0">sin cuentas Clearing</option>');
        } else {
            datos = json.resultados.result_cuentas_liquidacion_clearing;
			 $.each(datos, function (key, val) {
				if(datos[key].cdCodigo !== "00P"){
					$("#cdCodigo").append('<option value="'+datos[key].cdCodigo+'">'+datos[key].cdCodigo+'</option>');
			 	}
			});
        }

    });
}
//para ver el filtro al minimizar la búsqueda
function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();

	var  cadenaFiltros = "";
	if ($('#cdCodigo').val() !== "" && $('#cdCodigo').val() !== undefined){
		if ($('#cdCodigo').val() === "0"){
		cadenaFiltros += " cdCodigo: todas ";
		}
		else{
		cadenaFiltros += " cdCodigo: "+ $('#cdCodigo').val();
		}
	}
	if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined){
		cadenaFiltros += " fliquidacionDe: "+ $('#fliquidacionDe').val();
	}
	if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined){
		cadenaFiltros += " fliquidacionA: "+ $('#fliquidacionA').val();
	}
	if ($('#porcentaje').val() !== "" && $('#porcentaje').val() !== undefined){
		cadenaFiltros += " % fallidos : "+ $('#porcentaje').val();
	}
	$('.mensajeBusqueda').append(cadenaFiltros);
}

/*
//exportar
function SendAsExport( format ) {
	var c = this.document.forms['propiaefectivos'];
	var f = this.document.forms["export"];
	console.log("Forms: "+f.name + "/"+c.name);
	f.action="/sibbac20back/rest/export." + format;
	var json = {
		    "service": "SIBBACServiceConciliacionClearing",
		    "action": "getDesgloseCamaraBySentidoCuentasClearing",
	        "filters" : {"fcontratacionA" : transformaFechaInv($("#fcontratacionA").val()),
	            		 "fcontratacionDe": transformaFechaInv($("#fcontratacionDe").val()),
	            		 "codCtaDeCompensacion" : ($("#cdCodigo").val() === "0" || $("#cdCodigo").val() === "" ) ? "" : $("#cdCodigo").val() }};
	if ( json!=null && json!=undefined && json.length==0 ) {
		alert("Introduzca datos");
	} else {
		f.webRequest.value=JSON.stringify(json);
		f.submit();
	}
}*/

