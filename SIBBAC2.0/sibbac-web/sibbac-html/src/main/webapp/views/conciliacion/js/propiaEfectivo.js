function consultar(params) {

	var request = requestSIBBAC( params );
	request.success( function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
                    json.resultados.result_cuenta_propia === undefined) {
                datos={};
            } else {
                datos = json.resultados.result_cuenta_propia;
            }
            var tbl = $("#tblCtaPropia > tbody");
			$(tbl).html("");
            var difClass = 'centrado';
            $.each(datos, function (key, val) {
                var row = "<tr id=\"" + datos[key].idanotacionecc + "\" class=\"" + difClass + "\" >" +
                        "<td>" + datos[key].cdisin +
                        "</td><td>" + datos[key].cdCuenta +
                        "</td><td>" + datos[key].cdcamara +
                        "</td><td>" + datos[key].cdsentido +
                        "</td><td>" + transformaFecha(datos[key].fcontratacion) +
                        "</td><td>" + transformaFecha(datos[key].fliquidacion) +
                        "</td><td>" + datos[key].nutitulos +
                        /*"</td><td>" + ((colDif === false) ? datos[key].imprecio : '') +*/
                        "</td><td>" + datos[key].imefectivo +
                        "</td></tr>";
                tbl.append(row);
            });
			$("#tblCtaPropia").tablesorter({sortList:[[0,0]], widgets: ['zebra']});
        });

}
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {
     var fechas = ['fcontratacionDe','fcontratacionA','fliquidacionA','fliquidacionDe'];
	loadpicker(fechas);
    obtenerDatos();
}

$(document).ready(function () {
    initialize();

    /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }

    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({
            marginTop: '0px'
        }, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({
            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
        }, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
    });

    $('.collapser').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

        return false;
    });
    $('.collapser_search').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        if ($(this).text() == "Ocultar opciones de búsqueda") {
            $(this).text("Mostrar opciones de búsqueda");
        } else {
            $(this).text("Ocultar opciones de búsqueda");
        }
        return false;
    });
    $('#cargarTabla').submit(function (event) {
		event.preventDefault();
    	$("#tblCtaPropia > tbody").empty();
        $("#tblCtaPropia").trigger("update");
        obtenerDatos();

        $('.collapser_search').parents('.title_section').next().slideToggle();
        $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
        $('.collapser_search').toggleClass('active');
        if ($('.collapser_search').text() == "Ocultar opciones de búsqueda") {
            $('.collapser_search').text("Mostrar opciones de búsqueda");
        } else {
            $('.collapser_search').text("Ocultar opciones de búsqueda");
        }
		seguimientoBusqueda();
        return false;
    });
	$('#limpiar').click(function (event) {
		event.preventDefault();
		$('input[type=text]').val('');
		$('select').val('0');
	});

});
function render() {
    //
}


function obtenerDatos() {
    var params = [{"fcontratacionDe": transformaFechaInv($("#fcontratacionDe").val())},
            {"fcontratacionA": transformaFechaInv($("#fcontratacionA").val())},
            {"fliquidacionDe": transformaFechaInv($("#fliquidacionDe").val())},
            {"fliquidacionA": transformaFechaInv($("#fliquidacionA").val())},
            {"cdisin": $("#cdisin").val()},
            {"cdcamara": $("#cdcamara").val()}];
	var data = new DataBean();
	data.setService('SIBBACServiceConciliacionPropia');
	data.setAction('getAnotacioneseccByTipoCuenta');
	data.setParams(JSON.stringify(params));
    $("#tblCtaPropia > tbody").html("");
    consultar(data);
}

//para ver el filtro al minimizar la búsqueda
function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";
	if ($('#fcontratacionDe').val() !== "" && $('#fcontratacionDe').val() !== undefined){
		cadenaFiltros += " fcontratacionDe: "+ $('#fcontratacionDe').val();
	}
	if ($('#fcontratacionA').val() !== "" && $('#fcontratacionA').val() !== undefined){
		cadenaFiltros += " fcontratacionA: "+ $('#fcontratacionA').val();
	}
	if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined){
		cadenaFiltros += " fliquidacionDe: "+ $('#fliquidacionDe').val();
	}
	if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined){
		cadenaFiltros += " fliquidacionA: "+ $('#fliquidacionA').val();
	}
	if ($('#cdisin').val() !== "" && $('#cdisin').val() !== undefined){
		cadenaFiltros += " isin: "+ $('#cdisin').val();
	}
	if ($('#cdcamara').val() !== "" && $('#cdcamara').val() !== undefined){
		cadenaFiltros += " camara: "+ $('#cdcamara').val();
	}
	$('.mensajeBusqueda').append(cadenaFiltros);
}
