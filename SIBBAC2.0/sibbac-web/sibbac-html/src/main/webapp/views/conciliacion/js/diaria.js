var listaIsin = [];
var listaCamara = [];
var availableTags = [];
var availableTagsCamara = [];
var oTable = undefined;
function consultar(params) {

//    for(key in params.filters){
//        alert("key: "+key + " value: "+params.filters[key]);
//    }
	inicializarLoading();
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(params),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
                // x.overrideMimeType("application/json;charset=UTF-8");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos = undefined;
            if (json === undefined ||
            		json.resultados === undefined ||
            		json.resultados === null ||
                    json.resultados.resultados_diaria === undefined) {
                datos={};
            } else {
                datos = json.resultados.resultados_diaria;
            }
            var tbl = $("#tblDiaria > tbody");
            var difClass = 'centrado';
			oTable.fnClearTable();
            $.each(datos, function (key, val) {
			    oTable.fnAddData([
                                  "<span>" +  datos[key].isin + "</span>",
                                  "<span>" +  datos[key].descIsin + "</span>",
                                  "<span>" + datos[key].idAlias  + "</span>",
                                  "<span>" +  datos[key].alias + "</span>",
                                  "<span>" + datos[key].camara  + "</span>",
                                  "<span>" + datos[key].estado.toUpperCase()  + "</span>",
                                  "<span>" + transformaFecha(datos[key].tradedate)  + "</span>",
                                  "<span>" + transformaFecha(datos[key].settlementdate)  + "</span>",
                                  "<span>" + datos[key].sentido  + "</span>",
                                  "<span>" + a2digitos(datos[key].nuTitulos)  + "</span>",
                                  "<span>" + a6digitos(datos[key].precio)  + "</span>"
             					]);
              /*  var row = "<tr class=\"" + difClass + "\" >" +
                        "<td>" + datos[key].isin +
                        "</td><td>" + datos[key].alias +
                        "</td><td>" + datos[key].camara +
                        "</td><td>" + datos[key].estado +
                        "</td><td>" + transformaFecha(datos[key].tradedate) +
                        "</td><td>" + transformaFecha(datos[key].settlementdate) +
                        "</td><td>" + datos[key].sentido +
                        "</td><td class=\"monedaR\">" + a2digitos(datos[key].nuTitulos) +
                        "</td><td class=\"monedaR\">" + a6digitos(datos[key].precio) +
                        "</td></tr>";

                tbl.append(row);   */
            });
           // $("#tblDiaria").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
            $.unblockUI();
        },
        error : function(err){
        	$.unblockUI();
        }
    });

}

function consultarFechas(params2) {
	/* parte ajax */

		 $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(params2),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
                // x.overrideMimeType("application/json;charset=UTF-8");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
	            var datos = undefined;
	            if (json === undefined || json.resultados === undefined ||
	                    json.resultados.fcontratacionIni === undefined) {
	                datos={};
	            } else {
	                datos = json.resultados.fcontratacionIni;
	                $("#fcontratacionDe").val(transformaFecha(json.resultados.fcontratacionIni));
		            $("#fcontratacionA").val(transformaFecha(json.resultados.fcontratacionIni));
	                //obtenerDatos();
	            }


	    }
	});
}
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {

     var fechas = ['fcontratacionA','fcontratacionDe','fliquidacionA','fliquidacionDe'];
     verificarFechas([['fcontratacionDe','fcontratacionA'],['fliquidacionDe','fliquidacionA']]);
	loadpicker(fechas);
	cargaCombos();
	cargarFechasIni();

}
$(document).ready(function () {
	oTable = $("#tblDiaria").dataTable({
		"dom": 'T<"clear">lfrtip',
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "language": {
             "url": "i18n/Spanish.json"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "aoColumns" : [
                        {sClass : "centrar"},
                        {sClass : "centrar"},
                        {sClass : "centrar"},
                        {sClass : "centrar"},
                        {sClass : "centrar"},
                        {sClass : "centrar"},
                        {sClass : "centrar",type: 'date-eu'},
                        {sClass : "centrar",type: 'date-eu'},
                        {sClass : "centrar"},
                        {sClass : "monedaR",type: 'formatted-num'},
                        {sClass : "monedaR",type: 'formatted-num'}
         ]
	});
    initialize();

    /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }

    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
        $('#release-wrapper').animate({
            marginTop: '0px'
        }, 600, 'linear');
    }, function () {
        $('#release-wrapper').animate({
            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
        }, 600, 'linear');
    });

    $('#download a').mousedown(function () {
        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
    });

    $('.collapser').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

        return false;
    });
    $('.collapser_search').click(function (event) {
        event.preventDefault();
        $(this).parents('.title_section').next().slideToggle();
        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        if ($(this).text() == "Ocultar opciones de búsqueda") {
            $(this).text("Mostrar opciones de búsqueda");
        } else {
            $(this).text("Ocultar opciones de búsqueda");
        }
        return false;
    });
    $('#cargarTabla').submit(function (event) {
    	//$("#tblDiaria > tbody").empty();
        //$("#tblDiaria").trigger("update");
        obtenerDatos();
        event.preventDefault();
        $('.collapser_search').parents('.title_section').next().slideToggle();
        $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
        $('.collapser_search').toggleClass('active');
        if ($('.collapser_search').text() == "Ocultar opciones de búsqueda") {
            $('.collapser_search').text("Ocultar opciones de búsqueda");
        } else {
            $('.collapser_search').text("Mostrar opciones de búsqueda");
        }
        seguimientoBusqueda();
        return false;
    });

	$('#limpiar').click(function (event) {
		event.preventDefault();
		$('input[type=text]').val('');
		$('select').val('0');
	});
	return false;
});
function render() {
    //
}

function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";
	if ($('#fcontratacionDe').val() !== "" && $('#fcontratacionDe').val() !== undefined){
		cadenaFiltros += " fcontratacionDe: "+ $('#fcontratacionDe').val();
	}
	if ($('#fcontratacionA').val() !== "" && $('#fcontratacionA').val() !== undefined){
		cadenaFiltros += " fcontratacionA: "+ $('#fcontratacionA').val();
	}
	if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined){
		cadenaFiltros += " fliquidacionDe: "+ $('#fliquidacionDe').val();
	}
	if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined ){
		cadenaFiltros += " fliquidacionA: "+ $('#fliquidacionA').val();
	}
	if ($('#cdisin').val() !== "" && $('#cdisin').val() !== undefined){
		cadenaFiltros += " isin: "+ $('#cdisin').val();
	}
	if ($('#cdcamara').val() !== "" && $('#cdcamara').val() !== undefined){
		cadenaFiltros += " camara: "+ $('#cdcamara').val();
	}
	if ($('#sentido').val() !== "" && $('#sentido').val() !== undefined){
		cadenaFiltros += " sentido: "+ $('#sentido').val();
	}
	if ($('#estado').val() !== "" && $('#estado').val() !== undefined){
		cadenaFiltros += " estado: "+ $('#estado').val();
	}
		$('.mensajeBusqueda').append(cadenaFiltros);

}

function obtenerDatos() {
    var params = {"service": "SIBBACServiceConciliacionDiaria",
        "action": "getOperacionesDiaria",
        "filters" : {"isin": getIsin(),
            "camara": getCamara(),
            "sentido": $("#sentido").val(),
            "estado": $("#estado").val(),
            "fcontratacionDe": transformaFechaInv($("#fcontratacionDe").val()),
            "fcontratacionA": transformaFechaInv($("#fcontratacionA").val()),
            "fliquidacionDe": transformaFechaInv($("#fliquidacionDe").val()),
            "fliquidacionA": transformaFechaInv($("#fliquidacionA").val())}

    };
    $("#tblDiaria > tbody").html("");
    consultar(params);

}

function cargarFechasIni(){

	    var params = {"service": "SIBBACServiceConciliacionDiaria",
        "action": "getFechaInicial"};
//		$("#tblDiaria > tbody").html("");
    	consultarFechas(params);

}

//llamada para cargar datos combos
function cargaCombos(){

	 var llamada2 = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getIsin"
			   };
	 var llamada3 = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getCamara"
			   };
	 //limpio los combos

	 $("#cdisin").html("");
	 $("#cdcamara").html("");

	 //cargo isin
	 cargaDatosCombo(llamada2);
	 //cargo camara
	 cargaDatosCombo(llamada3);
}

///carga datos combos
function cargaDatosCombo(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {

            var datosIsin = undefined;
            var datosCamara = undefined;
            if( json !== undefined || json.resultados !== undefined){


            if (json.resultados.result_isin === undefined ) {
                datosIsin={};
             } else {
            	  datosIsin = json.resultados.result_isin;
             }

            if (json.resultados.result_camara === undefined ) {
                datosCamara={};
             } else {
            	 datosCamara = json.resultados.result_camara;
             }
            }else{
            	return false;
            }



            $.each(datosIsin, function (key, val) {

            		listaIsin.push(datosIsin[key].codigo);
    				ponerTag = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
    				availableTags.push(ponerTag);

    			//código de autocompletar

               $( "#cdisin" ).autocomplete({
                 source: availableTags
               });


            });

            $.each(datosCamara, function (key, val) {
				listaCamara.push(datosCamara[key].cdCodigo);
				ponerTagCamara = datosCamara[key].cdCodigo+" - "+datosCamara[key].nbDescripcion;
				availableTagsCamara.push(ponerTagCamara);
           });
          //código de autocompletar
            $( "#cdcamara" ).autocomplete({
                source: availableTagsCamara

            });
        }
    });
}



//exportar
function SendAsExport( format ) {
	var c = this.document.forms['diaria'];
	var f = this.document.forms["export"];
	console.log("Forms: "+f.name + "/"+c.name);
	f.action="/sibbac20back/rest/export." + format;
	var json = {
		    "service": "SIBBACServiceConciliacionDiaria",
		    "action": "getOperacionesDiaria",
	        "filters" : {"isin": getIsin(),
	            "camara": getCamara(),
	            "sentido": $("#sentido").val(),
	            "estado": $("#estado").val(),
	            "fcontratacionDe": transformaFechaInv($("#fcontratacionDe").val()),
	            "fcontratacionA": transformaFechaInv($("#fcontratacionA").val()),
	            "fliquidacionDe": transformaFechaInv($("#fliquidacionDe").val()),
	            "fliquidacionA": transformaFechaInv($("#fliquidacionA").val())}};
	if ( json!=null && json!=undefined && json.length==0 ) {
		alert("Introduzca datos");
	} else {
		f.webRequest.value=JSON.stringify(json);
		f.submit();
	}
}

function getIsin(){
	var isinCompleto = $('#cdisin').val();
	var guion=isinCompleto.indexOf("-");
	if (guion<0){
		return isinCompleto;
	}else{
		return isinCompleto.substring(0, guion);
	}
}


function getCamara(){
	var camaraCompleto = $('#cdcamara').val();
	var guion=camaraCompleto.indexOf("-");
	if (guion<0){
		return camaraCompleto;
	}else{
		return camaraCompleto.substring(0, guion);
	}
}

