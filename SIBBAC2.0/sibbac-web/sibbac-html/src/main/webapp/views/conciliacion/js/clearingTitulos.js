var listaIsin = [];
var availableTags = [];
var oTableD = undefined;
var oTableI = undefined;
function consultar(params) {
	inicializarLoading();
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(params),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
                // x.overrideMimeType("application/json;charset=UTF-8");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
        	var datos = undefined;
            var datosD = undefined;
            var datosI = undefined;

            if (json === undefined || json.resultados === undefined ||json.resultados === null||
                    json.resultados.derecha === undefined || json.resultados.izquierda === undefined){
                datos={};
                datosD={};
                datosI={};
                $.unblockUI();
                return false;
            }


            $("#ultFecha").html(transformaFecha(json.resultados.newestDate)+" "+json.resultados.newestHour);

            //tabla izquierda
            var tblIzquierda = $("#tblIzquierda > tbody");
			tblIzquierda.html("");
            var difClassI = '';
            var colDifI = '';
            datosI = json.resultados.izquierda;
            oTableD.fnClearTable();
			oTableI.fnClearTable();
			var rownum=0;
            $.each(datosI, function (key, val) {

                //fila tabla izquierda
            	colDifI = (datosI[key].iguales);
                difClassI = (colDifI === "false") ? 'colorFondo' : 'centrado';
                var sistema;
                if(datosI[key].sistemaLiq){
                	sistema = datosI[key].sistemaLiq;
                }else{
                	sistema = " ";
                }
				oTableI.fnAddData([
                                "<span>" +   datosI[key].isin + "</span>",
                                "<span>" +  datosI[key].descIsin + "</span>",
                                "<span>" +  datosI[key].titulos + "</span>",
                                "<span>" +  transformaFecha(datosI[key].settlementdate)+ "</span>",
                                "<span>" +  datosI[key].cuentaliq+ "</span>",
                                "<span>" +  sistema + "</span>",
                                "<span>" +  datosI[key].diferencia + "</span>"
             					]);
				$(oTableI.fnGetNodes(rownum)).addClass(difClassI);
				rownum++;

            });
            //tabla derecha
            var tblDerecha = $("#tblDerecha > tbody");
			$(tblDerecha).html("");
            var difClassD = '';
            var colDifD = '';
            datosD = json.resultados.derecha;
            rownum=0;
            $.each(datosD, function (key, val) {
			//fila tabla derecha
            colDifD = (datosD[key].iguales);
            difClassD = (colDifD === 'false') ? 'colorFondo' : 'centrado';
            oTableD.fnAddData([
                                    "<span>" +datosD[key].isin+ "</span>",
                                    "<span>" +  datosD[key].descIsin+ "</span>",
                                    "<span>" +  datosD[key].titulos+ "</span>",
                                    "<span>" +  transformaFecha(datosD[key].settlementdate)+ "</span>",
                                    "<span>" +  datosD[key].cuentaliq+ "</span>",
                                    "<span>" + datosD[key].diferencia+ "</span>"
             					]);
				$(oTableD.fnGetNodes(rownum)).addClass(difClassD);
				rownum++;

            });
            $.unblockUI();
        },
        error : function(){
        	$.unblockUI();
        }
    });
}
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {
    var fechas = ['fcontratacionDe','fcontratacionA','fliquidacionA','fliquidacionDe'];

	loadpicker(fechas);
	verificarFechas([['fcontratacionDe','fcontratacionA'],['fliquidacionDe','fliquidacionA']])
    cargarFechaActualizacionMovimientoAlias();
	cargaCombo();
	 $('#fliquidacionDe').val(getfechaHoy());
    obtenerDatos();
}

$(document).ready(function () {
	oTableI = $("#tblIzquierda").dataTable({
		"dom": 'T<"clear">lfrtip',
		"bSort": false,
		"tableTools": {
			"sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
		"language": {
			"url": "i18n/Spanish.json"
		},
        "scrollY": "480px",
        "scrollCollapse": true,
		"aoColumns" :[
                      {"sClass": "centrado"},
                      {"sClass": "centrado"},
                      {"sClass": "monedaR", "type": 'formatted-num'},
                      {"sClass": "centrado"},
                      {"sClass": "centrado"},
                      {"sClass": "centrado"},
                      {"sClass": "monedaR", "type": 'formatted-num'}
                      ]
	});
	oTableD = $("#tblDerecha").dataTable({
		"dom": 'T<"clear">lfrtip',
		"bSort": false,
		"tableTools": {
			"sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
		"language": {
			"url": "i18n/Spanish.json"
		},
        "scrollY": "480px",
        "scrollCollapse": true,
		"aoColumns" :[
                      {"sClass": "centrado"},
                      {"sClass": "centrado"},
                      {"sClass": "monedaR", "type": 'formatted-num'},
                      {"sClass": "centrado"},
                      {"sClass": "centrado"},
                      {"sClass": "monedaR", "type": 'formatted-num'}
                      ]
	});
	initialize();


        /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
        if ($('.contenedorTabla').length >= 1) {
            var anchoBotonera;
            $('.contenedorTabla').each(function (i) {
                anchoBotonera = $(this).find('table').outerWidth();
                $(this).find('.botonera').css('width', anchoBotonera + 'px');
                $(this).find('.resumen').css('width', anchoBotonera + 'px');
                $('.resultados').hide();
            });
        }

        /*Función para cambiar texto*/
        jQuery.fn.extend({
            toggleText: function (a, b) {
                var that = this;
                if (that.text() != a && that.text() != b) {
                    that.text(a);
                } else if (that.text() == a) {
                    that.text(b);
                } else if (that.text() == b) {
                    that.text(a);
                }
                return this;
            }
        });
        /*FIN Función para cambiar texto*/

        $('a[href="#release-history"]').toggle(function () {
    		$('#release-wrapper').animate({
    			marginTop: '0px'
    		}, 600, 'linear');
    	}, function () {
    		$('#release-wrapper').animate({
    			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
    		}, 600, 'linear');
    	});

    	$('#download a').mousedown(function () {
    		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
    	});

    	$('.collapser').click(function (event) {
    		event.preventDefault();
    		$(this).parents('.title_section').next().slideToggle();
    		$(this).toggleClass('active');
    		$(this).toggleText('Clic para mostrar','Clic para ocultar');

    		return false;
    	});
    	$('.collapser_search').click(function(event){
    		event.preventDefault();
    		$(this).parents('.title_section').next().slideToggle();
    		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
    		$(this).toggleClass('active');
    		if ( $(this).text()=="Ocultar opciones de búsqueda" ) {
    			$(this).text( "Mostrar opciones de búsqueda" );
    		} else if ( $(this).text()=="Mostrar opciones de búsqueda" ) {
    			$(this).text( "Ocultar opciones de búsqueda" );
    		}else{
    			$(this).text( "Mostrar opciones de búsqueda" );
    		}
    		return false;
    	});
    	$('#cargarTabla').submit(function(event){
		    event.preventDefault();

			if (!validar()){
				return false;
			}
			else{

				$("#tblIzquierda > tbody").empty();
				//$("#tblIzquierda").trigger("update");
				$("#tblDerecha > tbody").empty();
				//$("#tblDerecha").trigger("update");
				if($('#fliquidacionDe').val() !== undefined && $('#fliquidacionDe').val() !=="" && $("#selectCuenta option:selected").val() != "0"){
					obtenerDatos();

		    		$('.collapser_search').parents('.title_section').next().slideToggle();
		    		$('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
		    		$('.collapser_search').toggleClass('active');
		    		if ( $('.collapser_search').text()=="Ocultar opciones de búsqueda" ) {
		    			$('.collapser_search').text( "Mostrar opciones de búsqueda" );
		    		} else if ( $('.collapser_search').text()=="Mostrar opciones de búsqueda" ) {
		    			$('.collapser_search').text( "Ocultar opciones de búsqueda" );
		    		}else{
		    			$('.collapser_search').text( "Mostrar opciones de búsqueda" );
		    		}
					seguimientoBusqueda();
			}else{
				alert("Se deben informar todos los campos obligatorios.");
			}

    		return false;
			}
    	});
    	$('#limpiar').click(function (event) {
		event.preventDefault();
		$('input[type=text]').val('');
		$('select').val('0');
	});

});
function render() {
    //
}
function getCuadrados(){
	var cuadrados;
	if ( $("#todos").is(":checked") ) {
		cuadrados = $("#todos").val();
	}else if( $("#cuadrados").is(":checked") ){
		cuadrados = $("#cuadrados").val();
	}else if( $("#desCuadrados").is(":checked") ){
		cuadrados = $("#desCuadrados").val();
	}
	return cuadrados;
}

function obtenerDatos(){
	var params = {"service": "SIBBACServiceConciliacionClearing",
		     	   "action": "getDiferenciasS3",
		     	   "filters" :{
		                     "fliquidacionDe" : transformaFechaInv($("#fliquidacionDe").val()),
		                     "fliquidacionA" : transformaFechaInv($("#fliquidacionA").val()),
		                     "idCuentaLiq" : $("#selectCuenta option:selected").val(),
		                     "isin" : getIsin(),
		                     "cuadrados": getCuadrados()}

	 			  };
	    $("#tblDerecha > tbody").html("");
	    $("#tblIzquierda > tbody").html("");
	    $("#ultFecha").html("");
		consultar(params);
}
//para ver el filtro al minimizar la búsqueda
function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";

	if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined){
		cadenaFiltros += " Fecha Liquidaci&oacute;n Desde: "+ $('#fliquidacionDe').val();
	}
	if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined){
		cadenaFiltros += " Fecha Liquidaci&oacute;n Hasta: "+ $('#fliquidacionA').val();
	}
	if ($('#isin').val() !== "" && $('#isin').val() !== undefined){
		cadenaFiltros += " isin: "+ $('#isin').val();
	}
	if($('#selectCuenta').val() !== ""){
		cadenaFiltros += " Cuenta: " + $('#selectCuenta option:selected').text();
	}
	cadenaFiltros += " Cuadrados: "+ getCuadrados();
	$('.mensajeBusqueda').append(cadenaFiltros);
}

// llamada para cargar datos combo
function cargaCombo(){
	 var llamada = {"service": "SIBBACServiceConciliacionClearing",
		     	   "action": "getCuentasLiquidacion"
	 			   };
	 var llamada2 = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getIsin"};
	 $("#selectCuenta").html("");
	 $("#selectCuenta").append("<option value=\"\">Seleccione  opcion</option>");
	 $("#isin").html("");
	 cargaDatosCombo(llamada);
	 cargaDatosCombo(llamada2);

}

//carga datos combos
function cargaDatosCombo(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datosCL = undefined;
            var datosIsin = undefined;

            if( json !== undefined || json.resultados !== undefined){
             if(json.resultados.cuentasLiquidacion === undefined ) {
               datosCL={};
             } else {
                datosCL = json.resultados.cuentasLiquidacion;
             }

            if (json.resultados.result_isin === undefined ) {
                datosIsin={};
             } else {
            	  datosIsin = json.resultados.result_isin;
             }


            }else{
            	return false;
            }


            $.each(datosCL, function (key, val) {
            	$("#selectCuenta").append('<option value='+datosCL[key].id+'>'+datosCL[key].name+'</option>');
            });

            $.each(datosIsin, function (key, val) {

            		listaIsin.push(datosIsin[key].codigo);
    				ponerTag = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
    				availableTags.push(ponerTag);

    			//código de autocompletar

               $( "#isin" ).autocomplete({
                 source: availableTags
               });


            });

        }
    });
}

//exportar
function SendAsExport( format ) {
	var c = this.document.forms['clearingtitulos'];
	var f = this.document.forms["export"];
	console.log("Forms: "+f.name + "/"+c.name);
	f.action="/sibbac20back/rest/export." + format;
	var json = {
		    "service": "SIBBACServiceConciliacionClearing",
		    "action": "getDiferenciasS3",
	     	   "filters" :{
	                     "fliquidacionDe" : transformaFechaInv($("#fliquidacionDe").val()),
	                     "fliquidacionA" : transformaFechaInv($("#fliquidacionA").val()),
	                     "idCuentaLiq" : $("#selectCuenta option:selected").val(),
	                     "isin" : getIsin()}};
	if ( json!=null && json!=undefined && json.length==0 ) {
		alert("Introduzca datos");
	} else {
		f.webRequest.value=JSON.stringify(json);
		f.submit();
	}
}


function getIsin(){
	var isinCompleto = $('#isin').val();
	var guion=isinCompleto.indexOf("-");
	if (guion<0){
		return isinCompleto;
	}else{
		return isinCompleto.substring(0, guion);
	}
}

function validar(){
	var campos = $('input[campo="obligatorio"]');
	if (campos.length===0){
		return true;
	}
	else{
		var valido = true;
		for(var i =0; i<campos.length; i++){
			if ($(campos[i]).val()===''){
				$(campos[i]).addClass('error');
				valido=false;
			}
		}
		if(!valido){
			alert('Complete todos los campos');
			return false;
		}
		else{
			$("input[campo='obligatorio']").removeClass('error');
			return true;
		}

	}
}





