var listaIsin = [];
var availableTags = [];
var listaAlias = [];
var availableTagsAlias = [];
var oTable = undefined;
function consultar(params) {

	var data = new DataBean();
	data.setService('SIBBACServiceConciliacionClearing');
	data.setAction('getBalanceMovimientosCuentaVirtualPorIsin');
	data.setFilters(params);
	var request = requestSIBBAC( data );

     request.success(function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
            		json.resultados === null ||
                    json.resultados.result_balance_clearing === undefined) {
                datos={};
            } else {
                datos = json.resultados.result_balance_clearing;
                var errors = cargarDatos(datos);
            }

        });
     request.error(function(){
    	 $.unblockUI();
     });

}
function cargarDatos(datos){
	var tbl = $("#tblClearingTitulosValor > tbody");
    $(tbl).html("");
    var srcValid = "img/activo.png";
    var srcWarning = "img/desactivo.png";
	oTable.fnClearTable();
	var lineaMostrada = false;
	var cabeceraMostrada = false;
	var totalEfectivoTag = "";
	var totalTitulosTag = "";
	var totalTitulos = 0;
	//
	var isin = "";
	var descisin = "";
	var alias = "";
	var descAlias ="";
	var titulos = 0;
	var efectivo = 0;
	var src = "";
	//
    $.each(datos, function (key, val) {
        lineaMostrada = false;
        cabeceraMostrada = false;
        totalEfectivoTag = "";
        totalTitulosTag = "";
        totalTitulos = 0;
        //
    	var grupo = val.grupoBalanceISINList;
        if(grupo !== null && grupo !== undefined){
	        isin = (val.isin !== null && val.isin !== undefined) ? val.isin : '';
        	descisin = (val.descrisin !== null && val.descrisin !== undefined) ? val.descrisin : '';
        	alias = (grupo[0].alias !== "" && grupo[0].alias !== null) ? grupo[0].alias : grupo[0].descAli;
	        descAlias = (grupo[0].descAli !== "" && grupo[0].descAli !== null) ? grupo[0].descAli : grupo[0].alias;
	        titulos = (grupo[0].titulos!=null && grupo[0].titulos!=undefined )?grupo[0].titulos:'';
	        totalTitulos += titulos;
	        efectivo = (grupo[0].efectivo !== null && grupo[0].efectivo !== undefined) ? grupo[0].efectivo : "";
	        src = (grupo[0].valido === false) ?srcWarning : srcValid;
	        if(titulos !== 0 || efectivo !==0){
	        	lineaMostrada = true;
	        	cabeceraMostrada = true;
	        oTable.fnAddData([
		  				  isin,
		  				  descisin,
		  				  alias,
		  				  descAlias,
		  				  $.number(titulos,0,',','.'),
		  				  $.number(efectivo, 2, ',', '.')
     					], false);
	        }
	        var i = 1;
	        $.each(grupo, function(indice, valor){

	            if(i < grupo.length){
	            	alias = (grupo[i].alias !== "" && grupo[i].alias !== null) ? grupo[i].alias : "";
	            	descAlias = (grupo[i].descAli !== "" && grupo[i].descAli !== null) ? grupo[i].descAli : grupo[i].alias;
	     	        titulos = (grupo[i].titulos!=null && grupo[i].titulos!=undefined )?grupo[i].titulos:'';
	     	        totalTitulos +=titulos;
	     	        efectivo = (grupo[i].efectivo !== null && grupo[i].efectivo !== undefined) ? grupo[i].efectivo : "";
	            	src = (grupo[i].valido === false) ? srcWarning : srcValid;
	            	if(titulos !== 0 || efectivo !==0){
	            		lineaMostrada = true;

	            	oTable.fnAddData([
					                  ((!cabeceraMostrada) ? isin : ''),
					                  ((!cabeceraMostrada) ? descisin : ''),
					                  alias,
					                  descAlias,
					                  $.number(titulos, 0, ',', '.'),
					                  $.number(efectivo, 2, ',', '.')
	     					], false);
	            	}
	                i++;
            }
        });
	    if(lineaMostrada && (totalTitulos !== 0 || val.totalEfectivos !== 0)){
	    	totalTitulosTag = "<div class=\"negrita celda-tabla-245\"><span class=\"total-efectivo-label\">"+
            "Total T&iacute;tulos: </span><span style=\"float:right;\">" + $.number(totalTitulos, 0, ',','.') + "</span></div>"
	    	totalEfectivoTag = "<div class=\"negrita celda-tabla-245\"><span class=\"total-efectivo-label\">"+
            "Total Efectivo: </span><span style=\"float:right;\">" + $.number(val.totalEfectivos, 2, ',','.') + "</span></div>";
	    	oTable.fnAddData([
						  '',
                          '',
                          '',
                          '',
                          (totalTitulos !== 0) ? totalTitulosTag : '',
                          (val.totalEfectivos !== 0) ? totalEfectivoTag : ''
     					],false);
        	}
        }
    });
    oTable.fnDraw();
    return false;
}
/////////////////////////////////////////////////
////// SE EJECUTA NADA MÁS CARGAR LA PÁGINA ////
function initialize() {
	var fechas = ['fecha'];
	loadpicker(fechas);
	$('#fecha').val(getfechaHoy());
	cargarFechaActualizacionMovimientoAlias();
	cargaCombo();
	cargarAlias();
    //obtenerDatos();
}

function cargaCombo(){

	 var llamada = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getIsin"};
	 $("#isin").html("");
	 cargaDatosCombo(llamada);
	 var selectCuentaCall = {"service": "SIBBACServiceConciliacionClearing",
	     	   "action": "getCuentasLiquidacionClearing"
			   };

		$("#selectCuenta").html("");
		//$("#selectCuenta").append("<option value='0'>Todas</option>");
		cargaDatosCombo(selectCuentaCall);

}
$(document).ready(function () {
	oTable = $("#tblClearingTitulosValor").dataTable({
		"dom": 'T<"clear">lfrtip',
		"bSort": false,
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "deferRender":true,/*solo renderiza los registros que se muestran*/
         "language": {
             "url": "i18n/Spanish.json"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "aoColumns" :[
          {"sClass": "align-left"},
          {"sClass": "align-left"},
          {"sClass": "align-left"},
          {"sClass": "align-left"},
          {"sClass": "align-right", "type": 'formatted-num'},
          {"sClass": "align-right", "type": 'formatted-num'}
          ]
	});


    /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }

    /*Eva:Función para cambiar texto*/
    jQuery.fn.extend({
        toggleText: function (a, b) {
            var that = this;
            if (that.text() != a && that.text() != b) {
                that.text(a);
            } else if (that.text() == a) {
                that.text(b);
            } else if (that.text() == b) {
                that.text(a);
            }
            return this;
        }
    });
    /*FIN Función para cambiar texto*/

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function (event) {
		event.preventDefault();
		$(this).parents('.title_section').next().slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function(event){
		event.preventDefault();
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		if ( $(this).text()=="Ocultar opciones de búsqueda" ) {
			$(this).text( "Mostrar opciones de búsqueda" );
		} else {
			$(this).text( "Ocultar opciones de búsqueda" );
		}
		return false;
	});
	$('#cargarTabla').submit(function(event){
		event.preventDefault();

		$("#tblClearingTitulosValor > tbody").empty();
		//$("#tblClearingTitulosValor").trigger("update");
        obtenerDatos();
		$('.collapser_search').parents('.title_section').next().slideToggle();
		$('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
		$('.collapser_search').toggleClass('active');
		if ( $('.collapser_search').text().indexOf('Ocultar')!==-1 ) {
			$('.collapser_search').text( "Mostrar opciones de búsqueda" );
		} else {
			$('.collapser_search').text( "Ocultar opciones de búsqueda" );
		}
		seguimientoBusqueda();
		return false;
	});

	$('#limpiar').click(function (event) {
		event.preventDefault();
		$('input[type=text]').val('');
		$('select').val('0');
	});

	initialize();
});
function render() {
	//
}


function obtenerDatos(){

	 var params = {"isin" : getIsin(),
			 "alias" : getAliasId(),
			 "fecha": transformaFechaInv($("#fecha").val()),
			 "cdcodigocuentaliq":$("#selectCuenta").val()
			 };
	    $("#tblClearingTitulosValor > tbody").html("");
		consultar(JSON.stringify(params));
}
//para ver el filtro al minimizar la búsqueda
function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";
	if ($('#isin').val() !== "" && $('#isin').val() !== undefined){
		cadenaFiltros += " isin: "+ $('#isin').val();
	}
	if ($('#alias').val() !== "" && $('#alias').val() !== undefined ){
		cadenaFiltros += " alias: "+ $('#alias').val();
	}
	if ($('#fecha').val() !== "" && $('#fecha').val() !== undefined ){
		cadenaFiltros += " fecha: "+ $('#fecha').val();
	}
	if ($('#selectCuenta').val() !== "" && $('#selectCuenta').val() !== undefined && $('#selectCuenta').val() !== '0' ){
		cadenaFiltros += " cuenta: "+ $('#selectCuenta option:selected').html();
	}
	$('.mensajeBusqueda').append(cadenaFiltros);
}

//carga datos combos
function cargaDatosCombo(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datosIsin = undefined;
            var datos = {};
            if( json !== undefined || json.resultados !== undefined){
            	 if (json !== undefined && json.resultados !== undefined &&
                         json.resultados.result_cuentas_liquidacion_clearing !== undefined) {
            		 datos = json.resultados.result_cuentas_liquidacion_clearing;
            		 cargarCuentasLiquidacion(datos);
                 } else if (json.resultados.result_isin !== undefined ) {
                	 datosIsin = json.resultados.result_isin;
	            	 cargarAutocompleteIsin(datosIsin);
	             }
            }else{
            	return false;
            }
        }
    });
}
function cargarCuentasLiquidacion(datos){
	 $("#selectCuenta").empty();
     $("#selectCuenta").append("<option value=\"\">Seleccione una Cuenta</option>" );
     $.each(datos, function (key, val) {
     	$("#selectCuenta").append('<option value='+val.cdCodigo+'>'+val.cdCodigo+'</option>');
     });
}
function cargarAutocompleteIsin(datosIsin){
	var isines = new Array(datosIsin.length);
	var tagItem = "";
	$.each(datosIsin, function (key, val) {
		tagItem = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
		isines[key] = tagItem;
	});
	$( "#isin" ).autocomplete({
		   source: isines
	});
}

function getIsin(){
	var isinCompleto = $('#isin').val();
	var guion=isinCompleto.indexOf("-");
	if (guion > 0){
		return isinCompleto.substring(0, guion);
	}else{
		return isinCompleto;
	}
}

function cargarAlias()
{
	var params = new DataBean();
	params.setService('SIBBACServiceTmct0ali');
	params.setAction('getAlias');
	var request = requestSIBBAC(params);
	request.success(function(json) {
		var datos = undefined;

		if (json === undefined || json.resultados === undefined
				|| json.resultados.result_alias === undefined) {
			datos = {};
		} else {
			datos = json.resultados.result_alias;
			var item = null;

			for ( var k in datos) {
				item = datos[k];
				listaAlias.push(item.alias);
				ponerTag = item.alias + " - " + item.descripcion;
				availableTagsAlias.push(ponerTag);

			}
			// código de autocompletar

			$("#alias").autocomplete({
				source : availableTagsAlias
			});
		}
		// fin de código de autocompletar
	});
	request.error( function(c) {
			console.log( "ERROR: " + c );
		});
	}


function getAliasId() {
	var alias = $('#alias').val();
	var guion = alias.indexOf("-");
	return (guion > 0) ? alias.substring(0, guion) : alias;
}
