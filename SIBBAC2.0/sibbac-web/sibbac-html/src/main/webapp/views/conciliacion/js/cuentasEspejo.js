var listaIsin = [];
var availableTags = [];
var oTable = undefined;
function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}
function consultar(params) {
	inicializarLoading();
	$.ajax({
		type : 'POST',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		url : "/sibbac20back/rest/service",
		data : JSON.stringify(params),
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers',
					'Origin, X-Requested-With, Content-Type, Accept')
		},
		success : function(json) {
			var datos = undefined;
			if (json === undefined ||
					json.resultados === undefined ||
					json.resultados === null ||
					json.resultados.result_cuentas_virtuales === undefined) {
				datos = {};
			} else {
				datos = json.resultados.result_cuentas_virtuales;
				oTable.fnClearTable();

				//
				var cdcodigocuentaliq = "";
				var isin = "";
				var descrIsin = "";
				var sistemaLiq = "";
				var descOperacion = "";
				var cdoperacion = "";
				var signoAnotacion = "";
				var fcontratacion = "";
				var fliquidacion = "";
				var titulos = "";
				var efectivo = "";
				var corretaje = "";
				var acumTitulos = 0;
				var acumEfectivo = 0;
				var auditUser = "";
				var auditDate = "";
				$.each(datos, function(key, val) {
					cdcodigocuentaliq = (datos[key].cdcodigocuentaliq !== null) ? datos[key].cdcodigocuentaliq : "";
					isin = (datos[key].isin !== null) ? datos[key].isin : "";
					descrIsin = (datos[key].descrIsin != null) ? datos[key].descrIsin : "";
					sistemaLiq = (datos[key].sistemaLiq !== null) ? datos[key].sistemaLiq : "";
					cdoperacion = (datos[key].cdoperacion !== null) ? datos[key].cdoperacion : "";
					descOperacion = (datos[key].descripcionOperacion !== null) ? datos[key].descripcionOperacion : "";
					signoAnotacion = (datos[key].signoAnotacion !== null) ? datos[key].signoAnotacion : "";
					fcontratacion = (datos[key].fcontratacion !== null) ? transformaFecha(datos[key].fcontratacion) : "";
					fliquidacion = (datos[key].fliquidacion !== null) ? transformaFecha(datos[key].fliquidacion) : "";
					titulos = (datos[key].titulos !== null) ? datos[key].titulos : "0";
					titulos = $.number(titulos, 0, ',','.');
					efectivo = (datos[key].efectivo !== null) ? a2digitos(datos[key].efectivo) : "0,00";
					/*corretaje = (datos[key].corretaje !== null) ? a2digitos(datos[key].corretaje) : "0,00";*/
 					acumTitulos = (cdoperacion.trim() === "Sald.Inicial") ? (datos[key].titulos) : (acumTitulos + (datos[key].titulos));
 					acumTitulos = $.number(acumTitulos,0,',','.');
 					acumEfectivo = (cdoperacion.trim() === "Sald.Inicial") ? (datos[key].efectivo) : (acumEfectivo + (datos[key].efectivo));
 					auditUser = (datos[key].auditUser !== null) ? datos[key].auditUser : "";
 					auditDate = (datos[key].auditDate !== null) ? transformaFecha(datos[key].auditDate) : "";
					var obj = oTable.fnAddData([
					        cdcodigocuentaliq,
					        isin,
					        descrIsin,
					        sistemaLiq,
					        cdoperacion,
					        descOperacion,
					        signoAnotacion,
					        fcontratacion,
					        fliquidacion,
					        titulos,
					        efectivo,
					        /*corretaje,*/
					        ((cdoperacion.trim() !== "Sald.Inicial" && cdoperacion.trim() !== "Sald.Final") ? acumTitulos : ''),
					        ((cdoperacion.trim() !== "Sald.Inicial" && cdoperacion.trim() !== "Sald.Final") ? a2digitos(round(acumEfectivo, 2)) : '')]);
					var row = oTable.fnSettings().aoData[ obj[0] ].nTr;
					var aData = $("td", row);

					if ( $(aData[4]).text().trim() === "Sald.Inicial")
                    {
						//Cambia el color del fondo a nivel de fila
						$(row).css("background-color", "#E5F5F9!important");
						$(row).css("font-weight", "bold");
						//cambia el color del fondo a nivel de columna
						//$('td', row).css('background-color', '#ddeeee');
                    }
                    else if ( $(aData[4]).text().trim() === "Sald.Final")
                    {
                    	$(row).css("background-color", "#D8EFF7!important");
                    	$(row).css("border-bottom", "1px solid #C00");
                    	$(row).css("font-weight", "bold");
                    	//$('td', row).css('background-color', '#ccccee');
                    }else if( $(aData[4]).text().trim() === "MAN" ){
                    	var popups = "";
                    	var popupsId = "popupsId-"+key;
                    	popups += "<div id='"+ popupsId +"' class='tep'>";
                    	popups += "<span class='negrita subrayado'>Usuario Alta Movimiento: </span>";
                    	popups += "<span class='negrita'>"+auditUser+"</span><br>";
                    	popups += "<span class='negrita subrayado'>Fecha Alta Movimiento: </span>";
                    	popups += "<span class='negrita'>"+auditDate+"</span><br>";
                    	popups += "<span class='negrita subrayado'>Importe titulos: </span>";
                    	popups += "<span class='negrita'>"+titulos+"</span><br>";
                    	popups += "<span class='negrita subrayado'>Importe efectivo: </span>";
                    	popups += "<span class='negrita'>"+efectivo+"</span><br>";
                    	popups += "<span class='negrita subrayado'>Isin: </span>";
                    	popups += "<span class='negrita'>"+isin+"</span><br>";
                    	popups += "<span class='negrita subrayado'>F. contratacion: </span>";
                    	popups += "<span class='negrita'>"+fcontratacion+"</span><br>";
                    	popups += "<span class='negrita subrayado'>F. Liquidacion: </span>";
                    	popups += "<span class='negrita'>"+fliquidacion+"</span><br>";
                    	popups += "</div>";
                    	$(row).append(popups);
                    	$(row).click(function(){
                    		$("#"+popupsId).bPopup({
                                onOpen: function() { $(row).css("background-color", "#FE2E2E!important"); },
                                onClose: function() { $(row).css("background-color", "") }
                            });
                    	});
                    }
				});

			}
			$.unblockUI();
		},
		error : function(err){
			$.unblockUI();
		}

	});
}
// ///////////////////////////////////////////////
// ////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {

	var fechas = [ 'fliquidacionA', 'fliquidacionD' ];
	loadpicker(fechas);
	 verificarFechas([['fliquidacionD','fliquidacionA']])
	 cargarFechaActualizacionMovimientoAlias();
	 cargaCombos();

	 getFechaPrevia();
	// obtenerDatos();

}
$(document)
		.ready(
				function() {
					initialize();

					/*
					 * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS
					 * BOTONES DE LA TABLA
					 */
					if ($('.contenedorTabla').length >= 1) {
						var anchoBotonera;
						$('.contenedorTabla').each(
								function(i) {
									anchoBotonera = $(this).find('table')
											.outerWidth();
									$(this).find('.botonera').css('width',
											anchoBotonera + 'px');
									$(this).find('.resumen').css('width',
											anchoBotonera + 'px');
									$('.resultados').hide();
								});
					}

					/* Eva:Función para cambiar texto */
					jQuery.fn.extend({
						toggleText : function(a, b) {
							var that = this;
							if (that.text() != a && that.text() != b) {
								that.text(a);
							} else if (that.text() == a) {
								that.text(b);
							} else if (that.text() == b) {
								that.text(a);
							}
							return this;
						}
					});
					/* FIN Función para cambiar texto */

					$('a[href="#release-history"]').toggle(
							function() {
								$('#release-wrapper').animate({
									marginTop : '0px'
								}, 600, 'linear');
							},
							function() {
								$('#release-wrapper').animate(
										{
											marginTop : '-'
													+ ($('#release-wrapper')
															.height() + 20)
													+ 'px'
										}, 600, 'linear');
							});

					$('#download a').mousedown(
							function() {
								_gaq.push([ '_trackEvent', 'download-button',
										'clicked' ])
							});

					$('.collapser').click(
							function(event) {
								event.preventDefault();
								$(this).parents('.title_section').next()
										.slideToggle();
								$(this).toggleClass('active');
								$(this).toggleText('Clic para mostrar',
										'Clic para ocultar');

								return false;
							});
					$('.collapser_search')
							.click(
									function(event) {
										event.preventDefault();
										$(this).parents('.title_section')
												.next().slideToggle();
										$(this).parents('.title_section')
												.next().next('.button_holder')
												.slideToggle();
										$(this).toggleClass('active');
										if ($(this).text() == "Ocultar opciones de búsqueda") {
											$(this)
													.text(
															"Mostrar opciones de búsqueda");
										} else if ($(this).text() == "Mostrar opciones de búsqueda") {
											$(this)
													.text(
															"Ocultar opciones de búsqueda");
										} else {
											$(this)
													.text(
															"Mostrar opciones de búsqueda");
										}
										return false;
									});
					$('#cargarTabla').submit(function(event) {
						if ($("#fliquidacionD").val() === ""
							|| $("#selectCuenta option:selected").val() === 0
							|| $("#selectCuenta option:selected").val() === "0") {
						alert("Por favor cumplimente los filtros con (*)");
						return false;
					}
						obtenerDatos();
						event.preventDefault();
						$('.collapser_search').parents('.title_section').next().slideToggle();
						$('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
						$('.collapser_search').toggleClass('active');
						if ($('.collapser_search').text() == "Ocultar opciones de búsqueda") {
							$('.collapser_search').text("Mostrar opciones de búsqueda");
						} else if ($('.collapser_search').text() == "Mostrar opciones de búsqueda") {
							$('.collapser_search').text("Ocultar opciones de búsqueda");
						}else{
							$('.collapser_search').text("Mostrar opciones de búsqueda");
						}
						seguimientoBusqueda();
						return false;
					});
					$('#limpiar').click(function(event) {
						event.preventDefault();
						$('input[type=text]').val('');
						$('select').val('0');
					});
					oTable = $("#tblCuentaVirtual").dataTable({
										"dom" : 'T<"clear">lfrtip',
										"language" : {
											"url" : "i18n/Spanish.json"
										},
										"scrollX":        "auto",
										"pageLength" : 20,
										"lengthMenu": [ 20, 25, 50, 75, 100 ],
										"tableTools" : {
											"sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
										},
										"order":[],
										"columnDefs": [ {
										      "targets"  : 'no-sort',
										      "orderable": false,
										    }],
										"aoColumns" : [
										{sClass : "centrar"},
										{sClass : "monedaR",type:"formatted-num"},
										{sClass : "aleft"},
										{sClass : "aleft"},
										{sClass : "aleft"},
										{sClass : "centrar"},
										{sClass : "centrar"},
										{sClass : "centrar"},
										{sClass : "centrar"},
										{sClass : "align-right",type: 'date-eu'},
										{sClass : "align-right",type:"date-eu"},
										{sClass : "monedaR",type:"formatted-num"},
										{sClass : "monedaR",type:"formatted-num"}
										],
						                "scrollY": "480px",
						                "scrollCollapse": true,
										"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
						                   /*Esta funcion se llama una vez cargada la tabla
											if ( aData[6] == "Sald.Inicial" )
						                    {
												$('td', nRow).css('background-color', '#ddeeee');
						                    }
						                    else if ( aData[6] == "Sald.Final" )
						                    {
						                    	 $('td', nRow).css('background-color', '#ccccee');
						                    }*/
										}
									});

				});
function render() {
	//
}

function obtenerDatos() {

	var params = {
		"service" : "SIBBACServiceMovimientoVirtual",
		"action" : "getMovimientosEspejo",
		"filters" : {
			"isin" : getIsin(),
			/* "aliasDes": getAliasDes(), */
			"cdcodigocuentaliq" : ($("#selectCuenta option:selected").val() !== 0 && $(
					"#selectCuenta option:selected").val() !== "0") ? $(
					"#selectCuenta option:selected").val() : "",
			"fliquidacionDe" : transformaFechaInv($("#fliquidacionD").val()),
			"fliquidacionA" : transformaFechaInv($("#fliquidacionA").val())
		}
	};
	consultar(params);
}

// llamada para cargar datos combos
function cargaCombos() {
	var llamada = {
		"service" : "SIBBACServiceConciliacionClearing",
		"action" : "getCuentasLiquidacion"
	};
	var llamada2 = {
		"service" : "SIBBACServiceConciliacion",
		"action" : "getIsin"
	};

	// limpio los combos
	$("#selectCuenta").html("");
	$("#cdisin").html("");
	// cargo cuenta liquidacion
	$("#selectCuenta").append(
			"<option value=\"\">Seleccione una opci&oacute;n</option>");

	cargaDatosCombo(llamada);
	// cargo isin
	cargaDatosCombo(llamada2);
}

// carga datos combos
function cargaDatosCombo(llamada) {
	$.ajax({
		type : 'POST',
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		url : "/sibbac20back/rest/service",
		data : JSON.stringify(llamada),
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers',
					'Origin, X-Requested-With, Content-Type, Accept')
		},
		success : function(json) {
			var datosCL = undefined;
			var datosIsin = undefined;

			if (json !== undefined || json.resultados !== undefined) {
				if (json.resultados.cuentasLiquidacion === undefined) {
					datosCL = {};
				} else {
					datosCL = json.resultados.cuentasLiquidacion;
				}

				if (json.resultados.result_isin === undefined) {
					datosIsin = {};
				} else {
					datosIsin = json.resultados.result_isin;
				}

			} else {

				return false;
			}

			$.each(datosCL, function(key, val) {
				$("#selectCuenta").append(
						'<option value=' + datosCL[key].name + '>'
								+ datosCL[key].name + '</option>');
			});

			$.each(datosIsin, function(key, val) {

				listaIsin.push(datosIsin[key].codigo);
				ponerTag = datosIsin[key].codigo + " - "
						+ datosIsin[key].descripcion;
				availableTags.push(ponerTag);

				// código de autocompletar

				$("#cdisin").autocomplete({
					source : availableTags
				});

			});

		}
	});
}
function getFechaPrevia() {
	var params = new DataBean();
	params.setService('SIBBACServiceMovimientoVirtual');
	params.setAction('getFechaPreviaLiquidacion');
	var request = requestSIBBAC(params);
	request.success(function(json) {
		var datos = undefined;

		if (json === undefined || json.resultados === undefined
				|| json.resultados.result_fecha_previa_laboral === undefined) {
			datos = {};
		} else {
			datos = json.resultados.result_fecha_previa_laboral;
			$("#fliquidacionD").val(transformaFecha(datos[0]));
		}
	});
}
function getIsin() {
	var isinCompleto = $('#cdisin').val();
	var guion = isinCompleto.indexOf("-");

	return (guion > 0) ? isinCompleto.substring(0, guion) : isinCompleto;
}

function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";

	if ($('#fliquidacionD').val() !== "" && $('#fliquidacionD').val() !== undefined){
		cadenaFiltros += " fecha Desde: "+ $('#fliquidacionD').val();
	}
	if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined){
		cadenaFiltros += " fecha Hasta: "+ $('#fliquidacionA').val();
	}
	if ($('#cdisin').val() !== "" && $('#cdisin').val() !== undefined){
		cadenaFiltros += " isin: "+ $('#cdisin').val();
	}
	if ($('#selectCuenta').val() !== "" &&  $("#selectCuenta option:selected").val() !== undefined){
		if ( $("#selectCuenta option:selected").val() === "0"){
			cadenaFiltros += " Cuenta: todas ";
		}
		else{
			cadenaFiltros += " Cuenta: "+ $("#selectCuenta option:selected").val();
		}
	}
	$('.mensajeBusqueda').append(cadenaFiltros);
}
