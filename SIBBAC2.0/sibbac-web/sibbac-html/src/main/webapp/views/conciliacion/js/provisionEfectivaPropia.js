var listaIsin = [];
var availableTags = [];
var oTable = undefined;

$(document).ready(function() {
	oTable = $("#tblClearingTitulos").dataTable({
		"dom": 'T<"clear">lfrtip',
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "language": {
             "url": "i18n/Spanish.json"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "aoColumns" : [
                        {sClass : "centrar"},
                        {sClass : "centrar"},
                        {sClass : "centrar"},
                        {sClass : "monedaR",type:"num-html"}
                        ]
	});
	initializePicker();
	cargarFechaActualizacionMovimientoAlias();
	cargaCombo();
	$(function(){
		prepareCollapsion();
	});
	$('#cargarTabla').submit(btnBuscador);
});

function initializePicker() {
	loadpicker(['fliquidacionDe', 'fliquidacionA']);
	verificarFechas([["fliquidacionDe","fliquidacionA"]]);
}
function cargaCombo(){
	 var selectCuentaCall = {"service": "SIBBACServiceConciliacionClearing",
		     	   "action": "getCuentasLiquidacion"
	 			   };
	 var cdIsinCall = {"service": "SIBBACServiceConciliacion",
	     	   "action": "getIsin"
			   };

	 $("#selectCuenta").html("");
	 $("#selectCuenta").append("<option value='0'>Todas</option>");
	 cargaDatosCombo(selectCuentaCall);

	 $("#cdisin").html("");
	 cargaDatosCombo(cdIsinCall);
}
//carga datos combo cuenta
function cargaDatosCombo(llamada) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: JSON.stringify(llamada),
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined ||
                    json.resultados.cuentasLiquidacion === undefined) {
                datos={};
//                return false;
            } else {
                datos = json.resultados.cuentasLiquidacion;
            }

            if (json.resultados.result_isin === undefined ) {
                datosIsin={};
             } else {
            	datosIsin = json.resultados.result_isin;
             }

            $.each(datos, function (key, val) {
            	$("#selectCuenta").append('<option value='+datos[key].id+'>'+datos[key].name+'</option>');
            });

            $.each(datosIsin, function (key, val) {
        		listaIsin.push(datosIsin[key].codigo);
				ponerTag = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
				availableTags.push(ponerTag);

				$( "#cdisin" ).autocomplete({
					source: availableTags
				});

            });
        }
    });
}
function collapserSearch(event){
	event.preventDefault();
	$(this).parents('.title_section').next().slideToggle();
	$(this).parents('.title_section').next().next('.button_holder').slideToggle();
	$(this).toggleClass('active');
	if ( $(this).text()=="Ocultar opciones de búsqueda" ) {
		$(this).text( "Mostrar opciones de búsqueda" );
	} else if ( $(this).text()=="Mostrar opciones de búsqueda" ) {
		$(this).text( "Ocultar opciones de búsqueda" );
	}else{
		$(this).text( "Mostrar opciones de búsqueda" );
	}
	return false;
}
function btnBuscador(event){
	event.preventDefault();
	if($('#fliquidacionDe').val() === "" || $('#fliquidacionDe').val() === undefined){
		alert("La 'Fecha Liquidacion Desde' es obligatoria");
		return false;
	}
	collapseSearchForm();
	$("#tblClearingTitulos > tbody").empty();
    obtenerDatos();
	seguimientoBusqueda();
	return false;
}

function obtenerDatos(){

	var  gFiltros = "";
	if(getIsin()!== "" && getIsin()!== undefined){
		gFiltros += " \"isin\": \"" + getIsin() + "\",";
	}
	if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined){
		gFiltros += " \"fromdate\": \""+ transformaFechaInv($('#fliquidacionDe').val()) + "\",";
	}
	if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined){
		gFiltros += " \"ftodate\": \""+ transformaFechaInv($('#fliquidacionA').val()) + "\",";
	}
	console.log( "Filtros: " + gFiltros );

	gFiltros += " \"cuenta\": \"00P\",";
	gFiltros = gFiltros.substring(0,gFiltros.length-1);
	 var params = "{"
		 +"\"service\": \"SIBBACServiceConciliacionClearing\","
		 +"\"action\": \"getMovimientoUnificadoEfectivo\","
		 +"\"filters\": {"+gFiltros+"}}"


	    $("#tblClearingTitulos > tbody").html("");
		consultar(params);
}
function consultar(params) {
	$.blockUI({
		message: '<h1><img src="img/loading.gif" /> Cargando datos...</h1>',
		css: {
	        border: 'none',
	        padding: '15px',
	        backgroundColor: '#000',
	        '-webkit-border-radius': '10px',
	        '-moz-border-radius': '10px',
	        opacity: .5,
	        color: '#fff'
	     }
	});
    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/sibbac20back/rest/service",
        data: params,
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        success: function (json) {
            var datos = undefined;
            if (json === undefined || json.resultados === undefined || json.resultados === null ||
            		json.resultados.result_mov_unificados_efectivo === undefined) {
                datos={};
            } else {
                datos = json.resultados.result_mov_unificados_efectivo;
                var errors = cargarDatos(datos);
           }
           $.unblockUI();
        },
        error : function(err){
        	$.unblockUI();
        }

    });
}
function cargarDatos(datos){
	 var tbl = $("#tblClearingTitulos > tbody");
     $(tbl).html("");
		oTable.fnClearTable();
     //var row;
     $.each(datos, function (key, val) {
     	if(val.efectivo !== 0){
	    	 if(val.descrIsin==null){
	     		valorDescr = "";
	     	} else {
	     		valorDescr = val.descrIsin;
	     	}
				oTable.fnAddData([
	                            "<div class='centrado'>"+val.isin+"</div>",
	                            "<div class='centrado'>"+valorDescr+"</div>",
	                            "<div class='centrado'>"+val.cdcodigocuentaliq+"</div>",
	                            "<div class=''>"+a2digitos(val.efectivo)+"</div>"
	      					]);
	     	/*row = "<tr id=\"" + key + "\" >";
	     	row = row + "<td class=\"centrado\">" + val.isin + "</td>";
	     	row = row + "<td class=\"centrado\">" + val.descrIsin + "</td>";
	     	row = row + "<td class=\"centrado\">" + val.cdCodigo + "</td>";
	     	row = row + "<td class=\"centrado\">" + val.titulos + "</td>";
	     	row = row + "</tr>";
	     	tbl.append(row);*/
	     }
     });
     return false;
}
function seguimientoBusqueda(){
	 $('.mensajeBusqueda').empty();
	var  cadenaFiltros = "";
	if ($('#cdisin').val() !== "" && $('#cdisin').val() !== undefined){
		cadenaFiltros += " isin: "+ $('#cdisin').val();
	}
	if ($('#fliquidacionDe').val() !== "" && $('#fliquidacionDe').val() !== undefined){
		cadenaFiltros += " fliquidacionDe: "+ $('#fliquidacionDe').val();
	}
	if ($('#fliquidacionA').val() !== "" && $('#fliquidacionA').val() !== undefined){
		cadenaFiltros += " fliquidacionA: "+ $('#fliquidacionA').val();
	}
	$('.mensajeBusqueda').append(cadenaFiltros);
}
function getIsin(){
	var isinCompleto = $('#cdisin').val();
	var guion=isinCompleto.indexOf("-");
	if (guion<0){
		return isinCompleto;
	}else{
		return isinCompleto.substring(0, guion);
	}
}
