var listaIsin = [];
var availableTags = [];
var oTable = undefined;
//var isLoading = false;
var refreshDataListener = 0;
var REFRESH	= 30;	// Segundos de refresco de la pagina
//var isLoadingRequired = true;
var isLoadingRequired = true;
var ultimoJson = {};



/* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
if ($('.contenedorTabla').length >= 1) {
	var anchoBotonera;
	$('.contenedorTabla').each(function (i) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera + 'px');
		$(this).find('.resumen').css('width', anchoBotonera + 'px');
		$('.resultados').hide();
	});
}

function obtenerDatosPeriodicamente() {
	//no se muestra el loading
	// console.log( "[obtenerDatosPeriodicamente::("+refreshDataListener+")] [SIBBACServiceConciliacionPropia.getAnotacioneseccByTipoCuenta]..." );
//	$("#tblCtaPropia > tbody > tr").remove();
	var param = obtenerFiltros();
	var params = new DataBean();
	params.setService('SIBBACServiceConciliacionPropia');
	params.setAction('getAnotacioneseccByTipoCuenta');
	params.setFilters(JSON.stringify(param));
	consultar(params);
	if(refreshDataListener != 0){
		clearTimeout(refreshDataListener);
	}
	var refresco = parseInt(REFRESH) * 1000;
	refreshDataListener = setTimeout(obtenerDatosPeriodicamente, refresco );

}

function consultar(params, flag) {
	var request;
	if(isLoadingRequired){
		request = requestSIBBAC(params);
	}else{
		request = requestSIBBACSinBlockUI(params);
	}

	request.success(function (json) {
		var datos = undefined;

		if (json === undefined || json.resultados === undefined ||
				json.resultados.result_cuenta_propia === undefined) {
			datos = {};
		} else {
			//Si se esta refrescando la pagina automaticamente y el resultado es distinto,
			//se muestran los nuevos datos, de lo contrario se deja tal como esta
			//por otro lado si se pulsa el boton consultar, entonces isLoadingRequired es true por tanto
			//se muestran los datos si o si.
			if( JSON.stringify(ultimoJson) !== JSON.stringify(json) || isLoadingRequired){
				console.log("La peticion actual es diferente a la ultima realizada");
				var difClass = '';
				var colDif = '';
				var rownum = 0;
				oTable.fnClearTable();
				datos = json.resultados.result_cuenta_propia;
				$.each(datos, function (key, val) {

					if(datos[key].imprecio !== null && datos[key].imprecio >=0 && datos[key].nutitulos !== null && datos[key].nutitulos !== 0 && datos[key].cdCuenta !==""){

						//fila tabla izquierda
						colDif = (datos[key].nutitulos);
						var difClass = (colDif>0) ? 'colorFondo' : 'colorRojo';
						var titulos = $.number(datos[key].nutitulos, 0, ',', '.');
						var descIsin = (datos[key].isinDescripcion === null) ? "" : datos[key].isinDescripcion;
						oTable.fnAddData([
									   datos[key].cdisin,
									   descIsin,
									   datos[key].cdCuenta,
									   a6digitos(datos[key].imprecio),
									   titulos,
									   a2digitos(datos[key].imefectivo)
									], false);
						$(oTable.fnGetNodes(rownum)).addClass(difClass);
						rownum++;
					}

				});
				oTable.fnDraw();
			}else{
				console.log("La peticion actual es igual a la ultima realizada");
			}
			ultimoJson = json;
		}
		isLoadingRequired = false;
	});
	request.error(function(err){
		if(isLoadingRequired){
			$.unblockUI();
		}
		isLoadingRequired = false;
		console.log( "[Consultar:success] ERROR: " + err );
	});
}
/////////////////////////////////////////////////
//////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize_propiaTitulos() {
	cargaIsin();
}
$(document).ready(function () {
	oTable = $("#tblCtaPropia").dataTable({
		"dom": 'T<"clear">lfrtip',
		 "tableTools": {
			 "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		 },
		 "deferRender" : true,
		 "language": {
			 "url": "i18n/Spanish.json"
		 },
         "scrollY": "480px",
         "scrollCollapse": true,
		 "aoColumns" : [
			{sClass : "align-left"},
			{sClass : "align-left"},
			{sClass : "centrar"},
			{sClass : "monedaR",type:"formatted-num"},
			{sClass : "monedaR",type:"formatted-num"},
			{sClass : "monedaR",type:"formatted-num"}
		 ]
	});
	if ($("#tblCtaPropia > tbody").children().size() === 0) {
		// console.log('carga initialize');
		initialize_propiaTitulos();
	}
	else {
		console.log('ups no se deberia cargar');
	}
	/*Eva:Función para cambiar texto*/
	jQuery.fn.extend({
		toggleText: function (a, b) {
			var that = this;
			if (that.text() !== a && that.text() !== b) {
				that.text(a);
			} else if (that.text() === a) {
				that.text(b);
			} else if (that.text() === b) {
				that.text(a);
			}
			return this;
		}
	});
	/*FIN Función para cambiar texto*/

	$('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function (event) {
		event.preventDefault();
		$(this).parents('.title_section').next().slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar', 'Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function (event) {
		event.preventDefault();
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		if ($(this).text() == "Ocultar opciones de búsqueda") {
			$(this).text("Mostrar opciones de búsqueda");
		} else {
			$(this).text("Ocultar opciones de búsqueda");
		}
		return false;
	});
	$('.btn.buscador').click(function (event) {
		event.preventDefault();
		//mostrar loading cuando cargue tabla
		isLoadingRequired = true;
		$("#tblCtaPropia > tbody").empty();
		//$("#tblCtaPropia").trigger("update");
		obtenerDatosPeriodicamente();

		$('.collapser_search').parents('.title_section').next().slideToggle();
		$('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
		$('.collapser_search').toggleClass('active');
		if ($('.collapser_search').text() == "Ocultar opciones de búsqueda") {
			$('.collapser_search').text("Mostrar opciones de búsqueda");
		} else {
			$('.collapser_search').text("Ocultar opciones de búsqueda");
		}
		seguimientoBusqueda();
		return false;
	});

	$('#limpiar').click(function (event) {
		event.preventDefault();
		$('input[type=text]').val('');
		$('select').val('0');
	});

});
function render() {
	//
}
//rellena los filtros
function obtenerFiltros() {
	return {"cdisin": getIsin()};
}


//llamada para cargar datos isin
function cargaIsin(){

	 var llamada = {"service": "SIBBACServiceConciliacion",
			   "action": "getIsin"
			   };
	 //limpio los datos
	 $("#cdisin").html("");
	 //cargo isin
	 cargaDatosIsin(llamada);
}

function cargaDatosIsin(llamada) {
	$.ajax({
		type: 'POST',
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		url: "/sibbac20back/rest/service/",
		data: JSON.stringify(llamada),
		beforeSend: function (x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		success: function (json) {
			var datosIsin = undefined;
			if( json !== undefined || json.resultados !== undefined){
				if (json.resultados.result_isin === undefined ) {
					datosIsin={};
				} else {
					datosIsin = json.resultados.result_isin;
					$.each(datosIsin, function (key, val) {
						listaIsin.push(datosIsin[key].codigo);
						ponerTag = datosIsin[key].codigo+" - "+datosIsin[key].descripcion;
						availableTags.push(ponerTag);

						//código de autocompletar
						$( "#cdisin" ).autocomplete({
							source: availableTags
						});
					});
				}
			} else {
				return false;
			}
		}
	});
}


function getIsin(){
	var isinCompleto = $('#cdisin').val();
	var guion=isinCompleto.indexOf("-");

	return (guion!=-1) ? isinCompleto.substring(0, guion) : isinCompleto;
}

//para ver el filtro al minimizar la búsqueda
function seguimientoBusqueda() {
	$('.mensajeBusqueda').empty();
	var cadenaFiltros = "";
	if( $('#cdisin').val() !== ""  && $('#cdisin').val() !== undefined){
		cadenaFiltros += " Isin: " + $('#cdisin').val();
	}
	$('.mensajeBusqueda').append(cadenaFiltros);
}

//exportar
function SendAsExport(format) {
	var c = this.document.forms['propiaTitulos'];
	var f = this.document.forms["export"];
	f.action = "/sibbac20back/rest/export." + format;
	var json = {
		"service": "SIBBACServiceConciliacionPropia",
		"action": "getAnotacioneseccByTipoCuentaExport",
		"filters": {"cdisin": getIsin()}};
	if (json != null && json != undefined && json.length == 0) {
		alert("Introduzca datos");
	} else {
		f.webRequest.value = JSON.stringify(json);
		f.submit();
	}
}
