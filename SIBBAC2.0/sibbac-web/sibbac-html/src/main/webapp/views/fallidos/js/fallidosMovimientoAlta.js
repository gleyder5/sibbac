var idOcultosAlias = [];
var availableTags = [];


var idOcultosIsin = [];
var codigoOcultoIsin =[];
var valorIsin = [];

var idOcultosCamara = [];
var valorCamara = [];
$(document).ready(function () {

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		$('editarParamPop').bPopup({
        modalClose: false,
        opacity: 0.6,
        positionStyle: 'fixed' //'fixed' or 'absolute'
	});
		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Búsqueda Fallidos','Ocultar Búsqueda Fallidos');
		return false;
	});

/*BUSCADOR*/
	$('.btn.buscador').click(function(){
		//event.preventDefault();
			$('.resultados').show();
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');
			//cargarTabla();
		return false;
	});
/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}


	var fechas = ['textFecha'];
	loadpicker(fechas);
	cargarIsin();
	cargarCamara();
	cargarAlias();

});


function cargarAlias()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceTmct0ali\", \"action\"  : \"getAlias\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var resultados = json.resultados.result_alias;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				idOcultosAlias.push(item.id);
				ponerTag = item.alias.trim()+" - "+item.descripcion;
				availableTags.push(ponerTag);
				}
			//código de autocompletar

           $( "#textAlias" ).autocomplete({
             source: availableTags
           });


			//fin de código de autocompletar
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
	}

function cargarIsin()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceIsin\", \"action\"  : \"listIsin\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var resultados = json.resultados.listadoIsin;
			var item = null;


			for ( var k in resultados ) {
				item = resultados[ k ];
				idOcultosIsin.push(item.id);
				codigoOcultoIsin.push(item.codigo);
				ponerTag = item.descripcion.trim();
				valorIsin.push(ponerTag);
				}
			//código de autocompletar

           $( "#textIsin" ).autocomplete({
             source: valorIsin
           });


//			for ( var k in resultados ) {
//				item = resultados[ k ];
//
//				// Rellenamos el combo
//				$('#textIsin').append(
//						"<option value='" + item.id +"'>" + item.descripcion +"</option>"
//
//				);
//
//				}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});

}

//listado cámaras
function cargarCamara()
{

	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceCamara\", \"action\"  : \"listCamara\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var resultados = json.resultados.listadoCamara;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				idOcultosCamara.push(item.codigo);
				ponerTag = item.codigo.trim()+" - "+item.descripcion;
				valorCamara.push(ponerTag);
				}
			//código de autocompletar

           $( "#textCamara" ).autocomplete({
             source: valorCamara
           });







//			var idOcultosCamara = [];
//			var valorCamara = [];
//
//			for ( var k in resultados ) {
//				item = resultados[ k ];
//
//				// Rellenamos el combo
//				$('#textCamara').append(
//						"<option value='" + item.codigo +"'>" + item.descripcion +"</option>"
//
//				);
//
//				}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});

}

//alta Fallido
//insertar cobro
$('#anadir').click(function(){
	var listaAlias = document.getElementById("textAlias").value;

	posicionAlias = availableTags.indexOf(listaAlias);
	if(posicionAlias=="-1"){
		alert("El alias introducido no existe");
		return false;
	}
	valorSeleccionadoAlias = idOcultosAlias[posicionAlias];
	 	//creo filters

	var listaIsin = document.getElementById('textIsin').value;

	posicionIsin = valorIsin.indexOf(listaIsin);
	//posicionCodigoIsin = codigoOcultoIsin.indexOf(listaIsin);
	if (posicionIsin=="-1")
		{
		alert("El isin introducido no existe");
		return false;
		}

	textIsinV = idOcultosIsin[posicionIsin];
	codigoIsin = codigoOcultoIsin[posicionIsin];

//	var indiceSeleccionadoIsin = listaIsin.selectedIndex;
//	var opcionSeleccionadaIsin = listaIsin.options[indiceSeleccionadoIsin];
//	var textIsinV = opcionSeleccionadaIsin.value;

	var listaCamara = document.getElementById('textCamara').value;

	posicionCamara = valorCamara.indexOf(listaCamara);

	if (posicionCamara=="-1")
		{
		alert("La c&aacute;mara introducida no existe");
		return false;
		}

	textCamaraV = idOcultosCamara[posicionCamara];



//	var indiceSeleccionadoCamara = listaCamara.selectedIndex;
//	var opcionSeleccionadaCamara = listaCamara.options[indiceSeleccionadoCamara];
//	var textCamaraV = opcionSeleccionadaCamara.value;


	 	textNumTitV = document.getElementById('textNumTit').value;
	 	textFechaV = transformaFechaInv(document.getElementById('textFecha').value);
	 	textSentidoV = document.getElementById('textSentido').value;
	 	//textTipoV = document.getElementById('textTipo').value;
	 	textNuOperacion = document.getElementById('textNuOperacion').value;

	 	if (textNuOperacion.length < 16)
	 	{
	 		alert("La longitud del número de operación debe ser 16");
	 		return false;
	 	}

	 	var params = "[{\"idAlias\" : \""+ valorSeleccionadoAlias + "\",\"Isin\" : \""+ codigoIsin + "\",\"TitulosFallidosOperacion\" : \""+ textNumTitV + "\",\"Feejecuc\" : \""+ textFechaV + "\",\"Camara\" : \""+ textCamaraV + "\",\"Sentido\" : \""+ textSentidoV + "\",\"NumeroOperacion\" : \""+ textNuOperacion + "\"}]";

		var data = new DataBean();
		data.setService('SIBBACServiceOperacionFallida');
		data.setAction('createOperacionFallida');
		data.setParams(params);
		var request = requestSIBBAC( data );
		request.success( function(json) {
			$("section").load("views/fallidos/fallidosMovimiento.html");
		}
		);
	})
