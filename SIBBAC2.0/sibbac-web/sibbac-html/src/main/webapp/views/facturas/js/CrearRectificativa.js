var idOcultos = [];
var availableTags = [];
var oTable = undefined;

$(document).ready(function() {
	oTable = $(".tablaPrueba").dataTable({
		"dom" : 'T<"clear">lfrtip',
		"tableTools" : {
			"sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
		"language" : {
			"url" : "i18n/Spanish.json"
		},
        "scrollY": "480px",
        "scrollCollapse": true,
		"columns" : [ {
			"width" : "5%"
		}, {
			"width" : "auto"
		}, {
			"width" : "20%"
		}, {
			"width" : "auto",
			type : "date-eu"
		}, {
			"width" : "auto",
			sClass : "monedaR",
			type : "formatted-num"
		}, {
			"width" : "auto",
			sClass : "monedaR",
			type : "formatted-num"
		}, {
			"width" : "auto",
			sClass : "monedaR",
			type : "formatted-num"
		}, {
			"width" : "auto",
			type : "date-eu"
		}, {
			"width" : "auto",
			type : "date-eu"
		} ]
	});
	var fechas = [ 'fechaDesde', 'fechaHasta' ];
	loadpicker(fechas);
	verificarFechas([ [ 'fechaDesde', 'fechaHasta' ] ]);

	$('a[href="#release-history"]').toggle(function() {
		$('#release-wrapper').animate({
			marginTop : '0px'
		}, 600, 'linear');
	}, function() {
		$('#release-wrapper').animate({
			marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function() {
		_gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
	});

	$('.collapser').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		// $(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar', 'Clic para ocultar');

		$('editarParamPop').bPopup({
			modalClose : false,
			opacity : 0.6,
			positionStyle : 'fixed' // 'fixed' or 'absolute'
		});
		return false;
	});
	$('.collapser_search').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Buscar Factura', 'Ocultar Buscar Factura');
		return false;
	});

	/* BUSCADOR */
	$('#cargarTabla').submit(function(event){
		event.preventDefault();
		if (validarCampoFecha('fechaDesde', false) && validarCampoFecha('fechaHasta', false)) {
			var numeroFactura = document.getElementById("numeroFactura").value;
			var fechaDesde = document.getElementById('fechaDesde').value;
			var fechaHasta = document.getElementById('fechaHasta').value;
			var idAlias = getIdAlias();
			var fechas = [ 'fechaDesde', 'fechaHasta' ];
			loadpicker(fechas);
			verificarFechas([ fechas ]);
			cargarTabla(numeroFactura, idAlias, fechaDesde, fechaHasta);
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda', 'Ocultar opciones de búsqueda');
		}else {
			alert("Los campos de fecha introducidos no son correctos. Por favor revíselos.");
		}
		return false;
	});
	/*
	 * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
	 * TABLA
	 */
	if ($('.contenedorTabla').length >= 1) {
		var anchoBotonera;
		$('.contenedorTabla').each(function(i) {
			anchoBotonera = $(this).find('table').outerWidth();
			$(this).find('.botonera').css('width', anchoBotonera + 'px');
			$(this).find('.resumen').css('width', anchoBotonera + 'px');
			// $('.resultados').hide();
		});
	}

	cargarAlias();
	cargarTabla('', '', '', '');
});

// funcion para cargar la tabla
function cargarTabla(numeroFactura, idAlias, fechaDesde, fechaHasta) {
	console.log("[valorSeleccionadoAlias==" + idAlias + "] [fechaDesde==" + fechaDesde + "] [fechaHasta==" + fechaHasta + "]");

	if (numeroFactura != "") {
		var filtro = "{\"numeroFactura\" : \"" + numeroFactura + "\" }";
	} else {
		var filtro = "{\"idAlias\" : \"" + idAlias + "\"," + "\"fechaDesde\" : \"" + transformaFechaInv(fechaDesde) + "\","
				+ "\"fechaHasta\" : \"" + transformaFechaInv(fechaHasta) + "\" }";
	}

	var jsonData = "{" + "\"service\" : \"SIBBACServiceFactura\", " + "\"action\"  : \"getListadoFacturasEnviadas\", " + "\"filters\"  : "
			+ filtro + "}";
	console.log("[filtro==" + filtro + "] [jsonData==" + jsonData + "]");
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos = {};
			} else {
				datos = json.resultados.listadoFacturas;
			}

			var item = null;
			var contador = 0;
			oTable.fnClearTable();
			for ( var k in datos) {
				item = datos[k];
				oTable.fnAddData([
						"<input type='checkbox' class='checkTabla' id='checkTabla' name='checkTabla' /><input type='hidden' name='idmarcar"
								+ contador + "' id='idmarcar" + contador + "' value='" + item.idFactura + "' />",
						"<span>" + item.numeroFactura + "</span>", "<span >" + item.cdAlias.trim() + " - " + item.nombreAlias + "</span>",
						"<span>" + transformaFecha(item.fechaCreacion) + "</span>", "<span>" + item.baseImponible + "</span>",
						"<span>" + item.importeImpuestos + "</span>", "<span>" + item.importeTotal + "</span>",
						"<span>" + transformaFecha(item.fechaInicio) + "</span>", "<span>" + transformaFecha(item.fechaFin) + "</span>" ]);
				contador++;
			}

		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});

}

function cargarAlias() {
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacion\"}",

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos = {};
			} else {
				datos = json.resultados.listaAlias;
			}
			var resultados = datos;
			var item = null;

			for ( var k in resultados) {
				item = resultados[k];
				idOcultos.push(item.id);
				ponerTag = item.nombre.trim();
				availableTags.push(ponerTag);
			}
			// código de autocompletar

			$("#textAlias").autocomplete({
				source : availableTags
			});
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

// marcar y desmarcar
function clickSeleccionarTodos() {
	var fila = $('.tablaPrueba').find('td input.checkTabla');
	fila.prop('checked', true);
}

function clickDesmarcarTodos() {
	var fila = $('.tablaPrueba').find('td input.checkTabla');
	fila.prop('checked', false);
}

// Evento para marcar facturas
$('#btnMarcar').click(function() {
	var suma = 0;
	var valores = new Array();
	var los_cboxes = document.getElementsByName('checkTabla');
	for (var i = 0, j = los_cboxes.length; i < j; i++) {
		// alert(los_cboxes[i].checked.value);
		if (los_cboxes[i].checked == true) {
			valor = document.getElementById("idmarcar" + i).value;
			valores.push(valor);
			suma++;
		}
	}

	if (suma == 0) {
		alert('Marque al menos una línea');
		return false;
	} else {
		marcarFacturas(valores);
	}
})

function cambioAlias() {
	document.getElementById('numeroFactura').value = "";
}

function cambioNumeroFactura() {
	document.getElementById('textAlias').value = "";
	document.getElementById('fechaDesde').value = "";
	document.getElementById('fechaHasta').value = "";

}

function getIdAlias() {
	var idAlias = "";
	var textAlias = document.getElementById("textAlias").value;
	if ((textAlias !== null) && (textAlias !== '')) {
		idAlias = idOcultos[availableTags.indexOf(textAlias)];
		if (idAlias === undefined) {
			idAlias = "";
		}
	}
	return idAlias;
}

// Funcion para marcar facturas
function marcarFacturas(valores) {
	bucleVal = valores.length;
	console.log("entro en el metodo");

	var params = "[ ";
	for (var v = 0; v < bucleVal; v++) {
		params = params + "{ \"idFactura\" :\"" + valores[v] + "\" },";
	}
	params = params.substring(0, params.length - 1);
	params = params + "\ ]";

	var jsonData = "{" + "\"service\" : \"SIBBACServiceFactura\", " + "\"action\"  : \"marcarRectificar\", " + "\"params\"  : " + params
			+ "}";
	console.log(jsonData);
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			$("section").load("views/facturas/CrearRectificativa.html");
		},
		error : function(c) {
			alert("ERROR: " + c);
		}

	});
}
