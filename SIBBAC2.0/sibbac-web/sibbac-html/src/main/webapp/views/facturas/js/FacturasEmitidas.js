var idOcultos = [];
var availableTags = [];
var idAlias = "";
var fechaDesde = "";
var fechaHasta = "";
var oTable = undefined;

$(document).ready(function() {
	oTable = $("#tablaPrueba").dataTable({
		"dom" : 'T<"clear">lfrtip',
		"tableTools" : {
			"sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
		"language" : {
			"url" : "i18n/Spanish.json"
		},
        "scrollY": "480px",
		"columns" : [ {
			"width" : "17%"
		}, {
			"width" : "17%"
		}, {
			"width" : "8%",
			type : "date-eu"
		}, {
			"width" : "10%",
			type : "date-eu"
		}, {
			"width" : "10%"
		}, {
			"width" : "10%",
			sClass : "monedaR",
			type : "formatted-num"
		}, {
			"width" : "10%",
			sClass : "monedaR",
			type : "formatted-num"
		}, {
			"width" : "8%",
			sClass : "monedaR",
			type : "formatted-num"
		}, {
			"width" : "8%",
			type : "date-eu"
		}, {
			"width" : "8%",
			type : "date-eu"
		} ]
	});

	$('a[href="#release-history"]').toggle(function() {
		$('#release-wrapper').animate({
			marginTop : '0px'
		}, 600, 'linear');
	}, function() {
		$('#release-wrapper').animate({
			marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function() {
		_gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
	});

	$('.collapser').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		// $(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar', 'Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Opciones de Búsqueda', 'Ocultar Opciones de Búsqueda');
		return false;
	});

	/* BUSCADOR */
	$('#cargarTabla').submit(function(event) {
		event.preventDefault();
		if (validarCampoFecha('fechaDesde', false) && validarCampoFecha('fechaHasta', false)) {
			var fechaDesde = document.getElementById('fechaDesde').value;
			var fechaHasta = document.getElementById('fechaHasta').value;
			var fechas = [ 'fechaDesde', 'fechaHasta' ];
			loadpicker(fechas);
			var numeroFactura = document.getElementById("numeroFactura").value;
			var idAlias = getIdAlias();
			var idEstado = document.getElementById("idEstado").value;
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			cargarTabla(numeroFactura, idAlias, fechaDesde, fechaHasta, idEstado);
		} else {
			alert("Los campos de fecha introducidos no son correctos. Por favor revíselos.");
		}
		return false;
	});
	/*
	 * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
	 * TABLA
	 */
	if ($('.contenedorTabla').length >= 1) {
		var anchoBotonera;
		$('.contenedorTabla').each(function(i) {
			anchoBotonera = $(this).find('table').outerWidth();
			$(this).find('.botonera').css('width', anchoBotonera + 'px');
			$(this).find('.resumen').css('width', anchoBotonera + 'px');
			// $('.resultados').hide();
		});
	}

	var fechas = [ 'fechaHasta', 'fechaDesde' ];
	loadpicker(fechas);
	verificarFechas([ [ 'fechaDesde', 'fechaHasta' ] ]);
	cargarAlias();
	cargarEstados();

});

function cambioAlias() {
	document.getElementById('numeroFactura').value = "";
}

function cambioNumeroFactura() {
	document.getElementById('textAlias').value = "";
	document.getElementById('fechaDesde').value = "";
	document.getElementById('fechaHasta').value = "";

	var listaEstado = document.getElementById("idEstado");
	listaEstado.selectedIndex = 0;
}

function getIdAlias() {
	var idAlias = "";
	var textAlias = document.getElementById("textAlias").value;
	if ((textAlias !== null) && (textAlias !== '')) {
		idAlias = idOcultos[availableTags.indexOf(textAlias)];
		if (idAlias === undefined) {
			idAlias = "";
		}
	}
	return idAlias;
}

function cargarAlias() {
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacion\"}",

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos = {};
			} else {
				datos = json.resultados.listaAlias;
			}
			var resultados = datos;
			var item = null;

			for ( var k in resultados) {
				item = resultados[k];
				idOcultos.push(item.id);
				ponerTag = item.nombre.trim();
				availableTags.push(ponerTag);
			}
			// código de autocompletar

			$("#textAlias").empty();
			$("#textAlias").autocomplete({
				source : availableTags
			});
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

function cargarTabla(numeroFactura, idAlias, fechaDesde, fechaHasta, idEstado) {

	console.log("[idAlias==" + idAlias + "] [fechaDesde==" + fechaDesde + "] [fechaHasta==" + fechaHasta + "] [estado==" + idEstado + "]");
	var filtro = null;
	if (numeroFactura != "") {
		filtro = "{\"numeroFactura\" : \"" + numeroFactura + "\" }";
	} else {
		filtro = "{\"idAlias\" : \"" + idAlias + "\"," + "\"fechaDesde\" : \"" + transformaFechaInv(fechaDesde) + "\","
				+ "\"fechaHasta\" : \"" + transformaFechaInv(fechaHasta) + "\"," + "\"idEstado\" : \"" + idEstado + "\" }";
	}

	var jsonData = "{" + "\"service\" : \"SIBBACServiceFactura\", " + "\"action\"  : \"getListadoFacturas\", " + "\"filters\"  : " + filtro
			+ "}";
	console.log("[filtro==" + filtro + "] [jsonData==" + jsonData + "]");

	$
			.ajax({
				type : "POST",
				dataType : "json",
				url : "/sibbac20back/rest/service",
				data : jsonData,
				beforeSend : function(x) {
					if (x && x.overrideMimeType) {
						x.overrideMimeType("application/json");
					}
					// CORS Related
					x.setRequestHeader("Accept", "application/json");
					x.setRequestHeader("Content-Type", "application/json");
					x.setRequestHeader("X-Requested-With", "HandMade");
					x.setRequestHeader("Access-Control-Allow-Origin", "*");
					x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
				},
				async : true,
				success : function(json) {
					var datos = undefined;
					if (json === undefined || json.resultados === undefined || json.resultados.listadoFacturas === undefined) {
						datos = {};
					} else {
						datos = json.resultados.listadoFacturas;
					}

					oTable.fnClearTable();
					var item = null;
					for ( var k in datos) {
						item = datos[k];
						oTable
								.fnAddData([
										item.numeroFactura
												+ "<div class='lupa'><a href='#' onclick = \"document.getElementById('formulariosDatosFactura').style.display='block';document.getElementById('fade').style.display='block';cargoInputsFactura('"
												+ item.numeroFactura
												+ "','"
												+ item.idFactura
												+ "'); \"><img src='img/lupa.gif'></a>&nbsp;<a href='#' onclick = \"document.getElementById('formulariosHistorialFactura').style.display='block';document.getElementById('fade').style.display='block';cargarHistorico('"
												+ item.numeroFactura + "','" + item.idFactura
												+ "'); \"><img src='img/hist.gif'></a>&nbsp;<a href='#' onclick = \"duplicarFactura('"
												+ item.idFactura + "','" + item.numeroFactura
												+ "'); \"><img src='img/duplic.gif'></a></div>",
										"<span>" + item.cdAlias.trim() + " - " + item.nombreAlias.trim() + "</span>",
										"<span>" + transformaFecha(item.fechaCreacion) + "</span>",
										"<span>" + sumoFecha(item.fechaCreacion) + "</span>", "<span>" + item.descEstado + "</span>",
										"<span>" + $.number(item.baseImponible, 2, ',', '.') + "</span>", "<span>" + $.number(item.importeImpuestos, 2, ',', '.') + "</span>",
										"<span>" + $.number(item.importeTotal, 2, ',', '.') + "</span>", "<span>" + transformaFecha(item.fechaInicio) + "</span>",
										"<span>" + transformaFecha(item.fechaFin) + "</span>" ]);
					}

				},
				error : function(c) {
					console.log("ERROR: " + c);
				}
			});

}

// listado estados
function cargarEstados() {
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : "{\"service\" : \"SIBBACServiceTmct0estado\", \"action\"  : \"getEstadosFactura\"}",

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos = {};
			} else {
				datos = json.resultados.estados;
			}
			var resultados = datos;
			var item = null;

			for ( var k in resultados) {
				item = resultados[k];
				// Rellenamos el combo
				$('#idEstado').append("<option value='" + item.idestado + "'>" + item.nombre + "</option>"

				);

			}
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

// listado estados
function cargarHistorico(num, id) {
	$('#hf').empty();
	$('#hf').append(num);
	$('#tablaHistorial').empty();
	var filtro = "{\"idFactura\" : \"" + id + "\" }";
	var jsonData = "{" + "\"service\" : \"SIBBACServiceFactura\", " + "\"action\"  : \"getTransicionesFactura\", " + "\"filters\"  : "
			+ filtro + "}";
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",

		data : jsonData,

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos = {};
			} else {
				datos = json.resultados.listadoFacturas;
			}
			var resultados = datos;
			var item = null;
			$('#tablaHistorial').append("<tr>" + "<th>N&uacute;mero Factura</th>" + "<th>Estado</th>" + "<th>Fecha</th>" + "</tr>");
			for ( var k in resultados) {
				item = resultados[k];
				// Rellenamos la tabla
				$('#tablaHistorial').append(
						"<tr>" + "<td>" + item.numeroFactura + "</td>" + "<td>" + item.estado + "</td>" + "<td>"
								+ transformaFechaHora(item.fecha) + "</td>" + "</tr>");

			}
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

// listado lupa
function cargoInputsFactura(num, id) {
	$('#df').empty();
	$('#df').append(num);
	$('#tablaDatos').empty();
	var filtro = "{\"facturaId\" : \"" + id + "\" }";
	var jsonData = "{" + "\"service\" : \"SIBBACServiceOrdenesFactura\", " + "\"action\"  : \"getListaOrdenesFactura\", "
			+ "\"filters\"  : " + filtro + "}";
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",

		data : jsonData,

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos = {};
			} else {
				datos = json.resultados;
			}
			var resultados = datos;
			var item = null;
			$('#tablaDatos').append(
					"<tr>" + "<th>Orden</th>" + "<th>Booking</th>" + "<th>Num</th>" + "<th>Ref. Banco</th>" + "<th>L&iacute;quido</th>"
							+ "<th>Neto</th>" + "<th>Bruto</th>" + "<th>Comisiones</th>" + "<th>T&iacute;tulo</th>" + "</tr>");
			for ( var k in resultados) {
				item = resultados[k];
				// Rellenamos la tabla

				$('#tablaDatos').append(
						"<tr>" + "<td class='taleft'>" + item.nOrden + "</td>" + "<td class='taleft'>" + item.nBooking + "</td>"
								+ "<td class='taleft'>" + item.nUcnfclt + "</td>" + "<td class='tacenter'>" + item.cdrefban + "</td>"
								+ "<td class='taright'>" + $.number(item.imnetliq,2,',','.') + "</td>" + "<td class='taright'>" + $.number(item.imtotnet,2,',','.') + "</td>"
								+ "<td class='taright'>" + $.number(item.imtotbru,2,',','.') + "</td>" + "<td class='taright'>" + $.number(item.imcomisn,2,',','.') + "</td>"
								+ "<td class='taleft'>" + item.cdordtit + "</td>" + "</tr>");

			}
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

// exportar
function SendAsExport(format) {
	var c = this.document.forms['facturasemitidas'];
	var f = this.document.forms["export"];
	console.log("Forms: " + f.name + "/" + c.name);
	f.action = "/sibbac20back/rest/export." + format;

	var json = {
		"service" : "SIBBACServiceFactura",
		"action" : "getListadoFacturas",
		"filters" : {
			"idAlias" : getIdAlias(),
			"fechaDesde" : transformaFechaInv(document.getElementById('fechaDesde').value),
			"fechaHasta" : transformaFechaInv(document.getElementById('fechaHasta').value),
			"idEstado" : document.getElementById("idEstado").value
		}
	};
	if (json != null && json != undefined && json.length == 0) {
		alert("Introduzca datos");
	} else {
		f.webRequest.value = JSON.stringify(json);
		f.submit();
	}
}

// emitir duplicado
function duplicarFactura(id, nf) {

	var jsonData = "{" + "\"service\" : \"SIBBACServiceFactura\", " + "\"action\"  : \"enviarDuplicado\", "
			+ "\"filters\" : {\"idFactura\" : \"" + id + "\"}}";
	if (confirm("¿Desea emitir un duplicado de la factura " + nf + "?") == true) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : "/sibbac20back/rest/service",
			data : jsonData,
			beforeSend : function(x) {
				if (x && x.overrideMimeType) {
					x.overrideMimeType("application/json");
				}
				// CORS Related
				x.setRequestHeader("Accept", "application/json");
				x.setRequestHeader("Content-Type", "application/json");
				x.setRequestHeader("X-Requested-With", "HandMade");
				x.setRequestHeader("Access-Control-Allow-Origin", "*");
				x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
			},
			async : true,
			success : function(json) {
				$("section").load("views/facturas/FacturasEmitidas.html");
			},
			error : function(c) {
				console.log("ERROR: " + c);
			}
		});
	}

}
