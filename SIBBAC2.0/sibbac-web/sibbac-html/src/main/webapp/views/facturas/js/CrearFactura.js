var idOcultos = [];
var availableTags = [];
var oTable = undefined;
$(document).ready(function () {
	cargarAlias();
	oTable = $(".tablaPrueba").dataTable({
		"dom": 'T<"clear">lfrtip',
		 "tableTools": {
			 "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		 },
		 "language": {
			 "url": "i18n/Spanish.json"
		 },
         "scrollY": "480px",
         "scrollCollapse": true,
		 "aoColumns" : [
						{"width":"9%",sClass : "centrar"},
						{"width":"35%"},
						{"width":"15%",type: "date-eu"},
						{"width":"15%",sClass : "monedaR"},
						{"width":"13%",type: "date-eu"},
						{"width":"13%",type: "date-eu"}
		 ]
	});

	$('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');
		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Opciones de búsqueda','Ocultar Opciones de búsqueda');
		return false;
	});

	/*BUSCADOR*/
	$('.btn.buscador').click(function(){

		// Crear una factura
		// Miramos si el alias existe
		var listaAlias = $("#textAlias").val();
		posicionAlias = availableTags.indexOf(listaAlias);

		if ( ( posicionAlias=="-1" ) && ( listaAlias!="" ) ) {
			alert("El alias introducido no existe");
			return false;
		} else {
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar Opciones de búsqueda','Ocultar Opciones de búsqueda');

			var listaAlias = document.getElementById("textAlias").value;
			var valorSeleccionadoAlias = "";

			if ( listaAlias==null || listaAlias==undefined || listaAlias=="" ) {
				valorSeleccionadoAlias = "";
			} else {
				posicionAlias = availableTags.indexOf(listaAlias);
				valorSeleccionadoAlias = idOcultos[posicionAlias];
			}
			// console.log( "[valorSeleccionadoAlias=="+valorSeleccionadoAlias+"]" );
			if ( this.id==='buscar' ) {
				// El buscar de siempre:
				// Aquí asignamos el estado
				cargarTabla( valorSeleccionadoAlias );
			} else {
				// Inicialmente, "sugerir".
				if ( valorSeleccionadoAlias=="" ) {
					alert("Debe introducir un alias");
				} else {
					inicializarLoading();
					var datos = new DataBean();
					var filters = "{ \"idAlias\" : \""+valorSeleccionadoAlias+"\" }";
					datos.setService( "SIBBACServiceFacturaSugerida" );
					datos.setAction( "crearSugeridas" );
					datos.setFilters( filters );

					var request = requestSIBBAC( datos );
					request.success( function( json ) {
						alert( json.error );
					});
				}
			}
			return false;
		}

	});
/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		//$('.resultados').hide();
		});
	}

});

function cargarAlias()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacion\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos={};
			} else {
				datos = json.resultados.listaAlias;
			}
			var resultados = datos;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				idOcultos.push(item.id);
				ponerTag = item.nombre.trim();
				availableTags.push(ponerTag);
				}
			//código de autocompletar

		   $( "#textAlias" ).autocomplete({
			 source: availableTags
		   });
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}

//funcion para cargar la tabla
function cargarTabla(alias)
{

	//hay que crear el filtro con alias y estado
	var filtro = "{";
	if(alias!=""){
		filtro = filtro + "\"idAlias\" : \""+ alias + "\",";
	}
	if(filtro.length>1){
		filtro = filtro.substring(0,filtro.length-1);
	}

	filtro = filtro + "}";
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceFacturaSugerida\", \"action\"  : \"getListadoCrearFacturas\", \"filters\"  : "+filtro+"}",
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
				var datos = undefined;
				if (json === undefined || json.resultados === undefined ||
						json.resultados.listadoFacturas === undefined) {
					datos={};
				} else {
					datos = json.resultados.listadoFacturas;
				}

				contador = 0;
				oTable.fnClearTable();
				for ( var k in datos ) {
					item = datos[ k ];
					var activeC = 0;
					//miro los valores de info alias, y si están a 1 los pinto verdes, a 0 rojos
					if(item.nContacto=="1"){
						cont = "s";
						eCont = "";
						activeC++;
					} else {
						cont = "n";
						eCont = "no ";
					}
					if(item.nDireccion=="1"){
						dir = "s";
						eDir = "";
						activeC++;
					} else {
						dir = "n";
						eDir = "no ";
					}
					if(item.nCIF=="1"){
						cif = "s";
						eCif = "";
						activeC++;
					} else {
						cif = "n";
						eCif = "no ";
					}
					if(activeC == 3){
						chkD = "";
					} else {
						chkD = "disabled";
					}
					oTable.fnAddData([
							   "<input type='checkbox' class='checkTabla' id='checkTabla' name='checkTabla' data='idmarcar"+contador+"' "+chkD+"/><input type='hidden' name='idmarcar"+contador+"' id='idmarcar"+contador+"' value='"+item.idFacturaSugerida + "' />&nbsp;<img src='img/"+cont+"cont.gif' title='Contacto "+eCont+"asignado'></a>&nbsp;<img src='img/"+dir+"direc.gif' title='Direcci&oacute;n "+eDir+"asignada'></a>&nbsp;<img src='img/"+cif+"cif.gif' title='CIF "+eCif+"asignado'></a>",
							   "<span >"+ item.cdAlias.trim()+" - "+item.nombreAlias.trim() +"</span>",
							   "<span>" +transformaFecha(item.fechaCreacion)+ "</span>",
							   "<span>" +$.number(item.importe,2,',','.') + "</span>",
							   "<span>" +transformaFecha(item.fechaInicio)+ "</span>",
							   "<span>" +transformaFecha(item.fechaFin)+ "</span>"
							]);
					contador++;
				}
				//poner botones
				$('.botonera').empty();
				$('.botonera').append("<input type='button' class='mybutton' id='seleccionarTodos' value='Seleccionar Todas' onclick='clickSeleccionarTodos()' >");
				$('.botonera').append("<input type='button' class='mybutton' id='desmarcarTodos' value='Desmarcar Todas' onclick='clickDesmarcarTodos()' >");
				$('.piePagina').empty();
				$('.piePagina').append("<input type='button' class='mybutton' id='btnMarcar' value='Crear' >");

				//Evento para marcar facturas
				$('#btnMarcar').click(function(){
						var suma = 0;
						var valores = new Array();
						var los_cboxes = document.getElementsByName('checkTabla');

						for (var i = 0, j = los_cboxes.length; i < j; i++) {

							if(los_cboxes[i].checked == true){
								valor = $('#' + $(los_cboxes[i]).attr('data')).val();
								valores.push(valor);
								suma++;
							}
						}

						if(suma == 0){
							alert('Marque al menos una línea');
							return false;
						}else{
							marcarFacturas(valores);
						}
				});

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});

}

//marcar y desmarcar
function clickSeleccionarTodos() {
	var fila = $('.tablaPrueba').find('td input.checkTabla:enabled');
	fila.prop('checked',true);
}

function clickDesmarcarTodos() {
	var fila = $('.tablaPrueba').find('td input.checkTabla');
	fila.prop('checked',false);
}

//Funcion para marcar facturas
function marcarFacturas(valores)
{

	bucleVal = valores.length;
	// console.log("entro en el metodo");

	var params = "[ ";
	for (var v = 0; v < bucleVal; v++) {
		params = params + "{ \"idFacturaSugerida\" :\""+valores[v]+ "\" },";
	}
	params = params.substring(0,params.length-1);
	params = params + "\ ]";

	var jsonData = "{" +
			"\"service\" : \"SIBBACServiceFacturaSugerida\", " +
			"\"action\"  : \"marcarFacturar\", "+
			"\"params\"  : "+ params + "}";
	console.log(jsonData);
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: jsonData,
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			$("section").load("views/facturas/CrearFactura.html");
		},
		error: function(c) {
			alert( "ERROR: " + c );
		}

	});
}

