var idOcultos = [];
var availableTags = [];
var marcarChecks = [];
var idsFacturas = [];

var datepickerCheck = 0;// carga de datepickers
var checkTime = 0;// carga de tabla
var oTable = undefined;

$(document).ready(function () {

    oTable = $("#tablaPrueba").dataTable({
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
        },
        "language": {
            "url": "i18n/Spanish.json"
        },
        "scrollY": "480px",
        "scrollCollapse": true,
        "columns": [{"width": "auto"
            }, {
                "width": "auto"
            }, {
                "width": "auto"
            }, {
                "width": "auto",
                type: "date-eu"
            }, {
                "width": "auto",
                sClass: "monedaR",
                type: "formatted-num"
            }, {
                "width": "auto",
                sClass: "monedaR",
                type: "formatted-num"
            }, {
                "width": "auto",
                sClass: "monedaR",
                type: "formatted-num"
            }, {
                "width": "auto",
                sClass: "monedaR",
                type: "formatted-num"
            }, {
                "width": "auto",
                sClass: "monedaR",
                type: "formatted-num"
            }, {
                "width": "auto"
            }]
    });

    $('#importeCobro').blur(function () {
        calculosInicialesCobro(this);
    });
    /* BUSCADOR */
    $('#cargarTabla').submit(function (event) {
        event.preventDefault();
        var idAlias = getIdAlias();
        if (idAlias !== "") {
            cargarTabla(idAlias);
            collapseSearchForm();
        }
        return false;
    });

    $("td.editable span").click(clickEditable);
    /*
     * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
     * TABLA
     */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
        });
    }
    prepareCollapsion();
    // insertar cobro
    $('#crearCobro').submit(function (event) {
        event.preventDefault();
        crearCobro();
    });

    $('#modificarImporteAplicado').click(function () {
        modificarImporteAplicado();
    });

    initialize();
});

function initialize() {
    var fechas = ['fechaContabilizacion'];
    loadpicker(fechas);
    cargarAlias();
}

function cargarAlias() {
    var data = new DataBean();
    data.setService('SIBBACServiceAlias');
    data.setAction('getListaAliasFacturacion');
    var request = requestSIBBAC(data);
    request.success(function (json) {

        datos = undefined;
        if (json === undefined || json.resultados === undefined) {
            datos = {};
        } else {
            datos = json.resultados.listaAlias;
        }
        var resultados = datos;
        var item = null;
        for (var k in resultados) {
            item = resultados[k];
            idOcultos.push(item.id);
            ponerTag = item.nombre.trim();
            availableTags.push(ponerTag);
        }
        // código de autocompletar
        $("#textAlias").empty();
        $("#textAlias").autocomplete({
            source: availableTags
        });
    });
    request.error(function (c) {
        console.log("ERROR: " + c);
    });
}

function calculosInicialesCobro(cobro) {
    var importeCobro = $(cobro).val();
    if (isNaN(importeCobro) || (importeCobro === null) || (importeCobro === '')) {
        // vaciamos los datos y bloqueamos las filas
        $(this).empty();
        $('#importeVencido').empty();
        $('#saldoPendiente').empty();
        $('#saldoPendiente').val('');
        if (idsFacturas.length > 0) {
            for (z = 0; z < idsFacturas.length; z++) {
                var idFactura = idsFacturas[z];
                $('#ckFila' + idFactura).attr('disabled', true);
            }
        }
    } else {
        // inicializamos los valores y desbloqueamos las filas
        var importeVencido = 0;
        var saldoPendiente = (importeCobro - importeVencido);
        $('#importeVencido').val($.number(importeVencido, 2, ',', '.'));
        $('#saldoPendiente').val($.number(saldoPendiente, 2, ',', '.'));
        $('#importeCobro').val($.number(importeCobro, 2, ',', '.'));
        if (idsFacturas.length > 0) {
            for (z = 0; z < idsFacturas.length; z++) {
                var idFactura = idsFacturas[z];
                $('#ckFila' + idFactura).removeAttr('disabled');
            }
        }
    }
}

function crearCobro() {
    var idAlias = getIdAlias();
    var importeCobrado = parseFloat($('#importeCobro').val());
    var fechaContabilizacion = transformaFechaInv($('#fechaContabilizacion').val());

    var filters = '{"idAlias" : "'+idAlias+'" , "importeCobrado" : "'+importeCobrado+'" ,"fechaContabilizacion" : "'+fechaContabilizacion+'"}';
    var params = [];
    var param = {};
    var cierre = "";
    for (z = 0; z < marcarChecks.length; z++) {
        var idFactura = marcarChecks[z];
        // accedo al contenido del div
        var importeAplicado = $('#tablaPrueba').find('#importeAplicado' + idFactura).text();
        cierre = $('#tablaPrueba').find('#ckCerrar' + idFactura).is(':checked');

        param ={idFactura : idFactura  , importeAplicado : importeAplicado , cierre : cierre};
        params[z] = param;
    }

    // fin del for, le resto la última coma
    var data = new DataBean();
    data.setService('SIBBACServiceCobros');
    data.setAction('insertarCobro');
    data.setFilters(filters);
    data.setParams(JSON.stringify(params));
    var request = requestSIBBAC(data);
    request.success(function (json) {
        if(json.error !== null && json !== undefined){
            alert(json.error);
        }else{
            $("section").load("views/facturas/CrearCobro.html");
        }
    });
}

function modificarImporteAplicado() {
    var importeAplicado = parseFloat($('#importeAplicado').val());
    var importePendiente = parseFloat($('#importePendiente').val());
    var saldoPendiente = parseFloat($('#saldoPendienteCobro').val());
    var importeVencido = parseFloat($('#importeVencidoCobro').val());

    if (importeAplicado > importePendiente) {
        alert("El importe aplicado es superior al importe pendiente de la factura");
        return false;
    }
    if (importeAplicado > saldoPendiente) {
        alert("El importe aplicado es superior al saldo pendiente del cobro");
        return false;
    }
    var idFactura = $('#idFactura').val();
    $('#importeAplicado' + idFactura).text($.number(importeAplicado, 2, ',', '.'));
    if ($('#ckCerrar').is(':checked')) {
        $('#ckCerrar' + idFactura).attr('checked', true);
    } else {
        $('#ckCerrar' + idFactura).removeAttr('checked');
    }
    $('#saldoPendiente').val($.number((saldoPendiente - importeAplicado), 2, ',', '.'));
    $('#importeVencido').val($.number((importeVencido + importeAplicado), 2, ',', '.'));
    document.getElementById('formulariosModificarCobro').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
}

function render() {
    console.log("se usa el render??");
}

function consultar(params) {
    console.log("se usa el consultar??");
}

function clickEditable(event) {
    event.preventDefault();
    var td, campo, valor, id;
    $("td:not(.id)").removeClass("editable");
    td = $(this).closest("td");
    campo = $(this).closest("td").data("campo");
    valor = $(this).text();
    id = $(this).closest("tr").find(".id").text();
    td.text("").html(
            "<input type='text' name='" + campo + "' value='" + valor
            + "'><a class='enlace guardar' href='#'>Guardar</a><a class='enlace cancelar' href='#'>Cancelar</a>");
}

function clickCancelar(event) {
    var td, campo, valor, id;
    td.html("<span>" + valor + "</span>");
    $("td:not(.id)").addClass("editable");
}

function clickGuardar(event) {
    event.preventDefault();
    var td, campo, valor, id;
    $(".mensaje").html("<img src='images/loading.gif'>");
    nuevovalor = $(this).closest("td").find("input").val();
    $.ajax({
        type: "POST",
        url: "editinplace.php",
        data: {
            campo: campo,
            valor: nuevovalor,
            id: id
        }
    }).done(function (msg) {
        $(".mensaje").html(msg);
        td.html("<span>" + nuevovalor + "</span>");
        $("td:not(.id)").addClass("editable");
        setTimeout(function () {
            $('.ok,.ko').fadeOut('fast');
        }, 3000);
    });
}

function getIdAlias() {
    var idAlias = "";
    var textAlias = document.getElementById("textAlias").value;
    if ((textAlias !== null) && (textAlias !== '')) {
        idAlias = idOcultos[availableTags.indexOf(textAlias)];
        if (idAlias === undefined) {
            idAlias = "";
        }
    }
    if (idAlias === "") {
        alert("El alias introducido no existe");
    }
    return idAlias;
}

function cargarTabla(idAlias) {
    /* parte ajax */
    var data = new DataBean();
    data.setService('SIBBACServiceFactura');
    data.setAction('getListadoFacturasPendienteCobros');
    var filters = "{ \"idAlias\" : \"" + idAlias + "\" }";
    data.setFilters(filters);

    var request = requestSIBBAC(data);
    request
            .success(function (json) {
                var datos = undefined;
                if (json === undefined || json.resultados === undefined || json.resultados.listadoFacturas === undefined) {
                    datos = {};
                } else {
                    datos = json.resultados.listadoFacturas;
                }

                oTable.fnClearTable();
                var factura = null;
                var idChk;
                for (var k in datos) {
                    factura = datos[k];
                    var idFactura = factura.idFactura;
                    var importePendiente = factura.importePendiente;
                    idsFacturas.push(idFactura);
                    idChk = "ckFila" + idFactura;
                    oTable.fnAddData([
                                "<input id='" + idChk + "' type='checkbox' class='checkTabla' idFactura='" + idFactura + "' numeroFactura='" + factura.numeroFactura + "' fechaFactura='" + transformaFecha(factura.fechaCreacion) + "' importePendiente='" + $.number(factura.importePendiente, 2, ',', '.') + "' importeTotal='" + $.number(factura.importeTotal, 2, ',', '.') + "'  />",
                                "<input id='ckCerrar" + idFactura + "' type='checkbox' class='checkTabla' disabled />",
                                "<span>" + factura.numeroFactura + "</span>",
                                "<span>" + transformaFecha(factura.fechaCreacion) + "</span>",
                                "<span>" + $.number(factura.baseImponible, 2, ',', '.') + "</span>",
                                "<span>" + $.number(factura.importeImpuestos, 2, ',', '.') + "</span>",
                                "<span>" + $.number(factura.importeTotal, 2, ',', '.') + "</span>",
                                "<div id=importePendiente" + idFactura + ">" + $.number(factura.importePendiente, 2, ',', '.') + "</div>",
                                "<div id=importeAplicado" + idFactura + ">" + $.number('0,00', 2, ',', '.') + "</div>",
                                "<a class=\"btn\" id='modificarImporte" + idFactura + "'><img id='img" + idFactura + "' src='img/editp.png' title='Editar'/></a>"]);
                    $("#" + idChk).click(function () {
                        var idFactura = $(this).attr('idFactura');
                        var importeCobro = parseFloat($('#importeCobro').val());
                        var importeVencido = parseFloat($('#importeVencido').val());
                        var saldoPendiente = parseFloat(importeCobro - importeVencido);
                        var importeAplicado = parseFloat($('#importeAplicado' + idFactura).text());
                        var importePendiente = parseFloat($('#importePendiente' + idFactura).text());
                        if ($(this).is(':checked')) {
                            // Hemos seleccionado la factura
                            if (saldoPendiente <= importePendiente) {
                                importeAplicado = saldoPendiente;
                            } else {
                                importeAplicado = importePendiente;
                            }
                            importeVencido = importeVencido + importeAplicado;
                            marcarChecks.push(idFactura);
                            $('#modificarImporte' + idFactura).click(function (e) {
                                e.preventDefault();
                                cargarInputsCobro(idFactura);
                            });
                        } else {
                            // Hemos deseleccionado la factura
                            importeVencido = importeVencido - importeAplicado;
                            importeAplicado = 0, 00;
                            marcarChecks.splice($.inArray(idFactura, marcarChecks), 1);
                            $('#modificarImporte' + idFactura).unbind('click');
                        }
                        if (marcarChecks.length === 0) {
                            $('#importeCobro').removeAttr('disabled');
                        } else {
                            $('#importeCobro').attr('disabled', true);
                        }
                        saldoPendiente = (importeCobro - importeVencido, 2, ',', '.');
                        $('#importeVencido').val($.number(importeVencido, 2, ',', '.'));
                        $('#saldoPendiente').val($.number(saldoPendiente, 2, ',', '.'));
                        $('#importeAplicado' + idFactura).text($.number(importeAplicado, 2, ',', '.'));
                    });
                }
            });
}

function cargarInputsCobro(idFactura) {
    document.getElementById('formulariosModificarCobro').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    $('#idFactura').val(idFactura);
    var checkIdFactura = $('#ckFila' + idFactura);
    $('#numeroFactura').val(checkIdFactura.attr('numeroFactura'));
    $('#fechaFactura').val(checkIdFactura.attr('fechaFactura'));
    $('#importeTotal').val(checkIdFactura.attr('importeTotal'));
    $('#importePendiente').val(checkIdFactura.attr('importePendiente'));
    var importeAplicado = parseFloat($('#importeAplicado' + idFactura).text());
    $('#importeAplicado').val($.number(importeAplicado, 2, ',', '.'));
    $('#ckCerrar').val($('#ckCerrar' + idFactura).val());
    var saldoPendiente = parseFloat($('#saldoPendiente').val());
    var importeVencido = parseFloat($('#importeVencido').val());
    $('#saldoPendienteCobro').val(saldoPendiente + importeAplicado);
    $('#importeVencidoCobro').val(importeVencido - importeAplicado);
}

function SendAsExport(format) {
    var c = this.document.forms['crearCobro'];
    var f = this.document.forms["export"];
    console.log("Forms: " + f.name + "/" + c.name);
    f.action = "/sibbac20back/rest/export." + format;
    var json = {
        "service": "SIBBACServiceFactura",
        "action": "getListadoFacturasPendienteCobros"
    };
    if (json != null && json != undefined && json.length == 0) {
        alert("Introduzca datos");
    } else {
        f.webRequest.value = JSON.stringify(json);
        f.submit();
    }
}
