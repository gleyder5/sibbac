var idOcultos = [];
var availableTags = [];
var oTable = undefined;

var ESTADO_SUGERIDA = 501;

$(document).ready(function () {
	cargarAlias();
	cargarEstados();
	oTable = $(".tablaPrueba").dataTable({
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
		"language": {
			"url": "i18n/Spanish.json"
		},
        "scrollY": "480px",
        "scrollCollapse": true,
		"columns": [
			{"width":"7%"},
			{"width":"30%"},
			{"width":"10%",type: 'date-eu'},
			{"width":"10%", sClass: "monedaR",type: "formatted-num"},
			{"width":"10%",type: 'date-eu'},
			{"width":"8%",type: 'date-eu'},
			{"width":"10%"},
			{"width":"15%"}
		]
	});
	$('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});
	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});
	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');
		$('editarParamPop').bPopup({
		modalClose: false,
		opacity: 0.6,
		positionStyle: 'fixed' //'fixed' or 'absolute'
	});
		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Opciones de búsqueda','Ocultar Opciones de búsqueda');
		return false;
	});
	$('.btn.buscador').click(function(){
		//miramos si el alias existe
		var listaAlias = $("#textAlias").val();
		posicionAlias = availableTags.indexOf(listaAlias);
		var listaEstados = $("#textEstado").val();
		if((posicionAlias=="-1")&&(listaAlias!="")){
			alert("El alias introducido no existe");
			return false;
		} else {
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar Opciones de búsqueda','Ocultar Opciones de búsqueda');
			var valorSeleccionadoAlias = "";
			if ( listaAlias==null || listaAlias==undefined || listaAlias=="" ) {
					valorSeleccionadoAlias = "";
			} else {
				posicionAlias = availableTags.indexOf(listaAlias);
				valorSeleccionadoAlias = idOcultos[posicionAlias];
			}
			//aquí asignamos el estado
			var estado = $("#textEstado").val();
			cargarTabla(valorSeleccionadoAlias, estado);
			return false;
		}
	});
	/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		//$('.resultados').hide();
		});
	}
	//cargarTabla();
});

//funcion para cargar la tabla
function cargarTabla(alias,estado) {
	//hay que crear el filtro con alias y estado
	var filtro = "{";
	if(alias!=""){
		filtro = filtro + "\"idAlias\" : \""+ alias + "\",";
	}
	if(estado!=""){
		filtro = filtro + "\"idEstado\" : \""+ estado + "\",";
	}
	if(filtro.length>1){
		filtro = filtro.substring(0,filtro.length-1);
	}
	filtro = filtro + "}";
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service/",
		data: "{\"service\" : \"SIBBACServiceFacturaSugerida\", \"action\"  : \"getListadoFacturasSugeridas\", \"filters\"  : "+filtro+"}",
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var datos = undefined;
			if (json === undefined || json.resultados === undefined || json.resultados.listadoFacturas === undefined) {
				datos={};
			} else {
				datos = json.resultados.listadoFacturas;
			}
			var item = null;
			var contador = 0;
			var wastebasket = null;
			oTable.fnClearTable();
			for ( var k in datos ) {
				item = datos[ k ];
				wastebasket = "";
				//miro los valores de info alias, y si están a 1 los pinto verdes, a 0 rojos
				if(item.nContacto=="1"){
					cont = "s";
					eCont = "";
				} else {
					cont = "n";
					eCont = "no ";
				}
				if(item.nDireccion=="1"){
					dir = "s";
					eDir = "";
				} else {
					dir = "n";
					eDir = "no ";
				}
				if(item.nCIF=="1"){
					cif = "s";
					eCif = "";
				} else {
					cif = "n";
					eCif = "no ";
				}
				if(item.comentarios==null){comentarios="";} else {comentarios=item.comentarios;}
				wastebasket = (item.idEstado==ESTADO_SUGERIDA) ? "&nbsp;<img src=\"img/del.png\" title=\"Eliminar Factura Sugerida\" width=\"12px\" onClick=\"DeleteFacturaSugerida("+item.idFacturaSugerida+")\">" : "";
				oTable.fnAddData([
					"<span ><img src='img/"+cont+"cont.gif' title='Contacto "+eCont+"asignado'>&nbsp;"
					+"<img src='img/"+dir+"direc.gif' title='Direcci&oacute;n "+eDir+"asignada'>&nbsp;"
					+"<img src='img/"+cif+"cif.gif' title='CIF "+eCif+"asignado'></span>" ,
					"<span >"+ item.cdAlias.trim()+" - "+item.nombreAlias +"</span>" ,
					"<span >"+transformaFecha(item.fechaCreacion)+"</span>",
					"<span class=\"monedaR\">"+$.number(item.importe, 2, ',','.')+"</span>",
					"<span >"+transformaFecha(item.fechaInicio)+"</span>",
					"<span >"+transformaFecha(item.fechaFin)+"</span>",
					"<span >"+item.descEstado+wastebasket+"</span>",
					"<span >"+comentarios+"</span>"
				]);
			}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}
//exportar
function DeleteFacturaSugerida( id ) {
	var datos = new DataBean();
	var filters = "{ \"idFacturaSugerida\" : \""+id+"\" }";
	datos.setService( "SIBBACServiceFacturaSugerida" );
	datos.setAction( "borrarSugeridas" );
	datos.setFilters( filters );

	inicializarLoading();
	var request = requestSIBBAC( datos );
	request.success( function( json ) {
		alert( json.error );
		$('.buscador').hide();
		$('.btn.buscador').click();
	});
}
//exportar
function SendAsExport( format ) {
	var c = this.document.forms['facturaconsulta'];
	var f = this.document.forms["export"];
	f.action="/sibbac20back/rest/export." + format;
	var json = {
		"service": "SIBBACServiceFacturaSugerida",
		"action": "getListadoFacturasSugeridas"};
	if ( json!=null && json!=undefined && json.length==0 ) {
		alert("Introduzca datos");
	} else {
		f.webRequest.value=JSON.stringify(json);
		f.submit();
	}
}
function cargarAlias() {
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service/",
		data: "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacion\"}",
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos={};
			} else {
				datos = json.resultados.listaAlias;
			}
			var resultados = datos;
			var item = null;
			for ( var k in resultados ) {
				item = resultados[ k ];
				idOcultos.push(item.id);
				ponerTag = item.nombre.trim();
				availableTags.push(ponerTag);
				}
			//código de autocompletar
			$( "#textAlias" ).autocomplete({
				source: availableTags
			});
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}
function cargarEstados() {
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service/",
		data: "{\"service\" : \"SIBBACServiceTmct0estado\", \"action\"  : \"getEstadosFacturaSugerida\"}",
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos={};
			} else {
				datos = json.resultados.estados;
			}
			var resultados = datos;
			var item = null;
			for ( var k in resultados ) {
				item = resultados[ k ];
				// Rellenamos el combo
				$('#textEstado').append(
						"<option value='" + item.idestado +"'>" + item.nombre +"</option>"
				);
				}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}
