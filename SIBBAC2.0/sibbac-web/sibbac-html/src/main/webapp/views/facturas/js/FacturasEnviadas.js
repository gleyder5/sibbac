$(document).ready(function () {

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Buscar Factura','Ocultar Buscar Factura');
		return false;
	});

/*BUSCADOR*/
	$('.btn.buscador').click(function(){
		//event.preventDefault();
			$('.resultados').show();
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');
			cargarTabla();
		return false;
	});
/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}

	cargarAlias();
	cargarEstados();


});


function cargarAlias()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacion\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var resultados = json.resultados.listaAlias;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				// Rellenamos el combo
				$('#textAlias').append(
						"<option value='" + item.id +"'>" + item.nombre +"</option>"

				);

				}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}



function cargarTabla()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceFactura\", \"action\"  : \"getListadoFacturasEnviadas\"}",
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
		    datos = undefined;
			if (json === undefined || json.resultados === undefined || json.resultados.listadoFacturas === undefined) {
                datos={};
            } else {
                datos = json.resultados.listadoFacturas;
            }
			var resultados = datos;
			var item = null;
			var contador = 0;

			for ( var k in resultados ) {
				item = resultados[ k ];
				if (contador%2==0) {
					estilo = "even";
				} else {
					estilo = "odd";
				}

				// Pintamos la tabla "padre". La cabecera...
					$('.tablaPrueba').append(
							"<tr class='"+ estilo +"'>" +
								"<td class='taleft' ><input type='checkbox' class='checkTabla' id='checkTabla' name='checkTabla' /><input type='hidden' name='idmarcar"+contador+"' id='idmarcar"+contador+"' value='"+item.idFactura + "' /></td>" +
								"<td class='taleft'>"+item.descEstado + "</td>" +
								"<td class='taleft'>"+item.nombreAlias + "</td>" +
								"<td class='taright'>"+transformaFecha(item.fechaCreacion) + "</td>" +
								"<td class='taleft'>"+item.baseImponible + "</td>" +
								"<td class='taleft'>"+item.importeImpuestos + "</td>" +
								"<td class='taleft'>"+item.importeTotal + "</td>" +
						 "</tr>"

					);
					contador++;

				}

				// "Footer" de la tabla "padre".
				$('.tablaPrueba').append( "</table>" );

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});

}




//listado estados
function cargarEstados()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceTmct0estado\", \"action\"  : \"getEstadosFactura\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var resultados = json.resultados.estados;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				// Rellenamos el combo
				$('#textEstado').append(
						"<option value='" + item.idestado +"'>" + item.nombre +"</option>"

				);

				}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}
