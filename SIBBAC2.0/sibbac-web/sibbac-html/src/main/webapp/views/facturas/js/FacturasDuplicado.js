var idOcultos = [];
var availableTags = [];
$(document).ready(function () {

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Opciones de Búsqueda','Ocultar Opciones de Búsqueda');
		return false;
	});

/*BUSCADOR*/
	$('.btn.buscador').click(function(){
		//event.preventDefault();
			$('.resultados').show();
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');
			cargarTabla();
		return false;
	});
/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}


	cargarAlias();
	cargarEstados();


});


function cargarAlias()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacion\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined ) {
                datos={};
            } else {
                datos = json.resultados.listaAlias;
            }
			var resultados = datos;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				idOcultos.push(item.id);
				ponerTag = item.nombre.trim();
				availableTags.push(ponerTag);
				}
			//código de autocompletar

			$( "#textAlias" ).empty();
           $( "#textAlias" ).autocomplete({
             source: availableTags
           });
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}



function cargarTabla()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceFactura\", \"action\"  : \"getListadoFacturas\"}",
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined || json.resultados.listadoFacturas === undefined) {
                datos={};
            } else {
                datos = json.resultados.listadoFacturas;
            }
			var resultados = datos;
			var item = null;
			var contador = 0;

			$('.tablaPrueba').empty();


			$('.tablaPrueba').append(
					"<thead>"+
					"<tr>"+
						"<th class='taleft'>Seleccionado</th>"+
						"<th class='taleft'>N&uacute;mero factura</th>"+
						"<th class='taleft'>Alias</th>"+
						"<th class='taleft'>Fecha factura</th>"+
						"<th class='tacenter'>Descripci&oacute;n</th>"+
						"<th class='taleft'>Base Imponible</th>"+
						"<th class='taleft'>Importe Impuestos</th>"+
						"<th class='taleft'>Importe Total</th>"+

					"</tr>"+
				"</thead>"
			);


			for ( var k in resultados ) {
				item = resultados[ k ];
				if (contador%2==0) {
					estilo = "even";
				} else {
					estilo = "odd";
				}

				pinto = item.idEstado;

				// Pintamos la tabla "padre". La cabecera...
					$('.tablaPrueba').append(
							"<tr class='"+ estilo +"'>" +
								"<td class='taleft' data-campo='selec'><input id='ckFila1' type='checkbox' class='checkTabla'/></td>" +
								"<td class='taleft'>"+item.numeroFactura + "</td>" +
								"<td class='taleft'>"+item.nombreAlias + "</td>" +
								"<td class='taright'>"+transformaFecha(item.fechaCreacion) + "</td>" +
								"<td class='taleft'>"+item.descEstado + "</td>" +
								"<td class='taright'>"+item.baseImponible + "</td>" +
								"<td class='taright'>"+item.importeImpuestos + "</td>" +
								"<td class='taright'>"+item.importeTotal + "</td>" +
						 "</tr>"

					);
					contador++;

				}

				// "Footer" de la tabla "padre".
				$('.tablaPrueba').append( "</table>" );

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});

}


//listado estados
function cargarEstados()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceTmct0estado\", \"action\"  : \"getEstadosFactura\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var resultados = json.resultados.estados;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				// Rellenamos el combo
				$('#textEstado').append(
						"<option value='" + item.idestado +"'>" + item.nombre +"</option>"

				);

				}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}


