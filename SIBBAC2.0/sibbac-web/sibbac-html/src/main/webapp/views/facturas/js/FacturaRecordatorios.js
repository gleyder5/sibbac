$(document).ready(function () {

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		$('editarParamPop').bPopup({
        modalClose: false,
        opacity: 0.6,
        positionStyle: 'fixed' //'fixed' or 'absolute'
	});
		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Búsqueda Recordatorios','Ocultar Búsqueda Recordatorios');
		return false;
	});

/*BUSCADOR*/
	$('.btn.buscador').click(function(){
		//event.preventDefault();
			$('.resultados').show();
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');

		return false;
	});
/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		//$('.resultados').hide();
		});
	}

	cargarTabla();

});


//funcion para cargar la tabla
function cargarTabla()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceFacturaSugerida\", \"action\"  : \"getListadoFacturasRecordatorio\"}",
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined || json.resultados.listadoFacturas === undefined) {
                datos={};
            } else {
                datos = json.resultados.listadoFacturas;
            }
			var resultados = datos;
			var item = null;
			var contador = 0;

			for ( var k in resultados ) {
				item = resultados[ k ];
				if (contador%2==0) {
					estilo = "even";
				} else {
					estilo = "odd";
				}


				// Pintamos la tabla "padre". La cabecera...
					$('.tablaPrueba').append(
							"<tr class='"+ estilo +"'>" +
								"<td class='taleft' ><input type='checkbox' class='checkTabla' id='checkTabla' name='checkTabla' /><input type='hidden' name='idmarcar"+contador+"' id='idmarcar"+contador+"' value='"+item.idFacturaSugerida + "' /></td>" +
								"<td class='taleft'>"+item.descEstado + "</td>" +
								"<td class='taleft'>"+item.nombreAlias + "</td>" +
								"<td class='taright'>"+transformaFecha(item.fechaCreacion) + "</td>" +
								"<td class='taleft'>"+item.descPeriodicidad + "</td>" +
								"<td class='taright'>"+item.importe + "</td>" +
						 "</tr>"

					);
					contador++;

				}

				// "Footer" de la tabla "padre".
				$('.tablaPrueba').append( "</table>" );

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});

}

//marcar y desmarcar
function clickSeleccionarTodos() {
	var fila = $('.tablaPrueba').find('td input.checkTabla');
	fila.prop('checked',true);
}


function clickDesmarcarTodos() {
	var fila = $('.tablaPrueba').find('td input.checkTabla');
	fila.prop('checked',false);
}

//Evento para marcar facturas
$('#btnMarcar').click(function(){
		var suma = 0;
		var valores = new Array()
		var los_cboxes = document.getElementsByName('checkTabla');
		for (var i = 0, j = los_cboxes.length; i < j; i++) {

			if(los_cboxes[i].checked == true){
				valor = document.getElementById("idmarcar"+i).value;
				valores.push(valor);
				suma++;
			}
		}

		if(suma == 0){
			alert('Marque al menos una línea');
			return false;
		}else{
			marcarFacturas(valores);
		}
})

//Funcion para marcar facturas
function marcarFacturas(valores)
{
	bucleVal = valores.length;
	console.log("entro en el metodo");

	var params = "[ ";
	for (var v = 0; v < bucleVal; v++) {
		params = params + "{ \"idFacturaSugerida\" :\""+valores[v]+ "\" },";
	}
	params = params.substring(0,params.length-1);
	params = params + "\ ]";


	var jsonData = "{" +
			"\"service\" : \"SIBBACServiceFacturaSugerida\", " +
			"\"action\"  : \"marcarFacturar\", "+
			"\"params\"  : "+ params + "}";
	console.log(jsonData);
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: jsonData,
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			$("section").load("views/facturas/CrearFactura.html");
		},
		error: function(c) {
			alert( "ERROR: " + c );
		}

	});
}



