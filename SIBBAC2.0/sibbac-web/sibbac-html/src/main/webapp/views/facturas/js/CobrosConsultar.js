var idOcultos = [];
var availableTags = [];
var oTable = undefined;

$(document).ready(function() {
	oTable = $(".tablaPrueba").dataTable({
		"dom" : 'T<"clear">lfrtip',
		"tableTools" : {
			"sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
		"language" : {
			"url" : "i18n/Spanish.json"
		},
        "scrollY": "480px",
        "scrollCollapse": true,
		"columns" : [ {
			"width" : "auto"
		}, {
			"width" : "auto"
		}, {
			"width" : "auto",
			type : "date-eu"
		}, {
			"width" : "auto",
			sClass : "monedaR",
			type : "formatted-num"
		}, {
			"width" : "auto",
			sClass : "monedaR",
			type : "formatted-num"
		} ]
	});

	$('a[href="#release-history"]').toggle(function() {
		$('#release-wrapper').animate({
			marginTop : '0px'
		}, 600, 'linear');
	}, function() {
		$('#release-wrapper').animate({
			marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function() {
		_gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
	});

	$('.collapser').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar', 'Clic para ocultar');

		$('editarParamPop').bPopup({
			modalClose : false,
			opacity : 0.6,
			positionStyle : 'fixed' // 'fixed' or 'absolute'
		});
		return false;
	});
	$('.collapser_search').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Búsqueda Cobro', 'Ocultar Búsqueda Cobro');
		return false;
	});
	/* BUSCADOR */
	$('#cobrosConsultar').submit(function(event) {
		event.preventDefault();
		$('.mensajeBusqueda').empty();
		var idAlias = getIdAlias();
		var fechaDesde = document.getElementById("fechaDesde").value;
		var fechaHasta = document.getElementById("fechaHasta").value;
		var numeroCobro = document.getElementById("numeroCobro").value;
		var textAlias = document.getElementById("textAlias").value;
		// Ejecutamos la llamada
		var fechas = [ 'fechaDesde', 'fechaHasta' ];
		loadpicker(fechas);
		verificarFechas([ [ 'fechaDesde', 'fechaHasta' ] ]);
		var fechaCont = "";
		if ((fechaDesde !== null) && (fechaDesde !== undefined)) {
			fechaCont += fechaDesde;
		} else {
			fechaCont = '***';
		}
		fechaCont += '-';
		if ((fechaHasta !== null) && (fechaHasta !== undefined)) {
			fechaCont += fechaHasta;
		} else {
			fechaCont = '***';
		}
		cargarTabla(idAlias, fechaDesde, fechaHasta, numeroCobro);
		cadenaFiltros = "Alias: " + textAlias + " / Fecha de contabilización: " + fechaCont + " / Número Cobro: " + numeroCobro + "";
		$('.mensajeBusqueda').append(cadenaFiltros);
		$('.collapser_search').parents('.title_section').next().slideToggle();
		$('.collapser_search').toggleClass('active');
		$('.collapser_search').toggleText('Mostrar opciones de búsqueda', 'Ocultar opciones de búsqueda');
		return false;
	});

	/*
	 * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
	 * TABLA
	 */
	if ($('.contenedorTabla').length >= 1) {
		var anchoBotonera;
		$('.contenedorTabla').each(function(i) {
			anchoBotonera = $(this).find('table').outerWidth();
			$(this).find('.botonera').css('width', anchoBotonera + 'px');
			$(this).find('.resumen').css('width', anchoBotonera + 'px');
		});
	}

	cargarAlias();
	var fechas = [ 'fechaDesde', 'fechaHasta' ];
	loadpicker(fechas);
	verificarFechas([ [ 'fechaDesde', 'fechaHasta' ] ]);
	cargarTabla('', '', '', '');

});

function cargarAlias() {
	var datos = new DataBean();
	datos.setService("SIBBACServiceAlias");
	datos.setAction("getListaAliasFacturacion");
	var request = requestSIBBAC(datos);
	request.success(function(json) {
		var resultados = json.resultados.listaAlias;

		var item = null;
		$('#textAlias').empty();
		for ( var k in resultados) {
			item = resultados[k];
			idOcultos.push(item.id);
			ponerTag = item.nombre.trim();
			availableTags.push(ponerTag);
		}
		// código de autocompletar

		$("#textAlias").autocomplete({
			source : availableTags
		});
	});
	request.error(function(a, b, c) {
		alert("ERROR cargando los alias");
		console.log("ERROR: " + a);
		console.log("ERROR: " + b);
		console.log("ERROR: " + c);
	});
}

function getIdAlias() {
	var idAlias = "";
	var textAlias = document.getElementById("textAlias").value;
	if ((textAlias !== null) && (textAlias !== '')) {
		idAlias = idOcultos[availableTags.indexOf(textAlias)];
		if (idAlias === undefined) {
			idAlias = "";
		}
	}
	return idAlias;
}

// funcion para cargar la tabla
function cargarTabla(idAlias, fechaDesde, fechaHasta, numeroCobro) {
	var datos = new DataBean();
	datos.setService("SIBBACServiceCobros");
	datos.setAction("getListaCobro");
	var filtros = "{\"idAlias\" : \"" + idAlias + "\"," + "\"fechaDesde\" : \"" + transformaFechaInv(fechaDesde) + "\","
			+ "\"fechaHasta\" : \"" + transformaFechaInv(fechaHasta) + "\"," + "\"numCobro\" : \"" + numeroCobro + "\"}";
	datos.setFilters(filtros);
	var request = requestSIBBAC(datos);

	// El tratamiento de la respuesta.
	request
			.success(function(json) {
				var datos = undefined;
				if (json === undefined || json.resultados === undefined) {
					datos = {};
				} else {
					datos = json.resultados;
				}
				oTable.fnClearTable();
				var item = null;
				var contador = 0;
				for ( var k in datos) {
					item = datos[k];
					oTable
							.fnAddData([
									item.nombreAlias,
									item.cobro
											+ "&nbsp;<div class='lupa'><a href='#' onclick = \"document.getElementById('formulariosDatosFactura').style.display='block';document.getElementById('fade').style.display='block';cargoInputsFactura('"
											+ item.numeroFactura + "','" + item.id + "'); \"><img src='img/lupa.gif'></a></div>",
									// casca formato raro para la funcion
									// transformaFechaGuion(item.fechaContabilizacion),
									"<span>" + EpochToHuman(item.fechaContabilizacion) + "</span>",
									"<span>" + a2digitos(item.importeCobrado) + "</span>",
									"<span>" + a2digitos(item.comisiones) + "</span>" ]);
					contador++;
				}
			});
	request.error(function(a, b, c) {
		alert("ERROR cargando las combinaciones del alias " + alias);
		if (SHOW_LOG)
			console.error("ERROR: " + a);
		if (SHOW_LOG)
			console.error("ERROR: " + b);
		if (SHOW_LOG)
			console.error("ERROR: " + c);
	});

}

// marcar y desmarcar
function clickSeleccionarTodos() {
	var fila = $('.tablaPrueba').find('td input.checkTabla');
	fila.prop('checked', true);
}

function clickDesmarcarTodos() {
	var fila = $('.tablaPrueba').find('td input.checkTabla');
	fila.prop('checked', false);
}

// Evento para marcar facturas
$('#btnMarcar').click(function() {
	var suma = 0;
	var valores = new Array()
	var los_cboxes = document.getElementsByName('checkTabla');
	for (var i = 0, j = los_cboxes.length; i < j; i++) {

		if (los_cboxes[i].checked == true) {
			valor = document.getElementById("idmarcar" + i).value;
			valores.push(valor);
			suma++;
		}
	}

	if (suma == 0) {
		alert('Marque al menos una línea');
		return false;
	} else {
		marcarFacturas(valores);
	}
})

function SendAsExport(format) {
	var c = this.document.forms['cobrosConsultar'];
	var f = this.document.forms["export"];
	f.action = "/sibbac20back/rest/export." + format;
	var json = {
		"service" : "SIBBACServiceCobros",
		"action" : "getListaCobro"
	};
	if (json != null && json != undefined && json.length == 0) {
		alert("Introduzca datos");
	} else {
		f.webRequest.value = JSON.stringify(json);
		f.submit();
	}
}

// listado lupa
function cargoInputsFactura(num, id) {
	// $('#df').empty();
	// $('#df').append(num);
	$('#tablaDatos').empty();
	var filtro = "{\"idCobros\" : \"" + id + "\" }";
	var jsonData = "{" + "\"service\" : \"SIBBACServiceLineaDeCobro\", " + "\"action\"  : \"getFacturaLineaDeCobros\", "
			+ "\"filters\"  : " + filtro + "}";
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",

		data : jsonData,

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos = {};
			} else {
				datos = json.resultados;
			}
			var resultados = datos;
			var item = null;
			$('#tablaDatos').append("<tr>" + "<th>Importe Aplicado</th>" + "<th>Num. Factura</th>" + "<th>Fecha Factura</th>" + "</tr>");
			for ( var k in resultados) {
				item = resultados[k];
				// Rellenamos la tabla

				$('#tablaDatos').append(
						"<tr>" + "<td class='taright'>" + $.number(item.importeAplicado,2,',','.') + "</td>" + "<td class='taright'>" + item.numeroFactura
								+ "</td>" + "<td class='tacenter'>" + EpochToHuman(item.fechaFactura) + "</td>" + "</tr>");

			}
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}
