var idOcultos = [];
var availableTags = [];
var oTable = undefined;

$(document).ready(function() {
	oTable = $(".tablaResultados").dataTable({
		"dom" : 'T<"clear">lfrtip',
		"tableTools" : {
			"sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
		"language" : {
			"url" : "i18n/Spanish.json"
		},
        "scrollY": "480px",
        "scrollCollapse": true,
		"columns" : [ {
			"width" : "auto"
		}, {
			"width" : "auto"
		}, {
			"width" : "auto",
			type : 'date-eu'
		}, {
			"width" : "auto"
		}, {
			"width" : "auto"
		}, {
			"width" : "auto"
		}, {
			"width" : "auto",
			type : 'date-eu'
		}, {
			"width" : "auto",
			type : 'date-eu'
		} ]
	});

	$('a[href="#release-history"]').toggle(function() {
		$('#release-wrapper').animate({
			marginTop : '0px'
		}, 600, 'linear');
	}, function() {
		$('#release-wrapper').animate({
			marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function() {
		_gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
	});

	$('.collapser').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar', 'Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function() {
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Opciones de Búsqueda', 'Ocultar Opciones de Búsqueda');
		return false;
	});

	/* BUSCADOR */
	$('#cargarTabla').submit(function(event){
		event.preventDefault();
		if (validarCampoFecha('fechaDesde', false) && validarCampoFecha('fechaHasta', false)) {
			var idAlias = getIdAlias();
			var fechaDesde = document.getElementById('fechaDesde').value;
			var fechaHasta = document.getElementById('fechaHasta').value;
			var fechas = [ 'fechaDesde', 'fechaHasta' ];
			loadpicker(fechas);
			verificarFechas([ fechas ]);
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			cargarTablaResultados(idAlias, fechaDesde, fechaHasta);
		}else {
			alert("Los campos de fecha introducidos no son correctos. Por favor revíselos.");
		}
	});
	/*
	 * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
	 * TABLA
	 */
	if ($('.contenedorTabla').length >= 1) {
		var anchoBotonera;
		$('.contenedorTabla').each(function(i) {
			anchoBotonera = $(this).find('table').outerWidth();
			$(this).find('.botonera').css('width', anchoBotonera + 'px');
			$(this).find('.resumen').css('width', anchoBotonera + 'px');
		});
	}

	cargarAlias();
	var fechas = [ 'fechaDesde', 'fechaHasta' ];
	loadpicker(fechas);
	verificarFechas([ fechas ]);
	cargarTablaResultados('', '', '');
});


function getIdAlias() {
	var idAlias = "";
	var textAlias = document.getElementById("textAlias").value;
	if ((textAlias !== null) && (textAlias !== '')) {
		idAlias = idOcultos[availableTags.indexOf(textAlias)];
		if (idAlias === undefined) {
			idAlias = "";
		}
	}
	return idAlias;
}

function cambioAlias() {
	document.getElementById('nuFactura').value = "";
}

function cambioNuFactura() {
	document.getElementById('textAlias').value = "";
	document.getElementById('fechaDesde').value = "";
	document.getElementById('fechaHasta').value = "";

}
function cargarAlias() {
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAliasFacturacion\"}",

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined) {
				datos = {};
			} else {
				datos = json.resultados.listaAlias;
			}
			var resultados = datos;
			var item = null;

			for ( var k in resultados) {
				item = resultados[k];
				idOcultos.push(item.id);
				ponerTag = item.nombre.trim();
				availableTags.push(ponerTag);
			}
			// código de autocompletar

			$("#textAlias").autocomplete({
				source : availableTags
			});
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

function cargarTablaResultados(idAlias, fechaDesde, fechaHasta)
{
	var filtro = "{\"idAlias\" : \"" + idAlias + "\"," + "\"fechaDesde\" : \"" + transformaFechaInv(fechaDesde) + "\","
			+ "\"fechaHasta\" : \"" + transformaFechaInv(fechaHasta) + "\" }";

	var jsonData = "{" + "\"service\" : \"SIBBACServiceFacturaRectificativa\", " + "\"action\"  : \"getListadoFacturasRectificativas\", "
			+ "\"filters\"  : " + filtro + "}";

	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			var item = null;
			var datos = undefined;
			if (json === undefined || json.resultados === undefined || json.resultados.listadoFacturas === undefined
					|| json.resultados.listadoFacturas.length === 0) {
				datos = {};
			} else {
				datos = json.resultados.listadoFacturas;
			}

			oTable.fnClearTable();
			// var row;

			for ( var k in datos) {
				item = datos[k];
				oTable
						.fnAddData([ "<span >" + item.numeroFactura + "</span>",
								"<span >" + item.cdAlias.trim() + " - " + item.nombreAlias + "</span>",
								"<span >" + transformaFecha(item.fechaCreacion) + "</span>",
								"<span class=\"monedaR\">" + $.number(item.baseImponible, 2, ',','.') + "</span>",
								"<span class=\"monedaR\">" + $.number(item.importeImpuestos, 2, ',','.') + "</span>",
								"<span class=\"monedaR\">" + $.number(item.importeTotal, 2, ',','.') + "</span>",
								"<span >" + transformaFecha(item.fechaInicio) + "</span>",
								"<span >" + transformaFecha(item.fechaFin) + "</span>" ]);
			}
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});

}

// listado estados
function cargarEstados() {
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : "{\"service\" : \"SIBBACServiceTmct0estado\", \"action\"  : \"getEstadosFactura\"}",

		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			var resultados = json.resultados.estados;
			var item = null;

			for ( var k in resultados) {
				item = resultados[k];
				// Rellenamos el combo
				$('#textEstado').append("<option value='" + item.idestado + "'>" + item.nombre + "</option>"

				);

			}
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}
