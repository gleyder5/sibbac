
$(document).ready(function () {
    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});
    	/*BUSCADOR*/
	$('.btn.buscador').click(function(){
		//event.preventDefault();
			$('.resultados').show();
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');

		    var listaAlias = document.getElementById("textAlias").value;

			posicionAlias = availableTags.indexOf(listaAlias);

			valorSeleccionadoAlias = idOcultos[posicionAlias];



			fechaDesde = document.getElementById('textFechaDesde').value;

			fechaHasta = document.getElementById('textFechaHasta').value;

			estado = document.getElementById('textEstado').value;

			cargarTabla(valorSeleccionadoAlias, fechaDesde, fechaHasta, estado);

		return false;
	});
	/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}
    //COLLAPSER

    $('.collapser').click(function () {
        $(this).parents('.title_section').next().slideToggle();
        //$(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');
        return false;
    });
    $('.collapser_search').click(function () {
        $(this).parents('.title_section').next().slideToggle();
        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
        $(this).toggleClass('active');
        $(this).toggleText('Mostrar opciones de búsqueda', 'Ocultar opciones de búsqueda');
        return false;
    });

 //cargarTabla();

    cargarEstados();
});


function cargarTabla(valorSeleccionadoAlias, fechaDesde, fechaHasta, estado){

	var filtro = "{\"idAlias\" : \""+ valorSeleccionadoAlias + "\","+
	  "\"fechaDesde\" : \""+ transformaFechaInv(fechaDesde) + "\","+
	  "\"fechaHasta\" : \""+ transformaFechaInv(fechaHasta) + "\","+
	  "\"idEstado\" : \""+ estado + "\" }";

	var jsonData = "{" + "\"service\" : \"SIBBACServiceFacturaRectificativa\", " +
	"\"action\"  : \"getListarFacturas\", "+
	"\"filters\"  : "+ filtro + "}";


	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data:jsonData,
		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			datos = undefined;
			if (json === undefined || json.resultados === undefined ) {
                datos={};
            } else {
                datos = json.resultados;
            }
			var resultados = datos;
			var item = null;
			var contador = 0;

			for ( var k in resultados ) {
				item = resultados[ k ];
				if (contador%2==0) {
					estilo = "even";
				} else {
					estilo = "odd";
				}


				$('.tablaPrueba').append(
						"<tr class='"+ estilo +"'>" +
							"<td class='taleft' ><input type='checkbox' class='checkTabla' id='checkTabla' name='checkTabla' /><input type='hidden' name='idmarcar"+contador+"' id='idmarcar"+contador+"' value='"+item.idFacturaSugerida + "' /></td>" +
							"<td class='taleft'>"+item.descEstado + "</td>" +
							"<td class='taleft'>"+item.nombreAlias + "</td>" +
							"<td class='taright'>"+transformaFecha(item.fechaCreacion) + "</td>" +
							"<td class='taleft'>"+item.descPeriodicidad + "</td>" +
							"<td class='taleft'>"+item.importe + "</td>" +
					 "</tr>"

				);
				contador++;
				}

				// "Footer" de la tabla "padre".
				$('.tablaPrueba').append( "</table>" );

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}

//listado estados
function cargarEstados()
{
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceTmct0estado\", \"action\"  : \"getEstadosFactura\"}",

		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {
			var resultados = json.resultados.estados;
			var item = null;

			for ( var k in resultados ) {
				item = resultados[ k ];
				// Rellenamos el combo
				$('#textEstado').append(
						"<option value='" + item.idestado +"'>" + item.nombre +"</option>"

				);

				}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}
