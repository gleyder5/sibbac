var oTable = undefined;
var contadorI = new Array();
$(document).ready(function() {
	oTable = $(".tablaPrueba").dataTable({
		"dom" : 'T<"clear">lfrtip',
		"tableTools" : {
			"sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
		},
        "scrollY": "480px",
        "scrollCollapse": true,
		"language" : {
			"url" : "i18n/Spanish.json"
		}
	});

	$('a[href="#release-history"]').toggle(function() {
		$('#release-wrapper').animate({
			marginTop : '0px'
		}, 600, 'linear');
	}, function() {
		$('#release-wrapper').animate({
			marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function() {
		_gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
	});

	/*
	 * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
	 * TABLA
	 */
	if ($('.contenedorTabla').length >= 1) {
		var anchoBotonera;
		$('.contenedorTabla').each(function(i) {
			anchoBotonera = $(this).find('table').outerWidth();
			$(this).find('.botonera').css('width', anchoBotonera + 'px');
			$(this).find('.resumen').css('width', anchoBotonera + 'px');
		});
	}
	// cargo los idiomas en el popupoculto nada más empezar
	cargarIdiomas();
	cargarTabla();

});

$('#editarTexto').click(
		function() {

			idTexto = document.getElementById('textIdTexto').value;
			modifTipo = document.getElementById('textModifTipo').value;
			modifDescripcion = document.getElementById('textModifDescripcion').value;

			var filtro = "{\"id\" : \"" + idTexto + "\"," + "\"tipo\" : \"" + modifTipo + "\"," + "\"descripcion\" : \"" + modifDescripcion
					+ "\" }";

			var jsonData = "{" + "\"service\" : \"SIBBACServiceTexto\", " + "\"action\"  : \"modificarTexto\", " + "\"filters\"  : "
					+ filtro + "}";

			$.ajax({
				type : "POST",
				dataType : "json",
				url : "/sibbac20back/rest/service",
				data : jsonData,
				beforeSend : function(x) {
					if (x && x.overrideMimeType) {
						x.overrideMimeType("application/json");
					}
					// CORS Related
					x.setRequestHeader("Accept", "application/json");
					x.setRequestHeader("Content-Type", "application/json");
					x.setRequestHeader("X-Requested-With", "HandMade");
					x.setRequestHeader("Access-Control-Allow-Origin", "*");
					x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
				},
				async : true,
				success : function(json) {
					$("section").load("views/configuracion/Textos.html");
				},
				error : function(c) {
					alert("ERROR: " + c);
				}

			});

		})

function cargarTabla() {
	$
			.ajax({
				type : "POST",
				dataType : "json",
				url : "/sibbac20back/rest/service",
				data : "{\"service\" : \"SIBBACServiceTexto\", \"action\"  : \"getListaTextos\"}",
				beforeSend : function(x) {
					if (x && x.overrideMimeType) {
						x.overrideMimeType("application/json");
					}
					// CORS Related
					x.setRequestHeader("Accept", "application/json");
					x.setRequestHeader("Content-Type", "application/json");
					x.setRequestHeader("X-Requested-With", "HandMade");
					x.setRequestHeader("Access-Control-Allow-Origin", "*");
					x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
				},
				async : true,
				success : function(json) {
					if (json === undefined || json.resultados === undefined) {
						datos = {};
					} else {
						var status = json.resultados.status;
						if (status === "OK") {
							var listado = json.resultados.listado;
							oTable.fnClearTable();
							for ( var k in listado) {
								var tipo = "";
								var item = listado[k];
								if (item.tipo == "C") {
									tipo = "Concepto";
								}
								if (item.tipo == "K") {
									tipo = "Recordatorio(Asunto)";
								}
								if (item.tipo == "L") {
									tipo = "Recordatorio(Cuerpo)";
								}
								if (item.tipo == "B") {
									tipo = "Cobro(Asunto)";
								}
								if (item.tipo == "D") {
									tipo = "Cobro(Cuerpo)";
								}
								if (item.tipo == "I") {
									tipo = "Factura(Asunto)";
								}
								if (item.tipo == "J") {
									tipo = "Factura(Cuerpo)";
								}
								if (item.tipo == "G") {
									tipo = "Anulaci&oacute;n(Asunto)";
								}
								if (item.tipo == "H") {
									tipo = "Anulaci&oacute;n(Cuerpo)";
								}
								if (item.tipo == "E") {
									tipo = "Duplicado(Asunto)";
								}
								if (item.tipo == "F") {
									tipo = "Duplicado(Cuerpo)";
								}

								if (item.tipo == "M") {
									tipo = "Nº Factura";
								}

								if (item.tipo == "N") {
									tipo = "Concepto de factura";
								}

								if (item.tipo == "O") {
									tipo = "Importe de factura";
								}

								if (item.tipo == "P") {
									tipo = "Importe base de factura";
								}

								if (item.tipo == "Q") {
									tipo = "Impuestos ";
								}

								if (item.tipo == "R") {
									tipo = "Total";
								}

								if (item.tipo == "S") {
									tipo = "Cuenta bancaria";
								}

								if (item.tipo == "T") {
									tipo = "IBAN";
								}

								if (item.tipo == "U") {
									tipo = "Código BIC";
								}

								oTable
										.fnAddData([
												'<span>' + tipo + '</span>',
												'<span>' + item.descripcion + '</span>',
												"<a href='javascript:void(0)' onclick = \"document.getElementById('formulariosTraducirTexto').style.display='block';document.getElementById('fade').style.display='block';cargoInputsTraducir('"
														+ item.id
														+ "'); \"><img src='img/trad.gif' title='Traducir'/></a>&nbsp;<a href='javascript:void(0)' onclick = \"document.getElementById('formulariosModificarTexto').style.display='block';document.getElementById('fade').style.display='block';cargoInputsContacto('"
														+ item.id
														+ "','"
														+ item.descripcion
														+ "','"
														+ item.tipo
														+ "'); \"><img src='img/editp.png' title='Editar'/></a>"

										]);
							}
						}

					}
				}
			});

}

function cargoInputsContacto(id, descripcion, tipo) {

	document.getElementById('textIdTexto').value = id;
	document.getElementById('textModifTipo').value = tipo;
	document.getElementById('textModifDescripcion').value = descripcion;
}

function cargoInputsTraducir(id) {

	cargarEtiquetas();
	$('.exito').empty();
	// llamada al servicio de SIBBACServiceTextosIdiomas -
	// findTextoIdiomaByIdTexto
	var filtro = "{\"idTexto\" : \"" + id + "\" }";
	var jsonData = "{" + "\"service\" : \"SIBBACServiceTextosIdiomas\", " + "\"action\"  : \"findTextoIdiomaByIdTexto\", "
			+ "\"filters\"  : " + filtro + "}";

	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			// cargo los datos, bucle con los id de idioma

			var resultados = json.resultados.resultado;

			var item = null;

			for ( var k in resultados) {

				item = resultados[k];
				// para cada dato, lo pongo en su id correspondiente
				document.getElementById("textoIdioma" + item.idIdioma).value = item.descripcion;
				document.getElementById("idtextoIdioma" + item.idIdioma).value = item.idTextoIdioma;

			}
			document.getElementById("idTextoOculto").value = id;
		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

function updateTextoIdiomas(id) {

	descripcion = document.getElementById("textoIdioma" + id).value;
	textoIdioma = document.getElementById("idtextoIdioma" + id).value;
	idtexto = document.getElementById("idTextoOculto").value;
	ididioma = id;

	// Escapamos caracteres, en caso de haberlos.
	descripcion = encodeURIComponent(descripcion);

	// llamada al servicio de SIBBACServiceTextosIdiomas - updateTextosIdiomas
	var filtro = "{\"id\" : \"" + textoIdioma + "\",\"descripcion\" : \"" + descripcion + "\",\"idTexto\" : \"" + idtexto
			+ "\",\"idIdiomas\" : \"" + ididioma + "\" }";

	var jsonData = "{" + "\"service\" : \"SIBBACServiceTextosIdiomas\", " + "\"action\"  : \"updateTextosIdiomas\", " + "\"filters\"  : "
			+ filtro + "}";

	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			// cambio dato
			$('.exito').empty();
			$('.exito').append("Dato cambiado con &eacute;xito");

		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

$('#guardarTextos').click(function() {

	numIdioma = document.getElementById("idiomaActual").value;
	updateTextoIdiomas(numIdioma);
})

function cargarIdiomas() {
	// llamada al servicio de SIBBACServiceTextosIdiomas - getListaIdiomas
	var jsonData = "{" + "\"service\" : \"SIBBACServiceIdioma\", " + "\"action\"  : \"getListaIdiomas\"}";

	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			// cargo los idiomas
			var resultados = json.resultados;

			var item = null;

			$('#idiomasTraducir').empty();
			$('#textosTraducir').empty();
			fila = "";
			capa = "";
			visible = "1";
			for ( var k in resultados) {
				item = resultados[k];
				if (visible == "1") {
					eldisplay = "block";
					visible = "0";
					document.getElementById("idiomaActual").value = item.id;
				} else {
					eldisplay = "none";
				}

				fila = fila + "<a href='#' style='cursor: pointer; color: #ff0000;' onclick='mostrarIdioma(\"" + item.id + "\");'>"
						+ item.descripcion + "</a>&nbsp;";

				capa = capa + "<div id='idioma" + item.id + "' style='display:" + eldisplay + ";'><div class='titIdioma'>IDIOMA: "
						+ item.descripcion + "</div><textarea id='textoIdioma" + item.id
						+ "' rows='10' cols='40' /></textarea><input type='hidden' id='idtextoIdioma" + item.id + "' /></div>"

				contadorI.push(item.id);
			}

			$('#idiomasTraducir').append(fila);
			$('#textosTraducir').append(capa);

		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

function borrarTexto(id, nombre) {
	var filtro = "{\"id\" : \"" + id + "\" }";
	var jsonData = "{" + "\"service\" : \"SIBBACServiceContacto\", " + "\"action\"  : \"bajaContacto\", " + "\"filters\"  : " + filtro
			+ "}";

	if (confirm("¿Desea borrar realmente el contacto " + nombre + "?") == true) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : "/sibbac20back/rest/service",
			data : jsonData,
			beforeSend : function(x) {
				if (x && x.overrideMimeType) {
					x.overrideMimeType("application/json");
				}
				// CORS Related
				x.setRequestHeader("Accept", "application/json");
				x.setRequestHeader("Content-Type", "application/json");
				x.setRequestHeader("X-Requested-With", "HandMade");
				x.setRequestHeader("Access-Control-Allow-Origin", "*");
				x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
			},
			async : true,
			success : function(json) {
				$("section").load("views/contactos/contactos.html");
			},
			error : function(c) {
				console.log("ERROR: " + c);
			}
		});
	} else {
		// alert("You pressed Cancel!");
	}

}

function mostrarIdioma(id) {
	// alert(contadorI.length);
	// hay que recorrer los valores del array y ocultar las capas de esos ids
	for (z = 0; z < contadorI.length; z++) {
		cap = contadorI[z];
		$('#idioma' + cap).css('display', 'none');
	}
	$('#idioma' + id).css('display', 'block');
	document.getElementById("idiomaActual").value = id;

}

// cargar etiquetas
function cargarEtiquetas() {
	$('#txtEtiquetas').empty();
	$('#txtEtiquetas').append("<option value='0'>Seleccione una etiqueta</option>");
	var jsonData = "{" + "\"service\" : \"SIBBACServiceEtiquetas\", " + "\"action\"  : \"getListaEtiquetas\"}";

	$.ajax({
		type : "POST",
		dataType : "json",
		url : "/sibbac20back/rest/service",
		data : jsonData,
		beforeSend : function(x) {
			if (x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			// CORS Related
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async : true,
		success : function(json) {
			// cargo los idiomas
			var resultados = json.resultados;

			var item = null;

			for ( var k in resultados) {
				item = resultados[k];
				$('#txtEtiquetas').append("<option value='" + item.id + "'>" + item.etiqueta + "</option>");
			}

		},
		error : function(c) {
			console.log("ERROR: " + c);
		}
	});
}

// añadir etiqueta al textarea
function anadirEtiqueta() {

	var etiqueta = document.getElementById("txtEtiquetas");
	var etiquetaSeleccionada = etiqueta.selectedIndex;
	var vEtiquetaSeleccionada = etiqueta.options[etiquetaSeleccionada];
	var indiceEtiqueta = vEtiquetaSeleccionada.value;
	if (indiceEtiqueta == "0") {
		alert("Seleccione una etiqueta");
		return false;
	}
	var estaEtiqueta = vEtiquetaSeleccionada.text;
	numIdioma = document.getElementById("idiomaActual").value;
	textoActual = document.getElementById("textoIdioma" + numIdioma).value;
	textoFinal = textoActual + " " + estaEtiqueta;
	document.getElementById("textoIdioma" + numIdioma).value = textoFinal;

}
