//
var oTable = undefined;
function consultar(params) {
	/*
	 * $(params).each(function(key, val){ console.log("key: "+key +" = "+val);
	 * });
	 */
	var data = new DataBean();
	data.setService('SIBBACServiceCuentasDeCompensacion');
	data.setAction('getCuentasDeCompensacion');
	if (params !== '') {
		data.setFilters(params);
	}
	var request = requestSIBBAC(data);
	request
			.success(function(json) {
				var datos = undefined;
				if (json === undefined
						|| json.resultados === undefined
						|| json.resultados === null
						|| json.resultados.result_cuentas_compensacion === undefined
						|| json.resultados.result_cuentas_compensacion === null) {
					datos = {};
				} else {
					datos = json.resultados.result_cuentas_compensacion;
				}
				var difClass = 'centrado';
				oTable.fnClearTable();
				var frecuencia = 0;
				var cuentaLiquidacion = "";
				var entidadRegistro = "";
				var cl = null;
				var codigoS3 = "";
				var esClearing = "";
				var idCompensador = "";
				var idTipoCuenta = "";
				var subcustodio = "";
				var numCtaSubcustodio = "";

				$.each(datos, function(key, val) {
                                    cl = val.cuentaLiquidacion;
                                    entidadRegistro = '<span data="'+ ((val.entidadRegistro !== null) ? val.entidadRegistro.id	: "")
											+ '">'
											+ ((val.entidadRegistro !== null) ? val.entidadRegistro.nombre
													: "") + '</span>';
									frecuencia = val.frecuenciaEnvioExtracto;
									frecuencia = $.number(frecuencia, 0, ',',
											'.');
									cuentaLiquidacion = "<ul class=\"inner-table-ul\">";
									cuentaLiquidacion += '<li id="'
											+ val.cuentaLiquidacionId + '">'
											+ val.cuentaLiquidacionCodigo
											+ '</li>';
									cuentaLiquidacion += "</ul>";
									codigoS3 = val.codigoS3;
									esClearing = val.esClearing;
									idCompensador = val.idCompensador;
									idTipoCuenta = val.idTipoCuenta;
									subcustodio = (val.subcustodio === null) ? ""
											: val.subcustodio;
									numCtaSubcustodio = (val.numCtaSubcustodio === null) ? ""
											: val.numCtaSubcustodio;
									oTable
											.fnAddData([
													"<a data=\""
															+ val.idCuentaCompensacion
															+ "\" onclick=\"modificarCuenta('"
															+ val.idCuentaCompensacion
															+ "');\"><img src='img/editp.png'  title='editar' /></a>",
													val.idCuentaCompensacion,
													entidadRegistro,
													(val.entidadRegistro !== null) ? val.entidadRegistro.tipoer.nombre
															: "",
													addMercados(
															val.relacionCuentaMercadoDataList,
															"mercados_" + key),
													val.numCuentaER,
													(val.tipoCuentaConciliacion !== null) ? val.tipoCuentaConciliacion.name
															: '',
													(val.tipoNeteoTitulo !== null) ? val.tipoNeteoTitulo.nombre
															: '',
													(val.tipoNeteoEfectivo !== null) ? val.tipoNeteoEfectivo.nombre
															: '',
													val.cdCodigo,
													cuentaLiquidacion,
													frecuencia,
													val.tipoFicheroConciliacion,
													codigoS3, esClearing,
													idCompensador,
													idTipoCuenta, subcustodio,
													numCtaSubcustodio,
													'<span id="'+val.sistemaLiquidacion.id+'">' + val.sistemaLiquidacion.codigo +'</span>'
													]);
								});

			});
}
function addMercados(datos, id) {
	// var list = "<select class=\"inner-select\" id=\""+id+"\"
	// multiple=\"multiple\" >";
	var list = '<ul class="inner-table-ul" id="' + id + '" >';
	var valor;
	var texto;
	$(datos).each(function(key, val) {
		valor = val.id;
		texto = val.codigo;
		// list +='<option value="'+valor+'">'+texto+
		// '</option>';
		list += '<li value="' + valor + '" >' + texto + '</li>';
	});
	list += "</ul>";
	// list +="</select>";
	return list;
}
// obtiene el id de entidad de registro seleccionada
function getIdEntidadRegistro() {
	var er = $("#ER").val();
	if (er === undefined) {
		return "";
	}
	er = er.toLowerCase();
	if (er.trim().length > 0 && er.indexOf("tipo") !== -1) {
		er = er.substring(0, er.indexOf("tipo") - 1);
	}
	return er;
}
function obtenerIdsSeleccionados(lista) {
	var selected = "";

	$(lista + ' option').each(function(key, val) {
		if (key > 0) {
			selected += ", ";
		}
		selected += $(val).val().trim();
	});

	return selected;
}
// ///////////////////////////////////////////////
// ////SE EJECUTA NADA MÁS CARGAR LA PÁGINA //////
function initialize() {

	cargaCombos();
	// autocomplete multiple
	cargarCuentasMercados();
	cargarCuentasLiquidacion();
	cargarCompensadores();
	cargarTiposCuentaCompensacion();
	obtenerDatos();
	consultar('');
}
function cargarCuentasMercados() {
	var availableTags = [];
	var data = new DataBean();
	data.setService('SIBBACServiceCuentaLiquidacion');
	data.setAction('getCuentasMercado');
	var request = requestSIBBAC(data);
	request.success(function(json) {
		var datos = undefined;
		if (json === undefined || json.resultados === undefined
				|| json.resultados.result_cuentas_mercados === undefined) {
			datos = {};
		} else {
			datos = json.resultados.result_cuentas_mercados;
		}
		var obj;
		$(datos).each(function(key, val) {
			obj = {
				label : val.codigo + " - " + val.descripcion,
				id : val.id
			};
			availableTags.push(obj);
		});
		populateMCAComponents("#MCA", "#selectedMCA", "#btnDelMCA",
				availableTags);
		populateMCAComponents("#alta_MCA", "#alta_selectedMCA",
				"#alta_btnDelMCA", availableTags);
	});
}
/**
 * Para cargar el autocomplete de los mercados disponibles
 */
function populateMCAComponents(input, lista, boton, availableTags) {
	$(input).autocomplete(
			{
				source : availableTags,
				minLengh : 0,
				maxLength : 10,
				focus : function(event, ui) {
					// $("#MCA" ).val(ui.item.label);
					return false;
				},
				select : function(event, ui) {
					if (!valueExistInList(lista, ui.item.id)) {
						$(lista).append(
								"<option value=\"" + ui.item.id + "\" >"
										+ ui.item.label + "</option>");
						$(input).val("");
						$(lista + "> option").attr('selected', 'selected');
					}
					return false;
				}
			});

	// preparar boton para eliminar filas de mercados selecccionados
	$(boton).on("click", function(event) {
		event.preventDefault();
		$(lista).find('option:selected').remove().end();
		$(lista + "> option").attr('selected', 'selected');
	});
}
/** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
function valueExistInList(lista, valor) {
	var inList = false;

	$(lista + ' option').each(function(index) {

		if (this.value == valor) {
			inList = true;
			return inList;
		}
	});

	return inList;
}
/**
 * Carga autocomplete de tiops de cuenta compensacion
 */
function cargarTiposCuentaCompensacion() {
	var tipos = [];
	$(function() {
		var data = new DataBean();
		data.setService("SIBBACServiceCuentasDeCompensacion");
		data.setAction("getTiposCuentaCompensacion");
		var request = requestSIBBAC(data);
		request
				.success(function(json) {
					var datos = undefined;
					if (json === null
							|| json === undefined
							|| json.resultados === null
							|| json.resultados.result_tipos_cuenta_compensacion === null) {
						datos = {};
					} else {
						datos = json.resultados.result_tipos_cuenta_compensacion;
					}

					var obj;
					$(datos).each(
							function(key, val) {
								obj = {
									label : val.id + " - " + val.codigo + " - "
											+ val.descripcion,
									value : val.id
								};
								tipos.push(obj);
							});
					populateAutocomplete("#tipoCuenta", "#idTipoCuenta", tipos);
					populateAutocomplete("#alta_tipoCuenta",
							"#alta_idTipoCuenta", tipos);
				});
	});
}
/**
 * Carga autocomplete de compensadores
 */
function cargarCompensadores() {
	var compensadores = [];
	$(function() {
		var data = new DataBean();
		data.setService("SIBBACServiceCuentasDeCompensacion");
		data.setAction("getListaCompensadores");
		var request = requestSIBBAC(data);
		request.success(function(json) {
			var datos = undefined;
			if (json === null || json === undefined || json.resultados === null
					|| json.resultados.result_lista_compensadores === null) {
				datos = {};
			} else {
				datos = json.resultados.result_lista_compensadores;
			}

			var obj;
			$(datos).each(
					function(key, val) {
						obj = {
							label : val.idCompensador + " - " + val.nombre
									+ " - " + val.descripcion,
							value : val.idCompensador
						};
						compensadores.push(obj);
					});
			populateAutocomplete("#compensador", "#idCompensador",
					compensadores);
			populateAutocomplete("#alta_compensador", "#alta_idCompensador",
					compensadores);
		});
	});
}
function populateAutocomplete(autoInput, idInput, datos) {
	$(autoInput).autocomplete({
		source : datos,
		focus : function(event, ui) {
			$(autoInput).val(ui.item.label);
			$(idInput).val(ui.item.value);
			return false;
		},
		select : function(event, ui) {
			$(autoInput).val(ui.item.label);
			$(idInput).val(ui.item.value);
			return false;
		}
	});
}
/**
 * Carga el autocomplete para las cuentas de compensacion
 *
 */
function cargarCuentasLiquidacion() {
	var availableTags = [];
	$(function() {
		var data = new DataBean();
		data.setService('SIBBACServiceCuentasDeCompensacion');
		data.setAction('getCuentaLiquidacion');
		var request = requestSIBBAC(data);
		request
				.success(function(json) {
					var datos = undefined;
					if (json === undefined
							|| json.resultados === undefined
							|| json.resultados === null
							|| json.resultados.result_cuentas_liquidacion === null
							|| json.resultados.result_cuentas_liquidacion === undefined) {
						datos = {};
					} else {
						datos = json.resultados.result_cuentas_liquidacion;
					}
					var obj;
					$(datos).each(function(key, val) {
						obj = {
							label : val.cdCodigo,
							value : val.id
						};
						availableTags.push(obj);
					});
					$("#btnDelCL").click(
							function(event) {
								event.preventDefault();
								$("#selectedCL").find("option:selected")
										.remove().end();
							});
					pupulateCuentaRelacionadaAutocomplete(
							"#cuentasrelacionadas", "#idCtaLiq", availableTags,
							"#selectedCL");
					pupulateCuentaRelacionadaAutocomplete("#alta_ctaLiq",
							"#alta_idCtaLiq", availableTags, '');
					// DE MOMENTO NO SE VAN A MODIFICAR LAS CUENTAS DE
					// COMPENSACION DESDE EL ALTA
					// pupulateCuentaRelacionadaAutocomplete("#alta_cuentasrelacionadas","#alta_idCtaComp",
					// availableTags, "#alta_selectedCC");
				});
	});
	// fin
}
function pupulateCuentaRelacionadaAutocomplete(input, idContainer,
		availableTags, lista) {
	$(input)
			.autocomplete(
					{
						minLength : 0,
						source : availableTags,
						focus : function(event, ui) {
							if (lista === '') {
								$(this).val(ui.item.label);
								$(idContainer).val(ui.item.value);
							}
							return false;
						},
						select : function(event, ui) {
							// $(idContainer).val(ui.item.value);
							if (lista !== ''
									&& !valueExistInList(lista, ui.item.value)) {
								$(lista).append(
										"<option value=\"" + ui.item.value
												+ "\" >" + ui.item.label
												+ "</option>");
								$(lista + "> option").attr('selected',
										'selected');
								$(input).val('');

							}
							return false;
						}
					});

}
$(document).ready(function() {
    oTable = $("#tblCuentasCompensacion").dataTable(
    {   "dom" : 'T<"clear">lfrtip',
        "tableTools" : {
            "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
	},"language" : {
            "url" : "i18n/Spanish.json"
	},"scrollX" : true,
    "scrollY": "480px",
    "scrollCollapse": true,
	"aoColumns" : [
            {"sClass" : "centrado","bSortable" : "false"},
            {"sClass" : "centrado"},
            {"sClass" : "centrado"},
            {"sClass" : "centrado"
										}, {
											"sClass" : "centrado"
										}, {
											"sClass" : "centrado"
										}, {
											"sClass" : "centrado"
										}, {
											"sClass" : "centrado"
										}, {
											"sClass" : "centrado"
										}, {
											"sClass" : "centrado"
										}, {
											"sClass" : "centrado"
										}, {
											"sClass" : "align-right",
											"sType" : "numeric"
										}, {
											"sClass" : "centrado"
										}, {
											"sClass" : "centrado"
										}, {"sClass" : "centrado"
										}, {"sClass" : "centrado"
										}, {"sClass" : "centrado"
										}, {"sClass" : "align-left"
										}, {"sClass" : "align-left"},
										{"sClass" : "align-center"} ],
										"sort" : [ 1 ]
									});
					initialize();

					/*
					 * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS
					 * BOTONES DE LA TABLA
					 */
					if ($('.contenedorTabla').length >= 1) {
						var anchoBotonera;
						$('.contenedorTabla').each(
								function(i) {
									anchoBotonera = $(this).find('table')
											.outerWidth();
									$(this).find('.botonera').css('width',
											anchoBotonera + 'px');
									$(this).find('.resumen').css('width',
											anchoBotonera + 'px');
									$('.resultados').hide();
								});
					}

					/* Eva:Función para cambiar texto */
					jQuery.fn.extend({
						toggleText : function(a, b) {
							var that = this;
							if (that.text() != a && that.text() != b) {
								that.text(a);
							} else if (that.text() == a) {
								that.text(b);
							} else if (that.text() == b) {
								that.text(a);
							}
							return this;
						}
					});
					/* FIN Función para cambiar texto */

					$('a[href="#release-history"]').toggle(
							function() {
								$('#release-wrapper').animate({
									marginTop : '0px'
								}, 600, 'linear');
							},
							function() {
								$('#release-wrapper').animate(
										{
											marginTop : '-'
													+ ($('#release-wrapper')
															.height() + 20)
													+ 'px'
										}, 600, 'linear');
							});

					$('#download a').mousedown(
							function() {
								_gaq.push([ '_trackEvent', 'download-button',
										'clicked' ])
							});

					$(function() {
						prepareCollapsion();
					});
					$('#cargarTabla').submit(function(event) {
						event.preventDefault();
						obtenerDatos();
						collapseSearchForm();
						seguimientoBusqueda();
						return false;
					});

					$('#limpiar').click(function(event) {
						event.preventDefault();
						$('input[type=text]').val('');
						$('select').val('0');
					});

					// ALTA DE CUENTA LIQUIDACION
					$('#cargarPopupParam')
							.submit(
									function(event) {
										event.preventDefault();
										var msgValid = formIsValid();
										if (msgValid !== "") {
											alert(msgValid);
											return false;
										}
										$('#formularioalta').css('display',
												'none');
										$('fade').css('display', 'none');
										var ER = $('#id_alta_ER').val();
										var TER = $('#alta_TER').val();
										var MCA = obtenerIdsSeleccionados("#alta_selectedMCA");
										var numero_cuenta = $(
												'#alta_numero_cuenta').val();
										var tipo_cuenta = $('#alta_tipo_cuenta')
												.val();
										var tipo_neteo_titulos = $(
												'#alta_tipo_neteo_titulos')
												.val();
										var tipo_neteo_efectivo = $(
												'#alta_tipo_neteo_efectivo')
												.val();
										/*
										 * var cuentas_relacionadas =
										 * obtenerIdsSeleccionados("#alta_selectedCC");
										 */
										var cdCodigo = $("#alta_cdCodigo")
												.val();
										var FEE = $('#alta_FEE').val();
										var tipo_fichero = $(
												'#alta_tipo_fichero').val();

										// do validar
										var filtro = {
											"entidadRegistro" : ER,
											/*-"tipoER" : TER,*/
											"idMercado" : MCA,
											"numCuentaER" : numero_cuenta,
											"esCuentaPropia" : tipo_cuenta,
											"tipoNeteoTitulo" : tipo_neteo_titulos,
											"tipoNeteoEfectivo" : tipo_neteo_efectivo,
											"cdCodigo" : cdCodigo,
											/*
											 * "idCtaCompensacion" :
											 * cuentas_relacionadas ,
											 */
											"frecuenciaEnvioExtracto" : FEE,
											"idCtaCompensacion" : $(
													"#idCuentaCompensacion")
													.val(),
											"idCuentaLiquidacion" : $(
													"#alta_idCtaLiq").val(),
											"tipoFicheroConciliacion" : tipo_fichero,
											"idCompensador" : $(
													"#alta_idCompensador")
													.val(),
											"idTipoCuentaComp" : $(
													"#alta_idTipoCuenta").val(),
											"idTipoCuentaConciliacion" : "",
											"esClearing" : $("#alta_esClearing")
													.val(),
											"numCtaSubcustodio" : $(
													"#alta_numCtaSubcustodio")
													.val(),
											"subcustodio" : $(
													"#alta_subcustodio").val(),
											"idSistemaLiquidacion" : $("#sistemas_liq").val()
										};
										var data = new DataBean();
										data.setService('SIBBACServiceCuentasDeCompensacion');
										// if
										// ($("#altaparam").attr('data')==='alta'){
										data.setAction('addCuentaCompensacion');
										// }
										// else if
										// ($("#altaparam").attr('data')==='modificar'){
										// data.setAction('updateCuentaLiquidacion');
										// }
										data.setFilters(JSON.stringify(filtro));
										var request = requestSIBBAC(data);
										request
												.success(function(json) {
													// alert(json.request.filters.diaMes);
													if (json !== null
															&& json.error !== null
															&& json.error !== ""
															&& json.error !== undefined) {
														alert("Ocurrió un error al intentar guardar la cuenta: "
																+ json.error);

													} else {
														alert("Datos guardados correctamente.");
														$(
																"#idCuentaCompensacion")
																.val("");
														$("section")
																.load(
																		"views/configuracion/cuentaCompensacion.html");
													}
												});
									});

					$('#altapopup').click(function(event) {
						event.preventDefault();
						$('#formularioalta').css('display', 'block');
						$('fade').css('display', 'block');
						$('div.piePagina input.mybutton').attr('data', 'alta');
						$('p.titleAlta').text('Alta Cuenta Compensación');
					});

				});
function render() {
	//
}
function formIsValid() {
	var msgError = "";

	var tpFc = $("#alta_tipo_fichero").val();
	if (tpFc === "0" || tpFc === "") {
		// msgError = "Por favor seleccione el tipo de fichero.\n";
	}
	var tpNtEf = $("#alta_tipo_neteo_efectivo").val();
	if (tpNtEf === "0" || tpNtEf === "") {
		// msgError = "Por favor introduzca el tipo de neteo para el
		// efectivo.\n";
	}
	var tpNtTt = $("#alta_tipo_neteo_titulos").val();
	if (tpNtTt === "0" || tpNtTt === "") {
		// msgError = "Por favor seleccione el tipo de neteo para los
		// títulos.\n";
	}
	var tipoCuenta = $("#alta_tipo_cuenta").val();
	if (tipoCuenta === "0" || tipoCuenta === "") {
		// msgError = "Por favor seleccione un tipo de cuenta.\n";
	}
	return msgError;
}
function seguimientoBusqueda() {
	$('.mensajeBusqueda').empty();
	var cadenaFiltros = "";
	if ($('#ER').val() !== "" && $('#ER').val() !== undefined) {
		cadenaFiltros += " Entidad de registro: " + $('#ER').val();
	}
	if ($('#TER').val() !== "" && $('#TER').val() !== undefined
			&& $('#TER').val() !== "0") {
		cadenaFiltros += " Tipo de entidad de registro: " + $('#TER').val();
	}
	if ($('#MCA').val() !== "" && $('#MCA').val() !== undefined) {
		cadenaFiltros += " Mercados asociados: " + $('#MCA').val();
	}
	if ($('#numero_cuenta').val() !== ""
			&& $('#numero_cuenta').val() !== undefined) {
		cadenaFiltros += "N&uacute;mero Cuenta: " + $('#numero_cuenta').val();
	}
	if ($('#tipo_cuenta').val() !== "" && $('#tipo_cuenta').val() !== undefined
			&& $('#tipo_cuenta').val() !== "0") {
		cadenaFiltros += " Tipo Cuenta: " + $('#tipo_cuenta').val();
	}
	if ($('#tipo_neteo').val() !== "" && $('#tipo_neteo').val() !== undefined
			&& $('#tipo_neteo').val() !== "0") {
		cadenaFiltros += " Tipo Neteo: " + $('#tipo_neteo').val();
	}
	if ($('#FEE').val() !== "" && $('#FEE').val() !== undefined) {
		cadenaFiltros += " Frecuencia Env&iacute;o Extracto: "
				+ $('#FEE').val();
	}
	if ($('#tipo_fichero').val() !== ""
			&& $('#tipo_fichero').val() !== undefined
			&& $('#tipo_fichero').val() !== "0") {
		cadenaFiltros += " Tipo Fichero: " + $('#tipo_fichero').val();
	}
	if ($('#cuentas_relacionadas').val() !== ""
			&& $('#cuentas_relacionadas').val() !== undefined) {
		cadenaFiltros += " Tipo Fichero: " + $('#cuentas_relacionadas').val();
	}
	if ($('#selectedMCA').find('option:selected').length > 0) {
		cadenaFiltros += " Mercados Contratación: ";
		$('#selectedMCA').find('option:selected').each(function(key, val) {
			cadenaFiltros += $(val).text() + " ";
		});
	}
	if ($("#selectedCL").find("option:selected").length > 0) {
		cadenaFiltros += "Cuentas de Liquidaci&oacute;n: ";
		$("#selectedCL").find("option:selected").each(function(key, val) {
			cadenaFiltros += $(val).text() + " ";
		});
	}
	$('.mensajeBusqueda').append(cadenaFiltros);

}

function obtenerDatos() {
	var params = {
		"entidadRegistro.id" : ($("#idER").val() !== undefined && $("#idER")
				.val() !== "") ? $("#idER").val() : "",
		/* "tipoer.id" : $("#TER").val(), */
		"tipoFicheroConciliacion" : $("#tipo_fichero").val(),
		"idMercado" : obtenerIdsSeleccionados("#selectedMCA"),
		"numCuentaER" : $("#numero_cuenta").val(),
		"tipoCuentaConciliacion.id" : $("#tipo_cuenta").val(),
		"tipoNeteoTitulo.id" : $("#tipo_neteo_titulos").val(),
		"tipoNeteoEfectivo.id" : $("#tipo_neteo_efectivo").val(),
		"tmct0CuentaLiquidacion.id" : obtenerIdsSeleccionados("#selectedCL"),
		"cdCodigo" : $("#cdCodigo").val(),
		"frecuenciaEnvioExtracto" : $("#FEE").val()
	};

	$("#tblCuentasCompensacion > tbody").html("");
	consultar(JSON.stringify(params));

}

// llamada para cargar datos combos
function cargaCombos() {
	var cuenta = new DataBean();
	cuenta.setService('SIBBACServiceCuentaLiquidacion');
	cuenta.setAction('getTipoCuenta');
	var neteo = new DataBean();
	neteo.setService('SIBBACServiceCuentaLiquidacion');
	neteo.setAction('getTipoNeteo');
	var fichero = new DataBean();
	fichero.setService('SIBBACServiceCuentaLiquidacion');
	fichero.setAction('getTipoFichero');
	var tipoER = new DataBean();
	tipoER.setService('SIBBACServiceEntidadRegistro');
	tipoER.setAction('getAllTiposEntidadRegistro');
	var er = new DataBean();
	er.setService("SIBBACServiceEntidadRegistro");
	er.setAction("getAllEntidadRegistro");
	var sistemasLiq = new DataBean();
	sistemasLiq.setService('SIBBACServiceCuentaLiquidacion');
	sistemasLiq.setAction('getSistemasLiquidacion');

	// limpio los combos
	$("#tipo_cuenta").html("<option value=\"0\"> Seleccione un tipo </option>");
	$("#tipo_neteo_titulos").html(
			"<option value=\"0\"> Seleccione un tipo </option>");
	$("#tipo_neteo_efectivo").html(
			"<option value=\"0\"> Seleccione un tipo </option>");
	$("#tipo_fichero")
			.html("<option value=\"0\"> Seleccione un tipo </option>");
	$("#TER").html("<option value=\"0\"> Seleccione un tipo </option>");
	$("#sistemas_liq").html("<option value=\"\"> Seleccione un Sistema </option>");
	// cargo tipo cuenta
	cargaDatosCombo(cuenta, 'tipo_cuenta');
	// cargo tipo neteo titulos
	cargaDatosCombo(neteo, 'tipo_neteo_titulos');
	// cargo tipo neteo efectivo
	cargaDatosCombo(neteo, 'tipo_neteo_efectivo');
	// cargo tipo ER
	cargaDatosCombo(tipoER, 'TER');
	// cargo tipo fichero
	cargaDatosCombo(fichero, 'tipo_fichero');
	// carga entidades de registro es un autocomplete
	cargarEntidadesRegistro(er);
	//cargar sistemas de liquidacion
	cargaDatosCombo(sistemasLiq, "sistemas_liq");
}
function cargarEntidadesRegistro(llamada) {
	var request = requestSIBBAC(llamada);
	request
			.success(function(json) {
				var datos = undefined;
				if (json !== undefined && json.resultados !== null
						&& json.resultados !== undefined) {
					if (json.resultados.result_entidades_de_registro !== null
							&& json.resultados.result_entidades_de_registro !== undefined) {
						datos = json.resultados.result_entidades_de_registro;
						var tags = new Array(datos.length);
						var tag;
						$(datos).each(
								function(key, val) {
									if (val.tipoer !== null) {
										tag = {
											label : val.nombre + " - "
													+ val.tipoer.nombre,
											value : val.id,
											tipoId : val.tipoer.id,
											tipoName : val.tipoer.nombre
										};
										tags[key] = tag;
									}

								});
						// si se borra la entidad de registro del campo, que
						// tambien
						// se borre el id almacenado en el imput hidden
						$("#ER").change(function() {
							if ($(this).val() === "") {
								$("#idER").val("");
							}
						});
						loadAutocompleteER("#ER", "#TER", "#idER", tags);
						loadAutocompleteER("#alta_ER", "#alta_TER",
								"#id_alta_ER", tags);
					} else {
						return false;
					}
				}
			});
}
function loadAutocompleteER(idER, idTER, idHidden, tags) {
	$(idER).autocomplete({
		source : tags,
		focus : function(event, ui) {
			$(this).val(ui.item.label);
			return false;
		},
		select : function(event, ui) {
			$(idHidden).val(ui.item.value);
			$(idTER).val(ui.item.tipoId);
			return false;
		}
	});
}
// /carga datos combos
function cargaDatosCombo(llamada, id) {
	var request = requestSIBBAC(llamada);
	request
			.success(function(json) {
				var datos = undefined;
				if (json !== undefined && json.resultados !== undefined) {
					if (json.resultados.result_tipo_cuenta != undefined) {
						datos = json.resultados.result_tipo_cuenta;
					}
					if (json.resultados.result_tipo_Neteo != undefined) {
						datos = json.resultados.result_tipo_Neteo;
					}
					if (json.resultados.result_tipo_fichero != undefined) {
						datos = json.resultados.result_tipo_fichero;
					}
					if (json.resultados.result_tipos_entidad_registro != undefined) {
						datos = json.resultados.result_tipos_entidad_registro;
					}
					if(json.resultados.result_sistemas_liquidacion != undefined){
						datos = json.resultados.result_sistemas_liquidacion;
					}
					if (datos === undefined) {
						return;
					} else {
						// $.each(datos, function (key, val) {
						var idData;
						var nombre;

						for (var i = 0; i < datos.length; i++) {
							idData = (datos[i].id !== undefined) ? datos[i].id
									: datos[i];
							nombre = (datos[i].nombre !== undefined) ? datos[i].nombre
									: ((datos[i].name !== undefined) ? datos[i].name
											: ((datos[i].codigo !== undefined) ? datos[i].codigo : datos[i]));
							if (((id === "tipo_neteo_titulos" || id === "alta_tipo_neteo_titulos") && idData !== 3)
									|| (id !== "tipo_neteo_titulos" && id !== "alta_tipo_neteo_titulos") ||
									(id === "sistemas_liq")) {
								$('#' + id).append(
										"<option value=\"" + idData + "\">"
												+ nombre + "</option>");
								if($("#alta_"+id) !== undefined){
								$('#alta_' + id).append(
										"<option value=\"" + idData + "\">"
												+ nombre + "</option>");
								}
							}
						}
						// });
					}
				}
			});

}

function modificarCuenta(id) {
	cargarInput(id);
	$('#formularioalta').css('display', 'block');
	$('fade').css('display', 'block');
	$('div#formularioalta input.mybutton').attr('data', 'modificar');
	$('p.titleAlta').text('Modificar Cuenta Compensación');
}
function limpiarMercadosCuenta() {
	$("#alta_selectedMCA").find("option").remove().end();
}
function selectOption(queLista, texto) {
	$(queLista + " option:selected").attr('selected', 'false');
	var external = "";
	var inList = "";
	$(queLista).find("option").each(function(key, val) {
		inList = $(val).text().trim().toLowerCase();
		external = texto.trim().toLowerCase();

		if (inList === external) {
			$(val).attr("selected", "selected");
		}
	});
}
function cargarInput(id) {
	var fila = $('a[data="' + id + '"]').parent().parent().children();
	/** ***ID CUENTA LIQUIDACION***** */
	$("#idCuentaCompensacion").val(id);
	$("#alta_idCC").val(id);
	/** ******ENTIDAD REGISTRO ********* */
	var erId = $(fila[2]).find("span").attr("data");
	var erName = $(fila[2]).find("span").text();
	$('#alta_ER').val(erName);
	$("#id_alta_ER").val(erId);
	/** **** tipo entidad registro ******* */
	selectOption("#alta_TER", $(fila[3]).text());
	/** MERCADOS CUENTAS* */
	limpiarMercadosCuenta();
	var opcion = "";
	$(fila[4]).find("ul > li").each(
			function(key, val) {
				opcion = '<option selected="selected" value="' + $(val).val()
						+ '">' + $(val).text() + '</option>';
				$("#alta_selectedMCA").append(opcion);
			});
	/** *************** */
	$('#alta_numero_cuenta').val($(fila[5]).text());
	/** **** TIPO CUENTA ******* */
	selectOption('#alta_tipo_cuenta', $(fila[6]).text());
	/** ***** TIPO NETEO TITULOS ******* */
	selectOption("#alta_tipo_neteo_titulos", $(fila[7]).text());
	/** ***** TIPO NETEO TITULOS ******* */
	selectOption("#alta_tipo_neteo_efectivo", $(fila[8]).text());
	/** **** CODIGO CUENTA ***** */
	$("#alta_cdCodigo").val($(fila[9]).text());
	/** CUENTA LIQUIDACION * */
	extraerDatosCtaLiquidacion($(fila[10]));
	/** ***FRECUENCIA ****** */
	var frec = $.number($(fila[11]).text(), 0, ',', '');
	$('#alta_FEE').val(frec);
	/** ***TIPO FICHERO****** */
	selectOption("#alta_tipo_fichero", $(fila[12]).text());
	/** CODIGO S3 * */
	$("#alta_codS3").val($(fila[13]).text());
	/** ES CLEARING * */
	$("#alta_esClearing").val($(fila[14]).text());
	/** COMPENSADOR * */
	$("#alta_compensador").val($(fila[15]).text());
	$("#alta_idCompensador").val($(fila[15]).text());
	/** TIPO CUENTA* */
	$("#alta_tipoCuenta").val($(fila[16]).text());
	$("#alta_idTipoCuenta").val($(fila[16]).text());
	$("#alta_subcustodio").val($(fila[17]).text());
	$("#alta_numCtaSubcustodio").val($(fila[18]).text());
	var sistemLiq = $(fila[19]).find("span").attr("id");
	$("#sistemas_liq").val(sistemLiq);

}
function extraerDatosCtaLiquidacion(celda) {
	var idCta = $(celda).find("ul > li").attr("id");
	var codCta = $(celda).find("ul > li").text();
	//
	$("#alta_ctaLiq").val(codCta);
	$("#alta_idCtaLiq").val(idCta);
}
function cerrar() {
	document.getElementById('formularioalta').style.display = 'none';
	document.getElementById('fade').style.display = 'none';
	$('#alta_ER').val('');
	$('#alta_TER').val('');
	$('#alta_MCA').val('');
	$('#alta_numero_cuenta').val('');
	$('#alta_tipo_cuenta').val('');
	$('#alta_tipo_neteo_titulos').val('');
	$('#alta_tipo_neteo_efectivo').val('');
	$('#alta_cuentas_relacionadas').val('');
	$('#alta_FEE').val('');
	$('#alta_tipo_fichero').val('');
}
