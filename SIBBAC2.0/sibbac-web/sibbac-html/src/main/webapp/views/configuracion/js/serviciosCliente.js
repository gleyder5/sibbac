var idOcultos = [];
var availableTags = [];
$(document).ready(function() {


/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}

	cargarAlias();


});

	/*cargar tabla alias*/
	$('.btn.buscador').click(function(){
			//event.preventDefault();

		var listaAlias = document.getElementById("textAlias").value;

		posicionAlias = availableTags.indexOf(listaAlias);
		if(posicionAlias==="-1"){
			alert("El alias introducido no existe");
			return false;
		}
		else
			{
			valorSeleccionadoAlias = idOcultos[posicionAlias];
			cargarTabla(valorSeleccionadoAlias);
			}
		return false;
	});



function cargarAlias(){

		$.ajax({
			type: "POST",
			dataType: "json",
			url:  "/sibbac20back/rest/service",
			data: "{\"service\" : \"SIBBACServiceAlias\", \"action\"  : \"getListaAlias\"}",

			beforeSend: function( x ) {
				if(x && x.overrideMimeType) {
					x.overrideMimeType("application/json");
				}
				// CORS Related
				x.setRequestHeader("Accept", "application/json");
				x.setRequestHeader("Content-Type", "application/json");
				x.setRequestHeader("X-Requested-With", "HandMade");
				x.setRequestHeader("Access-Control-Allow-Origin", "*");
				x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
			},
			async: true,
			success: function(json) {
				var resultados = json.resultados.listaAlias;
				var item = null;

				for ( var k in resultados ) {
					item = resultados[ k ];
					idOcultos.push(item.id);
					ponerTag = item.nombre.trim();
					availableTags.push(ponerTag);
				}
				//código de autocompletar

	           $( "#textAlias" ).autocomplete({
	             source: availableTags
	           });
			},
			error: function(c) {
				console.log( "ERROR: " + c );
			}
		});
}

function cargarTabla(alias) {
	// Ponemos el DIV de "Cargando"...
	inicializarLoading();
	document.getElementById('aliasOculto').value = alias;
	// console.log("entro en el método [alias=="+alias+"]");
	var filtro = "{\"alias\" : \""+ alias + "\" }";
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service/",
		data: "{\"service\" : \"SIBBACServiceServicios\", \"action\"  : \"getContactosByService\", \"filters\"  : "+ filtro + "}",


		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {

			//validar si hay contactos y/o servicios
			if ( json.resultados!=null ) {
				var contactos = json.resultados.contactos;
				var servicios = json.resultados.servicios;
				numServicios = servicios.length;
				numContactos = contactos.length;
				if((numServicios!=0)&&(numContactos!=0))
				{ //éxito, cargo toda la tabla

					//cargo las combinaciones
					var combinacion = json.resultados.combinacion;
					var item0 = null;
					//creo array con los resultados contacto-servicio-regla
					var datosCombinaciones = new Array();
					//
					for ( var z in combinacion ) {
						item0 = combinacion[ z ];

						valorContacto = item0.idContacto;
						var combinacion2 = json.resultados.combinacion[z].servicios;
						for (var h in combinacion2) {
								item1 = combinacion2[ h ];
								//push al array con contacto-servicio-regla
								elvalor = valorContacto+'-'+item1.idServicio+'-'+item1.idRegla;
								datosCombinaciones.push(elvalor);
						}
					}

					//comprobación array
					longitud = datosCombinaciones.length;
					for(cont=0;cont<longitud;cont++){
						//alert(datosCombinaciones[cont]);
					}

					//cargo los servicios
					//miro los length de los resultados para el número de columnas y filas
					columnasS = numServicios +1;
					filaS = numContactos +2;
					var item = null;
					//cabecera de la tabla
					$('.lanzador').empty();
					$('.piePagina').empty();
					//pinto lo que está fuera del bucle
					//una variable para ir metiendo el contenido
					contTabla = "<table class='tablaPrueba ancha'><tr class='cabeceraLanzador'><td rowspan='"+filaS+"' class='titContactos'></td><td colspan='"+columnasS+"' class='titServicios'></td></tr><tr>"+
								"<th class='taleft grisclaro'></th>";

					//pinto los servicios y cargo los ids en un array
					losServicios = new Array();
					for ( var k in servicios ) {
						item = servicios[ k ];
						losServicios.push(item.idServicio);
						contTabla =contTabla + "<th class='taleft' onmouseover='mostrarServicio(\"tip"+item.idServicio+"\")'  onmouseout='ocultarServicio(\"tip"+item.idServicio+"\")' id='elemento"+item.idServicio+"'>"+item.nombreServicio + "</th>"+
												"<div class='tip' id='tip"+item.idServicio+"' style='display: none;'><span class='negrita subrayado'>Datos del Servicio</span></br><span class='negrita'>ID: </span>"+item.idServicio+"</br><span class='negrita'>Nombre del Servicio: </span>"+item.nombreServicio + "</br><span class='negrita'>Comentarios del Servicio: </span>"+item.descripcionServicio + "</div>";
					}
					contTabla = contTabla + "</tr>";

					//cargo los contactos

					var item = null;
					//filas para cada contacto
					//tengo que mirar en el array datosCombinaciones las coincidencias de contacto y servicio (idContacto,losServicios) para marcar la regla en caso de coincidencia
					contTabla = contTabla +  "<tr>";
					for ( var l in contactos ) {
						item = contactos[ l ];
						contTabla =contTabla + "<td class='titular' onmouseover='mostrarContacto(\"tep"+item.idContacto+"\")' onmouseout='ocultarContacto(\"tep"+item.idContacto+"\")'>"+item.nombreContacto + " " + item.pApellidoContacto + "</td>" +
						"<div class='tep' id='tep"+item.idContacto+"' style='display: none;'><span class='negrita subrayado'>Datos del Contacto</span></br><span class='negrita'>ID: </span>"+item.idContacto+"</br><span class='negrita'>Nombre del Contacto: </span>"+item.nombreContacto + " "+item.pApellidoContacto+"</br><span class='negrita'>Teléfono del Contacto: </span>"+item.telefonoAContacto + "</br><span class='negrita'>Email del Contacto: </span>"+item.emailContacto +"</div>";
						//hago un bucle de 'numServicios' para colocar las celdas de cada contacto correspondientes a cada servicio
						//primero el bucle de 'numServicios'
						for (r=1;r<=numServicios;r++){
							var td = "";
							var isChecked = "";
							var combinacion = json.resultados.combinacion;
							for(var m in combinacion){
								var union = combinacion[ m ];
								if( union.idContacto == item.idContacto ){
									serviciosCombinacion = union.servicioActivoDatas;
									for(var z in serviciosCombinacion){
										var unionServicios = serviciosCombinacion[ z ];
										if( unionServicios.idServicio === losServicios[r-1] ){
											if( unionServicios.active ){
												isChecked = "checked";
											}
										}
									}
								}
							}
							td += "<td class='centrar' id='td-"+item.idContacto+"-"+losServicios[r-1]+"'>";
							td += "<input type='checkbox' id='"+item.idContacto+"-"+losServicios[r-1]+"' value='"+item.idContacto+"-"+losServicios[r-1]+"' ";
							td += isChecked + " modified='false' >";
							td += "</td>";
							contTabla += td;
						}
					contTabla = contTabla + "</tr>";
					}
					contTabla = contTabla + "</table>";
					$('.lanzador').append(contTabla);
					$('.piePagina').append("<input type='button' class='mybutton' value='Asignar Servicio - Contacto' onclick='btnAsignar()' ><input type='reset' class='mybutton' value='Deshacer cambios' onclick='btnDeshacer()' >"
										  );
					$(':checkbox').unbind( "click" );
					$(':checkbox').click(function (){
						var tdId = "#td-"+ $(this).attr('id');
						console.log('¿ Esta modificado el campo '+ $(this).attr('id') +' ? ' + $(this).attr('modified') );
						console.log("Se modifica el el estilo a: " + tdId);
						 if( $(this).attr('modified') === 'true' ){
							$(this).attr('modified','false');
							$(tdId).removeClass("cambioCombo");
							$(tdId).removeClass("nocambioCombo");
						 }else{
							 $(this).attr('modified','true');
							 if( $(this).is(':checked') ){
								$(tdId).addClass("cambioCombo");
								$(tdId).removeClass("nocambioCombo");
							 }else{
								$(tdId).removeClass("cambioCombo");
								$(tdId).addClass("nocambioCombo");
							 }
						 }
						 console.log('Se modifica a: ' + $(this).attr('modified') );
						});
				} else {
					//aviso de que no hay datos para mostrar la tabla correctamente
					$('.lanzador').empty();
					$('.lanzador').append("<div class='error'>No hay datos suficientes para mostrar el Servicios a Cliente</div>");
				}
			} else {
				alert( "No hay resultados" );
				console.log( "ERROR: No hay resultados" );
			}
		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
	// Quitamos el DIV de "Cargando"...
	$.unblockUI();
}

/*asignar reglas*/
function btnAsignar(){

	$("[modified='true']").each(function (){
		var value = $(this).val();
		var position = value.split("-");
		var idServicio = position[1];
		var idContacto = position[0];
		if( $(this).is(':checked') ){
			console.log( "Esta modificado y chequeado "  +  value);
			altaServicioContacto(idServicio,idContacto);
	    }else{
	    	console.log( "Esta modificado y no chequeado "  + value );
	    	bajaServicioContacto(idServicio,idContacto);
	    }
	});
	valorSeleccionado = document.getElementById('aliasOculto').value;
	setTimeout(function () {cargarTabla(valorSeleccionado)}, 3000);
}

function bajaServicioContacto(idServicio, idContacto){
	var filtro = "{\"idServicio\" : \""+ idServicio + "\", \"idContacto\" : \""+ idContacto + "\" }";
	console.log(filtro);
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceServicios\", \"action\"  : \"bajaServicioContacto\", \"filters\"  : "+ filtro + "}",


		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}
function altaServicioContacto(idServicio, idContacto){
	var filtro = "{\"idServicio\" : \""+ idServicio + "\", \"idContacto\" : \""+ idContacto + "\" }";
	console.log(filtro);
	$.ajax({
		type: "POST",
		dataType: "json",
		url:  "/sibbac20back/rest/service",
		data: "{\"service\" : \"SIBBACServiceServicios\", \"action\"  : \"altaServicioContacto\", \"filters\"  : "+ filtro + "}",


		beforeSend: function( x ) {
			if(x && x.overrideMimeType) {
				x.overrideMimeType("application/json");
			}
			x.setRequestHeader("Accept", "application/json");
			x.setRequestHeader("Content-Type", "application/json");
			x.setRequestHeader("X-Requested-With", "HandMade");
			x.setRequestHeader("Access-Control-Allow-Origin", "*");
			x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		},
		async: true,
		success: function(json) {

		},
		error: function(c) {
			console.log( "ERROR: " + c );
		}
	});
}
function btnDeshacer(){
		valorSeleccionado = document.getElementById('aliasOculto').value;
		cargarTabla(valorSeleccionado);
}

//mostrar y ocultar tips
function mostrarServicio(id){
	  document.getElementById(id).style.display = "block";
	  document.getElementById(id).style.left = "500px";
	  document.getElementById(id).style.top = "160px";
}

function ocultarServicio(id){
      document.getElementById(id).style.display = "none";
}

function mostrarContacto(id){
	  document.getElementById(id).style.display = "block";
	  document.getElementById(id).style.left = "500px";
	  document.getElementById(id).style.top = "160px";
}

function ocultarContacto(id){
      document.getElementById(id).style.display = "none";
}
