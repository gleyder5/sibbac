//creo los 3 arrays necesarios (alta,update y baja)
var datosAltaServicio			= new Array();
var datosUpdateServicioRegla	= new Array();
var datosBajaServicio			= new Array();
var idOcultos					= [];
var availableTags				= [];


// Constantes
var URL_SEGUNDA					= "views/configuracion/serviciosCliente.html";
var _ID_ALIAS_CALLED			= null;

var NL							= "\n";
var TAB							= "\t";
var BR							= "<br/>";

var PREFIX_SERVICE				= "service-";
var PREFIX_RULE					= "rule-";
var PREFIX_SELECT				= "select-";

var CMD_ACTION					= "cmdACTION";
var CMD_DELETE					= "cmdDELETE";

var CMD_ACTIVE_ON				= "cmdACTIVE_ON";
var CMD_ACTIVE_OFF				= "cmdACTIVE_OFF";

var SERVICE_CMD_ALTA			= "altaServicio";
var SERVICE_CMD_UPDATE			= "updateServicioRegla";
var SERVICE_CMD_BAJA			= "bajaServicio";
var SERVICE_CMD_SERVICIO		= "updateActivoServicio";

var IMG_CMD						= "activo.png";
var IMG_DEL						= "desactivo.png";
var IMG_SIZE					= "12px";

var SHOW_LOG					= false;

var CURRENT_ALIAS				= null;




/*
 * ---- SAMPLE -----------------------------------------
 *                     "id" : 1
 *         "idReglaUsuario" : "Regla 1",
 *                 "activa" : true,
 *            "descripcion" : "regla de prueba",
 */
var ALL_REGLAS					= null;
var EMPTY_RULE					= {
	"id" : "-1",
	"idReglaUsuario" : "empty",
	"activa" : "true",
	"descripcion" : "Sin asignar",
};

/*
 * ---- SAMPLE -----------------------------------------
 *             "idServicio" : "1",
 *         "nombreServicio" : "F.Sugerida",
 *   "configuracionDeAlias" : "true",
 *                 "activo" : "true",
 *    "descripcionServicio" : "Generación de Facturas sugeridas"
 */
var ALL_SERVICIOS				= null;

/*
 * ---- SAMPLE -----------------------------------------
 *                     "id" : 90377,
 *                 "nombre" : "nullMI NUEVO ALIAS CON SETT MAP 10",
 *               "idIdioma" : 2,
 *       "descipcionIdioma" : "ESPAÑOL",
 *     "idAliasFacturacion" : null,
 * "nombreAliasFacturacion" : null
 */
var ALL_ALIAS					= null;

/*
 * ---- SAMPLE -----------------------------------------
 * {
 *   "servicios" : [ {
 *             "idServicio" : "1",
 *                "idRegla" : "41"
 *   }, {
 *             "idServicio" : "4",
 *                "idRegla" : "5"
 *   }, {
 *             "idServicio" : "5",
 *                "idRegla" : "41"
 *   } ],
 *   "idAlias" : 71429
 * }
 */
var ALL_CRUCES					= null;





$(document).ready(function() {
	/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		$('.resultados').hide();
		});
	}
	$(document).mousedown( keyPressed );

	// Cargamos la lista inicial
	// cargarTabla(0);
	cargaInicial();
});


function keyPressed(e) {
	var event = window.event ? window.event : e;

	var type = event.type;
	var button = event.button;
	var which = event.which;
	var shiftKey = event.shiftKey;
	var ctrlKey = event.ctrlKey;
	var altKey = event.altKey;
	var metaKey = event.metaKey;

	if ( which==3 && shiftKey && ctrlKey && ( metaKey || altKey ) ) {
		if ( event.stopPropagation ) {
			event.stopPropagation();
		}
		if ( event.cancelBubble!=null ) {
			event.cancelBubble = true;
		}
		document.oncontextmenu = function() {return false;}
		SHOW_LOG = !SHOW_LOG;
		console.log( "LOG JavaScript: ["+SHOW_LOG+"]" );
		return false;
	} else {
		document.oncontextmenu = function() {return true;}
		return true;
	}
}


/*
 * Hay que llamar a:
 *
 * SIBBACServiceServicios::getReglas()
 * SIBBACServiceServicios::getServicios()
 *
 * Y, luego, por cada alias que se desee:
 *
 * SIBBACServiceServicios::getCombinacion( idAlias )
 *
 */
function cargaInicial() {
	innerCargaReglas();
	innerCargaServicios();
	innerCargaAlias();
}


function innerCargaReglas() {
	var datos = new DataBean();
	datos.setService( "SIBBACServiceServicios" );
	datos.setAction( "getReglas" );
	var request = requestSIBBAC( datos );
	request.success( function( json ) {
		ALL_REGLAS = json.resultados.reglas;
		var item = null;
		for ( var s in ALL_REGLAS ) {
			item = ALL_REGLAS[ s ];
			if ( SHOW_LOG ) console.log( "> theRule: " + JSON.stringify( item ) );
		}
	});
	request.error(function(a, b, c){
		alert("ERROR cargando las reglas");
		if ( SHOW_LOG ) console.log( "ERROR: " + a );
		if ( SHOW_LOG ) console.log( "ERROR: " + b );
		if ( SHOW_LOG ) console.log( "ERROR: " + c );
	});
}
function innerCargaServicios() {
	var datos = new DataBean();
	datos.setService( "SIBBACServiceServicios" );
	datos.setAction( "getServicios" );
	var request = requestSIBBAC( datos );
	request.success( function( json ) {
		ALL_SERVICIOS = json.resultados.servicios;
		var item = null;
		for ( var s in ALL_SERVICIOS ) {
			item = ALL_SERVICIOS[ s ];
			if ( SHOW_LOG ) console.log( "> theService: " + JSON.stringify( item ) );
		}
	});
	request.error(function(a, b, c){
		alert("ERROR cargando los servicios");
		if ( SHOW_LOG ) console.log( "ERROR: " + a );
		if ( SHOW_LOG ) console.log( "ERROR: " + b );
		if ( SHOW_LOG ) console.log( "ERROR: " + c );
	});
}


function innerCargaAlias() {
	inicializarLoading();
	var datos = new DataBean();
	datos.setService( "SIBBACServiceAlias" );
	datos.setAction( "getListaAliasSinFacturacion" );
	var request = requestSIBBAC( datos );
	request.success( function( json ) {
		var resultados = json.resultados.listaAlias;
		ALL_ALIAS = resultados;
		if ( SHOW_LOG ) console.log( "Alias recuperados: " + ALL_ALIAS.length );

		var item = null;
		var k = 0;
		for ( k in resultados ) {

			item = resultados[ k ];
			idOcultos.push(item.id);
			var ponerTag = item.nombre.trim();
			availableTags.push(ponerTag);
		}
		//código de autocompletar

		$( "#textAlias" ).autocomplete({
			source: availableTags
		});
	});
	request.error(function(a, b, c){
		alert("ERROR cargando los alias");
		if ( SHOW_LOG ) console.log( "ERROR: " + a );
		if ( SHOW_LOG ) console.log( "ERROR: " + b );
		if ( SHOW_LOG ) console.log( "ERROR: " + c );
	});
}


/*cargar tabla alias*/
$('.btn.buscador').click(function(){
	//event.preventDefault();
	var ok = true;
	var idAlias = document.getElementById("textAlias");
	switch ( this.id ) {
		case "limpiar":
			idAlias.value = "";
			// Quitamos la tabla.
			$('.lanzador').empty();
			break;
		default:
			var listaAlias = idAlias.value;
			posicionAlias = availableTags.indexOf( listaAlias );
			if ( posicionAlias=="-1" ) {
				alert( "El alias introducido no existe" );
				ok = false;
			} else {
				valorSeleccionadoAlias = idOcultos[ posicionAlias ];
				cargarTabla( valorSeleccionadoAlias );
			}
	}
	return ok;
});


/*
 * Funcion para controlar qué hacer cuando el usuario hace click en un alias.
 */
function go2SecondPage(idAlias) {
	// Primero, hay que ver si tenemos cambios pendientes...
	var changeds			= $( "table.GestorServicios td[data-changed]" );
	var anyChanged			= false;
	var nChanges			= 0;
	var change				= null;
	if ( changeds.length>0 ) {
		var changed			= false;
		var total			= changeds.length;
		if ( SHOW_LOG ) console.log("[go2SecondPage] [total=="+changeds.length+"]");
		for ( var r=0; r<total; r++ ) {
			change			= changeds[ r ];
			if ( SHOW_LOG ) console.log("[go2SecondPage] [r=="+r+"]");
			changed			= change.getAttribute("data-changed");
			if ( SHOW_LOG ) console.log("[go2SecondPage] [data-changed=="+changed+"]");
			changed			= ( changed!=null ) ? ( changed=="true" ) : false;
			if ( SHOW_LOG ) console.log("[go2SecondPage] [change.id=="+change.id+"] [data-changed=="+changed+"]");
			anyChanged		= anyChanged || changed;
			nChanges		+= ( changed ) ? 1 : 0;
			if ( SHOW_LOG ) console.log("[go2SecondPage] [anyChanged=="+anyChanged+"] [nChanges=="+nChanges+"]");
		}
	}
	if ( SHOW_LOG ) console.log("[go2SecondPage] FINAL [anyChanged=="+anyChanged+"] [nChanges=="+nChanges+"]");

	if ( anyChanged ) {
		alert( "Tiene aun "+nChanges+" cambio(s) pendiente(s)" );
	} else {
		_ID_ALIAS_CALLED = idAlias;
		$("section").load( URL_SEGUNDA, go2SecondPageCallBack );
	}
	return false;
}


/*
 * Funcion para controlar lo que se debe hacer tras cargar la segunda pagina.
 */
function go2SecondPageCallBack() {
	// Llamamos a la funcion de la segunda pagina que carga la tabla con el idAlias.
	cargarTabla( _ID_ALIAS_CALLED );
	_ID_ALIAS_CALLED = null;
}


//esta es la tabla que carga todos los datos
function cargarTabla( idAliasRequested ) {
	if ( SHOW_LOG ) console.log("alias: " + idAliasRequested );
	CURRENT_ALIAS							= idAliasRequested;
	document.getElementById('aliasOculto').value = idAliasRequested;
	if ( SHOW_LOG ) console.log("entro en el método");
	var filtro = "{\"idAlias\" : \""+ idAliasRequested + "\" }";

	if ( SHOW_LOG ) console.log( "Cargando las combinaciones del alias "+idAliasRequested+"..." );
	var datos								= new DataBean();
	datos.setService( "SIBBACServiceServicios" );
	datos.setAction( "getCombinacion" );
	var filters								= "{ \"idAlias\" : \""+ idAliasRequested + "\" }";
	datos.setFilters( filters );
	var request = requestSIBBAC( datos );

	// El tratamiento de la respuesta.
	request.success( function( json ) {

		if ( json.error!==null && json.error!==undefined ) {
			if ( SHOW_LOG ) console.log( "ERROR: " + json.error );
		}

		ALL_CRUCES							= json.resultados.combinacion;
		var numCombinaciones				= (ALL_CRUCES!==undefined) ? ALL_CRUCES.length : 0;

		if ( SHOW_LOG ) console.log( "Resultados: " + JSON.stringify( ALL_CRUCES ) );
		var idAliasRequested				= (json.request.filters.idAlias!==undefined) ? json.request.filters.idAlias : "";
		var theAlias						= getAlias( idAliasRequested );

		// Ahora, pintamos la tabla
		var theRuleId						= null;
		var theRule							= null;
		var theCombination					= null;
		var theCrossService					= null;
		var theServiceId					= null;
		var theService						= null;
		var html							= null;
		var forAlias						= null;


		/*
		 * Vamos listando todos los servicios que aplican a alias y,
		 * por cada servicio encontrado, miramos a ver si tiene combinacion.
		 * Si la tiene, pintamos la regla que diga; si no, "empty".
		 */
		html = HTMLTableHeader();
		html += HTMLRowHeader( theAlias );

		for ( var s in ALL_SERVICIOS ) {
			theService						= ALL_SERVICIOS[ s ];
			if ( SHOW_LOG ) console.log( "[Servicio Registrado("+s+")] [idServicio=="+theService.idServicio+"] [nombreServicio=="+theService.nombreServicio+"] [configuracionDeAlias=="+theService.configuracionDeAlias+"] [activo=="+theService.activo+"] [descripcionServicio=="+theService.descripcionServicio+"]" );
			forAlias						= ( theService.configuracionDeAlias!=undefined ) ? (theService.configuracionDeAlias=='true') : false;
			if ( !forAlias ) {
				continue;
			}

			// Es un servicio configurable para un alias.
			// Miramos a ver si ha venido en las combinaciones...
			theCombination					= getCombinacion( theService.idServicio );
			if ( SHOW_LOG ) console.log( ">  theCombination: " + JSON.stringify( theCombination ) );
			if ( theCombination!=null ) {
				// Tenemos el servicio en la combinacion.
				theServiceId				= theCombination.idServicio;
				if ( SHOW_LOG ) console.log( ">    theServiceId: " + theServiceId );
				theRuleId					= theCombination.idRegla;
				if ( SHOW_LOG ) console.log( ">       theRuleId: " + theServiceId );
				theCrossService				= getServicio( theServiceId );
				if ( SHOW_LOG ) console.log( "> theCrossService: " + JSON.stringify( theCrossService ) );
				theRule						= getRegla( theRuleId );
				if ( SHOW_LOG ) console.log( ">         theRule: " + JSON.stringify( theRule ) );
			} else {
				// No lo tenemos. Pintamos la "regla" por defecto: "empty".
				theCrossService				= theService;
				theRule						= EMPTY_RULE;
			}
			html += HTMLRowCross( theCrossService, s, theRule );

		}
		html += HTMLTableFooter();
		$('.lanzador').html( html );

	});
	request.error(function(a, b, c){
		alert("ERROR cargando las combinaciones del alias "+alias);
		if ( SHOW_LOG ) console.error( "ERROR: " + a );
		if ( SHOW_LOG ) console.error( "ERROR: " + b );
		if ( SHOW_LOG ) console.error( "ERROR: " + c );
	});
}

function sortCombinaciones(a,b) {	return parseInt(a.id) > parseInt(b.id); }
function sortAliases(a,b) {
	var aName = a.nombre.split( " - " )[1];
	var bName = b.nombre.split( " - " )[1];
	return aName > bName;
}
function sortReglas(a,b) {
	var aName = a.idReglaUsuario;
	var bName = b.idReglaUsuario;
	return aName > bName;
}
function sortServicios(a,b) {
	var aName = a.nombreServicio;
	var bName = b.nombreServicio;
	return aName > bName;
}


function getRegla(id) {
	if ( id===null || id===undefined ) { return null; }
	var item = null;
	for ( var r in ALL_REGLAS ) {
		item = ALL_REGLAS[ r ];
		if ( item.id==id ) { break; } else { item = null; }
	}
	return item;
}


function getServicio(id) {
	if ( id===null || id===undefined ) { return null; }
	var item = null;
	for ( var r in ALL_SERVICIOS ) {
		item = ALL_SERVICIOS[ r ];
		if ( item.idServicio==id ) { break; } else { item = null; }
	}
	return item;
}


function getCombinacion(id) {
	if ( id===null || id===undefined ) { return null; }
	if ( ALL_CRUCES===null || ALL_CRUCES===undefined ) { return null; }
	if ( ALL_CRUCES[0]===null || ALL_CRUCES[0]===undefined ) { return null; }
	var item = null;
	for ( var r in ALL_CRUCES[0].servicios ) {
		item = ALL_CRUCES[0].servicios[ r ];
		if ( item.idServicio==id ) { break; } else { item = null; }
	}
	return item;
}


function getAlias(id) {
	if ( id===null || id===undefined ) { return null; }
	var item = null;
	for ( var r in ALL_ALIAS ) {
		item = ALL_ALIAS[ r ];
		if ( item.id==id ) { break; } else { item = null; }
	}
	return item;
}


function syntaxHighlight(json) {
	if (typeof json != 'string') {
		 json = JSON.stringify(json, undefined, 2);
	}
	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
		var cls = 'number';
		if (/^"/.test(match)) {
			if (/:$/.test(match)) {
				cls = 'key';
			} else {
				cls = 'string';
			}
		} else if (/true|false/.test(match)) {
			cls = 'boolean';
		} else if (/null/.test(match)) {
			cls = 'null';
		}
		return '<span class="' + cls + '">' + match + '</span>';
	});
}


function HTMLTableHeader() {
	var html = "";
	html += "<table class=\"GestorServicios\">" + NL;
	return html;
}

function HTMLRowHeader( theAlias ) {
	var html		= "";
	html			+= "<tr>" + NL;
	html			+= "<th colspan=\"3\" onClick='go2SecondPage(\""+theAlias.id+"\")' title=\"Click para ir al detalle\">" + theAlias.nombre +" &nbsp;&nbsp;&nbsp;&nbsp;(<i>Click for details</i>)</th>" + NL;
	html			+= "</tr>" + NL;
	return html;
}

function HTMLTableFooter() {
	var html		= "";
	html			+= "</table>" + NL;
	return html;
}

function HTMLRowCross( theService, r, theRule ) {
	var idService		= theService.idServicio;
	var activeRule		= ( theRule.activa || theRule.activa==="true" ) ? "" : " (inactiva)";
	var idRule			= theRule.id;
	var html			= "";
	var imgDel			= "inline";
	var imgActive		= ( theService.activo!=null ) ? theService.activo=="true" : "";
	var stylePARAR		= ( !imgActive ) ? "none" : "inline";
	var styleACTIVAR	= ( imgActive ) ? "none" : "inline";
	html				+= "<tr>" + NL;
	if ( SHOW_LOG ) console.log( "[HTMLRowCross] [imgActive=="+imgActive+"] [stylePARAR=="+stylePARAR+"] [styleACTIVAR=="+styleACTIVAR+"]" );

	// El servicio
	html			+= TAB + "<td id=\""+PREFIX_SERVICE+idService+"\" data-id=\""+idService+"\" data-active=\""+theService.activo+"\" title=\"" + theService.nombreServicio+"\">" + theService.descripcionServicio +"</td>" + NL;

	// Los iconos de control del servicio
	html			+= TAB + "<td>"
					// Las imagenes de accion
					+" <img src=\"img/"+IMG_DEL+"\" id=\""+CMD_ACTIVE_ON+"_"+r+"\"  data-action=\""+CMD_ACTIVE_ON+"\" style=\"display: "+styleACTIVAR+"; vertical-align: text-bottom;\" height=\""+IMG_SIZE+"\" title=\"Inactivo. Click para activar\" onClick=\"toggleServiceStatus(this, '"+idService+"', '"+r+"')\">"
					+" <img src=\"img/"+IMG_CMD+"\" id=\""+CMD_ACTIVE_OFF+"_"+r+"\" data-action=\""+CMD_ACTIVE_OFF+"\" style=\"display: "+stylePARAR+"; vertical-align: text-bottom;\" height=\""+IMG_SIZE+"\" title=\"Activo. Click para desactivar\"  onClick=\"toggleServiceStatus(this, '"+idService+"', '"+r+"')\">"
					// El campo donde poner la descripcion, a la derecha
					+ "</td>" + NL;

	// Las reglas
	html			+= TAB + "<td id=\""+PREFIX_SERVICE+idService+"-"+PREFIX_RULE+r+"\" data-id=\""+idRule+"\" data-active=\""+theRule.activa+"\" data-changed=\"false\">" + HTMLGenerateSelect( theService, r, theRule )
					// Las imagenes de accion
					+" <img src=\"img/"+IMG_CMD+"\" id=\""+CMD_ACTION+"_"+r+"\" data-action=\""+CMD_ACTION+"\" style=\"display: none;   vertical-align: text-bottom;\" height=\""+IMG_SIZE+"\" title=\"Validar\"  onClick=\"userConfirmAction(this, '"+idService+"', '"+r+"','')\">"
					+" <img src=\"img/"+IMG_DEL+"\" id=\""+CMD_DELETE+"_"+r+"\" data-action=\""+CMD_DELETE+"\" style=\"display: inline; vertical-align: text-bottom;\" height=\""+IMG_SIZE+"\" title=\"Eliminar\" onClick=\"userConfirmAction(this, '"+idService+"', '"+r+"','"+CMD_DELETE+"')\">"
					// El campo donde poner la descripcion, a la derecha
					+ "<span id=\"span-"+PREFIX_SERVICE+idService+"-"+PREFIX_RULE+r+"\">"+theRule.descripcion+"</span>" + NL;
					+ "</td>" + NL;
	html			+= "</tr>" + NL;
	return html;
}


function HTMLGenerateSelect( theService, r, theRule ) {
	var activeRule	= null;
	var currentRule	= null;
	var currentHtml	= null;
	var idService	= theService.idServicio;
	var idDefRule	= theRule.id;
	var idRule		= null;
	var rule		= null;
	var html		= "";
	if ( SHOW_LOG ) console.log( "[HTMLGenerateSelect] [Service=="+theService.nombreServicio+"] [Rule("+theRule.id+")=="+theRule.descripcion+"]" );
	html			+= "<select id=\""+PREFIX_SELECT+idService+"\" onChange=\"ruleSelectChanged( '"+idService+"', '"+r+"' )\" onUpdate=\"ruleSelectChanged( '"+idService+"', '"+r+"' )\">" + NL;
	html			+= HTMLGenerateOption( EMPTY_RULE, "", "", " (active)" );
	for ( var r in ALL_REGLAS ) {
		rule		= ALL_REGLAS[ r ];
		idRule		= rule.id;
		currentRule	= ( idRule==theRule.id ) ? "  selected=\"selected\" style=\"font-weight: bold; color: red;\"" : "";
		currentHtml	= ( currentRule ) ? "[DEFAULT] " : "";
		activeRule	= ( theRule.activa || theRule.activa==="true" ) ? " (active)" : "";
		html		+= HTMLGenerateOption( rule, currentRule, currentHtml, activeRule );
	}
	html += "</select>" + NL;
	return html;
}


function HTMLGenerateOption( theRule, currentRule, currentHtml, activeRule ) {
	if ( SHOW_LOG ) console.log( "[HTMLGenerateOption] [Rule("+theRule.id+")=="+theRule.descripcion+"] [currentRule=="+currentRule+"] [currentHtml=="+currentHtml+"] [activeRule=="+activeRule+"]" );
	return TAB + "<option value=\"" + theRule.id+"\" title=\""+ currentHtml + theRule.descripcion + activeRule + "\" "+currentRule+">" + theRule.idReglaUsuario + "</option>" + NL;
}


function ruleSelectChanged( idService, r ) {
	if ( SHOW_LOG ) console.log( "[ruleSelectChanged] [idService=="+idService+"] [r=="+r+"]" );
	var theService		= getServicio( idService );
	var tdService		= document.getElementById( PREFIX_SERVICE+idService );
	var tdRule			= document.getElementById( PREFIX_SERVICE+idService+"-"+PREFIX_RULE+r );
	var tdSelect		= document.getElementById( PREFIX_SELECT+idService );
	var theSpan			= document.getElementById( "span-"+PREFIX_SERVICE+idService+"-"+PREFIX_RULE+r );
	var idx				= tdSelect.selectedIndex;
	var currentRuleID	= tdRule.getAttribute('data-id');
	if ( SHOW_LOG ) console.log( "[ruleSelectChanged] [currentRuleID=="+currentRuleID+"]" );
	var theRule			= ( currentRuleID!=null ) ? getRegla( currentRuleID ) : null;
	var selectedRuleID	= tdSelect.options[idx].value;
	if ( SHOW_LOG ) console.log( "[ruleSelectChanged] [selectedRuleID=="+selectedRuleID+"]" );
	var tdSelectedRule	= getRegla( selectedRuleID );
	var hasChanged		= ( selectedRuleID!=currentRuleID );
	var isTheDefault	= ( selectedRuleID==currentRuleID );
	if ( SHOW_LOG ) console.log( "[ruleSelectChanged] [Service=="+theService.nombreServicio+"] [Rule=="+theRule+"] [TDService=="+tdService.id+"] [tdRule=="+tdRule.id+"] [idx=="+idx+"] [Select=="+selectedRuleID+"]" );
	if ( SHOW_LOG ) console.log( "[ruleSelectChanged] [tdService] [data-id=="+tdService.getAttribute('data-id')+"]" );
	if ( SHOW_LOG ) console.log( "[ruleSelectChanged] [tdRule] [selectedRuleID=="+selectedRuleID+"] [hasChanged=="+hasChanged+"] [data-id=="+tdRule.getAttribute('data-id')+"] [data-active=="+tdRule.getAttribute('data-active')+"] [data-changed=="+tdRule.getAttribute('data-changed')+"]" );
	theSpan.innerHTML	= ( tdSelectedRule!=null ) ? tdSelectedRule.descripcion : "*** Sin Regla ***";
	if ( SHOW_LOG ) console.log( "[ruleSelectChanged] [tdService] [theSpan=="+theSpan.id+"] [innerHTML=="+theSpan.innerHTML+"]" );
	var tdImgACTION		= document.getElementById( CMD_ACTION+"_"+r );
	var tdImgDELETE		= document.getElementById( CMD_DELETE+"_"+r );
	if ( hasChanged ) {
		tdRule.setAttribute('data-changed', 'true');
		tdImgACTION.style.display	= "";
		tdImgDELETE.style.display	= "none";
	} else {
		tdRule.setAttribute('data-changed', 'false');
		tdImgACTION.style.display	= "none";
		tdImgDELETE.style.display	= "";
	}
	return true;
}

function toggleServiceStatus( obj, idService, r ) {
	var command					= obj.id;
	var action					= obj.getAttribute( "data-action" );
	if ( SHOW_LOG ) console.log( "[toggleServiceStatus] [Command=="+command+"] [Action=="+action+"] [idService=="+idService+"] [r=="+r+"]" );

	// Los JSON de los objetos.
	var theService				= getServicio( idService );
	var status					= (action==CMD_ACTIVE_OFF) ? "false" : "true";

	var tdImgACTIVE_ON			= document.getElementById( CMD_ACTIVE_ON+"_"+r );
	var tdImgACTIVE_OFF			= document.getElementById( CMD_ACTIVE_OFF+"_"+r );
	var tdService				= document.getElementById( PREFIX_SERVICE+idService );

	var datos						= new DataBean();
	datos.setService( "SIBBACServiceServicios" );
	datos.setAction( SERVICE_CMD_SERVICIO );
	var filters						= "{"
		+ "\"activoServicio\" : \""+ status + "\""
		+ "\"idServicio\" : \""+ idService + "\""
		+ " }";
	datos.setFilters( filters );
	var request = requestSIBBAC( datos );
	request.success( function( json ) {
		// Todo OK.
		if ( SHOW_LOG ) console.log( "[toggleServiceStatus] [OK]" );

		if ( action==CMD_ACTIVE_ON ) {
			tdService.setAttribute( "data-active", "true" );
			tdImgACTIVE_ON.style.display	= "none";
			tdImgACTIVE_OFF.style.display	= "inline";
		} else {
			tdService.setAttribute( "data-active", "false" );
			tdImgACTIVE_ON.style.display	= "inline";
			tdImgACTIVE_OFF.style.display	= "none";
		}
	});
	request.error(function(a, b, c){
		if ( SHOW_LOG ) console.error( "ERROR: " + a );
		if ( SHOW_LOG ) console.error( "ERROR: " + b );
		if ( SHOW_LOG ) console.error( "ERROR: " + c );
		alert( "ERROR cambiando el estado del servicio \""+theService.descripcionServicio );
		console.warn( "ERROR cambiando el estado del servicio \""+theService.descripcionServicio );
		// alert( "No se han recibido datos para el ID Alias: " + idAliasRequested );
	});

}

function userConfirmAction( obj, idService, r, accion ) {
	var command			= obj.id;
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [Command=="+command+"] [idService=="+idService+"] [r=="+r+"] [accion=="+accion+"]");

	// Los JSON de los objetos.
	var theService		= getServicio( idService );
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [theService=="+theService+"]");

	// Los ID's de los elementos HTML
	var idHTMLService	= PREFIX_SERVICE+idService;
	var idHTMLRule		= PREFIX_SERVICE+idService+"-"+PREFIX_RULE+r;
	var idHTMLSelect	= PREFIX_SELECT+idService;
	var idHTMLSpan		= "span-"+PREFIX_SERVICE+idService+"-"+PREFIX_RULE+r;
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [idHTMLService=="+idHTMLService+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [idHTMLRule=="+idHTMLRule+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [idHTMLSelect=="+idHTMLSelect+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [idHTMLSpan=="+idHTMLSpan+"]");

	// Los elementos HTML
	var tdService		= document.getElementById( idHTMLService );
	var tdRule			= document.getElementById( idHTMLRule );
	var tdSelect		= document.getElementById( idHTMLSelect );
	var tdSpan			= document.getElementById( idHTMLSpan );
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [tdService=="+tdService+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [tdRule=="+tdRule+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [tdSelect=="+tdSelect+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [tdSpan=="+tdSpan+"]");

	// Informacion util
	var currentRuleID	= tdRule.getAttribute('data-id');
	var theCurrentRule	= getRegla( currentRuleID );
	var idx				= tdSelect.selectedIndex;
	var selectedRuleID	= tdSelect.options[idx].value;
	var theDesiredRule	= getRegla( selectedRuleID );
	var hasCurrentRule	= ( theCurrentRule!=null );
	var hasDesiredRule	= ( theDesiredRule!=null );
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [currentRuleID=="+currentRuleID+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [theCurrentRule=="+theCurrentRule+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [idx=="+idx+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [selectedRuleID=="+selectedRuleID+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [theDesiredRule=="+theDesiredRule+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [hasCurrentRule=="+hasCurrentRule+"]");
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [hasDesiredRule=="+hasDesiredRule+"]");

	// Evaluamos...
	var action			= null;
	if ( accion==CMD_DELETE ) {
		if ( SHOW_LOG ) console.log( "[userConfirmAction] [CMD_DELETE]");
		if ( hasCurrentRule ) {
			// Una baja.
			action			= "BAJA";
			tdSelect.selectedIndex = 0;
			selectedRuleID	= tdSelect.options[0].value;
			ruleSelectChanged( idService, r );
		} else {
			// Nothing
		}
	} else {
		if ( SHOW_LOG ) console.log( "[userConfirmAction] [!CMD_DELETE]");
		if ( !hasCurrentRule && hasDesiredRule ) {
			// Un alta.
			action			= "ALTA";
		} else if ( hasCurrentRule && hasDesiredRule ) {
			// Una modificacion.
			action			= "EDIT";
		} else if ( hasCurrentRule && !hasDesiredRule ) {
			// Una baja.
			action			= "BAJA";
			tdSelect.selectedIndex = 0;
			selectedRuleID	= tdSelect.options[0].value;
			ruleSelectChanged( idService, r );
		} else {
			// Nothing
		}
	}
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [action=="+action+"]");

	if ( SHOW_LOG ) console.log( "[userConfirmAction] [tdService=="+tdService.id+"] [tdRule=="+tdRule.id+"] [tdSelect=="+tdSelect.id+"] [tdSpan=="+tdSpan.id+"]" );
	if ( SHOW_LOG ) console.log( "[userConfirmAction] [action=="+action+"] [theCurrentRule=="+theCurrentRule+"] [theDesiredRule=="+theDesiredRule+"]" );

	if ( action=="ALTA" ) {
		// Alta
		cmdALTARule4ServiceAndAlias( idService, r, selectedRuleID );
	} else if ( action=="EDIT" ) {
		// Modificacion
		cmdEDITRule4ServiceAndAlias( idService, r, selectedRuleID );
	} else {
		// Baja
		cmdBAJARule4ServiceAndAlias( idService, r, selectedRuleID );
	}
}


function cmdALTARule4ServiceAndAlias( idService, r, idRule ) {
	if ( SHOW_LOG ) console.log( "[cmdALTARule4ServiceAndAlias] [Current Alias=="+CURRENT_ALIAS+"] [idService=="+idService+"] [idRule=="+idRule+"]" );
	cmdINNER_R4SA( SERVICE_CMD_ALTA, idService, r, idRule );
}


function cmdEDITRule4ServiceAndAlias( idService, r, idRule ) {
	if ( SHOW_LOG ) console.log( "[cmdEDITRule4ServiceAndAlias] [Current Alias=="+CURRENT_ALIAS+"] [idService=="+idService+"] [idRule=="+idRule+"]" );
	cmdINNER_R4SA( SERVICE_CMD_UPDATE, idService, r, idRule );
}


function cmdBAJARule4ServiceAndAlias( idService, r, idRule ) {
	if ( SHOW_LOG ) console.log( "[cmdBAJARule4ServiceAndAlias] [Current Alias=="+CURRENT_ALIAS+"] [idService=="+idService+"] [idRule=="+idRule+"]" );
	cmdINNER_R4SA( SERVICE_CMD_BAJA, idService, r, idRule );
}

function cmdINNER_R4SA( cmd, idService, r, idRule ) {
	var emptyRuleID					= (idRule==null || idRule==-1);
	if ( SHOW_LOG ) console.log( "[cmdINNER_R4SA] [idRule=="+idRule+"] [emptyRuleID=="+emptyRuleID+"]" );
	var datos						= new DataBean();
	datos.setService( "SIBBACServiceServicios" );
	datos.setAction( cmd );
	var filters						= "{"
		+ " \"idAlias\" : \""+ CURRENT_ALIAS + "\""
		+ ", \"idRegla\" : \""+ idRule + "\""
		+ ", \"idServicio\" : \""+ idService + "\""
		+ " }";
	datos.setFilters( filters );
	var request = requestSIBBAC( datos );
	request.success( function( json ) {
		// Todo OK.
		if ( SHOW_LOG ) console.log( "[cmdINNER_R4SA] [OK]" );
		// Cambiamos el "data-id" y lo demas.
		var tdRuleID				= PREFIX_SERVICE+idService+"-"+PREFIX_RULE+r;
		if ( SHOW_LOG ) console.log( "[cmdINNER_R4SA] [tdRuleID=="+tdRuleID+"]" );
		var tdRule					= document.getElementById( tdRuleID );
		if ( SHOW_LOG ) console.log( "[cmdINNER_R4SA] [tdRule.id=="+tdRule.id+"]" );
		tdRule.setAttribute( "data-id", (!emptyRuleID)?idRule:"" );
		// TODO: Aun no sabemos... tdRule.setAttribute( "data-active", idRule );
		tdRule.setAttribute( "data-changed", "false" );

		// Las imagenes
		var tdImgACTION				= document.getElementById( CMD_ACTION+"_"+r );
		var tdImgDELETE				= document.getElementById( CMD_DELETE+"_"+r );
		if ( SHOW_LOG ) console.log( "[cmdINNER_R4SA] [tdImgACTION=="+tdImgACTION+"]" );
		if ( SHOW_LOG ) console.log( "[cmdINNER_R4SA] [tdImgDELETE=="+tdImgDELETE+"]" );
		if ( SHOW_LOG ) console.log( "[cmdINNER_R4SA] [emptyRuleID=="+emptyRuleID+"]" );
		tdImgACTION.style.display	= "none";
		tdImgDELETE.style.display	= (emptyRuleID) ? "none" : "";
	});
	request.error(function(a, b, c){
		if ( SHOW_LOG ) console.error( "ERROR: " + a );
		if ( SHOW_LOG ) console.error( "ERROR: " + b );
		if ( SHOW_LOG ) console.error( "ERROR: " + c );
		console.warn( "ERROR con el comando \""+cmd+"\" y alias: " + idAliasRequested );
		alert( "ERROR con el comando \""+cmd+"\" y alias: " + idAliasRequested );
	});
}
