var idOcultos = [];
var availableTags = [];
var oTable = undefined;


$(document).ready(function () {

	oTable = $("#tablaPrueba").dataTable({
		"dom": 'T<"clear">lfrtip',
         "tableTools": {
             "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
         },
         "language": {
             "url": "i18n/Spanish.json"
         },
         "scrollY": "480px",
         "scrollCollapse": true,
         "columns" : [
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"},
                 	 {"width": "auto"}
                  ]
	});

    $('a[href="#release-history"]').toggle(function () {
		$('#release-wrapper').animate({
			marginTop: '0px'
		}, 600, 'linear');
	}, function () {
		$('#release-wrapper').animate({
			marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
		}, 600, 'linear');
	});

	$('#download a').mousedown(function () {
		_gaq.push(['_trackEvent', 'download-button', 'clicked'])
	});

	$('.collapser').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		//$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Clic para mostrar','Clic para ocultar');

		return false;
	});
	$('.collapser_search').click(function(){
		$(this).parents('.title_section').next().slideToggle();
		$(this).parents('.title_section').next().next('.button_holder').slideToggle();
		$(this).toggleClass('active');
		$(this).toggleText('Mostrar Alta de Contactos','Ocultar Alta de Contactos');
		return false;
	});

/*BUSCADOR*/
	$('.btn.buscador').click(function(){
		//event.preventDefault();
			//$('.resultados').show();
			$('.collapser_search').parents('.title_section').next().slideToggle();
			$('.collapser_search').toggleClass('active');
			$('.collapser_search').toggleText('Mostrar opciones de búsqueda','Ocultar opciones de búsqueda');

		return false;
	});
/*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	if($('.contenedorTabla').length >=1){
		var anchoBotonera;
		$('.contenedorTabla').each(function( i ) {
		anchoBotonera = $(this).find('table').outerWidth();
		$(this).find('.botonera').css('width', anchoBotonera+'px');
		$(this).find('.resumen').css('width', anchoBotonera+'px');
		//$('.resultados').hide();
		});
	}


	$('#btnAltaContacto').click(function(){
		email =  document.getElementById('textEmail').value;

		var listaEmail = email.split(";");
		var todosValidos = true;
		for(var k in listaEmail ) {
			item = listaEmail[ k ].trim();
			if (!validarEmail(item))
				{
				todosValidos = false;
				}
		}

		var listaAlias = document.getElementById("textAlias").value;

		posicionAlias = availableTags.indexOf(listaAlias);
		if(posicionAlias=="-1"){

			return false;
		}
		valorSeleccionadoAlias = idOcultos[posicionAlias];

	    var nombre = document.getElementById('textNombre').value;

	   var ape1 = document.getElementById('textApel1').value;

	    if (nombre=="")
	    	{
	    	mensajeError("Debe escribir un nombre");
	    	return false;
	    	}
	    else if (ape1=="")
	    	{
	    	mensajeError("Debe escribir el primer apellido");
	    	return false;
	    	}
	    else{
	    	if (todosValidos)
			{
	    		crearContacto(email, valorSeleccionadoAlias, nombre, ape1);
			}
	    	else
			{
	    		return false;
			}
	    }
	});

	//cargarTabla();
	cargarAlias();
	//editarContacto();
	cargartabla();

});


$('#editarContacto').click(function(){

	idAlias =  document.getElementById('textIdAlias').value;
	nombre = document.getElementById('textNombre1').value;
	apellido1 = document.getElementById('textApel11').value;
	apellido2 = document.getElementById('textApel21').value;
	telefono1 = document.getElementById('textTelefono1').value;
	telefonoMovil = document.getElementById('textMovil1').value;
	fax = document.getElementById('textFax1').value;
	email =  document.getElementById('textEmail1').value;
	cargo = document.getElementById('textCargo1').value;
	activo = document.getElementById('chkActiv').checked;
	defecto = document.getElementById('chkDefecto').checked;
	gestor = document.getElementById('textGestor1').checked;


	//alert("defecto = " + defecto);
	//var pordefecto = ( defecto==true ) ? "1" : "0";
	//alert("por defecto = " + pordefecto);
	//return false;
	//document.getElementById('chkDefecto').disabled=false;
	//document.getElementById('chkDefecto').checked='';
	var activar = ( activo=="true" ) ? "checked='checked'" : "";


	textIdContacto =  document.getElementById('textIdContacto').value;

		  var descripcion = "";
		  var telefono2 = "";
		  var comentarios = "";


	var listaEmail = email.split(";");

	var todosValidos = true;
	for(var k in listaEmail ) {
		item = listaEmail[ k ].trim();
		if (!validarEmail(item))
			{
			todosValidos = false;
			}
	}

	if (todosValidos)
	{
			var filtro = "{\"id\" : \""+ textIdContacto + "\"," +
			      "\"descripcion\" : \""+descripcion+ "\"," +
			      "\"telefono1\" : \""+ telefono1 + "\"," +
			      "\"telefono2\" : \""+ telefono2 + "\"," +
			      "\"movil\" : \""+ telefonoMovil + "\"," +
			      "\"fax\" : \""+ fax + "\"," +
			      "\"email\" : \""+ email + "\"," +
			      "\"comentarios\" : \""+comentarios  + "\"," +
			      "\"posicionCargo\" : \""+ cargo + "\"," +
			      "\"activo\" : \""+ activo + "\"," +
			      "\"nombre\" : \""+ nombre + "\"," +
			      "\"apellido1\" : \""+ apellido1 + "\"," +
			      "\"apellido2\" : \""+ apellido2 + "\"," +
			      "\"gestor\" : \""+ gestor + "\"," +
			      "\"defecto\" : \""+ defecto + "\" }";

			var jsonData = "{" +
				"\"service\" : \"SIBBACServiceContacto\", " +
				"\"action\"  : \"modificacionContacto\", "+
				"\"filters\"  : "+ filtro + "}";


			$.ajax({
				type: "POST",
				dataType: "json",
				url:  "/sibbac20back/rest/service",
				data: jsonData,
				beforeSend: function( x ) {
				if(x && x.overrideMimeType) {
					x.overrideMimeType("application/json");
				}
				// CORS Related
				x.setRequestHeader("Accept", "application/json");
				x.setRequestHeader("Content-Type", "application/json");
				x.setRequestHeader("X-Requested-With", "HandMade");
				x.setRequestHeader("Access-Control-Allow-Origin", "*");
				x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
			},
			async: true,
			success: function(json) {
				$("section").load("views/contactos/contactos.html");
			},
			error: function(c) {
				mensajeError( "ERROR: " + c );
			}

		});
	}
	else
	{
	    return false;
	}



})





function cargarAlias()
{



	var params = new DataBean();
	params.setService('SIBBACServiceTmct0ali');
	params.setAction('getAlias');
	var request = requestSIBBAC(params);
	request.success(function(json) {
		var datos = undefined;

		if (json === undefined || json.resultados === undefined
				|| json.resultados.result_alias === undefined) {
			datos = {};
		} else {
			datos = json.resultados.result_alias;
			var item = null;

			for ( var k in datos) {
				item = datos[k];
				idOcultos.push(item.alias);
				ponerTag = item.alias + " - " + item.descripcion;
				availableTags.push(ponerTag);
			}
			// código de autocompletar

			$("#textAlias").autocomplete({
				source : availableTags
			});
			$("#textAliasB").autocomplete({
				source : availableTags
			});
		}
		// fin de código de autocompletar
	});
	request.error( function(c) {
		console.log( "ERROR: " + c );
	});

	}


function cargoInputsContacto(idAlias,id,nombr,apellido1,apellido2,telefono1,movil,fax,activo,email,posicionCargo,defecto,gestor)
{
	document.getElementById('textIdAlias').value =idAlias;
	document.getElementById('textIdContacto').value = id;
	document.getElementById('textNombre1').value = nombr;
	document.getElementById('textApel11').value = apellido1;
	document.getElementById('textApel21').value = apellido2;
	document.getElementById('textTelefono1').value = telefono1;
	document.getElementById('textFax1').value = fax;
	document.getElementById('textEmail1').value = email;
	document.getElementById('textCargo1').value = posicionCargo;
	document.getElementById('textMovil1').value = movil;
	document.getElementById('chkActiv').checked = activo;

	document.getElementById('chkDefecto').checked = defecto;
	if(defecto=="true"){
		document.getElementById('chkDefecto').disabled=true;
	} else {
		document.getElementById('chkDefecto').disabled=false;
		document.getElementById('chkDefecto').checked = false;
	}

	if(gestor=="true"){
		document.getElementById('textGestor1').checked = true;
	} else {
		document.getElementById('textGestor1').checked = false;
	}




}

function borrarContacto(id, nombre)
{
	var filtro = "{\"id\" : \""+ id + "\" }";
	var jsonData = "{" +
					"\"service\" : \"SIBBACServiceContacto\", " +
					"\"action\"  : \"bajaContacto\", "+
					"\"filters\"  : "+ filtro + "}";

    if (mensajeConfirmacion("¿Desea borrar realmente el contacto " + nombre + "?") == true) {
    	$.ajax({
			type: "POST",
			dataType: "json",
			url:  "/sibbac20back/rest/service",
			data: jsonData,
			beforeSend: function( x ) {
				if(x && x.overrideMimeType) {
					x.overrideMimeType("application/json");
				}
				// CORS Related
				x.setRequestHeader("Accept", "application/json");
				x.setRequestHeader("Content-Type", "application/json");
				x.setRequestHeader("X-Requested-With", "HandMade");
				x.setRequestHeader("Access-Control-Allow-Origin", "*");
				x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
			},
			async: true,
			success: function(json) {
				$("section").load("views/contactos/contactos.html");
			},
			error: function(c) {
				console.log( "ERROR: " + c );
			}
    	});
    } else {
       // alert("You pressed Cancel!");
    }

}

function cambioActivoContacto(id, activoContacto)
{
	var activo=true;
	if (activoContacto=='S')
		{
		activo='n';
		}
	else
		{
		activo='s';
		}
	var filtro = "{\"id\" : \""+ id + "\","+
	 			  "\"activo\" : \""+ activo + "\" }";
	var jsonData = "{" +
					"\"service\" : \"SIBBACServiceContacto\", " +
					"\"action\"  : \"cambioStatus\", "+
					"\"filters\"  : "+ filtro + "}";
	 if (mensajeConfirmacion("¿Desea cambiar el estado del contacto" + id + "?") == true) {
    	$.ajax({
			type: "POST",
			dataType: "json",
			url:  "/sibbac20back/rest/service",
			data: jsonData,
			beforeSend: function( x ) {
				if(x && x.overrideMimeType) {
					x.overrideMimeType("application/json");
				}
				// CORS Related
				x.setRequestHeader("Accept", "application/json");
				x.setRequestHeader("Content-Type", "application/json");
				x.setRequestHeader("X-Requested-With", "HandMade");
				x.setRequestHeader("Access-Control-Allow-Origin", "*");
				x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
			},
			async: true,
			success: function(json) {
				$("section").load("views/contactos/contactos.html");
			},
			error: function(c) {
				console.log( "ERROR: " + c );
			}
    	});
	 }

}


function crearContacto(email, valorSeleccionado, nombre, ape1)
{

	  var nombre = document.getElementById('textNombre').value;
      var ape1 = document.getElementById('textApel1').value;
	  var ape2 = document.getElementById('textApel2').value;
	  var telefono = document.getElementById('textTelefono').value;
	  var movil = document.getElementById('textMovil').value;
	  var fax = document.getElementById('textFax').value;
	  var cargo = document.getElementById('textCargo').value;
	  var gestor = document.getElementById('textGestor').value;

	  var activo = "s";
	  var defecto ="true";
	  var descripcion = "";
	  var telefono2 = "";
	  var comentarios = "";

	  if(gestor=="on"){gestoria=true;} else {gestoria=false;}



		var filtro = "{\"cdaliass\" : \""+ valorSeleccionado + "\"," +
	      "\"descripcion\" : \""+descripcion+ "\"," +
	      "\"telefono1\" : \""+ telefono + "\"," +
	      "\"telefono2\" : \""+ telefono2 + "\"," +

	      "\"movil\" : \""+ movil + "\"," +
	      "\"fax\" : \""+ fax + "\"," +
	      "\"email\" : \""+ email + "\"," +


	      "\"comentarios\" : \""+comentarios  + "\"," +
	      "\"posicionCargo\" : \""+ cargo + "\"," +
	      "\"activo\" : \""+ activo + "\"," +

	      "\"nombre\" : \""+ nombre + "\"," +
	      "\"apellido1\" : \""+ ape1 + "\"," +
	      "\"apellido2\" : \""+ ape2 + "\"," +
	      "\"gestor\" : \""+ gestoria + "\"," +
	      "\"defecto\" : \""+ defecto + "\" }";


			var jsonData = "{" +
				"\"service\" : \"SIBBACServiceContacto\", " +
				"\"action\"  : \"altaContacto\", "+
				"\"filters\"  : "+ filtro + "}";

			console.log(jsonData);
			$.ajax({
			type: "POST",
			dataType: "json",
			url:  "/sibbac20back/rest/service",
			data: jsonData,
			beforeSend: function( x ) {
				if(x && x.overrideMimeType) {
					x.overrideMimeType("application/json");
				}
				// CORS Related
				x.setRequestHeader("Accept", "application/json");
				x.setRequestHeader("Content-Type", "application/json");
				x.setRequestHeader("X-Requested-With", "HandMade");
				x.setRequestHeader("Access-Control-Allow-Origin", "*");
				x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
			},
			async: true,
			success: function(json) {
				$("section").load("views/contactos/contactos.html");
			},
			error: function(c) {
				mensajeError( "ERROR: " + c );
			}

			});
}

function desactivarCheck(){
	document.getElementById('chkDefecto').disabled=true;
}

//búsqueda contactos
$('#busqueda').click(function(){
	//$('.resultados').show();
	var listaAliasB = $("#textAliasB").val();
	posicionAliasB = availableTags.indexOf(listaAliasB);

	if(posicionAliasB=="-1"){
		mensajeError("El alias introducido no existe");
		return false;
	}
	cargartabla(posicionAliasB);

});

function cargartabla(dato){

	valorSeleccionadoAliasB = idOcultos[dato] || 0;



			var filtro = "{\"cdaliass\" : \""+ valorSeleccionadoAliasB + "\" }";
			$.ajax({
				type: "POST",
				dataType: "json",
				url:  "/sibbac20back/rest/service",
				data: "{\"service\" : \"SIBBACServiceContacto\", \"action\"  : \"getListaContactos\", \"filters\"  : "+ filtro + "}",


				beforeSend: function( x ) {
					if(x && x.overrideMimeType) {
						x.overrideMimeType("application/json");
					}
					x.setRequestHeader("Accept", "application/json");
					x.setRequestHeader("Content-Type", "application/json");
					x.setRequestHeader("X-Requested-With", "HandMade");
					x.setRequestHeader("Access-Control-Allow-Origin", "*");
					x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
				},
				async: true,
				success: function(json) {
					 var datos = undefined;
			            if (json === undefined || json.resultados === undefined ) {
			                datos={};
			            } else {
			                datos = json.resultados;
			            }

						oTable.fnClearTable();



			            //var row;
						for ( var k in datos ) {

							item = datos[ k ];

							if ((item.activo=="S") || (item.activo=="s")) {
								imagenactivo = "activo";
								titleactivo = "Desactivar";
							} else {
								imagenactivo = "desactivo";
								titleactivo = "Activar";
							}

							if((item.defecto==true)&&((item.activo=="S") || (item.activo=="s"))){
								defEnlace1 =  "";
								defEnlace2 = "";
							} else {
								defEnlace1 = "<a class=\"btn\" onclick=\"cambioActivoContacto('"+item.id+"','"+ item.activo+"')\">";
								defEnlace2 = "</a>";
							}

							oTable.fnAddData([
			                                   item.cdAlias,
			                                   item.nombre,
			                                   item.apellido1,
			                                   item.apellido2,
			                                   item.telefono1,
			                                   item.movil,
			                                   item.fax,
			                                   defEnlace1+"<img src='img/"+ imagenactivo +".png'  title='"+ titleactivo +"'/>"+defEnlace2,
			                                   item.email,
			                                   item.posicionCargo,
			                                   "<a class=\"btn\" onclick = \"document.getElementById('formulariosModificarContacto').style.display='block';document.getElementById('fade').style.display='block';cargoInputsContacto('"+item.cdAlias+"','"+item.id+"','"+item.nombre+"','"+item.apellido1+"','"+item.apellido2+"','"+item.telefono1+"','"+item.movil+"','"+item.fax+"','"+item.activo+"','"+item.email+"','"+item.posicionCargo+"','"+item.defecto+"','"+item.gestor+"'); \"><img src='img/editp.png' title='Editar'/></a>&nbsp;<a class=\"btn\" onclick=\"borrarContacto('"+item.id+"','"+item.nombre+" "+item.apellido1+" "+item.apellido2+"');\"><img src='img/del.png'  title='Eliminar'/></a>"
			             					]);
						}

				}
			});
}
