Llamada:
{ "service":"SIBBACServicePeriodicidades",
"action": "getDiasSemana"
}

Respuesta:
{
  "request" : {
    "action" : "getDiasSemana",
    "export" : null,
    "service" : "SIBBACServicePeriodicidades",
    "filters" : null,
    "params" : null,
    "autoComplete" : null
  },
  "error" : null,
  "resultados" : {
    "semanas" : [ {
      "nombre" : "Sunday",
      "id" : "1"
    }, {
      "nombre" : "Monday",
      "id" : "2"
    }, {
      "nombre" : "Tuesday",
      "id" : "3"
    }, {
      "nombre" : "Wednesday",
      "id" : "4"
    }, {
      "nombre" : "Thursday",
      "id" : "5"
    }, {
      "nombre" : "Friday",
      "id" : "6"
    }, {
      "nombre" : "Saturday",
      "id" : "7"
    }, {
      "nombre" : "Monday to Friday",
      "id" : "98"
    }, {
      "nombre" : "All",
      "id" : "99"
    } ]
  }
}