Llamada:
{ "service":"SIBBACServicePeriodicidades",
"action": "getMeses"
}

Respuesta:
{
  "request" : {
    "action" : "getMeses",
    "export" : null,
    "service" : "SIBBACServicePeriodicidades",
    "filters" : null,
    "params" : null,
    "autoComplete" : null
  },
  "error" : null,
  "resultados" : {
    "meses" : [ {
      "nombre" : "January",
      "id" : "1"
    }, {
      "nombre" : "February",
      "id" : "2"
    }, {
      "nombre" : "March",
      "id" : "3"
    }, {
      "nombre" : "April",
      "id" : "4"
    }, {
      "nombre" : "May",
      "id" : "5"
    }, {
      "nombre" : "June",
      "id" : "6"
    }, {
      "nombre" : "July",
      "id" : "7"
    }, {
      "nombre" : "August",
      "id" : "8"
    }, {
      "nombre" : "September",
      "id" : "9"
    }, {
      "nombre" : "October",
      "id" : "10"
    }, {
      "nombre" : "November",
      "id" : "11"
    }, {
      "nombre" : "December",
      "id" : "12"
    }, {
      "nombre" : "All",
      "id" : "99"
    } ]
  }
}