PARAMETROS DE LLAMADA
{
     "service" : "SIBBACServiceConciliacionPropia",
     "action"  : "getAnotacioneseccByTipoCuenta",
     "params" : [{"fcontratacionDe" : "20150217"},
                 {"fcontratacionA" : "20150220"}]
}
FILTROS PERMITIDOS:
	cdisin, imprecio, idCuentaDeCompensacion y busqueda entre fechas (fcontratacionDe y fcontratacionA), (fliquidacionDe y fliquidacionA);
	
RESULTADO JSON:
{
  "request" : {
    "action" : "getAnotacioneseccByTipoCuenta",
    "export" : null,
    "service" : "SIBBACServiceConciliacionPropia",
    "filters" : null,
    "params" : [ {
      "fcontratacionDe" : "20150217"
    }, {
      "fcontratacionA" : "20150220"
    } ],
    "autoComplete" : null
  },
  "error" : null,
  "resultados" : {
    "result_cuenta_propia" : [ {
      "idanotacionecc" : 2531,
      "cdsentido" : "C",
      "fcontratacion" : "20150217",
      "fliquidacion" : "20150220",
      "nutitulos" : 7648,
      "imefectivo" : 1220530,
      "imprecio" : 11.70000000,
      "cdoperacion" : "M",
      "cdoperacionmkt" : "CV",
      "cdmiembromkt" : "9838",
      "cdisin" : "FOR000972282",
      "cdCuenta" : "00P",
      "cdcamara" : "XMCE",
      "diferencia" : 0,
      "codigoCuentaCompensacion" : "IBRCBSSSESM2XXX000000011I0EX0000001",
      "idCuentaDeCompensacion" : 121
    }, {
      "idanotacionecc" : 2533,
      "cdsentido" : "V",
      "fcontratacion" : "20150217",
      "fliquidacion" : "20150220",
      "nutitulos" : 6772,
      "imefectivo" : 1525558,
      "imprecio" : 11.70000000,
      "cdoperacion" : "M",
      "cdoperacionmkt" : "CV",
      "cdmiembromkt" : "9838",
      "cdisin" : "FOR000972282",
      "cdCuenta" : "00P",
      "cdcamara" : "XMCE",
      "diferencia" : 0,
      "codigoCuentaCompensacion" : "IBRCBSSSESM2XXX000000011I0EX0000001",
      "idCuentaDeCompensacion" : 121
    }, {
      "idanotacionecc" : 0,
      "cdsentido" : "-",
      "fcontratacion" : "20150217",
      "fliquidacion" : "20150220",
      "nutitulos" : 876.0,
      "imefectivo" : 305028.0,
      "imprecio" : 11.70000000,
      "cdoperacion" : "M",
      "cdoperacionmkt" : "CV",
      "cdmiembromkt" : "9838",
      "cdisin" : "FOR000972282",
      "cdCuenta" : "00P",
      "cdcamara" : "XMCE",
      "diferencia" : null,
      "codigoCuentaCompensacion" : "IBRCBSSSESM2XXX000000011I0EX0000001",
      "idCuentaDeCompensacion" : 0
    } ]
  }
}