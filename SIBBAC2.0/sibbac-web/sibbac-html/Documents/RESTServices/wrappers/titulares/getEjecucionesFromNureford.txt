getEjecucionesFromNureford

Llamada:
{
    "service": "SIBBACServiceTitulares",
    "action": "getEjecucionesFromNureford",
    "filters": {
        "nureford": "0009048396"
    }
}

Respuesta:
{
  "request" : {
    "action" : "getEjecucionesFromNureford",
    "export" : null,
    "service" : "SIBBACServiceTitulares",
    "filters" : {
      "nureford" : "0009048396"
    },
    "params" : null,
    "list" : null,
    "autoComplete" : null
  },
  "error" : null,
  "resultados" : {
    "resultados_ejecuciones" : [ {
      "nutiteje" : 1150.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 766.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 1000.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 807.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 84.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 1193.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 807.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 84.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 766.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 1150.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 1000.00000,
      "imcbmerc" : 4.12600000
    }, {
      "nutiteje" : 1193.00000,
      "imcbmerc" : 4.12600000
    } ]
  }
}