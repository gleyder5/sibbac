checkAfiErrors

Llamada:
{
    "service": "SIBBACServiceTitulares",
    "action": "checkAfiErrors"
}

Respuesta:
{
  "request" : {
    "action" : "checkAfiErrors",
    "export" : null,
    "service" : "SIBBACServiceTitulares",
    "filters" : null,
    "params" : null,
    "list" : null,
    "autoComplete" : null
  },
  "error" : null,
  "resultados" : {
    "fixedAfis" : 1
  }
}