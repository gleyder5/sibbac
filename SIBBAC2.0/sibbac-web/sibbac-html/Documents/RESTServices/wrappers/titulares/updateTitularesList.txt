updateTitularesList

Llamada:
{
    "service": "SIBBACServiceTitulares",
    "action": "updateTitularesList",
  "filters" : {
      "nbclient" : "PEPE",
      "nbclient1": "GARCIA",
      "nbclient2": "DE LA ROSA"
      },
   "params" : 
     [
       {"hasErrors" : "true" , "id" : "1009", "nbooking": "" },
       {"hasErrors" : "true" , "id" : "1010", "nbooking": "" },
       {"hasErrors" : "false" , "id" : "", "nbooking": "00000042208TRMA1", "nuorden":"00010011924ORMA1", "nucnfclt":"00000005519CAMA1", "nucnfliq":"1", "nusecuen":"1438939708945" }
     ]
}
    }
}

En filters se indican los datos que queremos poner mientras que en params indicamos a quién se los queremos poner.
Si has errors es true se buscará en afi_error y tiene que llevar id. Si es false se buscará en afi
y tiene que llevar nbooking, nufcnclt y nuorden. (Faltan en el ejemplo porque aún no está hecho)