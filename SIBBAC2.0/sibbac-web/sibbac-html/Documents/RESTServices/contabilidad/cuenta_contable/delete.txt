LLAMADA
{
  "service":"srvCuentaContable",
  "action": "delete",
  "params": [
    {"id":45}
  ]
}

RESULTADO:
{
  "request" : {
    "action" : "delete",
    "export" : null,
    "service" : "srvCuentaContable",
    "filters" : null,
    "params" : [ {
      "id" : "45"
    } ],
    "autoComplete" : null
  },
  "error" : null,
  "resultados" : {
    "resultado" : {
      "id" : 45
    }
  }
}
