Por lo que yo veo, básicamente, hay que meter estos dos scripts en las páginas de conciliación que toquen:

	<script src="js/datatables/jquery.dataTables.js"></script>
	<script src="js/datatables/tableTools.js"></script> 

declararse una variable: "oTable" y configurarla:
	oTable = $("#tblDiaria").dataTable({
		"dom": 'T<"clear">lfrtip',	
         "tableTools": {
             "sSwfPath": "/lastsibbac/js/swf/copy_csv_xls_pdf.swf"
         },
         "language": {
             "url": "i18n/Spanish.json"
         }
	});


Luego ya en el "success":

	oTable.fnClearTable();

y, por cada "row", agregar los datos a la tabla.


