package sibbac.webapp.util;


import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * This class manages SIBBAC Services.<br/>
 *
 * This class manages SIBBAC Services, classes implementing the {@link SIBBACServiceBean} interface,
 * and annotated with {@link SIBBACService}.<br/>
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@Component
public class SIBBACServicesManager {

	// ------------------------------------------ Bean static properties

	private static final Logger					LOG	= LoggerFactory.getLogger( SIBBACServicesManager.class );

	// ------------------------------------------------- Bean properties

	/**
	 * The property where all {@link SIBBACService}'s are injected.
	 */
	private Map< String, SIBBACServiceBean >	services;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public SIBBACServicesManager() {
		this.services = new Hashtable< String, SIBBACServiceBean >();
	}

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	/**
	 * Method to add a {@link SIBBACService}.
	 *
	 * @param name The bean's name.
	 * @param service An instance of {@link SIBBACServiceBean}, corresponding with the bean being processed.
	 */
	public void addService( final String name, final SIBBACServiceBean service ) {
		String prefix = "[SIBBACServicesManager::addService] ";
		LOG.info( prefix + "Adding the SIBBAC Service: [name=={}] [class=={}].", name, service.getClass().getName() );
		this.services.put( name, service );
		LOG.trace( prefix + "SIBBAC Service available commands:" );
		List< String > commands = service.getAvailableCommands();
		if ( commands != null && !commands.isEmpty() ) {
			for ( String command : commands ) {
				LOG.trace( prefix + "+ {}.{}", name, command );
			}
		} else {
			LOG.trace( prefix + "- No commands available for {}", name );
		}
	}

	/**
	 * Method to retrieve all of the registered {@link SIBBACService}'s.
	 *
	 * @return A {@link List} of <{@link String}>, with the list of all available SIBBC Services registered.
	 */
	public List< String > getServices() {
		List< String > serviceNames = new ArrayList< String >();
		for ( String key : this.services.keySet() ) {
			serviceNames.add( key );
		}
		return serviceNames;
	}

	/**
	 * Method to retrieve a registered {@link SIBBACService}.
	 *
	 * @param name The bean's name.
	 * @return An instance of {@link SIBBACServiceBean}, corresponding with the registered {@link SIBBACService}.
	 */
	public SIBBACServiceBean findService( final String name ) {
		if ( this.services.containsKey( name ) ) {
			LOG.debug( "[SIBBACServicesManager::getService] Returning the SIBBAC Service: [{}].", name );
			return this.services.get( name );
		} else {
			LOG.debug( "[SIBBACServicesManager::getService] SIBBAC Service: [{}] not found.", name );
			return null;
		}
	}

	// ------------------------------------------------ Internal Methods

	// ----------------------------------------------- Inherited Methods

}
