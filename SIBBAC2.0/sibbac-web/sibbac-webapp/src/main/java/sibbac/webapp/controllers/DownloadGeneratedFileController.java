package sibbac.webapp.controllers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.ContactoBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.dto.ContactoDTO;
import sibbac.business.wrappers.database.dto.PayLetterDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.common.utils.FormatDataUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

/**
 * @author XI316153
 */
@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DownloadGeneratedFileController {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(DownloadGeneratedFileController.class);

  /** Directorio padre de los templates de velocity. Debe estar en el classpath. */
  private static final String PARENT_PATH = "velocity";

  /** Ruta para acceder a los textos de la carta en función del idioma del alias. */
  private static final String I18N_PATH = PARENT_PATH + ".carta_pago.i18n.cartaPago";

  /** Formato de las fechas que llegan desde la pantalla. */
  private static final DateFormat VIEW_DATEFORMAT = new SimpleDateFormat("dd/MM/yyyy");

  private static final int SCALE = 2;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Contexto de la aplicación. */
  @Autowired
  private ApplicationContext applicationContext;

  /** Business object para la tabla <code>TMCT0_APUNTE_CONTABLE</code>. */
  @Autowired
  private ApunteContableBo apunteContableBo;

  /** Business object para la tabla <code>TMCT0_ALIAS</code>. */
  @Autowired
  private AliasBo aliasBo;

  @Autowired
  private ContactoBo contactoBo;

  /** Business object para la tabla <code>TMCT0ALC</code>. */
  @Autowired
  private Tmct0alcBo tmct0alcBo;

  /** Localización en función del idioma configurado para el alias. */
  private Locale locale;

  /** Fichero de textos en el idioma del alias. */
  private ResourceBundle resourceBundle;

  /** Formato de las fechas. */
  private DateFormat dateFormat;

  /** Formato de los números. */
  private NumberFormat decimalFormat;

  /** Formato de los porcentajes. */
  // private NumberFormat percentageFormat;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CARTA DE PAGO

  /**
   * Recibe las peticiones de generación de cartas de pago.
   * 
   * @param service Servicio
   * @param action
   * @param alias
   * @param comissionType
   * @param fechaDesde
   * @param fechaHasta
   * @param request
   * @param response
   * @throws Exception
   */
  @RequestMapping(value = "/velocityCartaPago", method = RequestMethod.POST)
  public void generatePdfCartaDePago(@RequestParam(value = "halias") String alias,
                                     @RequestParam(value = "hfechaDesde") String fechaDesde,
                                     @RequestParam(value = "fechaPago") String fechaPago,
                                     @RequestParam(value = "hcomissionType") String comissionType,
                                     @RequestParam(value = "hfechaHasta") String fechaHasta,
                                     @RequestParam(value = "hsearchType") String searchType,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Exception {
    LOG.debug("[generatePdfCartaDePago] Recibida peticion de generación de carta de pago ...");
    LOG.trace("[generatePdfCartaDePago] Parámetros recibidos: alias {}, commisionType {}, fechaDesde {}, fechaHasta {}"
              + "fechaPago {}", alias, comissionType, fechaDesde, fechaHasta);

    // Si los parámetros han sido informados
    if (StringUtils.isNotEmpty(alias) && StringUtils.isNotEmpty(comissionType) && StringUtils.isNotEmpty(fechaDesde)
        && StringUtils.isNotEmpty(fechaHasta)) {

      Alias tmct0alias = aliasBo.findByCdaliass(alias);
      String idioma = null;
      if (tmct0alias != null) {
        idioma = tmct0alias.getIdioma().getCodigo().toLowerCase();
        LOG.debug("[generatePdfCartaDePago] Idioma: " + idioma + "");
      } // if

      // Se carga el idioma del alias
      loadLanguaje(idioma);

      String carta_pago_vm = PARENT_PATH + File.separator + "carta_pago" + File.separator + "carta_pago.vm";
      String[] carta_pago_css = { PARENT_PATH + File.separator + "carta_pago" + File.separator + "carta_pago.css"
      };

      Calendar now = Calendar.getInstance();
      String exportFileName = resourceBundle.getString("filename") + "_" + alias + "_"
                              + String.valueOf(now.get(Calendar.YEAR))
                              + String.format("%02d", Integer.valueOf(now.get(Calendar.MONTH) + 1))
                              + String.format("%02d", Integer.valueOf(now.get(Calendar.DAY_OF_MONTH))) + ".pdf";

      // Se prepara la respuesta
      response.setContentType("application/pdf;charset=utf-8");
      response.setHeader("Content-disposition", "attachment; filename=" + exportFileName);
      response.setHeader("Expires", "0");
      response.setHeader("Cache-Control", "private, no-cache, no-store, proxy-revalidate, no-transform");
      response.setHeader("Pragma", "no-cache");

      try {
        String empresa = tmct0alias.getDescrali();
        String atencion = null;
        List<ContactoDTO> contactosAliasList = contactoBo.getLista(alias);
        for (ContactoDTO contactoDTO : contactosAliasList) {
          if (contactoDTO.getActivo().compareTo('S') == 0 && atencion == null) {
            atencion = new String(contactoDTO.getNombre() + " " + contactoDTO.getApellido1() + " "
                                  + contactoDTO.getApellido2());
          } else if (contactoDTO.getDefecto() && contactoDTO.getActivo().compareTo('S') == 0) {
            atencion = new String(contactoDTO.getNombre() + " " + contactoDTO.getApellido1() + " "
                                  + contactoDTO.getApellido2());
          } // else if
        } // for

        // Si no se ha encontrado ningún contacto activo se pone el contenido de la variable empresa
        if (atencion == null) {
          atencion = new String(empresa);
        }

        String html = mergeVelocityTemplateWithData(getData(alias, comissionType, fechaDesde, fechaHasta, fechaPago,
                                                            empresa, atencion, searchType), carta_pago_vm);
        LOG.debug("[DownloadGeneratedFileController :: generatePdfCartaDePago] Html generado: " + html);

        createPdf(html, carta_pago_css).writeTo(response.getOutputStream());
      } catch (RuntimeException e) {
        throw new Exception("Error al generar la carta de pago", e);
      }
    } else {
      LOG.error("[DownloadGeneratedFileController :: generatePdfCartaDePago] Parámetros no recibidos ...");
      throw new Exception("Parámetros no recibidos");
    } // else
  } // generatePdfCartaDePago

  /**
   * Carga el fichero con los textos para el idioma del alias.
   * 
   * @param alias
   * @return
   */
  private void loadLanguaje(String idioma) {
    LOG.error("[DownloadGeneratedFileController :: loadLanguaje] Cargando idioma  '" + idioma + "' ...");
    try {
      locale = new Locale(idioma);
      loadFormats();
    } catch (RuntimeException ex) {
      LOG.error("[DownloadGeneratedFileController :: generatePdfCartaDePago] Error cargando idioma ... ", ex);
      locale = new Locale("en");
      loadFormats();
    } // catch
  } // loadLanguaje

  private void loadFormats() {
    LOG.info("[DownloadGeneratedFileController :: generatePdfCartaDePago] Cargando idioma '{}'.",
             locale.getDisplayLanguage());
    resourceBundle = ResourceBundle.getBundle(I18N_PATH, locale, new UTF8Control());
    dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);
    decimalFormat = FormatDataUtils.builDecimalFormat(SCALE);
  }

  /**
   * Obtiene los datos a mostrar en el documento PDF.
   * 
   * @param alias Alias.
   * @param comissionType Tipo de comisión.
   * @param fechaDesde Fecha inicial para el filtrado de los datos a mostrar.
   * @param fechaHasta Fecha final para el filtrado de los datos a mostrar.
   * @return <code>java.util.Map<String, Object> - </code>Datos a mostrar en el documento PDF.
   * @throws Exception Si ocurre algún error al preparar los datos.
   */
  private Map<String, Object> getData(String alias,
                                      String comissionType,
                                      String fechaDesde,
                                      String fechaHasta,
                                      String fechaPago,
                                      String empresa,
                                      String atencion,
                                      String searchType) throws Exception {
    // Mapa con los datos a mostrar
    Map<String, Object> model = new HashMap<>();

    try {
      List<PayLetterDTO> payLetterDTOList = null;
      if (searchType != null && searchType.compareTo("true") == 0) {
        if ("0".equals(comissionType)) { // Comisión de devolución
          payLetterDTOList = tmct0alcBo.findAllDataAndBrkByFechaAndAlias(alias, VIEW_DATEFORMAT.parse(fechaDesde),
                                                                         VIEW_DATEFORMAT.parse(fechaHasta),
                                                                         TIPO_ESTADO.COMISION_DEVOLUCION.getId(),
                                                                         CONTABILIDAD.COBRADA.getId());
        } else {
          payLetterDTOList = tmct0alcBo.findAllDataAndDvoByFechaAndAlias(alias, VIEW_DATEFORMAT.parse(fechaDesde),
                                                                         VIEW_DATEFORMAT.parse(fechaHasta),
                                                                         TIPO_ESTADO.COMISION_ORDENANTE.getId(),
                                                                         CONTABILIDAD.COBRADA.getId());
        }
      } else {
        payLetterDTOList = apunteContableBo.generatePayLetter(alias, comissionType, VIEW_DATEFORMAT.parse(fechaDesde),
                                                              VIEW_DATEFORMAT.parse(fechaHasta));
      } // else

      List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

      Map<String, Object> bean;
      BigDecimal titulos = BigDecimal.ZERO;
      BigDecimal efectivo = BigDecimal.ZERO;
      BigDecimal devolucion = BigDecimal.ZERO;
      BigDecimal pcdevolucion = BigDecimal.ZERO;

      for (PayLetterDTO payLetterDTO : payLetterDTOList) {
        list.add(bean = new HashMap<>());
        bean.put("feejeliq", dateFormat.format(payLetterDTO.getFeejeliq()));
        bean.put("nbooking", payLetterDTO.getNbooking());
        bean.put("tpoper", payLetterDTO.getTpoper());
        bean.put("nbvalor", payLetterDTO.getNbvalor());
        bean.put("isin", payLetterDTO.getIsin());
        bean.put("titulos", payLetterDTO.getTitulos().setScale(0, BigDecimal.ROUND_DOWN).toString());
        titulos = titulos.add(payLetterDTO.getTitulos());
        bean.put("efectivo", decimalFormat.format(payLetterDTO.getEfectivo()));
        efectivo = efectivo.add(payLetterDTO.getEfectivo());
        bean.put("pcdevolucion", decimalFormat.format(payLetterDTO.getPcdevolucion()));
        pcdevolucion = pcdevolucion.add(payLetterDTO.getPcdevolucion());
        bean.put("devolucion", decimalFormat.format(payLetterDTO.getDevolucion()));
        devolucion = devolucion.add(payLetterDTO.getDevolucion());
      }

      SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
      int mes = Integer.parseInt(dateFormat.format(VIEW_DATEFORMAT.parse(fechaHasta)));

      DateFormatSymbols DFS = new DateFormatSymbols(locale);
      String textMes = DFS.getMonths()[mes - 1];
      String primera = textMes.substring(0, 1);
      textMes = primera.toUpperCase() + textMes.substring(1);

      model.put("fechaHasta", fechaHasta);
      model.put("fechaPago", fechaPago);
      model.put("mes", textMes);
      model.put("empresa", empresa);
      model.put("atencion", atencion);
      model.put("list", list);
      model.put("titulos", titulos.setScale(0, BigDecimal.ROUND_DOWN).toString());
      model.put("efectivo", decimalFormat.format(efectivo));
      model.put("devolucion", decimalFormat.format(devolucion));
      model.put("pcdevolucion", decimalFormat.format(pcdevolucion));
      model.put("messages", resourceBundle);
      model.put("logoPath",
                applicationContext.getResource("classpath:" + PARENT_PATH + File.separator + "img" + File.separator
                                                   + "logo.png").getFile().getAbsolutePath());

    } catch (Exception e) {
      LOG.error("[DownloadGeneratedFileController :: getData] Error recuperando la información ... ", e);
      throw e;
    } // catch

    return model;
  } // getData

  /**
   * Construye el documento html mezclando la plantilla de velocity con los datos especificados.
   * 
   * @param data datos a insertar en el documento html.
   * @param carta_pago_vm Plantilla de velocity.
   * @return <code>java.lang.String - </code>Documento html formado.
   */
  String mergeVelocityTemplateWithData(Map<String, Object> data, String carta_pago_vm) {
    // Se inicializa el motor de velocity
    VelocityEngine velocityEngine = new VelocityEngine();
    velocityEngine.setProperty("resource.loader", "class");
    velocityEngine.setProperty("class.resource.loader.class",
                               "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    velocityEngine.init();
    // Se construye el html con la mezcla de template de velocity y los datos a mostrar
    return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, carta_pago_vm, "utf-8", data);
  } // mergeVelocityTemplateWithData

  /**
   * Crea el documento PDF.
   * 
   * @param html Documento html.
   * @param css Array de hojas de estilos a aplicar al documento.
   * @return <code>java.io.ByteArrayOutputStream - </code>Documento pdf formado.
   * @throws DocumentException Si se produce algún error al crear el PdfWriter.
   * @throws IOException Si se produce algún error al crear la salida.
   */
  private ByteArrayOutputStream createPdf(String html, String... css) throws DocumentException, IOException {
    // Array con el pdf formado
    ByteArrayOutputStream salida = new ByteArrayOutputStream();

    // Docuemnto PDF
    Document document = new Document(PageSize.A4.rotate(), 15f, 15f, 15f, 15f);

    PdfWriter writer = PdfWriter.getInstance(document, salida);
    writer.setInitialLeading(12.5f);

    // Se cargan las hojas de estilos
    CSSResolver cssResolver = new StyleAttrCSSResolver();
    for (String file : css) {
      try {
        cssResolver.addCss(XMLWorkerHelper.getCSS(applicationContext.getResource("classpath:" + file).getInputStream()));
      } catch (Exception e) {
        LOG.error("[DownloadGeneratedFileController :: createPdf] Error cargando hoja de estilos '" + file + "': " + e);
      } // catch
    } // for

    HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
    htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());

    document.open();

    PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
    HtmlPipeline htmlPipeline = new HtmlPipeline(htmlContext, pdf);
    CssResolverPipeline cssResolverPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
    XMLWorker worker = new XMLWorker(cssResolverPipeline, true);
    XMLParser p = new XMLParser(worker);
    p.parse(new ByteArrayInputStream(html.getBytes()));
    document.close();
    return salida;
  } // createPdf

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INNER CLASSES

  /**
   * Carga un fichero de propiedades con formato UTF-8.
   * 
   * @author XI316153
   */
  class UTF8Control extends Control {
    public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException,
                                                                                                                      InstantiationException,
                                                                                                                      IOException {
      // The below is a copy of the default implementation.
      String bundleName = toBundleName(baseName, locale);
      String resourceName = toResourceName(bundleName, "properties");
      ResourceBundle bundle = null;
      InputStream stream = null;

      if (reload) {
        URL url = loader.getResource(resourceName);
        if (url != null) {
          URLConnection connection = url.openConnection();
          if (connection != null) {
            connection.setUseCaches(false);
            stream = connection.getInputStream();
          }
        }
      } else {
        stream = loader.getResourceAsStream(resourceName);
      } // else

      if (stream != null) {
        try {
          // Only this line is changed to make it to read properties files as UTF-8.
          bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
        } finally {
          stream.close();
        } // finally
      } // if (stream != null)
      return bundle;
    } // newBundle
  } // UTF8Control

} // DownloadGeneratedFileController
