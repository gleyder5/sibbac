package sibbac.webapp.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import sibbac.business.fixml.database.model.FixmlFile;
import sibbac.business.fixml.rest.SIBBACServiceFixml;
import sibbac.common.FormatStyle;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.StreamResult;
import sibbac.common.export.ExportBuilder;
import sibbac.fixml.ToolsFIXML;
import sibbac.language.CustomReloadableResourceBundleMessageSource;
import sibbac.webapp.beans.WebExport;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebRequestForDataTable;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.beans.WebResponseDataTable;
import sibbac.webapp.business.SIBBACDataTableServiceBean;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.util.SIBBACServicesManager;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Main SIBBAC REST Service controller.
 * 
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@EnableWebMvc
@RestController
public class ServiceController extends AbstractController {

	// ------------------------------------------ Bean static properties

	protected static final Logger LOG = LoggerFactory.getLogger(ServiceController.class);

	// ------------------------------------------------- Bean properties

	/**
	 * The Spring {@link MessageSource} for accesing applcation properties.
	 */
	@Autowired
	private MessageSource messageSource;

	/**
	 * The SIBBAC {@link SIBBACServicesManager}, for accessing SIBBAC Services.
	 */
	@Autowired
	private SIBBACServicesManager servicesManager;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public ServiceController() {
	}

	@Value("${app.version}")
	private String version ;


	// ------------------------------------------- Bean methods: Getters
	public String getVersion() {
		return version;
	}
	// ------------------------------------------- Bean methods: Setters
	public void setVersion(String version) {
		this.version = version;
	}

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	// ----------------------------------------- JSON-based REST Methods

	/**
	 * This test method returns a FIXML object.
	 *
	 * @param request
	 *            The {@link HttpServletRequest}.
	 * @return Returns, as a Http Body, a {@link ResponseEntity} with a
	 *         {@link Map} of keypairs of <{@link String}, {@link Cliente}>
	 */
	@RequestMapping(value = "/fixml", method = RequestMethod.GET)
	public void getFIXML(@RequestParam(value = "selectArchivo") String selectArchivo,
			@RequestParam(value = "textFecha") String textFecha, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		// public @ResponseBody String getFIXML(
		// @RequestParam(value="selectArchivo") String selectArchivo,
		// @RequestParam(value="textFecha")
		// String textFecha, Model model, HttpServletRequest request,
		// HttpServletResponse response) {
		String serviceName = SIBBACServiceFixml.class.getSimpleName();
		String respuesta = null;

		// Grab some info.
		String prefix = "[ServiceController::getFIXML] ";
		LOG.trace(prefix + "[serviceName: {}] [selectArchivo: {}] [textFecha: {}]", serviceName, selectArchivo,
				textFecha);
		LOG.trace(prefix + "[Content-Type: {}] [Accept: {}]", request.getContentType(), request.getHeader("Accept"));

		response.setContentType("application/json");
		response.addHeader("Content-Type", "application/json");

		// Localizamos cosas.
		SIBBACServiceFixml sibbacServiceFixml = (SIBBACServiceFixml) servicesManager.findService(serviceName);

		// Si hay algo...
		if (sibbacServiceFixml != null) {
			String tmpFolder = sibbacServiceFixml.getDirPath();
			String zipFileName = null;
			try {
				LOG.trace(prefix + "Calling [{}.findByTipoAndId({})]...", serviceName, textFecha);
				FixmlFile fixmlFile = sibbacServiceFixml.findById(textFecha);

				// Si hay algo...
				if (fixmlFile != null) {

					// Tenemos Archivo FIXML.
					zipFileName = fixmlFile.getZip();
					LOG.trace(prefix + "Localizado el ZIP: [{}::{}]...", tmpFolder, zipFileName);

					// Cogemos el archivo XML de dentro del zip.
					File zipFile = ToolsFIXML.getFIXMLFromZip(selectArchivo, tmpFolder, zipFileName);
					if (zipFile != null) {
						LOG.trace(prefix + "Tenemos el archivo ZIP: [{}]...", zipFileName);

						// Cogemos el contenido.
						respuesta = this.readFileAsStream(zipFile, zipFileName, selectArchivo + ".xml");
						LOG.trace(prefix + "Tenemos el contenido del archivo XML: [{}]...", respuesta.length());

						// Preparamos la respuesta "XML"...
						response.setContentType("application/vnd.ms-excel");
						response.addHeader("Content-Type", "application/vnd.ms-excel");
						response.addHeader("Content-Disposition",
								"inline; filename=\"" + selectArchivo + "_" + textFecha + ".xls\"");
						response.addHeader("Content-Length", "" + respuesta.getBytes().length);
						response.addHeader("Expires", "0");
						response.addHeader("Cache-Control",	"private, no-cache, no-store, proxy-revalidate, no-transform");
						response.addHeader("Pragma", "no-cache");
					} else {
						respuesta = "{ \"error\" : \"FIXMLFile [" + selectArchivo + "/" + textFecha + "] not found\" }";
						LOG.error(prefix + respuesta);
					}
				} else {
					respuesta = "{ \"error\" : \"FIXMLFile [" + selectArchivo + "/" + textFecha + "] not found\" }";
					LOG.error(prefix + respuesta);
				}
			} catch (NumberFormatException e) {
				respuesta = "{ \"error\" : \"(NumberFormatException) No es posible leer el contenido del archivo XML en el ZIP indicado [xml:"
						+ selectArchivo + "/" + textFecha + " / zip:" + zipFileName + "]\" }";
				LOG.error(prefix + respuesta);
			} catch (IOException e) {
				respuesta = "{ \"error\" : \"(IOException) No es posible leer el contenido del archivo XML en el ZIP indicado [xml:"
						+ selectArchivo + "/" + textFecha + " / zip:" + zipFileName + "]\" }";
				LOG.error(prefix + respuesta);
			}

		} else {
			respuesta = "{ \"error\" : \"Service [" + serviceName + "] not found\" }";
			LOG.error(prefix + respuesta);
		}
		try {
			response.getWriter().println(respuesta);
		} catch (IOException e) {
		    LOG.warn("[getFIXML] Error al enviar respuesta", e);
		}
	}

	/**
	 * This method returns a {@link WebResponse} with the list of all recognized
	 * and available {@link SIBBACServiceBean}'s.
	 *
	 * @param request
	 *            The {@link HttpServletRequest}.
	 * @return Returns, as a Http Body, a {@link ResponseEntity} with a
	 *         {@link WebResponse} of keypairs of <{@link String},
	 *         {@link SIBBACServiceBean}>.
	 */
	@RequestMapping(value = "/services", consumes = { "application/json",
			"application/x-www-form-urlencoded" }, produces = "application/json")
	public @ResponseBody ResponseEntity<WebResponse> servicesRequest(HttpServletRequest request) {
		LOG.trace("[ServiceController::servicesRequest] Returning the available services");
		this.logHeaders(request);
		WebResponse webResponse = new WebResponse();
		LOG.trace("[ServiceController::servicesRequest] + Retrieving the list of available services");
		List<String> services = servicesManager.getServices();
		LOG.trace("[ServiceController::servicesRequest] + Writing the results back into the response: [{}]", services);
		Map<String, List<String>> data = new Hashtable<String, List<String>>();
		data.put("services", services);
		webResponse.setResultados(data);
		LOG.trace("[ServiceController::servicesRequest] + Returning the response");
		return new ResponseEntity<WebResponse>(webResponse, HttpStatus.OK);
	}

	/**
	 * This method returns a {@link WebResponse} with the result of the
	 * execution of a given {@link SIBBACServiceBean}.
	 *
	 * @param request
	 *            The {@link HttpServletRequest}.
	 * @return Returns, as a Http Body, a {@link ResponseEntity} with a
	 *         {@link WebResponse} corresponding with the execution of the given
	 *         {@link SIBBACServiceBean}.
	 */
	@RequestMapping(value = "/service", consumes = { "application/json",
			"application/x-www-form-urlencoded","application/x-www-form-urlencoded;charset=UTF-8" }, produces = { "application/json", "application/xml",
					"application/vnd.ms-excel", "application/pdf","application/ms-excel" })
	@Transactional
	public @ResponseBody ResponseEntity<WebResponse> serviceRequest(@Valid @RequestBody WebRequest webRequest,
			HttpServletRequest request) {

		LOG.trace("[ServiceController::serviceRequest] >>> Requested a service execution...");
		this.logHeaders(request);
		WebResponse webResponse = null;
		String serviceName = webRequest.getService();

		SIBBACServiceBean sibbacService = servicesManager.findService(serviceName);
		HttpStatus httpStatus = HttpStatus.OK;
		if (sibbacService != null) {
            LOG.debug("[serviceRequest] GOT Service Bean [Service=={}]"
                    + "[Action=={}][Filters=={}][ Params=={}][Command List=={}]", serviceName,
                    webRequest.getAction(), webRequest.getFilters(), webRequest.getParams(), webRequest.getList());
			try {
				webResponse = sibbacService.process(webRequest);
				LOG.debug("[ServiceController::serviceRequest]   <<< Executed service [{}:{}].", serviceName,
						webRequest.getAction());
				
				webResponse.setRequest(webRequest);
			} catch (Exception e) {
				if (webResponse == null) {
					webResponse = new WebResponse(webRequest);
				}
				webResponse.setError("Error ejecutando [" + serviceName + "]: " + e.getMessage());
				LOG.error(MessageFormat.format("[serviceRequest] ({0}) executing service [{1}:{2}]: {3}",
						e.getClass().getName(), serviceName, webRequest.getAction(), e.getMessage()), e);
			}
		} else {
			LOG.warn("[serviceRequest]  Service Bean [Service=={}] NOT Found!", serviceName);
			webResponse = new WebResponse(webRequest);
			webResponse.setError("Service: [" + serviceName + "] not found!");
			httpStatus = HttpStatus.BAD_REQUEST;
		}
		LOG.trace("[ServiceController::serviceRequest] <<< END service execution...");

		return new ResponseEntity<WebResponse>(webResponse, httpStatus);
	}
	
	@RequestMapping(value="/service/{serviceName}/datatable", produces = "application/json;charset=UTF-8")
	public void servicePagingDatatable (@PathVariable(value="serviceName") String serviceName,
	        HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
	    final SIBBACServiceBean sibbacService;
	    final SIBBACDataTableServiceBean dataTableService;
	    final StreamResult streamResult;
	    
	    streamResult = new HttpStreamResultHelper(servletResponse);
	    try {
	        sibbacService = servicesManager.findService(serviceName);
	        if(sibbacService == null || !(sibbacService instanceof SIBBACDataTableServiceBean)) {
	            LOG.warn("Servicio no encontrado: {}", serviceName);
	            servletResponse.sendError(HttpServletResponse.SC_BAD_GATEWAY);
	            return;
	        }
	        dataTableService = SIBBACDataTableServiceBean.class.cast(sibbacService);
	        dataTableService.queryDataTable(servletRequest.getParameterMap(),  streamResult);
	    }
	    catch(RuntimeException rex) {
	        LOG.error("Error inesperado al ejecutar servicio {}", serviceName, rex);
	        servletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    }
	} 

  @RequestMapping(value="/service/{serviceName}/excel")
  public void serviceDatatableToExcel (@PathVariable(value="serviceName") String serviceName,
          HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
    final SIBBACServiceBean sibbacService;
    final SIBBACDataTableServiceBean dataTableService;
    final StreamResult streamResult;
    
    streamResult = new HttpStreamResultHelper(servletResponse);
    try {
        sibbacService = servicesManager.findService(serviceName);
        if(sibbacService == null || !(sibbacService instanceof SIBBACDataTableServiceBean)) {
            LOG.warn("Servicio no encontrado: {}", serviceName);
            servletResponse.sendError(HttpServletResponse.SC_BAD_GATEWAY);
            return;
        }
        dataTableService = SIBBACDataTableServiceBean.class.cast(sibbacService);
        dataTableService.queryExport(servletRequest.getParameterMap(), FormatStyle.EXCEL, streamResult);
    }
    catch(RuntimeException rex) {
        LOG.error("Error inesperado al ejecutar servicio {}", serviceName, rex);
        servletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
  }
  
	/**
	 * Path usado por el objeto datatable para dejar al servidor la tarea de
	 * paginación
	 * 
	 * @param request
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/service/datatable",consumes = "application/json", produces = {"application/json", "application/vnd.ms-excel", "application/pdf"})

	public @ResponseBody ResponseEntity<WebResponse> serviceRequestDatatable(
			@Valid @RequestBody WebRequestForDataTable webRequest, HttpServletRequest request) {
		LOG.trace("[ServiceController::serviceRequestDatatable] >>> Requested a service execution...");
		this.logHeaders(request);
		WebResponse webResponse = null;

		String serviceName = webRequest.getService();

		SIBBACServiceBean sibbacService = servicesManager.findService(serviceName);
		HttpStatus httpStatus = HttpStatus.OK;
		if (sibbacService != null) {
            LOG.debug("[ServiceController::serviceRequestDatatable] GOT Service Bean [Service=={}]"
                    + "[Action=={}][Filters=={}][ Params=={}][Command List=={}]", serviceName,
                    webRequest.getAction(), webRequest.getFilters(), webRequest.getParams(), webRequest.getList());
			try {
				webResponse = (WebResponseDataTable<?>) sibbacService.process(webRequest);
				LOG.trace("[ServiceController::serviceRequestDatatable]   <<< Executed service [{}:{}].", serviceName,
						webRequest.getAction());
			} catch (Exception e) {
				if (webResponse == null) {
					webResponse = new WebResponseDataTable<>(webRequest);
				}
				webResponse.setError("Error ejecutando [" + serviceName + "]: " + e.getMessage());
				LOG.warn("[ServiceController::serviceRequestDatatable]   <<< ERROR({}) executing service [{}:{}]: {}",
						e.getClass().getName(), serviceName, webRequest.getAction(), e.getMessage());
			}
			webResponse.setRequest(webRequest);
		} else {
			LOG.info("[ServiceController::serviceRequestDatatable]   Service Bean [Service=={}] NOT Found!",
					serviceName);
			webResponse = new WebResponse(webRequest);
			webResponse.setError("Service: [" + serviceName + "] not found!");
			httpStatus = HttpStatus.BAD_REQUEST;
		}
		LOG.trace("[ServiceController::serviceRequestDatatable] <<< END service execution...");
		return new ResponseEntity<WebResponse>(webResponse, httpStatus);
	}

	/**
	 * This method returns a {@link WebResponse} with the result of the
	 * execution of a given {@link SIBBACServiceBean}.
	 *
	 * @param request
	 *            The {@link HttpServletRequest}.
	 * @return Returns, as a Http Body, a {@link ResponseEntity} with a
	 *         {@link WebResponse} corresponding with the execution of the given
	 *         {@link SIBBACServiceBean}.
	 */
	@RequestMapping(value = "/serviceCommands", consumes = { "application/json",
			"application/x-www-form-urlencoded" }, produces = "application/json")
	public @ResponseBody ResponseEntity<WebResponse> serviceCommandsRequest(@Valid @RequestBody WebRequest webRequest,
			HttpServletRequest request) {
		LOG.trace("[ServiceController::serviceCommandsRequest] Requested a service command list...");
		this.logHeaders(request);
		WebResponse webResponse = new WebResponse(webRequest);
		String serviceName = webRequest.getService();

		SIBBACServiceBean sibbacService = servicesManager.findService(serviceName);
		HttpStatus httpStatus = HttpStatus.OK;
		if (sibbacService != null) {
			LOG.debug("[ServiceController::serviceCommandsRequest]   GOT Service Bean [Service=={}]", serviceName);
			Map<String, List<String>> commands = new Hashtable<String, List<String>>();
			commands.put("commands", sibbacService.getAvailableCommands());
			webResponse.setResultados(commands);
		} else {
			LOG.info("[ServiceController::serviceCommandsRequest]   Service Bean [Service=={}] NOT Found!",
					serviceName);
			webResponse.setError("Service: [" + serviceName + "] not found!");
			httpStatus = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<WebResponse>(webResponse, httpStatus);
	}

	
	@RequestMapping(value = "/server-vars", produces = "application/json", method = RequestMethod.POST)
	public @ResponseBody WebResponse getServerVars() {
		WebResponse response = new WebResponse();
		String hostName = "";
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			response.setError(e.getMessage());
		}
		hostName = hostName.replace(".", "_");
		Map<String, String> result = new HashMap<String, String>();
		result.put("server_node", hostName);
		result.put("version", version);

		response.setResultados(result);
		return response;
	}

	/**
	 * This REST method returns the i18n strings.
	 *
	 * @param request
	 *            The {@link HttpServletRequest}.
	 * @return Returns, as a Http Body, a {@link ResponseEntity} with a
	 *         {@link Map} of keypairs of <{@link String}, {@link Cliente}>
	 */
	@RequestMapping(value = "/i18n", consumes = { "application/json",
			"application/x-www-form-urlencoded" }, produces = "application/json")
	public @ResponseBody ResponseEntity<Properties> getI18N(HttpServletRequest request) {
		LOG.trace("[ServiceController::getI18N] Returning: [List<Cliente> people]");
		logHeaders(request);
		Locale locale = LocaleContextHolder.getLocale();
		String language = locale.getLanguage();
		Properties properties = ((CustomReloadableResourceBundleMessageSource) messageSource).getAllProperties(locale);
		Set<String> keySet = properties.stringPropertyNames();
		LOG.debug("[ServiceController::getI18N][locale=={}][language=={}][properties=={}][keySet=={}]",
		        locale, language, properties, keySet);
		Properties i18nProperties = new Properties();
		Object value;
		for (String key : keySet) {
			if (!key.startsWith("i18n")) {
				continue;
			}
			value = properties.get(key);
			LOG.trace("[ServiceController::getI18N]   [{}=={}]", key, value);
			i18nProperties.setProperty(key, value.toString());
		}
		return new ResponseEntity<Properties>(i18nProperties, HttpStatus.OK);
	}

	@Value("classpath*:**/*.json")
	private Resource[] jsonArray;

	// ----------------------------------------- VIEW-based REST Methods

	@Autowired
	private ObjectMapper objectMapper;

	/**
	 * This method returns a {@link WebResponse} with the result of the
	 * execution of a given {@link SIBBACServiceBean}.
	 *
	 * @param request
	 *            The {@link HttpServletRequest}.
	 * @return Returns, as a Http Body, a {@link ResponseEntity} with a
	 *         {@link WebResponse} corresponding with the execution of the given
	 *         {@link SIBBACServiceBean}.
	 */
	@RequestMapping(value = "/exportReport")
	public ModelAndView exportReport(@RequestParam(value = "webExport") String webExportString, ModelAndView mav,
			HttpServletRequest request) {
		LOG.trace("[exportReport] Requested an EXPORT of a service execution...: [{}]", webExportString);
		this.logHeaders(request);

		if (webExportString != null && webExportString.trim().length() > 0) {
			// Convertimos "String" a "WebExport".
			WebExport webExport = null;
			try {
				webExport = this.objectMapper.readValue(webExportString.getBytes(), WebExport.class);
				LOG.trace("[exportReport] GOT the WebExport...");

				// Extractamos cosas...
				WebResponse webResponse = webExport.getWebResponse();
				LOG.trace("[exportReport] GOT the WebResponse...");
				Map<Integer, String> columnas = webExport.getColumnas();
				List<String> dataSets = webExport.getDataSet();

				// Service y Command
				WebRequest webRequest = webResponse.getRequest();
				String serviceName = webRequest.getService();
				String commandName = webRequest.getAction();

				// Datos
				Map<String, ? extends Object> resultados = webResponse.getResultados();

				mav.setViewName(serviceName + "." + commandName);
				if (resultados != null) {
					LOG.trace("[exportReport] GOT the resultados [{}]...", resultados.getClass().getName());
					mav.getModel().put(ExportBuilder.MODEL_KEY_NAME, serviceName + "." + commandName);
					mav.getModel().put(ExportBuilder.MODEL_KEY_DATA, resultados);
					mav.getModel().put(ExportBuilder.MODEL_KEY_HEADER, columnas);
					mav.getModel().put(ExportBuilder.MODEL_KEY_DATASETS, dataSets);
					LOG.trace("[exportReport] MODEL generated. Calling the view...");
				} else {
					LOG.trace("[exportReport] GOT NO resultados!");
				}

			} catch (IOException e) {
				LOG.warn("[exportReport] ERROR[{}]: {}", e.getClass().getName(), e.getMessage());
				mav.setViewName("error");
				mav.getModel().put("data", null);
			}
		} else {
			LOG.warn("[exportReport] ERROR: Nothing received!");
			mav.setViewName("error");
			mav.getModel().put("data", null);
		}
		return mav;
	}

	/**
	 * This method returns a {@link WebResponse} with the result of the
	 * execution of a given {@link SIBBACServiceBean}.
	 *
	 * @param request
	 *            The {@link HttpServletRequest}.
	 * @return Returns, as a Http Body, a {@link ResponseEntity} with a
	 *         {@link WebResponse} corresponding with the execution of the given
	 *         {@link SIBBACServiceBean}.
	 */
	@RequestMapping(value = "/export")
	public ModelAndView export(@RequestParam(value = "webRequest") String webRequestString, ModelAndView mav,
			HttpServletRequest request) {
		LOG.debug("[export] Requested an EXPORT of a service execution...: [{}]", webRequestString);
		this.logHeaders(request);

		if (webRequestString != null && webRequestString.trim().length() > 0) {
			// Convertimos "String" a "WebRequest".
			WebRequest webRequest = null;
			try {
				webRequest = this.objectMapper.readValue(webRequestString.getBytes(), WebRequest.class);
				LOG.trace("[export] GOT the WebRequest...");

				// Llamamos al metodo de negocio.
				String serviceName = webRequest.getService();
				String commandName = webRequest.getAction();
				LOG.trace("[export] Calling the internal service ({}::{}) to process JSON data...", serviceName,
						commandName);
				ResponseEntity<WebResponse> response = this.serviceRequest(webRequest, request);

				// Extraemos cosas.
				WebResponse webResponse = response.getBody();
				LOG.trace("[export] GOT the WebResponse...");
				Map<String, ? extends Object> resultados = webResponse.getResultados();
				mav.setViewName(serviceName + "." + commandName);
				if (resultados != null) {
					LOG.trace("[export] GOT the resultados [{}]...", resultados.getClass().getName());
					mav.getModel().put(ExportBuilder.MODEL_KEY_NAME, serviceName + "." + commandName);
					mav.getModel().put(ExportBuilder.MODEL_KEY_DATA, resultados);
					String dtoType = "";
					if ("getTitularesExport".equals(commandName))
					{
						mav.getModel().put(ExportBuilder.MODEL_KEY_DATA, null);
						mav.getModel().put(ExportBuilder.MODEL_KEY_MODEL, resultados);
						dtoType = "TitularesDTO";
					}
					else
					if ("getDesglosesHuerfanoExport".equals(commandName))
					{
						mav.getModel().put(ExportBuilder.MODEL_KEY_DATA, null);
						mav.getModel().put(ExportBuilder.MODEL_KEY_MODEL, resultados);
						dtoType = "DesglosesHuerfanosDTO";
					}
					else
					if ("getExportarParam".equals(commandName))
					{
						mav.getModel().put(ExportBuilder.MODEL_KEY_DATA, null);
						mav.getModel().put(ExportBuilder.MODEL_KEY_MODEL, resultados);
						dtoType = "Tmct0parametrizacionDTO";
					}
					mav.getModel().put(ExportBuilder.MODEL_DTO_TYPE, dtoType);
					LOG.trace("[export] MODEL generated. Calling the view...");
				} else {
					LOG.trace("[export] GOT NO resultados!");
				}

			} catch (IOException e) {
				LOG.warn("[export] ERROR[{}]: {}", e.getClass().getName(), e.getMessage());
				mav.setViewName("error");
				mav.getModel().put("data", null);
			}
		} else {
			LOG.warn("[export] ERROR: Nothing received!");
			mav.setViewName("error");
			mav.getModel().put("data", null);
		}
		return mav;
	}

	// ------------------------------------------------ Internal Methods

	private String readFileAsStream(final File zipFile, final String zipFileName, final String file2retrieve)
			throws IOException {
		String prefix = "[ServiceController::readFileAsStream] ";
		if (zipFile == null) {
			LOG.warn(prefix + "No zip file received!");
			return null;
		}
		if (zipFileName == null) {
			LOG.warn(prefix + "No zip file name received!");
			return null;
		}
		if (file2retrieve == null || file2retrieve.trim().length() == 0) {
			LOG.warn(prefix + "No file to retrieve received!");
			return null;
		}

		LOG.trace(prefix + "Localizando el archivo XML: [{}] en el archivo ZIP: [{}/{}]", file2retrieve, zipFileName,
				zipFile.getName());
		ZipFile zip = new ZipFile(zipFileName);
		LOG.trace(prefix + "+ Abierto el ZIP: [{}]", zipFileName);
		Enumeration<? extends ZipEntry> entries = zip.entries();
		LOG.trace(prefix + "+ Recuperada la lista de archivo en el ZIP: [{}]", zipFileName);
		String name = null;
		ZipEntry file = null;
		ZipEntry found = null;
		StringBuilder sb = null;
		while (entries.hasMoreElements()) {
			file = (ZipEntry) entries.nextElement();
			name = file.getName();
			LOG.trace(prefix + "+ ZipEntry Read: [name={}] [size={}] [time=={}]", name, file.getSize(), file.getTime());

			if (name.equalsIgnoreCase(file2retrieve)) {
				found = file;
				LOG.trace(prefix + "  >>> FOUND! [name=={}]", name);

				// Leemos el contenido.
				InputStream is = null;
				try {
					is = zip.getInputStream(file);
					if (is != null) {
						BufferedReader reader = new BufferedReader(new InputStreamReader(is));
						String line = null;
						sb = new StringBuilder();
						while ((line = reader.readLine()) != null) {
							sb.append(line + "\n");
						}
					}
				} finally {
					if (is != null) {
						try {
							is.close();
						} catch (Exception ignore) {
						}
					}
					if (zip != null) {
						try {
							zip.close();
						} catch (Exception ignore) {
						}
					}
				}

				break;
			}
		}

		if (found != null && sb != null) {
			return sb.toString();
		} else {
			LOG.warn(prefix + "No zip found for name: [{} @ {}]!", file2retrieve, zipFileName);
			return null;
		}

	}

}
