package sibbac.webapp.filters;


import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.database.DBConstants;


public class WebAppFilter implements Filter, DBConstants {

	private static final Logger	LOG			= LoggerFactory.getLogger( WebAppFilter.class );

	private String				className	= null;

	public WebAppFilter() {
		this.className = this.getClass().getSimpleName();
	}

	public void init( final FilterConfig config ) throws ServletException {
		LOG.debug( "[" + this.className + ":init] Loading filter..." );

		// Get init parameter
		String testParam = config.getInitParameter( "test-param" );

		// Print the init parameter
		LOG.debug( "[" + this.className + ":init] Test Param: " + testParam );
	}

	public void doFilter( final ServletRequest req, final ServletResponse res, final FilterChain chain ) throws IOException,
			ServletException {

		HttpServletRequest request = ( HttpServletRequest ) req;

		// Get the IP address of client machine.
		String ipAddress = request.getRemoteAddr();

		// Log the IP address and current timestamp.
		LOG.trace( "[" + this.className + ":doFilter] FILTERING(" + request.getContextPath() + request.getServletPath() + "): IP "
				+ ipAddress + ", Time " + new Date().toString() );
		chain.doFilter( req, res );
	}

	public void destroy() {
		LOG.debug( "[" + this.className + ":destroy] END of filter execution" );
	}

}
