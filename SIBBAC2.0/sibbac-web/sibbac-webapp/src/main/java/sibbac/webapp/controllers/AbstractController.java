package sibbac.webapp.controllers;


import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;


/**
 * This class is an abstract class for child SIBBAC Spring {@link RestController}'s.<br/>
 * 
 * This class defines commonly used methods, available for all child SIBBAC Spring {@link RestController}'s, like methods for exception
 * handling.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public abstract class AbstractController {

	// ------------------------------------------ Bean static properties

	protected static final Logger	LOG	= LoggerFactory.getLogger( AbstractController.class );

	// ------------------------------------------------- Bean properties

	/**
	 * Method for handling exceptions.<br/>
	 *
	 * This method handles exceptions of type:
	 *
	 * <ul>
	 * <li>{@link MethodArgumentNotValidException}</li>
	 * <li>{@link MissingServletRequestParameterException}</li>
	 * <li>{@link TypeMismatchException}</li>
	 * </ul>
	 *
	 * @param e The exception to manage.
	 * @return A {@link ResponseEntity}, initially for the {@link Void} type, according with this exception.
	 */
	@ExceptionHandler( {
			MethodArgumentNotValidException.class, MissingServletRequestParameterException.class, TypeMismatchException.class
	} )
	public ResponseEntity< Void > exceptionHandler( final Throwable e ) {
		LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		return new ResponseEntity< Void >( HttpStatus.BAD_REQUEST );
	}

	// ------------------------------------------------ Internal Methods

	/**
	 * This method shows some relevant information about the header {@link HttpServletRequest}.
	 *
	 * @param request The {@link HttpServletRequest}.
	 */
	protected void logHeaders( final HttpServletRequest request ) {
		LOG.trace( "[AbstractController::logHeaders()] >>> ---- Request Headers ------------------------------" );
		Enumeration< String > headerNames = request.getHeaderNames();
		String headerName = null;
		String headerValue = null;
		while ( headerNames.hasMoreElements() ) {
			headerName = headerNames.nextElement();
			headerValue = request.getHeader( headerName );
			LOG.trace( "[AbstractController::logHeaders()] + [{}=={}]", headerName, headerValue );
		}
		LOG.trace( "[AbstractController::logHeaders()] <<< ---- Request Headers ------------------------------" );
	}

	// ----------------------------------------------- Inherited Methods

	/**
	 * Las execepciones de este tipo representan situaciones de las que el usuario debe estar informado.
	 */
	/*
	 * @ExceptionHandler({ BusinessException.class })
	 * public ResponseEntity<ErrorResponse> businessExceptionHandler(final BusinessException e) {
	 * LOG.debug("", e);
	 * return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getCode(), i18n.getText(e.getMsg(), e.getArgs())), HttpStatus.OK);
	 * }
	 */

	/*
	 * @ExceptionHandler(Throwable.class)
	 * public ResponseEntity<ErrorResponse> throwableHandler(Throwable e) {
	 * return new ResponseEntity<ErrorResponse>(processError(e), HttpStatus.INTERNAL_SERVER_ERROR);
	 * }
	 */

	/**
	 * Accede al contexto de seguridad de Spring y devuelve el usuario.
	 */
	/*
	 * protected String getUser() {
	 * final UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	 * return userDetails.getUsername();
	 * }
	 */

	/**
	 * Accede al contexto de seguridad de Spring y devuelve el comando.
	 */
	/*
	 * protected String getCommand() {
	 * final SecurityUser userDetails = (SecurityUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	 * return userDetails.getCommand();
	 * }
	 */

	/*
	 * protected ErrorResponse processError(final Throwable t) {
	 * ErrorResponse resp = null;
	 * 
	 * // Es un error producido en EntireX?
	 * if (t instanceof HostServiceExcepcion) {
	 * if (t instanceof HostTimeoutException) {
	 * HostTimeoutException te = (HostTimeoutException)t;
	 * LOG.warn("HostTimeoutException: " + te.getMessage());
	 * resp = new ErrorResponse(te.getBrokerException().getErrorCodeString(), i18n.getText("error.entirex.connection"));
	 * }
	 * else if (t.getCause() instanceof BrokerException) {
	 * final BrokerException bex = (BrokerException)t.getCause();
	 * String msg = bex.getErrorDetail();
	 * msg = msg.replace("Natural RPC Server returns: ", "");
	 * msg = msg.replaceAll("(, NE=.*)$", "");
	 * resp = new ErrorResponse(bex.getErrorCodeString(), i18n.getText("error.entirex.general", msg));
	 * LOG.error("BrokerException: " + bex.getMessage());
	 * }
	 * }
	 * 
	 * // Error generico del core:
	 * if (resp == null) {
	 * resp = new ErrorResponse("WEB0002", i18n.getText("error.general"));
	 * LOG.error("", t);
	 * }
	 * 
	 * return resp;
	 * }
	 */

}
