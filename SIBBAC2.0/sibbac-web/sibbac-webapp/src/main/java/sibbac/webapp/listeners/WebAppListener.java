package sibbac.webapp.listeners;


import java.security.Principal;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants;


// WITH Servlet 3.0 impl: import javax.servlet.ServletContainerInitializer;
@Component
@WebListener
public class WebAppListener implements DBConstants, ServletContextAttributeListener, ServletContextListener,
		ServletRequestAttributeListener, ServletRequestListener, HttpSessionActivationListener, HttpSessionAttributeListener,
		HttpSessionBindingListener, HttpSessionListener {

	private static final Logger	LOG	= LoggerFactory.getLogger( WebAppListener.class );

	// FROM: ServletContextAttributeListener
	public void attributeAdded( final ServletContextAttributeEvent e ) {
		explore( "ServletContextAttributeListener::attributeAdded", e );
	}

	public void attributeRemoved( final ServletContextAttributeEvent e ) {
		explore( "ServletContextAttributeListener::attributeRemoved", e );
	}

	public void attributeReplaced( final ServletContextAttributeEvent e ) {
		explore( "ServletContextAttributeListener::attributeReplaced", e );
	}

	// FROM: ServletContextListener
	public void contextDestroyed( final ServletContextEvent e ) {
		explore( "ServletContextListener::contextDestroyed", e );
	}

	public void contextInitialized( final ServletContextEvent e ) {
		explore( "ServletContextListener::contextInitialized", e );
	}

	// FROM: ServletRequestAttributeListener
	public void attributeAdded( final ServletRequestAttributeEvent e ) {
		explore( "ServletRequestAttributeListener::attributeAdded", e );
	}

	public void attributeRemoved( final ServletRequestAttributeEvent e ) {
		explore( "ServletRequestAttributeListener::attributeRemoved", e );
	}

	public void attributeReplaced( final ServletRequestAttributeEvent e ) {
		explore( "ServletRequestAttributeListener::attributeReplaced", e );
	}

	// FROM: ServletRequestListener
	public void requestInitialized( final ServletRequestEvent e ) {
		LOG.trace( "[ServletRequestListener::requestInitialized] " + SEPARATOR );
		explore( "ServletRequestListener::requestInitialized", e );
		ServletRequest sr = e.getServletRequest();
		HttpServletRequest hsr = ( HttpServletRequest ) sr;

		LOG.trace( "Basic Request Info:" );
		LOG.trace( "- AuthType: [{}]", hsr.getAuthType() );
		LOG.trace( "- CharacterEncoding: [{}]", hsr.getCharacterEncoding() );
		LOG.trace( "- ContentLength: [{}]", hsr.getContentLength() );
		LOG.trace( "- ContentType: [{}]", hsr.getContentType() );
		LOG.trace( "- ContextPath: [{}]", hsr.getContextPath() );
		LOG.trace( "- LocalAddr: [{}]", hsr.getLocalAddr() );
		LOG.trace( "- LocalName: [{}]", hsr.getLocalName() );
		LOG.trace( "- LocalPort: [{}]", hsr.getLocalPort() );
		LOG.trace( "- Method: [{}]", hsr.getMethod() );
		LOG.trace( "- PathInfo: [{}]", hsr.getPathInfo() );
		LOG.trace( "- PathTranslated: [{}]", hsr.getPathTranslated() );
		LOG.trace( "- Protocol: [{}]", hsr.getProtocol() );
		LOG.trace( "- QueryString: [{}]", hsr.getQueryString() );
		LOG.trace( "- RemoteAddr: [{}]", hsr.getRemoteAddr() );
		LOG.trace( "- RemoteHost: [{}]", hsr.getRemoteHost() );
		LOG.trace( "- RemotePort: [{}]", hsr.getRemotePort() );
		LOG.trace( "- RemoteUser: [{}]", hsr.getRemoteUser() );
		LOG.trace( "- RequestURI: [{}]", hsr.getRequestURI() );
		LOG.trace( "- RequestURL: [{}]", hsr.getRequestURL() );
		LOG.trace( "- RequestedSessionId: [{}]", hsr.getRequestedSessionId() );
		LOG.trace( "- Scheme: [{}]", hsr.getScheme() );
		LOG.trace( "- ServerName: [{}]", hsr.getServerName() );
		LOG.trace( "- ServerPort: [{}]", hsr.getServerPort() );
		LOG.trace( "- ServletPath: [{}]", hsr.getServletPath() );
		LOG.trace( "- isAsyncStarted: [{}]", hsr.isAsyncStarted() );
		LOG.trace( "- isAsyncSupported: [{}]", hsr.isAsyncSupported() );
		LOG.trace( "- isRequestedSessionIdFromCookie: [{}]", hsr.isRequestedSessionIdFromCookie() );
		LOG.trace( "- isRequestedSessionIdFromURL: [{}]", hsr.isRequestedSessionIdFromURL() );
		LOG.trace( "- isRequestedSessionIdValid: [{}]", hsr.isRequestedSessionIdValid() );
		LOG.trace( "- isSecure: [{}]", hsr.isSecure() );

		HttpSession session = hsr.getSession();
		Principal principal = hsr.getUserPrincipal();
		LOG.trace( "- Session ID: [{}]", ( session != null ) ? session.getId() : "<none>" );
		LOG.trace( "- Principal: [{}]", ( principal != null ) ? principal.getName() : "<none>" );

		String name = null;
		Object obj = null;
		String value = null;
		Enumeration< String > names = null;
		Enumeration< Locale > locales = null;
		Locale locale = null;

		names = hsr.getHeaderNames();
		LOG.trace( "- Headers:" );
		while ( names != null && names.hasMoreElements() ) {
			name = names.nextElement();
			value = hsr.getHeader( name );
			LOG.trace( "  > Header: [{}=={}]", name, value );
		}

		names = hsr.getAttributeNames();
		LOG.trace( "- Attributes:" );
		while ( names != null && names.hasMoreElements() ) {
			name = names.nextElement();
			obj = hsr.getAttribute( name );
			value = ( obj != null ) ? obj.toString() : "<null>";
			LOG.trace( "  > Attribute: [{}=={}]", name, value );
		}

		names = hsr.getParameterNames();
		LOG.trace( "- Parameters:" );
		while ( names != null && names.hasMoreElements() ) {
			name = names.nextElement();
			value = hsr.getParameter( name );
			LOG.trace( "  > Parameter: [{}=={} / {}]", name, value, hsr.getParameterValues( name ) );
		}
		Map< String, String[] > post = hsr.getParameterMap();
		if ( post != null ) {
			Set< String > keys = post.keySet();
			for ( String key : keys ) {
				LOG.trace( "  > Map Parameter: [{}=={}]", key, post.get( key ) );
			}
		} else {
			LOG.trace( "  No Map Parameters" );
		}

		locales = hsr.getLocales();
		LOG.trace( "- Locales:" );
		while ( locales != null && locales.hasMoreElements() ) {
			locale = locales.nextElement();
			LOG.trace( "  > Locale: [{}]", locale );
		}

		LOG.trace( "- Cookies:" );
		Cookie[] cookies = hsr.getCookies();
		if ( cookies != null && cookies.length > 0 ) {
			for ( Cookie c : hsr.getCookies() ) {
				LOG.trace( "  > Cookie: [{}=={}, {}{} ({})]", c.getName(), c.getValue(), c.getDomain(), c.getPath(), c.getMaxAge() );
			}
		}

	}

	public void requestDestroyed( final ServletRequestEvent e ) {
		explore( "ServletRequestListener::requestDestroyed", e );
		LOG.trace( "[ServletRequestListener::requestDestroyed] " + SEPARATOR );
	}

	// FROM: HttpSessionActivationListener
	public void sessionDidActivate( final HttpSessionEvent e ) {
		explore( "HttpSessionActivationListener::sessionDidActivate", e );
	}

	public void sessionWillPassivate( final HttpSessionEvent e ) {
		explore( "HttpSessionActivationListener::sessionWillPassivate", e );
	}

	// FROM: HttpSessionAttributeListener
	public void attributeAdded( final HttpSessionBindingEvent e ) {
		explore( "HttpSessionAttributeListener::attributeAdded", e );
	}

	public void attributeRemoved( final HttpSessionBindingEvent e ) {
		explore( "HttpSessionAttributeListener::attributeRemoved", e );
	}

	public void attributeReplaced( final HttpSessionBindingEvent e ) {
		explore( "HttpSessionAttributeListener::attributeReplaced", e );
	}

	// FROM: HttpSessionBindingListener
	public void valueBound( final HttpSessionBindingEvent e ) {
		explore( "HttpSessionBindingListener::valueBound", e );
	}

	public void valueUnbound( final HttpSessionBindingEvent e ) {
		explore( "HttpSessionBindingListener::valueUnbound", e );
	}

	// FROM: HttpSessionListener
	public void sessionCreated( final HttpSessionEvent e ) {
		explore( "HttpSessionListener::sessionCreated", e );
	}

	public void sessionDestroyed( final HttpSessionEvent e ) {
		explore( "HttpSessionListener::sessionDestroyed", e );
	}

	private static void explore( final String p, final HttpSessionBindingEvent e ) {
		LOG.trace( "[" + p + "] Name: " + e.getName() );
		// LOG.trace( "[" + p + "] Source: " + e.getSource() );
		LOG.trace( "[" + p + "] Value: " + e.getValue() );
		// LOG.trace( "[" + p + "] Session: " + e.getSession() );
	}

	private static void explore( final String p, final HttpSessionEvent e ) {
		LOG.trace( "[" + p + "] Session: " + e.getSession() );
		LOG.trace( "[" + p + "] Source: " + e.getSource() );
	}

	private static void explore( final String p, final ServletContextAttributeEvent e ) {
		LOG.trace( "[" + p + "] Name: " + e.getName() );
		// LOG.trace( "[" + p + "] Source: " + e.getSource() );
		LOG.trace( "[" + p + "] Value: " + e.getValue() );
		// LOG.trace( "[" + p + "] ServletContext: " + e.getServletContext() );
	}

	private static void explore( final String p, final ServletContextEvent e ) {
		ServletContext sc = e.getServletContext();
		LOG.debug( "[" + p + "] Context Path: " + sc.getContextPath() );
	}

	private static void explore( final String p, final ServletRequestAttributeEvent e ) {
		LOG.trace( "[" + p + "] Name: " + e.getName() );
		// LOG.trace( "[" + p + "] Source: " + e.getSource() );
		LOG.trace( "[" + p + "] Value: " + e.getValue() );
		// LOG.trace( "[" + p + "] ServletContext: " + e.getServletContext() );
		// LOG.trace( "[" + p + "] ServletRequest: " + e.getServletRequest() );
	}

	private static void explore( final String p, final ServletRequestEvent e ) {
		LOG.trace( "[" + p + "] Source: " + e.getSource() );
		LOG.trace( "[" + p + "] ServletContext: " + e.getServletContext() );
		LOG.trace( "[" + p + "] ServletRequest: " + e.getServletRequest() );
	}

}
