package sibbac.webapp.controllers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.bluejoe.elfinder.controller.FsException;
import cn.bluejoe.elfinder.controller.executor.CommandExecutionContext;
import cn.bluejoe.elfinder.controller.executor.CommandExecutionContextImpl;
import cn.bluejoe.elfinder.controller.executor.CommandExecutor;
import cn.bluejoe.elfinder.controller.executor.CommandExecutorFactory;
import cn.bluejoe.elfinder.service.FsServiceFactory;

@Controller
@RequestMapping(value = "fsbrowser")
public class FileSystemConnectorController {
    private static final Logger LOG = LoggerFactory
	    .getLogger(FileSystemConnectorController.class);
    @Resource(name = "commandExecutorFactory")
    private CommandExecutorFactory _commandExecutorFactory;

    @Resource(name = "fsServiceFactory")
    private FsServiceFactory _fsServiceFactory;

    @RequestMapping
    public void connector(HttpServletRequest request,
	    final HttpServletResponse response) throws IOException {
	try {
	    request = parseMultipartContent(request);
	} catch (Exception e) {
	    throw new IOException(e.getMessage());
	}

	String cmd = request.getParameter("cmd");
	CommandExecutor ce = _commandExecutorFactory.get(cmd);

	if (ce == null) {
	    // This shouldn't happen as we should have a fallback command set.
	    throw new FsException(String.format("Comando desconocido: %s", cmd));
	}

	try {
	    final HttpServletRequest finalRequest = request;
	    CommandExecutionContext cmdExecCtx = new CommandExecutionContextImpl(
		    _fsServiceFactory, finalRequest, response);
	    ce.execute(cmdExecCtx);
	} catch (Exception e) {
	    // throw new FsException( "unknown error", e );
	    LOG.error(e.getMessage(), e);
	}
    }

    private HttpServletRequest parseMultipartContent(
	    final HttpServletRequest request) throws Exception {
	if (!ServletFileUpload.isMultipartContent(request))
	    return request;

	final Map<String, String> requestParams = new HashMap<String, String>();
	List<FileItemStream> listFiles = new ArrayList<FileItemStream>();

	// Parse the request
	ServletFileUpload sfu = new ServletFileUpload();
	String characterEncoding = request.getCharacterEncoding();
	if (characterEncoding == null) {
	    characterEncoding = "UTF-8";
	}
	sfu.setHeaderEncoding(characterEncoding);
	FileItemIterator iter = null;
	try {
	    iter = sfu.getItemIterator(request);
	} catch (Exception ex) {
	    LOG.error(ex.getMessage(), ex);
	}
	if (iter != null) {
	    retrieveFileStream(requestParams, listFiles, characterEncoding,
		    iter);
	}

	request.setAttribute(FileItemStream.class.getName(), listFiles);

	// 'getParameter()' method can not be called on original request object
	// after parsing
	// so we stored the request values and provide a delegate request object

	return prepareResponseProxy(request, requestParams);
    }

    private HttpServletRequest prepareResponseProxy(
	    final HttpServletRequest request,
	    final Map<String, String> requestParams) {
	HttpServletRequest response = (HttpServletRequest) Proxy
		.newProxyInstance(this.getClass().getClassLoader(),
			new Class[] { HttpServletRequest.class },
			new InvocationHandler() {

			    @Override
			    public Object invoke(Object arg0, Method arg1,
				    Object[] arg2) throws Throwable {
				// we replace getParameter() and
				// getParameterValues()
				// methods
				if ("getParameter".equals(arg1.getName())) {
				    String paramName = (String) arg2[0];
				    return requestParams.get(paramName);
				}

				if ("getParameterValues".equals(arg1.getName())) {
				    String paramName = (String) arg2[0];

				    // normalize name 'key[]' to 'key'
				    if (paramName.endsWith("[]"))
					paramName = paramName.substring(0,
						paramName.length() - 2);

				    if (requestParams.containsKey(paramName))
					return new String[] { requestParams
						.get(paramName) };

				    // if contains key[1], key[2]...
				    int i = 0;
				    List<String> paramValues = new ArrayList<String>();
				    while (true) {
					String name2 = String.format("%s[%d]",
						paramName, i++);
					if (requestParams.containsKey(name2)) {
					    paramValues.add(requestParams
						    .get(name2));
					} else {
					    break;
					}
				    }

				    return paramValues.isEmpty() ? new String[0]
					    : paramValues
						    .toArray(new String[0]);
				}

				return arg1.invoke(request, arg2);
			    }
			});
	return response;
    }

    private void retrieveFileStream(final Map<String, String> requestParams,
	    List<FileItemStream> listFiles, String characterEncoding,
	    FileItemIterator iter) throws FileUploadException, IOException {
	while (iter.hasNext()) {
	    final FileItemStream item = iter.next();
	    String name = item.getFieldName();
	    InputStream stream = item.openStream();
	    if (item.isFormField()) {
		requestParams.put(name,
			Streams.asString(stream, characterEncoding));
	    } else {
		String fileName = item.getName();
		if (fileName != null && !"".equals(fileName.trim())) {
		    ByteArrayOutputStream os = new ByteArrayOutputStream();
		    IOUtils.copy(stream, os);
		    final byte[] bs = os.toByteArray();
		    stream.close();

		    listFiles.add((FileItemStream) Proxy.newProxyInstance(this
			    .getClass().getClassLoader(),
			    new Class[] { FileItemStream.class },
			    new InvocationHandler() {

				@Override
				public Object invoke(Object proxy,
					Method method, Object[] args)
					throws Throwable {
				    if ("openStream".equals(method.getName())) {
					return new ByteArrayInputStream(bs);
				    }

				    return method.invoke(item, args);
				}
			    }));
		}
	    }
	}
    }
}
