package sibbac.webapp.view;


import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import sibbac.common.export.ExportBuilder;
import sibbac.common.export.excel.ExcelBuilderDesglosesHuerfanos;
import sibbac.common.export.excel.ExcelBuilderTitulares;
import sibbac.common.export.pdf.PDFBuilderDesglosesHuerfanos;
import sibbac.common.export.pdf.PDFBuilderDesgloses;
import sibbac.common.export.pdf.PDFBuilderTitulares;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * Bean that resolves a view as a Adobe&trade; PDF archive.<br/>
 * 
 * The {@link AbstractPdfView} inherits ancient iText version.
 * This "View" works with the latest, supported iText version extending "AbstractITextPDFView", and implementing its own:
 * "buildPdfDocument()" method.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@Component
public class PDFView extends AbstractITextPDFView implements HttpExportableView {

	private static final Logger			LOG	= LoggerFactory.getLogger( PDFView.class );

	protected HttpExportableView.MODE	exportMode;

	// @Autowired
	// protected PDFBuilder pdfBuilder;

	/**
	 * Default constructor.
	 */
	public PDFView() {
		super( "PDFView" );
	}

	/**
	 * Constructor for a bean named "name".
	 * 
	 * @param name The name of the received Spring bean.
	 */
	public PDFView( final String name ) {
		super( name );
		this.setBeanName( name );
		this.className = this.getClass().getName();
		this.exportMode = HttpExportableView.MODE.INLINE;
	}

	/**
	 * Getter for exportMode
	 *
	 * @return the exportMode
	 */
	public String getExportMode() {
		return this.exportMode.name();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.view.HttpExportableView#setExportMode(sibbac.webapp.view.HttpExportableView.MODE)
	 */
	public void setExportMode( final MODE exportMode ) {
		this.exportMode = HttpExportableView.MODE.INLINE;
		if ( exportMode != null && exportMode.equals( MODE.ATTACHMENT ) ) {
			this.exportMode = HttpExportableView.MODE.ATTACHMENT;
		}
	}

	/**
	 * Subclasses must implement this method to build an iText PDF document, given the model.
	 *
	 * @param model the model Map
	 * @param document the PDF Document to complete
	 * @param writer the PDF Writer to use
	 * @param request in case we need locale etc. Shouldn't look at attributes.
	 * @param response in case we need to set cookies. Shouldn't write to it.
	 */
	@SuppressWarnings( "unchecked" )
	@Override
	protected void buildPdfDocument( Map< String, Object > model, Document document, PdfWriter writer, HttpServletRequest request,
			HttpServletResponse response ) throws Exception {
		String prefix = "[" + this.className + ":buildPdfDocument] ";

		// Cogemos lo que nos interesa.
		Set< String > keys = model.keySet();
		for ( String key : keys ) {
			LOG.trace( prefix + "[MODEL: key:{}==val:{}]", key, model.get( key ) );
		}

		// Cogemos los datos ("data")
		Map< String, ? extends Object > data = ( Map< String, ? extends Object > ) model.get( ExportBuilder.MODEL_KEY_DATA );
		LOG.trace( prefix + "[data null=={}]", ( data == null ) );
		String name = ( String ) model.get( ExportBuilder.MODEL_KEY_NAME );
		LOG.trace( prefix + "[name=={}]", name );
		
		final String dtoType = (model.get(ExportBuilder.MODEL_DTO_TYPE) != null) ? model.get(ExportBuilder.MODEL_DTO_TYPE).toString() : "";
		String filename = getBeanName();
		switch(dtoType){
        case "TitularesDTO":
        	data = ( Map< String, ? extends Object > ) model.get( ExportBuilder.MODEL_KEY_MODEL );
        	new PDFBuilderTitulares().generate( name, document, data );
        	filename = "controlTitulares"+new Date().getTime()+".pdf";
            break;
        case "DesglosesHuerfanosDTO":
        	data = ( Map< String, ? extends Object > ) model.get( ExportBuilder.MODEL_KEY_MODEL );
        	new PDFBuilderDesglosesHuerfanos().generate( name, document, data );
        	filename = "desglosesHuerfanos"+new Date().getTime()+".pdf";
            break;
        case "DesgloseDTO":
        	data = ( Map< String, ? extends Object > ) model.get( ExportBuilder.MODEL_KEY_MODEL );
        	new PDFBuilderDesgloses().generate( name, document, data );
        	filename = "Desgloses"+new Date().getTime()+".pdf";
            break;   
        default:
        	document = ( Document ) this.pdfBuilder.generate( name, document, data );
            break;
        }

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment:filename=" +filename );
	}

}

/*
 * 
 * 
 * 
 * Map<String,Object> getAttributesMap() Allow Map access to the static attributes of this view, with the option to add or override specific
 * entries.
 * Map<String,Object> getStaticAttributes() Return the static attributes for this view.
 * String getBeanName() Return the view's name.
 * String getContentType() Return the content type for this view.
 * String getRequestContextAttribute() Return the name of the RequestContext attribute, if any.
 * String toString()
 * boolean isExposePathVariables() Return whether to add path variables to the model or not.
 * protected ByteArrayOutputStream createTemporaryOutputStream() Create a temporary OutputStream for this view.
 * protected HttpServletRequest getRequestToExpose(HttpServletRequest originalRequest) Get the request handle to expose to
 * renderMergedOutputModel(java.util.Map<java.lang.String, java.lang.Object>, javax.servlet.http.HttpServletRequest,
 * javax.servlet.http.HttpServletResponse), i.e.
 * protected Map<String,Object> createMergedOutputModel(Map<String,?> model, HttpServletRequest request, HttpServletResponse response)
 * Creates a combined output Map (never null) that includes dynamic values and static attributes.
 * protected RequestContext createRequestContext(HttpServletRequest request, HttpServletResponse response, Map<String,Object> model) Create
 * a RequestContext to expose under the specified attribute name.
 * protected abstract void renderMergedOutputModel(Map<String,Object> model, HttpServletRequest request, HttpServletResponse response)
 * Subclasses must implement this method to actually render the view.
 * protected boolean generatesDownloadContent() Return whether this view generates download content (typically binary content like PDF or
 * Excel files).
 * protected void exposeModelAsRequestAttributes(Map<String,Object> model, HttpServletRequest request) Expose the model objects in the given
 * map as request attributes.
 * protected void prepareResponse(HttpServletRequest request, HttpServletResponse response) Prepare the given response for rendering.
 * protected void setResponseContentType(HttpServletRequest request, HttpServletResponse response) Set the content type of the response to
 * the configured content type unless the View.SELECTED_CONTENT_TYPE request attribute is present and set to a concrete media type.
 * protected void writeToResponse(HttpServletResponse response, ByteArrayOutputStream baos) Write the given temporary OutputStream to the
 * HTTP response.
 * void addStaticAttribute(String name, Object value) Add static data to this view, exposed in each view.
 * void render(Map<String,?> model, HttpServletRequest request, HttpServletResponse response) Prepares the view given the specified model,
 * merging it with static attributes and a RequestContext attribute, if necessary.
 * void setAttributes(Properties attributes) Set static attributes for this view from a java.util.Properties object.
 * void setAttributesCSV(String propString) Set static attributes as a CSV string.
 * void setAttributesMap(Map<String,?> attributes) Set static attributes for this view from a Map.
 * void setBeanName(String beanName) Set the view's name.
 * void setContentType(String contentType) Set the content type for this view.
 * void setExposeContextBeansAsAttributes(boolean exposeContextBeansAsAttributes) Set whether to make all Spring beans in the application
 * context accessible as request attributes, through lazy checking once an attribute gets accessed.
 * void setExposePathVariables(boolean exposePathVariables) Specify whether to add path variables to the model or not.
 * void setExposedContextBeanNames(String... exposedContextBeanNames) Specify the names of beans in the context which are supposed to be
 * exposed.
 * void setRequestContextAttribute(String requestContextAttribute) Set the name of the RequestContext attribute for this view.
 */
