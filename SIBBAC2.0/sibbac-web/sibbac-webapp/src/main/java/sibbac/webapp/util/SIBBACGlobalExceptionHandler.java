package sibbac.webapp.util;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * This class allows to prepare a customized web response when an exception occurs within a {@link RestController}.<br/>
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@ControllerAdvice
public class SIBBACGlobalExceptionHandler {

	// ------------------------------------------ Bean static properties

	private static final Logger	logger	= LoggerFactory.getLogger( SIBBACGlobalExceptionHandler.class );

	// ------------------------------------------------- Bean properties

	@Autowired
	private ObjectMapper		objectMapper;

	// ----------------------------------------------- Bean Constructors

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	/**
	 * For managing any {@link Exception}.
	 *
	 * @param request The {@link HttpServletRequest}.
	 * @param ex The captured {@link Exception}.
	 * @returns A {@link String} with a descriptive message.
	 */
	@ExceptionHandler( Exception.class )
	@ResponseBody
	public String handleException( final HttpServletRequest request, HttpServletResponse response, final Exception ex ) {
		logger.info( "Exception Occured({}):: URL=[{}]:: Message==[{}]", ex.getClass().getName(), request.getRequestURL(), ex.getMessage() );
		logger.trace( "> Exception: [{}]", request.getParameter( "javax.servlet.error.exception" ) );
		this.describeException( ex );
		try {
			return this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString( ex.getMessage() );
		} catch ( JsonProcessingException e ) {
			return ex.getMessage();
		}
	}

	/**
	 * For managing any {@link IOException}.
	 */
	@ExceptionHandler( IOException.class )
	@ResponseStatus( value = HttpStatus.NOT_FOUND, reason = "IOException occured" )
	public void handleIOException( HttpServletResponse response, final Exception ex ) {
		logger.error( "ERROR({}): [{}]", ex.getClass().getName(), ex.getMessage() );
		this.describeException( ex );
	}

	// ------------------------------------------------ Internal Methods

	private void describeException( final Exception e ) {
		logger.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		Throwable t = e.getCause();
		if ( t != null ) {
			logger.error( "  caused by: [{}]", t.getClass().getName(), t.getMessage() );
		}
		StackTraceElement[] st = e.getStackTrace();
		if ( st.length > 0 ) {
			for ( StackTraceElement ste : st ) {
				logger.error( "  [{}]", ste );
			}
		}
	}

	// ----------------------------------------------- Inherited Methods

}
