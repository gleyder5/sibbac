package sibbac.webapp.view;


import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import sibbac.common.export.ExportBuilder;
import sibbac.common.export.excel.ExcelBuilder;
import sibbac.common.export.excel.ExcelBuilderDesgloses;
import sibbac.common.export.excel.ExcelBuilderDesglosesHuerfanos;
import sibbac.common.export.excel.ExcelBuilderParametrizacion;
import sibbac.common.export.excel.ExcelBuilderTitulares;


/**
 * Bean that resolves a view as a Microsoft&trade; Excel archive.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 *
 */
@Component
@SuppressWarnings( "unused" )
public class ExcelView extends AbstractExcelView implements HttpExportableView {

	private static final Logger			LOG	= LoggerFactory.getLogger( ExcelView.class );
	private String						className;
	protected HttpExportableView.MODE	exportMode;

	@Autowired
	private ExcelBuilder				excelBuilder;

	public ExcelView() {
		this( "ExcelView" );
	}

	public ExcelView( final String name ) {
		super();
		this.setBeanName( name );
		this.className = this.getClass().getName();
		this.exportMode = HttpExportableView.MODE.INLINE;
	}

	/**
	 * Getter for exportMode
	 *
	 * @return the exportMode
	 */
	public String getExportMode() {
		return this.exportMode.name();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.view.HttpExportableView#setExportMode(sibbac.webapp.view.HttpExportableView.MODE)
	 */
	public void setExportMode( final MODE exportMode ) {
		this.exportMode = HttpExportableView.MODE.INLINE;
		if ( exportMode != null && exportMode.equals( MODE.ATTACHMENT ) ) {
			this.exportMode = HttpExportableView.MODE.ATTACHMENT;
		}
	}

	public void setMimeType( final String mimeType ) {
		this.setContentType( mimeType );
	}

	/**
	 * Subclasses must implement this method to create an Excel HSSFWorkbook document,
	 * given the model.
	 *
	 * @param model the model Map
	 * @param workbook the Excel workbook to complete
	 * @param request in case we need locale etc. Shouldn't look at attributes.
	 * @param response in case we need to set cookies. Shouldn't write to it.
	 */
	@SuppressWarnings( "unchecked" )
	@Override
	protected void buildExcelDocument( Map< String, Object > model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response ) throws Exception {
		String prefix = "[" + this.className + ":buildExcelDocument] ";

		// Cogemos lo que nos interesa.
		Set< String > keys = model.keySet();
		for ( String key : keys ) {
			LOG.trace( prefix + "[MODEL: key:{}==val:{}]", key, model.get( key ) );
		}

		// Cogemos los datos ("data")
		Map< String, ? extends Object > data = ( Map< String, ? extends Object > ) model.get( ExportBuilder.MODEL_KEY_DATA );
		LOG.trace( prefix + "[data null=={}]", ( data == null ) );

		// Cogemos los datos ("dataset")
		List< String > dataSets = ( List< String > ) model.get( ExportBuilder.MODEL_KEY_DATASETS );
		LOG.trace( prefix + "[dataSets null=={}] [{}]", ( dataSets == null ), dataSets );

		String name = ( String ) model.get( ExportBuilder.MODEL_KEY_NAME );
		LOG.trace( prefix + "[name=={}]", name );
		
		final String dtoType = (model.get(ExportBuilder.MODEL_DTO_TYPE) != null) ? model.get(ExportBuilder.MODEL_DTO_TYPE).toString() : "";
        switch(dtoType){
        case "TitularesDTO":
        	data = ( Map< String, ? extends Object > ) model.get( ExportBuilder.MODEL_KEY_MODEL );
        	workbook = ( HSSFWorkbook ) new ExcelBuilderTitulares().generate( name, workbook, data );
            break;
        case "DesglosesHuerfanosDTO":
        	data = ( Map< String, ? extends Object > ) model.get( ExportBuilder.MODEL_KEY_MODEL );
        	workbook = ( HSSFWorkbook ) new ExcelBuilderDesglosesHuerfanos().generate( name, workbook, data );
            break;

        case "Tmct0parametrizacionDTO":
        	data = ( Map< String, ? extends Object > ) model.get( ExportBuilder.MODEL_KEY_MODEL );
//            Calendar fecha = Calendar.getInstance();
//            String ano = String.valueOf( fecha.get(Calendar.YEAR) );
//            String mes = String.valueOf( fecha.get(Calendar.MONTH) +1 );
//            String dia = String.valueOf( fecha.get(Calendar.DAY_OF_MONTH) );
//        	String nombre = "Lista_Parametrizaciones_"+ano+"_"+mes+"_"+dia+".xls";
        	workbook = ( HSSFWorkbook ) new ExcelBuilderParametrizacion().generate( name, workbook, data );
//        	workbook.getName(nombre);
            break;           
   
        default:
                workbook = ( HSSFWorkbook ) this.excelBuilder.generate( name, workbook, data, dataSets );
                break;
        }
		
		
		/*
		 * if ( data!=null && !data.isEmpty() ) {
		 * LOG.trace( prefix + "Cogemos un elemento al azar" );
		 * // Cogemos uno al azar.
		 * Set<String> modelKeys = data.keySet();
		 * if ( modelKeys!=null ) {
		 * Object obj = data.get( modelKeys.iterator().next() );
		 * if ( obj!=null ) {
		 * Class clz = obj.getClass();
		 * if ( Map.class.isAssignableFrom( clz ) || List.class.isAssignableFrom( clz ) ) {
		 * // Es un Map o un List.
		 * workbook = ( HSSFWorkbook ) this.excelBuilder.generate( name, workbook, data );
		 * } else {
		 * // WARN: No sabemos que es.
		 * LOG.error( prefix + "Unknown data type: [{}]!", clz );
		 * }
		 * } else {
		 * LOG.warn( prefix + "No objects!" );
		 * }
		 * } else {
		 * LOG.warn( prefix + "No modelKeys!" );
		 * }
		 * } else {
		 * LOG.warn( prefix + "No data!" );
		 * }
		 */
		Cookie cookie = new Cookie( "excel_export", "true" );
		cookie.setPath( "/" );
		response.addCookie( cookie );
		response.addHeader( "Content-Disposition", this.getExportMode() + "; filename=\"" + this.getBeanName() );
	}

	/**
	 * Puede extraer propiedades definidas con: buSet.0.code
	 */
	private Object getPropertyWithSets( Object dataBean, String columnPath ) throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		Object out = dataBean;
		if ( out == null ) {
			return null;
		}

		String[] paths = columnPath.split( "\\." );
		for ( int n = 0; n < paths.length && out != null; n++ ) {
			// Elimina índices de colecciones. Ej: buSet.0.code
			if ( StringUtils.isNumeric( paths[ n ] ) ) {
				continue;
			}

			out = PropertyUtils.getProperty( out, paths[ n ] );
			if ( out instanceof Set< ? > ) {
				if ( !( ( Set< ? > ) out ).isEmpty() ) {
					out = ( ( Set< ? > ) out ).iterator().next();
				} else {
					// Colección vacía, no hay nada más que ver:
					return null;
				}
			}
		}

		return out;
	}
}
