package sibbac.webapp.util;


import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import sibbac.webapp.view.ExcelView;
import sibbac.webapp.view.PDFView;


/**
 * This class allows to choose the correct {@link ViewResolver} for some concrete file extensions.<br/>
 *
 * By default, the custom view resolved is JSON.<br/>
 *
 * Available file formats are:
 * <ul>
 * <li>PDF</li>
 * <li>XLS</li>
 * <li>XLSX</li>
 * <li>JSON</li>
 * </ul>
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@Component
public class SIBBACCustomViewResolver implements ViewResolver, Ordered {

	// ------------------------------------------ Bean static properties

	private static final Logger	LOG	= LoggerFactory.getLogger( SIBBACCustomViewResolver.class );

	// ------------------------------------------------- Bean properties

	/**
	 * The order of the Custom {@link ViewResolver} among all the available view resolvers.
	 */
	private int					order;

	/**
	 * For logging purposes.
	 */
	private String				className;

	@Autowired
	private ExcelView			excelView;

	@Autowired
	private PDFView				pdfView;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public SIBBACCustomViewResolver() {
		this.className = this.getClass().getName();
		this.order = Ordered.HIGHEST_PRECEDENCE;
	}

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see ViewResolver#resolveViewName( String, Locale )
	 */
	@Override
	public View resolveViewName( final String viewName, final Locale locale ) throws Exception {
		String prefix = "[" + this.className + ":resolveViewName] ";
		LOG.debug( prefix + "[viewName==" + viewName + "] [locale==" + locale + "]" );

		// Extension?
		int pos = viewName.lastIndexOf( "." );
		if ( pos > 0 ) {
			String extension = viewName.substring( ++pos );
			LOG.debug( prefix + "> Resolving the right view for: [{} ({})].", viewName, extension );
			if ( extension.endsWith( SupportedMediaTypes.PDF.getExtension() ) ) {
				LOG.debug( prefix + "+ Returning an PDFView instance." );
				// PDFView view = new PDFView( viewName );
				// return view;
				this.pdfView.setBeanName( viewName );
				return this.pdfView;
			} else if ( extension.endsWith( SupportedMediaTypes.XLS.getExtension() ) ) {
				LOG.debug( prefix + "+ Returning an ExcelView instance." );
				// ExcelView view = new ExcelView( viewName );
				// view.setMimeType( SupportedMediaTypes.XLS.getContentType() );
				// return view;
				this.excelView.setBeanName( viewName );
				this.excelView.setMimeType( SupportedMediaTypes.XLS.getContentType() );
				return this.excelView;
			} else if ( extension.endsWith( SupportedMediaTypes.XLSX.getExtension() ) ) {
				LOG.debug( prefix + "+ Returning an ExcelView instance." );
				// ExcelView view = new ExcelView( viewName );
				// view.setMimeType( SupportedMediaTypes.XLSX.getContentType() );
				// return view;
				this.excelView.setBeanName( viewName );
				this.excelView.setMimeType( SupportedMediaTypes.XLSX.getContentType() );
				return this.excelView;
			} else if ( extension.endsWith( SupportedMediaTypes.JSON.getExtension() ) ) {
				LOG.debug( prefix + "+ Returning an MappingJackson2JsonView instance." );
				MappingJackson2JsonView view = new MappingJackson2JsonView();
				view.setPrettyPrint( true );
				return view;
			} else {
				LOG.debug( prefix + "- Nothing applicable for [{}].", extension );
				return null;
			}
		} else {
			LOG.debug( prefix + "- No extension detected. Returning an MappingJackson2JsonView instance." );
			MappingJackson2JsonView view = new MappingJackson2JsonView();
			view.setPrettyPrint( true );
			return view;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Ordered#getOrder()
	 */
	@Override
	public int getOrder() {
		return this.order;
	}

	public void setOrder( int order ) {
		this.order = order;
	}

	// ------------------------------------------------ Internal Methods

	// ----------------------------------------------- Inherited Methods

}
