package sibbac.webapp.view;


import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.AbstractView;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import sibbac.common.export.pdf.PDFBuilder;
import sibbac.webapp.util.SupportedMediaTypes;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * The {@link AbstractPdfView} inherits ancient iText version.
 * This bean works with the latest, supported iText version.<br/>
 * 
 * This abstract class allow child classes to implement their own: "buildPdfDocument()" method.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
// @Component
@SuppressWarnings( "unused" )
public abstract class AbstractITextPDFView extends AbstractView {

	private static final Logger	LOG				= LoggerFactory.getLogger( AbstractITextPDFView.class );

	private static final String	MODEL_AND_VIEW	= "modelAndView";

	protected String			className;

	@Autowired
	protected PDFBuilder		pdfBuilder;

	public AbstractITextPDFView() {
		this( "AbstractITextPDFView" );
	}

	public AbstractITextPDFView( final String name ) {
		super();
		this.setContentType( SupportedMediaTypes.PDF.getContentType() );
		this.setBeanName( name );
		this.className = this.getClass().getName();
	}

	@Override
	protected boolean generatesDownloadContent() {
		LOG.debug( "[" + this.className + ":<generatesDownloadContent>]" );
		return true;
	}

	@Override
	protected void renderMergedOutputModel( Map< String, Object > model, HttpServletRequest request, HttpServletResponse response )
			throws Exception {
		String prefix = "[" + this.className + ":<renderMergedOutputModel>]";

		// Explore model...
		Set< String > keys = model.keySet();
		Object value = null;
		LOG.debug( prefix + "Keys:" );
		for ( String key : keys ) {
			value = model.get( key );
			LOG.debug( prefix + "+ [{}=={} ({})]", key, value, ( value != null ) ? value.getClass().getName() : "" );
		}

		// El modelo real viene en: "modelAndView".
		ModelAndView mav = ( ModelAndView ) model.get( MODEL_AND_VIEW );

		// IE workaround: write into byte array first.
		ByteArrayOutputStream baos = createTemporaryOutputStream();

		// El "parser".
		// PDFBuilder pdfBuilder = new PDFBuilder();

		// Apply preferences and build metadata.
		Document document = pdfBuilder.newDocument();
		PdfWriter writer = PdfWriter.getInstance( document, baos );
		pdfBuilder.prepareWriter( model, writer );
		pdfBuilder.buildPdfMetadata( model, document, this.getBeanName() );

		// Build PDF document.
		document.open();
		this.buildPdfDocument( model, document, writer, request, response );
		document.close();

		// Flush to HTTP response.
		writeToResponse( response, baos );
	}

	/**
	 * Subclasses must implement this method to build an iText PDF document, given the model.
	 *
	 * @param model the model Map
	 * @param document the PDF Document to complete
	 * @param writer the PDF Writer to use
	 * @param request in case we need locale etc. Shouldn't look at attributes.
	 * @param response in case we need to set cookies. Shouldn't write to it.
	 */
	protected abstract void buildPdfDocument( Map< String, Object > model, Document document, PdfWriter writer, HttpServletRequest request,
			HttpServletResponse response ) throws Exception;

}
