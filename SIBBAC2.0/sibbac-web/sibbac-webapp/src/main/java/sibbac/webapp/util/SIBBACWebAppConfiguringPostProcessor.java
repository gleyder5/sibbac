package sibbac.webapp.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.view.HttpExportableView;


/**
 * This class allows Spring beans pre- and post-processing.<br/>
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@Component
public class SIBBACWebAppConfiguringPostProcessor implements BeanPostProcessor {

	// ------------------------------------------ Bean static properties

	protected static final Logger	LOG					= LoggerFactory.getLogger( SIBBACWebAppConfiguringPostProcessor.class );

	// ------------------------------------------------- Bean properties

	/**
	 * Clave para determinar el modo de exportacion de reports.<br/>
	 * Valor: {@value}
	 */
	@Value( "${reports.export.mode:INLINE}" )
	private String					REPORT_EXPORT_MODE	= HttpExportableView.MODE.INLINE.name();

	@Value( "${reports.export.mode:INLINE}" )
	private String					TEST_REPORT_EXPORT_MODE;

	/**
	 * The Spring {@link MessageSource} for accessing application properties.
	 */
	@Autowired
	private ApplicationContext		context;

	/**
	 * The bean that manages the SIBBAC Services.
	 */
	@Autowired
	private SIBBACServicesManager	manager;

	// ----------------------------------------------- Bean Constructors

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Internal Methods

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	/**
	 * Allows to prepare a Spring bean BEFORE properties processing.
	 */
	public Object postProcessBeforeInitialization( final Object bean, final String name ) throws BeansException {
		return bean;
	}

	/**
	 * Allows to prepare a Spring bean AFTER properties processing.<br/>
	 *
	 * Within the application, after bean processing, some beans are post-configured:
	 *
	 * <ul>
	 * <li>{@link MappingJackson2HttpMessageConverter}: For pretty printing</li>
	 * <li>{@link SIBBACService}: For SIBBAC Services managment injection</li>
	 * </ul>
	 */
	public Object postProcessAfterInitialization( Object bean, String name ) throws BeansException {
		String prefix = "[SIBBACWebAppConfiguringPostProcessor::postProcessAfterInitialization] ";
		Class< ? extends Object > beanClass = bean.getClass();
		// LOG.trace( prefix + "Exploring bean: [name=={}] [class=={}]", name, bean.getClass().getName() );
		if ( bean instanceof HttpMessageConverter< ? > ) {
			if ( bean instanceof MappingJackson2HttpMessageConverter ) {
				( ( MappingJackson2HttpMessageConverter ) bean ).setPrettyPrint( true );
			}
		} else if ( beanClass.isAnnotationPresent( SIBBACService.class ) || ( bean instanceof SIBBACServiceBean ) ) {
			// LOG.trace( prefix + ">>> SIBBAC Service Bean: [name=={}] [class=={}]", name, bean.getClass().getName() );
			manager.addService( name, ( SIBBACServiceBean ) bean );
		} else if ( bean instanceof HttpExportableView ) {
			LOG.trace( prefix + "HttpExportableView: [name=={}] [class=={}] [REPORT_EXPORT_MODE==NORM:{}/TEST:{}]", name, bean.getClass()
					.getName(), REPORT_EXPORT_MODE, TEST_REPORT_EXPORT_MODE );
			// Agregamos el formato de exportación.
			( ( HttpExportableView ) bean ).setExportMode( HttpExportableView.MODE.valueOf( REPORT_EXPORT_MODE ) );
		}
		return bean;
	}

}
