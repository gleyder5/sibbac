package sibbac.webapp.business;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.CalendarIntervalTriggerImpl;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import sibbac.common.logback.LogBackLoggers;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.beans.JobExecution;
import sibbac.tasks.beans.JobInformation;
import sibbac.tasks.beans.RunningJobInformation;
import sibbac.tasks.database.Blog;
import sibbac.tasks.database.BlogBO;
import sibbac.tasks.database.Job;
import sibbac.tasks.database.JobBO;
import sibbac.tasks.database.JobPK;
import sibbac.tasks.enums.SIBBACJobType;
import sibbac.tasks.utils.AutowiringSpringBeanJobFactory;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;

/**
 * Perform business operations related with: "Jobs Management".<br/>
 * This class, implementing {@link SIBBACServiceBean}, is responsible of parsing incoming {@link WebRequest}'s and
 * process them returning a {@link WebResponse} to be sent back to the client.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@SIBBACService
public class SIBBACServiceJobsManagement implements SIBBACServiceBean {

  public static final String CMD_LISTING = "LISTING";
  public static final String CMD_START_JOB = "START_JOB";
  public static final String CMD_STOP_JOB = "STOP_JOB";
  public static final String CMD_KILL_JOB = "KILL_JOB";
  public static final String CMD_START_ALL_JOBS = "START_ALL_JOBS";
  public static final String CMD_STOP_ALL_JOBS = "STOP_ALL_JOBS";
  public static final String CMD_REPLAN_JOB = "REPLAN_JOB";
  public static final String CMD_HISTORY = "HISTORY";
  public static final String CMD_FIRE_NOW = "FIRE_NOW";
  public static final String CMD_LIST_LOGGERS = "LIST_LOGGERS";
  public static final String CMD_CHANGE_LOG = "CHANGE_LOG";
  public static final String CMD_LOG_RESET = "LOG_RESET";

  private static final Object KEY_REPLAN_JOB_NAME = "JOB_NAME";
  private static final Object KEY_REPLAN_JOB_DATE = "JOB_DATE";
  private static final Object KEY_FIRENOW_GROUP = "group";
  private static final Object KEY_FIRENOW_NAME = "name";

  public static final String FIELD_COMMAND = "command";
  public static final String FIELD_DATA = "data";
  public static final String FIELD_RUNNING = "running";
  public static final String FIELD_ERROR = "error";
  
  public static final int DELAY = 1000;

  // ------------------------------------------ Bean static properties

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceJobsManagement.class);

  // ------------------------------------------------- Bean properties

  @Autowired
  protected SchedulerFactoryBean quartzSchedulerFactoryBean;

  @Autowired
  private BlogBO blogBo;

  @Autowired
  private JobBO jobBo;
  
  protected Scheduler scheduler;

  @Autowired
  protected AutowiringSpringBeanJobFactory awsbjf;
  
  @Resource(name = "sibbacProps")
  private Properties taskProps;

  // ----------------------------------------------- Bean Constructors

  /**
   * The default constructor.
   */
  public SIBBACServiceJobsManagement() {
  }

  /*
   * (non-Javadoc)
   * @see SIBBACServiceBean#getAvailableCommands()
   */
  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_LISTING);
    commands.add(CMD_HISTORY);
    commands.add(CMD_START_JOB);
    commands.add(CMD_STOP_JOB);
    commands.add(CMD_KILL_JOB);
    commands.add(CMD_START_ALL_JOBS);
    commands.add(CMD_STOP_ALL_JOBS);
    commands.add(CMD_REPLAN_JOB);
    commands.add(CMD_FIRE_NOW);
    commands.add(CMD_LIST_LOGGERS);
    commands.add(CMD_CHANGE_LOG);
    commands.add(CMD_LOG_RESET);
    return commands;
  }
  
  private Scheduler getScheduler(){
    if(this.scheduler == null)
      return quartzSchedulerFactoryBean.getScheduler();
    return this.scheduler;
  }

  /*
   * (non-Javadoc)
   * @see SIBBACServiceBean#getFields()
   */
  @Override
  public List<String> getFields() {
    List<String> fields = new ArrayList<String>();
    return fields;
  }

  /*
   * (non-Javadoc)
   * @see SIBBACServiceBean#process(WebRequest)
   */
  @Override
  public WebResponse process(final WebRequest webRequest) {
    WebResponse webResponse = new WebResponse();
    webResponse.setRequest(webRequest);
    String command = webRequest.getAction();
    boolean skip = false;
    if (command == null || command.trim().length() == 0) {
      // Nada en particular: devolvemos La lista
    } else {
      if (command.equalsIgnoreCase(CMD_LISTING)) {
        // Nada en particular: devolvemos La lista
      } else if (command.equalsIgnoreCase(CMD_START_JOB)) {
        webResponse.setError(this.manageJobs(CMD_START_JOB, webRequest.getList()));
      } else if (command.equalsIgnoreCase(CMD_STOP_JOB)) {
        webResponse.setError(this.manageJobs(CMD_STOP_JOB, webRequest.getList()));
      } else if (command.equalsIgnoreCase(CMD_KILL_JOB)) {
        webResponse.setError(this.manageJobs(CMD_KILL_JOB, webRequest.getList()));
      } else if (command.equalsIgnoreCase(CMD_START_ALL_JOBS)) {
        webResponse.setError(this.manageAllJobs(CMD_START_ALL_JOBS));
      } else if (command.equalsIgnoreCase(CMD_STOP_ALL_JOBS)) {
        webResponse.setError(this.manageAllJobs(CMD_STOP_ALL_JOBS));
      } else if (command.equalsIgnoreCase(CMD_REPLAN_JOB)) {
        webResponse.setError(this.replanJob(webRequest.getFilters()));
      } else if (command.equalsIgnoreCase(CMD_FIRE_NOW)) {
        webResponse.setError(this.fireNow(webRequest.getFilters()));
      } else if (command.equalsIgnoreCase(CMD_HISTORY)) {
        this.getHistory(webResponse, webRequest.getFilters());
        skip = true;
      } else if (command.equalsIgnoreCase(CMD_LIST_LOGGERS)) {
        this.getLogbackLoggers(webResponse);
        skip = true;
      } else if (command.equalsIgnoreCase(CMD_CHANGE_LOG)) {
        this.changeLogging(webResponse, webRequest);
        skip = true;
      } else if (command.equalsIgnoreCase(CMD_LOG_RESET)) {
        this.resetLogLevels(webResponse);
        skip = true;
      } else {
        webResponse.setError("WARN: Comando [" + command + "] no reconocido!");
      }
    }

    // En cualquier caso, finalmente, devolvemos la lista de jobs y su estado.
    if (!skip) {
      processConfigurationUpdates();
      webResponse.setResultados(this.getJobsInformation(command));
    }
    return webResponse;
  }

  private String manageAllJobs(final String action) {
    String prefix = "[SIBBACServiceJobsManagement::manageAllJobs<" + action + ">] ";
    String respuesta = null;
    boolean parar = action.equalsIgnoreCase(CMD_STOP_ALL_JOBS);
    String actionMessage = parar ? "detener" : "iniciar";
    if (parar) {
      try {
        getScheduler().pauseAll();
        respuesta = "Jobs detenidos correctamente";
        LOG.trace(prefix + "Jobs detenidos correctamente");
      } catch (SchedulerException e) {
        respuesta = "Errores deteniendo jobs";
        LOG.warn(prefix + "ERROR({}): Error al {} todos los jobs: [{}]", e.getClass().getName(), actionMessage,
                 e.getMessage());
      }
    } else {
      try {
        getScheduler().resumeAll();
        LOG.trace(prefix + "Jobs reiniciados correctamente");
        respuesta = "Jobs reiniciados correctamente";
      } catch (SchedulerException e) {
        respuesta = "Errores reiniciando jobs";
        LOG.warn(prefix + "ERROR({}): Error al {} todos los jobs: [{}]", e.getClass().getName(), actionMessage,
                 e.getMessage());
      }
    }
    return respuesta;
  }

  private String manageJobs(final String action, final List<String> jobNames) {
    String prefix = "[SIBBACServiceJobsManagement::manageJobs<" + action + ">] ";
    String respuesta = null;
    boolean parar = action.equalsIgnoreCase(CMD_STOP_JOB);
    boolean matar = action.equalsIgnoreCase(CMD_KILL_JOB);
    String actionMessage = "iniciar";
    if (parar)
      actionMessage = "detener";
    if (matar)
      actionMessage = "interrumpir";

    if (jobNames == null || jobNames.isEmpty()) {
      // No hay "jobNames"
      respuesta = "WARN: No se han recibidos trabajos a " + actionMessage + "!";
    } else {
      // Hay "jobNames". Ejecutamos la peticion.
      List<String> managed = new ArrayList<String>();
      List<String> unmanaged = new ArrayList<String>();
      JobKey jobKey = null;
      for (String jobName : jobNames) {
        // Buscamos su "jobKey"...
        jobKey = null;
        LOG.trace(prefix + "+ Requested job: [{}]", jobName);
        try {
          jobKey = this.findJob(jobName);
          LOG.trace(prefix + "  Encontrada la \"jobKey\": [{}]", jobKey);
        } catch (SchedulerException e) {
          LOG.warn(prefix + "ERROR({}): Error localizando la \"jobKey\" para el jobname: {}: [{}]", e.getClass()
                                                                                                     .getName(),
                   jobName, e.getMessage());
        }

        if (jobKey != null) {
          if (matar) {
            try {
              getScheduler().interrupt(jobKey);
              LOG.trace(prefix + "  Job: [{}] interrumpido correctamente", jobKey);
              managed.add(jobName);
            } catch (SchedulerException e) {
              unmanaged.add(jobName);
              LOG.warn(prefix + "ERROR({}): Error al {} el job: {}: [{}]", e.getClass().getName(), actionMessage,
                       jobName, e.getMessage());
            }
          } else if (parar) {
            try {
              getScheduler().pauseJob(jobKey);
              LOG.trace(prefix + "  Job: [{}] detenido correctamente", jobKey);
              managed.add(jobName);
            } catch (SchedulerException e) {
              unmanaged.add(jobName);
              LOG.warn(prefix + "ERROR({}): Error al {} el job: {}: [{}]", e.getClass().getName(), actionMessage,
                       jobName, e.getMessage());
            }
          } else {
            try {
              getScheduler().resumeJob(jobKey);
              LOG.trace(prefix + "  Job: [{}] reiniciado correctamente", jobKey);
              managed.add(jobName);
            } catch (SchedulerException e) {
              unmanaged.add(jobName);
              LOG.warn(prefix + "ERROR({}): Error al {} el job: {}: [{}]", e.getClass().getName(), actionMessage,
                       jobName, e.getMessage());
            }
          }
        }
      }
      if (!unmanaged.isEmpty()) {
        respuesta = "Accion[" + actionMessage + "]: Ha sido imposible gestionar los procesos: " + unmanaged;
      }
    }
    return respuesta;
  }

  /**
   * This method checks if there is any new configuration in jobs.json file. If
   * there is any, reschedules those jobs affected
   */
  private void processConfigurationUpdates() {

    // retrieve information stored in backupFile
    Set<JobInformation> dynamicJobsInfo = SIBBACTask.loadJobsConfiguration();
    
    try {
      List<String> groupNames = this.getScheduler().getTriggerGroupNames();
      if (groupNames != null && !groupNames.isEmpty()) {
        Trigger trigger = null;

        JobKey jobKey = null;
        JobInformation jobInfo;
        JobPK jobPK = null;
        for (String name : groupNames) {
          Set<TriggerKey> triggerKeys = this.getScheduler().getTriggerKeys(GroupMatcher.triggerGroupEquals(name));
          if (triggerKeys != null && !triggerKeys.isEmpty()) {
            for (TriggerKey triggerKey : triggerKeys) {

              trigger = this.getScheduler().getTrigger(triggerKey);
              jobKey = trigger.getJobKey();
              jobPK = new JobPK(jobKey.getGroup(), jobKey.getName());
              jobInfo = this.awsbjf.findSavedJobInformation(jobPK);

              if (jobInfo != null) {
                for (JobInformation dynamicJobInfo : dynamicJobsInfo) {
                  if (jobInfo.equals(dynamicJobInfo)) {
                    // Nos quedamos con la configuración de fichero EXCEPTO
                    // "nextFireTime", "previousFireTime" y "startTime"
                    dynamicJobInfo.setNextFireTime(jobInfo.getNextFireTime());
                    dynamicJobInfo.setPreviousFireTime(jobInfo.getPreviousFireTime());
                    dynamicJobInfo.setStartTime(jobInfo.getStartTime());

                    // si no son iguales, replanificar con los datos que vienen
                    // en dynamicJobInfo
                    if (!jobInfo.equalsConfiguration(dynamicJobInfo)) {

                      trigger = createJobTrigger(trigger, dynamicJobInfo);
                      this.getScheduler().rescheduleJob(triggerKey, trigger);
                      this.awsbjf.updateJobInformation(jobPK, dynamicJobInfo);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    catch (SchedulerException e) {
      LOG.warn("ERROR at processConfigurationUpdates", e);
    }
  }

  private String replanJob(final Map<String, String> filters) {
    String prefix = "[SIBBACServiceJobsManagement::replanJob] ";
    if (filters != null && !filters.isEmpty()) {
      String strJobName = filters.get(KEY_REPLAN_JOB_NAME);
      String strDate = filters.get(KEY_REPLAN_JOB_DATE);
      LOG.trace(prefix + "Planning Job({}) for: [{}]", strJobName, strDate);
      return "replanned";
    } else {
      return "WARN: No data!";
    }
  }

  private String fireNow(final Map<String, String> filters) {
    String prefix = "[SIBBACServiceJobsManagement::fireNow] ";
    if (filters != null && !filters.isEmpty()) {
      String strJobGroup = filters.get(KEY_FIRENOW_GROUP);
      String strJobName = filters.get(KEY_FIRENOW_NAME);
      JobKey jobKey = new JobKey(strJobName, strJobGroup);
      LOG.trace(prefix + "Firing Job: [{}]", jobKey);
      try {
        getScheduler().triggerJob(jobKey);
        LOG.trace(prefix + "Job: [{}] fired!", jobKey);
        return "Job [" + jobKey + "] fired!";
      } catch (SchedulerException e) {
        LOG.trace(prefix + "ERROR({}) firing Job: [{}]! [{}]", e.getClass().getName(), jobKey, e.getMessage());
        return "Errors firing job [" + jobKey + "]: [" + e.getMessage() + "]!";
      }
    } else {
      return "WARN: No data!";
    }
  }

  private void resetLogLevels(WebResponse webResponse) {
    LogBackLoggers.resetDefault();
    this.getLogbackLoggers(webResponse);
  }

  private void changeLogging(WebResponse webResponse, WebRequest webRequest) {
    String prefix = "[SIBBACServiceJobsManagement::changeLogging] ";
    List<Map<String, String>> params = webRequest.getParams();
    if (params != null && !params.isEmpty()) {
      LOG.trace(prefix + "Exploring received data...");
      String loggerName = null;
      String loggerLevel = null;
      for (Map<String, String> param : params) {
        try {
          loggerName = param.get("name");
          loggerLevel = param.get("level");
          LOG.trace(prefix + "+ [{}=={}]", loggerName, loggerLevel);
          LogBackLoggers.setLoggerLevel(loggerName, loggerLevel);
        } catch (Exception e) {
          LOG.warn(prefix + "+ ERROR({}): Error leyendo los parametros: [{}]", e.getClass().getName(), e.getMessage());
        }
      }
    }
    this.getLogbackLoggers(webResponse);
  }

  private void getLogbackLoggers(WebResponse webResponse) {
    Map<String, Object> response = new Hashtable<String, Object>();
    List<ch.qos.logback.classic.Logger> loggers = LogBackLoggers.getLoggers();
    Map<String, String> logNames = new Hashtable<String, String>();
    for (ch.qos.logback.classic.Logger logger : loggers) {
      logNames.put(logger.getName(), logger.getLevel().levelStr);
    }
    response.put(WebResponse.KEY_DATA, logNames);
    webResponse.setResultados(response);
  }

  private void getHistory(WebResponse webResponse, Map<String, String> filters) {
    String prefix = "[SIBBACServiceJobsManagement::getHistory] ";
    Map<String, Object> response = new Hashtable<String, Object>();
    if (filters != null && !filters.isEmpty()) {
      Set<String> keys = filters.keySet();
      String name = null;
      JobPK jobPk = null;
      Job job = null;
      Page<Blog> executions = null;
      List<Blog> history = null;
      String processId = null;
      Long executionId = null;
      List<JobExecution> jobExecutions = new ArrayList<JobExecution>();
      JobExecution jobExecution = null;
      for (String group : keys) {
        name = filters.get(group);
        LOG.trace(prefix + "Retrieving history for Job: [{}.{}]", group, name);
        jobPk = new JobPK(group, name);
        job = this.jobBo.findById(jobPk);

        // If no job...
        if (job == null) {
          LOG.warn(prefix + "No jobs found for id: [{}]", jobPk.getJobKey());
        } else {
          // Found job in database.
          executions = this.blogBo.getAllJobExecutions(job, new PageRequest(0, 10));
          // Number: [0], NumberOfElements: [10], Size: [10], TotalElements: [182], TotalPages: [19],
          LOG.trace("> Number: [{}], NumberOfElements: [{}], Size: [{}], TotalElements: [{}], TotalPages: [{}], ",
                    executions.getNumber(), executions.getNumberOfElements(), executions.getSize(),
                    executions.getTotalElements(), executions.getTotalPages());
          // Got job and executions.
          for (Blog execution : executions) {
            jobExecution = new JobExecution();
            jobExecution.setExecution(execution);
            processId = execution.getProcessId();
            executionId = execution.getExecutionId();
            // Miramos la historia de cada ejecucion...
            history = this.blogBo.getJobHistory(job, executionId);
            if (history != null && !history.isEmpty()) {
              LOG.trace(prefix + "+ Blog entries for this execution id (j{}::e{}/p{})", jobPk.getJobKey(), executionId,
                        processId);
              for (Blog entry : history) {
                // Para cada uno de ellos, las mostramos...
                LOG.trace(prefix + "+ {}", entry);
              }
              jobExecution.setHistory(history);
            } else {
              LOG.warn(prefix + "No blog entries for this execution id (j{}::e{}/p{})", jobPk.getJobKey(), executionId,
                       processId);
            }
            jobExecutions.add(jobExecution);
          }
        }
      }
      response.put(WebResponse.KEY_DATA, jobExecutions);
      webResponse.setResultados(response);
    } else {
      webResponse.setError("WARN: No data!!!");
    }
  }

  @SuppressWarnings("unused")
  private List<String> getAllJobs() {
    String prefix = "[SIBBACServiceJobsManagement::getAllJobs] ";
    List<String> allJobs = new ArrayList<String>();
    List<String> groupNames = null;
    try {
      groupNames = getScheduler().getJobGroupNames();
    } catch (SchedulerException e) {
      LOG.warn(prefix + "ERROR({}): Error al obtener el grupo de jobs: [{}]", e.getClass().getName(), e.getMessage());
    }

    if (groupNames != null && !groupNames.isEmpty()) {
      Set<JobKey> jobKeys = null;
      for (String name : groupNames) {
        try {
          jobKeys = getScheduler().getJobKeys(GroupMatcher.jobGroupEquals(name));
        } catch (SchedulerException e) {
          LOG.warn(prefix + "ERROR({}): Error al obtener el grupo de jobKeys para el grupo [{}]: [{}]", e.getClass()
                                                                                                         .getName(),
                   name, e.getMessage());
        }
        if (jobKeys != null && !jobKeys.isEmpty()) {
          for (JobKey jobKey : jobKeys) {
            allJobs.add(jobKey.getName());
          }
        }
      }
    }

    return allJobs;
  }

  private JobKey findJob(final String jobName) throws SchedulerException {
    String prefix = "[SIBBACServiceJobsManagement::findJob] ";
    List<String> groupNames = getScheduler().getJobGroupNames();
    if (groupNames != null && !groupNames.isEmpty()) {
      JobKey jobKeyFound = null;
      String composedKey = null;
      for (String name : groupNames) {
        Set<JobKey> jobKeys = getScheduler().getJobKeys(GroupMatcher.jobGroupEquals(name));
        if (jobKeys != null && !jobKeys.isEmpty()) {
          for (JobKey jobKey : jobKeys) {
            composedKey = jobKey.getGroup() + "." + jobKey.getName();
            if (composedKey.equalsIgnoreCase(jobName)) {
              jobKeyFound = jobKey;
              break;
            }
          }
        }
      }
      return jobKeyFound;
    } else {
      LOG.warn(prefix + "Job named: [{}] not found!", jobName);
      return null;
    }
  }

  private Map<String, Object> getJobsInformation(final String command) {
    String prefix = "[SIBBACServiceJobsManagement::getJobsInformation] ";
    Map<String, Object> response = new Hashtable<String, Object>();
    Set<RunningJobInformation> runningJobsInfo = new TreeSet<RunningJobInformation>();
    Set<JobInformation> jobsInfo = new TreeSet<JobInformation>();
    response.put(FIELD_COMMAND, command);

    // Save info in a temporary bean.
    JobInformation jobInfo = null;
    RunningJobInformation runningJobInfo = null;

    LOG.trace(prefix + "Retrieving information about RUNNING jobs...");
    try {
      
      List<JobExecutionContext> currentJobs = getScheduler().getCurrentlyExecutingJobs();
      if (currentJobs != null && !currentJobs.isEmpty()) {
        Trigger trigger = null;
        TriggerState triggerState = null;
        JobDetail jobDetail = null;
        JobKey jobKey = null;
        for (JobExecutionContext jeContext : currentJobs) {
          LOG.trace(prefix + "+ Job: [{}]", jeContext);
          runningJobInfo = new RunningJobInformation();
          jobDetail = jeContext.getJobDetail();
          jobKey = jobDetail.getKey();
          trigger = jeContext.getTrigger();
          triggerState = getScheduler().getTriggerState(trigger.getKey());

          runningJobInfo.setFireInstanceId(jeContext.getFireInstanceId());
          runningJobInfo.setFireTime(jeContext.getFireTime());
          runningJobInfo.setJobDescription(jobDetail.getDescription());
          runningJobInfo.setGroup(jobKey.getGroup());
          runningJobInfo.setName(jobKey.getName());
          runningJobInfo.setJobRunTime(jeContext.getJobRunTime());
          runningJobInfo.setScheduledFireTime(jeContext.getScheduledFireTime());
          runningJobInfo.setNextFireTime(jeContext.getNextFireTime());
          runningJobInfo.setPreviousFireTime(jeContext.getPreviousFireTime());
          runningJobInfo.setRefireCount(jeContext.getRefireCount());
          runningJobInfo.setTriggerState(triggerState.name());

          runningJobsInfo.add(runningJobInfo);
        }
      } else {
        LOG.trace(prefix + "No current Jobs.");
      }

      response.put(FIELD_RUNNING, runningJobsInfo);
    } catch (SchedulerException e) {
      LOG.error(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
      response.put(FIELD_ERROR, "ERROR(" + e.getClass().getName() + "): [" + e.getMessage() + "]");
    }

    LOG.trace(prefix + "Retrieving information about DECLARED jobs...");
    try {

      LOG.trace(prefix + "Scheduler ID: [{}]", this.getScheduler().getSchedulerInstanceId());
      LOG.trace(prefix + "Scheduler Name: [{}]", this.getScheduler().getSchedulerName());

      SchedulerContext context = this.getScheduler().getContext();      
      LOG.trace(prefix + "Current Context: [{}]", context);
      
      List<String> groupNames = getScheduler().getTriggerGroupNames();
      if (groupNames != null && !groupNames.isEmpty()) {
        Trigger trigger = null;
        TriggerState triggerState = null;
        JobDetail jobDetail = null;
        JobKey jobKey = null;
        for (String name : groupNames) {
          Set<TriggerKey> triggerKeys = this.getScheduler().getTriggerKeys(GroupMatcher.triggerGroupEquals(name));
          if (triggerKeys != null && !triggerKeys.isEmpty()) {
            for (TriggerKey triggerKey : triggerKeys) {

              trigger = getScheduler().getTrigger(triggerKey);
              jobKey = trigger.getJobKey();
              jobDetail = getScheduler().getJobDetail(jobKey);
              triggerState = getScheduler().getTriggerState(triggerKey);
              
              jobInfo = this.awsbjf.findSavedJobInformation(new JobPK(jobKey.getGroup(), jobKey.getName()));              
              if (jobInfo == null) {
                jobInfo = new JobInformation();
                jobInfo.setSchedulerInstanceId(getScheduler().getSchedulerInstanceId());
                jobInfo.setSchedulerName(getScheduler().getSchedulerName());

                // http://quartz-scheduler.org/documentation/quartz-2.x/cookbook/UpdateTrigger
                /*
                 * // To change launching time. tb = trigger.getTriggerBuilder(); cisb =
                 * CalendarIntervalScheduleBuilder.calendarIntervalSchedule(); // cisb.withIn newTrigger =
                 * tb.withSchedule( cisb ).build(); this.scheduler.rescheduleJob( triggerKey, newTrigger );
                 */
                jobInfo.setCalendarName(trigger.getCalendarName());
                jobInfo.setTriggerDescription(trigger.getDescription());
                jobInfo.setEndTime(trigger.getEndTime());
                jobInfo.setFinalFireTime(trigger.getFinalFireTime());
                jobInfo.setMisfireInstruction(trigger.getMisfireInstruction());

                jobInfo.setPriority(trigger.getPriority());

                jobInfo.setGroup(jobKey.getGroup());
                jobInfo.setName(jobKey.getName());
                jobInfo.setJobDetailDescription(jobDetail.getDescription());

                // Cogemos informacion de "Calendar"... CalendarIntervalTriggerImpl
                CalendarIntervalTriggerImpl ct = null;
                CronTriggerImpl crt = null;
                SimpleTriggerImpl st = null;

                jobInfo.setJobType(SIBBACJobType.CALENDAR_INTERVAL_JOB);

                // Simple Trigger? Repeat every 'x' milliseconds.
                try {
                  st = (SimpleTriggerImpl) trigger;
                  jobInfo.setRepeatInterval(((int) st.getRepeatInterval() / 1000));
                  jobInfo.setRepeatIntervalUnit(IntervalUnit.SECOND);
                } catch (Exception e) {
                  LOG.debug("Exception while trying to set setRepeatInterval and setRepeatIntervalUnit from SimpleTriggerImpl ", e);
                }

                // Calendar Trigger? Repeat every 'x' 'units'.
                try {
                  ct = (CalendarIntervalTriggerImpl) trigger;
                  jobInfo.setRepeatInterval(ct.getRepeatInterval());
                  jobInfo.setRepeatIntervalUnit(ct.getRepeatIntervalUnit());
                } catch (Exception e) {
                  LOG.debug("Exception while trying to set setRepeatInterval and setRepeatIntervalUnit from CalendarIntervalTriggerImpl ", e);
                }

                try {
                  crt = (CronTriggerImpl) trigger;
                  jobInfo.setCronExpression(crt.getCronExpression());
                  jobInfo.setJobType(SIBBACJobType.CRON_JOB);
                } catch (Exception e) {
                  LOG.debug("Exception while trying to set setCronExpression and setJobType ", e);
                }
              }
              
              //Actualizamos los tiempos de la tarea
              jobInfo.setStartTime(trigger.getStartTime());
              jobInfo.setNextFireTime(trigger.getNextFireTime());
              jobInfo.setPreviousFireTime(trigger.getPreviousFireTime());
              jobInfo.setTriggerState(triggerState.name());

              // LOG.trace( prefix + "+ Trigger[{}]: [{}]", name, trigger );
              jobsInfo.add(jobInfo);
            }
          } else {
            LOG.trace(prefix + "No TriggerKeys.");
          }
        }
      } else {
        LOG.trace(prefix + "No Triggers.");
      }

      // Almacenamos en un "properties" el estado de los jobs.
      SIBBACTask.storeJobsInformation(jobsInfo);

      // Y devolvemos dicha informacion
      response.put(FIELD_DATA, jobsInfo);
      
    } catch (SchedulerException e) {
      LOG.error(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
      response.put(FIELD_ERROR, "ERROR(" + e.getClass().getName() + "): [" + e.getMessage() + "]");
    }

    return response;
  }
  
  /**
   * Creates a new job trigger using the information of an existing trigger and data from a JobInformation object
   * @param input
   * @param info
   * @return
   */
  private Trigger createJobTrigger(Trigger input, final JobInformation info) {
    LOG.trace("[createJobTrigger] + Job trigger...");

    if (SIBBACJobType.CRON_JOB == info.getJobType()) {
      final CronTriggerImpl cronTrigger = new CronTriggerImpl();

      cronTrigger.setName(info.getName());
      if (StringUtils.isNotBlank(info.getCronExpression())) {
        try {
          cronTrigger.setCronExpression(info.getCronExpression());
        }
        catch (ParseException e) {
          LOG.warn("[createJobTrigger] ", e);
        }
      }
      cronTrigger.setJobName(info.getName());
      cronTrigger.setJobGroup(info.getGroup());
      cronTrigger.setDescription("Trigger for " + info.getGroup() + "." + info.getName());
      cronTrigger.setMisfireInstruction(CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING);

      if (info != null) {

        LOG.trace("[createJobTrigger]   Tunning Job trigger with 'saved' job information...");
        long savedInitDate = info.getStartTime().getTime();
        Date savedEndTime = info.getEndTime();

        cronTrigger.setStartTime(new Date(savedInitDate + 1000));
        cronTrigger.setEndTime(savedEndTime);
        cronTrigger.setPreviousFireTime(info.getPreviousFireTime());
        if (StringUtils.isNotBlank(info.getCronExpression())) {
          try {
            cronTrigger.setCronExpression(info.getCronExpression());
          }
          catch (ParseException e) {
            LOG.warn("[createJobTrigger] ", e);
          }
        }
      }

      return cronTrigger;
    }
    else {
      final CalendarIntervalTriggerImpl trigger = new CalendarIntervalTriggerImpl();
      trigger.setName(info.getName());
      trigger.setJobName(info.getName());
      trigger.setJobGroup(info.getGroup());
      trigger.setDescription("Trigger for " + info.getGroup() + "." + info.getName());

      // Desde aqui hay que mirar tambien en el "info".
      trigger.setStartTime(input.getStartTime());
      trigger.setEndTime(null);
      trigger.setRepeatInterval(info.getRepeatInterval());
      trigger.setRepeatIntervalUnit(info.getRepeatIntervalUnit());
      trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_SMART_POLICY);

      if (info != null) {
        LOG.trace("[createJobTrigger]   Tunning Job trigger with 'saved' job information...");
        long savedInitDate = info.getStartTime().getTime();
        Date savedEndTime = info.getEndTime();
        int savedRepeatInterval = info.getRepeatInterval();
        IntervalUnit savedRepeatIntervalUnit = info.getRepeatIntervalUnit();
        int savedMisfireInstruction = info.getMisfireInstruction();

        trigger.setStartTime(new Date(savedInitDate + DELAY));
        trigger.setEndTime(savedEndTime);
        trigger.setRepeatInterval(savedRepeatInterval);
        trigger.setRepeatIntervalUnit(savedRepeatIntervalUnit);
        trigger.setMisfireInstruction(savedMisfireInstruction);
        trigger.setPreviousFireTime(info.getPreviousFireTime());
      }

      // Las tareas programadas para ejecutarse a una hora deben hacerlo a esa hora
      // y no ejecutarse cada vez que se reinicia el servidor por cualquier motivo.'
      Date now = new Date();
      if (input.getStartTime().after(now)) {
        // Se establece la fecha de inicio como la fecha calculada y se programa
        // la ejecución para ese momento
        trigger.setStartTime(input.getStartTime());
      }
      return trigger;
    }
  }
}
