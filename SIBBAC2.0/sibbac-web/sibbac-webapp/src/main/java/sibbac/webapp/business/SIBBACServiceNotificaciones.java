package sibbac.webapp.business;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import sibbac.business.fase0.database.bo.Tmct0MensajesBo;
import sibbac.business.fase0.database.model.Tmct0Mensajes;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;


/**
 * Perform business operations related with: "Notificaciones".<br/>
 * This class, implementing {@link SIBBACServiceBean}, is responsible of parsing incoming {@link WebRequest}'s and process them
 * returning a {@link WebResponse} to be sent back to the client.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */

@SIBBACService
public class SIBBACServiceNotificaciones implements SIBBACServiceBean {

	// ------------------------------------------ Bean static properties

	private static final Logger	LOG			= LoggerFactory.getLogger( SIBBACServiceNotificaciones.class );

	public static final String	KEY_COMMAND	= "command";
	public static final String	CMD_LISTING	= "LIST";

	// ------------------------------------------------- Bean properties

	@Autowired
	private Tmct0MensajesBo		boMensajes;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public SIBBACServiceNotificaciones() {
	}

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_LISTING );
		return commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		List< String > fields = new ArrayList< String >();
		fields.add( "NOTHING" );
		return fields;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#process(WebRequest)
	 */
	@Override
	public WebResponse process( final WebRequest webRequest ) {
		LOG.debug( "[SIBBACServiceNotificaciones::process] Processing a web request..." );
		WebResponse webResponse = new WebResponse();
		Map< String, List< Mensaje > > mensajes = this.findAll();
		webResponse.setResultados( mensajes );
		if ( mensajes == null ) {
			webResponse.setError( "No hay mensajes" );
		}
		return webResponse;
	}

	/*
	 * private Map< String, ? > findAll() {
	 * 
	 * Page< Tmct0Mensajes > messages = boMensajes.getLast10Messages();
	 * LOG.trace( "GOT [{}]", messages );
	 * LOG.trace("# Mensajes: {}/{}", messages.getTotalElements(), messages.getTotalPages() );
	 * 
	 * if ( messages!=null && messages.getTotalElements()>0 ) {
	 * Map< String, Map< Date, String > > mensajes = new Hashtable< String, Map< Date, String > >();
	 * Map< Date, String > msgs = new Hashtable< Date, String >();
	 * for ( Tmct0Mensajes m : messages ) {
	 * msgs.put( m.getHoraRecepcion(), m.getFullText() );
	 * }
	 * mensajes.put( "mensajes", msgs );
	 * return mensajes;
	 * } else {
	 * Map< String, String > mensajes = new Hashtable< String, String >();
	 * mensajes.put( "mensajes", "No hay mensajes" );
	 * return mensajes;
	 * }
	 * 
	 * }
	 */

	private Map< String, List< Mensaje > > findAll() {
		Page< Tmct0Mensajes > messages = boMensajes.getLast10Messages();
		LOG.trace( "GOT [{}]", messages );
		LOG.trace( "# Mensajes: {}/{}", messages.getTotalElements(), messages.getTotalPages() );

		Map< String, List< Mensaje > > msgs = new Hashtable< String, List< Mensaje > >();
		List< Mensaje > mensajes = new ArrayList< Mensaje >();
		if ( messages != null && messages.getTotalElements() > 0 ) {
			for ( Tmct0Mensajes m : messages ) {
				mensajes.add( new Mensaje( m.getIdMensaje(), m.getHoraRecepcion(), m.getFullText() ) );
			}
			msgs.put( WebResponse.KEY_DATA, mensajes );
		} else {
			msgs = null;
		}
		return msgs;
	}

	public class Mensaje implements Serializable {

		/**
		 * 
		 */
		private static final long	serialVersionUID	= 4070081146758841287L;
		private Long				id;
		private Date				hora;
		private String				texto;

		public Mensaje( final Long id, final Date hora, final String texto ) {
			this.id = id;
			this.hora = hora;
			this.texto = texto;
		}

		public Long getId() {
			return this.id;
		}

		public Date getHora() {
			return this.hora;
		}

		public String getTexto() {
			return this.texto;
		}

		public void setId( final Long id ) {
			this.id = id;
		}

		public void setHora( final Date hora ) {
			this.hora = hora;
		}

		public void setTexto( final String texto ) {
			this.texto = texto;
		}
	}

}
