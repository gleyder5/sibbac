<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/css/sibbac20.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-2.1.3.js">
		</script>
		<script type="text/javascript">
<!--

var CONTEXT = "<%=request.getContextPath()%>";
var BASE_PATH = CONTEXT + "/rest/people";

// View: http://stackoverflow.com/questions/3499597/javascript-jquery-to-download-file-via-post-with-json-data/8394118#8394118
$(document).ready(function(){
	//attach a jQuery live event to the button
	$('#getdata-button').click(function(){
		console.log("Click!");
		var data = {
			"username": "pacopepe"
		};
		data = $(this).serialize() + "&" + $.param(data);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: BASE_PATH,
			data: data,
			async: false,
			beforeSend: function(x) {
				if(x && x.overrideMimeType) {
					x.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			success: function(data) {
				var html = "";
				for ( var person of data ) {
					html += "<br/>Person:";
					for ( var key in person ) {
						html += " " + key + ": " + person[key];
					}
				}
				$("#showdata").html( html );
			}
		});
	})
})

//-->
		</script>
	</head>
	<body>

		<header>
			<h2>Hello World!</h2>
			<nav>
				<a rel="external" href="#">Home</a>
				<a rel="external" href="#">About us</a>
				<a rel="external" href="#">Contacts</a>
			</nav>
		</header>

		<div id="core" class="clearfix">  
			<section id="left">  
				<a href="<%=request.getContextPath()%>/rest/people" target="_new">people</a><br/>
				<a href="<%=request.getContextPath()%>/rest/people.jsp" target="_new">people.jsp</a><br/>
				<a href="<%=request.getContextPath()%>/rest/people.html" target="_new">people.html</a><br/>
				<a href="<%=request.getContextPath()%>/rest/people.json" target="_new">people.json</a><br/>
				<a href="<%=request.getContextPath()%>/rest/people.pdf">people.pdf</a><br/>
				<a href="<%=request.getContextPath()%>/rest/people.xls">people.xls</a><br/>
				<a href="<%=request.getContextPath()%>/rest/people.xlsx">people.xlsx</a><br/>
			</section>  
			  
			<section id="right">  
			<a href="#" id="getdata-button">Get JSON Data</a>
			<div id="showdata">pacopepe</div>
			</section>  
		</div>  

		<footer>
			&copy; Santander&trade; 2015
		</footer>

	</body>
</html>
