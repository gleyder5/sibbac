package gamma.web;

import static org.junit.Assert.assertNotNull;
import gamma.exports.security.domain.User;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Pruebas de autenticación y autorización en una app con seguridad activada (con login previo).
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthControllerTest extends BaseControllerTest {
    @Ignore 
	@Test
	public void wrongLogin() {
		login("xx", "xx", HttpStatus.UNAUTHORIZED);
	}
	
    @Ignore 
	@Test
	public void wrongLogin2() {
		login("xx", null, HttpStatus.BAD_REQUEST);
	}
	
    @Ignore 
    @Test
    public void unauthorizedMethod() {
        get(Path.class, "/admin/users/1", MediaType.APPLICATION_JSON, HttpStatus.UNAUTHORIZED);
    }

    @Ignore 
	@Test
	public void unathorizedMethod2() {
        get(Path.class, "/admin/users/1?token=xxxxx", MediaType.APPLICATION_JSON, HttpStatus.UNAUTHORIZED);
	}
	
	/**
	 * Login y envía el token en el header.
	 */
    @Ignore 
	@Test
	public void authenticatedMethod() {
    	String authToken = login("system", "system", HttpStatus.OK);
		
		// Enviamos el token como un header:
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("X-Auth-Token", authToken);
		
        User resp = get(User.class, "/admin/users/1", headers, MediaType.APPLICATION_JSON, HttpStatus.OK);
        assertNotNull(resp);
	}
	
	/**
	 * Login y envía el token en el request.
	 */
    @Ignore 
	@Test
	public void authenticatedMethod2() {
        String authToken = login("system", "system", HttpStatus.OK);
        
        User resp = get(User.class, "/admin/users/1?token=" + authToken, MediaType.APPLICATION_JSON, HttpStatus.OK);
        assertNotNull(resp);
	}
}
