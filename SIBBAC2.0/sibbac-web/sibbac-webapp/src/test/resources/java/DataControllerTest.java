package sibbac.webapp;

import static org.junit.Assert.assertNotNull;
import gamma.commons.dao.Filter;
import gamma.commons.dao.Hints;
import gamma.commons.dao.Sort;
import gamma.exports.ISimpleEntity;
import gamma.exports.core.domain.Article;
import gamma.exports.core.domain.ArticleBu;
import gamma.exports.core.domain.BusinessUnit;
import gamma.exports.core.domain.Centre;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Pruebas de invocación a servicios de acceso a datos.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class DataControllerTest extends BaseControllerTest {
    @Ignore
    @Test
    public void findBy1() {
        // Enviamos el token como un header:
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("X-Auth-Token", "usu0001");
        headers.put("accept-language", "es-ES");
        
        Filter filter = new Filter();
        filter.add(Article.ARTICLE_BU_SET +"." + ArticleBu.BU + "." + BusinessUnit.ID, 1L);
        DataRequest req = new DataRequest(filter, new Sort(), new Hints());

        Response resp = post(req, Response.class, "/data/Article.post.json", headers, MediaType.APPLICATION_JSON, HttpStatus.OK);

        assertNotNull(resp);
    }

    @Test
    public void findBy2() {
        // Enviamos el token como un header:
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("X-Auth-Token", "usu0001");
        headers.put("accept-language", "es-ES");
        
        Filter filter = new Filter();
        filter.add(Centre.BU +"." + BusinessUnit.ID, 2L);
        DataRequest req = new DataRequest(filter, new Sort(), new Hints());

        Response resp = post(req, Response.class, "/data/Centre.post.json", headers, MediaType.APPLICATION_JSON, HttpStatus.OK);

        assertNotNull(resp);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void findSimpleEntities() {
        // Enviamos el token como un header:
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("X-Auth-Token", "usu0001");
        headers.put("accept-language", "es-ES");
        
        List<ISimpleEntity> resp = (List<ISimpleEntity>)get(List.class, "/data/Supplier.post.json", headers, MediaType.APPLICATION_JSON, HttpStatus.OK);

        assertNotNull(resp);
    }
}

