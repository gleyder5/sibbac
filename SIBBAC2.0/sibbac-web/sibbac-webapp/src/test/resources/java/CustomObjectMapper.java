package sibbac.webapp;

import gamma.commons.dao.Filter;
import gamma.commons.dao.Filter.OrFilter;
import gamma.commons.dao.Hints;
import gamma.commons.dao.Sort;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * Registra serializadores Jackson para objectos Filter.
 */
public class CustomObjectMapper extends ObjectMapper {
    /** Attribute "serialVersionUID". */
    private static final long serialVersionUID = 1L;

    public CustomObjectMapper() {
        super();

        final SimpleModule testModule = new SimpleModule("MyModule");
        testModule.addSerializer(Filter.class, new FilterJsonSerializer());
        testModule.addSerializer(Sort.class, new SortJsonSerializer());
        testModule.addSerializer(Hints.class, new HintsJsonSerializer());
        this.registerModule(testModule);
    }

    public class FilterJsonSerializer extends JsonSerializer<Filter> {
        @Override
        public void serialize(final Filter value, final JsonGenerator jgen, final SerializerProvider prov) throws IOException, JsonProcessingException {
            jgen.writeStartObject();

            if (value instanceof OrFilter) {
                jgen.writeStringField("cond", "or");
            }
            
            jgen.writeObjectField("rules", value.getRules());
            
            jgen.writeEndObject();
        }
    }

    public class HintsJsonSerializer extends JsonSerializer<Hints> {
        @Override
        public void serialize(final Hints value, final JsonGenerator jgen, final SerializerProvider prov) throws IOException, JsonProcessingException {
            jgen.writeStartObject();

            jgen.writeObjectField("includes", value.getIncludes());
            jgen.writeObjectField("pageSize", value.getPageSize());
            jgen.writeObjectField("rowStart", value.getRowStart());
            
            jgen.writeEndObject();

        }
    }

    public class SortJsonSerializer extends JsonSerializer<Sort> {
        @Override
        public void serialize(final Sort value, final JsonGenerator jgen, final SerializerProvider prov) throws IOException, JsonProcessingException {
            jgen.writeStartObject();

            jgen.writeArrayFieldStart("rules");
            for (gamma.commons.dao.Sort.Rule rule : value.getRules()) {
                jgen.writeObject(rule);
            }
            jgen.writeEndArray();

            jgen.writeEndObject();

        }
    }
}
