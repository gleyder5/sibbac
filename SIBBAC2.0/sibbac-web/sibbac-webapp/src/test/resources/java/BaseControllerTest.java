package sibbac.webapp;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpMessageConverterExtractor;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Usa RestTemplate para comunicarse con el servidor, pero está modificada para poder recibir los 
 * ErrorResponse en caso de BAD_REQUEST.
 */
@ContextConfiguration(locations = { "/spring-webservices.xml" })
public class BaseControllerTest {

	// TODO : Leer las propiedades generales y extractar la URL, el puerto y las rutas.
    public static final String LOCAL_SERVER = "http://127.0.0.1:8080/sibbac/rest";
    public static final String DEVEL_SERVER = "http://192.168.6.156/sibbac/rest";

    public static final String server = DEVEL_SERVER;

    protected static final Logger log = Logger.getLogger(BaseControllerTest.class);
    
    private Mode mode = Mode.Normal;
    
    private File dataFile = null;
    
    private ObjectMapper mapper = new ObjectMapper();
    
    public void setRecordMode(Mode mode, File file) {
        this.mode = mode;
        this.dataFile = file;
    }
    
    @Autowired
    protected RestTemplate restTemplate;
    
    protected <T,K> K post(T obj, Class<K> clazz, String url, MediaType mediaType, HttpStatus expectedStatus) {
        return execute(HttpMethod.POST, obj, clazz, url, new HashMap<String, String>(), mediaType, expectedStatus);
    }

    protected <T,K> K post(T obj, Class<K> clazz, String url, Map<String, String> headers, MediaType mediaType, HttpStatus expectedStatus) {
        return execute(HttpMethod.POST, obj, clazz, url, headers, mediaType, expectedStatus);
    }

    protected <K> K get(Class<K> clazz, String url, MediaType mediaType, HttpStatus expectedStatus) {
        return execute(HttpMethod.GET, null, clazz, url, new HashMap<String, String>(), mediaType, expectedStatus);
    }
    
    protected <K> K get(Class<K> clazz, String url, Map<String, String> headers, MediaType mediaType, HttpStatus expectedStatus) {
        return execute(HttpMethod.GET, null, clazz, url, headers, mediaType, expectedStatus);
    }
    
    protected void delete(String url, HttpStatus expectedStatus) {
        execute(HttpMethod.DELETE, null, Void.class, url, new HashMap<String, String>(), MediaType.TEXT_PLAIN, expectedStatus);
    }
    
    protected <K> K get2(Class<K> clazz, String url, HttpStatus status) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));

        ResponseEntity<K> response = restTemplate.getForEntity(server + url, clazz);
        
        if (status != null) {
            assertEquals(status, response.getStatusCode());
        }
        
        return response.getBody();
    }
    
    protected <T,K> K execute(HttpMethod method, T obj, Class<K> clazz, String url, Map<String, String> extHeaders, MediaType mediaType, HttpStatus expectedStatus) {
        ClientHttpRequestFactory baseFactory = restTemplate.getRequestFactory();
        
        BufferingClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(baseFactory);
        try {
            ClientHttpRequest req = factory.createRequest(new URI(server + url), method);

            HttpHeaders headers = req.getHeaders();
            headers.setContentType(mediaType);
            headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
            for (String headerName : extHeaders.keySet()) {
            	headers.add(headerName, extHeaders.get(headerName));
            }
            
            if (obj != null) {
                writeBody(obj, req);
            }
            
            ClientHttpResponse res = req.execute();

            assertEquals(expectedStatus, res.getStatusCode());
            
            if (res.getStatusCode() == HttpStatus.BAD_REQUEST) {
                log.info("Bad request");
            }
            else if (res.getStatusCode() == HttpStatus.OK) {
                ResponseExtractor<K> extractor = new HttpMessageConverterExtractor<K>(clazz, restTemplate.getMessageConverters());

                if (mode == Mode.Record) {
                    writeToFile(res.getBody(), dataFile);
                }
                else if (mode == Mode.Compare) {
                    compareData(res.getBody(), dataFile);
                }
                
                return extractor.extractData(res);
            }
            
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        
        return null;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void writeBody(Object requestBody, ClientHttpRequest req ) throws IOException {
        Class<?> requestType = requestBody.getClass();

        for (HttpMessageConverter messageConverter : restTemplate.getMessageConverters()) {
            if (messageConverter.canWrite(requestType, MediaType.APPLICATION_JSON)) {
                
                messageConverter.write(requestBody, MediaType.APPLICATION_JSON, req);
                return;
            }
        }

        throw new RestClientException("");
    }
    
    protected void writeToFile(InputStream body, File file) throws IOException {
        InputStreamReader reader = new InputStreamReader(body);
        PrintWriter writer = new PrintWriter(file);
        
        try {
            int len;
            char[] cbuff = new char[100];
            while ((len = reader.read(cbuff)) != -1) {
                writer.write(cbuff, 0, len);
            }
        }
        finally {
            writer.close();
            reader.close();
        }
    }
    
    protected void compareData(InputStream body, File file) throws IOException {
        InputStreamReader reader1 = new InputStreamReader(body);
        FileReader reader2 = new FileReader(file);
        
        try {
            int len1;
            char[] cbuff1 = new char[100];
            char[] cbuff2 = new char[100];
            while ((len1 = reader1.read(cbuff1)) != -1) {
                int len2 = reader2.read(cbuff2);
                
                assertEquals(len2, len1);
                Assert.assertArrayEquals(new String(cbuff2) + "\n" + new String(cbuff1), cbuff2, cbuff1);
            }
        }
        finally {
            reader2.close();
            reader1.close();
        }
    }
    
    public enum Mode {
        Normal,       // Normal
        Record,     // Graba los datos recibidos a un fichero
        Compare     // Compara los datos recibidos con un fichero
    };

    protected <K> K post(MultiValueMap<String, String> map, Class<K> clazz, String url, HttpStatus expectedStatus) {
        ClientHttpRequestFactory baseFactory = restTemplate.getRequestFactory();
        
        BufferingClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(baseFactory);
        try {
            ClientHttpRequest req = factory.createRequest(new URI(server + url), HttpMethod.POST);

            HttpHeaders headers = req.getHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            boolean found = false;
            for (HttpMessageConverter<?> messageConverter : restTemplate.getMessageConverters()) {
            	if (messageConverter instanceof FormHttpMessageConverter) {
            		((FormHttpMessageConverter)messageConverter).write(map, MediaType.APPLICATION_FORM_URLENCODED, req);
            		found = true;
            		break;
            	}
            }
            
            if (!found) {
            	throw new RestClientException("");
            }
            
            ClientHttpResponse res = req.execute();

            assertEquals(expectedStatus, res.getStatusCode());
            
            if (res.getStatusCode() == HttpStatus.BAD_REQUEST) {
                log.info("Bad request");
            }
            else if (res.getStatusCode() == HttpStatus.OK) {
                ResponseExtractor<K> extractor = new HttpMessageConverterExtractor<K>(clazz, restTemplate.getMessageConverters());

                if (mode == Mode.Record) {
                    writeToFile(res.getBody(), dataFile);
                }
                else if (mode == Mode.Compare) {
                    compareData(res.getBody(), dataFile);
                }
                
                return extractor.extractData(res);
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        
        return null;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T,K> K post(T obj, String fieldFileName, File file, Class<K> clazz, String url, Map<String, String> extHeaders, HttpStatus expectedStatus) {
        ClientHttpRequestFactory baseFactory = restTemplate.getRequestFactory();
        
        BufferingClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(baseFactory);
        try {
            ClientHttpRequest req = factory.createRequest(new URI(server + url), HttpMethod.POST);

            HttpHeaders headers = req.getHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
            for (String headerName : extHeaders.keySet()) {
                headers.add(headerName, extHeaders.get(headerName));
            }

            for (HttpMessageConverter messageConverter : restTemplate.getMessageConverters()) {
                if (messageConverter.canWrite(LinkedMultiValueMap.class, MediaType.MULTIPART_FORM_DATA)) {

                    HttpHeaders partHeaders = new HttpHeaders();
                    partHeaders.setContentType(new MediaType("text", "plain", Charset.forName("UTF-8"))); //.TEXT_PLAIN);  // Lo mandamos como text plain para que jackson lo intente deserializarlo
/*                    
                    partHeaders.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
*/                    
                    MultiValueMap<String, Object> mvm = new LinkedMultiValueMap<String, Object>();
                    mvm.add("json", new HttpEntity<T>(obj, partHeaders));
                    mvm.add(fieldFileName, new FileSystemResource(file));

                    messageConverter.write(mvm, MediaType.MULTIPART_FORM_DATA, req);
                }
            }

            ClientHttpResponse res = req.execute();

            assertEquals(expectedStatus, res.getStatusCode());
            
            if (res.getStatusCode() == HttpStatus.BAD_REQUEST) {
                log.info("Bad request");
            }
            else if (res.getStatusCode() == HttpStatus.OK) {
                ResponseExtractor<K> extractor = new HttpMessageConverterExtractor<K>(clazz, restTemplate.getMessageConverters());

                if (mode == Mode.Record) {
                    writeToFile(res.getBody(), dataFile);
                }
                else if (mode == Mode.Compare) {
                    compareData(res.getBody(), dataFile);
                }
                
                return extractor.extractData(res);
            }
            
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        
        return null;
    }
    
    
    String login(String username, String password, HttpStatus expectedStatus) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("username", username);
        if (password != null) {
            map.add("password", password);
        }
        
        return post(map, String.class, "/authenticate", expectedStatus);
    }
    
    public String toPrettyJson(Object obj) {
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException();
        }
    }
    
    public String toJson(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException();
        }
    }
}
