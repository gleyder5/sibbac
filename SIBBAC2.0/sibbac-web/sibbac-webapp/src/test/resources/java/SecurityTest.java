package gamma.web;

import static org.junit.Assert.assertNotNull;
import gamma.core.dto.ServiceDetails;
import gamma.exports.security.domain.User;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Pruebas de autenticación y autorización. SIN login, sólo paso del código de usuario.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class SecurityTest extends BaseControllerTest {
    @Test
    public void unauthorizedMethod() {
        get(User.class, "/almoarfi", MediaType.APPLICATION_JSON, HttpStatus.UNAUTHORIZED);
    }

	/**
	 * Envía el token en el header.
	 */
	@Test
	public void authenticatedMethod() {
    	String authToken = "codusu";
		
		// Enviamos el token como un header:
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("X-Auth-Token", authToken);
		
        ServiceDetails resp = get(ServiceDetails.class, "/almoarfi", headers, MediaType.APPLICATION_JSON, HttpStatus.OK);
        assertNotNull(resp);
	}
	
	/**
	 * Login y envía el token en el request.
	 */
	@Test
	public void authenticatedMethod2() {
        String authToken = "codusu";
        
        ServiceDetails resp = get(ServiceDetails.class, "/almoarfi?token=" + authToken, MediaType.APPLICATION_JSON, HttpStatus.OK);
        assertNotNull(resp);
	}
}
