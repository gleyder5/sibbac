package sibbac.webapp;

import static org.junit.Assert.assertNotNull;
import gamma.core.service.HostServiceError;
import gamma.core.service.IHostServiceResponse;
import gamma.services.host.cm83100n.Pe_datos_entrada;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Pruebas de invocación a servicios Host.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ServiceControllerTest extends BaseControllerTest {
	@Test
	public void executeWithFile() {
		// Enviamos el token como un header:
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("X-Auth-Token", "JTER");
		
		File file = new File("data/cm83100n-valid.xls");
		
        Pe_datos_entrada in = new Pe_datos_entrada();
        in.setPe_coprov("00000724");
        in.setPe_counne("DF");
        in.setPe_tab_unne(new String[] { "DF" });
        in.setPe_path_fichero("cm83100n-valid2.xls");
        in.setPe_ind_first_call(true);
        in.setPe_ind_end_of_data(true);
        in.setPe_inmodi(false);
        in.setPe_pomami(new BigDecimal(30.0));
		
		HSResponse resp = post(toJson(in), "file", file, HSResponse.class, "/execute/cm83100n", headers, HttpStatus.OK);
        assertNotNull(resp);
        System.out.println(toJson(resp));
	}

    @Ignore
    @Test
    public void execute() {
        // Enviamos el token como un header:
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("X-Auth-Token", "usu0001");
        headers.put("accept-language", "es-ES");
        
        String resp = post("{ \"name\" : \"VSF\" }", String.class, "/execute/almoarfi", headers, MediaType.TEXT_PLAIN, HttpStatus.OK);

        // protected <T,K> K execute(HttpMethod method, T obj, Class<K> clazz, String url, Map<String, String> extHeaders, HttpStatus expectedStatus) {
        
        assertNotNull(resp);
    }
  
    public static class HSResponse implements IHostServiceResponse {
        private Object output;
        
        private HostServiceError[] errors;
        
        @Override
        public Object getOutput() {
            return output;
        }

        @Override
        public HostServiceError[] getErrors() {
            return Arrays.copyOf(errors, errors.length);
        }
    }
}
