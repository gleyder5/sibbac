package sibbac.webapp;


import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith( SpringJUnit4ClassRunner.class )
/*
 * @ContextConfiguration( locations = {
 * "file:src/main/webapp/WEB-INF/spring-test.xml",
 * "file:src/main/webapp/WEB-INF/spring.xml",
 * // "file:src/main/webapp/WEB-INF/spring.security.xml",
 * "file:src/main/webapp/WEB-INF/servlet.dispatcher.xml",
 * "file:src/main/webapp/WEB-INF/spring/spring.web.xml"
 * } )
 */
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@ContextConfiguration( locations = {
		"file:src/test/resources/spring-test.xml", "file:src/main/webapp/WEB-INF/spring.xml",
		"file:src/main/webapp/WEB-INF/servlet.dispatcher.xml"
} )
@WebAppConfiguration( "file:target/sibbac-webapp-4.0.0" )
@SuppressWarnings( "unused" )
public class JMSTest extends BaseWebTest {

	protected static final Logger	LOG	= LoggerFactory.getLogger( JMSTest.class );

	public JMSTest() {
		// TODO Auto-generated constructor stub
	}

	@Test
	@Ignore
	public void test01Tasks() {
		LOG.debug( "[test01Tasks]" );
		try {
			LOG.debug( "[test01Tasks] Waiting 10 seconds..." );
			Thread.sleep( 10000 );
			LOG.debug( "[test01Tasks] Out." );
		} catch ( InterruptedException ex ) {
			Thread.currentThread().interrupt();
		}
	}

	@Test
	@Ignore
	public void test02JMSSend() {
		LOG.debug( "[test02JMSSend]" );

		Context ctx = null;
		QueueConnectionFactory qcf = null;
		QueueConnection qc = null;
		Queue q = null;
		QueueSession qs = null;
		TextMessage tm = null;

		try {
			ctx = ( Context ) new InitialContext().lookup( "java:comp/env" );
			qcf = ( QueueConnectionFactory ) ctx.lookup( "jms/MyQCF" );
			qc = qcf.createQueueConnection();
			q = ( Queue ) ctx.lookup( "jms/MyQ" );
			qs = qc.createQueueSession( false, Session.AUTO_ACKNOWLEDGE );
			tm = qs.createTextMessage();
			tm.setText( "Hi, there!" );
			/*
			 * QueueSender sender = qc.createSender();
			 * sender.send(tm);
			 * sender.close();
			 */
		} catch ( JMSException e ) {
		} catch ( NamingException e ) {
		} finally {
			if ( qs != null ) {
				try {
					qs.close();
				} catch ( JMSException e ) {
				}
			}
			if ( qc != null ) {
				try {
					qc.close();
				} catch ( JMSException e ) {
				}
			}
		}
	}

}

/*
 * JMSTest.java:[48,41] unreported exception javax.naming.NamingException; must be caught or declared to be thrown
 * JMSTest.java:[48,68] unreported exception javax.naming.NamingException; must be caught or declared to be thrown
 * JMSTest.java:[49,81] unreported exception javax.naming.NamingException; must be caught or declared to be thrown
 * JMSTest.java:[50,63] unreported exception javax.jms.JMSException; must be caught or declared to be thrown
 * JMSTest.java:[51,45] unreported exception javax.naming.NamingException; must be caught or declared to be thrown
 * JMSTest.java:[52,56] unreported exception javax.jms.JMSException; must be caught or declared to be thrown
 * JMSTest.java:[53,54] unreported exception javax.jms.JMSException; must be caught or declared to be thrown
 * JMSTest.java:[54,27] unreported exception javax.jms.JMSException; must be caught or declared to be thrown
 * JMSTest.java:[60,25] unreported exception javax.jms.JMSException; must be caught or declared to be thrown
 * JMSTest.java:[61,25] unreported exception javax.jms.JMSException; must be caught or declared to be thrown
 */
