package sibbac.webapp;


import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import sibbac.webapp.beans.WebResponse;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
		"file:src/test/resources/spring-test.xml", "file:src/main/webapp/WEB-INF/spring.xml",
		"file:src/main/webapp/WEB-INF/servlet.dispatcher.xml"
} )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@WebAppConfiguration( "file:target/sibbac-webapp-4.0.0" )
public class ReglasServiceTest extends BaseWebTest {

	protected static final Logger	LOG		= LoggerFactory.getLogger( ReglasServiceTest.class );

	private MockMvc					mockMvc;

	transient boolean				builded	= false;

	@Autowired
	private WebApplicationContext	webApplicationContext;

	@Before
	public void setUp() {
		String prefix = "[ReglasServiceTest::setUp] ";
		LOG.debug( prefix + "Setting the Web App Spring context..." );
		// We have to reset our mock between tests because the mock objects
		// are managed by the Spring container. If we would not reset them,
		// stubbing and verified behavior would "leak" from one test to another.
		// Mockito.reset(userService);
		if ( !builded ) {
			LOG.debug( prefix + "Rebuilding the Web App Spring context..." );
			mockMvc = MockMvcBuilders.webAppContextSetup( webApplicationContext ).build();
			builded = true;
		}
	}

	@Test
	@Ignore
	// @Sql({"file:src/test/resources/load.sql"})
			public
			void test10ListaDeReglas() {
		String prefix = "[ReglasServiceTest::test10ListaDeReglas] ";

		LOG.debug( prefix + "Solicitando la lista de reglas..." );
		try {
			ResultActions response = mockMvc.perform( post( "/reglas" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ).content( "" ) )
					.andDo( print() ).andExpect( content().contentType( "application/json" ) ).andExpect( status().isOk() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response, false );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	private static void displayResponse( final ResultActions response, final boolean parseContent ) {
		String prefix = "[ReglasServiceTest::displayResponse] ";
		MvcResult result = response.andReturn();
		MockHttpServletResponse servletResponse = result.getResponse();
		try {
			String content = servletResponse.getContentAsString();
			LOG.debug( prefix + "Response content: [{}]", content );
			if ( parseContent ) {
				WebResponse webResponse = mapper.readValue( content, WebResponse.class );
				LOG.debug( prefix + "+ error: [{}]", webResponse.getError() );
			}
		} catch ( UnsupportedEncodingException e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		} catch ( IOException e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

}
