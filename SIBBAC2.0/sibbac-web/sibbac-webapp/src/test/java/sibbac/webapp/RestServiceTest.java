package sibbac.webapp;


import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceJobsManagement;
import sibbac.webapp.business.SIBBACServiceNotificaciones;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
		"file:src/test/resources/spring-test.xml", "file:src/main/webapp/WEB-INF/spring.xml",
		"file:src/main/webapp/WEB-INF/servlet.dispatcher.xml"
} )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@WebAppConfiguration( "file:target/sibbac-webapp-4.0.0" )
public class RestServiceTest extends BaseWebTest {

	protected static final Logger	LOG		= LoggerFactory.getLogger( RestServiceTest.class );

	private MockMvc					mockMvc;

	transient boolean				builded	= false;

	@Autowired
	private WebApplicationContext	webApplicationContext;

	@Before
	public void setUp() {
		String prefix = "[RestServiceTest::setUp] ";
		LOG.debug( prefix + "Setting the Web App Spring context..." );
		// We have to reset our mock between tests because the mock objects
		// are managed by the Spring container. If we would not reset them,
		// stubbing and verified behavior would "leak" from one test to another.
		// Mockito.reset(userService);
		if ( !builded ) {
			LOG.debug( prefix + "Rebuilding the Web App Spring context..." );
			mockMvc = MockMvcBuilders.webAppContextSetup( webApplicationContext ).build();
			builded = true;
		}
	}

	// @Sql({"file:src/test/resources/load.sql"})
	@Test
	public void test01LoadData() {
		String prefix = "[RestServiceTest::test01LoadData] ";

		Map< String, String > headers = new HashMap< String, String >();
		headers.put( "X-Auth-Token", "usu0001" );
		headers.put( "Accept-Language", "es-ES" );
		headers.put( "Content-Type", "application/json" );

		LOG.debug( prefix + "Requesting something..." );
		try {
			ResultActions response = mockMvc.perform( post( "/people" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ).content( "" ) )
					.andDo( print() ).andExpect( content().contentType( "application/json" ) ).andExpect( status().isOk() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response, false );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	@Test
	public void test10ServicesRequest() {
		String prefix = "[RestServiceTest::test10ServicesRequest] ";
		try {
			ResultActions response = mockMvc.perform( post( "/services" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ).content( "" ) )
					.andDo( print() ).andExpect( content().contentType( "application/json" ) ).andExpect( status().isOk() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	@Test
	public void test11ServiceRequest() {
		String prefix = "[RestServiceTest::test11ServiceRequest] ";

		WebRequest webRequest = new WebRequest();
		webRequest.setAction( "get" );
		webRequest.setService( "facturacion" );
		Map< String, String > filtros = new Hashtable< String, String >();
		filtros.put( "username", "pacopepe" );
		webRequest.setFilters( filtros );
		Map< String, String > autoComplete = new Hashtable< String, String >();
		autoComplete.put( "name", "art" );
		webRequest.setAutoComplete( autoComplete );
		String jsonWebRequest = this.toJson( webRequest );
		String jsonPrettyWebRequest = this.toPrettyJson( webRequest );
		LOG.debug( "WebRequest to JSON:" + NL + jsonPrettyWebRequest );
		jsonWebRequest = jsonWebRequest.replace( "\"", "\\\"" );
		LOG.debug( "Requesting: /service=" + jsonWebRequest );
		try {
			ResultActions response = mockMvc
					.perform(
							post( "/service" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ).content(
									TestUtil.convertObjectToJsonBytes( webRequest ) ) ).andDo( print() )
					.andExpect( content().contentType( "application/json" ) ).andExpect( status().isBadRequest() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	@Test
	public void test31SIBBACJobManagementListing() {
		String prefix = "[RestServiceTest::test31SIBBACJobManagementListing] ";

		WebRequest webRequest = new WebRequest();
		webRequest.setService( "SIBBACServiceJobsManagement" );
		webRequest.setAction( SIBBACServiceJobsManagement.CMD_LISTING );
		String jsonPrettyWebRequest = this.toPrettyJson( webRequest );
		LOG.debug( prefix + "WebRequest to JSON:" + NL + jsonPrettyWebRequest );
		try {
			ResultActions response = mockMvc
					.perform(
							post( "/service" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ).content(
									TestUtil.convertObjectToJsonBytes( webRequest ) ) ).andDo( print() )
					.andExpect( content().contentType( "application/json" ) ).andExpect( status().isOk() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	@Test
	public void test32SIBBACJobManagementStart() {
		String prefix = "[RestServiceTest::test32SIBBACJobManagementStart] ";

		WebRequest webRequest = new WebRequest();
		webRequest.setService( "SIBBACServiceJobsManagement" );
		webRequest.setAction( SIBBACServiceJobsManagement.CMD_START_JOB );
		String jsonPrettyWebRequest = this.toPrettyJson( webRequest );
		LOG.debug( prefix + "WebRequest to JSON:" + NL + jsonPrettyWebRequest );
		try {
			ResultActions response = mockMvc
					.perform(
							post( "/service" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ).content(
									TestUtil.convertObjectToJsonBytes( webRequest ) ) ).andDo( print() )
					.andExpect( content().contentType( "application/json" ) ).andExpect( status().isOk() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	@Test
	public void test40i18n() {
		String prefix = "[RestServiceTest::test40i18n] ";
		LOG.debug( prefix + "Requesting the translations..." );
		try {
			ResultActions response = mockMvc.perform( post( "/i18n" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ) ).andDo( print() )
					.andExpect( content().contentType( "application/json" ) ).andExpect( status().isOk() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	@Test
	public void test50Menu01() {
		String prefix = "[RestServiceTest::test50Menu01] ";
		LOG.debug( prefix + "Requesting the menu items..." );
		try {
			ResultActions response = mockMvc.perform( post( "/menu/raquel" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ) )
					.andDo( print() ).andExpect( content().contentType( "application/json" ) ).andExpect( status().isOk() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	@Test
	public void test50Notificaciones() {
		String prefix = "[RestServiceTest::test50Notificaciones] ";
		LOG.debug( prefix + "Requesting the notifications..." );
		WebRequest webRequest = new WebRequest();
		webRequest.setService( "SIBBACServiceNotificaciones" );
		webRequest.setAction( SIBBACServiceNotificaciones.CMD_LISTING );
		String jsonPrettyWebRequest = this.toPrettyJson( webRequest );
		LOG.debug( prefix + "WebRequest to JSON:" + NL + jsonPrettyWebRequest );
		try {
			ResultActions response = mockMvc
					.perform(
							post( "/service" ).contentType( TestUtil.APPLICATION_JSON_UTF8 ).content(
									TestUtil.convertObjectToJsonBytes( webRequest ) ) ).andDo( print() )
					.andExpect( content().contentType( "application/json" ) ).andExpect( status().isOk() );
			LOG.debug( prefix + "GOT response: [response=={}]", response );
			assertNotNull( response );
			displayResponse( response );
		} catch ( Exception e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

	private static void displayResponse( final ResultActions response ) {
		displayResponse( response, true );
	}

	private static void displayResponse( final ResultActions response, final boolean parseContent ) {
		String prefix = "[RestServiceTest::displayResponse] ";
		MvcResult result = response.andReturn();
		MockHttpServletResponse servletResponse = result.getResponse();
		try {
			String content = servletResponse.getContentAsString();
			LOG.debug( prefix + "Response content: [{}]", content );
			if ( parseContent ) {
				WebResponse webResponse = mapper.readValue( content, WebResponse.class );
				LOG.debug( prefix + "+ error: [{}]", webResponse.getError() );
			}
		} catch ( UnsupportedEncodingException e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		} catch ( IOException e ) {
			LOG.error( "ERROR({}): [{}]", e.getClass().getName(), e.getMessage() );
		}
	}

}
