package sibbac.webapp;


import java.util.HashMap;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;


/*
 * import org.junit.Test;
 * import org.junit.runner.RunWith;
 * import org.springframework.beans.factory.annotation.Autowired;
 * import org.springframework.test.context.ContextConfiguration;
 * import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 * import org.springframework.test.context.web.WebAppConfiguration;
 * 
 * import java.util.Arrays;
 * 
 * import static org.junit.Assert.assertEquals;
 * 
 * import java.io.File;
 * import java.io.FileReader;
 * import java.io.IOException;
 * import java.io.InputStream;
 * import java.io.InputStreamReader;
 * import java.io.PrintWriter;
 * import java.net.URI;
 * import java.net.URISyntaxException;
 * import java.nio.charset.Charset;
 * import java.util.Arrays;
 * import java.util.HashMap;
 * import java.util.Map;
 * 
 * 
 * import org.junit.Assert;
 * import org.junit.FixMethodOrder;
 * import org.slf4j.Logger;
 * import org.slf4j.LoggerFactory;
 * import org.springframework.core.io.FileSystemResource;
 * import org.springframework.http.HttpEntity;
 * import org.springframework.http.HttpHeaders;
 * import org.springframework.http.HttpMethod;
 * import org.springframework.http.HttpStatus;
 * import org.springframework.http.MediaType;
 * import org.springframework.http.ResponseEntity;
 * import org.springframework.http.client.BufferingClientHttpRequestFactory;
 * import org.springframework.http.client.ClientHttpRequest;
 * import org.springframework.http.client.ClientHttpRequestFactory;
 * import org.springframework.http.client.ClientHttpResponse;
 * import org.springframework.http.converter.FormHttpMessageConverter;
 * import org.springframework.http.converter.HttpMessageConverter;
 * import org.springframework.util.LinkedMultiValueMap;
 * import org.springframework.util.MultiValueMap;
 * import org.springframework.web.client.HttpMessageConverterExtractor;
 * import org.springframework.web.client.ResponseExtractor;
 * import org.springframework.web.client.RestClientException;
 * import org.springframework.web.client.RestTemplate;
 * 
 * import sibbac.test.BaseTest;
 * 
 * import com.fasterxml.jackson.core.JsonProcessingException;
 * import com.fasterxml.jackson.databind.ObjectMapper;
 * 
 * import org.junit.Test;
 * import org.junit.runner.RunWith;
 */

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
		"file:src/test/resources/spring-test.xml", "file:src/main/webapp/WEB-INF/spring.xml",
		"file:src/main/webapp/WEB-INF/servlet.dispatcher.xml"
} )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@WebAppConfiguration( "file:target/sibbac-webapp-4.0.0" )
public class ServiceTest extends BaseWebTest {

	protected static final Logger	LOG	= LoggerFactory.getLogger( ServiceTest.class );

	@Autowired
	private WebApplicationContext	webApplicationContext;

	@Test
	@Ignore
	// @Sql({"file:src/test/resources/load.sql"})
			public
			void test61LoadDataWithJetty() {
		Map< String, String > headers = new HashMap< String, String >();
		headers.put( "X-Auth-Token", "usu0001" );
		headers.put( "Accept-Language", "es-ES" );
		headers.put( "Content-Type", "application/json" );
	}

}
